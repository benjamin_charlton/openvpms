/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.TaskQueueHandle;
import nextapp.echo2.webcontainer.ContainerContext;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * A {@link TaskQueue} that can be stopped and started.
 * <p/>
 * This is useful to avoid unwanted synchronisation occurring while editing, as this can premature cause events
 * to be sent to the server.
 * <p/>
 * NOTE: while an interval is provided to determine the interval in seconds between asynchronous callbacks from the
 * client, Echo uses the minimum interval from all registered queues.
 *
 * @author Tim Anderson
 */
class RestartableTaskQueue implements TaskQueue, RestartableTask {

    /**
     * The application instance. A WeakReference is held to avoid memory leaks should the ApplicationInstance be
     * disposed independently of this.
     */
    private final WeakReference<ApplicationInstance> app;

    /**
     * The the interval in seconds between asynchronous callbacks from the client to check for queued tasks for this
     * queue, or {@code 0} to use the default.
     */
    private final int interval;

    /**
     * Determines how tasks should be queued if the queue is stopped.
     */
    private final TaskQueues.QueueMode queueMode;

    /**
     * The queue handle. Note that the handle has a reference to the ApplicationInstance.
     */
    private WeakReference<TaskQueueHandle> queue;

    /**
     * Tasks received after the queue was stopped.
     */
    private final List<Runnable> pending = new ArrayList<>();

    /**
     * Constructs a {@link RestartableTaskQueue}.
     *
     * @param app       the application instance
     * @param interval  the interval in seconds between asynchronous callbacks from the client to check
     *                  for queued tasks for this queue, or {@code 0} to use the default
     * @param start     if {@code true} start queueing tasks
     * @param queueMode determines how tasks are queued while the queue is stopped
     */
    public RestartableTaskQueue(ApplicationInstance app, int interval, boolean start, TaskQueues.QueueMode queueMode) {
        this.app = new WeakReference<>(app);
        this.interval = interval;
        this.queueMode = queueMode;
        if (start) {
            start();
        }
    }

    /**
     * Starts the task queue.
     * <p/>
     * If the queue is already running, this is ignored.
     *
     * @return {@code true} if the queue was started, {@code false} if it was already running or has been disposed
     */
    @Override
    public synchronized boolean start() {
        boolean result = false;
        if (queue == null) {
            ApplicationInstance instance = app.get();
            if (instance != null) {
                TaskQueueHandle handle = instance.createTaskQueue();
                queue = new WeakReference<>(handle);
                if (interval != 0) {
                    ContainerContext context = (ContainerContext) instance.getContextProperty(
                            ContainerContext.CONTEXT_PROPERTY_NAME);
                    if (context != null) {
                        context.setTaskQueueCallbackInterval(handle, interval * 1000);
                    }
                }
                ListIterator<Runnable> iterator = pending.listIterator();
                while (iterator.hasNext()) {
                    Runnable next = iterator.next();
                    iterator.remove();
                    instance.enqueueTask(handle, next);
                }
            }
            result = true;
        }
        return result;
    }

    /**
     * Stops the task.
     *
     * @return {@code true} if the task was stopped, {@code false} if it was already stopped
     */
    public synchronized boolean stop() {
        boolean stopped = false;
        if (queue != null) {
            TaskQueueHandle handle = queue.get();
            if (handle != null) {
                ApplicationInstance instance = app.get();
                if (instance != null) {
                    instance.removeTaskQueue(handle);
                    stopped = true;
                }
                queue = null;
            }
        }
        return stopped;
    }

    /**
     * Disposes of this queue.
     */
    @Override
    public synchronized void dispose() {
        pending.clear();
        stop();
        app.clear();
    }

    /**
     * Enqueues a task to be run during the next client/server synchronization.<br/>
     * in changes being pushed to the client.
     *
     * @param task the task to queue
     * @return {@code true} if the task was queued, otherwise {@code false}
     */
    @Override
    public synchronized boolean queue(Runnable task) {
        TaskQueueHandle handle = queue != null ? queue.get() : null;
        return queue(task, handle, app.get());
    }

    /**
     * Determines if the task is running.
     *
     * @return {@code true} if the task is running
     */
    @Override
    public synchronized boolean isRunning() {
        return queue != null && queue.get() != null;
    }

    /**
     * Returns the the interval between asynchronous callbacks from the client to check for queued tasks for this queue.
     * <p/>
     * NOTE: this is indicative only. Echo uses the minimum interval from all registered queues.
     *
     * @return the interval, in seconds
     */
    public int getInterval() {
        return interval;
    }

    /**
     * Queues a task.
     *
     * @param task     the task
     * @param handle   the task queue handle. May be {@code null}
     * @param instance the application instance. May be {@code null}
     * @return {@code true} if the task was queued, otherwise {@code false}
     */
    private boolean queue(Runnable task, TaskQueueHandle handle, ApplicationInstance instance) {
        boolean result = false;
        if (instance != null) {
            if (handle != null) {
                instance.enqueueTask(handle, task);
                result = true;
            } else {
                switch (queueMode) {
                    case QUEUE_ALL:
                        pending.add(task);
                        result = true;
                        break;
                    case QUEUE_LAST:
                        pending.clear();
                        pending.add(task);
                        result = true;
                        break;
                    default:
                        // discard;
                        break;
                }
            }
        }
        return result;
    }
}