/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

/**
 * Queue for tasks to be run during the next client/server synchronization.
 *
 * @author Tim Anderson
 */
public interface TaskQueue {

    /**
     * Enqueues a task to be run during the next client/server synchronization.<br/>
     * The task will be run  <b>synchronously</b> in the user interface update thread.<br/>
     * Enqueuing a task in response to an external event will result
     * in changes being pushed to the client.
     *
     * @param task the task to queue
     * @return {@code true} if the task was queued, otherwise {@code false}
     */
    boolean queue(Runnable task);

    /**
     * Disposes of this queue.
     */
    void dispose();
}