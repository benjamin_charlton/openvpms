/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.openvpms.web.echo.servlet.SessionMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

/**
 * Periodically schedules a background task that has an associated user interface.
 * <p/>
 * This can be used to decouple the user interface from tasks that would block user input if they were to
 * run in the UI thread.
 * <p/>
 * The background task runs while the supplied condition indicates that it is not complete.
 * <p/>
 * NOTE: this uses {@link TaskQueues} to queue updates to the callback via the user interface thread. It must
 * therefore be started after its parent dialog is displayed to avoid UI updates being suspended.
 *
 * @author Tim Anderson
 */
public class PeriodicBackgroundTask {

    /**
     * The executor service to periodically schedule the task.
     */
    private final ScheduledExecutorService executor;

    /**
     * The delay between running the task.
     */
    private final int interval;

    /**
     * The units of the delay.
     */
    private final TimeUnit units;

    /**
     * The session monitor, used to keep the session alive if required.
     */
    private final SessionMonitor monitor;

    /**
     * The condition to evaluate to determine if the task is complete.
     */
    private final Supplier<Boolean> condition;

    /**
     * The task to run in the background. This returns {@code true} if the UI needs to be updated.
     */
    private final Supplier<Boolean> task;

    /**
     * The callback to update the UI.
     */
    private final Runnable callback;

    /**
     * Used to prevent the job running while the UI is refreshing, and vice-versa.
     * This prevents the UI blocking on resources used or provided by the task.
     */
    private final Lock lock = new ReentrantLock();

    /**
     * Used to schedule UI updates.
     */
    private ApplicationInstanceRunnable callbackRunner;

    /**
     * Handle used to cancel the job.
     */
    private ScheduledFuture<?> future;

    /**
     * Determines if the UI callback needs to be invoked.
     */
    private volatile boolean updateRequired;

    /**
     * Determines if the task is complete.
     * This is evaluated within the background thread and used in the UI thread, rather than calling
     * {@link #isComplete()}, to avoid the UI blocking on resources locked by the background thread.
     */
    private volatile boolean complete;

    /**
     * Factory for threads to run background tasks.
     */
    private static final BasicThreadFactory factory
            = new BasicThreadFactory.Builder().namingPattern("PeriodicBackgroundTask-%d").daemon(true).build();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PeriodicBackgroundTask.class);

    /**
     * Constructs a {@link PeriodicBackgroundTask}.
     *
     * @param interval  the frequency to call the job
     * @param units     the frequency units
     * @param monitor   if specified, keeps the http session alive while the task is running. This will prevent lock
     *                  screens being displayed. May be {@code null}
     * @param condition completion condition. Returns {@code true} if the task is complete
     * @param task      the task to execute while {@code condition} returns {@code false}.
     *                  Returns {@code true} if the UI needs to update
     * @param callback  the callback to invoke when the task returns {@code true}, to update the user interface
     */
    public PeriodicBackgroundTask(int interval, TimeUnit units, SessionMonitor monitor, Supplier<Boolean> condition,
                                  Supplier<Boolean> task, Runnable callback) {
        this.interval = interval;
        this.units = units;
        this.monitor = monitor;
        this.condition = condition;
        this.task = task;
        this.callback = callback;
        executor = Executors.newScheduledThreadPool(1, factory);
    }

    /**
     * Starts the task running.
     */
    public void start() {
        // run the UI callback twice as often as the background task, to limit timing mismatches
        callbackRunner = new ApplicationInstanceRunnable(interval / 2, TaskQueues.QueueMode.DISCARD, this::runCallback);
        future = executor.scheduleWithFixedDelay(this::runTask, interval, interval, units);
        callbackRunner.run();
    }

    /**
     * Disposes of the task.
     */
    public void dispose() {
        if (callbackRunner != null) {
            callbackRunner.dispose();
        }
        executor.shutdownNow();
    }

    /**
     * Runs the task, if:
     * <ul>
     *     <li>the UI doesn't need to refresh; and</li>
     *     <li>the condition returns {@code true}</li>
     * </ul>
     * If the UI hasn't refreshed yet, the task is skipped.
     * <br/>
     * If the condition returns {@code false}, the task is cancelled.
     */
    private void runTask() {
        if (!updateRequired) {
            lock.lock();
            try {
                complete = isComplete();
                if (!complete) {
                    try {
                        Boolean status = task.get();
                        updateRequired = status != null && status;
                    } catch (Throwable exception) {
                        log.warn("Failed to run background task: " + exception.getMessage(), exception);
                    }
                    complete = isComplete();
                }
                if (complete) {
                    future.cancel(false);
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Runs the UI callback if:
     * <ul>
     *     <li>the task has indicated the UI needs to update; and</li>
     *     <li>the task is not currently running</li>
     * </ul>
     */
    private void runCallback() {
        if (monitor != null) {
            monitor.active();  // keep the session alive
        }
        if (updateRequired) {
            if (lock.tryLock()) {
                // only refresh the UI with the task is not running, as it may cause it to block
                try {
                    callback.run();
                } catch (Throwable exception) {
                    log.warn("Failed to perform UI update: " + exception.getMessage(), exception);
                } finally {
                    updateRequired = false;
                    lock.unlock();
                }
            }
        }
        if (!complete) {
            callbackRunner.run(); // reschedule
        }
    }

    /**
     * Determines if the task is complete.
     *
     * @return {@code true} if the task is complete
     */
    private boolean isComplete() {
        try {
            Boolean result = condition.get();
            return result != null && result;
        } catch (Throwable exception) {
            log.warn("Failed to check condition: " + exception.getMessage(), exception);
        }
        return false;
    }
}
