/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

import echopointng.xhtml.XhtmlFragment;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Insets;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.echo.error.ErrorHandler;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.spring.SpringApplicationInstance;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextArea;
import org.openvpms.web.echo.util.StyleSheetHelper;

import java.util.Map;
import java.util.function.Consumer;


/**
 * A generic modal dialog that displays a message.
 *
 * @author Tim Anderson
 */
public abstract class MessageDialog extends ModalDialog {

    /**
     * The preamble. If present, this is displayed before the message.
     */
    private final String preamble;

    /**
     * The message.
     */
    private final String message;

    /**
     * Determines if the message is an HTML fragment or plain text.
     */
    private final boolean messageHTML;

    /**
     * The footer. If present, this is displayed after the message.
     */
    private final String footer;

    /**
     * Dialog style name.
     */
    private static final String STYLE = "MessageDialog";


    /**
     * Constructs a {@link MessageDialog}.
     *
     * @param title   the dialog title
     * @param message the message to display
     * @param buttons the buttons to display
     */
    public MessageDialog(String title, String message, String[] buttons) {
        this(title, message, STYLE, buttons);
    }

    /**
     * Constructs a {@link MessageDialog}.
     *
     * @param title   the dialog title
     * @param message the message to display
     * @param buttons the buttons to display
     * @param help    the help context. May be {@code null}
     */
    public MessageDialog(String title, String message, String[] buttons, HelpContext help) {
        this(title, message, STYLE, buttons, help);
    }

    /**
     * Constructs a {@link MessageDialog}.
     *
     * @param title   the dialog title
     * @param message the message to display
     * @param style   the dialog style
     * @param buttons the buttons to display
     */
    public MessageDialog(String title, String message, String style, String[] buttons) {
        this(title, message, style, buttons, null);
    }

    /**
     * Constructs a {@link MessageDialog}.
     *
     * @param title   the dialog title
     * @param message the message to display
     * @param style   the dialog style
     * @param buttons the buttons to display
     * @param help    the help context. May be {@code null}
     */
    public MessageDialog(String title, String message, String style, String[] buttons, HelpContext help) {
        super(title, style, buttons, help);
        this.message = message;
        this.messageHTML = false;
        this.preamble = null;
        this.footer = null;
    }

    /**
     * Constructs a {@link MessageDialog}.
     *
     * @param builder the builder
     */
    protected MessageDialog(MessageDialogBuilder<?, ?> builder) {
        super(builder.getTitle(), builder.getStyle(), new String[0], builder.getHelp());
        if (builder.getButtons().isEmpty()) {
            builder.ok();
        }
        for (String button : builder.getButtons()) {
            addButton(button, builder.disableShortcut(button));
        }
        setDefaultButton(builder.getDefaultButton());
        this.preamble = builder.getPreamble();
        this.message = builder.getMessage();
        this.messageHTML = builder.isMessageHTML() && isValidHTML(message);
        // if a message was supposed to be HTML, but isn't, just display as plain text
        this.footer = builder.getFooter();
        Consumer<String> defaultListener = builder.getDefaultListener();
        Map<String, Runnable> listeners = builder.getListeners();
        if (defaultListener != null || !listeners.isEmpty()) {
            addWindowPaneListener(new WindowPaneListener() {
                @Override
                public void onClose(WindowPaneEvent event) {
                    onButton(listeners, defaultListener);
                }
            });
        } else if (builder.getListener() != null) {
            addWindowPaneListener(builder.getListener());
        }
        String size = builder.getSize();
        if (size != null) {
            resize(size);
        } else if (builder.getSizeToContent()) {
            sizeToContent();
        }
        if (builder.getX() != null) {
            setPositionX(builder.getX());
        }
        if (builder.getY() != null) {
            setPositionY(builder.getY());
        }
    }

    /**
     * Returns the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Shows the dialog for a limited time.
     *
     * @param time the time to show the window for, in seconds
     */
    public void show(int time) {
        show();
        SpringApplicationInstance app = (SpringApplicationInstance) getApplicationInstance();
        app.getTaskQueues().schedule(time, this::userClose);
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        if (preamble == null && footer == null) {
            Label content;
            if (messageHTML) {
                content = LabelFactory.html(message, true);
            } else {
                content = LabelFactory.create(true, true);
                content.setText(message);
            }
            Row row = RowFactory.create(Styles.LARGE_INSET, content);
            getLayout().add(row);
        } else {
            // display the preamble, message and footer, if present.
            // For plain text messages, the message is displayed in a text area.
            // This needs to jump through some hoops in order for the text area to grow to the dialog width.
            Insets large = StyleSheetHelper.getInsets(Column.class, Styles.LARGE_INSET, Column.PROPERTY_INSETS);
            Insets insets = StyleSheetHelper.getInsets(Column.class, Styles.INSET, Column.PROPERTY_INSETS);
            Grid grid = GridFactory.create(1);
            grid.setInsets(new Insets(large.getLeft(), new Extent(0), large.getRight(), new Extent(0)));
            grid.setWidth(Styles.FULL_WIDTH);
            grid.setColumnWidth(0, Styles.FULL_WIDTH);
            grid.setHeight(Styles.FULL_HEIGHT);
            if (preamble != null) {
                Label label = LabelFactory.text(preamble, true);
                Row row = RowFactory.create(label);
                row.setInsets(new Insets(new Extent(0), large.getTop(), new Extent(0), insets.getBottom()));
                grid.add(row);
            }
            if (messageHTML) {
                // currently don't support limiting the no. of displayed lines of HTML.
                grid.add(LabelFactory.html(message, true));
            } else {
                TextArea text = createTextArea(message);
                grid.add(text);
                if (footer != null) {
                    // display up to 10 lines of the message
                    int lines = 0;
                    if (!StringUtils.isEmpty(message)) {
                        lines = StringUtils.countMatches(message, "\n");
                        lines = Math.max(message.length() / 60, lines);
                    }
                    if (lines < 3) {
                        lines = 3;
                    } else if (lines > 10) {
                        lines = 10;
                    }
                    text.setHeight(new Extent(lines, Extent.EM));
                } else {
                    text.setHeight(new Extent(90, Extent.PERCENT));
                    grid.setRowHeight(grid.getComponentCount() - 1, Styles.FULL_HEIGHT);
                }
                text.setWidth(Styles.FULL_WIDTH);
            }
            if (footer != null) {
                Label label = LabelFactory.text(footer, true);
                Row row = RowFactory.create(label);
                row.setInsets(new Insets(new Extent(0), large.getTop(), new Extent(0), large.getBottom()));
                grid.add(row);
            }
            getLayout().add(grid);
        }
    }

    /**
     * Size the dialog to the content.
     * NOTE: at present, this sets the content height not the width, and is restricted to dialogs
     * that display plain text.
     */
    protected void sizeToContent() {
        int fontSize = StyleSheetHelper.getProperty("font.size", 10);
        int lines = getLines(message);
        if (footer != null) {
            lines += getLines(footer);
            lines++;
        }
        lines += 2;  // padding
        setContentHeight(lines * fontSize);
    }

    /**
     * Creates a text area to display the message.
     *
     * @param message the message
     * @return the text area
     */
    protected TextArea createTextArea(String message) {
        TextArea text = TextComponentFactory.createTextArea();
        text.setText(message);
        text.setEnabled(false);
        return text;
    }

    /**
     * Returns the lines to render a message.
     *
     * @param text the text
     * @return the lines
     */
    private int getLines(String text) {
        int lines = StringUtils.countMatches(text, "\n");
        if (lines == 0) {
            // no lines, so calculate lines based on message length
            lines = (text.length() / 60);
        }
        return lines;
    }

    /**
     * Invoked when a button is pressed.
     *
     * @param listeners       the available listeners
     * @param defaultListener the default listener. May be {@code null}
     */
    private void onButton(Map<String, Runnable> listeners, Consumer<String> defaultListener) {
        boolean handled = false;
        String action = getAction();
        if (action != null) {
            Runnable runnable = listeners.get(action);
            if (runnable != null) {
                handled = true;
                try {
                    runnable.run();
                } catch (Exception exception) {
                    ErrorHandler.getInstance().error(exception);
                }
            }
        }
        if (!handled && defaultListener != null) {
            try {
                defaultListener.accept(action);
            } catch (Exception exception) {
                ErrorHandler.getInstance().error(exception);
            }
        }
    }

    /**
     * Determines if a message is a valid HTML fragment.
     *
     * @param message the message
     * @return {@code true} if the message is a valid HTML fragment, otherwise {@code false}
     */
    private boolean isValidHTML(String message) {
        return message != null && XhtmlFragment.isValidXML(message) == null;
    }

}
