/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.servlet.file;

import com.atlassian.plugin.servlet.ContentTypeResolver;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@link ContentTypeResolver}.
 *
 * @author Tim Anderson
 */
public class ContentTypeResolverImpl implements ContentTypeResolver {

    private final Map<String, String> mimeTypes;

    /**
     * Default constructor.
     */
    public ContentTypeResolverImpl() {
        Map<String, String> types = new HashMap<String, String>();
        types.put(".js", "application/x-javascript");
        types.put(".css", "text/css");
        mimeTypes = Collections.unmodifiableMap(types);
    }

    /**
     * Returns the content type for the given resource path.
     */
    public String getContentType(String requestUrl) {
        String extension = requestUrl.substring(requestUrl.lastIndexOf('.'));
        return mimeTypes.get(extension);
    }

}
