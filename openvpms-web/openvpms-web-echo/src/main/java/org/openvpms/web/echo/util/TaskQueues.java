/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.WindowPane;
import org.openvpms.web.echo.dialog.DialogManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Manages a set of {@link TaskQueue}s.
 * <p/>
 * These are suspended when a dialog displayed via {@link DialogManager} and resumed when the dialog closes, to
 * avoid Echo2 synchronisation being triggered while users are editing.
 * <p/>
 * TaskQueues created after a dialog is shown remain active until a new dialog is displayed.
 *
 * @author Tim Anderson
 * @see PeriodicTask
 * @see TaskQueue
 */
public class TaskQueues {

    /**
     * Determines the behaviour when a queue is stopped.
     */
    public enum QueueMode {
        DISCARD,
        // Discard tasks when the queue is stopped. This should be used if execution is not important and limits memory
        // use.
        QUEUE_LAST,
        // Discard all but the last task when the queue is stopped. This should be if it least one task should be
        // executed.
        QUEUE_ALL,
        // Queue all tasks when the queue is stopped. Tasks will be delivered when the queue restarts,
        // or discarded when the queue is disposed of.
    }

    /**
     * The tasks.
     */
    private final Set<RestartableTask> tasks = Collections.synchronizedSet(new HashSet<>());

    /**
     * The suspended tasks.
     */
    private final Map<RestartableTask, List<WeakReference<WindowPane>>> suspendedTasks
            = Collections.synchronizedMap(new HashMap<>());


    /**
     * The application instance.
     */
    private ApplicationInstance app;

    /**
     * Constructs a {@link TaskQueues}.
     *
     * @param app the application instance
     */
    public TaskQueues(ApplicationInstance app) {
        this.app = app;
    }

    /**
     * Schedules a task to run once, after a delay.
     *
     * @param delay the delay, in seconds
     * @param task  the task to run
     */
    public void schedule(int delay, Runnable task) {
        PeriodicTask periodicTask = newTask(delay, delay, true, task);
        periodicTask.start();
    }

    /**
     * Creates a new {@link PeriodicTask}.
     * <p/>
     * Tasks must be started in order to execute. Tasks started before a dialog is displayed will be stopped and resumed
     * after the dialog is closed.
     *
     * @param interval the frequency to run the task, in seconds
     * @param task     the task to run
     * @return a new task
     */
    public PeriodicTask newTask(int interval, Runnable task) {
        return newTask(0, interval, task);
    }

    /**
     * Creates a new {@link PeriodicTask}.
     * <p/>
     * Tasks must be started in order to execute. Tasks started before a dialog is displayed will be stopped and resumed
     * after the dialog is closed.
     *
     * @param initialDelay the delay before the very first execution, in seconds
     * @param interval     the frequency to run the task, in seconds
     * @param task         the task to run
     * @return a new task
     */
    public PeriodicTask newTask(int initialDelay, int interval, Runnable task) {
        return newTask(initialDelay, interval, false, task);
    }

    /**
     * Returns a new queue.
     * <p/>
     * Invoking {@link #suspend(WindowPane) suspend} prevents the queue from executing new tasks,
     * until {@link #resume(WindowPane) resume} is invoked.
     *
     * @param queueMode determines how tasks are queued while the queue is stopped
     * @return a new queue
     */
    public TaskQueue newQueue(QueueMode queueMode) {
        return newQueue(0, queueMode);
    }

    /**
     * Returns a new queue.
     * <p/>
     * Invoking {@link #suspend(WindowPane) suspend} prevents the queue from accepting new tasks,
     * until {@link #resume(WindowPane) resume} is invoked.
     * <p/>
     * No tasks will be accepted for queueing while the queue is stopped, unless {@code queueLastTaskOnStop == true}.
     * In this case, the most recent task will be kept when {@link TaskQueue#queue(Runnable)} is invoked, and added
     * to the queue when it restarts.<br/>
     * This can be used where the caller only needs one task to be queued at a time.
     *
     * @param interval  the interval in seconds between asynchronous callbacks from the client to check
     *                  for queued tasks for this queue, or {@code 0} to use the default
     * @param queueMode determines how tasks are queued while the queue is stopped
     * @return a new queue
     */
    public TaskQueue newQueue(int interval, QueueMode queueMode) {
        if (app == null) {
            throw new IllegalStateException("TaskQueues has been disposed");
        }
        RestartableTaskQueue queue = new RestartableTaskQueue(app, interval, true, queueMode) {

            @Override
            public void dispose() {
                super.dispose();
                tasks.remove(this);
            }
        };
        tasks.add(queue);
        return queue;
    }

    /**
     * Suspends background tasks while a window is displayed.
     *
     * @param window the window
     * @return if {@code true} a task was stopped
     */
    public boolean suspend(WindowPane window) {
        boolean suspended = false;
        for (RestartableTask task : tasks.toArray(new RestartableTask[0])) {
            if (task.isRunning()) {
                task.stop(); // PeriodicTask instances will be removed from tasks, suspendedTasks
                suspendedTasks.computeIfAbsent(task, k -> new ArrayList<>());
            }
        }
        // for each suspended task, add the WindowPane that must be closed before the task can be resumed
        for (List<WeakReference<WindowPane>> windows : suspendedTasks.values()) {
            windows.removeIf(reference -> reference.get() == null); // purged gc'ed entries
            windows.add(new WeakReference<>(window));
            suspended = true;
        }
        return suspended;
    }

    /**
     * Resumes tasks that were suspended when a window was displayed.
     * <p/>
     * A task can only be resumed if all of the windows that were opened after it started have been closed.
     */
    public void resume(WindowPane window) {
        for (Map.Entry<RestartableTask, List<WeakReference<WindowPane>>> entry : getSuspendedTasks()) {
            RestartableTask task = entry.getKey();
            List<WeakReference<WindowPane>> windows = entry.getValue();
            if (windows != null) {
                windows.removeIf(reference -> {
                    WindowPane other = reference.get();
                    return other == null || window == other;
                });
                if (windows.isEmpty()) {
                    // task can be resumed
                    suspendedTasks.remove(task);
                    task.start();
                }
            }
        }
    }

    /**
     * Disposes all tasks.
     */
    public void dispose() {
        for (RestartableTask task : tasks.toArray(new RestartableTask[0])) {
            task.dispose();
        }
        suspendedTasks.clear();
        app = null;
    }


    /**
     * Returns the suspended task entries as an array.
     *
     * @return the suspended task entries
     */
    @SuppressWarnings("unchecked")
    private Map.Entry<RestartableTask, List<WeakReference<WindowPane>>>[] getSuspendedTasks() {
        return suspendedTasks.entrySet().toArray(new Map.Entry[0]);
    }

    /**
     * Creates a new {@link PeriodicTask}.
     * <p/>
     * Tasks must be started in order to execute. Tasks started before a dialog is displayed will be stopped and resumed
     * after the dialog is closed.
     *
     * @param initialDelay the delay before the very first execution, in seconds
     * @param interval     the frequency to run the task, in seconds
     * @param task         the task to run
     * @param once         if {@code true}, only run the task once
     * @return a new task
     */
    private PeriodicTask newTask(int initialDelay, int interval, boolean once, Runnable task) {
        if (app == null) {
            throw new IllegalStateException("TaskQueues has been disposed");
        }
        return new ManagedPeriodicTask(initialDelay, interval, once, task);
    }

    /**
     * Extension to {@link PeriodicTask} to ensure that tasks are registered and deregistered on start and stop.
     */
    private class ManagedPeriodicTask extends PeriodicTask {

        public ManagedPeriodicTask(int initialDelay, int interval, boolean once, Runnable task) {
            super(app, initialDelay, interval, once, task);
        }

        @Override
        public boolean start() {
            boolean started = super.start();
            if (started) {
                tasks.add(this);
                suspendedTasks.remove(this);  // can no longer be suspended
            }
            return started;
        }

        @Override
        public boolean stop() {
            boolean stopped = super.stop();
            if (stopped) {
                tasks.remove(this);
                suspendedTasks.remove(this);
            }
            return stopped;
        }
    }
}