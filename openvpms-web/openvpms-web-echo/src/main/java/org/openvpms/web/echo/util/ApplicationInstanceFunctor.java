/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.TaskQueueHandle;
import nextapp.echo2.app.WindowPane;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openvpms.web.echo.spring.SpringApplicationInstance;

/**
 * Base class for functors that need to be queued via the {@link ApplicationInstance}
 * {@link ApplicationInstance#enqueueTask(TaskQueueHandle, Runnable) task queue}, in order to be able to
 * update the user interface.
 * <p/>
 * Note that instances created prior to a {@link WindowPane} being displayed will have their execution suspended until
 * the dialog is closed, to avoid unwanted UI synchronisation events being generated<br/>
 * While execution is suspended, the behaviour for handling calls to {@link #enqueueTask(Runnable)} is determined by
 * the {@link TaskQueues.QueueMode} specified at construction.
 *
 * @author Tim Anderson
 * @see TaskQueue
 * @see TaskQueues
 */
public abstract class ApplicationInstanceFunctor {

    /**
     * The task queue.
     */
    private final TaskQueue taskQueue;

    /**
     * The logger.
     */
    private static final Log log = LogFactory.getLog(ApplicationInstanceFunctor.class);

    /**
     * Constructs an {@link ApplicationInstanceFunctor}.
     *
     * @param interval  the interval in seconds between asynchronous callbacks from the client to check
     *                  for queued tasks produced by this, or {@code 0} to use the default
     * @param queueMode determines how tasks are queued while the queue is stopped
     */
    public ApplicationInstanceFunctor(int interval, TaskQueues.QueueMode queueMode) {
        SpringApplicationInstance app = (SpringApplicationInstance) ApplicationInstance.getActive();
        if (app == null) {
            throw new IllegalStateException("No current ApplicationInstance");
        }
        taskQueue = app.getTaskQueues().newQueue(interval, queueMode);
    }

    /**
     * Disposes of this instance.
     */
    public void dispose() {
        taskQueue.dispose();
    }

    /**
     * Enqueues a task.
     *
     * @param task the task to enqueue
     */
    protected void enqueueTask(Runnable task) {
        taskQueue.queue(() -> {
            try {
                task.run();
            } catch (Exception exception) {
                log.warn("ApplicationInstanceFunctor callback threw exception", exception);
            }
        });
    }
}
