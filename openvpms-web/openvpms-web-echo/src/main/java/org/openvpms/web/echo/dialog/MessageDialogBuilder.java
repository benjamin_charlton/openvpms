/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

import nextapp.echo2.app.Extent;
import nextapp.echo2.app.event.WindowPaneEvent;
import nextapp.echo2.app.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Builder for {@link MessageDialog}s.
 *
 * @author Tim Anderson
 */
public abstract class MessageDialogBuilder<T extends MessageDialog, B extends MessageDialogBuilder<T, B>> {

    /**
     * The buttons.
     */
    private final List<String> buttons = new ArrayList<>();

    /**
     * Listeners for individual buttons.
     */
    private final Map<String, Runnable> listeners = new HashMap<>();

    /**
     * Determines if shortcuts should be disabled for individual buttons.
     */
    private final Map<String, Boolean> disableShortcuts = new HashMap<>();

    /**
     * The default button.
     */
    private String defaultButton;

    /**
     * The dialog title.
     */
    private String title;

    /**
     * The preamble.
     */
    private String preamble;

    /**
     * The message.
     */
    private String message;

    /**
     * Determines if the message is an HTML fragment or plain text.
     */
    private boolean messageHTML;

    /**
     * The footer.
     */
    private String footer;

    /**
     * The dialog style name.
     */
    private String style;

    /**
     * The dialog size. This overrides that of the style.
     */
    private String size;

    /**
     * Determines if the dialog should be sized to the content.
     */
    private boolean sizeToContent;

    /**
     * The X offset.
     */
    private Extent x;

    /**
     * The Y offset.
     */
    private Extent y;

    /**
     * The help context.
     */
    private HelpContext help;

    /**
     * The default button listener.
     */
    private Consumer<String> defaultListener;

    /**
     * The window pane listener.
     */
    private WindowPaneListener listener;

    /**
     * Sets the dialog title.
     *
     * @param title the title
     * @return this
     */
    public B title(String title) {
        this.title = title;
        return getThis();
    }

    /**
     * Sets the dialog title from a resource bundle key.
     *
     * @param key  the resource bundle key
     * @param args arguments to format the title
     * @return this
     */
    public B titleKey(String key, Object... args) {
        return title(Messages.format(key, args));
    }

    /**
     * Returns the dialog title.
     *
     * @return the title. May be {@code null}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the preamble.
     *
     * @param preamble the preamble
     * @return this
     */
    public B preamble(String preamble) {
        this.preamble = preamble;
        return getThis();
    }

    /**
     * Sets the preamble from a resource bundle key.
     *
     * @param key  the preamble key
     * @param args arguments to format the preamble
     * @return this
     */
    public B preambleKey(String key, Object... args) {
        return preamble(Messages.format(key, args));
    }

    /**
     * Returns the preamble.
     *
     * @return the preamble. May be {@code null}
     */
    public String getPreamble() {
        return preamble;
    }

    /**
     * Sets the message.
     *
     * @param message the message
     * @return this
     */
    public B message(String message) {
        return message(message, false);
    }

    /**
     * Sets the message.
     *
     * @param message the message
     * @param html    if {@code true}, the message is an HTML fragment, otherwise it is plain text
     * @return this
     */
    public B message(String message, boolean html) {
        this.message = message;
        this.messageHTML = html;
        return getThis();
    }

    /**
     * Sets the dialog message from a resource bundle key.
     *
     * @param key  the resource bundle key
     * @param args arguments to format the message
     * @return this
     */
    public B messageKey(String key, Object... args) {
        return message(Messages.format(key, args));
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    public String getMessage() {
        return message;
    }

    /**
     * Determines if the message is an HTML fragment.
     *
     * @return {@code true} if the message is an HTML fragment, {@code false} if it is plain text
     */
    public boolean isMessageHTML() {
        return messageHTML;
    }

    /**
     * Sets the footer.
     *
     * @param footer the footer
     * @return this
     */
    public B footer(String footer) {
        this.footer = footer;
        return getThis();
    }

    /**
     * Sets the footer from a resource bundle key.
     *
     * @param key  the resource bundle key
     * @param args arguments to format the message
     * @return this
     */
    public B footerKey(String key, Object ... args) {
        return footer(Messages.format(key, args));
    }

    /**
     * Returns the footer.
     *
     * @return the footer. May be {@code null}
     */
    public String getFooter() {
        return footer;
    }

    /**
     * Sets the display of the OK buttons with it set as the default.
     *
     * @return this
     */
    public B ok() {
        buttons(MessageDialog.OK_ID);
        return defaultButton(MessageDialog.OK_ID);
    }

    /**
     * Sets the display of OK/CANCEL buttons, with CANCEL as the default.
     *
     * @return this
     */
    public B okCancel() {
        buttons(MessageDialog.OK_CANCEL);
        return defaultButton(MessageDialog.CANCEL_ID);
    }

    /**
     * Sets the display of the CANCEL button, with CANCEL as the default.
     *
     * @return this
     */
    public B cancel() {
        buttons(MessageDialog.CANCEL_ID);
        return defaultButton(MessageDialog.CANCEL_ID);
    }

    /**
     * Sets the display of YES/NO buttons, with NO as the default.
     *
     * @return this
     */
    public B yesNo() {
        buttons(MessageDialog.YES_NO);
        return defaultButton(MessageDialog.NO_ID);
    }

    /**
     * Sets the display of YES/NO/CANCEL buttons, with CANCEL as the default.
     *
     * @return this
     */
    public B yesNoCancel() {
        buttons(MessageDialog.YES_NO_CANCEL);
        return defaultButton(MessageDialog.CANCEL_ID);
    }

    /**
     * Display a YES button, bound to the specified listener.
     *
     * @param listener the listener
     * @return this
     */
    public B yes(Runnable listener) {
        return yes(false, listener);
    }

    /**
     * Display a YES button, bound to the specified listener.
     *
     * @param disableShortcut if {@code true} disable the shortcut
     * @param listener        the listener
     * @return this
     */
    public B yes(boolean disableShortcut, Runnable listener) {
        return button(ConfirmationDialog.YES_ID, disableShortcut, listener);
    }

    /**
     * Display a NO button, bound to the specified listener.
     *
     * @param listener the listener
     * @return this
     */
    public B no(Runnable listener) {
        return button(ConfirmationDialog.NO_ID, listener);
    }

    /**
     * Display an OK button, bound to the specified listener.
     *
     * @param listener the listener
     * @return this
     */
    public B ok(Runnable listener) {
        return button(ConfirmationDialog.OK_ID, listener);
    }

    /**
     * Display a CANCEL button, bound to the specified listener.
     *
     * @param listener the listener
     * @return this
     */
    public B cancel(Runnable listener) {
        return button(ConfirmationDialog.CANCEL_ID, listener);
    }

    /**
     * Adds a button with a listener, or associate a listener with an existing button.
     * <p/>
     * Note that if a button listener is registered, the {@link #getListener()} will be ignored.
     *
     * @param button   the button
     * @param listener the listener
     * @return this
     */
    public B button(String button, Runnable listener) {
        return button(button, false, listener);
    }

    /**
     * Adds a button with a listener, or associate a listener with an existing button.
     * <p/>
     * Note that if a button listener is registered, the {@link #getListener()} will be ignored.
     *
     * @param button          the button
     * @param disableShortcut if {@code true}, disable the shortcut for the button
     * @param listener        the listener
     * @return this
     */
    public B button(String button, boolean disableShortcut, Runnable listener) {
        if (!buttons.contains(button)) {
            buttons.add(button);
        }
        disableShortcuts.put(button, disableShortcut);
        listeners.put(button, listener);
        return getThis();
    }

    /**
     * Sets the buttons to display.
     *
     * @param buttons the button identifiers
     * @return this
     */
    public B buttons(String... buttons) {
        this.buttons.addAll(Arrays.asList(buttons));
        return getThis();
    }

    /**
     * Returns the button identifiers.
     *
     * @return the button identifiers
     */
    public List<String> getButtons() {
        return buttons;
    }

    /**
     * Sets the default button.
     *
     * @param defaultButton the default button. May be {@code null}
     * @return this
     */
    public B defaultButton(String defaultButton) {
        this.defaultButton = defaultButton;
        return getThis();
    }

    /**
     * Returns the default button.
     *
     * @return the default button. May be {@code null}
     */
    public String getDefaultButton() {
        return defaultButton;
    }

    /**
     * Sets the dialog style.
     *
     * @param style the dialog style
     * @return this
     */
    public B style(String style) {
        this.style = style;
        return getThis();
    }

    /**
     * Returns the dialog style.
     *
     * @return the dialog style. May be {@code null}
     */
    public String getStyle() {
        return style;
    }

    /**
     * Sets the dialog size.
     *
     * @param size the dialog size property
     * @return this
     * @see PopupDialog#resize()
     */
    public B size(String size) {
        this.size = size;
        return getThis();
    }

    /**
     * Returns the dialog size.
     *
     * @return the dialog size. May be {@code null}
     */
    public String getSize() {
        return size;
    }

    /**
     * Indicates to size the dialog to the content.
     * <p/>
     * NOTE: at present this is limited to dialogs that display plain text.
     *
     * @return this
     */
    public B sizeToContent() {
        sizeToContent = true;
        return getThis();
    }

    /**
     * Determines if the dialog should size to fit the content.
     *
     * @return {@code true} if the dialog should size to fit the content
     */
    public boolean getSizeToContent() {
        return sizeToContent;
    }

    /**
     * Sets the dialog position.
     *
     * @param x the X offset, in pixels
     * @param y the Y offset in pixels
     * @return this
     */
    public B position(int x, int y) {
        return position(new Extent(x), new Extent(y));
    }

    /**
     * Sets the dialog position.
     *
     * @param x the X offset, in pixels
     * @param y the Y offset in pixels
     * @return this
     */
    public B position(Extent x, Extent y) {
        this.x = x;
        this.y = y;
        return getThis();
    }

    /**
     * Returns the X offset of the dialog.
     *
     * @return the X offset. May be {@code null}
     */
    public Extent getX() {
        return x;
    }

    /**
     * Returns the Y offset of the dialog.
     *
     * @return the Y offset. May be {@code null}
     */
    public Extent getY() {
        return y;
    }

    /**
     * Sets the help context.
     *
     * @param help the help context
     * @return this
     */
    public B help(HelpContext help) {
        this.help = help;
        return getThis();
    }

    /**
     * Returns the help context.
     *
     * @return the help context. May be {@code null}
     */
    public HelpContext getHelp() {
        return help;
    }

    /**
     * Sets the dialog listener.
     *
     * @param listener the listener
     * @return this
     */
    public B listener(WindowPaneListener listener) {
        this.listener = listener;
        return getThis();
    }

    /**
     * Sets the dialog listener.
     *
     * @param listener the listener
     * @return this
     */
    public B listener(Runnable listener) {
        this.listener = new org.openvpms.web.echo.event.WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                listener.run();
            }
        };
        return getThis();
    }

    /**
     * Returns the dialog listener.
     *
     * @return the dialog listener. May be {@code null}
     */
    public WindowPaneListener getListener() {
        return listener;
    }

    /**
     * Register a listener to handle any button for which no other listener has been registered.
     * <p/>
     * This accepts the button identifier as an argument.
     * <p/>
     * Note that if a default listener is registered, the {@link #getListener()} will be ignored.
     *
     * @param defaultListener the default listener
     * @return this
     */
    public B defaultListener(Consumer<String> defaultListener) {
        this.defaultListener = defaultListener;
        return getThis();
    }

    /**
     * Returns the default button listener.
     *
     * @return the default button listener. May be {@code null}
     */
    public Consumer<String> getDefaultListener() {
        return defaultListener;
    }

    /**
     * Returns the per-button listeners.
     *
     * @return the per-button listeners
     */
    public Map<String, Runnable> getListeners() {
        return listeners;
    }

    /**
     * Builds the dialog.
     *
     * @return the dialog
     */
    public abstract T build();

    /**
     * Builds and displays the dialog.
     *
     * @return the dialog
     */
    public T show() {
        T dialog = build();
        dialog.show();
        return dialog;
    }

    /**
     * Builds and displays the dialog, automatically closing it after a period of time.
     *
     * @param time the time to show the window for, in seconds
     * @return the dialog
     */
    public T show(int time) {
        T dialog = build();
        dialog.show(time);
        return dialog;
    }

    /**
     * Determines if the shortcut should be disabled for a button.
     *
     * @param button the button
     * @return {@code true} if the shortcut should be disabled, otherwise {@code false}
     */
    public boolean disableShortcut(String button) {
        Boolean disable = disableShortcuts.get(button);
        return disable != null && disable;
    }

    /**
     * Returns this.
     *
     * @return this
     */
    protected abstract B getThis();
}
