/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.ContentPane;
import nextapp.echo2.app.Window;
import nextapp.echo2.app.WindowPane;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.spring.SpringApplicationInstance;
import org.openvpms.web.echo.util.TaskQueues;


/**
 * Dialog manager.
 *
 * @author Tim Anderson
 */
public class DialogManager {

    /**
     * Shows a dialog.
     * <p/>
     * The dialog's focus group will be reindexed if it overlaps existing dialogs.
     *
     * @param dialog the dialog to show
     */
    public static void show(PopupWindow dialog) {
        show(dialog, false);
    }

    /**
     * Shows a dialog.
     * <p/>
     * The dialog's focus group will be reindexed if it overlaps existing dialogs.
     *
     * @param dialog     the dialog to show
     * @param forceToTop if {@code true}, ensures the dialog is displayed above other dialogs. If {@code false},
     *                   {@link ErrorDialog}s have priority
     */
    public static void show(PopupWindow dialog, boolean forceToTop) {
        // hack to workaround tabindexes being set on components on the root
        // window. Ideally, would have major components implement an interface
        // that returns a FocusGroup, to accurately determine tabindexes

        ApplicationInstance instance = ApplicationInstance.getActive();
        ContentPane root = getDefaultWindowContentPane(instance);
        if (root != null) {
            int lastIndex = 1000;
            int zIndex = 0;
            ErrorDialog error = null;
            for (Component component : root.getComponents()) {
                if (component instanceof WindowPane) {
                    WindowPane pane = (WindowPane) component;
                    if (pane.getZIndex() > zIndex) {
                        zIndex = pane.getZIndex();
                    }
                    if (pane instanceof PopupWindow) {
                        FocusGroup group = ((PopupWindow) pane).getFocusGroup();
                        if (group.getLast() > lastIndex) {
                            lastIndex = group.getLast();
                        }
                    }
                    if (pane instanceof ErrorDialog) {
                        if (error == null || error.getZIndex() < pane.getZIndex()) {
                            error = (ErrorDialog) pane;
                        }
                    }
                }
            }
            dialog.setZIndex(zIndex + 1);
            FocusGroup group = dialog.getFocusGroup();
            if (group.getFirst() <= lastIndex) {
                group.reindex(lastIndex + 1000);
                // give the parent dialog room to grow.
            }

            if (instance instanceof SpringApplicationInstance) {
                // suspend background tasks so they don't interfere with edits. See OVPMS-2459
                SpringApplicationInstance springApplicationInstance = (SpringApplicationInstance) instance;
                TaskQueues tasks = springApplicationInstance.getTaskQueues();
                if (tasks.suspend(dialog)) {
                    dialog.addWindowPaneListener(new WindowPaneListener() {
                        @Override
                        public void onClose(WindowPaneEvent event) {
                            tasks.resume(dialog);
                        }
                    });
                }
            }

            root.add(dialog);

            if (!forceToTop && error != null && !(dialog instanceof ErrorDialog)) {
                // always display error dialogs above other dialogs.
                moveErrorToTop(error, dialog);
            }
        }
    }


    /**
     * Determines if a {@code WindowPane} is being displayed.
     *
     * @return {@code true} if a window is being displayed, otherwise {@code false}
     */
    public static boolean isWindowDisplayed() {
        ContentPane root = getDefaultWindowContentPane();
        if (root != null) {
            for (Component component : root.getComponents()) {
                if (component instanceof WindowPane) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determines if a component is hidden behind modal dialogs.
     * <p>
     * The component must be a child of a {@link WindowPane}.
     *
     * @param component the component
     * @return {@code true} if the component is hidden, and therefore cannot be selected
     */
    public static boolean isHidden(Component component) {
        WindowPane parent = getWindowPane(component);
        if (parent != null) {
            ContentPane root = getDefaultWindowContentPane();
            if (root != null) {
                for (Component c : root.getComponents()) {
                    if (c instanceof WindowPane) {
                        WindowPane pane = (WindowPane) c;
                        if (pane.isModal() && pane.getZIndex() > parent.getZIndex()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns the {@code WindowPane} that a component belongs to.
     *
     * @param component the component
     * @return the {@code WindowPane} or {@code null} if none is found
     */
    protected static WindowPane getWindowPane(Component component) {
        while (component != null && !(component instanceof WindowPane)) {
            component = component.getParent();
        }
        return (WindowPane) component;
    }

    /**
     * Returns the {@code ContentPane} of the default window.
     *
     * @return the {@code ContentPane}, or {@code null} if there is none active
     */
    protected static ContentPane getDefaultWindowContentPane() {
        return getDefaultWindowContentPane(ApplicationInstance.getActive());
    }

    /**
     * Moves an error dialog to the top.
     *
     * @param error  the error dialog
     * @param dialog the current top dialog
     */
    private static void moveErrorToTop(ErrorDialog error, PopupWindow dialog) {
        error.setZIndex(dialog.getZIndex() + 1);

        // Need to force Echo to re-evaluate its set of modal dialogs by temporarily making it non-modal
        error.setModal(false);
        error.setModal(true);
    }

    /**
     * Returns the {@code ContentPane} of the default window.
     *
     * @param active the application instance. May be {@code null}
     * @return the {@code ContentPane}, or {@code null} if there is none active
     */
    private static ContentPane getDefaultWindowContentPane(ApplicationInstance active) {
        ContentPane result = null;
        if (active != null) {
            Window root = active.getDefaultWindow();
            if (root != null) {
                result = root.getContent();
            }
        }
        return result;
    }

}
