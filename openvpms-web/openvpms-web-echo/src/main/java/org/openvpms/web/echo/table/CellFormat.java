/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.table;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.resource.i18n.format.NumberFormatter;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Date;
import java.util.function.Function;

import static org.openvpms.web.resource.i18n.format.NumberFormatter.DECIMAL_VIEW;
import static org.openvpms.web.resource.i18n.format.NumberFormatter.ID_FORMAT;
import static org.openvpms.web.resource.i18n.format.NumberFormatter.INTEGER_VIEW;

/**
 * Functions to format table cells.
 * <p/>
 * The apply() method may be passed any object, including a {@code Component} and {@code null}.
 * It may return a {@code Component} and {@code null}. Any other object will be converted to string.
 *
 * @author Tim Anderson
 */
public class CellFormat {

    /**
     * Returns a function to format a number as an identifier.
     *
     * @return a new function
     */
    public static Function<Object, Object> id() {
        return new NumberFunction(NumberFormatter.getFormat(ID_FORMAT));
    }

    /**
     * Returns a function to format a currency amount.
     *
     * @return a new function
     */
    public static Function<Object, Object> currency() {
        return new NumberFunction(NumberFormatter.getCurrencyFormat());
    }

    /**
     * Returns a function to format an integer.
     *
     * @return a new function
     */
    public static Function<Object, Object> integer() {
        return number(NumberFormatter.getFormat(INTEGER_VIEW));
    }

    /**
     * Returns a function to format a decimal.
     *
     * @return a new function
     */
    public static Function<Object, Object> decimal() {
        return number(NumberFormatter.getFormat(DECIMAL_VIEW));
    }

    /**
     * Returns a function to format a number.
     *
     * @return a new function
     */
    public static Function<Object, Object> number() {
        return new DecimalIntegerFunction();
    }

    /**
     * Returns a function to format a number.
     *
     * @param format the number format
     * @return a new function
     */
    public static Function<Object, Object> number(NumberFormat format) {
        return new NumberFunction(format);
    }

    /**
     * Returns a function to format a number as a percentage.
     *
     * @return a new function
     */
    public static Function<Object, Object> percent() {
        return new PercentFunction();
    }

    /**
     * Returns a function to format a date.
     *
     * @return a new function
     */
    public static Function<Object, Object> date() {
        return new DateFunction(DateFormatter.getDateFormat(false), false, false);
    }

    /**
     * Returns a function to format a date/time.
     *
     * @return a new function
     */
    public static Function<Object, Object> dateTime() {
        return dateTime(false);
    }

    /**
     * Returns a function to format a date/time.
     *
     * @param excludeDateIfToday if {@code true}, the date will be suppressed if it is today
     * @return a new function
     */
    public static Function<Object, Object> dateTime(boolean excludeDateIfToday) {
        return new DateFunction(DateFormatter.getDateTimeFormat(false), true, excludeDateIfToday);
    }

    /**
     * Returns a function to format an object.
     *
     * @param format the format
     * @return a new function
     */
    public static Function<Object, Object> format(Format format) {
        return new FormatFunction(format);
    }

    /**
     * Returns a function to format an object as a single line of text.
     *
     * @return the format function
     */
    public static Function<Object, Object> value() {
        return text(false, false);
    }

    /**
     * Returns a function to format an object as multiline text.
     * <p/>
     * The text can wrap.
     *
     * @return the format function
     */
    public static Function<Object, Object> text() {
        return text(true, true);
    }

    /**
     * Returns a function to format an object as text.
     *
     * @param multiline if {@code true}, interpret new lines
     * @param wrap      if {@code true} wrap long lines
     * @return the format function
     */
    public static Function<Object, Object> text(boolean multiline, boolean wrap) {
        return new TextRenderer(multiline, wrap);
    }

    /**
     * Formats objects using a {@link Format}.
     */
    private static class FormatFunction implements Function<Object, Object> {

        /**
         * The format.
         */
        private final Format format;

        /**
         * Constructs a {@link FormatFunction}.
         *
         * @param format the format
         */
        public FormatFunction(Format format) {
            this.format = format;
        }

        /**
         * Applies this function to the given argument.
         *
         * @param object the function argument. May be {@code null}
         * @return the function result. May be {@code null}
         */
        @Override
        public Object apply(Object object) {
            return (object instanceof Component) ? object : format(object);
        }

        /**
         * Formats an object as a string.
         *
         * @param object the object. May be {@code null}
         * @return the formatted object. May be {@code null}
         */
        protected String format(Object object) {
            return format(object, format);
        }

        /**
         * Formats an object as a string.
         *
         * @param object the object. May be {@code null}
         * @param format the format to use
         * @return the formatted object. May be {@code null}
         */
        protected String format(Object object, Format format) {
            String result = null;
            if (object != null) {
                try {
                    result = format.format(object);
                } catch (IllegalArgumentException exception) {
                    result = object.toString();
                }
            }
            return result;
        }

        /**
         * Converts an object to string.
         *
         * @param object the object. May be {@code null}
         * @return the object as a string. May be {@code null}
         */
        protected String toString(Object object) {
            return object != null ? object.toString() : null;
        }
    }

    /**
     * Formats objects using a {@link NumberFormat}.
     * <p/>
     * These will be right-aligned.
     */
    private static class NumberFunction extends FormatFunction {

        /**
         * Constructs a {@link NumberFunction}.
         *
         * @param format the format
         */
        public NumberFunction(NumberFormat format) {
            super(format);
        }

        /**
         * Applies this function to the given argument.
         *
         * @param object the function argument. May be {@code null}
         * @return the function result. May be {@code null}
         */
        @Override
        public Object apply(Object object) {
            Object result;
            if (object instanceof Number) {
                result = TableHelper.rightAlign(format(object));
            } else if (object instanceof Component) {
                result = object;
            } else {
                result = toString(object);
            }
            return result;
        }
    }

    /**
     * A {@link NumberFunction} that uses a different format decimal and integer values.
     */
    private static class DecimalIntegerFunction extends NumberFunction {

        /**
         * The integer format.
         */
        private final NumberFormat integer;

        /**
         * Default constructor.
         */
        public DecimalIntegerFunction() {
            this(NumberFormatter.getFormat(DECIMAL_VIEW), NumberFormatter.getFormat(INTEGER_VIEW));
        }

        /**
         * Constructs a {@link DecimalIntegerFunction}.
         *
         * @param decimal the decimal format
         * @param integer the integer format
         */
        public DecimalIntegerFunction(NumberFormat decimal, NumberFormat integer) {
            super(decimal);
            this.integer = integer;
        }

        /**
         * Applies this function to the given argument.
         *
         * @param object the function argument. May be {@code null}
         * @return the function result. May be {@code null}
         */
        @Override
        public Object apply(Object object) {
            Object result;
            if (object instanceof Long || object instanceof Integer || object instanceof Short
                || object instanceof Byte || object instanceof BigInteger) {
                result= TableHelper.rightAlign(format(object, integer));
            } else {
                result = super.apply(object);
            }
            return result;
        }
    }

    /**
     * Formats an object as a percent.
     */
    private static class PercentFunction extends DecimalIntegerFunction {

        /**
         * Formats an object as a string.
         *
         * @param object the object. May be {@code null}
         * @param format the format to use
         * @return the formatted object. May be {@code null}
         */
        @Override
        protected String format(Object object, Format format) {
            String result = super.format(object, format);
            return result != null ? result + "%" : null;
        }
    }


    /**
     * Function to format dates.
     */
    private static class DateFunction extends FormatFunction {

        /**
         * The time format.
         */
        private final DateFormat timeFormat;

        /**
         * Constructs a {@link DateFunction}.
         *
         * @param format             the date format
         * @param dateTime           if {@code true}, display date/times
         * @param excludeDateIfToday if {@code true}, exclude the date portion in date/times, if it is today
         */
        public DateFunction(DateFormat format, boolean dateTime, boolean excludeDateIfToday) {
            super(format);
            timeFormat = dateTime && excludeDateIfToday ? DateFormatter.getTimeFormat(false) : null;
        }

        /**
         * Applies this function to the given argument.
         *
         * @param object the function argument. May be {@code null}
         * @return the function result. May be {@code null}
         */
        @Override
        public Object apply(Object object) {
            Object result;
            if (object instanceof Date) {
                Date date = (Date) object;
                if (timeFormat != null && DateRules.isToday(date)) {
                    result = format(object, timeFormat);
                } else {
                    result = format(date);
                }
            } else if (object instanceof Component) {
                result = object;
            } else {
                result = toString(object);
            }
            return result;
        }
    }

    /**
     * Function to render objects as text.
     */
    private static class TextRenderer implements Function<Object, Object> {

        /**
         * Determines if new lines are interpreted in the text.
         */
        private final boolean multiline;

        /**
         * Determines if long lines will be wrapped.
         */
        private final boolean wrap;

        /**
         * @param multiline if {@code true}, interprets new lines in the text
         * @param wrap      if {@code true}, long lines will be wrapped
         */
        public TextRenderer(boolean multiline, boolean wrap) {
            this.multiline = multiline;
            this.wrap = wrap;
        }

        /**
         * Applies this function to the given argument.
         *
         * @param object the function argument. May be {@code null}
         * @return the function result. May be {@code null}
         */
        @Override
        public Object apply(Object object) {
            Object result = null;
            if (object instanceof Component) {
                result = object;
            } else if (object != null) {
                Label label = LabelFactory.create(multiline, wrap);
                label.setText(object.toString());
                result = label;
            }
            return result;
        }
    }
}