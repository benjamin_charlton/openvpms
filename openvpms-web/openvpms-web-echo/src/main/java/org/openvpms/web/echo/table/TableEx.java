/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.table;

import nextapp.echo2.app.table.TableColumnModel;
import nextapp.echo2.app.table.TableModel;

/**
 * Extension to the EPNG TableEx to support fixed headers and scolling a row into view.
 *
 * @author Tim Anderson
 */
public class TableEx extends echopointng.TableEx {

    /**
     * Property to determine if the header is fixed.
     */
    private static final String PROPERTY_HEADER_FIXED = "headerFixed";

    /**
     * The row to scroll to.
     */
    private Integer scrollToRow;

    /**
     * Constructs a {@link TableEx}.
     */
    public TableEx() {
        super();
    }

    /**
     * Constructs a {@link TableEx}.
     *
     * @param model       the table model
     * @param columnModel the column model
     */
    public TableEx(TableModel model, TableColumnModel columnModel) {
        super(model, columnModel);
    }

    /**
     * Determines if headers are fixed.
     *
     * @param fixed if {@code true}, headers are fixed, else they scroll with the content
     */
    public void setHeaderFixed(boolean fixed) {
        setProperty(PROPERTY_HEADER_FIXED, fixed);
        this.invalidate();
    }

    /**
     * Determines if headers are fixed.
     *
     * @return {@code true} if headers are fixed, {@code false} if they scroll with the content
     */
    public boolean isHeaderFixed() {
        Object property = getProperty(PROPERTY_HEADER_FIXED);
        return (property instanceof Boolean) && (Boolean) property;
    }

    /**
     * Scrolls a row into view.
     *
     * @param row the row, or {@code -1} to reset the scroll position
     */
    public void setScrollToRow(int row) {
        this.scrollToRow = row;
        invalidate();   // be nice to do a partial update, but couldn't get it to work. TODO
    }

    /**
     * Returns the row to scroll into view.
     *
     * @return {@code null}, if no scrolling should take place, the row, or {@code -1} to reset the scroll position
     */
    public Integer getScrollToRow() {
        return scrollToRow;
    }

    /**
     * Reset the scroll. This is invoked by {@link TableExPeer} as the scroll position should
     * only have an affect when the table is rendered.
     */
    void resetScrollToRow() {
        scrollToRow = null;
    }
}
