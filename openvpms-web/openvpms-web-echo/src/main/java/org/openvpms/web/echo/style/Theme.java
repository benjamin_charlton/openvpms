/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.style;

import echopointng.util.ColorKit;
import nextapp.echo2.app.Color;
import org.openvpms.web.echo.colour.ColourHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * Defines properties for in a stylesheet.
 * <p/>
 * The following properties are recognised:
 * <ul>
 *     <li><strong>primary</strong> - the primary colour. This is an alias for <em>theme.background</em></li>
 *     <li><strong>secondary</strong> - the secondary colour. This is an alias for <em>theme.button.background</em></li>
 *     <li><strong>theme.background</strong> - the primary background colour.</li>
 *     <li><strong>theme.foreground</strong> - the primary foreground colour. If not present, derived from
 *     <em>theme.background</em></li>
 *     <li><strong>theme.button.background</strong> - the secondary colour, used for buttons. If not present, set to
 *     <em>theme.background</em></li>
 *     <li><strong>theme.button.foreground</strong> - the secondary foreground colour, used for buttons. If not present,
 *     derived from <em>theme.button.background</em></li>
 *     <li><strong>theme.button.rolloverBackground</strong> - the rollover button background colour.
 *     If not present, a darker version of <em>theme.button.background</em> is used</li>
 *     <li><strong>theme.button.rolloverForeground</strong> - the rollover button foreground colour.
 *     If not present, a colour is derived from <em>theme.button.rolloverBackground</em></li>
 *     <li><strong>theme.button.rolloverBorder</strong> - the rollover button border colour.
 *     If not present, a darker version of <em>theme.button.rolloverBackground</em> is used</li>
 *     <li><strong>theme.button.pressedBackground</strong> - the pressed button background colour.
 *     If not present, <em>theme.button.rolloverBackground</em> is used</li>
 *     <li><strong>theme.button.pressedForeground</strong> - the pressed button foreground colour.
 *     If not present, a colour derived from <em>theme.button.pressedBackground</em> is used</li>
 *     <li><strong>theme.button.pressedBorder</strong> - the pressed button border colour.
 *     If not present, <em>theme.button.rolloverBackground</em> is used</li>
 *     <li><strong>theme.button.pressedBorder</strong> - the pressed button border colour.
 *     If not present, <em>theme.button.rolloverBackground</em> is used</li>
 *     <li><strong>theme.button.disabledBackground</strong> - the disabled button background colour.
 *     If not present, a lighter version of <em>theme.button.background</em> is used</li>
 *     <li><strong>theme.button.disabledForeground</strong> - the disabled button foreground colour.
 *     If not present, a gray colour is derived from <em>theme.button.disabledBackground</em></li>
 *     <li><strong>theme.button.disabledBorder</strong> - the disabled button border colour.
 *     If not present, <em>theme.button.disabledBackground</em> is used</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class Theme {

    /**
     * The theme identifier.
     */
    private final String id;

    /**
     * The theme name.
     */
    private final String name;

    /**
     * The theme properties.
     */
    private final Map<String, String> properties;

    /**
     * The primary colour.
     */
    private static final String PRIMARY = "primary";

    /**
     * The secondary colour.
     */
    private static final String SECONDARY = "secondary";

    /**
     * The primary background.
     */
    private static final String PRIMARY_BACKGROUND = "theme.background";

    /**
     * The primary foreground.
     */
    private static final String PRIMARY_FOREGROUND = "theme.foreground";

    /**
     * The button background colour.
     */
    private static final String SECONDARY_BACKGROUND = "theme.button.background";

    /**
     * The button foreground colour.
     */
    private static final String SECONDARY_FOREGROUND = "theme.button.foreground";

    /**
     * The rollover button background colour.
     */
    private static final String ROLLOVER_BACKGROUND = "theme.button.rolloverBackground";

    /**
     * The rollover button foreground colour.
     */
    private static final String ROLLOVER_FOREGROUND = "theme.button.rolloverForeground";

    /**
     * The rollover button border colour.
     */
    private static final String ROLLOVER_BORDER = "theme.button.rolloverBorder";

    /**
     * The disabled button background colour.
     */
    private static final String DISABLED_BACKGROUND = "theme.button.disabledBackground";

    /**
     * The disabled button foreground colour.
     */
    private static final String DISABLED_FOREGROUND = "theme.button.disabledForeground";

    /**
     * The disabled button border colour.
     */
    private static final String DISABLED_BORDER = "theme.button.disabledBorder";

    /**
     * The pressed button background colour.
     */
    private static final String PRESSED_BACKGROUND = "theme.button.pressedBackground";

    /**
     * The pressed button border colour.
     */
    private static final String PRESSED_BORDER = "theme.button.pressedBorder";

    /**
     * Constructs a {@link Theme}.
     *
     * @param id         the theme identifier
     * @param name       the theme name
     * @param properties the theme properties
     */
    public Theme(String id, String name, Map<String, String> properties) {
        this.id = id;
        this.name = name;
        this.properties = new HashMap<>(properties);
        rename(PRIMARY, PRIMARY_BACKGROUND);
        rename(SECONDARY, SECONDARY_BACKGROUND);
        Color primaryBackground = getColor(PRIMARY_BACKGROUND);
        if (primaryBackground != null) {
            addForeground(primaryBackground, PRIMARY_FOREGROUND);
            add(SECONDARY_BACKGROUND, ColorKit.brighter(primaryBackground, 0.75));
        }
        Color secondaryBackground = getColor(SECONDARY_BACKGROUND);
        if (secondaryBackground != null) {
            addForeground(secondaryBackground, SECONDARY_FOREGROUND);
            Color rolloverBackground = darken(secondaryBackground, ROLLOVER_BACKGROUND);
            Color rolloverBorder = ColorKit.darker(rolloverBackground);
            add(ROLLOVER_BORDER, rolloverBorder);
            add(PRESSED_BORDER, rolloverBorder);
            add(PRESSED_BACKGROUND, rolloverBackground);
            Color disabled = add(DISABLED_BACKGROUND, ColorKit.brighter(secondaryBackground, 0.90));
            if (!properties.containsKey(DISABLED_FOREGROUND)) {
                Color textColour = ColourHelper.getTextColour(disabled);
                if (textColour == Color.BLACK) {
                    textColour = ColorKit.brighter(textColour, 0.99);
                } else {
                    textColour = ColorKit.darker(textColour, 0.99);
                }
                add(DISABLED_FOREGROUND, textColour);
            }
            add(DISABLED_BORDER, disabled);
        }
        addForeground(PRESSED_BACKGROUND, ROLLOVER_FOREGROUND);
        addForeground(ROLLOVER_BACKGROUND, ROLLOVER_FOREGROUND);
        addForeground(DISABLED_BACKGROUND, DISABLED_FOREGROUND);
    }

    /**
     * Returns the theme identifier.
     *
     * @return the theme identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the theme name.
     *
     * @return the theme name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the theme properties.
     *
     * @return the theme properties
     */
    public Map<String, String> getProperties() {
        return properties;
    }

    /**
     * Returns the properties as JXPath expressions.
     * <p/>
     * This single quotes any colours.
     *
     * @return the properties as JXPath expressions
     */
    public Map<String, String> getExpressions() {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            String value = entry.getValue();
            if (value.startsWith("#")) {
                value = "'" + value + "'";
            }
            result.put(entry.getKey(), value);
        }
        return result;
    }

    /**
     * Returns the primary theme colour.
     *
     * @return the colour, or {@code null} if none is defined
     */
    public Color getColor() {
        return getColor(PRIMARY_BACKGROUND);
    }

    /**
     * Assigns a property a darker version of the specified colour, if no value already exists.
     *
     * @param colour   the colour
     * @param property the property
     * @return the property colour, if one exists, else the darker colour used for the property
     */
    private Color darken(Color colour, String property) {
        Color result = getColor(property);
        if (result == null) {
            result = ColorKit.darker(colour, 0.90);
            properties.put(property, ColorKit.makeCSSColor(result));
        }
        return result;
    }


    /**
     * Renames a property.
     * <p/>
     * If the target already exists, the source is removed.
     *
     * @param source the name to rename from
     * @param target the name to rename to
     */
    private void rename(String source, String target) {
        if (properties.containsKey(source)) {
            if (!properties.containsKey(target)) {
                properties.put(target, properties.get(source));
            }
            properties.remove(source);
        }
    }

    /**
     * Adds a colour for a property, if none already exists.
     *
     * @param property the property
     * @param colour   the colour
     * @return the property's colour
     */
    private Color add(String property, Color colour) {
        Color result = getColor(property);
        if (result == null) {
            properties.put(property, ColorKit.makeCSSColor(colour));
            result = colour;
        }
        return result;
    }

    /**
     * Derives a foreground colour from a background colour, if none already exists.
     *
     * @param background the background colour property
     * @param foreground the foreground colour property
     */
    private void addForeground(String background, String foreground) {
        Color colour = getColor(foreground);
        if (colour == null) {
            colour = getColor(background);
            if (colour != null) {
                addForeground(colour, foreground);
            }
        }
    }

    /**
     * Adds a foreground colour, if none already exists.
     *
     * @param colour     the background colour
     * @param foreground the foreground property
     */
    private void addForeground(Color colour, String foreground) {
        if (getColor(foreground) == null) {
            properties.put(foreground, ColorKit.makeCSSColor(ColourHelper.getTextColour(colour)));
        }
    }

    /**
     * Returns the colour for a given property.
     *
     * @param property the property name
     * @return the corresponding colour. May be {@code null}
     */
    private Color getColor(String property) {
        String code = properties.get(property);
        Color result = null;
        if (code != null) {
            try {
                result = ColorKit.makeColor(code);
            } catch (IllegalArgumentException exception) {
                // no-op
            }
        }
        return result;
    }
}
