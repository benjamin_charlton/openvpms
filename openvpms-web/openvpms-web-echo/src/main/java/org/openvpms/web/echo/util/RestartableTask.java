/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

/**
 * Restartable task.
 *
 * @author Tim Anderson
 */
public interface RestartableTask {

    /**
     * Starts the task.
     * <p>
     *
     * @return {@code true} if the task was started, {@code false} if it was already running
     */
    boolean start();

    /**
     * Stops the task.
     * <p/>
     *
     * @return {@code true} if the task was stopped, {@code false} if it was already stopped
     */
    boolean stop();

    /**
     * Determines if the task is running.
     *
     * @return {@code true} if the task is running
     */
    boolean isRunning();

    /**
     * Disposes of the task.
     */
    void dispose();

}