/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

import org.openvpms.web.resource.i18n.Messages;

/**
 * Builder for {@link ErrorDialog}s.
 *
 * @author Tim Anderson
 */
public class ErrorDialogBuilder extends MessageDialogBuilder<ErrorDialog, ErrorDialogBuilder> {

    /**
     * Constructs an {@link ErrorDialogBuilder}.
     */
    protected ErrorDialogBuilder() {
        style(ErrorDialog.STYLE);
    }

    /**
     * Returns the dialog title.
     *
     * @return the title. May be {@code null}
     */
    @Override
    public String getTitle() {
        String title = super.getTitle();
        return (title != null) ? title : Messages.get("errordialog.title");
    }

    /**
     * Builds the dialog.
     *
     * @return the dialog
     */
    @Override
    public ErrorDialog build() {
        return new ErrorDialog(this);
    }

    /**
     * Returns this.
     *
     * @return this
     */
    @Override
    protected ErrorDialogBuilder getThis() {
        return this;
    }

}
