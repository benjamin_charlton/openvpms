/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.factory;

import echopointng.xhtml.XhtmlFragment;
import nextapp.echo2.app.Label;
import nextapp.echo2.webrender.output.HtmlDocument;
import nextapp.echo2.webrender.output.XmlDocument;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link LabelFactory} class.
 *
 * @author Tim Anderson
 */
public class LabelFactoryTestCase {

    /**
     * Verifies that unsupported characters are replaced by spaces.
     */
    @Test
    public void testBadCharactersReplacedWithSpace() {
        StringBuilder builder = new StringBuilder();
        for (char i = 0; i < 9; ++i) {
            builder.append(i);
        }
        builder.append('\u000B' );
        builder.append('\u000C' );
        for (char i = '\u000E'; i <= '\u001F'; ++i) {
            builder.append(i);
        }
        builder.append('\u007F' );

        Label label = LabelFactory.create();
        String text = builder.toString();
        assertEquals(30, text.length());
        label.setText(text);
        String actual = label.getText();
        assertEquals(StringUtils.repeat(' ', 30), actual);
    }

    /**
     * Tests the {@link LabelFactory#preformatted(String)} method.
     */
    @Test
    public void testPreformatted() {
        Label label1 = LabelFactory.preformatted("abc\ncde");
        assertEquals("<div xmlns='http://www.w3.org/1999/xhtml' style='white-space:pre'>abc\ncde</div>",
                     label1.getText());

        // test nulls
        Label label2 = LabelFactory.preformatted(null);
        assertNull(label2.getText());
    }

    /**
     * Verifies that the {@link LabelFactory#preformatted(String)} method supports embedded XML characters.
     *
     * @throws Exception for any error
     */
    @Test
    public void testPreformattedWithEmbeddedXML() throws Exception {
        String text = "<>&'\"\n’"; // include right quote to test the fix for OVPMS-2326
        Label label = LabelFactory.preformatted(text);
        // in order to support multi-line, the text is escaped and wrapped in a div
        assertEquals("<div xmlns='http://www.w3.org/1999/xhtml' style='white-space:pre'>&lt;&gt;&amp;&apos;&quot;\n" +
                     "’</div>", label.getText());
        XhtmlFragment fragment = (XhtmlFragment) label.getProperty(Label.PROPERTY_TEXT);
        assertNotNull(fragment);

        XmlDocument xmlDocument = new XmlDocument("data", null, null, HtmlDocument.XHTML_1_0_NAMESPACE_URI);
        Document doc = xmlDocument.getDocument();
        Node[] nodes = fragment.toDOM(doc);
        assertEquals(1, nodes.length);
        assertEquals(text, nodes[0].getChildNodes().item(0).getNodeValue());
    }

}
