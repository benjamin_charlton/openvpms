<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>openvpms-web</artifactId>
        <groupId>org.openvpms</groupId>
        <version>2.3.1</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>openvpms-web-echo</artifactId>

    <properties>
        <echo.version>2.1.1</echo.version>
        <echo-extras.version>2.1.0</echo-extras.version>
        <echo-filetransfer.version>2.1.0</echo-filetransfer.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-plugin-manager</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-web-resource</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-reports</artifactId>
        </dependency>
        <dependency>
            <!-- NOTE: this dependency only required by HelpContext TODO -->
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-framework</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.apache.servicemix.bundles</groupId>
                    <artifactId>org.apache.servicemix.bundles.spring-webmvc</artifactId>
                </exclusion>
                <!-- stax only required by StaxArchetypeDataLoader -->
                <exclusion>
                    <groupId>stax</groupId>
                    <artifactId>stax</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-web</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>javax.servlet</groupId>
                    <artifactId>jstl</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>taglibs</groupId>
                    <artifactId>standard</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-security-web</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <exclusions>
                <exclusion>
                    <!-- package clashes with jakarta.annotation:jakarta.annotation-api -->
                    <groupId>com.google.code.findbugs</groupId>
                    <artifactId>jsr305</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource-common</artifactId>
            <exclusions>
                <exclusion>
                    <!-- package clashes with jakarta.annotation:jakarta.annotation-api -->
                    <groupId>com.google.code.findbugs</groupId>
                    <artifactId>jsr305</artifactId>
                </exclusion>
                <exclusion>
                    <!-- replaced by org.apache.servicemix.bundles:org.apache.servicemix.bundles.dom4j -->
                    <groupId>dom4j</groupId>
                    <artifactId>dom4j</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-app</artifactId>
            <version>${echo.version}</version>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-webcontainer</artifactId>
            <version>${echo.version}</version>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-webrender</artifactId>
            <version>${echo.version}</version>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-extras-app</artifactId>
            <version>${echo-extras.version}</version>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-extras-webcontainer</artifactId>
            <version>${echo-extras.version}</version>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-filetransfer-app</artifactId>
            <version>${echo-filetransfer.version}</version>
        </dependency>
        <dependency>
            <groupId>com.nextapp</groupId>
            <artifactId>echo-filetransfer-webcontainer</artifactId>
            <version>${echo-filetransfer.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>net.sf.echopoint</groupId>
            <artifactId>echopointng</artifactId>
            <version>2.2.0rc2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>${log4j2.version}</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-archetypes</artifactId>
            <classifier>tests</classifier>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-webmvc</artifactId>
            <scope>test</scope>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>${basedir}/src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.js</include>
                    <include>**/*.png</include>
                </includes>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>${basedir}/src/test/java</directory>
                <includes>
                    <include>*.xml</include>
                </includes>
            </testResource>
            <testResource>
                <directory>${basedir}/src/test/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.stylesheet</include>
                </includes>
            </testResource>
        </testResources>
    </build>

</project>