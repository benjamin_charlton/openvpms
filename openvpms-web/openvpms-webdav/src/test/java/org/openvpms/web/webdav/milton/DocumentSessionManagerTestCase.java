/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.webdav.milton;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.web.webdav.resource.EditableDocuments;
import org.openvpms.web.webdav.session.Session;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentSessionManager} class.
 *
 * @author Tim Anderson
 */
public class DocumentSessionManagerTestCase extends ArchetypeServiceTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The document session manager.
     */
    private DocumentSessionManager manager;

    /**
     * The test user.
     */
    private User user;

    private DocumentAct letter;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        user = TestHelper.createUser();
        EditableDocuments documents = new EditableDocuments(getArchetypeService(),
                                                            new String[]{PatientArchetypes.DOCUMENT_LETTER});
        manager = new DocumentSessionManager(documents, 10, 60) {
            @Override
            protected User getUser() {
                return user;
            }
        };

        String fileName = "Referral Letter for Muffet O'Brien.txt";
        letter = PatientTestHelper.createDocumentLetter(new Date(), patientFactory.createPatient());
        letter.setFileName(fileName);
        save(letter);
    }

    /**
     * Tests the {@link DocumentSessionManager#create(DocumentAct)} method.
     * <p/>
     * This verifies that file names are encoded for OVPMS-2738.
     */
    @Test
    public void testCreate() {
        // Verify the session can be created using the encoded name
        Session session = manager.create(letter);
        assertNotNull(session);
        assertNotNull(session.getSessionId());
        assertEquals(letter.getObjectReference(), session.getDocumentAct());
        assertEquals(user.getUsername(), session.getUserName());
        assertTrue(session.getPath().endsWith("/Referral Letter for Muffet OBrien.txt"));

        assertEquals(session, manager.get(session.getSessionId()));
    }

    /**
     * Tests the {@link DocumentSessionManager#create(String, String, String)} method.
     * <p/>
     * This verifies that file names are encoded for OVPMS-2738.
     */
    @Test
    public void testRecreateSession() {
        String sessionId = UUID.randomUUID().toString();

        String encodedName = "Referral Letter for Muffet OBrien.txt";

        // Verify a session can't be created with an unencoded name.
        assertNull(manager.create(sessionId, Long.toString(letter.getId()), letter.getFileName()));

        // Verify the session can be created using the encoded name
        Session session = manager.create(sessionId, Long.toString(letter.getId()), encodedName);
        assertNotNull(session);
        assertEquals(sessionId, session.getSessionId());
        assertEquals(letter.getObjectReference(), session.getDocumentAct());
        assertEquals(user.getUsername(), session.getUserName());
        assertTrue(session.getPath().endsWith(encodedName));

        assertEquals(session, manager.get(sessionId));

        // can't create session again
        assertNull(manager.create(sessionId, Long.toString(letter.getId()), encodedName));
    }

}
