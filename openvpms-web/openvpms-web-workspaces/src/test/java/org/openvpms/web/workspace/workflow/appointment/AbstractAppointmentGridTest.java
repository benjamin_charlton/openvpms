package org.openvpms.web.workspace.workflow.appointment;

import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid.Availability;

import java.sql.Time;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Base class for appointment grid tests.
 *
 * @author Tim Anderson
 */
public class AbstractAppointmentGridTest extends AbstractAppTest {

    /**
     * Creates an event representing an appointment.
     *
     * @param appointment the appointment
     * @return the event
     */
    protected PropertySet createEvent(Act appointment) {
        PropertySet result = new ObjectSet();
        result.set(ScheduleEvent.ACT_START_TIME, appointment.getActivityStartTime());
        result.set(ScheduleEvent.ACT_END_TIME, appointment.getActivityEndTime());
        result.set(ScheduleEvent.ACT_REFERENCE, appointment.getObjectReference());
        return result;
    }

    ScheduleEvents getScheduleEvents(Act... appointments) {
        List<PropertySet> events = new ArrayList<>();
        for (Act appointment : appointments) {
            events.add(createEvent(appointment));
        }
        return new ScheduleEvents(events, 0);
    }

    Act createAppointment(String startTime, String endTime, Entity schedule) {
        return ScheduleTestHelper.createAppointment(getDatetime(startTime), getDatetime(endTime), schedule);
    }

    /**
     * Verifies a slot  matches that expected.
     *
     * @param grid         the grid view
     * @param schedule     the schedule
     * @param slot         the slot
     * @param time         the expected date/time. May be in ISO format to support timezones
     * @param appointment  the expected appointment. May be {@code null}
     * @param slots        the expected no. of slots the appointment occupies
     * @param availability the expected availability
     */
    void checkSlot(AppointmentGrid grid, Entity schedule, int slot, String time, Act appointment, int slots,
                   Availability availability) {
        Date date = time.contains("T") ? Date.from(OffsetDateTime.parse(time).toInstant()) :
                    TestHelper.getDatetime(time);
        Date actualTime = grid.getStartTime(slot);
        assertEquals(date, actualTime);

        // verify the passing the date for the slot returns the correct slot
        assertEquals(slot, grid.getSlot(actualTime));

        Schedule gridSchedule = getSchedule(grid, schedule);
        PropertySet event = grid.getEvent(gridSchedule, slot);
        if (appointment != null) {
            assertNotNull(event);
            assertEquals(appointment.getObjectReference(), event.getReference(ScheduleEvent.ACT_REFERENCE));
        } else {
            assertNull(event);
        }
        if (event != null) {
            int actualSlots = grid.getSlots(event, gridSchedule, slot);
            assertEquals(slots, actualSlots);
        }

        // check availability
        Availability actualAvailability = grid.getAvailability(getSchedule(grid, schedule), slot);
        assertEquals(availability, actualAvailability);
    }

    /**
     * Creates a new schedule.
     *
     * @param slotSize the slot size in minutes
     * @param start    the schedule start time
     * @param end      the schedule end time
     * @return a new schedule
     */
    Entity createSchedule(int slotSize, String start, String end) {
        Party location = TestHelper.createLocation();
        Entity schedule = ScheduleTestHelper.createSchedule(
                slotSize, DateUnits.MINUTES.toString(), 1, ScheduleTestHelper.createAppointmentType(), location);
        IMObjectBean bean = getBean(schedule);
        bean.setValue("startTime", Time.valueOf(start));
        bean.setValue("endTime", Time.valueOf(end));
        return schedule;
    }

    /**
     * Returns the schedule from a grid.
     *
     * @param grid     the grid
     * @param schedule the schedule entity
     * @return the corresponding schedule
     */
    private Schedule getSchedule(AppointmentGrid grid, Entity schedule) {
        for (Schedule sched : grid.getSchedules()) {
            if (sched.getSchedule().equals(schedule)) {
                return sched;
            }
        }
        fail("Schedule not found");
        return null;
    }

}
