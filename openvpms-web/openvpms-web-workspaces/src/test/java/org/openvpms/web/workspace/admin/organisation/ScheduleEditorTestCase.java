package org.openvpms.web.workspace.admin.organisation;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import java.sql.Time;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ScheduleEditor}.
 *
 * @author Tim Anderson
 */
public class ScheduleEditorTestCase extends AbstractAppTest {

    /**
     * Tests Start Time and End Time validation.
     */
    @Test
    public void testTimesValidation() {
        Entity schedule = (Entity) create(ScheduleArchetypes.ORGANISATION_SCHEDULE);
        Context local = new LocalContext();
        local.setLocation(TestHelper.createLocation());
        DefaultLayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));
        ScheduleEditor editor = new ScheduleEditor(schedule, null, context);
        editor.getComponent();
        editor.getProperty("name").setValue("Test Schedule");

        assertTrue(editor.isValid());

        Property startTime = editor.getProperty("startTime");
        Property endTime = editor.getProperty("endTime");
        startTime.setValue(Time.valueOf("08:00:00"));
        checkInvalid(editor, "Both Start Time and End Time must be specified");

        startTime.setValue(null);
        endTime.setValue(Time.valueOf("08:00:00"));
        checkInvalid(editor, "Both Start Time and End Time must be specified");

        startTime.setValue(Time.valueOf("09:00:00"));
        endTime.setValue(Time.valueOf("09:00:00"));
        checkInvalid(editor, "Start Time must be less than End Time");

        endTime.setValue(Time.valueOf("17:00:00"));
        assertTrue(editor.isValid());
    }

    private void checkInvalid(IMObjectEditor editor, String expected) {
        Validator validator = new DefaultValidator();
        assertFalse(editor.validate(validator));
        assertEquals(expected, validator.getFirstError().getMessage());
    }
}
