/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.estimate;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActCalculator;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.EstimateActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.tax.TaxRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;
import org.openvpms.web.workspace.customer.charge.ChargeItemRelationshipCollectionEditor;
import org.openvpms.web.workspace.customer.charge.DefaultCustomerChargeActEditor;
import org.openvpms.web.workspace.customer.charge.DefaultEditorQueue;
import org.openvpms.web.workspace.customer.charge.EditorQueue;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.PATIENT_MEDICATION;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkSavePopup;


/**
 * Tests the {@link EstimateInvoicer} class.
 *
 * @author Tim Anderson
 */
public class EstimateInvoicerTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The account act factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules accountRules;

    /**
     * Tax rules.
     */
    private TaxRules taxRules;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test patient.
     */
    private Party patient;

    /**
     * The test author.
     */
    private User author;

    /**
     * The test clinician.
     */
    private User clinician;

    /**
     * The layout context.
     */
    private DefaultLayoutContext context;

    /**
     * Sets up the test case.
     */
    @Override
    public void setUp() {
        super.setUp();
        taxRules = new TaxRules(getPractice(), getArchetypeService());

        customer = TestHelper.createCustomer();
        patient = TestHelper.createPatient(customer);

        // add a weight for the patient, for dose purposes
        PatientTestHelper.createWeight(patient, BigDecimal.TEN, WeightUnits.KILOGRAMS);

        author = TestHelper.createClinician();
        new AuthenticationContextImpl().setUser(author); // makes author the logged-in user

        clinician = TestHelper.createClinician();

        context = new DefaultLayoutContext(true, new LocalContext(), new HelpContext("foo", null));
        context.getContext().setPractice(getPractice());
        context.getContext().setUser(author);
        context.getContext().setClinician(clinician);
        context.getContext().setLocation(TestHelper.createLocation());
        context.getContext().setCustomer(customer);
        context.getContext().setPatient(patient);
    }

    /**
     * Tests invoicing of estimates.
     */
    @Test
    public void testInvoice() {
        Product product1 = createMedicationWithDose();
        Product product2 = productFactory.createService();
        Product product3 = productFactory.createMerchandise();
        Product product4 = productFactory.createMedication();
        Product product5 = productFactory.createService();
        Product template1 = createTemplate("Template1 Invoice Note", "Template1 Visit Note");
        Product template2 = createTemplate(null, "Template2 Visit Note");

        BigDecimal price1 = new BigDecimal("10.00");
        BigDecimal price2 = new BigDecimal("20.00");
        BigDecimal price3 = new BigDecimal("30.00");
        BigDecimal price4 = new BigDecimal("20.00");

        BigDecimal quantity1 = ONE;
        BigDecimal quantity2 = ONE;
        BigDecimal quantity3 = BigDecimal.valueOf(2);
        BigDecimal quantity4 = BigDecimal.valueOf(4);

        BigDecimal amount1 = quantity1.multiply(price1);
        BigDecimal amount2 = quantity2.multiply(price2);
        BigDecimal amount3 = quantity3.multiply(price3);
        BigDecimal amount4 = quantity4.multiply(price4);

        BigDecimal tax1 = calculateTax(product1, amount1);
        BigDecimal tax2 = calculateTax(product2, amount2);
        BigDecimal tax3 = calculateTax(product3, amount3);
        BigDecimal tax4 = calculateTax(product4, amount4);

        Act estimate = (Act) accountFactory.newEstimate()
                .customer(customer)
                .item().patient(patient).product(product1).lowQuantity(1).highQuantity(quantity1).highUnitPrice(price1)
                .add()
                .item().patient(patient).product(product2).lowQuantity(1).highQuantity(quantity2).highUnitPrice(price2)
                .add()
                .item().patient(patient).product(product3).template(template1).group(0).lowQuantity(1)
                .highQuantity(quantity3).highUnitPrice(price3)
                .add()
                .item().patient(patient).product(product4).template(template2).group(1).lowQuantity(1)
                .highQuantity(quantity4).highUnitPrice(price4)
                .add()
                .item().patient(patient).product(product5).template(template2).group(1).lowQuantity(1)
                .highQuantity(1).highUnitPrice(0).print(false).add()
                .build();

        TestEstimateInvoicer invoicer = createEstimateInvoicer();

        EditDialog dialog = invoicer.invoice(estimate, (FinancialAct) null, context);
        IMObjectEditor editor = dialog.getEditor();
        checkSavePopup(invoicer.getEditorQueue(), PATIENT_MEDICATION, false);
        checkSavePopup(invoicer.getEditorQueue(), PATIENT_MEDICATION, false);
        assertTrue(SaveHelper.save(editor));

        FinancialAct invoice = (FinancialAct) editor.getObject();

        BigDecimal total = sum(amount1, amount2, amount3, amount4);
        BigDecimal tax = sum(tax1, tax2, tax3, tax4);
        checkEquals(total, invoice.getTotal());
        assertEquals(EstimateActStatus.INVOICED, estimate.getStatus());

        assertEquals(getBean(estimate).getTarget("invoice"), invoice);
        // verify there is a link between the invoice and estimate

        IMObjectBean bean = getBean(invoice);
        assertEquals("Template1 Invoice Note", bean.getString("notes"));
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(5, items.size());

        List<Act> estimates = bean.getSources("estimates", Act.class);
        assertEquals(1, estimates.size());
        assertTrue(estimates.contains(estimate));

        checkCharge(invoice, customer, author, clinician, tax, total);

        BigDecimal discount = ZERO;
        IMObjectBean charge1 = checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product1, null, -1,
                                         author, clinician, ONE, quantity1, ZERO, price1, ZERO, ZERO,
                                         discount, tax1, amount1, true, null, 1);
        IMObjectBean charge2 = checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product2, null, -1,
                                         author, clinician, ONE, quantity2, ZERO, price2, ZERO, ZERO,
                                         discount, tax2, amount2, true, null, 0);
        IMObjectBean charge3 = checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product3, template1,
                                         0, author, clinician, ONE, quantity3, ZERO, price3,
                                         ZERO, ZERO, discount, tax3, amount3, true, null, 0);
        IMObjectBean charge4 = checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product4, template2,
                                         1, author, clinician, ONE, quantity4, ZERO, price4,
                                         ZERO, ZERO, discount,
                                         tax4, amount4, true, null, 1);
        IMObjectBean charge5 = checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product5, template2,
                                         1, author, clinician, ONE, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO,
                                         false, null, 0); // printing suppressed

        // verify all of the charges are linked to a single event
        Act event = getEvent(charge1);
        assertEquals(event, getEvent(charge2));
        assertEquals(event, getEvent(charge3));
        assertEquals(event, getEvent(charge4));
        assertEquals(event, getEvent(charge5));

        checkEventNote(event, patient, "Template1 Visit Note");
        checkEventNote(event, patient, "Template2 Visit Note");
    }

    /**
     * Verifies that if an invoice has had templates expanded previously, invoicing an estimate to it with its
     * own templates allocates new expansion groups.
     */
    @Test
    public void testTemplateExpansionGroups() {
        Product product1 = productFactory.createService();
        Product template1 = productFactory.createTemplate();
        Product product2 = productFactory.createService();
        Product template2 = productFactory.createTemplate();
        Product product3 = productFactory.createService();
        Product template3 = productFactory.createTemplate();
        FinancialAct invoice = (FinancialAct) accountFactory.newInvoice()
                .customer(customer)
                .item().patient(patient).product(product1).template(template1).group(0)
                .add()
                .item().patient(patient).product(product2).template(template2).group(1)
                .add()
                .build();

        Act estimate = (Act) accountFactory.newEstimate()
                .customer(customer)
                .item().patient(patient).product(product3).template(template3).group(0).add()
                .build();

        EstimateInvoicer invoicer = createEstimateInvoicer();

        EditDialog dialog = invoicer.invoice(estimate, invoice, context);
        IMObjectEditor editor = dialog.getEditor();
        assertTrue(SaveHelper.save(editor));

        IMObjectBean bean = getBean(get(invoice));
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());

        checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product1, template1, 0,
                  author, null, ZERO, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, true, null, 0);
        checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product2, template2, 1,
                  author, null, ZERO, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, true, null, 0);
        checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product3, template3, 2,
                  author, clinician, ONE, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, true, null, 0);
    }

    /**
     * Verifies an estimate item can have a negative quantity which when invoiced creates a negative quantity
     * on the invoice.
     */
    @Test
    public void testNegativeQuantity() {
        Product product = productFactory.createMedication();

        Act estimate = (Act) accountFactory.newEstimate()
                .customer(customer)
                .item().patient(patient).product(product)
                .lowQuantity(-2)
                .highQuantity(-1)
                .fixedPrice(5)
                .lowUnitPrice(4)
                .highUnitPrice(5)
                .add()
                .build();

        EstimateInvoicer invoicer = createEstimateInvoicer();

        EditDialog dialog = invoicer.invoice(estimate, (FinancialAct) null, context);
        IMObjectEditor editor = dialog.getEditor();
        assertTrue(SaveHelper.save(editor));

        FinancialAct invoice = (FinancialAct) editor.getObject();
        IMObjectBean bean = getBean(get(invoice));
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());

        assertEquals(ZERO, accountRules.getBalance(customer));

        invoice.setStatus(ActStatus.POSTED);
        save(invoice);

        checkEquals(BigDecimal.valueOf(-10), accountRules.getBalance(customer));

        checkItem(items, CustomerAccountArchetypes.INVOICE_ITEM, patient, product, null, 0,
                  author, clinician, BigDecimal.valueOf(-2), BigDecimal.valueOf(-1), ZERO, BigDecimal.valueOf(5), ZERO,
                  BigDecimal.valueOf(5), ZERO, new BigDecimal("-0.909"), BigDecimal.valueOf(-10), true, null, 0);


        // verifies the invoice can be reversed
        accountRules.reverse(invoice, new Date());

        checkEquals(ZERO, accountRules.getBalance(customer));
    }

    /**
     * Returns the patient.
     *
     * @return the patient
     */
    protected Party getPatient() {
        return patient;
    }

    /**
     * Creates a new {@link EstimateInvoicer}.
     *
     * @return a new estimate invoicer
     */
    protected TestEstimateInvoicer createEstimateInvoicer() {
        return new TestEstimateInvoicer();
    }

    /**
     * Helper to calculate tax for a product and amount.
     *
     * @param product the product
     * @param amount  the amount
     * @return the tax
     */
    protected BigDecimal calculateTax(Product product, BigDecimal amount) {
        return taxRules.calculateTax(amount, product, true);
    }

    /**
     * Sums a set of amounts.
     * NOTE: this rounds first, which is the same behaviour as {@link ActCalculator#sum}. Not sure if this is correct.
     * TODO.
     *
     * @param amounts the amounts
     * @return the sum of the amounts
     */
    protected BigDecimal sum(BigDecimal... amounts) {
        BigDecimal result = ZERO;
        for (BigDecimal amount : amounts) {
            result = result.add(MathRules.round(amount));
        }
        return result;
    }

    /**
     * Creates a medication with doses.
     *
     * @return a new medication product
     */
    protected Product createMedicationWithDose() {
        return productFactory.newMedication()
                .concentration(1)
                .dose().minWeight(0).maxWeight(5).rate(1).quantity(1).add()
                .dose().minWeight(5).maxWeight(15).rate(2).quantity(2).add()
                .build();
    }

    /**
     * Creates a product template.
     *
     * @param invoiceNote the invoice note. May be {@code null}
     * @param visitNote   the visit note. May be {@code null}
     * @return a new template
     */
    private Product createTemplate(String invoiceNote, String visitNote) {
        return productFactory.newTemplate()
                .invoiceNote(invoiceNote)
                .visitNote(visitNote)
                .build();
    }

    private static class TestEstimateInvoicer extends EstimateInvoicer {

        private EditorQueue queue;

        public EditorQueue getEditorQueue() {
            return queue;
        }

        /**
         * Constructs a {@code TestEstimateInvoicer}.
         *
         * @param invoice the invoice
         * @param context the layout context
         * @return a new charge editor
         */
        @Override
        protected DefaultCustomerChargeActEditor createChargeEditor(FinancialAct invoice, LayoutContext context) {
            queue = new DefaultEditorQueue(context.getContext());
            return new DefaultCustomerChargeActEditor(invoice, null, context) {
                @Override
                protected ActRelationshipCollectionEditor createItemsEditor(Act act, CollectionProperty items) {
                    ActRelationshipCollectionEditor editor = super.createItemsEditor(act, items);
                    if (editor instanceof ChargeItemRelationshipCollectionEditor) {
                        ((ChargeItemRelationshipCollectionEditor) editor).setEditorQueue(queue);
                    }
                    return editor;
                }
            };
        }
    }
}
