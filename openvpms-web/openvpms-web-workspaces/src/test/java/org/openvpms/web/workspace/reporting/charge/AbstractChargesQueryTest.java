/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.AbstractTestCustomerChargeBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestCounterSaleBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.query.QueryTestHelper;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.query.ResultSetIterator;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.reporting.charge.wip.IncompleteChargesQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Base class for tests of {@link AbstractChargesQuery} implementations.
 *
 * @author Tim Anderson
 */
public abstract class AbstractChargesQueryTest extends AbstractAppTest {

    /**
     * Determines if the query selects POSTED charges.
     */
    private final boolean selectsPosted;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The product.
     */
    private Product product;

    /**
     * Constructs an {@link AbstractChargesQueryTest}.
     *
     * @param selectsPosted if {@code true} the query includes POSTED charges, else it excludes them
     */
    protected AbstractChargesQueryTest(boolean selectsPosted) {
        this.selectsPosted = selectsPosted;
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        customer = (Party) customerFactory.createCustomer();
        patient = (Party) patientFactory.createPatient();
        product = productFactory.createMedication();
    }

    /**
     * Verifies that if there are no charges for a user's location, an empty result set is returned.
     */
    @Test
    public void testEmptyQuery() {
        Context context = new LocalContext();
        Party location1 = (Party) practiceFactory.createLocation();
        Party location2 = (Party) practiceFactory.createLocation();
        Party location3 = (Party) practiceFactory.createLocation();
        Party practice = (Party) practiceFactory.newPractice()
                .locations(location1, location2, location3)
                .build();

        User user = (User) userFactory.newUser()
                .addLocations(location1)
                .build();
        context.setUser(user);
        context.setPractice(practice);

        AbstractChargesQuery query = createQuery(context);
        query.setLocation(location1);
        QueryTestHelper.checkEmpty(query);

        Act invoice = createInvoice(location2, null, ActStatus.IN_PROGRESS);
        Act counter = createCounter(location2, ActStatus.IN_PROGRESS);
        Act credit = createCredit(location3, null, ActStatus.IN_PROGRESS);

        save(invoice);
        save(counter);
        save(credit);
        QueryTestHelper.checkEmpty(query);
    }

    /**
     * Tests querying charges by practice location.
     */
    @Test
    public void testQueryByLocation() {
        Context context = new LocalContext();
        Party location1 = (Party) practiceFactory.createLocation();
        Party location2 = (Party) practiceFactory.createLocation();
        Party location3 = (Party) practiceFactory.createLocation();
        Party practice = (Party) practiceFactory.newPractice()
                .locations(location1, location2, location3)
                .build();

        User user = (User) userFactory.newUser()
                .addLocations(location1, location2)
                .build();

        context.setUser(user);
        context.setPractice(practice);

        AbstractChargesQuery query = createQuery(context);

        query.setLocation(null);  // all locations
        checkNoLocationInvoices(query, IncompleteChargesQuery.SHORT_NAMES);

        Act invoice = createInvoice(location1, null, ActStatus.IN_PROGRESS);
        Act counter = createCounter(location2, ActStatus.IN_PROGRESS);
        Act credit = createCredit(location3, null, ActStatus.IN_PROGRESS);

        List<IMObjectReference> refs = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs.contains(invoice.getObjectReference()));
        assertTrue(refs.contains(counter.getObjectReference()));
        assertFalse(refs.contains(credit.getObjectReference()));

        query.setLocation(location1);
        refs = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs.contains(invoice.getObjectReference()));
        assertFalse(refs.contains(counter.getObjectReference()));
        assertFalse(refs.contains(credit.getObjectReference()));
    }

    /**
     * Verifies that the user inherits the practice's locations.
     */
    @Test
    public void testQueryByLocationForUserWithNoLocations() {
        Context context = new LocalContext();
        Party location1 = (Party) practiceFactory.createLocation();
        ;
        Party location2 = (Party) practiceFactory.createLocation();
        Party location3 = (Party) practiceFactory.createLocation();
        User user = (User) userFactory.createUser();
        Party practice = (Party) practiceFactory.newPractice()
                .locations(location1, location2)
                .build();

        context.setUser(user);
        context.setPractice(practice);

        AbstractChargesQuery query = createQuery(context);

        query.setLocation(null);  // all locations
        checkNoLocationInvoices(query, IncompleteChargesQuery.SHORT_NAMES);

        Act invoice = createInvoice(location1, null, ActStatus.IN_PROGRESS);
        Act counter = createCounter(location2, ActStatus.IN_PROGRESS);
        Act credit = createCredit(location3, null, ActStatus.IN_PROGRESS);

        List<IMObjectReference> refs = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs.contains(invoice.getObjectReference()));
        assertTrue(refs.contains(counter.getObjectReference()));
        assertFalse(refs.contains(credit.getObjectReference()));

        query.setLocation(location1);
        refs = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs.contains(invoice.getObjectReference()));
        assertFalse(refs.contains(counter.getObjectReference()));
        assertFalse(refs.contains(credit.getObjectReference()));
    }

    /**
     * Tests querying charges by status.
     */
    @Test
    public void testQueryByStatus() {
        Context context = new LocalContext();
        Party location1 = (Party) practiceFactory.createLocation();
        User user = (User) userFactory.newUser()
                .addLocations(location1)
                .build();
        context.setUser(user);

        AbstractChargesQuery query = createQuery(context);

        Act inProgress = createInvoice(location1, null, ActStatus.IN_PROGRESS);
        Act completed = createInvoice(location1, null, ActStatus.COMPLETED);
        Act onHold = createInvoice(location1, null, FinancialActStatus.ON_HOLD);
        Act posted = createInvoice(location1, null, FinancialActStatus.POSTED);

        List<IMObjectReference> refs = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs.contains(inProgress.getObjectReference()));
        assertTrue(refs.contains(completed.getObjectReference()));
        assertTrue(refs.contains(onHold.getObjectReference()));
        assertEquals(selectsPosted, refs.contains(posted.getObjectReference()));

        query.setStatus(ActStatus.IN_PROGRESS);
        refs = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs.contains(inProgress.getObjectReference()));
        assertFalse(refs.contains(completed.getObjectReference()));
        assertFalse(refs.contains(onHold.getObjectReference()));
        assertFalse(refs.contains(posted.getObjectReference()));
    }

    /**
     * Tests querying charges by clinician.
     */
    @Test
    public void testQueryByClinician() {
        Context context = new LocalContext();
        Party location1 = (Party) practiceFactory.createLocation();
        User user = (User) userFactory.createUser();
        context.setUser(user);

        User clinician1 = (User) userFactory.createClinician();
        User clinician2 = (User) userFactory.createClinician();

        Act invoice1 = createInvoice(location1, clinician1, ActStatus.IN_PROGRESS);
        Act invoice2 = createInvoice(location1, clinician2, ActStatus.COMPLETED);
        Act invoice3 = createInvoice(location1, null, FinancialActStatus.ON_HOLD);
        Act invoice4 = createInvoice(location1, clinician1, FinancialActStatus.POSTED);

        Act credit1 = createCredit(location1, clinician1, ActStatus.IN_PROGRESS);
        Act credit2 = createCredit(location1, clinician2, ActStatus.POSTED);

        Act counter1 = createCounter(location1, ActStatus.IN_PROGRESS);
        Act counter2 = createCounter(location1, ActStatus.POSTED);

        AbstractChargesQuery query = createQuery(context);
        query.setClinician(clinician1);

        List<IMObjectReference> refs1 = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs1.contains(invoice1.getObjectReference()));
        assertEquals(selectsPosted, refs1.contains(invoice4.getObjectReference()));
        assertTrue(refs1.contains(credit1.getObjectReference()));

        assertFalse(refs1.contains(invoice2.getObjectReference()));
        assertFalse(refs1.contains(invoice3.getObjectReference()));
        assertFalse(refs1.contains(credit2.getObjectReference()));
        assertFalse(refs1.contains(counter1.getObjectReference()));
        assertFalse(refs1.contains(counter2.getObjectReference()));

        query.setClinician(clinician2);
        List<IMObjectReference> refs2 = QueryTestHelper.getObjectRefs(query);
        assertTrue(refs2.contains(invoice2.getObjectReference()));
        assertEquals(selectsPosted, refs2.contains(credit2.getObjectReference()));

        assertFalse(refs2.contains(invoice1.getObjectReference()));
        assertFalse(refs2.contains(invoice3.getObjectReference()));
        assertFalse(refs2.contains(invoice4.getObjectReference()));
        assertFalse(refs2.contains(credit1.getObjectReference()));
        assertFalse(refs2.contains(counter1.getObjectReference()));
        assertFalse(refs2.contains(counter2.getObjectReference()));
    }

    /**
     * Creates a new query.
     *
     * @param context the context
     * @return a new query
     */
    protected abstract AbstractChargesQuery createQuery(Context context);

    /**
     * Verifies that only charges with no location are returned by the query.
     *
     * @param query      the query
     * @param shortNames the expected short names
     */
    private void checkNoLocationInvoices(AbstractChargesQuery query, String... shortNames) {
        ResultSet<Act> set = query.query();
        ResultSetIterator<Act> iterator = new ResultSetIterator<>(set);
        while (iterator.hasNext()) {
            Act act = iterator.next();
            IMObjectBean bean = getBean(act);
            assertTrue(bean.isA(shortNames));
            assertNull(bean.getTargetRef("location"));
        }
    }

    /**
     * Creates an invoice.
     *
     * @param location  the practice location
     * @param clinician the clinician. May be {@code null}
     * @param status    the invoice status
     * @return the invoice
     */
    private Act createInvoice(Party location, User clinician, String status) {
        return createCharge(accountFactory.newInvoice(), location, clinician, status);
    }

    /**
     * Creates a credit.
     *
     * @param location  the practice location
     * @param clinician the clinician. May be {@code null}
     * @param status    the credit status
     * @return the credit
     */
    private Act createCredit(Party location, User clinician, String status) {
        return createCharge(accountFactory.newCredit(), location, clinician, status);
    }

    /**
     * Creates a charge.
     *
     * @param builder   the charge builder
     * @param location  the practice location
     * @param clinician the clinician. May be {@code null}
     * @param status    the charge status
     * @return the charge
     */
    private Act createCharge(AbstractTestCustomerChargeBuilder<?, ?> builder, Party location, User clinician,
                             String status) {
        boolean counter = (builder instanceof TestCounterSaleBuilder);
        return (Act) builder.customer(customer)
                .location(location)
                .clinician(clinician)
                .status(status)
                .item()
                .patient(counter ? null : patient)
                .product(product)
                .add()
                .build();
    }

    /**
     * Creates a counter sale.
     *
     * @param location the practice location
     * @param status   the counter sale status
     * @return the counter sale
     */
    private Act createCounter(Party location, String status) {
        return createCharge(accountFactory.newCounterSale(), location, null, status);
    }
}