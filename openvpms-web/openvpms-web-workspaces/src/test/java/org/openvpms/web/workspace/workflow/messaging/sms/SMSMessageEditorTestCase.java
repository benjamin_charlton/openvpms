/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link SMSMessageEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class SMSMessageEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The SMS factory.
     */
    @Autowired
    private TestSMSFactory smsFactory;

    /**
     * Verifies that the start and end time are not subject to validation, as per OVPMS-2639.
     */
    @Test
    public void testStartEndTimeValidation() {
        Act act1 = (Act) smsFactory.newSMS()
                .message("test 1")
                .sent(DateRules.getYesterday())
                .updated(DateRules.getToday())
                .build(false);

        assertTrue(createEditor(act1).isValid());

        Act act2 = (Act) smsFactory.newSMS()
                .message("test 2")
                .sent(DateRules.getToday())
                .updated(DateRules.getYesterday())
                .build(false);

        assertTrue(createEditor(act2).isValid());
    }

    /**
     * Tests the {@link SMSMessageEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Act act = (Act) smsFactory.newSMS().build(false);
        SMSMessageEditor editor = createEditor(act);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof SMSMessageEditor);
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link SMSMessageEditor} for
     * <em>act.smsMessage</em> instances.
     */
    @Test
    public void testFactory() {
        Act act = (Act) smsFactory.newSMS().build(false);
        IMObjectEditor editor = factory.create(act, new DefaultLayoutContext(new LocalContext(),
                                                                             new HelpContext("foo", null)));
        assertTrue(editor instanceof SMSMessageEditor);
    }

    /**
     * Verifies that an {@link SMSMessageLayoutStrategy} is used to lay out the component.
     */
    @Test
    public void testLayoutStrategy() {
        Act act = (Act) smsFactory.newSMS().build(false);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        Mutable<IMObjectLayoutStrategy> strategy = new MutableObject<>();
        SMSMessageEditor editor = new SMSMessageEditor(act, null, context) {
            @Override
            protected IMObjectLayoutStrategy createLayoutStrategy() {
                IMObjectLayoutStrategy result = super.createLayoutStrategy();
                strategy.setValue(result);
                return result;
            }
        };
        editor.getComponent();
        assertTrue(strategy.getValue() instanceof SMSMessageLayoutStrategy);
    }

    private SMSMessageEditor createEditor(Act act) {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        return new SMSMessageEditor(act, null, context);
    }
}
