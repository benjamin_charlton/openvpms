/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.consult;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.workflow.DefaultTaskContext;
import org.openvpms.web.component.workflow.SynchronousTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.workflow.checkout.GetCheckOutInvoiceTask;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Base class for {@link GetConsultInvoiceTask} and {@link GetCheckOutInvoiceTask} to test common functionality.
 *
 * @author Tim Anderson
 */
public abstract class AbstractGetConsultInvoiceTaskTest extends AbstractAppTest {

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test patient.
     */
    private Party patient1;

    /**
     * The test visit.
     */
    private Act event;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        customer = TestHelper.createCustomer();
        patient1 = TestHelper.createPatient(customer);
        event = PatientTestHelper.createEvent(patient1);
    }

    /**
     * Verifies that the task completes if there is no invoice.
     */
    @Test
    public void testMissingInvoice() {
        SynchronousTask task = createGetInvoiceTask();
        TaskContext context = createContext();
        task.execute(context);
        assertNull(context.getObject(CustomerAccountArchetypes.INVOICE));
    }

    /**
     * Verifies that if there is an invoice linked to the event, it will be returned over any other invoice,
     * unless it is POSTED.
     */
    @Test
    public void testInvoiceLinkedToEvent() {
        Product product = TestHelper.createProduct();

        // create 2 invoices for the customer
        List<FinancialAct> acts1 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer, patient1,
                                                                            product, ActStatus.IN_PROGRESS);
        List<FinancialAct> acts2 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer, patient1,
                                                                            product, ActStatus.IN_PROGRESS);

        FinancialAct invoice1 = acts1.get(0);
        FinancialAct invoice2 = acts2.get(0);

        invoice1.setActivityStartTime(DateRules.getYesterday());
        invoice2.setActivityStartTime(DateRules.getToday());

        save(acts1);
        save(acts2);

        // link invoice1 to the event
        FinancialAct item1 = acts1.get(1);
        addToEvent(event, item1);

        // verify invoice1 is returned
        SynchronousTask task = createGetInvoiceTask();
        TaskContext context1 = createContext();
        task.execute(context1);
        assertEquals(invoice1, context1.getObject(CustomerAccountArchetypes.INVOICE));

        // now post invoice1. Verify invoice2 is returned
        invoice1.setStatus(ActStatus.POSTED);
        save(invoice1);

        TaskContext context2 = createContext();
        task.execute(context2);
        assertEquals(invoice2, context2.getObject(CustomerAccountArchetypes.INVOICE));

        // now post invoice2. Verify invoice1 is returned as it is linked to the event
        invoice2.setStatus(ActStatus.POSTED);
        save(invoice2);

        TaskContext context3 = createContext();
        task.execute(context3);
        assertEquals(invoice1, context3.getObject(CustomerAccountArchetypes.INVOICE));

        // now link invoice2 to the event. It should be returned now, as it is newer than invoice1
        FinancialAct item2 = acts2.get(1);
        addToEvent(event, item2);

        TaskContext context4 = createContext();
        task.execute(context4);
        assertEquals(invoice2, context4.getObject(CustomerAccountArchetypes.INVOICE));
    }

    /**
     * Returns the test customer.
     *
     * @return the test customer
     */
    protected Party getCustomer() {
        return customer;
    }

    /**
     * Adds an invoice item to an event.
     *
     * @param event the event
     * @param item the item to add
     */
    protected void addToEvent(Act event, FinancialAct item) {
        IMObjectBean bean = getBean(event);
        bean.addTarget("chargeItems", item);
        save(event, item);
    }

    /**
     * Creates a new task to populate the context with the invoice.
     *
     * @return a new task
     */
    protected abstract SynchronousTask createGetInvoiceTask();

    /**
     * Creates the task context.
     *
     * @return a new task context
     */
    private TaskContext createContext() {
        TaskContext context;
        context = new DefaultTaskContext(new HelpContext("foo", null));
        context.addObject(customer);
        context.addObject(patient1);
        context.addObject(event);
        return context;
    }
}
