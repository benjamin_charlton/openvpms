/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import nextapp.echo2.app.button.AbstractButton;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.till.TillBalanceStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link AccountCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class AccountCRUDWindowTestCase extends AbstractAppTest {

    /**
     * Verifies that payments can only be edited by administrators.
     */
    @Test
    public void testEditPayment() {
        Party customer = TestHelper.createCustomer();
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer,
                                                                 TestHelper.createTill(), ActStatus.POSTED);
        save(payment);
        List<FinancialAct> invoice = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer,
                                                                              TestHelper.createPatient(),
                                                                              TestHelper.createProduct(),
                                                                              ActStatus.POSTED);
        save(invoice);

        // verify that payments cannot be edited by non-administrators
        Context context1 = new LocalContext();
        context1.setUser(TestHelper.createUser(false));
        TestAccountCRUDWindow window1 = new TestAccountCRUDWindow(context1);
        assertNull(window1.getButtons().getButton(AccountCRUDWindow.EDIT_ID)); // not an administrator so no edit button
        window1.setObject(payment);
        assertNull(window1.getButtons().getButton(AccountCRUDWindow.EDIT_ID));

        // verify editing invoices not supported
        window1.setObject(invoice.get(0));
        assertNull(window1.getButtons().getButton(AccountCRUDWindow.EDIT_ID));

        // now verify payments can be edited by administrators
        Context context2 = new LocalContext();
        context2.setUser(TestHelper.createAdministrator(false));
        TestAccountCRUDWindow window2 = new TestAccountCRUDWindow(context2);
        AbstractButton button = window2.getButtons().getButton(AccountCRUDWindow.EDIT_ID);
        assertNotNull(button);
        assertFalse(button.isEnabled());
        window2.setObject(payment);
        assertTrue(button.isEnabled());

        // verify editing invoices not supported for administrators either
        window2.setObject(invoice.get(0));
        assertFalse(button.isEnabled());
    }

    /**
     * Verifies that if the till is cleared, the payment cannot be edited.
     */
    @Test
    public void testEditPaymentForClearedTill() {
        ArrayList<String> errors = new ArrayList<>();
        initErrorHandler(errors);
        Context context = new LocalContext();
        context.setUser(TestHelper.createAdministrator(false));
        TestAccountCRUDWindow window = new TestAccountCRUDWindow(context);

        Party customer = TestHelper.createCustomer();
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer,
                                                                 TestHelper.createTill(), ActStatus.POSTED);
        save(payment);
        IMObjectBean bean = getBean(payment);
        FinancialAct balance = bean.getSource("tillBalance", FinancialAct.class);
        assertNotNull(balance);
        assertEquals(TillBalanceStatus.UNCLEARED, balance.getStatus());

        window.administerPayment(payment);
        EditDialog dialog1 = EchoTestHelper.findEditDialog();
        assertNotNull(dialog1);
        EchoTestHelper.fireDialogButton(dialog1, EditDialog.OK_ID);
        assertNull(EchoTestHelper.findEditDialog());

        balance.setStatus(TillBalanceStatus.IN_PROGRESS);
        save(balance);
        window.administerPayment(payment);
        EditDialog dialog2 = EchoTestHelper.findEditDialog();
        assertNotNull(dialog2);
        EchoTestHelper.fireDialogButton(dialog2, EditDialog.OK_ID);
        assertNull(EchoTestHelper.findEditDialog());

        balance.setStatus(TillBalanceStatus.CLEARED);
        save(balance);

        window.administerPayment(payment);
        assertNull(EchoTestHelper.findEditDialog());
        ErrorDialog errorDialog = EchoTestHelper.findWindowPane(ErrorDialog.class);
        assertEquals("This payment cannot be edited as it linked to a Cleared Till Balance", errorDialog.getMessage());
    }

    private static class TestAccountCRUDWindow extends AccountCRUDWindow {

        public TestAccountCRUDWindow(Context context) {
            super(Archetypes.create("act.customerAccount*", FinancialAct.class), context, new HelpContext("foo", null));
            getComponent();
        }

        /**
         * Returns the button set.
         *
         * @return the button set
         */
        @Override
        public ButtonSet getButtons() {
            return super.getButtons();
        }
    }
}