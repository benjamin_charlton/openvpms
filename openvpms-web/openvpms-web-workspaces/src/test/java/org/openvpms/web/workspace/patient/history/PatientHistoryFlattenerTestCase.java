/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import org.junit.Test;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.workspace.customer.communication.CommunicationArchetypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE_ITEM;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.CLINICAL_NOTE;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.CLINICAL_PROBLEM;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.PATIENT_MEDICATION;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.PATIENT_WEIGHT;
import static org.openvpms.archetype.rules.patient.PatientTestHelper.createEvent;
import static org.openvpms.archetype.rules.patient.PatientTestHelper.createNote;
import static org.openvpms.archetype.rules.patient.PatientTestHelper.createProblem;
import static org.openvpms.archetype.rules.patient.PatientTestHelper.createWeight;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link PatientHistoryFlattener}.
 *
 * @author Tim Anderson
 */
public class PatientHistoryFlattenerTestCase extends AbstractPatientHistoryFlattenerTest {

    /**
     * Tests history flattening.
     */
    @Test
    public void testFlattener() {
        Party customer = TestHelper.createCustomer();
        Party patient = TestHelper.createPatient(customer);
        User clinician = TestHelper.createClinician();
        String include = getReason("INCLUDE", true);
        String exclude = getReason("EXCLUDE", false);

        Act communication1 = createCommunication(getDatetime("2019-07-02 09:00"), customer, patient, include);
        Act communication2 = createCommunication(getDatetime("2019-07-02 15:00"), customer, patient, include);
        Act communication3 = createCommunication(getDatetime("2019-07-03 09:00"), customer, patient, include);

        // create a communication that will be excluded
        createCommunication(getDatetime("2019-07-03 09:00"), customer, patient, exclude);

        Act weight = createWeight(getDatetime("2019-07-03 10:00:00"), patient, clinician);
        Act problemNote = createNote(getDatetime("2019-07-03 10:06:00"), patient, clinician);
        Act problem = createProblem(getDatetime("2019-07-03 10:05:00"), patient, clinician, problemNote);
        Act communication4 = createCommunication(getDatetime("2019-07-03 11:00"), customer, patient, null);
        Act event = createEvent(getDatetime("2019-07-03 10:00:00"), patient, clinician, weight, problem, problemNote);
        Act communication5 = createCommunication(getDatetime("2019-07-03 12:00"), customer, patient, null);
        Act communication6 = createCommunication(getDatetime("2019-07-03 13:00"), customer, patient, null);

        String[] archetypes = {CLINICAL_PROBLEM, PATIENT_WEIGHT, PATIENT_MEDICATION, INVOICE_ITEM, CLINICAL_NOTE};

        // primary acts sorted in ascending order
        List<Act> ascending = Arrays.asList(communication1, communication2, communication3, event,
                                            communication4, communication5, communication6);

        // and descending order
        List<Act> descending = new ArrayList<>(ascending);
        Collections.reverse(descending);

        PatientHistoryFlattener flattener = new PatientHistoryFlattener(getArchetypeService());

        // parent, child sorted ascending
        check(flattener, ascending, archetypes, true, true, communication1, communication2, communication3,
              event, weight, problem, problemNote, communication4, communication5, communication6);

        // parent ascending, child sorted descending
        check(flattener, ascending, archetypes, true, false, communication1, communication2, communication3,
              event, communication6, communication5, communication4, problem, problemNote, weight);

        // parent, child sorted descending
        check(flattener, descending, archetypes, false, false, event, communication6, communication5, communication4, problem,
              problemNote, weight, communication3, communication2, communication1);

        // parent descending, child sorted ascending
        check(flattener, descending, archetypes, false, true, event, weight, problem, problemNote, communication4, communication5,
              communication6, communication3, communication2, communication1);
    }

    /**
     * Verifies that records with the same timestamp are handled in a uniform manner.
     * <p/>
     * As the timestamps are the same, the object ids will be used to determine ordering.
     */
    @Test
    public void testSameTimestamp() {
        Party customer = TestHelper.createCustomer();
        Party patient = TestHelper.createPatient(customer);
        User clinician = TestHelper.createClinician();
        Date date = getDatetime("2019-07-05 10:00:00");
        Act communication = createCommunication(date, customer, patient, null);
        Act note = createNote(date, patient, clinician);
        Act event = createEvent(date, patient, clinician, note);

        List<Act> acts = Arrays.asList(communication, event);

        String[] archetypes = {CLINICAL_PROBLEM, PATIENT_WEIGHT, PATIENT_MEDICATION, INVOICE_ITEM, CLINICAL_NOTE};

        PatientHistoryFlattener flattener = new PatientHistoryFlattener(getArchetypeService());
        check(flattener, acts, archetypes, true, true, event, communication, note);
    }

    /**
     * Creates a communication reason.
     *
     * @param code    the lookup code
     * @param include if {@code true}, include it in patient history, else exclude it
     * @return the lookup code
     */
    private String getReason(String code, boolean include) {
        Lookup lookup = TestHelper.getLookup(CommunicationArchetypes.REASON, code);
        IMObjectBean bean = getBean(lookup);
        bean.setValue("showInPatientHistory", include);
        return lookup.getCode();
    }

}
