/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.internal.report.ReportBuilderImpl;
import org.openvpms.laboratory.internal.report.ResultImpl;
import org.openvpms.laboratory.internal.report.ResultsImpl;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;
import org.openvpms.laboratory.report.Result;
import org.openvpms.laboratory.report.Results;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.doc.DocumentActEditor;
import org.openvpms.web.component.im.doc.VersionedDocumentActEditorTest;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.ByteArrayInputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;


/**
 * Tests the {@link PatientInvestigationActEditor} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientInvestigationActEditorTestCase extends VersionedDocumentActEditorTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The laboratory services.
     */
    private LaboratoryServices laboratoryServices;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        laboratoryServices = Mockito.mock(LaboratoryServices.class);
        LaboratoryService service = Mockito.mock(LaboratoryService.class);
        Mockito.when(laboratoryServices.getService(any())).thenReturn(service);
    }

    /**
     * Verifies that if a test is removed from a product, existing investigations using both
     * the product and test may still be saved without triggering validation errors.
     * <p/>
     * This is required if a test is changed on a product (e.g. a different provider is used), but existing
     * investigations still need to be edited.
     */
    @Test
    public void testDeleteInvestigationTypeFromProduct() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Entity type = LaboratoryTestHelper.createInvestigationType();
        Entity test = LaboratoryTestHelper.createTest(type);
        IMObjectBean bean = getBean(act);
        bean.setTarget("investigationType", type);
        Product product = TestHelper.createProduct();
        ProductTestHelper.addTest(product, test);

        PatientInvestigationActEditor editor = createInvestigationEditor(act);
        editor.setPatient(TestHelper.createPatient());
        editor.setInvestigationType(type);
        editor.addProduct(product);
        editor.setLocation(TestHelper.createLocation());
        editor.save();

        IMObjectBean productBean = getBean(product);
        Relationship relationship = productBean.removeTarget("tests", test);
        assertNotNull(relationship);
        productBean.save();

        editor = createInvestigationEditor(act);
        editor.getProperty("description").setValue("Some notes to flag the editor as modified");
        assertTrue(editor.isModified());
        editor.save();
    }

    /**
     * Tests the {@link PatientInvestigationActEditor#canAddTest(Party, Entity)} method.
     */
    @Test
    public void testCanAddTest() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Entity typeA = LaboratoryTestHelper.createInvestigationType();
        Entity typeB = LaboratoryTestHelper.createInvestigationType();
        Entity test1 = LaboratoryTestHelper.createTest(typeA, true);
        Entity test2 = LaboratoryTestHelper.createTest(typeA, true);
        Entity test3 = LaboratoryTestHelper.createTest(typeA, false);
        Entity test4 = LaboratoryTestHelper.createTest(typeB, true);

        Product product = TestHelper.createProduct();
        ProductTestHelper.addTest(product, test1);

        PatientInvestigationActEditor editor = createInvestigationEditor(act);
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        editor.setPatient(patient1);

        assertFalse(editor.canAddTest(patient1, test1));    // no investigation type
        editor.setInvestigationType(typeA);
        assertTrue(editor.canAddTest(patient1, test1));
        editor.addTest(test1);

        assertFalse(editor.canAddTest(patient2, test1)); // wrong patient

        assertFalse(editor.canAddTest(patient1, test1));  // already present
        assertTrue(editor.canAddTest(patient1, test2));   // group == true
        assertFalse(editor.canAddTest(patient1, test3));   // group == false
        assertFalse(editor.canAddTest(patient1, test4));   // wrong investigation type

        test2.setActive(false);
        save(test2);
        assertFalse(editor.canAddTest(patient1, test2));   // active == false
    }

    /**
     * Verifies that a device is automatically selected when useDevice="YES"/"OPTIONAL" and there is a single
     * device for the practice location.
     */
    @Test
    public void testDefaultDevice() {
        Party location = TestHelper.createLocation();
        Entity laboratory = LaboratoryTestHelper.createLaboratory(location);
        Entity typeA = LaboratoryTestHelper.createInvestigationType(laboratory);
        Entity device1 = LaboratoryTestHelper.createDevice(laboratory);
        Entity device2 = LaboratoryTestHelper.createDevice(laboratory);
        device2.setActive(false);
        save(device2);
        Entity typeB = LaboratoryTestHelper.createInvestigationType(laboratory, device1, device2);
        Entity test1 = LaboratoryTestHelper.createTest(typeA, false, "YES");
        Entity test2 = LaboratoryTestHelper.createTest(typeB, false, "YES");
        Entity test3 = LaboratoryTestHelper.createTest(typeB, false, "OPTIONAL");
        Entity test4 = LaboratoryTestHelper.createTest(typeB, false, "NO");

        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        PatientInvestigationActEditor editor = createInvestigationEditor(act);
        editor.setLocation(location);
        editor.setPatient(TestHelper.createPatient());
        editor.setInvestigationType(typeA);
        editor.addTest(test1);
        assertNull(editor.getDevice());

        editor.setInvestigationType(typeB);
        assertEquals(0, editor.getTests().size());
        editor.addTest(test2);
        assertEquals(device1, editor.getDevice());

        editor.removeTest(test2);
        assertEquals(0, editor.getTests().size());
        assertNull(editor.getDevice());

        editor.addTest(test3);      // optional device
        assertEquals(device1, editor.getDevice());

        editor.removeTest(test3);
        editor.addTest(test4);      // no device
        assertNull(editor.getDevice());
    }

    /**
     * Tests validation.
     */
    @Test
    public void testValidation() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Party location1 = TestHelper.createLocation();
        Party location2 = TestHelper.createLocation();
        Entity laboratory = LaboratoryTestHelper.createLaboratory();
        Entity device1 = LaboratoryTestHelper.createDevice(laboratory, location1, location2);
        Entity device2 = LaboratoryTestHelper.createDevice(laboratory, location2);
        Entity typeA = LaboratoryTestHelper.createInvestigationType(laboratory, device1, device2);
        Entity typeB = LaboratoryTestHelper.createInvestigationType();
        Entity test1 = LaboratoryTestHelper.createTest(typeA, true, "YES");
        Entity test2 = LaboratoryTestHelper.createTest(typeA, true);
        Entity test3 = LaboratoryTestHelper.createTest(typeA, false);
        Entity test4 = LaboratoryTestHelper.createTest(typeB, true);

        PatientInvestigationActEditor editor = createInvestigationEditor(act);
        editor.setPatient(TestHelper.createPatient());

        // verifies an investigation type is required
        editor.setInvestigationType(null);
        checkInvalid(editor, "Investigation Type is required");

        editor.setInvestigationType(typeB);
        assertTrue(editor.isValid());

        // verify a test is required if the investigation type has a laboratory
        editor.setLocation(location1);   // NOTE: need a location to resolve the laboratory
        editor.setInvestigationType(typeA);
        checkInvalid(editor, "A Test is required for " + laboratory.getName());

        // verifies a location is required when a test is specified
        editor.setLocation(null);
        editor.addTest(test1);
        checkInvalid(editor, "Location is required");

        // Verifies a device must be specified if a test requires it. Corresponds to message
        // investigation.test.deviceRequired
        editor.setLocation(location1);
        checkInvalid(editor, "A Laboratory Device must be specified for " + test1.getName());

        editor.setDevice(device1);
        assertTrue(editor.isValid());

        editor.setDevice(device2);
        checkInvalid(editor, device2.getName() + " cannot be used at " + location1.getName());

        // investigation.test.unsupportedDevice
        editor.removeTest(test1);
        editor.addTest(test2);
        editor.setDevice(device1);
        checkInvalid(editor, "Test " + test2.getName() + " cannot use a Laboratory Device");

        // investigation.test.unsupported
        editor.removeTest(test2);
        editor.addTest(test4);
        checkInvalid(editor, "Test " + test4.getName() + " is not provided by " + typeA.getName());

        editor.removeTest(test4);
        editor.addTest(test1);

        editor.setInvestigationType(typeB); // not ordered via a laboratory
        editor.addTest(test4);
        assertTrue(editor.isValid());

        editor.setInvestigationType(typeA);
        editor.addTest(test2);
        editor.addTest(test3);
        checkInvalid(editor, test3.getName() + " cannot be grouped with other tests");
    }

    /**
     * Verifies that if there is laboratory configuration at one location, there must be configuration at
     * the location used.
     */
    @Test
    public void testValidateLaboratoryWithNoLocationConfiguration() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Party location1 = TestHelper.createLocation();
        Party location2 = TestHelper.createLocation();

        // configure a laboratory for location1, but not location2
        Entity laboratory = LaboratoryTestHelper.createLaboratory(location1);
        Entity type = LaboratoryTestHelper.createInvestigationType(laboratory);
        Entity test = LaboratoryTestHelper.createTest(type);

        // verify the investigation is valid when location1 is used
        PatientInvestigationActEditor editor = createInvestigationEditor(act);
        editor.setPatient(TestHelper.createPatient());
        editor.setLocation(location1);
        editor.setInvestigationType(type);
        editor.addTest(test);
        assertTrue(editor.isValid());

        // verify it is invalid when location2 is used
        editor.setLocation(location2);
        editor.setInvestigationType(type);
        checkInvalid(editor, "Cannot order tests for " + type.getName() + ". There is no configuration for "
                             + laboratory.getName() + " at " + location2.getName() + ".");
    }

    /**
     * Verifies that there is no requirement that the startTime  is {@code <=} endTime.
     */
    @Test
    public void testValidateStartEndTimes() {
        Party location = TestHelper.createLocation();
        Entity laboratory = LaboratoryTestHelper.createLaboratory(location);
        Entity typeA = LaboratoryTestHelper.createInvestigationType(laboratory);
        Entity test1 = LaboratoryTestHelper.createTest(typeA, false, "NO");

        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        PatientInvestigationActEditor editor = createInvestigationEditor(act);
        editor.setLocation(location);
        editor.setPatient(TestHelper.createPatient());
        editor.setInvestigationType(typeA);
        editor.addTest(test1);
        assertTrue(editor.isValid());

        editor.setStartTime(DateRules.getToday());
        editor.setEndTime(DateRules.getToday());
        assertTrue(editor.isValid());

        editor.setEndTime(DateRules.getTomorrow());
        assertTrue(editor.isValid());

        editor.setStartTime(DateRules.getTomorrow());
        editor.setEndTime(DateRules.getToday());
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that the laboratory on the investigation must agree with the laboratory on the investigation type.
     */
    @Test
    public void testMismatchLaboratoryOnInvestigationType() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Party location = TestHelper.createLocation();
        Entity laboratory1 = LaboratoryTestHelper.createLaboratory();
        Entity laboratory2 = LaboratoryTestHelper.createLaboratory();
        Entity investigationType = LaboratoryTestHelper.createInvestigationType(laboratory1);
        Entity test = LaboratoryTestHelper.createTest(investigationType, true);

        PatientInvestigationActEditor editor1 = createInvestigationEditor(act);
        editor1.setLocation(location);
        editor1.setPatient(TestHelper.createPatient());
        editor1.setInvestigationType(investigationType);
        editor1.addTest(test);
        assertTrue(save(editor1));

        IMObjectBean bean = getBean(investigationType);
        bean.setTarget("laboratory", laboratory2);
        bean.save();

        PatientInvestigationActEditor editor2 = createInvestigationEditor(act);
        checkInvalid(editor2, "Expected Laboratory " + laboratory2.getName() + " for Investigation Type "
                              + investigationType.getName() + " but Investigation has " + laboratory1.getName());
    }

    /**
     * Verifies that the laboratory on the investigation must agree with the laboratory on the device.
     */
    @Test
    public void testMismatchLaboratoryOnDevice() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Party location = TestHelper.createLocation();
        Entity laboratory1 = LaboratoryTestHelper.createLaboratory();
        Entity laboratory2 = LaboratoryTestHelper.createLaboratory();
        Entity investigationType = LaboratoryTestHelper.createInvestigationType(laboratory1);
        Entity device1 = LaboratoryTestHelper.createDevice(laboratory1);
        Entity device2 = LaboratoryTestHelper.createDevice(laboratory2);
        Entity test = LaboratoryTestHelper.createTest(investigationType, true, "YES");

        PatientInvestigationActEditor editor1 = createInvestigationEditor(act);
        editor1.setLocation(location);
        editor1.setPatient(TestHelper.createPatient());
        editor1.setInvestigationType(investigationType);
        editor1.addTest(test);
        editor1.setDevice(device1);
        assertTrue(save(editor1));

        editor1.setDevice(device2);

        PatientInvestigationActEditor editor2 = createInvestigationEditor(act);
        checkInvalid(editor2, "Device " + device2.getName() + " is not supported by " + laboratory1.getName());
    }

    /**
     * Verifies the protectedDocument flag is set for manually added documents.
     */
    @Test
    public void testProtectedDocument() {
        DocumentAct act = createAct();
        DocumentActEditor editor = createEditor(act);
        editor.getComponent();
        assertFalse(editor.getProperty(PatientInvestigationActEditor.PROTECTED_DOCUMENT).getBoolean());
        Document doc1 = createDocument();

        editor.setDocument(doc1);
        assertTrue(editor.getProperty(PatientInvestigationActEditor.PROTECTED_DOCUMENT).getBoolean());
    }

    /**
     * Verifies the order status updates to RECEIVED if a document is manually attached and the current status is SENT.
     * See OVPMS-2447.
     */
    @Test
    public void testUpdateStatusOnAttachDocument() {
        DocumentAct act1 = createAct();
        act1.setStatus2(InvestigationActStatus.SENT);
        PatientInvestigationActEditor editor1 = createEditor(act1);
        editor1.getComponent();
        Document doc1 = createDocument();
        editor1.setDocument(doc1);
        assertEquals(InvestigationActStatus.RECEIVED, editor1.getOrderStatus());

        // verify it doesn't update for statuses other than SENT
        DocumentAct act2 = createAct();
        act2.setStatus2(InvestigationActStatus.PENDING);
        PatientInvestigationActEditor editor2 = createEditor(act2);
        editor2.getComponent();
        Document doc2 = createDocument();
        editor2.setDocument(doc2);
        assertEquals(InvestigationActStatus.PENDING, editor2.getOrderStatus());
    }

    /**
     * Tests deletion.
     */
    @Test
    public void testDelete() {
        Party patient = (Party) patientFactory.createPatient();
        Entity laboratory = (Entity) laboratoryFactory.createLaboratory();
        Party location = (Party) practiceFactory.createLocation();
        Entity investigationType = (Entity) laboratoryFactory.createInvestigationType();
        DocumentAct investigation = (DocumentAct) patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .order(Order.Status.SUBMITTED.toString())
                .build();

        Act order = getBean(investigation).getTarget("order", Act.class);
        assertNotNull(order);

        ReportBuilder builder = new ReportBuilderImpl(investigation, order, getArchetypeService(), domainService,
                                                      handlers, documentRules, transactionManager);
        Report report = builder.results("1")
                .notes(RandomStringUtils.randomAlphanumeric(10000))
                .result("1")
                .analyteName("a1")
                .result(RandomStringUtils.randomAlphanumeric(10000))
                .image("foo.png", "image/png", new ByteArrayInputStream(new byte[10]))
                .notes(RandomStringUtils.randomAlphanumeric(10000))
                .add()
                .add()
                .build();

        List<Results> resultsList = report.getResults();
        assertEquals(1, resultsList.size());
        ResultsImpl results = (ResultsImpl) resultsList.get(0);
        DocumentAct resultsNoteAct = getDocumentAct(results.getAct(), "longNotes");

        List<Result> items = results.getResults();
        assertEquals(1, items.size());

        ResultImpl item = (ResultImpl) items.get(0);

        // verify DocumentActs were created for the longResult, longNotes and image nodes
        DocumentAct resultAct = getDocumentAct(item.getAct(), "longResult");
        DocumentAct resultNotesAct = getDocumentAct(item.getAct(), "longNotes");
        DocumentAct image = getDocumentAct(item.getAct(), "image");

        // now delete the investigation
        PatientInvestigationActEditor editor = createEditor(investigation);
        delete(editor);

        // verify the DocumentActs were deleted
        checkDeleted(resultsNoteAct);
        checkDeleted(resultAct);
        checkDeleted(resultNotesAct);
        checkDeleted(image);
    }

    /**
     * Verifies that the description is copied from the investigation type to the investigation.
     */
    @Test
    public void testDescriptionCopiedFromInvestigationType() {
        DocumentAct act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
        Entity type1 = (Entity) laboratoryFactory.newInvestigationType().description("type 1").build();
        Entity type2 = (Entity) laboratoryFactory.createInvestigationType();
        Entity type3 = (Entity) laboratoryFactory.newInvestigationType().description("type 3").build();
        Entity type4 = (Entity) laboratoryFactory.newInvestigationType()
                .laboratory(laboratoryFactory.createLaboratory())
                .description("type 4")
                .build();

        PatientInvestigationActEditor editor = createEditor(act);
        editor.getComponent();
        assertNull(act.getDescription());

        editor.setInvestigationType(type1);
        assertEquals("type 1", act.getDescription());

        editor.setInvestigationType(type2);
        assertNull(act.getDescription());

        editor.setInvestigationType(type3);
        assertEquals("type 3", act.getDescription());

        editor.setInvestigationType(type4);  // linked to a laboratory, so the description should be ignored
        assertNull(act.getDescription());

        // revert to type3 and verify the description is saved
        editor.setInvestigationType(type3);
        assertTrue(save(editor));

        act = get(act);
        assertEquals("type 3", act.getDescription());
    }

    /**
     * Creates a new act.
     *
     * @return a new act
     */
    protected DocumentAct createAct() {
        DocumentAct act = (DocumentAct) TestHelper.create(InvestigationArchetypes.PATIENT_INVESTIGATION);
        Entity type = LaboratoryTestHelper.createInvestigationType();
        IMObjectBean bean = getBean(act);
        bean.setTarget("investigationType", type);
        return act;
    }

    /**
     * Creates a new editor.
     *
     * @param act the act to edit
     * @return a new editor
     */
    protected PatientInvestigationActEditor createEditor(DocumentAct act) {
        DefaultLayoutContext layout = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        Context context = layout.getContext();
        context.setPatient(TestHelper.createPatient());
        context.setUser(TestHelper.createUser());
        context.setLocation(TestHelper.createLocation());
        return new PatientInvestigationActEditor(act, null, layout);
    }

    /**
     * Creates a new document.
     *
     * @return a new document
     */
    protected Document createDocument() {
        return createImage();
    }

    /**
     * Returns the document act associated with a node, verifying it has an attached document.
     *
     * @param parent the parent act
     * @param node   the node
     * @return the corresponding document act
     */
    private DocumentAct getDocumentAct(Act parent, String node) {
        DocumentAct act = getBean(parent).getTarget(node, DocumentAct.class);
        assertNotNull(act);
        assertNotNull(act.getDocument());
        return act;
    }

    /**
     * Verifies a document act and its associated document have been deleted.
     *
     * @param act the document act
     */
    private void checkDeleted(DocumentAct act) {
        assertNull(get(act));
        assertNull(get(act.getDocument()));
    }


    /**
     * Verifies a {@link Modifiable} has the expected error.
     *
     * @param modifiable the property
     * @param error      the expected error
     */
    private void checkInvalid(Modifiable modifiable, String error) {
        DefaultValidator validator = new DefaultValidator();
        assertFalse(modifiable.validate(validator));
        ValidatorError first = validator.getFirstError();
        assertNotNull(first);
        assertEquals(error, first.getMessage());
    }

    /**
     * Creates an editor for an act.
     *
     * @param act the act
     * @return the new editor
     */
    private PatientInvestigationActEditor createInvestigationEditor(DocumentAct act) {
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        PatientInvestigationActEditor editor = new PatientInvestigationActEditor(act, null, context) {
            @Override
            protected LaboratoryServices getLaboratoryServices() {
                return laboratoryServices;
            }
        };
        editor.getComponent();
        return editor;
    }

}
