/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderTestHelper;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Abstract base class for customer charge act editor tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCustomerChargeActEditorTest extends AbstractAppTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        // NOTE: need to create the practice prior to the application as it caches the practice in the context 
        practice = TestHelper.getPractice();
        practice.addClassification(TestHelper.createTaxType(BigDecimal.TEN));
        IMObjectBean bean = getBean(practice);
        bean.setValue("minimumQuantities", true);
        bean.setValue("minimumQuantitiesOverride", null);
        save(practice);
        setPharmacyOrderDiscontinuePeriod(0);
        super.setUp();
    }

    /**
     * Helper to create a product.
     *
     * @param shortName the product archetype short name
     * @return a new product
     */
    protected Product createProduct(String shortName) {
        return CustomerChargeTestHelper.createProduct(shortName);
    }

    /**
     * Returns the practice.
     *
     * @return the practice
     */
    protected Party getPractice() {
        return practice;
    }

    /**
     * Adds a charge item.
     *
     * @param editor   the editor
     * @param patient  the patient
     * @param product  the product
     * @param quantity the quantity. If {@code null}, indicates the quantity won't be changed
     * @param queue    the popup editor manager
     * @return the editor for the new item
     */
    protected CustomerChargeActItemEditor addItem(CustomerChargeActEditor editor, Party patient, Product product,
                                                  BigDecimal quantity, EditorQueue queue) {
        return CustomerChargeTestHelper.addItem(editor, patient, product, quantity, queue);
    }

    /**
     * Sets the values of a charge item.
     *
     * @param editor     the charge editor
     * @param itemEditor the charge item editor
     * @param patient    the patient
     * @param product    the product
     * @param quantity   the quantity
     * @param queue      the popup editor manager
     */
    protected void setItem(CustomerChargeActEditor editor, CustomerChargeActItemEditor itemEditor,
                           Party patient, Product product, BigDecimal quantity, EditorQueue queue) {
        CustomerChargeTestHelper.setItem(editor, itemEditor, patient, product, quantity, queue);
    }

    /**
     * Verifies a charge matches that expected.
     *
     * @param charge    the charge
     * @param customer  the expected customer
     * @param author    the expected created-by user
     * @param clinician the expected clinician
     * @param tax       the expected tax
     * @param total     the expected total
     */
    protected void checkCharge(FinancialAct charge, Party customer, User author, User clinician, BigDecimal tax,
                               BigDecimal total) {
        assertNotNull(charge);
        IMObjectBean bean = getBean(charge);
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(author.getObjectReference(), charge.getCreatedBy());
        if (bean.hasNode("clinician") && bean.getNode("clinician").getMaxCardinality() != 0) {
            // counter sales have an 0 cardinality clinician node to support querying
            checkClinician(charge, clinician);
        }
        checkEquals(tax, bean.getBigDecimal("tax"));
        checkEquals(total, bean.getBigDecimal("amount"));
    }

    /**
     * Verifies that an act has the expected clinician.
     *
     * @param act       the act to check
     * @param clinician the expected clinician. May be {@code null}
     */
    protected void checkClinician(Act act, User clinician) {
        IMObjectBean bean = getBean(act);
        Reference expected = (clinician != null) ? clinician.getObjectReference() : null;
        assertEquals(expected, bean.getTargetRef("clinician"));
    }

    /**
     * Verifies an item's properties match that expected.
     *
     * @param items           the items to search
     * @param archetype       the expected archetype
     * @param patient         the expected patient
     * @param product         the expected product
     * @param template        the expected template. May be {@code null}
     * @param group           the expected template expansion group, or {@code -1} if there should be none
     * @param author          the expected author
     * @param clinician       the expected clinician
     * @param minimumQuantity the expected minimum quantity
     * @param quantity        the expected quantity
     * @param unitCost        the expected unit cost
     * @param unitPrice       the expected unit price
     * @param fixedCost       the expected fixed cost
     * @param fixedPrice      the expected fixed price
     * @param discount        the expected discount
     * @param tax             the expected tax
     * @param total           the expected total
     * @param print           the expected print flag
     * @param event           the clinical event. May be {@code null}
     * @param childActs       the expected no. of child acts
     * @return the item
     */
    protected IMObjectBean checkItem(List<FinancialAct> items, String archetype, Party patient, Product product,
                                     Product template, int group, User author, User clinician,
                                     BigDecimal minimumQuantity, BigDecimal quantity, BigDecimal unitCost,
                                     BigDecimal unitPrice, BigDecimal fixedCost, BigDecimal fixedPrice,
                                     BigDecimal discount, BigDecimal tax, BigDecimal total, boolean print, Act event,
                                     int childActs) {
        int count = 0;
        FinancialAct item = FinancialTestHelper.find(items, patient, product, quantity);
        assertNotNull(item);
        FinancialTestHelper.checkItem(item, archetype, patient, product, template, group, author, clinician,
                                      minimumQuantity, quantity, unitCost, unitPrice, fixedCost, fixedPrice, discount,
                                      tax, total, print);
        IMObjectBean itemBean = getBean(item);
        IMObjectBean bean = getBean(product);

        if (item.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            if (product.isA(ProductArchetypes.MEDICATION)) {
                // verify there is a medication act that is linked to the event, if the quantity is positive
                if (quantity.signum() >= 0) {
                    Act medication = checkMedication(item, patient, product, author, clinician, null, null);
                    if (event != null) {
                        checkEventRelationship(event, medication);
                    }
                    ++count;
                } else {
                    // negative quantity, so there shouldn't be any product
                    assertEquals(0, itemBean.getTargets("dispensing").size());
                }
            } else {
                checkNoTargets(item, PatientArchetypes.PATIENT_MEDICATION);
            }
            Set<Entity> investigations = getInvestigationTypes(bean);
            assertEquals(investigations.size(), itemBean.getTargets("investigations", Act.class).size());
            for (Entity investigationType : investigations) {
                // verify there is an investigation for each investigation type, and it is linked to the event
                Act investigation = checkInvestigation(item, patient, investigationType, author, clinician);
                if (event != null) {
                    checkEventRelationship(event, investigation);
                }
                ++count;
            }
            List<Entity> reminderTypes = bean.getTargets("reminders", Entity.class);
            assertEquals(reminderTypes.size(), itemBean.getTargets("reminders").size());
            for (Entity reminderType : reminderTypes) {
                checkReminder(item, patient, product, reminderType, author, clinician);
                ++count;
            }
            List<Entity> alertTypes = bean.getTargets("alerts", Entity.class);
            assertEquals(alertTypes.size(), itemBean.getTargets("alerts", Act.class).size());
            for (Entity alertType : alertTypes) {
                checkAlert(item, patient, product, alertType, author, clinician);
                ++count;
            }
            List<Entity> templates = bean.getTargets("documents", Entity.class);
            assertEquals(templates.size(), itemBean.getTargets("documents").size());
            for (Entity docTemplate : templates) {
                // verify there is a document for each template, and it is linked to the event
                Act document = checkDocument(item, patient, product, docTemplate, author, clinician);
                if (event != null) {
                    checkEventRelationship(event, document);
                }
                ++count;
            }
        } else {
            // verify there are no medication, investigation, reminder nor document acts
            checkNoTargets(item, PatientArchetypes.PATIENT_MEDICATION);
            checkNoTargets(item, InvestigationArchetypes.PATIENT_INVESTIGATION);
            checkNoTargets(item, ReminderArchetypes.REMINDER);
            checkNoTargets(item, PatientArchetypes.ALERT);
            checkNoTargets(item, "act.patientDocument*");
        }
        assertEquals(childActs, count);
        return itemBean;
    }

    /**
     * Verifies that a relationship exists between an event and an act.
     *
     * @param event the event
     * @param act   the act
     */
    protected void checkEventRelationship(Act event, Act act) {
        checkEventRelationship(event, act, true);
    }

    /**
     * Verifies that a relationship exists/doesn't exist between an event and an act.
     *
     * @param event  the event
     * @param act    the act
     * @param exists if {@code true} the relationship must exist
     */
    protected void checkEventRelationship(Act event, Act act, boolean exists) {
        IMObjectBean bean = getBean(event);
        Relationship relationship;
        if (act.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            relationship = bean.getValue("chargeItems", Relationship.class, Predicates.targetEquals(act));
        } else {
            relationship = bean.getValue("items", Relationship.class, Predicates.targetEquals(act));
        }
        if (exists) {
            assertNotNull(relationship);
        } else {
            assertNull(relationship);
        }
    }

    /**
     * Verifies that an event has relationships to the specified acts, and that those relationships are the only
     * ones present.
     *
     * @param event the event
     * @param acts  the expected acts
     */
    protected void checkEventRelationships(Act event, Act... acts) {
        assertEquals(event.getSourceActRelationships().size(), acts.length);
        for (Act act : acts) {
            checkEventRelationship(event, act);
        }
    }

    /**
     * Verifies a patient medication act matches that expected.
     *
     * @param item        the charge item, linked to the medication
     * @param patient     the expected patient
     * @param product     the expected product
     * @param author      the expected author
     * @param clinician   the expected clinician. May be {@code null}
     * @param batch       the expected batch. May be {@code null}
     * @param batchExpiry the expected batch expiry date. May be {@code null}
     * @return the medication act
     */
    protected Act checkMedication(FinancialAct item, Party patient, Product product, User author, User clinician,
                                  Entity batch, Date batchExpiry) {
        IMObjectBean itemBean = getBean(item);
        List<Act> dispensing = itemBean.getTargets("dispensing", Act.class);
        assertEquals(1, dispensing.size());

        Act medication = dispensing.get(0);
        IMObjectBean bean = getBean(medication);
        assertEquals(item.getActivityStartTime(), medication.getActivityStartTime());
        if (batchExpiry == null) {
            assertNull(medication.getActivityEndTime());
        } else {
            batchExpiry = DateUtils.truncate(new Date(batchExpiry.getTime()), Calendar.SECOND); // remove millis
            assertEquals(batchExpiry, medication.getActivityEndTime());
        }
        assertTrue(bean.isA(PatientArchetypes.PATIENT_MEDICATION));
        assertEquals(product.getObjectReference(), bean.getTargetRef("product"));
        assertEquals(patient.getObjectReference(), bean.getTargetRef("patient"));
        assertEquals(author.getObjectReference(), medication.getCreatedBy());
        Reference clinicianRef = (clinician != null) ? clinician.getObjectReference() : null;
        assertEquals(clinicianRef, bean.getTargetRef("clinician"));
        checkEquals(item.getQuantity(), bean.getBigDecimal("quantity"));
        assertEquals(bean.getTargetRef("batch"), (batch != null) ? batch.getObjectReference() : null);
        return medication;
    }

    /**
     * Finds an investigation associated with a charge item, given its investigation type.
     *
     * @param item              the item
     * @param investigationType the investigation type
     * @return the corresponding investigation
     */
    protected DocumentAct getInvestigation(Act item, Entity investigationType) {
        IMObjectBean itemBean = getBean(item);
        List<Act> investigations = itemBean.getTargets("investigations", Act.class);
        for (Act investigation : investigations) {
            IMObjectBean bean = getBean(investigation);
            assertTrue(bean.isA(InvestigationArchetypes.PATIENT_INVESTIGATION));
            if (Objects.equals(bean.getTargetRef("investigationType"), investigationType.getObjectReference())) {
                return (DocumentAct) investigation;
            }
        }
        fail("Investigation not found");
        return null;
    }

    /**
     * Verifies a patient investigation act matches that expected.
     *
     * @param item              the charge item, linked to the investigation
     * @param patient           the expected patient
     * @param investigationType the expected investigation type
     * @param author            the expected author
     * @param clinician         the expected clinician
     * @return the investigation act
     */
    protected Act checkInvestigation(Act item, Party patient, Entity investigationType, User author, User clinician) {
        Act investigation = getInvestigation(item, investigationType);
        IMObjectBean bean = getBean(investigation);
        assertEquals(patient.getObjectReference(), bean.getTargetRef("patient"));
        assertEquals(investigationType.getObjectReference(), bean.getTargetRef("investigationType"));
        assertEquals(author.getObjectReference(), investigation.getCreatedBy());
        assertEquals(clinician.getObjectReference(), bean.getTargetRef("clinician"));
        return investigation;
    }

    /**
     * Finds a reminder associated with a charge item, given its reminder type.
     *
     * @param item         the item
     * @param reminderType the reminder type
     * @return the corresponding reminder
     */
    protected Act getReminder(Act item, Entity reminderType) {
        IMObjectBean itemBean = getBean(item);
        List<Act> reminders = itemBean.getTargets("reminders", Act.class);
        for (Act reminder : reminders) {
            IMObjectBean bean = getBean(reminder);
            assertTrue(bean.isA(ReminderArchetypes.REMINDER));
            if (Objects.equals(bean.getTargetRef("reminderType"), reminderType.getObjectReference())) {
                return reminder;
            }
        }
        fail("Reminder not found");
        return null;
    }

    /**
     * Verifies a patient reminder act matches that expected.
     *
     * @param item         the charge item, linked to the reminder
     * @param patient      the expected patient
     * @param product      the expected product
     * @param reminderType the expected reminder type
     * @param author       the expected author
     * @param clinician    the expected clinician. May be {@code null}
     * @return the reminder act
     */
    protected Act checkReminder(Act item, Party patient, Product product, Entity reminderType, User author,
                                User clinician) {
        Act reminder = getReminder(item, reminderType);
        IMObjectBean productBean = getBean(product);

        ReminderRules rules = ServiceHelper.getBean(ReminderRules.class);
        List<Relationship> rels = productBean.getValues("reminders", Relationship.class,
                                                        Predicates.targetEquals(reminderType));
        assertEquals(1, rels.size());
        IMObjectBean bean = getBean(reminder);
        Date initialTime = DateUtils.truncate(bean.getDate("initialTime"), Calendar.SECOND); // details nodes store ms
        assertEquals(0, DateRules.compareTo(item.getActivityStartTime(), initialTime));
        Date dueDate = rules.calculateProductReminderDueDate(item.getActivityStartTime(), rels.get(0));
        assertEquals(0, DateRules.compareTo(reminder.getActivityEndTime(), dueDate));
        assertEquals(product.getObjectReference(), bean.getTargetRef("product"));
        assertEquals(patient.getObjectReference(), bean.getTargetRef("patient"));
        assertEquals(reminderType.getObjectReference(), bean.getTargetRef("reminderType"));
        assertEquals(author.getObjectReference(), reminder.getCreatedBy());
        if (clinician != null) {
            assertEquals(clinician.getObjectReference(), bean.getTargetRef("clinician"));
        } else {
            assertNull(bean.getTargetRef("clinician"));
        }
        return reminder;
    }

    /**
     * Finds an alert associated with a charge item, given its alert type.
     *
     * @param item      the item
     * @param alertType the alert type
     * @return the corresponding alert
     */
    protected Act getAlert(Act item, Entity alertType) {
        IMObjectBean itemBean = getBean(item);
        List<Act> alerts = itemBean.getTargets("alerts", Act.class);
        for (Act alert : alerts) {
            IMObjectBean bean = getBean(alert);
            assertTrue(bean.isA(PatientArchetypes.ALERT));
            if (Objects.equals(bean.getTargetRef("alertType"), alertType.getObjectReference())) {
                return alert;
            }
        }
        fail("Alert not found");
        return null;
    }

    /**
     * Verifies a patient alert act matches that expected.
     *
     * @param item      the charge item, linked to the alert
     * @param patient   the expected patient
     * @param product   the expected product
     * @param alertType the expected alert type
     * @param author    the expected author
     * @param clinician the expected clinician
     * @return the alert act
     */
    protected Act checkAlert(Act item, Party patient, Product product, Entity alertType, User author, User clinician) {
        Act alert = getAlert(item, alertType);
        IMObjectBean bean = getBean(alert);
        assertEquals(item.getActivityStartTime(), alert.getActivityStartTime());
        IMObjectBean alertBean = getBean(alertType);
        if (alertBean.getString("durationUnits") != null) {
            int duration = alertBean.getInt("duration");
            DateUnits units = DateUnits.fromString(alertBean.getString("durationUnits"));
            Date endTime = DateRules.getDate(item.getActivityStartTime(), duration, units);
            assertEquals(endTime, alert.getActivityEndTime());
        }
        assertEquals(product.getObjectReference(), bean.getTargetRef("product"));
        assertEquals(patient.getObjectReference(), bean.getTargetRef("patient"));
        assertEquals(alertType.getObjectReference(), bean.getTargetRef("alertType"));
        assertEquals(author.getObjectReference(), alert.getCreatedBy());
        assertEquals(clinician.getObjectReference(), bean.getTargetRef("clinician"));
        return alert;
    }

    /**
     * Finds a document associated with a charge item, given its document template.
     *
     * @param item     the item
     * @param template the document template
     * @return the corresponding reminder
     */
    protected DocumentAct getDocument(Act item, Entity template) {
        IMObjectBean itemBean = getBean(item);
        List<DocumentAct> documents = itemBean.getTargets("documents", DocumentAct.class);
        for (DocumentAct document : documents) {
            IMObjectBean bean = getBean(document);
            assertTrue(bean.isA("act.patientDocument*"));
            if (Objects.equals(bean.getTargetRef("documentTemplate"), template.getObjectReference())) {
                return document;
            }
        }
        fail("Document not found");
        return null;
    }

    /**
     * Verifies a document act matches that expected.
     *
     * @param item      the charge item, linked to the document act
     * @param patient   the expected patient
     * @param product   the expected product
     * @param template  the expected document template
     * @param author    the expected author
     * @param clinician the expected clinician. May be {@code null}
     * @return the document act
     */
    protected DocumentAct checkDocument(Act item, Party patient, Product product, Entity template, User author,
                                        User clinician) {
        DocumentAct document = getDocument(item, template);
        DocumentTemplate documentTemplate = new DocumentTemplate(template, getArchetypeService());
        IMObjectBean bean = getBean(document);
        assertTrue(bean.isA(documentTemplate.getType()));
        assertEquals(product.getObjectReference(), bean.getTargetRef("product"));
        assertEquals(patient.getObjectReference(), bean.getTargetRef("patient"));
        assertEquals(template.getObjectReference(), bean.getTargetRef("documentTemplate"));
        assertEquals(author.getObjectReference(), document.getCreatedBy());
        if (clinician != null) {
            assertEquals(clinician.getObjectReference(), bean.getTargetRef("clinician"));
        } else {
            assertNull(bean.getTargetRef("clinician"));
        }
        return document;
    }

    /**
     * Verifies a document act matches that expected.
     *
     * @param item        the charge item, linked to the document act
     * @param patient     the expected patient
     * @param product     the expected product
     * @param template    the expected document template
     * @param author      the expected author
     * @param clinician   the expected clinician. May be {@code null}
     * @param hasDocument if {@code true}, expect a document to be present
     * @return the document act
     */
    protected DocumentAct checkDocument(Act item, Party patient, Product product, Entity template, User author,
                                        User clinician, boolean hasDocument) {
        DocumentAct act = checkDocument(item, patient, product, template, author, clinician);
        if (hasDocument) {
            assertNotNull(act.getDocument());
        } else {
            assertNull(act.getDocument());
        }
        return act;
    }

    /**
     * Verifies that an event linked to a charge has one instance of the expected note.
     *
     * @param chargeItem the charge item, linked to an event
     * @param patient    the expected patient
     * @param note       the expected note
     */
    protected void checkChargeEventNote(IMObjectBean chargeItem, Party patient, String note) {
        Act event = getEvent(chargeItem);
        checkEventNote(event, patient, note);
    }

    /**
     * Verifies that an event has one instance of the expected note.
     *
     * @param event   the event
     * @param patient the expected patient
     * @param note    the expected note
     */
    protected void checkEventNote(Act event, Party patient, String note) {
        int found = 0;
        IMObjectBean eventBean = getBean(event);
        assertEquals(patient, eventBean.getTarget("patient"));
        for (Act item : eventBean.getTargets("items", Act.class)) {
            if (TypeHelper.isA(item, PatientArchetypes.CLINICAL_NOTE)) {
                IMObjectBean bean = getBean(item);
                assertEquals(patient, bean.getTarget("patient"));
                if (StringUtils.equals(note, bean.getString("note"))) {
                    found++;
                }
            }
        }
        assertEquals(1, found);
    }

    /**
     * Returns an event linked to a charge item.
     *
     * @param item the charge item
     * @return the corresponding event
     */
    protected Act getEvent(IMObjectBean item) {
        Act event = item.getSource("event", Act.class);
        assertNotNull(event);
        return event;
    }

    /**
     * Helper to create a product.
     *
     * @param shortName  the product archetype short name
     * @param fixedPrice the fixed price
     * @return a new product
     */
    protected Product createProduct(String shortName, BigDecimal fixedPrice) {
        return CustomerChargeTestHelper.createProduct(shortName, fixedPrice);
    }

    /**
     * Helper to create a product.
     *
     * @param shortName  the product archetype short name
     * @param fixedPrice the fixed price
     * @param pharmacy   the pharmacy
     * @return a new product
     */
    protected Product createProduct(String shortName, BigDecimal fixedPrice, Entity pharmacy) {
        Product product = CustomerChargeTestHelper.createProduct(shortName, fixedPrice);
        IMObjectBean bean = getBean(product);
        bean.setTarget("pharmacy", pharmacy);
        bean.save();

        return product;
    }

    /**
     * Helper to create a product.
     *
     * @param shortName  the product archetype short name
     * @param fixedPrice the fixed price, tax-exclusive
     * @param unitPrice  the unit price, tax-exclusive
     * @return a new product
     */
    protected Product createProduct(String shortName, BigDecimal fixedPrice, BigDecimal unitPrice) {
        return CustomerChargeTestHelper.createProduct(shortName, BigDecimal.ZERO, fixedPrice, BigDecimal.ZERO,
                                                      unitPrice);
    }

    /**
     * Helper to create a product.
     *
     * @param shortName  the product archetype short name
     * @param fixedCost  the fixed cost
     * @param fixedPrice the fixed price, tax-exclusive
     * @param unitCost   the unit cost
     * @param unitPrice  the unit price, tax-exclusive
     * @return a new product
     */
    protected Product createProduct(String shortName, BigDecimal fixedCost, BigDecimal fixedPrice, BigDecimal unitCost,
                                    BigDecimal unitPrice) {
        return CustomerChargeTestHelper.createProduct(shortName, fixedCost, fixedPrice, unitCost, unitPrice);
    }

    /**
     * Adds a test to a product.
     *
     * @param product           the product
     * @param investigationType the investigation type
     * @return the test
     */
    protected Entity addTest(Product product, Entity investigationType) {
        Entity test = LaboratoryTestHelper.createTest(investigationType);
        IMObjectBean productBean = getBean(product);
        productBean.addTarget("tests", test);
        productBean.save();
        return test;
    }

    /**
     * Adds an HL7 laboratory test to a product.
     *
     * @param product the product
     * @return the laboratory test
     */
    protected Entity addHL7Test(Product product) {
        return addHL7Test(product, LaboratoryTestHelper.createInvestigationType());
    }

    /**
     * Adds an HL7 laboratory test to a product.
     *
     * @param product           the product
     * @param investigationType the investigation type
     * @return the laboratory test
     */
    protected Entity addHL7Test(Product product, Entity investigationType) {
        Entity test = LaboratoryTestHelper.createHL7Test(investigationType);
        IMObjectBean productBean = getBean(product);
        productBean.addTarget("tests", test);
        productBean.save();
        return test;
    }

    /**
     * Adds a document template to a product that creates an <em>act.patientDocumentForm</em>.
     *
     * @param product the product
     * @return the document template
     */
    protected Entity addTemplate(Product product) {
        return addTemplate(product, PatientArchetypes.DOCUMENT_FORM);
    }

    /**
     * Adds a document template to a product.
     *
     * @param product the product
     * @param type    the document template type
     * @return the document template
     */
    protected Entity addTemplate(Product product, String type) {
        return addTemplate(product, type, false);
    }

    /**
     * Adds a document template to a product.
     *
     * @param product    the product
     * @param type       the document template type
     * @param parameters if {@code true}, create a template that takes parameters
     * @return the document template
     */
    protected Entity addTemplate(Product product, String type, boolean parameters) {
        TestDocumentTemplateBuilder builder = documentFactory.newTemplate().type(type);
        if (parameters) {
            builder.documentWithParameters();
        } else {
            builder.blankDocument();
        }
        IMObjectBean productBean = getBean(product);
        Entity template = (Entity) builder.build();
        productBean.addTarget("documents", template);
        productBean.save();
        return template;
    }

    /**
     * Adds an interactive reminder type to a product.
     *
     * @param product the product
     * @return the reminder type
     */
    protected Entity addReminder(Product product) {
        Entity reminderType = ReminderTestHelper.createReminderType();
        addReminder(product, reminderType);
        return reminderType;
    }

    /**
     * Adds an interactive reminder type to a product.
     *
     * @param product      the product
     * @param reminderType the reminder type
     */
    protected void addReminder(Product product, Entity reminderType) {
        IMObjectBean productBean = getBean(product);
        Relationship rel = productBean.addTarget("reminders", reminderType);
        IMObjectBean relBean = getBean(rel);
        relBean.setValue("interactive", true);
        productBean.save();
    }

    /**
     * Adds an interactive alert type to a product.
     *
     * @param product the product
     * @return the alert type
     */
    protected Entity addAlertType(Product product) {
        Entity alertType = ReminderTestHelper.createAlertType("Z Test Alert", null, 1, DateUnits.YEARS, true);
        addAlertType(product, alertType);
        return alertType;
    }

    /**
     * Adds an alert type to a product.
     *
     * @param product   the product
     * @param alertType the alert type
     */
    protected void addAlertType(Product product, Entity alertType) {
        IMObjectBean bean = getBean(product);
        bean.addTarget("alerts", alertType);
        bean.save();
    }

    /**
     * Adds a tax exemption to a customer.
     *
     * @param customer the customer
     */
    protected void addTaxExemption(Party customer) {
        IMObjectBean bean = getBean(getPractice());
        List<Lookup> taxes = bean.getValues("taxes", Lookup.class);
        assertEquals(1, taxes.size());
        customer.addClassification(taxes.get(0));
        save(customer);
    }

    /**
     * Adds a discount to an entity.
     *
     * @param entity   the entity to add to
     * @param discount the discount
     */
    protected void addDiscount(org.openvpms.component.model.entity.Entity entity,
                               org.openvpms.component.model.entity.Entity discount) {
        IMObjectBean bean = getBean(entity);
        bean.addTarget("discounts", discount);
        bean.save();
    }

    /**
     * Sets the pharmacy order discontinue period.
     *
     * @param period the period, in minutes
     */
    protected void setPharmacyOrderDiscontinuePeriod(int period) {
        IMObjectBean bean = getBean(practice);
        bean.setValue("pharmacyOrderDiscontinuePeriod", period);
        bean.setValue("pharmacyOrderDiscontinuePeriodUnits", DateUnits.MINUTES.toString());
        save(practice);
    }

    /**
     * Saves an editor.
     * <p/>
     * This processes any documents, as per the {@link CustomerChargeActEditDialog}.
     *
     * @param editor the editor
     */
    protected void save(CustomerChargeActEditor editor) {
        assertTrue(SaveHelper.save(editor));
        editor.getDocumentManager().process(editor.getEditorQueue());
    }

    /**
     * Returns the unique investigation types for each test linked to a product.
     *
     * @param bean the product bean
     * @return the investigation types
     */
    private Set<Entity> getInvestigationTypes(IMObjectBean bean) {
        Set<Entity> result = new HashSet<>();
        for (Entity test : bean.getTargets("tests", Entity.class)) {
            IMObjectBean testBean = getBean(test);
            result.add(testBean.getTarget("investigationType", Entity.class));
        }
        return result;
    }

    /**
     * Verifies an act has no relationships with the specified target.
     *
     * @param act             the act
     * @param targetArchetype the target archetype
     */
    private void checkNoTargets(Act act, String targetArchetype) {
        ActRelationship match = act.getActRelationships().stream().filter(Predicates.targetIsA(targetArchetype))
                .findFirst().orElse(null);
        assertNull(match);
    }

}
