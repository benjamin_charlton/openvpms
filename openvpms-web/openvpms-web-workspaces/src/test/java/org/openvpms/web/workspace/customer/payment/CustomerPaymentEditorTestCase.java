/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link CustomerPaymentEditor}.
 *
 * @author Tim Anderson
 */
public class CustomerPaymentEditorTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies the till cannot be changed if there is an outstanding or approved EFTPOS transaction.
     */
    @Test
    public void testCannotChangeTill() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = (Entity) practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSPayment().customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();

        FinancialAct item = (FinancialAct) accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);

        FinancialAct payment = (FinancialAct) accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .add(item)
                .build();

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        CustomerPaymentEditor editor = new CustomerPaymentEditor(payment, null, context);
        editor.getComponent();

        editor.setTill((Entity) practiceFactory.createTill());
    }

    /**
     * Verifies that {@link CustomerPaymentEditor#makeSaveableAndPostOnCompletion()} sets the status to
     * {@link FinancialActStatus#IN_PROGRESS} if the act hasn't been saved {@link FinancialActStatus#POSTED}.
     */
    @Test
    public void testMakeSaveableAndPostOnCompletion() {
        FinancialAct payment = (FinancialAct) accountFactory.newPayment()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .status(FinancialActStatus.POSTED)
                .build(false);

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        CustomerPaymentEditor editor = new CustomerPaymentEditor(payment, null, context);
        editor.getComponent();

        assertEquals(FinancialActStatus.POSTED, editor.getObject().getStatus());
        assertFalse(editor.postOnCompletion());
        assertTrue(editor.canChangeStatus());

        editor.makeSaveableAndPostOnCompletion();
        assertEquals(FinancialActStatus.IN_PROGRESS, editor.getObject().getStatus());
        assertTrue(editor.postOnCompletion());
        assertTrue(editor.canChangeStatus());
    }

    /**
     * Verifies that {@link CustomerPaymentEditor#makeSaveableAndPostOnCompletion()} throws
     * {@code IllegalStateException} if the act has been saved and is POSTED.
     */
    @Test
    public void testMakeSaveableAndPostOnCompletionForPostedTranaction() {
        FinancialAct payment = (FinancialAct) accountFactory.newPayment()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .cash().amount(TEN).add()
                .status(ActStatus.POSTED)
                .build();

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        CustomerPaymentEditor editor = new CustomerPaymentEditor(payment, null, context);
        editor.getComponent();
        assertTrue(editor.isPosted());
        assertFalse(editor.canChangeStatus());
        try {
            editor.makeSaveableAndPostOnCompletion();
            fail("Expected makeSaveableAndPostOnCompletion to throw IllegalStateException");
        } catch (IllegalStateException expected) {
            // no-op
        }
    }
}
