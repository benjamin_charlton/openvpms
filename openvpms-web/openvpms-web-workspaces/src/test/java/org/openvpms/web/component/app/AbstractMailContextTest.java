/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.openvpms.component.model.party.Contact;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.test.AbstractAppTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base class for {@link MailContext} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMailContextTest extends AbstractAppTest {

    /**
     * Verifies addresses match those expected.
     *
     * @param addresses         the addresses to check
     * @param expectedPreferred the expected preferred email address. May be {@code null}
     * @param expected          the expected email addresses
     */
    protected void checkAddresses(MailContext.Addresses addresses, Contact expectedPreferred, Contact... expected) {
        assertEquals(expectedPreferred, addresses.getPreferred());
        assertEquals(expected.length, addresses.getContacts().size());
        for (Contact contact : expected) {
            assertTrue(addresses.getContacts().contains(contact));
        }
    }
}