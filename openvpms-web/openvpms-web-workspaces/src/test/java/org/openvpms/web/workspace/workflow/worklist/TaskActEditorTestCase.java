/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.workflow.worklist;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.EntityRelationship;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.helper.EntityBean;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link TaskActEditor} class.
 *
 * @author Tim Anderson
 */
public class TaskActEditorTestCase extends AbstractAppTest {

    /**
     * The work list.
     */
    private Party worklist;

    /**
     * The task type.
     */
    private Entity taskType;

    /**
     * The uaer to populate the author node with.
     */
    private User user;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        worklist = ScheduleTestHelper.createWorkList();
        taskType = ScheduleTestHelper.createTaskType();
        user = TestHelper.createClinician();

        EntityBean bean = new EntityBean(worklist);
        EntityRelationship relationship = bean.addNodeRelationship("taskTypes", taskType);
        IMObjectBean relBean = new IMObjectBean(relationship);
        relBean.setValue("noSlots", 100);
        save(taskType, worklist);
    }

    /**
     * Verifies that a task editor can be created and saved when mandatory fields are populated.
     */
    @Test
    public void testSave() {
        LayoutContext layout = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        Act act = (Act) create(ScheduleArchetypes.TASK);

        TaskActEditor editor = new TaskActEditor(act, null, layout);
        editor.getComponent();
        assertFalse(editor.isValid());
        editor.setCustomer(TestHelper.createCustomer());
        editor.setWorkList(worklist);
        assertFalse(editor.isValid());
        editor.setTaskType(taskType);
        assertFalse(editor.isValid());
        assertFalse(editor.isValid());

        editor.setStartTime(new Date());
        assertTrue(editor.isValid());  // should now be valid

        assertTrue(SaveHelper.save(editor));
    }

    /**
     * Verifies that the end time cannot be set prior to the start time.
     */
    @Test
    public void testTimes() {
        TaskActEditor editor = createEditor();
        editor.getComponent();

        Date start = DateRules.getToday();
        editor.setStartTime(start);
        Date end = DateRules.getDate(start, -1, DateUnits.MINUTES);
        editor.setEndTime(end);
        assertEquals(editor.getEndTime(), start);
        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));
    }

    /**
     * Tests inheritance of the work list, customer and patient from the context.
     */
    @Test
    public void testInheritance() {
        Party customer1 = TestHelper.createCustomer();
        Party customer2 = TestHelper.createCustomer();
        Party patient = TestHelper.createPatient(customer2);
        Party worklist2 = ScheduleTestHelper.createWorkList();

        LocalContext context = new LocalContext();
        context.setCustomer(customer1);
        context.setWorkList(worklist);
        context.setUser(user);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        // verify the task inherits customer1 and worklist
        Act act1 = (Act) create(ScheduleArchetypes.TASK);
        TaskActEditor editor1 = new TaskActEditor(act1, null, layout);
        editor1.getComponent();
        editor1.setStartTime(new Date());
        assertEquals(customer1, editor1.getCustomer());
        assertEquals(worklist, editor1.getWorkList());
        assertNull(editor1.getPatient());
        editor1.setTaskType(taskType);

        // now save it. It should not inherit anything next time round is it isn't new
        assertTrue(SaveHelper.save(editor1));

        context.setCustomer(customer2);
        context.setPatient(patient);
        context.setWorkList(worklist2);

        TaskActEditor editor2 = new TaskActEditor(act1, null, layout);
        editor2.getComponent();

        // verify customer and worklist unchanged, and patient not inherited
        assertEquals(customer1, editor2.getCustomer());
        assertEquals(worklist, editor2.getWorkList());
        assertNull(editor2.getPatient());
    }

    /**
     * Creates a new editor, pre-populating the customer, worklist, and user.
     *
     * @return a new editor
     */
    private TaskActEditor createEditor() {
        Context context = new LocalContext();
        Party customer = TestHelper.createCustomer();

        // populate the context. These will be used to initialise the task
        context.setCustomer(customer);
        context.setWorkList(worklist);
        context.setUser(user);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Act act = (Act) create(ScheduleArchetypes.TASK);

        return new TaskActEditor(act, null, layout);
    }


}
