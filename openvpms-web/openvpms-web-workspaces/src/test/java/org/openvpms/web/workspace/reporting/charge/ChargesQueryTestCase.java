/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge;

import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.reporting.charge.search.ChargesQuery;

/**
 * Tests the {@link ChargesQuery} class.
 *
 * @author Tim Anderson
 */
public class ChargesQueryTestCase extends AbstractChargesQueryTest {

    /**
     * Constructs a {@link ChargesQueryTestCase}.
     */
    public ChargesQueryTestCase() {
        super(true);
    }

    /**
     * Creates a new query.
     *
     * @param context the context
     * @return a new query
     */
    @Override
    protected AbstractChargesQuery createQuery(Context context) {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        return new ChargesQuery(layout);
    }
}