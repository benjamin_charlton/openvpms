/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.Deletable;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;

/**
 * Tests the {@link PaymentDeletionHandler}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PaymentDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules rules;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test till.
     */
    private Entity till;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        customer = customerFactory.createCustomer();
        till = practiceFactory.createTill();
    }

    /**
     * Verify non-POSTED payments and refunds can be deleted.
     */
    @Test
    public void testDelete() {
        FinancialAct payment = createPayment(IN_PROGRESS);
        FinancialAct refund = createRefund(IN_PROGRESS);
        checkDelete(payment);
        checkDelete(refund);
    }

    /**
     * Verify POSTED payments and refunds can't be deleted.
     */
    @Test
    public void testDeletePosted() {
        FinancialAct payment = createPayment(IN_PROGRESS);
        FinancialAct refund = createRefund(IN_PROGRESS);
        checkDeletePosted(payment);
        checkDeletePosted(refund);
    }

    /**
     * Verifies payments and refunds with outstanding EFTPOS transactions (i.e. not ERROR, ACCEPTED, or DECLINED) cannot
     * be deleted.
     */
    @Test
    public void testDeleteWithOutstandingEFT() {
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct payment = accountFactory.newPayment()
                .status(IN_PROGRESS)
                .till(practiceFactory.createTill())
                .customer(customer)
                .location(practiceFactory.createLocation())
                .eft()
                .amount(BigDecimal.TEN)
                .addTransaction(EFTPOSTransactionStatus.ERROR, terminal)
                .addTransaction(EFTPOSTransactionStatus.DECLINED, terminal)
                .addTransaction(EFTPOSTransactionStatus.IN_PROGRESS, terminal)
                .add()
                .build();

        FinancialAct refund = accountFactory.newRefund()
                .status(IN_PROGRESS)
                .till(practiceFactory.createTill())
                .customer(customer)
                .location(practiceFactory.createLocation())
                .eft()
                .amount(BigDecimal.TEN)
                .addTransaction(EFTPOSTransactionStatus.ERROR, terminal)
                .addTransaction(EFTPOSTransactionStatus.DECLINED, terminal)
                .addTransaction(EFTPOSTransactionStatus.IN_PROGRESS, terminal)
                .add()
                .build();

        checkDeleteWithOutstandingEFT(payment);
        checkDeleteWithOutstandingEFT(refund);
    }

    /**
     * Verifies payments and refunds with APPROVED EFTPOS transactions cannot be deleted.
     */
    @Test
    public void testDeleteWithApprovedEFT() {
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct payment = accountFactory.newPayment()
                .status(IN_PROGRESS)
                .till(practiceFactory.createTill())
                .customer(customer)
                .location(practiceFactory.createLocation())
                .eft()
                .amount(BigDecimal.TEN)
                .addTransaction(EFTPOSTransactionStatus.ERROR, terminal)
                .addTransaction(EFTPOSTransactionStatus.DECLINED, terminal)
                .addTransaction(EFTPOSTransactionStatus.APPROVED, terminal)
                .add()
                .build();

        FinancialAct refund = accountFactory.newRefund()
                .status(IN_PROGRESS)
                .till(practiceFactory.createTill())
                .customer(customer)
                .location(practiceFactory.createLocation())
                .eft()
                .amount(BigDecimal.TEN)
                .addTransaction(EFTPOSTransactionStatus.ERROR, terminal)
                .addTransaction(EFTPOSTransactionStatus.DECLINED, terminal)
                .addTransaction(EFTPOSTransactionStatus.APPROVED, terminal)
                .add()
                .build();

        checkDeleteWithApprovedEFT(payment);
        checkDeleteWithApprovedEFT(refund);
    }

    /**
     * Verifies payments and refunds with NO_TERMINAL EFTPOS transactions can be deleted.
     * <p/>
     */
    @Test
    public void testDeleteWithNoTerminalEFT() {
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct payment = accountFactory.newPayment()
                .status(IN_PROGRESS)
                .till(practiceFactory.createTill())
                .customer(customer)
                .location(practiceFactory.createLocation())
                .eft()
                .amount(BigDecimal.TEN)
                .addTransaction(EFTPOSTransactionStatus.ERROR, terminal)
                .addTransaction(EFTPOSTransactionStatus.DECLINED, terminal)
                .addTransaction(EFTPOSTransactionStatus.NO_TERMINAL, terminal)
                .add()
                .build();
        List<FinancialAct> paymentEFT = getEFTTransactions(payment);
        assertEquals(3, paymentEFT.size());

        FinancialAct refund = accountFactory.newRefund()
                .status(IN_PROGRESS)
                .till(practiceFactory.createTill())
                .customer(customer)
                .location(practiceFactory.createLocation())
                .eft()
                .amount(BigDecimal.TEN)
                .addTransaction(EFTPOSTransactionStatus.ERROR, terminal)
                .addTransaction(EFTPOSTransactionStatus.DECLINED, terminal)
                .addTransaction(EFTPOSTransactionStatus.NO_TERMINAL, terminal)
                .add()
                .build();
        List<FinancialAct> refundEFT = getEFTTransactions(refund);
        assertEquals(3, refundEFT.size());

        checkDeleteWithNoTerminalEFT(payment, paymentEFT);
        checkDeleteWithNoTerminalEFT(refund, refundEFT);
    }

    /**
     * Verify payments and refunds can't be deactivated.
     */
    @Test
    public void testDeactivate() {
        FinancialAct payment = createPayment(IN_PROGRESS);
        FinancialAct refund = createPayment(IN_PROGRESS);
        checkDeactivate(payment);
        checkDeactivate(refund);
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link PaymentDeletionHandler} for payments
     * and refunds.
     */
    @Test
    public void testFactory() {
        FinancialAct payment = createPayment(ActStatus.POSTED);
        FinancialAct refund = createRefund(POSTED);
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        assertTrue(factory.create(payment) instanceof PaymentDeletionHandler);
        assertTrue(factory.create(refund) instanceof PaymentDeletionHandler);
    }

    /**
     * Creates a new deletion handler for a payment/refund.
     *
     * @param act the payment/refund
     * @return a new deletion handler
     */
    protected PaymentDeletionHandler createDeletionHandler(FinancialAct act) {
        return new PaymentDeletionHandler(act, factory, ServiceHelper.getTransactionManager(),
                                          ServiceHelper.getArchetypeService(), rules);
    }

    /**
     * Verifies payments and refunds with outstanding EFTPOS transactions (i.e. not ERROR, ACCEPTED, or DECLINED) cannot
     * be deleted.
     *
     * @param act the payment/refund
     */
    private void checkDeleteWithOutstandingEFT(FinancialAct act) {
        PaymentDeletionHandler handler = createDeletionHandler(act);
        Deletable deletable = handler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals("Cannot delete " + getDisplayName(act) + ". It has an outstanding EFTPOS transaction.",
                     deletable.getReason());
        try {
            handler.delete(new LocalContext(), new HelpContext("foo", null));
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // do nothing
        }
        assertNotNull(get(act)); // verify it hasn't been removed
    }


    /**
     * Verifies payments and refunds with approved EFTPOS transactions cannot be deleted.
     *
     * @param act the payment/refund
     */
    private void checkDeleteWithApprovedEFT(FinancialAct act) {
        PaymentDeletionHandler handler = createDeletionHandler(act);
        Deletable deletable = handler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals("Cannot delete " + getDisplayName(act) + ". It has an approved EFTPOS transaction.",
                     deletable.getReason());
        try {
            handler.delete(new LocalContext(), new HelpContext("foo", null));
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // do nothing
        }
        assertNotNull(get(act)); // verify it hasn't been removed
    }

    /**
     * Verifies payments and refunds with No Terminal EFTPOS transactions can be deleted.
     *
     * @param act the payment/refund
     * @param eft the EFT transactions
     */
    private void checkDeleteWithNoTerminalEFT(FinancialAct act, List<FinancialAct> eft) {
        PaymentDeletionHandler handler = createDeletionHandler(act);
        Deletable deletable = handler.getDeletable();
        assertTrue(deletable.canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(act)); // verify it has been removed
        for (FinancialAct transaction : eft) {
            if (EFTPOSTransactionStatus.NO_TERMINAL.equals(transaction.getStatus())) {
                assertNull(get(transaction));
            } else {
                assertNotNull(get(transaction));
            }
        }
    }

    /**
     * Verify non-POSTED payments and refunds can be deleted.
     *
     * @param act the payment/refund
     */
    private void checkDelete(FinancialAct act) {
        act.setStatus(IN_PROGRESS);
        save(act);
        PaymentDeletionHandler handler = createDeletionHandler(act);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(act)); // verify it has been removed
    }

    /**
     * Verify POSTED payments and refunds can't be deleted.
     *
     * @param act the payment/refund
     */
    private void checkDeletePosted(FinancialAct act) {
        act.setStatus(POSTED);
        save(act);

        PaymentDeletionHandler handler = createDeletionHandler(act);
        Deletable deletable = handler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals("Cannot delete " + getDisplayName(act) + ". It has been finalised.", deletable.getReason());

        // verify delete throws IllegalStateException
        try {
            handler.delete(new LocalContext(), new HelpContext("foo", null));
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // do nothing
        }

        // verify nothing was deleted
        assertNotNull(get(act));
    }

    /**
     * Verifies payments and refunds can't be deactivated.
     *
     * @param act the payment/refund
     */
    private void checkDeactivate(FinancialAct act) {
        assertEquals(IN_PROGRESS, act.getStatus());

        // verify non-POSTED acts can't be deactivated.
        PaymentDeletionHandler handler1 = createDeletionHandler(act);
        assertFalse(handler1.canDeactivate());

        // verify deactivate throws IllegalStateException
        try {
            handler1.deactivate();
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // do nothing
        }

        // verify POSTED acts can't be deactivated.
        act.setStatus(ActStatus.POSTED);
        PaymentDeletionHandler handler2 = createDeletionHandler(act);
        assertFalse(handler2.canDeactivate());

        // verify deactivate throws IllegalStateException
        try {
            handler2.deactivate();
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // do nothing
        }
    }

    /**
     * Creates a new payment.
     *
     * @param status the payment status
     * @return a new payment
     */
    private FinancialAct createPayment(String status) {
        return accountFactory.newPayment()
                .customer(customer)
                .till(till)
                .status(status)
                .eft()
                .amount(100)
                .add()
                .build();
    }

    /**
     * Creates a new refund.
     *
     * @param status the refund status
     * @return a new refund
     */
    private FinancialAct createRefund(String status) {
        return accountFactory.newRefund()
                .customer(customer)
                .till(till)
                .status(status)
                .eft()
                .amount(100)
                .add()
                .build();
    }

    /**
     * Returns all EFT transactions associated with a payment or refund.
     *
     * @param act the payment/refund
     * @return the EFT transactions
     */
    private List<FinancialAct> getEFTTransactions(FinancialAct act) {
        List<FinancialAct> result = new ArrayList<>();
        for (FinancialAct item : getBean(act).getTargets("items", FinancialAct.class)) {
            result.addAll(getBean(item).getTargets("eft", FinancialAct.class));
        }
        return result;
    }
}
