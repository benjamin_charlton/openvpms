/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import nextapp.echo2.app.ApplicationInstance;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.statement.AbstractStatementTest;
import org.openvpms.archetype.rules.finance.statement.Statement;
import org.openvpms.archetype.rules.finance.statement.StatementProcessor;
import org.openvpms.archetype.rules.finance.statement.StatementRules;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.report.ReportFactory;
import org.openvpms.report.openoffice.Converter;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.app.PracticeMailContext;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.mail.DefaultMailer;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.Mailer;
import org.openvpms.web.component.mail.MailerFactory;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.OpenVPMSApp;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;


/**
 * Tests the {@link StatementEmailProcessor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class StatementEmailProcessorTestCase extends AbstractStatementTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Statement document template.
     */
    private Entity statementTemplate;

    /**
     * The app.
     */
    @Autowired
    private OpenVPMSApp app;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        app.setApplicationContext(applicationContext);
        ApplicationInstance.setActive(app);
        app.doInit();

        Party practice = getPractice();
        practice.addContact(TestHelper.createEmailContact("foo@bar.com"));
        Entity settings = (Entity) create("entity.mailServer");
        IMObjectBean bean = getBean(settings);
        bean.setValue("name", "Default Mail Server");
        bean.setValue("host", "localhost");
        bean.save();

        IMObjectBean practiceBean = getBean(practice);
        practiceBean.setTarget("mailServer", settings);

        statementTemplate = (Entity) documentFactory.newTemplate()
                .name("Statement")
                .type(CustomerAccountArchetypes.OPENING_BALANCE)
                .blankDocument()
                .build();
        IMObjectBean templateBean = getBean(statementTemplate);
        Entity emailTemplate = (Entity) documentFactory.newEmailTemplate().subject("Statement Email")
                .content("Statement Text")
                .build();
        templateBean.setTarget("email", emailTemplate);
    }

    /**
     * Verifies that a statement email is generated if the customer has a billing email address.
     */
    @Test
    public void testEmail() {
        JavaMailSender sender = mock(JavaMailSender.class);
        MimeMessage mimeMessage = mock(MimeMessage.class);
        when(sender.createMimeMessage()).thenReturn(mimeMessage);

        Date statementDate = getDate("2007-01-01");

        Party practice = getPractice();
        Party customer = getCustomer();
        addCustomerEmail(customer);
        save(customer);

        IArchetypeService archetypeService = getArchetypeService();
        CustomerAccountRules customerAccountRules = ServiceHelper.getBean(CustomerAccountRules.class);
        StatementRules rules = new StatementRules(practice, archetypeService, customerAccountRules);
        assertFalse(rules.hasStatement(customer, statementDate));
        List<Act> acts = getActs(customer, statementDate);
        assertEquals(0, acts.size());

        Money amount = new Money(100);
        List<FinancialAct> invoice1 = createChargesInvoice(amount, getDatetime("2007-01-01 10:00:00"));
        save(invoice1);

        acts = getActs(customer, statementDate);
        assertEquals(1, acts.size());
        checkAct(acts.get(0), invoice1.get(0), POSTED);

        List<Statement> statements = new ArrayList<>();
        StatementProcessor processor = new StatementProcessor(statementDate, practice, archetypeService,
                                                              customerAccountRules);
        processor.addListener(statements::add);
        processor.process(customer);
        assertEquals(1, statements.size());
        Converter converter = Mockito.mock(Converter.class);
        ReportFactory reportFactory = ServiceHelper.getBean(ReportFactory.class);
        EmailTemplateEvaluator evaluator = new EmailTemplateEvaluator(archetypeService, getLookupService(),
                                                                      ServiceHelper.getMacros(),
                                                                      reportFactory, converter);
        FileNameFormatter formatter = Mockito.mock(FileNameFormatter.class);
        DocumentHandlers handlers = ServiceHelper.getBean(DocumentHandlers.class);
        ReportFactory factory = new ReportFactory(archetypeService, getLookupService(), handlers,
                                                  ServiceHelper.getBean(ArchetypeFunctionsFactory.class));
        ReporterFactory reporterFactory = new ReporterFactory(factory, formatter, archetypeService,
                                                              getLookupService());

        MailerFactory mailerFactory = Mockito.mock(MailerFactory.class);
        sender = Mockito.mock(JavaMailSender.class);
        mimeMessage = Mockito.mock(MimeMessage.class);
        Mockito.when(sender.createMimeMessage()).thenReturn(mimeMessage);

        LocalContext context = new LocalContext();
        Party location = TestHelper.createLocation();
        context.setPractice(practice);
        context.setLocation(location);
        MailContext mailContext = new PracticeMailContext(context);
        Mailer mailer = new DefaultMailer(mailContext, sender, handlers);
        Mockito.when(mailerFactory.create(Mockito.any(), Mockito.any())).thenReturn(mailer);
        PracticeRules practiceRules = ServiceHelper.getBean(PracticeRules.class);

        CommunicationLogger logger = Mockito.mock(CommunicationLogger.class);

        StatementEmailProcessor emailProcessor = new StatementEmailProcessor(
                mailerFactory, evaluator, reporterFactory, practice, location, practiceRules, archetypeService,
                logger, Mockito.mock(MailPasswordResolver.class)) {
            @Override
            protected Entity getStatementTemplate(IArchetypeService service) {
                return statementTemplate;
            }
        };
        emailProcessor.process(statements.get(0));
        Mockito.verify(sender, times(1)).send(mimeMessage);
    }

    /**
     * Helper to create an email contact.
     *
     * @param customer the customer
     */
    private void addCustomerEmail(Party customer) {
        customer.getContacts().clear();
        Contact contact = (Contact) create(ContactArchetypes.EMAIL);
        IMObjectBean bean = getBean(contact);
        bean.setValue("emailAddress", "foo@bar.com");
        Lookup lookup = TestHelper.getLookup("lookup.contactPurpose", "BILLING");
        bean.addValue("purposes", lookup);
        customer.addContact(contact);
    }

}
