package org.openvpms.web.workspace.workflow.appointment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid.Availability;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link AppointmentGridView}.
 *
 * @author Tim Anderson
 */
public class AppointmentGridViewTestCase extends AbstractAppointmentGridTest {

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules rules;

    /**
     * The roster service.
     */
    @Autowired
    private RosterService rosterService;

    /**
     * The system time zone.
     */
    private TimeZone defaultTimeZone;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        // replace the default timezone with Australia/Melbourne, so that daylight savings tests work
        defaultTimeZone = TimeZone.getDefault();
        TimeZone.setDefault(TimeZone.getTimeZone("Australia/Melbourne"));
        super.setUp();
    }

    /**
     * Cleans up after the test.
     */
    @After
    @Override
    public void tearDown() {
        TimeZone.setDefault(defaultTimeZone);
        super.tearDown();
    }

    /**
     * Verifies that slots times are correct for a single schedule grid at the start and end of daylight savings.
     */
    @Test
    public void testDaylightSavingsForSingle9to5ScheduleGrid() {
        Date daylightSavingsStart = TestHelper.getDate("2019-10-06");
        Date daylightSavingsEnd = TestHelper.getDate("2020-04-05");
        Entity schedule = createSchedule(15, "09:00:00", "17:00:00"); // 9am-5pm schedule, 15 min slots
        Entity scheduleView = ScheduleTestHelper.createScheduleView(schedule);
        ScheduleEvents events = new ScheduleEvents(Collections.emptyList(), 0);
        SingleScheduleGrid grid1 = new SingleScheduleGrid(scheduleView, daylightSavingsStart, schedule, events, rules,
                                                          rosterService);
        AppointmentGridView view1 = new AppointmentGridView(grid1, 9 * 60, 17 * 60, rules, rosterService);
        assertEquals(32, view1.getSlots());
        checkSlot(view1, schedule, 0, "2019-10-06T09:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule, 1, "2019-10-06T09:15:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule, 2, "2019-10-06T09:30:00+11:00", null, 0, Availability.FREE);

        SingleScheduleGrid grid2 = new SingleScheduleGrid(scheduleView, daylightSavingsEnd, schedule, events, rules,
                                                          rosterService);
        AppointmentGridView view2 = new AppointmentGridView(grid2, 9 * 60, 17 * 60, rules, rosterService);
        assertEquals(32, view2.getSlots());
        checkSlot(view2, schedule, 0, "2020-04-05T09:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule, 1, "2020-04-05T09:15:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule, 2, "2020-04-05T09:30:00+10:00", null, 0, Availability.FREE);
    }

    /**
     * Verifies that slots times are correct for a multi schedule grid at the start and end of daylight savings.
     */
    @Test
    public void testDaylightSavingsForMutli9to5ScheduleGrid() {
        Date daylightSavingsStart = TestHelper.getDate("2019-10-06");
        Date daylightSavingsEnd = TestHelper.getDate("2020-04-05");
        Entity schedule1 = createSchedule(15, "09:00:00", "17:00:00"); // 9am-5pm schedule, 15 min slots
        Entity schedule2 = createSchedule(15, "09:00:00", "17:00:00"); // 9am-5pm schedule, 15 min slots
        Entity scheduleView = ScheduleTestHelper.createScheduleView(schedule1, schedule2);
        ScheduleEvents emptyEvents = new ScheduleEvents(Collections.emptyList(), 0);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule1, emptyEvents);
        map.put(schedule2, emptyEvents);
        MultiScheduleGrid grid1 = new MultiScheduleGrid(scheduleView, daylightSavingsStart, map, rules, rosterService);
        AppointmentGridView view1 = new AppointmentGridView(grid1, 9 * 60, 17 * 60, rules, rosterService);
        assertEquals(32, view1.getSlots());
        checkSlot(view1, schedule1, 0, "2019-10-06T09:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule2, 0, "2019-10-06T09:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule1, 1, "2019-10-06T09:15:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule2, 1, "2019-10-06T09:15:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule1, 2, "2019-10-06T09:30:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule2, 2, "2019-10-06T09:30:00+11:00", null, 0, Availability.FREE);

        MultiScheduleGrid grid2 = new MultiScheduleGrid(scheduleView, daylightSavingsEnd, map, rules, rosterService);
        AppointmentGridView view2 = new AppointmentGridView(grid2, 9 * 60, 17 * 60, rules, rosterService);
        assertEquals(32, view2.getSlots());
        checkSlot(view2, schedule1, 0, "2020-04-05T09:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule2, 0, "2020-04-05T09:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule1, 1, "2020-04-05T09:15:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule2, 1, "2020-04-05T09:15:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule1, 2, "2020-04-05T09:30:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule2, 2, "2020-04-05T09:30:00+10:00", null, 0, Availability.FREE);
    }

    /**
     * Verifies that slots times are correct for a 24 hour single schedule grid at the start and end of daylight
     * savings.
     * <p/>
     * For Melbourne, the start of daylight savings is 2am i.e 2am becomes 3am. For a 24 hour schedule there should
     * therefore be 23 slots. At the end of daylight savings, 2am happens twice; 2am+11:00 and 2am+10:00, so there
     * should be 25 slots.
     * <p/>
     * However, appointments (in fact any act) are stored without timezone information so appointments at 2am will be
     * displayed at:
     * <ul>
     *     <li>3am on at the start of daylight saving</li>
     *     <li>2am+11:00 at the end of daylight savings.</li>
     * </ul>
     */
    @Test
    public void test24HourSingleScheduleDuringDaylightSaving() {
        Date daylightSavingsStart = TestHelper.getDate("2019-10-06");
        Date daylightSavingsEnd = TestHelper.getDate("2020-04-05");

        Entity schedule = createSchedule(60, "00:00:00", "24:00:00");
        Entity scheduleView = ScheduleTestHelper.createScheduleView(schedule);

        Act appointment1 = ScheduleTestHelper.createAppointment(
                getDatetime("2019-10-06 02:00"), getDatetime("2019-10-06 02:30"), schedule);

        ScheduleEvents events1 = new ScheduleEvents(Collections.singletonList(createEvent(appointment1)), 0);
        SingleScheduleGrid grid1 = new SingleScheduleGrid(scheduleView, daylightSavingsStart, schedule, events1, rules,
                                                          rosterService);
        AppointmentGridView view1 = new AppointmentGridView(grid1, 0, 24 * 60, rules, rosterService);
        assertEquals(23, view1.getSlots());
        checkSlot(view1, schedule, 0, "2019-10-06T00:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule, 1, "2019-10-06T01:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule, 2, "2019-10-06T03:00:00+11:00", appointment1, 1, Availability.BUSY);
        ; // 2:00am becomes 3:00am
        checkSlot(view1, schedule, 3, "2019-10-06T04:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, schedule, 22, "2019-10-06T23:00:00+11:00", null, 0, Availability.FREE);

        Act appointment2 = ScheduleTestHelper.createAppointment(
                getDatetime("2020-04-05 02:00"), getDatetime("2020-04-05 02:30"), schedule);
        ScheduleEvents events2 = new ScheduleEvents(Collections.singletonList(createEvent(appointment2)), 0);

        SingleScheduleGrid grid2 = new SingleScheduleGrid(scheduleView, daylightSavingsEnd, schedule, events2, rules,
                                                          rosterService);
        AppointmentGridView view2 = new AppointmentGridView(grid2, 0, 24 * 60, rules, rosterService);
        assertEquals(25, view2.getSlots());
        checkSlot(view2, schedule, 0, "2020-04-05T00:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule, 1, "2020-04-05T01:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule, 2, "2020-04-05T02:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule, 3, "2020-04-05T02:00:00+10:00", appointment2, 1, Availability.BUSY);
        checkSlot(view2, schedule, 4, "2020-04-05T03:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, schedule, 24, "2020-04-05T23:00:00+10:00", null, 0, Availability.FREE);
    }

    /**
     * Verifies that slots times are correct for a 24 hour multi schedule grid at the start and end of daylight
     * savings.
     * <p/>
     * For Melbourne, the start of daylight savings is 2am i.e 2am becomes 3am. For a 24 hour schedule there should
     * therefore be 23 slots. At the end of daylight savings, 2am happens twice; 2am+11:00 and 2am+10:00, so there
     * should be 25 slots.
     * <p/>
     * However, appointments (in fact any act) are stored without timezone information so appointments at 2am will be
     * displayed at:
     * <ul>
     *     <li>3am on at the start of daylight saving</li>
     *     <li>2am+11:00 at the end of daylight savings.</li>
     * </ul>
     */
    @Test
    public void test24HourMutliScheduleDuringDaylightSaving() {
        Date daylightSavingsStart = TestHelper.getDate("2019-10-06");
        Date daylightSavingsEnd = TestHelper.getDate("2020-04-05");

        Entity scheduleA = createSchedule(60, "00:00:00", "24:00:00");
        Entity scheduleB = createSchedule(60, "00:00:00", "24:00:00");
        Entity scheduleView = ScheduleTestHelper.createScheduleView(scheduleA, scheduleB);

        Act appointment1a = createAppointment("2019-10-06 02:00", "2019-10-06 02:30", scheduleA);
        Act appointment1b = createAppointment("2019-10-06 02:00", "2019-10-06 03:00", scheduleB);

        Map<Entity, ScheduleEvents> map1 = new HashMap<>();
        map1.put(scheduleA, new ScheduleEvents(Collections.singletonList(createEvent(appointment1a)), 0));
        map1.put(scheduleB, new ScheduleEvents(Collections.singletonList(createEvent(appointment1b)), 0));

        MultiScheduleGrid grid1 = new MultiScheduleGrid(scheduleView, daylightSavingsStart, map1, rules, rosterService);
        AppointmentGridView view1 = new AppointmentGridView(grid1, 0, 24 * 60, rules, rosterService);
        assertEquals(23, view1.getSlots());
        checkSlot(view1, scheduleA, 0, "2019-10-06T00:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view1, scheduleA, 1, "2019-10-06T01:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view1, scheduleA, 2, "2019-10-06T03:00:00+11:00", appointment1a, 1, Availability.BUSY);
        // 2:00am becomes 3:00am
        checkSlot(view1, scheduleA, 3, "2019-10-06T04:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view1, scheduleA, 22, "2019-10-06T23:00:00+11:00", null, 0, Availability.FREE);

        Act appointment2a = createAppointment("2020-04-05 02:00", "2020-04-05 02:30", scheduleA);
        Act appointment2b = createAppointment("2020-04-05 02:00", "2020-04-05 03:00", scheduleB);

        Map<Entity, ScheduleEvents> map2 = new HashMap<>();
        map2.put(scheduleA, new ScheduleEvents(Collections.singletonList(createEvent(appointment2a)), 0));
        map2.put(scheduleB, new ScheduleEvents(Collections.singletonList(createEvent(appointment2b)), 0));
        MultiScheduleGrid grid2 = new MultiScheduleGrid(scheduleView, daylightSavingsEnd, map2, rules, rosterService);
        AppointmentGridView view2 = new AppointmentGridView(grid2, 0, 24 * 60, rules, rosterService);
        assertEquals(25, view2.getSlots());
        checkSlot(view2, scheduleA, 0, "2020-04-05T00:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view2, scheduleA, 1, "2020-04-05T01:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view2, scheduleA, 2, "2020-04-05T02:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(view2, scheduleA, 3, "2020-04-05T02:00:00+10:00", appointment2a, 1, Availability.BUSY);
        checkSlot(view2, scheduleA, 4, "2020-04-05T03:00:00+10:00", null, 0, Availability.FREE);
        checkSlot(view2, scheduleA, 24, "2020-04-05T23:00:00+10:00", null, 0, Availability.FREE);
    }

}
