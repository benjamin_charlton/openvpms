/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * .
 *
 * @author Tim Anderson
 */
public class CustomerAccountQueryTestCase extends AbstractAppTest {

    private Party customer;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        customer = TestHelper.createCustomer();
    }

    /**
     * Verifies that the query returns the expected no. of results.
     */
    @Test
    public void testQuery() {
        TestCustomerAccountQuery query = new TestCustomerAccountQuery(customer);
        ResultSet<Act> results = query.query();
        assertNotNull(results);
        assertEquals(0, results.getResults());

        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        List<FinancialAct> invoice1 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer,
                                                                               patient1, TestHelper.createProduct(),
                                                                               ActStatus.POSTED);
        List<FinancialAct> invoice2 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer,
                                                                               patient2, TestHelper.createProduct(),
                                                                               ActStatus.POSTED);
        List<FinancialAct> credit = FinancialTestHelper.createChargesCredit(BigDecimal.TEN, customer,
                                                                            patient1,
                                                                            TestHelper.createProduct(),
                                                                            ActStatus.POSTED);
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, TestHelper.createTill(),
                                                                 ActStatus.POSTED);
        save(invoice1);
        save(invoice2);
        save(credit);
        save(payment);

        results = query.query();
        assertEquals(4, results.getResults());

        // verify pages can be counted
        assertTrue(results.getPages() >= 1);

        // check query by patient
        query.setPatient(patient1);
        results = query.query();
        assertEquals(2, results.getResults());

        query.setPatient(patient2);
        results = query.query();
        assertEquals(1, results.getResults());

        // now verify if the query is restricted to an archetype without patients, no results are returned
        query.setShortName(CustomerAccountArchetypes.REFUND);
        results = query.query();
        assertEquals(0, results.getResults());
    }

    private static class TestCustomerAccountQuery extends CustomerAccountQuery<Act> {
        public TestCustomerAccountQuery(Party customer) {
            super(customer, new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null)));
        }

        /**
         * Set the archetype short name.
         *
         * @param name the archetype short name. If {@code null}, indicates to query using all matching short names.
         */
        @Override
        public void setShortName(String name) {
            super.setShortName(name);
        }
    }

}
