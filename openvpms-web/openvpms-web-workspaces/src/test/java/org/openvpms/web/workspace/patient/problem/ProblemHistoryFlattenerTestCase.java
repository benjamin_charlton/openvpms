/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.problem;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.web.workspace.patient.history.AbstractPatientHistoryFlattenerTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.openvpms.archetype.rules.patient.PatientArchetypes.CLINICAL_ADDENDUM;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.CLINICAL_NOTE;
import static org.openvpms.archetype.rules.patient.PatientArchetypes.PATIENT_WEIGHT;
import static org.openvpms.archetype.rules.patient.PatientTestHelper.createNote;
import static org.openvpms.archetype.rules.patient.PatientTestHelper.createProblem;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link ProblemHistoryFlattener}.
 *
 * @author Tim Anderson
 */
public class ProblemHistoryFlattenerTestCase extends AbstractPatientHistoryFlattenerTest {

    /**
     * Tests flattening the hierarchy of a single problem.
     */
    @Test
    public void testSingleProblem() {
        Party patient = TestHelper.createPatient();
        User clinician = TestHelper.createClinician();
        Act note1 = createNote(getDatetime("2014-05-09 10:04:00"), patient, clinician);
        Act note2 = createNote(getDatetime("2014-05-09 10:10:00"), patient, clinician);
        Act problem = createProblem(getDatetime("2014-05-09 10:00:00"), patient, clinician, note1, note2);
        Act event = PatientTestHelper.createEvent(getDatetime("2014-05-09 09:00:00"), patient, clinician, problem,
                                                  note1, note2);

        String[] archetypes = new String[]{PATIENT_WEIGHT, CLINICAL_NOTE, CLINICAL_ADDENDUM};

        ProblemHistoryFlattener flattener = new ProblemHistoryFlattener(getArchetypeService());

        List<Act> acts = Collections.singletonList(problem);
        check(flattener, acts, archetypes, true, true, problem, event, note1, note2);
        check(flattener, acts, archetypes, true, false, problem, event, note2, note1);
    }

    /**
     * Tests flattening the hierarchy of multiple problems.
     */
    @Test
    public void testFlattener() {
        Party patient = TestHelper.createPatient();
        User clinician = TestHelper.createClinician();

        Act note1 = createNote(getDatetime("2014-05-09 10:04:00"), patient, clinician);
        Act note2 = createNote(getDatetime("2014-05-09 10:10:00"), patient, clinician);
        Act problem1 = createProblem(getDatetime("2014-05-09 10:00:00"), patient, clinician, note1, note2);
        Act event1 = PatientTestHelper.createEvent(getDatetime("2014-05-09 09:00:00"), patient, clinician, problem1,
                                                  note1, note2);
        Act problem2 = createProblem(getDatetime("2019-07-04 12:00:00"), patient, clinician);
        Act event2 = PatientTestHelper.createEvent(getDatetime("2019-07-04 11:00:00"), patient, clinician, problem2);

        String[] archetypes = new String[]{PATIENT_WEIGHT, CLINICAL_NOTE, CLINICAL_ADDENDUM};

        // primary acts sorted in ascending order
        List<Act> ascending = Arrays.asList(problem1, problem2);

        // and descending order
        List<Act> descending = new ArrayList<>(ascending);
        Collections.reverse(descending);

        ProblemHistoryFlattener flattener = new ProblemHistoryFlattener(getArchetypeService());

        // parent, child sorted ascending
        check(flattener, ascending, archetypes, true, true, problem1, event1, note1, note2, problem2, event2);

        // parent ascending, child sorted descending
        check(flattener, ascending, archetypes, true, false, problem1, event1, note2, note1, problem2, event2);

        // parent descending, child sorted ascending
        check(flattener, descending, archetypes, false, true, problem2, event2, problem1, event1, note1, note2);

        // parent, child sorted descending
        check(flattener, descending, archetypes, false, false, problem2, event2, problem1, event1, note2, note1);
    }
}
