/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link LogReader}.
 *
 * @author Tim Anderson
 */
public class LogReaderTestCase {

    /**
     * Temporary directory for test files.
     */
    @Rule
    public TemporaryFolder dir = new TemporaryFolder();

    /**
     * Verifies an empty page is returned for an empty log file.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testEmptyFile() throws IOException {
        File file = dir.newFile();

        LogReader reader = new LogReader(file.getPath(), 10);
        LogReader.Page page = reader.read(0);
        assertEquals(0, page.getLines().size());
        assertEquals(0, page.getPage());
        assertEquals(0, page.getFirstLine());
        assertTrue(page.isPartial());
    }

    /**
     * Verifies that pages can be read in sequence.
     *
     * @throws IOException for any error
     */
    @Test
    public void testSequentialRead() throws IOException {
        File file = dir.newFile();
        append(file, 42, 1);       // write 42 lines, starting at '1'

        LogReader reader = new LogReader(file, 10); // read 10 lines at a time
        LogReader.Page page1 = reader.read(0);
        LogReader.Page page2 = reader.read(1);
        LogReader.Page page3 = reader.read(2);
        LogReader.Page page4 = reader.read(3);
        LogReader.Page page5 = reader.read(4);
        checkPage(page1, 0, 10, 0, false);
        checkPage(page2, 1, 10, 10, false);
        checkPage(page3, 2, 10, 20, false);
        checkPage(page4, 3, 10, 30, false);
        checkPage(page5, 4, 2, 40, true);
    }

    /**
     * Verifies that pages can be read out of sequence.
     *
     * @throws IOException for any error
     */
    @Test
    public void testRandomRead() throws IOException {
        File file = dir.newFile();
        append(file, 43, 1);

        LogReader reader = new LogReader(file, 10);
        LogReader.Page page1 = reader.read(0);
        LogReader.Page page4 = reader.read(3);
        LogReader.Page page2 = reader.read(1);
        LogReader.Page page5 = reader.read(4);
        checkPage(page1, 0, 10, 0, false);
        checkPage(page4, 3, 10, 30, false);
        checkPage(page2, 1, 10, 10, false);
        checkPage(page5, 4, 3, 40, true);

        // verify can't read past the last page
        LogReader.Page page6 = reader.read(5);
        assertNull(page6);

        // verify can still read the last page
        page5 = reader.read(4);
        checkPage(page5, 4, 3, 40, true);
    }

    /**
     * Verifies that if the last page is appended to, this is picked up in a subsequent read.
     *
     * @throws IOException for any error
     */
    @Test
    public void testAppend() throws IOException {
        File file = dir.newFile();
        append(file, 5, 1);   // write 5 lines

        LogReader reader = new LogReader(file, 10);
        LogReader.Page page1 = reader.read(0);
        checkPage(page1, 0, 5, 0, true);

        append(file, 12, 6);   // write 12 more lines

        page1 = reader.read(0);
        checkPage(page1, 0, 10, 0, false);

        LogReader.Page page2 = reader.read(1);
        checkPage(page2, 1, 7, 10, true);
    }

    /**
     * Verifies that a page is marked as partial if it doesn't have maxLines, or the last line didn't have a new line.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testPartialWrite() throws IOException {
        File file = dir.newFile();
        append(file, 9, 1);   // write 9 lines
        append(file, "10");   // write a 10th line, but don't include the new line.

        LogReader reader = new LogReader(file, 10);
        LogReader.Page page1 = reader.read(0);
        checkPage(page1, 0, 10, 0, true);

        LogReader.Page page2 = reader.read(1);
        assertNull(page2);   // no page2 as page1 is incomplete

        append(file, "\n");
        append(file, 2, 11);

        page1 = reader.read(0);
        checkPage(page1, 0, 10, 0, false);

        page2 = reader.read(1);
        checkPage(page2, 1, 2, 10, true);
    }

    /**
     * Tests the {@link LogReader#readLast()} method.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testReadLast() throws IOException {
        File file = dir.newFile();

        LogReader reader = new LogReader(file, 10);
        LogReader.Page page1 = reader.readLast();
        checkPage(page1, 0, 0, 0, true);

        append(file, 9, 1);   // write 9 lines

        page1 = reader.readLast();
        checkPage(page1, 0, 9, 0, true);

        append(file, 12, 10);   // write 12 more lines

        LogReader.Page page3 = reader.readLast();
        checkPage(page3, 2, 1, 20, true);
    }

    /**
     * Tests the {@link LogReader#readLast()} method when the file is truncated between reads.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testReadLastWithTruncate() throws IOException {
        File file = dir.newFile();

        LogReader reader = new LogReader(file, 10);
        append(file, 23, 1);   // write 23 lines

        LogReader.Page last1 = reader.readLast();
        checkPage(last1, 2, 3, 20, true);

        // re-write the file
        write(file, 5, 1, false);

        LogReader.Page last2 = reader.readLast();
        assertNull(last2);   // indicates file has been truncated since last read

        LogReader.Page last3 = reader.readLast();
        checkPage(last3, 0, 5, 0, true);
    }

    /**
     * Verifies that if the log includes binary data, and specifically 0xff characters, the log can still be read.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testBinaryData() throws IOException {
        File file = dir.newFile();
        DataOutputStream stream = new DataOutputStream(new FileOutputStream(file));
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                stream.write(0);
                stream.write(0xff);
                stream.write('\n');
            }
        }
        stream.close();
        LogReader reader = new LogReader(file, 10);
        LogReader.Page page0 = reader.read(0);
        assertNotNull(page0);
        assertEquals(0, page0.getPage());
        List<String> lines = page0.getLines();
        assertEquals(10, lines.size());
        String expected = new StringBuilder().append((char) 0).append((char) 0xff).toString();
        // use of StringBuilder avoids charset conversions encountered using new String(byte[])
        for (String line : lines) {
            assertEquals(expected, line);
        }
    }

    /**
     * Verifies a page matches that expected.
     *
     * @param page    the page
     * @param pageNo  the expected page number
     * @param count   the expected number of lines
     * @param first   the expected first line number
     * @param partial indicates if the page was partially read
     */
    private void checkPage(LogReader.Page page, int pageNo, int count, int first, boolean partial) {
        assertNotNull(page);
        assertEquals(pageNo, page.getPage());
        List<String> lines = page.getLines();
        assertEquals(count, lines.size());
        assertEquals(first, page.getFirstLine());
        assertEquals(partial, page.isPartial());
        for (int i = 0; i < lines.size(); ++i) {
            assertEquals(Integer.toString(first + i + 1), lines.get(i));
        }
    }

    /**
     * Appends a lines containing an incrementing number to a file.
     *
     * @param file  the file to append to
     * @param lines the number of lines to write
     * @param start the starting value
     * @throws IOException for any I/O error
     */
    private void append(File file, int lines, int start) throws IOException {
        write(file, lines, start, true);
    }

    /**
     * Writes lines containing an incrementing number to a file.
     *
     * @param file   the file to append to
     * @param lines  the number of lines to write
     * @param start  the starting value
     * @param append if {@code true} append to any existing file
     * @throws IOException for any I/O error
     */
    private void write(File file, int lines, int start, boolean append) throws IOException {
        FileWriter writer = new FileWriter(file, append);
        for (int i = 0; i < lines; ++i) {
            writer.write("" + (i + start) + "\n");
        }
        writer.close();
    }

    /**
     * Appends a value to a file.
     *
     * @param file  the file
     * @param value the value to write
     * @throws IOException for any I/O error
     */
    private void append(File file, String value) throws IOException {
        FileWriter writer = new FileWriter(file, true);
        writer.write(value);
        writer.close();
    }
}
