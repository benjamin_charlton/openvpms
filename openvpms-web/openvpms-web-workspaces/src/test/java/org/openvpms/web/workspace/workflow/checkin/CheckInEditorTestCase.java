/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkin;

import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

/**
 * Tests the {@link CheckInEditor}.
 *
 * @author Tim Anderson
 */
public class CheckInEditorTestCase extends AbstractAppTest {

    /**
     * Verifies that a patient cannot be selected that doesn't belong to the current customer.
     */
    @Test
    public void testInvalidPatient() {
        Party location = TestHelper.createLocation();
        Party practice = TestHelper.getPractice();
        CheckInEditor editor = new CheckInEditor(TestHelper.createCustomer("J", "Smith", true),
                                                 TestHelper.createPatient("Fido", true),
                                                 ScheduleTestHelper.createSchedule(location),
                                                 null, location, new Date(), null, null,
                                                 new HelpContext("foo", null));
        DefaultValidator validator = new DefaultValidator();
        assertFalse(editor.validate(validator));
        assertEquals("Fido is not owned by Smith,J", validator.getFirstError().getMessage());
    }

    /**
     * Tests the {@link CheckInEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Party practice = TestHelper.getPractice();
        Party location = TestHelper.createLocation();
        Party customer = TestHelper.createCustomer();
        Party patient = TestHelper.createPatient(customer);
        Party schedule = ScheduleTestHelper.createSchedule(location);
        Entity workList = ScheduleTestHelper.createWorkList();
        Entity taskType = ScheduleTestHelper.createTaskType();
        User clinician = TestHelper.createClinician();
        User user = TestHelper.createUser();
        Date arrivalTime = new Date();
        Act appointment = ScheduleTestHelper.createAppointment(DateRules.getToday(), DateRules.getTomorrow(), schedule,
                                                               customer, patient);
        save(appointment);
        CheckInEditor editor1 = new CheckInEditor(customer, patient, schedule, clinician, location,
                                                  arrivalTime, appointment, user, new HelpContext("foo", null));
        editor1.setWeight(BigDecimal.TEN);
        editor1.setWorkList(workList);
        editor1.setTaskType(taskType);

        CheckInEditor editor2 = editor1.newInstance();
        assertEquals(customer, editor2.getCustomer());
        assertEquals(patient, editor2.getPatient());
        assertEquals(schedule, editor2.getSchedule());
        assertEquals(clinician, editor2.getClinician());
        assertEquals(location, editor2.getLocation());
        assertEquals(arrivalTime, editor2.getArrivalTime());
        assertEquals(user, editor2.getUser());
        assertEquals(appointment, editor2.getAppointment());
        checkEquals(BigDecimal.TEN, editor2.getWeight());
        assertEquals(workList, editor2.getWorkList());
        assertEquals(taskType, editor2.getTaskType());

        // verify IllegalStateException is thrown if the customer doesn't correspond to that on the appointment
        IMObjectBean bean = getBean(appointment);
        bean.setTarget("customer", TestHelper.createCustomer());
        bean.save();
        try {
            editor2.newInstance();
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            assertEquals("The appointment customer has changed", expected.getMessage());
        }

        // verify IllegalStateException is thrown if the appointment is removed
        remove(appointment);
        try {
            editor2.newInstance();
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            assertEquals("Appointment no longer exists", expected.getMessage());
        }
    }
}
