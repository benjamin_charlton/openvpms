/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.query.AbstractQueryTest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base class for SMS query test cases.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSMSQueryTest extends AbstractQueryTest<Act> {

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests querying replies by practice location.
     * <p/>
     * Only those replies for locations accessible by the user are returned.
     * <br/>
     * SMSes not associated with a practice location are always returned.
     */
    @Test
    public void testQueryByLocation() {
        Party location1 = (Party) practiceFactory.createLocation();
        Party location2 = (Party) practiceFactory.createLocation();
        Party location3 = (Party) practiceFactory.createLocation();
        Party practice = (Party) practiceFactory.newPractice()
                .locations(location1, location2, location3)
                .build(false);

        User user = (User) userFactory.newUser()
                .addLocations(location1, location2)
                .build();

        LocalContext context = new LocalContext();
        context.setPractice(practice);
        context.setUser(user);
        AbstractSMSQuery query = createQuery(context);

        Act sms1 = createSMS(location1);
        Act sms2 = createSMS(location2);
        Act sms3 = createSMS(location3);
        Act sms4 = createSMS(null);

        query.setLocation(null);
        checkSelects(true, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(false, query, sms3);
        checkSelects(true, query, sms4);

        query.setLocation(location1);
        checkSelects(true, query, sms1);
        checkSelects(false, query, sms2);
        checkSelects(false, query, sms3);
        checkSelects(true, query, sms4);

        query.setLocation(location2);
        checkSelects(false, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(false, query, sms3);
        checkSelects(true, query, sms4);
    }

    /**
     * Tests querying SMSes by location and recipient/sender name.
     */
    @Test
    public void testQueryByLocationAndContact() {
        Party location1 = (Party) practiceFactory.createLocation();
        Party location2 = (Party) practiceFactory.createLocation();
        Party practice = (Party) practiceFactory.newPractice()
                .locations(location1, location2)
                .build(false);

        String name1 = getUniqueValue();
        String name2 = getUniqueValue();

        Act sms1 = createSMS(name1, location1);
        Act sms2 = createSMS(name1, location2);
        Act sms3 = createSMS(name1, null);
        Act sms4 = createSMS(name2, location1);
        Act sms5 = createSMS(name2, location2);
        Act sms6 = createSMS(name2, null);

        LocalContext context = new LocalContext();
        context.setPractice(practice);
        AbstractSMSQuery query = createQuery(context);
        query.getComponent();

        query.setLocation(null);
        query.setValue(name1);
        checkSelects(true, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(true, query, sms3);
        checkSelects(false, query, sms4);
        checkSelects(false, query, sms5);
        checkSelects(false, query, sms6);

        query.setLocation(location1);
        checkSelects(true, query, sms1);
        checkSelects(false, query, sms2);
        checkSelects(true, query, sms3);
        checkSelects(false, query, sms4);
        checkSelects(false, query, sms5);
        checkSelects(false, query, sms6);

        query.setLocation(location2);
        checkSelects(false, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(true, query, sms3);
        checkSelects(false, query, sms4);
        checkSelects(false, query, sms5);
        checkSelects(false, query, sms6);
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    @Override
    protected AbstractSMSQuery createQuery() {
        return createQuery(new LocalContext());
    }

    /**
     * Creates a new query.
     *
     * @param context the context
     * @return a new query
     */
    protected abstract AbstractSMSQuery createQuery(Context context);

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if {@code true} save the object, otherwise don't save it
     * @return the new object
     */
    @Override
    protected abstract Act createObject(String value, boolean save);

    /**
     * Generates a unique value which may be used for querying objects on.
     *
     * @return a unique value
     */
    @Override
    protected String getUniqueValue() {
        String value = getUniqueValue("Z");
        return value.length() <= 30 ? value : value.substring(0, 30);
    }

    /**
     * Creates an SMS linked to a practice location.
     *
     * @param location the practice location. May be {@code null}
     * @return the SMS
     */
    protected Act createSMS(Party location) {
        return createSMS(getUniqueValue(), location);
    }

    /**
     * Creates an SMS linked to a practice location.
     *
     * @param contactName the recipient/sender name
     * @param location    the practice location. May be {@code null}
     * @return the SMS
     */
    protected abstract Act createSMS(String contactName, Party location);

}
