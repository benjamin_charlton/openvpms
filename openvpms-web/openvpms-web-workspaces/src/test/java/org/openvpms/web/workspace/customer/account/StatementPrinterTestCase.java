/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.statement.StatementRules;
import org.openvpms.archetype.rules.finance.statement.StatementService;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.print.impl.locator.DefaultDocumentPrinterServiceLocator;
import org.openvpms.report.openoffice.Converter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.report.TemplatedReporter;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link StatementPrinter}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class StatementPrinterTestCase extends AbstractAppTest {

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules accountRules;

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The reporter factory.
     */
    @Autowired
    private ReporterFactory reporterFactory;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers documentHandlers;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory customerAccountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The statement rules.
     */
    private StatementRules statementRules;

    /**
     * The statement service.
     */
    private StatementService statementService;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        Party practice = (Party) practiceFactory.getPractice();
        context.setPractice(practice);

        statementRules = new StatementRules(practice, getArchetypeService(), accountRules);
        statementService = new StatementService((IArchetypeRuleService) getArchetypeService(), accountRules,
                                                laboratoryRules, getPracticeService());

        // make sure there is a statement template available
        documentFactory.newTemplate()
                .type(CustomerAccountArchetypes.OPENING_BALANCE)
                .blankDocument()
                .build();
    }

    /**
     * Verifies that the <em>overdueBalance</em> and <em>statementDate</em> parameters are populated correctly.
     */
    @Test
    public void testParameters() {
        Date now = new Date();
        Date yesterday = DateRules.getYesterday();

        // create a customer with an overdue balance
        Lookup accountType = FinancialTestHelper.createAccountType(30, DateUnits.DAYS, BigDecimal.ZERO);
        Party customer = (Party) customerFactory.newCustomer().addClassifications(accountType).build();

        customerAccountFactory.newCounterSale()
                .startTime(DateRules.getDate(now, -40, DateUnits.DAYS))
                .status(ActStatus.POSTED)
                .customer(customer)
                .item()
                .fixedPrice(100)
                .medicationProduct()
                .add()
                .build();

        context.setCustomer(customer);

        PrinterContext printerContext = new PrinterContext(new DefaultDocumentPrinterServiceLocator(),
                                                           getArchetypeService(), documentHandlers,
                                                           Mockito.mock(Converter.class), domainService);
        StatementPrinter printer = new StatementPrinter(context, accountRules, printerContext, reporterFactory);

        // verify that when printing the current statement, the overdueBalance = 100 and statementDate >= now
        printer.setPrintCurrent(true, false);
        Date statementDate1 = checkParameters(printer, 100);
        assertTrue(statementDate1.compareTo(now) >= 0);

        // now generate a closing balance for the customer
        statementService.endPeriod(customer, yesterday, false);
        FinancialAct closingBalance = statementRules.getClosingBalance(customer, yesterday);
        assertNotNull(closingBalance);

        // verify when printing the statement for the closing balance,  overdueBalance = 0 and
        // statementDate = closing balance date
        printer.setPrintStatement(closingBalance);
        checkParameters(printer, 0, closingBalance.getActivityStartTime());

        // verify when printing a range, overdueBalance = 0 and statementDate = range end
        printer.setPrintRange(yesterday, DateRules.getToday());
        checkParameters(printer, 0, DateRules.getToday());

        printer.setPrintRange(null, yesterday);
        checkParameters(printer, 0, yesterday);

        // verify that when printing an open range, overdueBalance = 0 and statementDate >= now
        printer.setPrintRange(null, null);
        Date statementDate2 = checkParameters(printer, 0);
        assertTrue(statementDate2.compareTo(now) >= 0);
    }

    /**
     * Verifies report parameters match those expected.
     *
     * @param printer        the statement printer
     * @param overdueBalance the expected overdue balance
     * @param statementDate  the expected statement date
     */
    private void checkParameters(StatementPrinter printer, int overdueBalance, Date statementDate) {
        Date actual = checkParameters(printer, overdueBalance);
        assertEquals(statementDate, actual);
    }

    /**
     * Verifies report parameters match those expected.
     *
     * @param printer        the statement printer
     * @param overdueBalance the expected overdue balance
     * @return the statement date
     */
    private Date checkParameters(StatementPrinter printer, int overdueBalance) {
        TemplatedReporter<FinancialAct> reporter = printer.getReporter();
        Map<String, Object> parameters = reporter.getParameters();
        checkEquals(overdueBalance, (BigDecimal) parameters.get("overdueBalance"));
        Date statementDate = (Date) parameters.get("statementDate");
        assertNotNull(statementDate);
        return statementDate;
    }

}
