/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountQueryFactory;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.insurance.InsuranceTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.internal.InsuranceFactory;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.act.ActEditDialog;
import org.openvpms.web.component.im.edit.payment.PaymentEditor;
import org.openvpms.web.component.im.edit.payment.PaymentItemEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.QueryHelper;
import org.openvpms.web.component.print.PrintDialog;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.credit.CreditActEditDialog;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.archetype.rules.finance.account.FinancialTestHelper.createChargesInvoice;
import static org.openvpms.archetype.test.TestHelper.createActIdentity;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Tests the {@link ClaimSubmitter}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ClaimSubmitterTestCase extends AbstractAppTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The insurance factory.
     */
    @Autowired
    private InsuranceFactory insuranceFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules rules;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test patient.
     */
    private Party patient;

    /**
     * The test clinician.
     */
    private User clinician;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The policy.
     */
    private Act policyAct;

    /**
     * The insurer.
     */
    private Party insurer;

    /**
     * The insurance service.
     */
    private TestGapInsuranceService insuranceService;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The till.
     */
    private Entity till;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        // NOTE: need to create the practice prior to the application as it caches the practice in the context
        practice = TestHelper.getPractice();
        super.setUp();
        customer = TestHelper.createCustomer();
        patient = TestHelper.createPatient(customer);
        clinician = TestHelper.createClinician();
        location = TestHelper.createLocation();
        till = TestHelper.createTill(location);

        // insurer
        insurer = (Party) InsuranceTestHelper.createInsurer(TestHelper.randomName("ZInsurer-"));

        // policy
        policyAct = (Act) InsuranceTestHelper.createPolicy(customer, patient, insurer,
                                                           createActIdentity("actIdentity.insurancePolicy", "POL123456"));
        save(policyAct);

        insuranceService = new TestGapInsuranceService();

        initDocumentTemplate(CustomerAccountArchetypes.PAYMENT, "Receipt");
        initDocumentTemplate(CustomerAccountArchetypes.REFUND, "Refund");
        initDocumentTemplate("INSURANCE_CLAIM_MEDICAL_RECORDS", "Insurance Claim Medical Records");
        initDocumentTemplate("INSURANCE_CLAIM_INVOICE", "Insurance Claim Invoice");

        initErrorHandler(errors);
    }

    /**
     * Tests the {@link ClaimSubmitter#submit(ClaimEditor, Consumer)} method when supplied with a claim that references
     * invoice items claimed by another claim.
     */
    @Test
    public void testDuplicate() {
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        FinancialAct invoice = createInvoice(POSTED, invoiceItem1, invoiceItem2);
        createPayment(invoice.getTotal());

        FinancialAct claim1Item = createClaimItem(invoiceItem1);
        FinancialAct claim1 = createClaim(policyAct, Claim.Status.PENDING, false, claim1Item);
        save(claim1, claim1Item, invoiceItem1);

        FinancialAct claim2Item = createClaimItem(invoiceItem1);
        FinancialAct claim2 = createClaim(policyAct, Claim.Status.PENDING, false, claim2Item);
        save(claim2, claim2Item, invoiceItem1);

        LocalContext context = new LocalContext();
        context.setPractice(practice);
        context.setLocation(location);
        context.setUser(clinician);
        context.setCustomer(customer);
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        ClaimEditor editor = new TestClaimEditor(claim1, layoutContext);
        editor.getComponent();

        checkDuplicate(editor, claim2, false);

        claim2.setStatus(Claim.Status.POSTED.toString());
        save(claim2);
        checkDuplicate(editor, claim2, false);

        claim2.setStatus(Claim.Status.ACCEPTED.toString());
        save(claim2);
        checkDuplicate(editor, claim2, false);

        claim2.setStatus(Claim.Status.SETTLED.toString());
        save(claim2);
        checkDuplicate(editor, claim2, false);

        claim2.setStatus(Claim.Status.DECLINED.toString());
        save(claim2);
        checkDuplicate(editor, claim2, true);

        claim2.setStatus(Claim.Status.CANCELLED.toString());
        save(claim2);
        checkDuplicate(editor, claim2, true);
    }

    /**
     * Tests the {@link ClaimSubmitter#submit(Act, Consumer)} method when supplied with a claim that references
     * invoice items claimed by another claim.
     */
    @Test
    public void testDuplicateAct() {
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claim1Item = createClaimItem(invoiceItem1);
        FinancialAct claim1 = createClaim(policyAct, Claim.Status.POSTED, false, claim1Item);
        save(claim1, claim1Item, invoiceItem1);

        FinancialAct claim2Item = createClaimItem(invoiceItem1);
        FinancialAct claim2 = createClaim(policyAct, Claim.Status.PENDING, false, claim2Item);
        save(claim2, claim2Item, invoiceItem1);

        checkDuplicate(claim1, claim2, false);

        claim2.setStatus(Claim.Status.POSTED.toString());
        save(claim2);
        checkDuplicate(claim1, claim2, false);

        claim2.setStatus(Claim.Status.ACCEPTED.toString());
        save(claim2);
        checkDuplicate(claim1, claim2, false);

        claim2.setStatus(Claim.Status.SETTLED.toString());
        save(claim2);
        checkDuplicate(claim1, claim2, false);

        claim2.setStatus(Claim.Status.DECLINED.toString());
        save(claim2);
        checkDuplicate(claim1, claim2, true);

        claim2.setStatus(Claim.Status.CANCELLED.toString());
        save(claim2);
        checkDuplicate(claim1, claim2, true);
    }

    /**
     * Tests paying the gap in a gap claim.
     * <p/>
     * A gap benefit amount payment till is configured on the practice location, so one is not prompted for.
     */
    @Test
    public void testPayGapClaim() {
        checkPayGapClaim(true);
    }

    /**
     * Tests paying the gap in a gap claim.
     * <p/>
     * No gap benefit amount payment till is configured on the practice location, so one will be prompted for.
     */
    @Test
    public void testPayGapClaimNoPreConfiguredTill() {
        checkPayGapClaim(false);
    }

    /**
     * Tests paying a gap claim in full.
     */
    @Test
    public void testPayFullGapClaim() {
        checkEquals(ZERO, rules.getBalance(customer));
        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claimItem = createClaimItem(invoiceItem1);
        FinancialAct claim = createClaim(policyAct, Claim.Status.POSTED, true, claimItem);
        save(claim, claimItem, invoiceItem1);

        ClaimSubmitter submitter = createSubmitter(context);
        submitter.submit(claim, Assert::assertNull);
        ConfirmationDialog confirm = findComponent(ConfirmationDialog.class);
        assertEquals("Submit Claim", confirm.getTitle());
        String message = "This claim will be submitted to " + insurer.getName() + " using Test Service.\n\n" +
                         "Submit claim?";
        assertEquals(message, confirm.getMessage());
        EchoTestHelper.fireDialogButton(confirm, ConfirmationDialog.YES_ID);

        BenefitDialog benefit = findComponent(BenefitDialog.class);
        assertEquals("Waiting for Claim Benefit", benefit.getTitle());
        assertEquals("The claim has been submitted to " + insurer.getName() + ".\n\n" +
                     "Please wait for them to determine the benefit amount.", benefit.getMessage());
        EchoTestHelper.fireDialogButton(benefit, BenefitDialog.PAY_FULL_CLAIM_ID);
        GapPaymentPrompt prompt = findComponent(GapPaymentPrompt.class);
        assertEquals("Pay Claim", prompt.getTitle());
        prompt.setPayFull(true);
        EchoTestHelper.fireDialogButton(prompt, ConfirmationDialog.OK_ID);

        // pay the claim
        CreditActEditDialog payment = pay(claim.getTotal());
        assertTrue(payment.getEditor().isValid());
        fireDialogButton(payment, PopupDialog.OK_ID);

        // verify the claim has been updated
        assertEquals(claim.getTotal(), getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());

        checkPrintDialog("Print Receipt?");

        // verify there are only two account acts for the customer, the original invoice, and the claim payment
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer,
                                                                       CustomerAccountArchetypes.ACCOUNT_ACTS);
        List<IMObject> objects = QueryHelper.query(query, getArchetypeService());
        assertEquals(2, objects.size());
        checkAccount(CustomerAccountArchetypes.INVOICE, BigDecimal.valueOf(18), objects);
        checkAccount(CustomerAccountArchetypes.PAYMENT, BigDecimal.valueOf(9), objects);

        // verify the gap status has been updated
        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // and the insurer notified
        assertEquals(1, insuranceService.getPaymentNotified());
        checkEquals(BigDecimal.valueOf(9), rules.getBalance(customer)); // only one item claimed
    }

    /**
     * Tests paying a gap claim in full, at the same time as the claim benefit is received.
     * <p/>
     * The payment dialog should roll back, but submission subsequently succeed.
     */
    @Test
    public void testUpdateBenefitWhenPayingFullGapClaim() {
        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claimItem = createClaimItem(invoiceItem1);
        FinancialAct claim = createClaim(policyAct, Claim.Status.POSTED, true, claimItem);
        save(claim, claimItem, invoiceItem1);

        ClaimSubmitter submitter = createSubmitter(context);
        submitter.submit(claim, Assert::assertNull);
        ConfirmationDialog confirm = findComponent(ConfirmationDialog.class);
        assertEquals("Submit Claim", confirm.getTitle());
        String message = "This claim will be submitted to " + insurer.getName() + " using Test Service.\n\n" +
                         "Submit claim?";
        assertEquals(message, confirm.getMessage());
        EchoTestHelper.fireDialogButton(confirm, ConfirmationDialog.YES_ID);

        BenefitDialog benefit = findComponent(BenefitDialog.class);
        assertEquals("Waiting for Claim Benefit", benefit.getTitle());
        assertEquals("The claim has been submitted to " + insurer.getName() + ".\n\n" +
                     "Please wait for them to determine the benefit amount.", benefit.getMessage());
        EchoTestHelper.fireDialogButton(benefit, BenefitDialog.PAY_FULL_CLAIM_ID);
        GapPaymentPrompt prompt = findComponent(GapPaymentPrompt.class);
        assertEquals("Pay Claim", prompt.getTitle());
        prompt.setPayFull(true);
        EchoTestHelper.fireDialogButton(prompt, ConfirmationDialog.OK_ID);

        // now simulate the claim benefit being received from the insurer
        claim = get(claim);
        claim.setStatus(Claim.Status.ACCEPTED.toString());
        claim.setStatus2(GapClaim.GapStatus.RECEIVED.toString());
        save(claim);

        CreditActEditDialog payment1 = pay(claim.getTotal());

        assertTrue(payment1.getEditor().isValid());
        fireDialogButton(payment1, PopupDialog.OK_ID);

        ErrorDialog error = findComponent(ErrorDialog.class);
        assertNotNull(error);
        assertEquals("Failed to save Insurance Claim. It may have been changed by another user.\n\nRetry?",
                     error.getMessage());
        fireDialogButton(error, PopupDialog.YES_ID);

        // verify the claim has been updated
        claim = get(claim);
        assertEquals(claim.getTotal(), getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());

        checkPrintDialog("Print Receipt?");

        // verify there are only two account acts for the customer, the original invoice, and the claim payment
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer,
                                                                       CustomerAccountArchetypes.ACCOUNT_ACTS);
        List<IMObject> objects = QueryHelper.query(query, getArchetypeService());
        assertEquals(2, objects.size());
        checkAccount(CustomerAccountArchetypes.INVOICE, BigDecimal.valueOf(18), objects);
        checkAccount(CustomerAccountArchetypes.PAYMENT, BigDecimal.valueOf(9), objects);

        // verify the gap status has been updated
        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // and the insurer notified
        assertEquals(1, insuranceService.getPaymentNotified());
    }

    /**
     * Verifies that if an invoice is partially paid, but less than the gap amount, a payment is collected
     * to make up the gap.
     */
    @Test
    public void testPayGapClaimForPartPaidInvoice() {
        Entity gapTill = TestHelper.createTill(location);
        addGapTill(gapTill, location);

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        // make a payment. This is less than the gap, so the difference will need to be paid
        BigDecimal prepaymentAmount = BigDecimal.valueOf(2);
        createPayment(prepaymentAmount);

        FinancialAct claimItem = createClaimItem(invoiceItem1);
        FinancialAct claim = createClaim(policyAct, Claim.Status.POSTED, true, claimItem);
        save(claim, claimItem, invoiceItem1);

        BigDecimal benefitAmount = BigDecimal.valueOf(5);
        BigDecimal gapAmount = BigDecimal.valueOf(4);
        BigDecimal remainingGap = gapAmount.subtract(prepaymentAmount);
        submitAndSelectPayGapClaim(claim, benefitAmount, prepaymentAmount, remainingGap, ZERO, context);

        CreditActEditDialog paymentDialog = findComponent(CreditActEditDialog.class);
        assertEquals("New Payment", paymentDialog.getTitle());
        GapPaymentEditor paymentEditor = (GapPaymentEditor) paymentDialog.getEditor();
        checkEquals(remainingGap, paymentEditor.getExpectedAmount());

        PaymentItemEditor paymentItemEditor = paymentEditor.getUnsavedItem();
        paymentItemEditor.setAmount(remainingGap);

        assertEquals(paymentEditor.getGapPaymentTill(), gapTill);
        assertTrue(paymentEditor.isValid());
        fireDialogButton(paymentDialog, PopupDialog.OK_ID);

        // verify the claim has been updated
        claim = get(claim);
        checkEquals(gapAmount, getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());

        checkPrintDialog("Print Receipt?");

        // verify there are four account acts for the customer, the original invoice, the claim prepayment,
        // the gap payment and payment for the benefit amount
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer,
                                                                       CustomerAccountArchetypes.ACCOUNT_ACTS);
        List<IMObject> objects = QueryHelper.query(query, getArchetypeService());
        assertEquals(4, objects.size());
        checkAccount(CustomerAccountArchetypes.INVOICE, BigDecimal.valueOf(18), objects);
        checkAccount(CustomerAccountArchetypes.PAYMENT, prepaymentAmount, objects);
        FinancialAct gapPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, remainingGap, objects);
        FinancialAct benefitPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(gapPayment, till);
        checkPaymentTill(benefitPayment, gapTill);

        // verify the gap status has been updated
        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // and the insurer notified
        assertEquals(1, insuranceService.getPaymentNotified());
    }

    /**
     * Verifies a gap claim can be submitted when the are already payments against the invoice.
     * <p/>
     * A refund will be made to refund down to the gap amount.
     * <p/>
     * A gap benefit amount payment till is configured on the practice location, so one is not prompted for.
     */
    @Test
    public void testRefundToMakeGapClaim() {
        checkRefundToPayGapClaim(true);
    }

    /**
     * /**
     * Verifies a gap claim can be submitted when the are already payments against the invoice.
     * <p/>
     * A refund will be made to refund down to the gap amount.
     * <p/>
     * No gap benefit amount payment till is configured on the practice location, so one will be prompted for.
     */
    @Test
    public void testRefundToMakeGapClaimNoPreConfiguredTill() {
        checkRefundToPayGapClaim(false);
    }

    /**
     * Verifies that two claims can be made on the same invoice that holds charges for different patients.
     */
    @Test
    public void testTwoClaimsForDifferentPatientOnSameInvoice() {
        checkEquals(ZERO, rules.getBalance(customer));
        Entity gapTill = TestHelper.createTill(location);
        addGapTill(gapTill, location);

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem(patient);
        Party patient2 = TestHelper.createPatient(customer);
        Act policyAct2 = (Act) InsuranceTestHelper.createPolicy(
                customer, patient2, insurer, createActIdentity("actIdentity.insurancePolicy", "POL2222"));
        save(policyAct2);

        FinancialAct invoiceItem2 = createInvoiceItem(patient2);
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claim1Item = createClaimItem(invoiceItem1);
        FinancialAct claim1 = createClaim(policyAct, Claim.Status.POSTED, true, claim1Item);
        save(claim1, claim1Item, invoiceItem1);

        FinancialAct claim2Item = createClaimItem(invoiceItem2);
        FinancialAct claim2 = createClaim(policyAct2, Claim.Status.POSTED, true, claim2Item);
        save(claim2, claim2Item, invoiceItem2);

        BigDecimal invoiceAmount = BigDecimal.valueOf(18);
        BigDecimal benefitAmount = BigDecimal.valueOf(5);
        BigDecimal gapAmount = BigDecimal.valueOf(4);

        submitAndSelectPayGapClaim(claim1, benefitAmount, ZERO, gapAmount, ZERO, context);
        makeGapPayment(claim1, gapAmount, gapAmount, gapTill, 1);

        // verify the insurer was notified
        assertEquals(1, insuranceService.getPaymentNotified());

        // verify there are three account acts for the customer, the original invoice, the claim payment and
        // a payment for the benefit amount
        List<IMObject> acts1 = getAccountActs(3);
        FinancialAct invoice = checkAccount(CustomerAccountArchetypes.INVOICE, invoiceAmount, acts1);
        FinancialAct gapPayment1 = checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount, acts1);
        FinancialAct benefitPayment1 = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, acts1);
        checkPaymentTill(gapPayment1, till);
        checkPaymentTill(benefitPayment1, gapTill);

        checkEquals(BigDecimal.valueOf(9), invoice.getAllocatedAmount());
        checkEquals(gapAmount, gapPayment1.getAllocatedAmount());
        checkEquals(benefitAmount, benefitPayment1.getAllocatedAmount());

        checkEquals(BigDecimal.valueOf(9), rules.getBalance(customer));

        // now submit the second claim
        submitAndSelectPayGapClaim(claim2, benefitAmount, ZERO, gapAmount, ZERO, context);
        makeGapPayment(claim2, gapAmount, gapAmount, gapTill, 2);

        // verify there are 2 additional payments, representing the gap payment and benefit
        List<IMObject> acts2 = getAccountActs(5);
        acts2.remove(invoice);
        acts2.remove(gapPayment1);
        acts2.remove(benefitPayment1);
        assertEquals(2, acts2.size());
        checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount, acts2);
        checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, acts2);

        // customer account balance should now be zero
        checkEquals(ZERO, rules.getBalance(customer));
    }

    /**
     * Verifies that two claims can be made on the same invoice that holds charges for different patients,
     * where a deposit has been made that is less than the gap.
     */
    @Test
    public void testTwoClaimsForDifferentPatientOnSameInvoiceWithPrepaymentLessThanGap() {
        checkEquals(ZERO, rules.getBalance(customer));
        Entity gapTill = TestHelper.createTill(location);
        addGapTill(gapTill, location);

        // add a deposit
        BigDecimal depositAmount = BigDecimal.valueOf(3);
        createPayment(depositAmount);

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem(patient);
        Party patient2 = TestHelper.createPatient(customer);
        Act policyAct2 = (Act) InsuranceTestHelper.createPolicy(
                customer, patient2, insurer, createActIdentity("actIdentity.insurancePolicy", "POL2222"));
        save(policyAct2);

        FinancialAct invoiceItem2 = createInvoiceItem(patient2);
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claim1Item = createClaimItem(invoiceItem1);
        FinancialAct claim1 = createClaim(policyAct, Claim.Status.POSTED, true, claim1Item);
        save(claim1, claim1Item, invoiceItem1);

        FinancialAct claim2Item = createClaimItem(invoiceItem2);
        FinancialAct claim2 = createClaim(policyAct2, Claim.Status.POSTED, true, claim2Item);
        save(claim2, claim2Item, invoiceItem2);

        BigDecimal invoiceAmount = BigDecimal.valueOf(18);
        BigDecimal benefitAmount = BigDecimal.valueOf(5);
        BigDecimal gapAmount = BigDecimal.valueOf(4);

        submitAndSelectPayGapClaim(claim1, benefitAmount, depositAmount, ONE, ZERO, context);
        makeGapPayment(claim1, gapAmount.subtract(depositAmount), gapAmount, gapTill, 1);

        // verify there are four account acts for the customer, the deposit, the original invoice, the claim payment and
        // a payment for the benefit amount
        List<IMObject> objects = getAccountActs(4);
        checkAccount(CustomerAccountArchetypes.PAYMENT, depositAmount, objects);
        FinancialAct deposit = checkAccount(CustomerAccountArchetypes.PAYMENT, depositAmount, objects);
        FinancialAct invoice = checkAccount(CustomerAccountArchetypes.INVOICE, invoiceAmount, objects);
        FinancialAct gapPayment1 = checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount.subtract(depositAmount),
                                                objects);
        FinancialAct benefitPayment1 = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(gapPayment1, till);
        checkPaymentTill(benefitPayment1, gapTill);

        checkEquals(BigDecimal.valueOf(9), invoice.getAllocatedAmount());
        checkEquals(gapAmount.subtract(depositAmount), gapPayment1.getAllocatedAmount());
        checkEquals(benefitAmount, benefitPayment1.getAllocatedAmount());

        checkEquals(BigDecimal.valueOf(9), rules.getBalance(customer));

        // now submit the second claim
        submitAndSelectPayGapClaim(claim2, benefitAmount, ZERO, gapAmount, ZERO, context);
        makeGapPayment(claim2, gapAmount, gapAmount, gapTill, 2);

        // verify there are 2 additional payments, representing the gap payment and benefit
        List<IMObject> acts2 = getAccountActs(6);
        acts2.remove(deposit);
        acts2.remove(invoice);
        acts2.remove(gapPayment1);
        acts2.remove(benefitPayment1);
        assertEquals(2, acts2.size());
        checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount, acts2);
        checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, acts2);

        // customer account balance should now be zero
        checkEquals(ZERO, rules.getBalance(customer));
    }

    /**
     * Verifies that two claims can be made on the same invoice that holds charges for different patients,
     * where a deposit has been made that is equal to the gap.
     * Here, no gap payment is prompted for, for the first claim.
     */
    @Test
    public void testTwoClaimsForDifferentPatientOnSameInvoiceWithPrepaymentEqualsToGap() {
        checkEquals(ZERO, rules.getBalance(customer));
        Entity gapTill = TestHelper.createTill(location);
        addGapTill(gapTill, location);

        // add a deposit
        BigDecimal depositAmount = BigDecimal.valueOf(4);
        FinancialAct deposit = createPayment(depositAmount);

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem(patient);
        Party patient2 = TestHelper.createPatient(customer);
        Act policyAct2 = (Act) InsuranceTestHelper.createPolicy(
                customer, patient2, insurer, createActIdentity("actIdentity.insurancePolicy", "POL2222"));
        save(policyAct2);

        FinancialAct invoiceItem2 = createInvoiceItem(patient2);
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claim1Item = createClaimItem(invoiceItem1);
        FinancialAct claim1 = createClaim(policyAct, Claim.Status.POSTED, true, claim1Item);
        save(claim1, claim1Item, invoiceItem1);

        FinancialAct claim2Item = createClaimItem(invoiceItem2);
        FinancialAct claim2 = createClaim(policyAct2, Claim.Status.POSTED, true, claim2Item);
        save(claim2, claim2Item, invoiceItem2);

        BigDecimal invoiceAmount = BigDecimal.valueOf(18);
        BigDecimal benefitAmount = BigDecimal.valueOf(5);
        BigDecimal gapAmount = BigDecimal.valueOf(4); // i.e. same as deposit

        submitAndSelectPayGapClaim(claim1, benefitAmount, depositAmount, ZERO, ZERO, context);
        // NOTE: no gap payment required, due to deposit

        // verify the claim has been updated and the insurer notified
        claim1 = get(claim1);
        checkEquals(gapAmount, getBean(claim1).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim1.getStatus2());
        assertEquals(1, insuranceService.getPaymentNotified());

        // verify there are 3 account acts for the customer, the deposit, the original invoice, and a payment for the
        // benefit amount
        List<IMObject> objects = getAccountActs(3);
        assertTrue(objects.contains(deposit));
        FinancialAct invoice = checkAccount(CustomerAccountArchetypes.INVOICE, invoiceAmount, objects);
        FinancialAct benefitPayment1 = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(benefitPayment1, gapTill);

        checkEquals(BigDecimal.valueOf(9), invoice.getAllocatedAmount());
        checkEquals(benefitAmount, benefitPayment1.getAllocatedAmount());

        checkEquals(BigDecimal.valueOf(9), rules.getBalance(customer));

        // now submit the second claim and select pay gap
        submitAndSelectPayGapClaim(claim2, benefitAmount, ZERO, gapAmount, ZERO, context);
        makeGapPayment(claim2, depositAmount, depositAmount, gapTill, 2);

        // verify there are 2 additional payments, representing the gap payment and benefit
        List<IMObject> acts2 = getAccountActs(5);
        acts2.remove(deposit);
        acts2.remove(invoice);
        acts2.remove(benefitPayment1);
        assertEquals(2, acts2.size());
        checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount, acts2);
        checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, acts2);

        // customer account balance should now be zero
        checkEquals(ZERO, rules.getBalance(customer));
    }

    /**
     * Verifies that two claims can be made on the same invoice that holds charges for different patients,
     * where a deposit has been made that is greater than the gap.
     * Here, a refund is made for the first claim.
     * NOTE: after both claims have completed, the customer account balance will be the same value as the refund.
     * This is unavoidable at present. The alternative would be to prompt if the user wants to make a refund. TODO
     */
    @Test
    public void testTwoClaimsForDifferentPatientOnSameInvoiceWithPrepaymentGreaterThanGap() {
        checkEquals(ZERO, rules.getBalance(customer));
        Entity gapTill = TestHelper.createTill(location);
        addGapTill(gapTill, location);

        // add a deposit
        BigDecimal depositAmount = BigDecimal.valueOf(7);
        FinancialAct deposit = createPayment(depositAmount);

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem(patient);
        Party patient2 = TestHelper.createPatient(customer);
        Act policyAct2 = (Act) InsuranceTestHelper.createPolicy(
                customer, patient2, insurer, createActIdentity("actIdentity.insurancePolicy", "POL2222"));
        save(policyAct2);

        FinancialAct invoiceItem2 = createInvoiceItem(patient2);
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claim1Item = createClaimItem(invoiceItem1);
        FinancialAct claim1 = createClaim(policyAct, Claim.Status.POSTED, true, claim1Item);
        save(claim1, claim1Item, invoiceItem1);

        FinancialAct claim2Item = createClaimItem(invoiceItem2);
        FinancialAct claim2 = createClaim(policyAct2, Claim.Status.POSTED, true, claim2Item);
        save(claim2, claim2Item, invoiceItem2);

        BigDecimal invoiceAmount = BigDecimal.valueOf(18);
        BigDecimal benefitAmount = BigDecimal.valueOf(5);
        BigDecimal gapAmount = BigDecimal.valueOf(4);
        BigDecimal refundAmount = depositAmount.subtract(gapAmount);
        submitAndSelectPayGapClaim(claim1, benefitAmount, depositAmount, BigDecimal.valueOf(2), refundAmount, context);
        // Expect refund as the deposit is greater than the gap
        makeGapRefund(claim1, refundAmount, gapAmount, gapTill, 1);

        // verify there are 4 account acts for the customer, the deposit, the original invoice, the refund, and
        // a payment for the benefit amount
        List<IMObject> objects = getAccountActs(4);
        assertTrue(objects.contains(deposit));
        FinancialAct invoice = checkAccount(CustomerAccountArchetypes.INVOICE, invoiceAmount, objects);
        FinancialAct refund = checkAccount(CustomerAccountArchetypes.REFUND, refundAmount, objects);
        FinancialAct benefitPayment1 = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(benefitPayment1, gapTill);

        checkEquals(BigDecimal.valueOf(12), invoice.getAllocatedAmount()); // +3 due to the refund
        checkEquals(benefitAmount, benefitPayment1.getAllocatedAmount());

        checkEquals(BigDecimal.valueOf(9), rules.getBalance(customer));

        // now submit the second claim and select pay gap
        submitAndSelectPayGapClaim(claim2, benefitAmount, refundAmount, gapAmount.subtract(refundAmount), ZERO,
                                   context);
        makeGapPayment(claim2, gapAmount.subtract(refundAmount), gapAmount, gapTill, 2);

        // verify there are 2 additional payments, representing the gap payment and benefit
        List<IMObject> acts2 = getAccountActs(6);
        acts2.remove(deposit);
        acts2.remove(invoice);
        acts2.remove(refund);
        acts2.remove(benefitPayment1);
        assertEquals(2, acts2.size());
        checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount.subtract(refundAmount), acts2);
        checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, acts2);

        // customer account balance should now be the value of the refund amount
        checkEquals(refundAmount, rules.getBalance(customer));
    }

    /**
     * Verifies that the claim customer is used when paying the gap, when there is a different customer in the context.
     */
    @Test
    public void testSubmitGapPaymentWithDifferentContextCustomer() {
        checkEquals(ZERO, rules.getBalance(customer));
        Entity gapTill = TestHelper.createTill(location);
        addGapTill(gapTill, location);

        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claimItem = createClaimItem(invoiceItem1, invoiceItem2);
        FinancialAct claim = createClaim(policyAct, Claim.Status.POSTED, true, claimItem);
        save(claim, claimItem, invoiceItem1, invoiceItem2);

        // submit the claim with a different context customer
        Context context = createContext(till);
        context.setCustomer(TestHelper.createCustomer());

        ClaimSubmitter submitter = createSubmitter(context);
        submitter.submit(claim, Assert::assertNull);
        ConfirmationDialog confirm = findComponent(ConfirmationDialog.class);
        assertEquals("Submit Claim", confirm.getTitle());
        String message = "This claim will be submitted to " + insurer.getName() + " using Test Service.\n\n" +
                         "Submit claim?";
        assertEquals(message, confirm.getMessage());
        EchoTestHelper.fireDialogButton(confirm, ConfirmationDialog.YES_ID);

        BenefitDialog benefit = findComponent(BenefitDialog.class);
        assertEquals("Waiting for Claim Benefit", benefit.getTitle());
        assertEquals("The claim has been submitted to " + insurer.getName() + ".\n\n" +
                     "Please wait for them to determine the benefit amount.", benefit.getMessage());

        BigDecimal invoiceAmount = BigDecimal.valueOf(18);
        BigDecimal benefitAmount = BigDecimal.valueOf(10);
        BigDecimal gapAmount = BigDecimal.valueOf(8);
        benefit.getClaim().setBenefit(benefitAmount, "Approved");
        benefit.refresh();

        GapPaymentPrompt prompt = findComponent(GapPaymentPrompt.class);
        assertEquals("Pay Claim", prompt.getTitle());
        prompt.setPayGap(true);
        EchoTestHelper.fireDialogButton(prompt, ConfirmationDialog.OK_ID);

        CreditActEditDialog payment = findComponent(CreditActEditDialog.class);
        assertEquals("New Payment", payment.getTitle());
        GapPaymentEditor paymentEditor = (GapPaymentEditor) payment.getEditor();
        assertEquals(customer, paymentEditor.getCustomer());  // verify the correct customer is used
        PaymentItemEditor paymentItemEditor = paymentEditor.getUnsavedItem();
        paymentItemEditor.setAmount(gapAmount);

        assertEquals(paymentEditor.getGapPaymentTill(), gapTill);
        assertTrue(paymentEditor.isValid());
        fireDialogButton(payment, PopupDialog.OK_ID); // will trigger the dialog validation

        // verify the claim has been updated
        claim = get(claim);
        checkEquals(gapAmount, getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());

        checkPrintDialog("Print Receipt?");

        // verify there are three account acts for the customer, the original invoice, the claim payment and
        // a payment for the benefit amount
        List<IMObject> objects = getAccountActs(3);
        FinancialAct invoice = checkAccount(CustomerAccountArchetypes.INVOICE, invoiceAmount, objects);
        FinancialAct gapPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount, objects);
        FinancialAct benefitPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(gapPayment, till);
        checkPaymentTill(benefitPayment, gapTill);

        checkEquals(invoiceAmount, invoice.getAllocatedAmount());
        checkEquals(gapAmount, gapPayment.getAllocatedAmount());
        checkEquals(benefitAmount, benefitPayment.getAllocatedAmount());

        // verify the gap status has been updated
        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // and the insurer notified
        assertEquals(1, insuranceService.getPaymentNotified());

        checkEquals(ZERO, rules.getBalance(customer));
    }

    /**
     * Creates a claim item.
     *
     * @param invoiceItems the invoice items to claim
     * @return the claim
     */
    private FinancialAct createClaimItem(FinancialAct... invoiceItems) {
        return (FinancialAct) InsuranceTestHelper.createClaimItem("VENOM_328", new Date(), new Date(), invoiceItems);
    }

    /**
     * Creates a claim.
     *
     * @param policy   the policy
     * @param status   the claim status
     * @param gapClaim if {@code true}, the claim is a gap claim
     * @param items    the claim items
     * @return the claim
     */
    private FinancialAct createClaim(Act policy, Claim.Status status, boolean gapClaim, FinancialAct... items) {
        FinancialAct claim = (FinancialAct) InsuranceTestHelper.createClaim(policy, location, clinician, clinician,
                                                                            gapClaim, items);
        claim.setStatus(status.toString());
        return claim;
    }

    /**
     * Creates and saves a POSTED payment for the customer.
     *
     * @param amount the payment amount
     * @return the payment
     */
    private FinancialAct createPayment(BigDecimal amount) {
        FinancialAct deposit = FinancialTestHelper.createPayment(amount, customer, till, POSTED);
        save(deposit);
        return deposit;
    }

    /**
     * Returns the customer account acts.
     *
     * @param expected the expected number of acts
     * @return the customer account acts
     */
    private List<IMObject> getAccountActs(int expected) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer,
                                                                       CustomerAccountArchetypes.ACCOUNT_ACTS);
        List<IMObject> objects = QueryHelper.query(query, getArchetypeService());
        assertEquals(expected, objects.size());
        return objects;
    }

    /**
     * Makes a gap payment and verify the gap claim updates.
     *
     * @param claim              the claim
     * @param amount             the payment amount
     * @param expectedPaidAmount the expected paid amount on the claim
     * @param expectedTill       the expected till
     * @param notifications      the expected no. of insurance service payment notifications
     * @return the updated claim
     */
    private FinancialAct makeGapPayment(FinancialAct claim, BigDecimal amount, BigDecimal expectedPaidAmount,
                                        Entity expectedTill, int notifications) {
        CreditActEditDialog payment = findComponent(CreditActEditDialog.class);
        assertEquals("New Payment", payment.getTitle());
        claim = makePaymentOrRefund(payment, claim, amount, expectedPaidAmount, expectedTill);

        // verify a print dialog is displayed for the receipt and cancel it
        checkPrintDialog("Print Receipt?");

        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // verify the insurer was notified
        assertEquals(notifications, insuranceService.getPaymentNotified());

        return claim;
    }

    /**
     * Refunds down to the gap amount and verify the gap claim updates.
     *
     * @param claim              the claim
     * @param amount             the refund amount
     * @param expectedPaidAmount the expected paid amount on the claim
     * @param expectedTill       the expected till
     * @param notifications      the expected no. of insurance service payment notifications
     * @return the updated claim
     */
    private FinancialAct makeGapRefund(FinancialAct claim, BigDecimal amount, BigDecimal expectedPaidAmount,
                                       Entity expectedTill, int notifications) {
        ActEditDialog payment = findComponent(ActEditDialog.class);
        assertEquals("New Refund", payment.getTitle());
        claim = makePaymentOrRefund(payment, claim, amount, expectedPaidAmount, expectedTill);

        // verify a print dialog is displayed for the receipt and cancel it
        checkPrintDialog("Print Refund?");

        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // verify the insurer was notified
        assertEquals(notifications, insuranceService.getPaymentNotified());

        return claim;
    }

    /**
     * Makes a gap payment or refund.
     *
     * @param dialog             the dialog
     * @param claim              the claim
     * @param amount             the amount to pay or refund
     * @param expectedPaidAmount the expected paid amount on the claim
     * @param expectedTill       the expected till
     * @return the claim
     */
    private FinancialAct makePaymentOrRefund(ActEditDialog dialog, FinancialAct claim, BigDecimal amount,
                                             BigDecimal expectedPaidAmount, Entity expectedTill) {
        GapPaymentEditor paymentEditor = (GapPaymentEditor) dialog.getEditor();
        PaymentItemEditor paymentItemEditor = paymentEditor.getUnsavedItem();
        paymentItemEditor.setAmount(amount);

        assertEquals(paymentEditor.getGapPaymentTill(), expectedTill);
        assertTrue(paymentEditor.isValid());
        fireDialogButton(dialog, PopupDialog.OK_ID);

        // verify the claim has been updated
        claim = get(claim);
        checkEquals(expectedPaidAmount, getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());
        return claim;
    }

    /**
     * Verifies a print dialog is displayed, with the expected title.
     *
     * @param expectedTitle the expected title
     */
    private void checkPrintDialog(String expectedTitle) {
        PrintDialog print = findComponent(PrintDialog.class);
        assertEquals(expectedTitle, print.getTitle());
        fireDialogButton(print, PopupDialog.CANCEL_ID);
    }

    /**
     * Submits and pays a gap claim.
     *
     * @param claim            the claim
     * @param benefitAmount    the benefit amount
     * @param expectedPaid     the expected paid amount
     * @param expectedToPay    the expected amount to pay
     * @param expectedToRefund the expected amount to refund
     * @param context          the context
     */
    private void submitAndSelectPayGapClaim(FinancialAct claim, BigDecimal benefitAmount, BigDecimal expectedPaid,
                                            BigDecimal expectedToPay, BigDecimal expectedToRefund, Context context) {
        ClaimSubmitter submitter = createSubmitter(context);
        submitter.submit(claim, Assert::assertNull);
        ConfirmationDialog confirm = findComponent(ConfirmationDialog.class);
        assertEquals("Submit Claim", confirm.getTitle());
        String message = "This claim will be submitted to " + insurer.getName() + " using Test Service.\n\n" +
                         "Submit claim?";
        assertEquals(message, confirm.getMessage());
        EchoTestHelper.fireDialogButton(confirm, ConfirmationDialog.YES_ID);

        BenefitDialog benefit = findComponent(BenefitDialog.class);
        assertEquals("Waiting for Claim Benefit", benefit.getTitle());
        assertEquals("The claim has been submitted to " + insurer.getName() + ".\n\n" +
                     "Please wait for them to determine the benefit amount.", benefit.getMessage());

        benefit.getClaim().setBenefit(benefitAmount, "Approved");
        benefit.refresh();

        GapPaymentPrompt prompt = findComponent(GapPaymentPrompt.class);
        assertEquals("Pay Claim", prompt.getTitle());
        prompt.setPayGap(true);
        checkEquals(expectedPaid, prompt.getPaid());
        checkEquals(expectedToPay, prompt.getAmountToPay());
        checkEquals(expectedToRefund, prompt.getAmountToRefund());
        EchoTestHelper.fireDialogButton(prompt, ConfirmationDialog.OK_ID);
    }

    /**
     * Adds a gap benefit till to a practice location.
     *
     * @param gapTill  the gap benefit till
     * @param location the practice location
     */
    private void addGapTill(Entity gapTill, Party location) {
        IMObjectBean bean = getBean(location);
        bean.setTarget("gapBenefitTill", gapTill);
        bean.save();
    }

    /**
     * Checks paying a gap claim.
     *
     * @param preConfiguredTill if {@code true}, pre-configure a till on the practice location
     */
    private void checkPayGapClaim(boolean preConfiguredTill) {
        checkEquals(ZERO, rules.getBalance(customer));
        Entity gapTill = TestHelper.createTill(location);
        if (preConfiguredTill) {
            addGapTill(gapTill, location);
        }

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        FinancialAct claimItem = createClaimItem(invoiceItem1, invoiceItem2);
        FinancialAct claim = createClaim(policyAct, Claim.Status.POSTED, true, claimItem);
        save(claim, claimItem, invoiceItem1, invoiceItem2);

        ClaimSubmitter submitter = createSubmitter(context);
        submitter.submit(claim, Assert::assertNull);
        ConfirmationDialog confirm = findComponent(ConfirmationDialog.class);
        assertEquals("Submit Claim", confirm.getTitle());
        String message = "This claim will be submitted to " + insurer.getName() + " using Test Service.\n\n" +
                         "Submit claim?";
        assertEquals(message, confirm.getMessage());
        EchoTestHelper.fireDialogButton(confirm, ConfirmationDialog.YES_ID);

        BenefitDialog benefit = findComponent(BenefitDialog.class);
        assertEquals("Waiting for Claim Benefit", benefit.getTitle());
        assertEquals("The claim has been submitted to " + insurer.getName() + ".\n\n" +
                     "Please wait for them to determine the benefit amount.", benefit.getMessage());

        BigDecimal invoiceAmount = BigDecimal.valueOf(18);
        BigDecimal benefitAmount = BigDecimal.valueOf(10);
        BigDecimal gapAmount = BigDecimal.valueOf(8);
        benefit.getClaim().setBenefit(benefitAmount, "Approved");
        benefit.refresh();

        GapPaymentPrompt prompt = findComponent(GapPaymentPrompt.class);
        assertEquals("Pay Claim", prompt.getTitle());
        prompt.setPayGap(true);
        EchoTestHelper.fireDialogButton(prompt, ConfirmationDialog.OK_ID);

        CreditActEditDialog payment = findComponent(CreditActEditDialog.class);
        assertEquals("New Payment", payment.getTitle());
        GapPaymentEditor paymentEditor = (GapPaymentEditor) payment.getEditor();
        PaymentItemEditor paymentItemEditor = paymentEditor.getUnsavedItem();
        paymentItemEditor.setAmount(gapAmount);

        if (preConfiguredTill) {
            assertEquals(paymentEditor.getGapPaymentTill(), gapTill);
            assertTrue(paymentEditor.isValid());
            fireDialogButton(payment, PopupDialog.OK_ID); // will trigger the dialog validation
        } else {
            assertNull(paymentEditor.getGapPaymentTill());
            fireDialogButton(payment, PopupDialog.OK_ID); // will trigger the dialog validation
            assertFalse(paymentEditor.isValid());
            paymentEditor.setGapPaymentTill(gapTill);
            assertTrue(paymentEditor.isValid());
            fireDialogButton(payment, PopupDialog.OK_ID);
        }

        // verify the claim has been updated
        claim = get(claim);
        checkEquals(gapAmount, getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());

        checkPrintDialog("Print Receipt?");

        // verify there are three account acts for the customer, the original invoice, the claim payment and
        // a payment for the benefit amount
        List<IMObject> objects = getAccountActs(3);
        FinancialAct invoice = checkAccount(CustomerAccountArchetypes.INVOICE, invoiceAmount, objects);
        FinancialAct gapPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, gapAmount, objects);
        FinancialAct benefitPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(gapPayment, till);
        checkPaymentTill(benefitPayment, gapTill);

        checkEquals(invoiceAmount, invoice.getAllocatedAmount());
        checkEquals(gapAmount, gapPayment.getAllocatedAmount());
        checkEquals(benefitAmount, benefitPayment.getAllocatedAmount());

        // verify the gap status has been updated
        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // and the insurer notified
        assertEquals(1, insuranceService.getPaymentNotified());

        checkEquals(ZERO, rules.getBalance(customer));
    }

    /**
     * Creates a context populated with the clinician, till, practice location, customer and practice.
     *
     * @param till the till
     * @return a new context
     */
    private Context createContext(Entity till) {
        Context context = new LocalContext();
        context.setUser(clinician);
        context.setTill((Party) till);
        context.setLocation(location);
        context.setCustomer(customer);
        context.setPractice(practice);
        return context;
    }

    /**
     * Checks paying a gap claim where a partial amount needs to be refunded before the claim is made.
     * <p/>
     * This occurs if a partial payment (e.g a deposit) is made before claim submission that exceeds the gap.
     * The difference needs to be refunded.
     *
     * @param preConfiguredTill if {@code true}, pre-configure a till on the practice location
     */
    private void checkRefundToPayGapClaim(boolean preConfiguredTill) {
        Entity gapTill = TestHelper.createTill(location);
        if (preConfiguredTill) {
            addGapTill(gapTill, location);
        }

        Context context = createContext(till);
        FinancialAct invoiceItem1 = createInvoiceItem();
        FinancialAct invoiceItem2 = createInvoiceItem();
        createInvoice(POSTED, invoiceItem1, invoiceItem2);

        // make a deposit. This exceeds the gap so will need to be partially refunded
        createPayment(BigDecimal.TEN);

        FinancialAct claimItem = createClaimItem(invoiceItem1);
        FinancialAct claim = createClaim(policyAct, Claim.Status.POSTED, true, claimItem);
        save(claim, claimItem, invoiceItem1);

        ClaimSubmitter submitter = createSubmitter(context);
        submitter.submit(claim, Assert::assertNull);
        ConfirmationDialog confirm = findComponent(ConfirmationDialog.class);
        assertEquals("Submit Claim", confirm.getTitle());
        String message = "This claim will be submitted to " + insurer.getName() + " using Test Service.\n\n" +
                         "Submit claim?";
        assertEquals(message, confirm.getMessage());
        EchoTestHelper.fireDialogButton(confirm, ConfirmationDialog.YES_ID);

        BenefitDialog benefit = findComponent(BenefitDialog.class);
        assertEquals("Waiting for Claim Benefit", benefit.getTitle());
        assertEquals("The claim has been submitted to " + insurer.getName() + ".\n\n" +
                     "Please wait for them to determine the benefit amount.", benefit.getMessage());

        BigDecimal benefitAmount = BigDecimal.valueOf(5);
        BigDecimal gapAmount = BigDecimal.valueOf(4);
        benefit.getClaim().setBenefit(benefitAmount, "Approved");
        benefit.refresh();

        GapPaymentPrompt prompt = findComponent(GapPaymentPrompt.class);
        assertEquals("Pay Claim", prompt.getTitle());
        prompt.setPayGap(true);
        EchoTestHelper.fireDialogButton(prompt, ConfirmationDialog.OK_ID);

        ActEditDialog refundDialog = findComponent(ActEditDialog.class);
        assertEquals("New Refund", refundDialog.getTitle());
        GapPaymentEditor refundEditor = (GapPaymentEditor) refundDialog.getEditor();
        BigDecimal refundAmount = BigDecimal.valueOf(6);
        checkEquals(refundAmount, refundEditor.getExpectedAmount());
        PaymentItemEditor refundItemEditor = refundEditor.getUnsavedItem();
        refundItemEditor.setAmount(refundAmount);

        if (preConfiguredTill) {
            assertEquals(refundEditor.getGapPaymentTill(), gapTill);
            assertTrue(refundEditor.isValid());
            fireDialogButton(refundDialog, PopupDialog.OK_ID);
        } else {
            assertNull(refundEditor.getGapPaymentTill());
            fireDialogButton(refundDialog, PopupDialog.OK_ID);
            assertFalse(refundEditor.isValid());
            refundEditor.setGapPaymentTill(gapTill); // will trigger the dialog validation
            assertTrue(refundEditor.isValid());
            fireDialogButton(refundDialog, PopupDialog.OK_ID);
        }

        // verify the claim has been updated
        claim = get(claim);
        checkEquals(gapAmount, getBean(claim).getBigDecimal("paid"));
        assertEquals(GapClaim.GapStatus.PAID.toString(), claim.getStatus2());

        checkPrintDialog("Print Refund?");

        // verify there are four account acts for the customer, the original invoice, the the deposit payment,
        // the refund and a payment for the benefit amount
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer,
                                                                       CustomerAccountArchetypes.ACCOUNT_ACTS);
        List<IMObject> objects = QueryHelper.query(query, getArchetypeService());
        assertEquals(4, objects.size());
        checkAccount(CustomerAccountArchetypes.INVOICE, BigDecimal.valueOf(18), objects);
        checkAccount(CustomerAccountArchetypes.PAYMENT, BigDecimal.TEN, objects); // deposit payment
        checkAccount(CustomerAccountArchetypes.REFUND, refundAmount, objects);    // refund down to gap
        FinancialAct benefitPayment = checkAccount(CustomerAccountArchetypes.PAYMENT, benefitAmount, objects);
        checkPaymentTill(benefitPayment, gapTill);

        // verify the gap status has been updated
        claim = get(claim);
        assertEquals(GapClaim.GapStatus.NOTIFIED.toString(), claim.getStatus2());

        // and the insurer notified
        assertEquals(1, insuranceService.getPaymentNotified());
    }

    /**
     * Verifies a till on a payment matches that expected.
     *
     * @param payment the payment
     * @param till    the expected till
     */
    private void checkPaymentTill(FinancialAct payment, Entity till) {
        IMObjectBean bean = getBean(payment);
        assertEquals(till.getObjectReference(), bean.getTargetRef("till"));
    }

    /**
     * Pays a claim.
     *
     * @param amount the amount to pay
     * @return the payment edit dialog
     */
    private CreditActEditDialog pay(BigDecimal amount) {
        CreditActEditDialog payment = findComponent(CreditActEditDialog.class);
        assertEquals("New Payment", payment.getTitle());
        PaymentEditor paymentEditor = (PaymentEditor) payment.getEditor();
        PaymentItemEditor paymentItemEditor = paymentEditor.getUnsavedItem();
        paymentItemEditor.getProperty("amount").setValue(amount);
        return payment;
    }

    /**
     * Verifies that a {@code POSTED} customer account act exists with the specified archetype and total, in a list of
     * acts.
     *
     * @param archetype the expected archetype
     * @param total     the expected total
     * @param acts      the acts to search
     * @return the matching act
     */
    private FinancialAct checkAccount(String archetype, BigDecimal total, List<IMObject> acts) {
        FinancialAct result = null;
        for (IMObject object : acts) {
            if (object.isA(archetype)) {
                FinancialAct act = (FinancialAct) object;
                assertEquals(ActStatus.POSTED, act.getStatus());
                if (act.getTotal().compareTo(total) == 0) {
                    result = act;
                    break;
                }
            }
        }
        assertNotNull(result);
        return result;
    }

    /**
     * Creates a dummy document template, if one doesn't exist.
     *
     * @param type the template type
     * @param name the template name
     */
    private void initDocumentTemplate(String type, String name) {
        Entity template = new TemplateHelper(getArchetypeService()).getTemplateForArchetype(type);
        if (template == null) {
            documentFactory.newTemplate()
                    .name(name)
                    .type(type)
                    .blankDocument()
                    .build();
        } else {
            if (!StringUtils.equals(name, template.getName())) {
                template.setName(name);
                save(template);
            }
        }
    }

    /**
     * Verifies that claims can/can't be submitted if they refer to invoices in another claim.
     *
     * @param editor    the editor
     * @param duplicate the claim the references the same invoice
     * @param allowed   if {@code true} duplicates are allowed (because the duplicate claim is CANCELLED, or DECLINED)
     */
    private void checkDuplicate(ClaimEditor editor, Act duplicate, boolean allowed) {
        errors.clear();
        ClaimSubmitter submitter = createSubmitter();
        submitter.submit(editor, Assert::assertNull);

        checkDuplicate(duplicate, allowed);
    }

    /**
     * Verifies that claims can/can't be submitted if they refer to invoices in another claim.
     *
     * @param claim     the claim
     * @param duplicate the claim the references the same invoice
     * @param allowed   if {@code true} duplicates are allowed (because the duplicate claim is CANCELLED, or DECLINED)
     */
    private void checkDuplicate(Act claim, Act duplicate, boolean allowed) {
        errors.clear();
        ClaimSubmitter submitter = createSubmitter();
        submitter.submit(claim, Assert::assertNull);

        checkDuplicate(duplicate, allowed);
    }

    /**
     * Verifies a duplicate error is/isn't present.
     *
     * @param duplicate the duplicate claim
     * @param allowed   if {@code true} if duplicates are allowed. If so, no error should be present
     */
    private void checkDuplicate(Act duplicate, boolean allowed) {
        if (!allowed) {
            assertEquals(1, errors.size());
            String error = "Cannot submit this claim. It contains charges already claimed by claim "
                           + NumberFormatter.format(duplicate.getId())
                           + ", dated " + DateFormatter.formatDate(duplicate.getActivityStartTime(), false) + ".";
            assertEquals(error, errors.get(0));
        } else {
            assertEquals(0, errors.size());
        }
    }

    /**
     * Creates a claim submitter.
     *
     * @return a new submitter
     */
    private ClaimSubmitter createSubmitter() {
        LocalContext context = new LocalContext();
        context.setPractice(practice);
        context.setLocation(location);
        context.setUser(clinician);
        return createSubmitter(context);
    }

    /**
     * Creates a claim submitter.
     *
     * @param context the context
     * @return a new submitter
     */
    private ClaimSubmitter createSubmitter(Context context) {
        InsuranceServices insuranceServices = mock(InsuranceServices.class);
        when(insuranceServices.canSubmit(Mockito.any())).thenReturn(true);
        when(insuranceServices.getService(Mockito.any())).thenReturn(insuranceService);
        return new ClaimSubmitter(getArchetypeService(), insuranceFactory, insuranceServices,
                                  context, new HelpContext("foo", null)) {
            @Override
            protected BenefitDialog createBenefitDialog(GapClaimImpl claim) {
                return new BenefitDialog(claim, new HelpContext("foo", null)) {
                    @Override
                    protected boolean reload(long now) {
                        return true;
                    }
                };
            }
        };

    }

    /**
     * Creates and saves an invoice.
     *
     * @param status the invoice status
     * @param items  the invoice items
     * @return the invoice
     */
    private FinancialAct createInvoice(String status, FinancialAct... items) {
        List<FinancialAct> invoice = createChargesInvoice(customer, clinician, status, items);
        save(invoice);
        return invoice.get(0);
    }

    /**
     * Creates an invoice item, with quantity=1, price=10, discount=1, tax=0.82, total=9
     *
     * @return the new invoice item
     */
    private FinancialAct createInvoiceItem() {
        return createInvoiceItem(patient);
    }

    /**
     * Creates an invoice item, with quantity=1, price=10, discount=1, tax=0.82, total=9
     *
     * @param patient the patient
     * @return the new invoice item
     */
    private FinancialAct createInvoiceItem(Party patient) {
        BigDecimal discount = ONE;
        BigDecimal tax = new BigDecimal("0.82");
        return createInvoiceItem(new Date(), patient, TestHelper.createProduct(), ONE, BigDecimal.TEN, discount, tax);
    }

    /**
     * Creates an invoice item.
     *
     * @param date     the date
     * @param patient  the patient
     * @param product  the product
     * @param quantity the quantity
     * @param price    the unit price
     * @param discount the discount
     * @param tax      the tax
     * @return the new invoice item
     */
    private FinancialAct createInvoiceItem(Date date, Party patient, Product product, BigDecimal quantity,
                                           BigDecimal price, BigDecimal discount, BigDecimal tax) {
        return FinancialTestHelper.createInvoiceItem(date, patient, clinician, product, quantity, ZERO, price,
                                                     discount, tax);
    }

}
