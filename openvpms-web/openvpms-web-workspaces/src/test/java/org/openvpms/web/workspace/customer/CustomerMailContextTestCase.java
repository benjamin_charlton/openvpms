/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer;

import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.party.Contact;
import org.openvpms.web.component.app.AbstractMailContextTest;
import org.openvpms.web.component.app.LocalContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link CustomerMailContext} class.
 *
 * @author Tim Anderson
 */
public class CustomerMailContextTestCase extends AbstractMailContextTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Verifies that the correct email to-addresses are returned for customers.
     */
    @Test
    public void testCustomerToAddresses() {
        LocalContext context = new LocalContext();
        CustomerMailContext mailContext = new CustomerMailContext(context, true);

        assertNull(mailContext.getCustomer());
        checkAddresses(mailContext.getToAddresses(), null);

        Contact contact1A = customerFactory.createEmail("customer1@gmail.com", true);
        Contact contact1B = customerFactory.createEmail("customer1@outlook.com");
        Party customer1 = (Party) customerFactory.newCustomer()
                .addContacts(contact1A, contact1B)
                .build();

        Party customer2 = (Party) customerFactory.createCustomer();

        Contact contact3 = customerFactory.createEmail("customer3@gmail.com");
        Party customer3 = (Party) customerFactory.newCustomer()
                .addContacts(contact3)
                .build();

        // verify the correct to-addresses are returned for customer1
        context.setCustomer(customer1);
        assertEquals(customer1, mailContext.getCustomer());
        checkAddresses(mailContext.getToAddresses(), contact1A, contact1A, contact1B);

        // verify the correct to-addresses are returned for customer2
        context.setCustomer(customer2);
        checkAddresses(mailContext.getToAddresses(), null);

        // verify the correct to-addresses are returned for customer3
        context.setCustomer(customer3);
        checkAddresses(mailContext.getToAddresses(), contact3, contact3);
    }

    /**
     * Verifies that the expected email addresses are returned when a customer is updated.
     */
    @Test
    public void testUpdateCustomer() {
        LocalContext context = new LocalContext();
        CustomerMailContext mailContext = new CustomerMailContext(context, true);

        assertNull(mailContext.getCustomer());
        checkAddresses(mailContext.getToAddresses(), null);

        Contact contactA = customerFactory.createEmail("customer1@gmail.com");
        Contact contactB = customerFactory.createEmail("customer1@outlook.com", true);
        Party customer1 = (Party) customerFactory.newCustomer()
                .addContacts(contactA)
                .build();

        context.setCustomer(customer1);
        checkAddresses(mailContext.getToAddresses(), contactA, contactA);

        // now update the customer and verify the correct email addresses are returned
        customerFactory.updateCustomer(customer1)
                .addContacts(contactB)
                .build();
        checkAddresses(mailContext.getToAddresses(), contactB, contactA, contactB);
    }

    /**
     * Verifies that the correct email to-addresses are returned for patients with referrals.
     */
    @Test
    public void testPatientReferralAddresses() {
        LocalContext context = new LocalContext();
        CustomerMailContext mailContext = new CustomerMailContext(context, true);

        assertNull(mailContext.getCustomer());
        assertNull(mailContext.getPatient());
        checkAddresses(mailContext.getToAddresses(), null);

        Contact vet1Contact = customerFactory.createEmail("vet1@vetsrus.com");
        Party vet1 = (Party) supplierFactory.newVet()
                .title("DR")
                .addContacts(vet1Contact)
                .build();
        Contact vetPracticeContact = customerFactory.createEmail("pactice@vetsrus.com");
        Party vet2 = (Party) supplierFactory.createVet();
        supplierFactory.newVetPractice()
                .vets(vet1, vet2)
                .addContact(vetPracticeContact)
                .build();

        Contact contact1A = customerFactory.createEmail("customer1@gmail.com", true);
        Contact contact1B = customerFactory.createEmail("customer1@outlook.com");

        Party customer1 = (Party) customerFactory.newCustomer()
                .addContacts(contact1A, contact1B)
                .build();
        Party patient1 = (Party) patientFactory.newPatient()
                .owner(customer1)
                .addReferredFrom(vet1)
                .build();
        Party patient2 = (Party) patientFactory.newPatient()
                .owner(customer1)
                .addReferredFrom(vet2)
                .build();

        // verify the correct to-addresses are returned for customer1 and patient1
        context.setCustomer(customer1);
        context.setPatient(patient1);
        checkAddresses(mailContext.getToAddresses(), contact1A, contact1A, contact1B, vet1Contact, vetPracticeContact);

        // verify the correct to-addresses are returned for customer1 and patient2
        context.setPatient(patient2);
        checkAddresses(mailContext.getToAddresses(), contact1A, contact1A, contact1B, vetPracticeContact);
    }

    /**
     * Verifies that the expected email addresses are returned when a patient is updated.
     */
    @Test
    public void testUpdatePatient() {
        LocalContext context = new LocalContext();
        CustomerMailContext mailContext = new CustomerMailContext(context, true);

        assertNull(mailContext.getCustomer());
        assertNull(mailContext.getPatient());
        checkAddresses(mailContext.getToAddresses(), null);

        Contact vetContact = customerFactory.createEmail("vet1@vetsrus.com");
        Party vet = (Party) supplierFactory.newVet()
                .title("DR")
                .addContacts(vetContact)
                .build();
        Contact vetPracticeContact = customerFactory.createEmail("pactice@vetsrus.com");
        supplierFactory.newVetPractice()
                .vets(vet)
                .addContact(vetPracticeContact)
                .build();

        Contact customerContact = customerFactory.createEmail("customer@gmail.com");

        Party customer = (Party) customerFactory.newCustomer()
                .addContacts(customerContact)
                .build();
        Party patient = (Party) patientFactory.newPatient()
                .owner(customer)
                .build();

        // verify the correct to-addresses are returned for the customer and patient
        context.setCustomer(customer);
        context.setPatient(patient);
        checkAddresses(mailContext.getToAddresses(), customerContact, customerContact);

        // now update the patient and verify the correct email addresses are returned
        patientFactory.updatePatient(patient)
                .addReferredTo(vet)
                .build();

        checkAddresses(mailContext.getToAddresses(), customerContact, customerContact, vetContact, vetPracticeContact);
    }
}