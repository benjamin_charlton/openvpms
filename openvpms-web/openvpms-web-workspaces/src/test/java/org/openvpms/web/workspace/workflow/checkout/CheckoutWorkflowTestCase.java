/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.insurance.InsuranceTestHelper;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.helper.ActBean;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.insurance.service.InsuranceService;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActEditDialog;
import org.openvpms.web.workspace.patient.insurance.claim.TestGapInsuranceService;
import org.openvpms.web.workspace.workflow.WorkflowTestHelper;
import org.openvpms.web.workspace.workflow.checkout.CheckOutWorkflow.ClaimAtCheckout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.SingletonBeanRegistry;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;
import static org.openvpms.web.workspace.workflow.WorkflowTestHelper.cancelDialog;
import static org.openvpms.web.workspace.workflow.WorkflowTestHelper.createTask;


/**
 * Tests the {@link CheckOutWorkflow}.
 * <p/>
 * Note that this injects a mock {@link InsuranceServices} into the Spring application context, hence the
 * {@code DirtiesContext} below.
 *
 * @author Tim Anderson
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class CheckoutWorkflowTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * The context.
     */
    private Context context;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The till.
     */
    private Entity till;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        till = practiceFactory.createTill();
        Party location = (Party) practiceFactory.newLocation()
                .tills(till)
                .build();
        User user = TestHelper.createUser();
        context = new LocalContext();
        context.setLocation(location);
        context.setUser(user);

        customer = TestHelper.createCustomer();
        clinician = TestHelper.createClinician();
        patient = TestHelper.createPatient(customer);

        // Register a mock InsuranceServices that returns a TestGapInsuranceService. TODO - should be a cleaner way
        // of doing this
        SingletonBeanRegistry factory = (SingletonBeanRegistry) applicationContext.getAutowireCapableBeanFactory();
        if (!factory.containsSingleton("insuranceServices")) {
            InsuranceService service = new TestGapInsuranceService();
            InsuranceServices insuranceServices = Mockito.mock(InsuranceServices.class);
            Mockito.when(insuranceServices.getService(Mockito.any())).thenReturn(service);
            factory.registerSingleton("insuranceServices", insuranceServices);
        }
    }

    /**
     * Tests the check-out workflow when started with an appointment.
     */
    @Test
    public void testCheckOutWorkflowForAppointment() {
        Act appointment = createAppointment(clinician);
        checkWorkflow(appointment, clinician);
    }

    /**
     * Tests the check-out workflow when started with a task.
     */
    @Test
    public void testCheckoutWorkflowForTask() {
        Act appointment = createAppointment(clinician);
        appointment.setStatus(AppointmentStatus.CHECKED_IN);
        Act task = createTask(customer, patient, clinician);
        ActBean bean = new ActBean(appointment);
        bean.addNodeRelationship("tasks", task);
        save(appointment, task);
        checkWorkflow(task, clinician);
        appointment = get(appointment);
        assertEquals(AppointmentStatus.COMPLETED, appointment.getStatus());
    }

    /**
     * Verifies that closing the invoice edit dialog by the 'x' button cancels the workflow.
     */
    @Test
    public void testCancelInvoiceByUserCloseNoSave() {
        checkCancelInvoice(false, true);
    }

    /**
     * Verifies that closing the invoice edit dialog by the 'x' button cancels the workflow.
     */
    @Test
    public void testCancelInvoiceByUserCloseAfterSave() {
        checkCancelInvoice(true, true);
    }

    /**
     * Verifies that cancelling the invoice edit dialog by the 'Cancel' button cancels the workflow.
     */
    @Test
    public void testCancelInvoiceByCancelButtonNoSave() {
        checkCancelInvoice(false, false);
    }

    /**
     * Verifies that cancelling the invoice edit dialog by the 'Cancel' button cancels the workflow.
     */
    @Test
    public void testCancelInvoiceByCancelButtonAfterSave() {
        checkCancelInvoice(true, false);
    }

    /**
     * Tests the behaviour of clicking the 'no' button on the finalise invoice confirmation dialog.
     */
    @Test
    public void testNoFinaliseInvoice() {
        Act appointment = createAppointment(clinician);
        createEvent(appointment);
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(appointment, getPractice(), context);
        workflow.start();
        BigDecimal amount = workflow.addInvoice(patient, false);
        workflow.confirm(PopupDialog.NO_ID);        // skip posting the invoice. Payment is skipped
        workflow.print();
        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, null, clinician);
        workflow.checkInvoice(ActStatus.IN_PROGRESS, amount, clinician);
        assertNull(workflow.getPayment());
    }

    /**
     * Tests the behaviour of clicking the 'cancel' button on the finalise invoice confirmation dialog.
     */
    @Test
    public void testCancelFinaliseInvoice() {
        checkCancelFinaliseInvoice(false);
    }

    /**
     * Tests the behaviour of clicking the 'user close' button on the finalise invoice confirmation dialog.
     */
    @Test
    public void testUserCloseFinaliseInvoice() {
        checkCancelFinaliseInvoice(true);
    }

    /**
     * Verifies that the payment can be skipped.
     */
    @Test
    public void testSkipPayment() {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(),
                                                                     context);
        workflow.start();
        BigDecimal amount = workflow.addInvoice(patient, true);

        workflow.skipPayment();

        workflow.print();
        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, null, clinician);
        workflow.checkInvoice(ActStatus.POSTED, amount, clinician);
        assertNull(workflow.getPayment());
    }

    /**
     * Verifies that the workflow cancels if the 'Cancel' button is pressed at the payment confirmation.
     */
    @Test
    public void testCancelPaymentConfirmation() {
        checkCancelPaymentConfirmation(false);
    }

    /**
     * Verifies that the workflow cancels if the 'user close' button is pressed at the payment confirmation.
     */
    @Test
    public void testUserClosePaymentConfirmation() {
        checkCancelPaymentConfirmation(true);
    }

    /**
     * Verifies that the workflow cancels after payment is cancelled.
     */
    @Test
    public void testCancelPayment() {
        checkCancelPayment(false);
    }

    /**
     * Verifies that the workflow cancels after payment is cancelled by pressing the 'user close' button.
     */
    @Test
    public void testCancelPaymentByUserClose() {
        checkCancelPayment(true);
    }

    /**
     * Verifies that a read-only invoice dialog is displayed if the invoice is finalised before check-out.
     */
    @Test
    public void testFinalisedInvoice() {
        Act appointment = createAppointment(clinician);
        Act event = createEvent(appointment);
        BigDecimal amount = BigDecimal.TEN;
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(
                amount, customer, patient, TestHelper.createProduct(), ActStatus.POSTED);
        save(acts);
        IMObjectBean bean = getBean(event);
        bean.addTarget("chargeItems", acts.get(1)); // link to the invoice item
        bean.save();

        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(appointment, getPractice(), context);
        workflow.start();

        // 1st task to pause should be the invoice viewer dialog
        InvoiceViewerDialog dialog = workflow.getInvoiceViewerDialog();
        fireDialogButton(dialog, PopupDialog.OK_ID);

        // 2nd to pause should be a confirmation prompting to pay the invoice
        workflow.confirm(PopupDialog.YES_ID);

        // 3rd task to pause should be payment editor
        workflow.addPayment(till);
        workflow.checkPayment(ActStatus.POSTED, amount);

        // 4th task to pause should be print dialog
        workflow.print();

        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, till, clinician);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set true, the clinician comes from the context
     * user rather than the appointment.
     */
    @Test
    public void testUseLoggedInClinicianForClinician() {
        User clinician2 = TestHelper.createClinician();
        checkUseLoggedInClinician(true, clinician2, clinician, clinician2);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set true, and the logged in user is not a
     * clinician, the clinician comes from the appointment.
     */
    @Test
    public void testUseLoggedInClinicianForNonClinician() {
        User user = TestHelper.createUser();
        checkUseLoggedInClinician(true, user, clinician, clinician);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set false, the clinician comes from the context
     * user rather than the appointment.
     */
    @Test
    public void testUseLoggedInClinicianDisabledForClinician() {
        User clinician2 = TestHelper.createClinician();
        checkUseLoggedInClinician(false, clinician, clinician2, clinician2);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set false, and the logged in user is not a
     * clinician, the clinician comes from the appointment.
     */
    @Test
    public void testUseLoggedInClinicianDisabledForNonClinician() {
        User user = TestHelper.createUser();
        checkUseLoggedInClinician(false, user, clinician, clinician);
    }

    /**
     * Verifies that if there is an existing invoice done at a different practice location, the payment
     * inherits the current practice location, not that of the invoice.
     */
    @Test
    public void testCheckoutAtLocationDifferentToInvoice() {
        Party location1 = context.getLocation();
        Party location2 = TestHelper.createLocation();

        // create an invoice, done at location2
        BigDecimal amount = BigDecimal.TEN;
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(
                amount, customer, patient, TestHelper.createProduct(), ActStatus.IN_PROGRESS);
        IMObjectBean invoiceBean = getBean(acts.get(0));
        invoiceBean.setTarget("location", location2);
        save(acts);

        // create an appointment at the location1
        Act appointment = createAppointment(clinician);
        Act event = createEvent(appointment);
        IMObjectBean bean = getBean(event);
        bean.addTarget("chargeItems", acts.get(1)); // link to the invoice item
        bean.save();

        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(appointment, getPractice(), context);
        workflow.start();

        // first task to pause should by the invoice edit task.
        EditDialog dialog = workflow.getEditDialog();
        assertTrue(dialog instanceof CustomerChargeActEditDialog);
        fireDialogButton(dialog, PopupDialog.OK_ID);

        // 2nd to pause should be a confirmation prompting to post the invoice
        workflow.confirmPostInvoice();

        // verify the invoice has been posted
        workflow.checkInvoice(ActStatus.POSTED, amount, null);

        // 3rd to pause should be a confirmation prompting to pay the invoice
        workflow.confirmPayment();

        // 4th task to pause should be payment editor
        workflow.addPayment(till);
        workflow.checkPayment(ActStatus.POSTED, amount, location1); // payment should be at location1

        // 5th task to pause should be print dialog
        workflow.print();

        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, till, clinician);
        assertEquals(location1, context.getLocation()); // location should be the same
    }

    /**
     * Verifies that if the practice is configured to prompt for standard and gap-claims at check-out,
     * a prompt will be displayed for both, when a patient has an insurance policy.
     */
    @Test
    public void testPromptForAllClaims() {
        checkPromptForClaim(ClaimAtCheckout.ALL);
    }

    /**
     * Verifies that if the practice is configured to not prompt for claims, no prompts are displayed
     * when a patient has an insurance policy.
     */
    @Test
    public void testDisablePromptForClaim() {
        checkPromptForClaim(ClaimAtCheckout.NONE);
    }

    /**
     * Verifies that if the practice is configured to prompt for standard claims, a prompt will be displayed
     * after collecting payment, when a patient has an insurance policy.
     */
    @Test
    public void testPromptForStandardClaim() {
        checkPromptForClaim(ClaimAtCheckout.STANDARD_CLAIM);
    }

    /**
     * Verifies that if the practice is configured to prompt for gap claims, a prompt will be displayed
     * before collecting payment, when a patient has an insurance policy.
     */
    @Test
    public void testPromptForGapClaim() {
        checkPromptForClaim(ClaimAtCheckout.GAP_CLAIM);
    }

    /**
     * Verifies that if the invoice is POSTED by another user, the workflow completes.
     */
    @Test
    public void testInvoicePostedByAnotherUser() {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(),
                                                                     context);
        workflow.start();
        BigDecimal amount = workflow.addInvoice(patient, false);

        // now at the confirm POST invoice step. Simulate posting the invoice by a different user
        FinancialAct invoice = get(workflow.getInvoice());
        assertNotNull(invoice);
        invoice.setStatus(ActStatus.POSTED);
        save(invoice);

        workflow.confirmPostInvoice();
        workflow.skipPayment();
        workflow.print();

        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, null, clinician);
        workflow.checkInvoice(ActStatus.POSTED, amount, clinician);
        assertNull(workflow.getPayment());
    }

    /**
     * Verifies that if the invoice is deleted by another user at the post-invoice confirmation, the workflow cancels.
     */
    @Test
    public void testInvoiceDeletedByAnotherUserPrePost() {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(),
                                                                     context);
        workflow.start();
        workflow.addInvoice(patient, false);

        // now at the confirm POST invoice step. Simulate deleting the invoice by a different user
        FinancialAct invoice = get(workflow.getInvoice());
        assertNotNull(invoice);
        remove(invoice);

        workflow.confirmPostInvoice();

        // verify an error dialog is displayed
        ErrorDialog error = EchoTestHelper.findWindowPane(ErrorDialog.class);
        assertNotNull(error);
        assertEquals(Messages.get("workflow.checkout.postinvoice.title"), error.getTitle());
        fireDialogButton(error, PopupDialog.OK_ID);

        // verify the workflow has completed
        workflow.checkComplete(false);
        workflow.checkContext(context, null, null, null, null);
        assertNull(workflow.getPayment());
    }

    /**
     * Verifies that if the invoice is invalid when POSTING, an edit dialog is displayed.
     */
    @Test
    public void testInvoiceCannotBePosted() {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(),
                                                                     context);
        workflow.start();
        Entity investigationType = laboratoryFactory.createInvestigationType(laboratoryFactory.createLaboratory());
        Product product = productFactory.newService()
                .tests(laboratoryFactory.createTest(investigationType))
                .build();
        EditDialog dialog = workflow.addInvoiceItem(patient, product);
        fireDialogButton(dialog, PopupDialog.OK_ID);  // save the invoice

        // skip investigation submission
        workflow.fireDialogButton(PopupDialog.class, Messages.get("customer.charge.investigation.submit.title"),
                                  PopupDialog.SKIP_ID);

        workflow.confirmPostInvoice();

        // verify an error dialog is displayed
        workflow.fireDialogButton(ErrorDialog.class, PopupDialog.OK_ID);

        // verify the charge dialog is display again, and cancel it
        workflow.fireDialogButton(CustomerChargeActEditDialog.class, PopupDialog.CANCEL_ID);

        // verify the workflow has completed
        workflow.checkComplete(false);
        workflow.checkContext(context, null, null, null, null);
        assertNull(workflow.getPayment());
    }

    /**
     * Verifies the behaviour of prompting for insurance claims, when a patient has an insurance policy.
     * <p/>
     * If ClaimAtCheckout is:
     * <ul>
     *     <li>ALL - should prompt for both gap and standard claims</li>
     *     <li>NONE - shouldn't prompt for claims</li>
     *     <li>GAP_CLAIM - should only prompt before payment has been accepted</li>
     *     <li>E_CLAIM - should only prompt after payment has been accepted</li>
     * </ul>
     * Note - this only checks for claim prompting; it doesn't test claim submission.
     *
     * @param claim the insurance claim configuration
     */
    private void checkPromptForClaim(ClaimAtCheckout claim) {
        Party insurer = (Party) InsuranceTestHelper.createInsurer();
        Act policy = (Act) InsuranceTestHelper.createPolicy(customer, patient, insurer, "APOLICY");
        save(policy);
        IMObjectBean bean = getBean(getPractice());
        bean.setValue("claimAtCheckout", claim.toString());
        bean.save();

        Act appointment = createAppointment(clinician);
        InsuranceService insuranceService = new TestGapInsuranceService();
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(appointment, getPractice(),
                                                                     context);
        workflow.start();

        // first task to pause should by the invoice edit task.
        BigDecimal amount = workflow.addInvoice(patient, false);

        // second task to pause should be a confirmation, prompting to post the invoice
        workflow.confirmPostInvoice();

        // verify the invoice has been posted
        workflow.checkInvoice(ActStatus.POSTED, amount, clinician);

        if (claim == ClaimAtCheckout.GAP_CLAIM || claim == ClaimAtCheckout.ALL) {
            // next task to pause should be gap claim prompt
            workflow.confirm("Gap Claim", PopupDialog.NO_ID);
        }

        // next task to pause should be a confirmation prompting to pay the invoice
        workflow.confirmPayment();

        workflow.addPayment(till);
        workflow.checkPayment(ActStatus.POSTED, amount);

        if (claim == ClaimAtCheckout.STANDARD_CLAIM || claim == ClaimAtCheckout.ALL) {
            // next task to pause should be standard claim confirmation
            workflow.confirm("Claim", PopupDialog.NO_ID);
        }

        // next task to pause should be print dialog
        workflow.print();

        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, till, clinician);
    }

    /**
     * Tests the effects of the practice useLoggedInClinician option during the Check-Out workflow.
     *
     * @param enabled              if {@code true}, enable the option, otherwise disable it
     * @param user                 the current user
     * @param appointmentClinician the appointment clinician
     * @param expectedClinician    the expected clinician on new acts
     */
    private void checkUseLoggedInClinician(boolean enabled, User user, User appointmentClinician,
                                           User expectedClinician) {
        context.setUser(user);
        context.setClinician(clinician);
        IMObjectBean bean = getBean(getPractice());
        bean.setValue("useLoggedInClinician", enabled);
        Act appointment = createAppointment(appointmentClinician);
        checkWorkflow(appointment, expectedClinician);
    }

    /**
     * Runs the workflow for the specified act.
     *
     * @param act       the act
     * @param clinician the expected clinician
     */
    private void checkWorkflow(Act act, User clinician) {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(act, getPractice(), context);
        workflow.start();

        // first task to pause should by the invoice edit task.
        BigDecimal amount = workflow.addInvoice(patient, false);

        // second task to pause should be a confirmation, prompting to post the invoice
        workflow.confirmPostInvoice();

        // verify the invoice has been posted
        workflow.checkInvoice(ActStatus.POSTED, amount, clinician);

        // third task to pause should be a confirmation prompting to pay the invoice
        workflow.confirmPayment();

        // 4th task to pause should be payment editor
        workflow.addPayment(till);
        workflow.checkPayment(ActStatus.POSTED, amount);

        // 5th task to pause should be print dialog
        workflow.print();

        workflow.checkComplete(true);
        workflow.checkContext(context, customer, patient, till, clinician);
    }

    /**
     * Verifies that cancelling the invoice cancels the workflow.
     *
     * @param save      if {@code true} save the invoice before cancelling
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    private void checkCancelInvoice(boolean save, boolean userClose) {
        context.setClinician(clinician);
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(),
                                                                     context);
        workflow.start();

        // first task to pause should by the invoice edit task.
        BigDecimal fixedPrice = new BigDecimal("18.18");
        EditDialog dialog = workflow.addInvoiceItem(patient, fixedPrice);
        if (save) {
            fireDialogButton(dialog, PopupDialog.APPLY_ID);          // save the invoice
        }
        workflow.addInvoiceItem(patient, fixedPrice);     // add another item. Won't be saved

        // close the dialog
        cancelDialog(dialog, userClose);

        if (save) {
            BigDecimal fixedPriceIncTax = BigDecimal.valueOf(20);
            workflow.checkInvoice(ActStatus.IN_PROGRESS, fixedPriceIncTax, clinician);
        } else {
            FinancialAct invoice = workflow.getInvoice();
            assertNotNull(invoice);
            assertTrue(invoice.isNew()); // unsaved
        }

        workflow.checkComplete(false);
        workflow.checkContext(context, null, null, null, clinician);
        assertNull(workflow.getPayment());
    }

    /**
     * Verifies that the workflow cancels if the invoice confirmation dialog is cancelled.
     *
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    private void checkCancelFinaliseInvoice(boolean userClose) {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(), context);
        workflow.start();
        BigDecimal amount = workflow.addInvoice(patient, false);
        String id = (userClose) ? null : PopupDialog.CANCEL_ID;
        workflow.confirm(id);
        workflow.checkComplete(false);
        workflow.checkContext(context, null, null, null, null);
        workflow.checkInvoice(ActStatus.IN_PROGRESS, amount, clinician);
        assertNull(workflow.getPayment());
    }

    /**
     * Verifies that the workflow cancels if the payment confirmation is cancelled.
     *
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    private void checkCancelPaymentConfirmation(boolean userClose) {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(), context);
        workflow.start();
        BigDecimal amount = workflow.addInvoice(patient, true);

        String id = (userClose) ? null : PopupDialog.CANCEL_ID;
        workflow.confirm(id); // cancel payment

        workflow.checkComplete(false);
        workflow.checkContext(context, null, null, null, null);
        workflow.checkInvoice(ActStatus.POSTED, amount, clinician);
        assertNull(workflow.getPayment());
    }


    /**
     * Verifies that the workflow completes after payment is cancelled.
     *
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    private void checkCancelPayment(boolean userClose) {
        CheckoutWorkflowRunner workflow = new CheckoutWorkflowRunner(createAppointment(clinician), getPractice(),
                                                                     context);
        workflow.start();
        BigDecimal amount = workflow.addInvoice(patient, true);

        workflow.confirm(PopupDialog.YES_ID);
        EditDialog dialog = workflow.addPaymentItem(till);
        cancelDialog(dialog, userClose);

        workflow.checkComplete(false);
        workflow.checkContext(context, null, null, null, null);
        workflow.checkInvoice(ActStatus.POSTED, amount, clinician);
        FinancialAct payment = workflow.getPayment();
        assertNotNull(payment);
        assertTrue(payment.isNew()); // unsaved
    }

    /**
     * Helper to create an appointment.
     *
     * @param clinician the clinician
     * @return a new appointment
     */
    private Act createAppointment(User clinician) {
        return WorkflowTestHelper.createAppointment(customer, patient, clinician, context.getLocation());
    }

    /**
     * Creates an event linked to an appointment.
     *
     * @param appointment the appointment
     * @return a new event
     */
    private Act createEvent(Act appointment) {
        Act event = PatientTestHelper.createEvent(patient);
        IMObjectBean bean = getBean(appointment);
        bean.addTarget("event", event, "appointment");
        bean.save(event);
        return event;
    }

}
