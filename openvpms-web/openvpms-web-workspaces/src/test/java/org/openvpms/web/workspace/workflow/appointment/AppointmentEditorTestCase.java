/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.archetype.rules.workflow.Times;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.workflow.appointment.repeat.AppointmentSeries;
import org.openvpms.web.workspace.workflow.appointment.repeat.Repeats;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link AppointmentEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AppointmentEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Tests the {@link AppointmentEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Date start = DateRules.getToday();
        Date end = DateRules.getTomorrow();
        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());
        Act appointment = ScheduleTestHelper.createAppointment(start, end, schedule);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        AppointmentEditor editor = new AppointmentEditor(appointment, null, context);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof AppointmentEditor);
    }

    /**
     * Tests the {@link AppointmentEditor#getEventTimes()} method.
     */
    @Test
    public void testGetEventTimes() {
        Date start = DateRules.getToday();
        Date end = DateRules.getTomorrow();
        Party schedule = ScheduleTestHelper.createSchedule(practiceFactory.createLocation());
        Act appointment = ScheduleTestHelper.createAppointment(start, end, schedule);

        // create a series of 3 appointments
        AppointmentSeries series = new AppointmentSeries(appointment, getArchetypeService());
        series.setExpression(Repeats.daily());
        series.setCondition(Repeats.twice());
        series.save();

        // create an editor to edit the series
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        AppointmentEditor editor1 = new AppointmentEditor(appointment, null, true, context);

        // verify that there are 3 appointments in the series
        List<Times> times1 = editor1.getEventTimes();
        assertNotNull(times1);
        assertEquals(3, times1.size());

        // now create an editor for the first appointment in the series.
        AppointmentEditor editor2 = new AppointmentEditor(appointment, null, false, context);

        // verify that getEventTimes() returns the one appointment, corresponding to that of the first appointment
        List<Times> times2 = editor2.getEventTimes();
        assertNotNull(times2);
        assertEquals(1, times2.size());
        assertEquals(appointment.getId(), times2.get(0).getId());
        assertEquals(appointment.getActivityStartTime(), times2.get(0).getStartTime());
        assertEquals(appointment.getActivityEndTime(), times2.get(0).getEndTime());
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link AppointmentEditor} for
     * <em>act.customerAppointment</em> instances.
     */
    @Test
    public void testFactory() {
        Act appointment = create(ScheduleArchetypes.APPOINTMENT, Act.class);
        IMObjectEditor editor = factory.create(appointment, new DefaultLayoutContext(new LocalContext(),
                                                                                     new HelpContext("foo", null)));
        assertTrue(editor instanceof AppointmentEditor);
    }

    /**
     * Verifies that changing the customer clears the patient, and changing the patient assigns that patient's
     * owner.
     */
    @Test
    public void testChangeCustomerAndPatient() {
        Act appointment = create(ScheduleArchetypes.APPOINTMENT, Act.class);
        Entity type = schedulingFactory.createAppointmentType();
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addAppointmentType(type, 1, true)
                .slotSize(15, DateUnits.MINUTES)
                .build();
        Party customer1 = TestHelper.createCustomer();
        Party patient1 = TestHelper.createPatient(customer1);
        Party customer2 = TestHelper.createCustomer();

        AppointmentEditor editor = createEditor(appointment);
        editor.setSchedule(schedule);
        editor.setAppointmentType(type);
        editor.setStartTime(DateRules.getToday());

        editor.setCustomer(customer1);
        assertTrue(editor.isValid());
        editor.setPatient(patient1);
        assertTrue(editor.isValid());

        // verify setting a different customer clears the patient
        editor.setCustomer(customer2);
        assertNull(editor.getPatient());
        assertTrue(editor.isValid());

        // verify setting a different patient assigns that patient's owner
        editor.setPatient(patient1);
        assertEquals(customer1, editor.getCustomer());
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that the default reason associated with an appointment type is populated on the appointment.
     */
    @Test
    public void testDefaultAppointmentReason() {
        // clear any default lookup.
        for (Lookup lookup : getLookupService().getLookups(ScheduleArchetypes.VISIT_REASONS)) {
            if (lookup.isDefaultLookup()) {
                lookup = get(lookup);
                lookup.setDefaultLookup(false);
                save(lookup);
            }
        }

        // create a default reason
        Lookup checkup = lookupFactory.createLookup(ScheduleArchetypes.VISIT_REASON, "CHECKUP", true);

        Act appointment = create(ScheduleArchetypes.APPOINTMENT, Act.class);
        Entity appointmentType1 = schedulingFactory.newAppointmentType().reason("FOO").build();
        Entity appointmentType2 = schedulingFactory.createAppointmentType();
        Entity appointmentType3 = schedulingFactory.createAppointmentType();
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addAppointmentType(appointmentType1, 1, true)
                .addAppointmentType(appointmentType2, 1, false)
                .addAppointmentType(appointmentType3, 1, false)
                .slotSize(15, DateUnits.MINUTES)
                .build();

        AppointmentEditor editor = createEditor(appointment);

        assertNull(editor.getAppointmentType());
        assertEquals("CHECKUP", editor.getReason());
        editor.setSchedule(schedule);
        editor.setAppointmentType(appointmentType1);
        assertEquals("FOO", editor.getReason());

        // now verify the reason is retained if an appointment type is set that has none
        editor.setAppointmentType(appointmentType2);
        assertEquals("FOO", editor.getReason());

        // clear the reason and verify the default is selected when an appointment type is used that has no reason
        editor.setReason(null);
        editor.setAppointmentType(appointmentType3);
        assertEquals("CHECKUP", editor.getReason());

        // set back to an appointment type with a reason
        editor.setAppointmentType(appointmentType1);
        assertEquals("FOO", editor.getReason());

        // clear the reason and verify the default is selected when an appointment type is used that has no reason
        editor.setReason(null);
        editor.setAppointmentType(appointmentType2);
        assertEquals("CHECKUP", editor.getReason());

        // make checkup non-default and change the appointment type.
        checkup.setDefaultLookup(false);
        save(checkup);
        editor.setAppointmentType(appointmentType1);
        assertEquals("FOO", editor.getReason());

        // verify that the reason is retained when there is no default
        editor.setAppointmentType(appointmentType2);
        assertEquals("FOO", editor.getReason());
    }

    /**
     * Verify that the customer and patient aren't editable if the appointment was saved with an associated visit.
     * <p/>
     * This is to prevent data inconsistency - e.g. to prevent an appointment being linked to a visit or tasks
     * with a different patient.
     */
    @Test
    public void testCustomerPatientReadOnly() {
        Entity appointmentType = schedulingFactory.createAppointmentType();
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addAppointmentType(appointmentType, 1, true)
                .slotSize(15, DateUnits.MINUTES)
                .build();
        Party customer = (Party) customerFactory.createCustomer();
        Party patient = (Party) patientFactory.createPatient(customer);
        Act appointment1 = (Act) schedulingFactory.newAppointment()
                .startTime(DateRules.getToday())
                .customer(customer)
                .patient(patient)
                .schedule(schedule)
                .appointmentType(appointmentType)
                .status(AppointmentStatus.PENDING)
                .build(false);

        AppointmentEditor editor1 = createEditor(appointment1);
        assertFalse(editor1.makeCustomerPatientReadOnly());

        schedulingFactory.updateAppointment(appointment1)
                .status(AppointmentStatus.CONFIRMED)
                .build();

        AppointmentEditor editor2 = createEditor(appointment1);
        assertFalse(editor2.makeCustomerPatientReadOnly());

        Act visit = (Act) patientFactory.newVisit()
                .patient(patient)
                .build();
        schedulingFactory.updateAppointment(appointment1)
                .event(visit)
                .build();

        AppointmentEditor editor3 = createEditor(appointment1);
        assertTrue(editor3.makeCustomerPatientReadOnly());

        // verify the customer and patient are read-only when the appointment is linked to a task
        Entity workList = schedulingFactory.createWorkList();
        Act task = (Act) schedulingFactory.newTask()
                .status(TaskStatus.PENDING)
                .startTime(new Date())
                .endTime(new Date())
                .taskType(schedulingFactory.createTaskType())
                .customer(customer)
                .workList(workList)
                .build();

        Act appointment2 = (Act) schedulingFactory.newAppointment()
                .startTime(DateRules.getToday())
                .customer(customer)
                .patient(patient)
                .schedule(schedule)
                .appointmentType(appointmentType)
                .status(AppointmentStatus.CHECKED_IN)
                .build();

        AppointmentEditor editor4 = createEditor(appointment2);
        assertFalse(editor4.makeCustomerPatientReadOnly());

        schedulingFactory.updateAppointment(appointment2)
                .task(task)
                .build();
        AppointmentEditor editor5 = createEditor(appointment2);
        assertTrue(editor5.makeCustomerPatientReadOnly());
    }

    private AppointmentEditor createEditor(Act appointment) {
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        AppointmentEditor editor = new AppointmentEditor(appointment, null, context);
        editor.getComponent();
        return editor;
    }

}
