/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.account;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.job.account.TestAccountReminderJobBuilder;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link AccountReminderJobEditor} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AccountReminderJobEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Verifies that <em>entity.accountReminderCount</em> instances are deleted when the parent job is deleted.
     */
    @Test
    public void testDelete() {
        Entity template = (Entity) documentFactory.newSMSTemplate(DocumentArchetypes.ACCOUNT_SMS_TEMPLATE)
                .content("TEXT")
                .content("Sample")
                .build();
        User user = userFactory.createUser();
        TestAccountReminderJobBuilder builder = new TestAccountReminderJobBuilder(getArchetypeService());
        Entity job = (Entity) builder
                .count(2, DateUnits.DAYS, template)
                .count(4, DateUnits.WEEKS, template)
                .runAs(user)
                .build();
        assertEquals(2, builder.getCounts().size());
        Entity count1 = (Entity) builder.getCounts().get(0);
        Entity count2 = (Entity) builder.getCounts().get(1);

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        AccountReminderJobEditor editor = new AccountReminderJobEditor(job, null, context);
        editor.delete();

        // verify the expected objects have been deleted
        assertNull(get(job));
        assertNull(get(count1));
        assertNull(get(count2));
        assertNotNull(get(template));
        assertNotNull(get(user));
    }

    /**
     * Tests the {@link AccountReminderJobEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Entity job = create(AccountReminderArchetypes.ACCOUNT_REMINDER_JOB, Entity.class);
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        AccountReminderJobEditor editor = new AccountReminderJobEditor(job, null, context);
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof AccountReminderJobEditor);
        assertEquals(job, newInstance.getObject());
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link AccountReminderJobEditor} for
     * <em>entity.jobAccountReminder</em> instances.
     */
    @Test
    public void testFactory() {
        Entity job = create(AccountReminderArchetypes.ACCOUNT_REMINDER_JOB, Entity.class);

        IMObjectEditor editor = factory.create(job, new DefaultLayoutContext(new LocalContext(),
                                                                             new HelpContext("foo", null)));
        assertTrue(editor instanceof AccountReminderJobEditor);
    }
}
