/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.history;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.patient.history.PatientHistoryCRUDWindow;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link PatientHistoryCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class PatientHistoryCRUDWindowTestCase extends AbstractAppTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Verifies a new visit cannot be created if the current visit is In Progress.
     */
    @Test
    public void testCreateVisitForExistingVisit() {
        Context context = new LocalContext();
        Party patient = (Party) patientFactory.createPatient();
        Act visit = (Act) patientFactory.newVisit()
                .patient(patient)
                .status(ActStatus.IN_PROGRESS)
                .startTime(TestHelper.getDate("2022-03-18"))
                .build();
        context.setPatient(patient);
        PatientHistoryCRUDWindow window = new PatientHistoryCRUDWindow(context, new HelpContext("foo", null));
        window.setEvent(visit);
        window.getComponent();

        // try and create a new visit.
        window.create();
        EchoTestHelper.findSelectionDialogAndSelect(PatientArchetypes.CLINICAL_EVENT);
        verifyCannotAddVisit(visit);

        // complete the existing visit
        visit.setStatus(ActStatus.COMPLETED);
        visit.setActivityEndTime(new Date());
        save(visit);

        // verify a visit can now be created
        window.create();
        EchoTestHelper.findSelectionDialogAndSelect(PatientArchetypes.CLINICAL_EVENT);
        EditDialog editDialog = EchoTestHelper.findEditDialog();
        assertNotNull(editDialog);
    }

    /**
     * Tests {@link PatientHistoryCRUDWindow#addVisitAndNote()}.
     */
    @Test
    public void testAddVisitAndNote() {
        Context context = new LocalContext();
        Party patient = (Party) patientFactory.createPatient();
        Act visit = (Act) patientFactory.newVisit()
                .patient(patient)
                .status(ActStatus.IN_PROGRESS)
                .startTime(TestHelper.getDate("2022-03-18"))
                .build();
        context.setPatient(patient);
        PatientHistoryCRUDWindow window = new PatientHistoryCRUDWindow(context, new HelpContext("foo", null));
        window.getComponent();

        // verify a vist cannot be added while the current visit is In Progress
        window.addVisitAndNote();
        verifyCannotAddVisit(visit);

        // mark the existing visit Completed.
        visit.setStatus(ActStatus.COMPLETED);
        visit.setActivityEndTime(new Date());
        save(visit);

        // verify a visit can be added
        window.addVisitAndNote();
        EditDialog editDialog = EchoTestHelper.findEditDialog();
        assertNotNull(editDialog);
    }

    /**
     * Verifies that an {@link InformationDialog} is displayed indicating a new visit cannot be created.
     * <p/>
     * The dialog will be closed.
     *
     * @param visit the existing visit
     */
    private void verifyCannotAddVisit(Act visit) {
        InformationDialog dialog = EchoTestHelper.findWindowPane(InformationDialog.class);
        assertNotNull(dialog);
        assertEquals("Cannot add Visit", dialog.getTitle());
        assertEquals("A new Visit cannot be added until the existing Visit dated "
                     + DateFormatter.formatDate(visit.getActivityStartTime(), false) + " is Completed.",
                     dialog.getMessage());
        EchoTestHelper.fireButton(dialog, InformationDialog.OK_ID);
    }
}
