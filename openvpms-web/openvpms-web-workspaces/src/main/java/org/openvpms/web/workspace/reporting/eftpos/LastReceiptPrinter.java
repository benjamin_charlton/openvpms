/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.eftpos;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.button.ButtonGroup;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentActPrinter;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.print.AbstractPrinter;
import org.openvpms.web.component.print.InteractivePrinter;
import org.openvpms.web.component.print.PrintDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Prints the last receipt from an EFTPOS terminal.
 *
 * @author Tim Anderson
 */
public class LastReceiptPrinter extends InteractivePrinter {

    /**
     * The merchant receipt. May be {@code null}
     */
    private final String merchantReceipt;

    /**
     * The merchant receipt with signature. May be {@code null}
     */
    private final String merchantReceiptWithSig;

    /**
     * The customer receipt. May be {@code null}
     */
    private final String customerReceipt;

    /**
     * Constructs an {@link InteractivePrinter}.
     *
     * @param merchantReceipt        the merchant receipt. May be {@code null}
     * @param merchantReceiptWithSig the merchant receipt with signature. May be {@code null}
     * @param customerReceipt        the customer receipt. May be {@code null}
     * @param printerContext         the printer context
     * @param context                the context
     * @param help                   the help context
     */
    public LastReceiptPrinter(String merchantReceipt, String merchantReceiptWithSig, String customerReceipt,
                              PrinterContext printerContext, Context context, HelpContext help) {
        super(new ReceiptPrinter(printerContext, context), context, help);
        this.merchantReceipt = merchantReceipt;
        this.merchantReceiptWithSig = merchantReceiptWithSig;
        this.customerReceipt = customerReceipt;
    }

    /**
     * Creates a new print dialog.
     *
     * @return a new print dialog
     */
    @Override
    protected PrintDialog createDialog() {
        return new LastReceiptPrintDialog(getHelpContext());
    }

    /**
     * Returns the underlying printer.
     *
     * @return the printer
     */
    @Override
    protected ReceiptPrinter getPrinter() {
        return (ReceiptPrinter) super.getPrinter();
    }

    class LastReceiptPrintDialog extends PrintDialog {

        /**
         * The options.
         */
        private final List<RadioButton> options = new ArrayList<>();

        /**
         * Constructs a {@link PrintDialog}.
         *
         * @param help the help context. May be {@code null}
         */
        public LastReceiptPrintDialog(HelpContext help) {
            super(Messages.get("reporting.eftpos.print.lastReceipt"), true, true, false, null, help);
            ButtonGroup group = new ButtonGroup();
            if (merchantReceipt != null) {
                options.add(ButtonFactory.create("reporting.eftpos.print.merchantreceipt", group,
                                                 new ActionListener() {
                                                     @Override
                                                     public void onAction(ActionEvent event) {
                                                         onSelected(merchantReceipt);
                                                     }
                                                 }));
            }
            if (merchantReceiptWithSig != null) {
                options.add(ButtonFactory.create("reporting.eftpos.print.merchantreceiptwithsig", group,
                                                 new ActionListener() {
                                                     @Override
                                                     public void onAction(ActionEvent event) {
                                                         onSelected(merchantReceiptWithSig);
                                                     }
                                                 }));
            }
            if (customerReceipt != null) {
                options.add(ButtonFactory.create("reporting.eftpos.print.customerreceipt", group,
                                                 new ActionListener() {
                                                     @Override
                                                     public void onAction(ActionEvent event) {
                                                         onSelected(customerReceipt);
                                                     }
                                                 }));
            }
            if (options.size() == 1) {
                options.get(0).setSelected(true);
            }
        }

        @Override
        protected void onPreview() {
            download();
        }

        @Override
        protected void onMail() {
            mail(this);
        }

        /**
         * Lays out the component prior to display.
         */
        @Override
        protected void doLayout() {
            Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING);
            doLayout(column);
            getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
        }

        /**
         * Lays out the dialog.
         *
         * @param container the container
         */
        @Override
        protected void doLayout(Component container) {
            for (RadioButton option : options) {
                container.add(option);
            }
            super.doLayout(container);
            enableButtons();
        }

        private void onSelected(String receipt) {
            LastReceiptPrinter.this.getPrinter().setReceipt(receipt);
            enableButtons();
        }

        private void enableButtons() {
            boolean selected = false;
            for (RadioButton button : options) {
                if (button.isSelected()) {
                    selected = true;
                    break;
                }
            }
            getButtons().setEnabled(OK_ID, selected);
            getButtons().setEnabled(PREVIEW_ID, selected);
            getButtons().setEnabled(MAIL_ID, selected);
        }
    }

    /**
     * Receipt printer.
     * <p/>
     * This lazily constructs the underying {@link DocumentActPrinter} when a receipt is selected.
     */
    private static class ReceiptPrinter extends AbstractPrinter {

        /**
         * The context.
         */
        private final Context context;

        /**
         * The template locator.
         */
        private final ContextDocumentTemplateLocator locator;

        /**
         * The printer.
         */
        private DocumentActPrinter printer;

        /**
         * The default printer.
         */
        private DocumentPrinter defaultPrinter;

        /**
         * Constructs a {@link ReceiptPrinter}.
         *
         * @param printerContext the archetype service
         * @param context        the context
         */
        public ReceiptPrinter(PrinterContext printerContext, Context context) {
            super(printerContext, context);
            this.context = context;
            locator = new ContextDocumentTemplateLocator(EFTPOSArchetypes.CUSTOMER_RECEIPT, context);
        }

        /**
         * Sets the receipt to print.
         *
         * @param receipt the receipt
         */
        public void setReceipt(String receipt) {
            ArchetypeService service = getService();
            DocumentAct act = service.create(EFTPOSArchetypes.CUSTOMER_RECEIPT, DocumentAct.class);
            IMObjectBean bean = service.getBean(act);
            bean.setValue("receipt", receipt);
            // NOTE: the receipt may exceed the 5000 character limit, but as the act isn't being saved, it doesn't
            // matter
            printer = new DocumentActPrinter(act, locator, getPrinterContext(), context,
                                             ServiceHelper.getBean(ReporterFactory.class));
        }

        /**
         * Prints the object.
         *
         * @param printer the printer
         */
        @Override
        public void print(DocumentPrinter printer) {
            this.printer.print(printer);
        }

        /**
         * Returns the default printer for an object.
         *
         * @return the default printer, or {@code null} if there is none defined
         * @throws OpenVPMSException for any error
         */
        public DocumentPrinter getDefaultPrinter() {
            if (defaultPrinter == null) {
                DocumentTemplate template = locator.getTemplate();
                defaultPrinter = getDefaultPrinter(template);
            }
            return defaultPrinter;
        }

        /**
         * Returns a document corresponding to that which would be printed.
         *
         * @return a document
         */
        @Override
        public Document getDocument() {
            return printer.getDocument();
        }

        /**
         * Returns a document corresponding to that which would be printed.
         * <p/>
         * If the document cannot be converted to the specified mime-type, it will be returned unchanged.
         *
         * @param mimeType the mime type. If {@code null} the default mime type associated with the report will be used.
         * @param email    if {@code true} indicates that the document will be emailed. Documents generated from
         *                 templates can perform custom formatting
         * @return a document
         */
        @Override
        public Document getDocument(String mimeType, boolean email) {
            return printer.getDocument(mimeType, email);
        }

        /**
         * Determines if printing should occur interactively.
         *
         * @return {@code true} if printing should occur interactively,
         * {@code false} if it can be performed non-interactively
         */
        @Override
        public boolean getInteractive() {
            return true;
        }

        /**
         * Returns a display name for the objects being printed.
         *
         * @return a display name for the objects being printed
         */
        @Override
        public String getDisplayName() {
            return printer.getDisplayName();
        }
    }
}