/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

/**
 * Query for <em>act.smsMessage</em>.
 *
 * @author Tim Anderson
 */
public class SMSQuery extends AbstractSMSQuery {

    /**
     * Dummy incomplete status, used in the status selector.
     */
    private static final Lookup INCOMPLETE_STATUS
            = new org.openvpms.component.business.domain.im.lookup.Lookup(
            new ArchetypeId("lookup.local"), "INCOMPLETE", Messages.get("workflow.sms.status.incomplete"));

    /**
     * The SMS message statuses.
     */
    private static final ActStatuses STATUSES = new ActStatuses(new StatusLookupQuery());


    /**
     * Constructs a {@link SMSQuery}.
     *
     * @param context the layout context
     */
    public SMSQuery(LayoutContext context) {
        super(SMSArchetypes.MESSAGE, STATUSES, context);
    }

    /**
     * Returns the act statuses to query.
     *
     * @return the act statuses to query
     */
    @Override
    protected String[] getStatuses() {
        String[] result = super.getStatuses();
        Lookup selected = getStatusSelector().getSelected();
        if (selected == INCOMPLETE_STATUS) {
            result = new String[]{OutboundMessage.Status.PENDING.toString(), OutboundMessage.Status.ERROR.toString()};
        }
        return result;
    }

    private static class StatusLookupQuery extends NodeLookupQuery {

        /**
         * Constructs a {@link StatusLookupQuery}.
         */
        StatusLookupQuery() {
            super(SMSArchetypes.MESSAGE, "status");
        }

        /**
         * Returns the default lookup.
         *
         * @return {@link #INCOMPLETE_STATUS}
         */
        @Override
        public Lookup getDefault() {
            return INCOMPLETE_STATUS;
        }

        /**
         * Returns the lookups.
         *
         * @return the lookups
         */
        @Override
        public List<Lookup> getLookups() {
            List<Lookup> lookups = super.getLookups();
            lookups.add(0, INCOMPLETE_STATUS);
            return lookups;
        }
    }
}
