/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.error.ExceptionHelper;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.workflow.EditIMObjectTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.payment.CustomerPaymentEditDialog;

import java.math.BigDecimal;
import java.util.List;

/**
 * Edits a gap payment.
 * <p>
 * This updates the claim benefit status when payment is made.
 * <p>
 * It makes no attempt to notify the insurer; that is left to the caller. This is because the status needs to be updated
 * irrespective of whether or not notification succeeds.
 *
 * @author Tim Anderson
 */
class GapPaymentRefundEditTask extends EditIMObjectTask {

    /**
     * The claim.
     */
    private final GapClaimImpl claim;

    /**
     * The gap claim practice location.
     */
    private final Party gapLocation;

    /**
     * The gap benefit amount payment till.
     */
    private final Entity gapBenefitTill;

    /**
     * Determines if a till is required for gap benefit amount payments.
     */
    private final boolean tillRequired;

    /**
     * The amount to pay or refund.
     */
    private final BigDecimal amount;

    /**
     * Determines if a payment or refund is being created.
     */
    private final boolean payment;

    /**
     * The amount already paid.
     */
    private final BigDecimal paid;

    /**
     * The logger.
     */
    private static final Log log = LogFactory.getLog(GapPaymentRefundEditTask.class);

    /**
     * Constructs a {@link GapPaymentRefundEditTask}.
     *
     * @param claim   the claim
     * @param amount  the amount to pay or refund
     * @param payment if {@code true} create a payment, else create a refund
     * @param paid    the amount already paid
     */
    public GapPaymentRefundEditTask(GapClaimImpl claim, BigDecimal amount, boolean payment, BigDecimal paid) {
        super(payment ? CustomerAccountArchetypes.PAYMENT : CustomerAccountArchetypes.REFUND, true);
        this.claim = claim;
        gapLocation = (Party) claim.getLocationParty();
        this.amount = amount;
        this.payment = payment;
        this.paid = paid;
        LocationRules rules = ServiceHelper.getBean(LocationRules.class);
        gapBenefitTill = rules.getGapBenefitTill(gapLocation);
        // if a gap claim is being made, need a gap benefit till
        tillRequired = !MathRules.isZero(claim.getBenefitAmount())
                       && (!payment || amount.add(paid).equals(claim.getGapAmount()));
    }

    /**
     * Creates a new edit dialog.
     *
     * @param editor  the editor
     * @param skip    if {@code true}, editing may be skipped
     * @param context the help context
     * @return a new edit dialog
     */
    @Override
    protected EditDialog createEditDialog(IMObjectEditor editor, boolean skip, TaskContext context) {
        return new ReloadingPaymentEditDialog((GapPaymentEditor) editor, claim, context);
    }

    /**
     * Creates a new editor for an object.
     *
     * @param object  the object to edit
     * @param context the task context
     * @return a new editor
     */
    @Override
    protected IMObjectEditor createEditor(IMObject object, TaskContext context) {
        LayoutContext layout = new DefaultLayoutContext(true, context, context.getHelpContext());
        return new GapPaymentEditor((FinancialAct) object, null, layout, amount, gapLocation, gapBenefitTill,
                                    tillRequired);
    }

    /**
     * A {@link CustomerPaymentEditDialog} that reloads the claim if the payment fails to save.
     * <p/>
     * This can occur if full payment is being made on the claim, at the same time as the benefit amount is received.
     */
    private class ReloadingPaymentEditDialog extends CustomerPaymentEditDialog {

        /**
         * The claim.
         */
        private GapClaimImpl claim;

        ReloadingPaymentEditDialog(GapPaymentEditor editor, GapClaimImpl claim, TaskContext context) {
            super(editor, (List<FinancialAct>) (List<?>) claim.getInvoices(), context);
            this.claim = claim;
        }

        /**
         * Invoked after {@link #onOK()} has completed. i.e. after:
         * <ul>
         *     <li>all EFTPOS transactions have been processed, and</li>
         *     <li>the act has been POSTED if required</li>
         * </ul>
         * This updates the claim and closes the dialog.
         */
        @Override
        protected void onOKCompleted() {
            updateClaimAndClose();
        }

        /**
         * Invoked to reload the object being edited when save fails.
         * <p/>
         * This implementation reloads the editor, but returns {@code false} if the act is saved and has been POSTED.
         *
         * @param editor the editor
         * @return {@code true} if the editor was reloaded and the act is not now POSTED.
         */
        @Override
        protected boolean reload(IMObjectEditor editor) {
            boolean result = false;
            if (super.reload(editor)) {
                // reload the claim, as that is probably what triggered the rollback in the first place
                try {
                    claim = claim.reload();
                    result = true;
                } catch (Exception exception) {
                    log.warn("Failed to reload claim: " + exception.getMessage(), exception);
                }
            }
            return result;
        }

        /**
         * Updates the claim after the payment has been finalised, and closes the dialog.
         * <p/>
         * If the update fails, prompts the user to retry.
         */
        private void updateClaimAndClose() {
            try {
                updateClaim();
                super.onOKCompleted();
            } catch (Exception exception) {
                String displayName = DescriptorHelper.getDisplayName(InsuranceArchetypes.CLAIM,
                                                                     ServiceHelper.getArchetypeService());
                String message = ErrorFormatter.format(exception, displayName);
                if (ExceptionHelper.isModifiedExternally(ExceptionUtils.getRootCause(exception))) {
                    message = Messages.format("patient.insurance.pay.retry", message);
                } else {
                    message = Messages.format("patient.insurance.pay.failwithretry", message);
                }
                ErrorDialog.newDialog()
                        .message(message)
                        .yes(this::retryUpdateClaim)
                        .no(this::onCancel)
                        .show();
            }
        }

        /**
         * Updates the claim after the payment has been finalised.
         */
        private void updateClaim() {
            GapPaymentEditor paymentEditor = (GapPaymentEditor) getEditor();
            FinancialAct act = paymentEditor.getObject();
            Context context = getContext();
            if (payment) {
                BigDecimal total = act.getTotal().add(paid);
                if (total.compareTo(claim.getTotal()) >= 0) {
                    claim.fullyPaid();
                } else if (total.compareTo(claim.getGapAmount()) == 0) {
                    claim.gapPaid(paymentEditor.getGapPaymentTill(), context.getLocation());
                }
            } else {
                // refunding down to the gap amount
                claim.gapPaid(paymentEditor.getGapPaymentTill(), context.getLocation());
            }
        }

        /**
         * Retries updating the claim after failure.
         */
        private void retryUpdateClaim() {
            boolean retry = false;
            try {
                claim = claim.reload();
                retry = true;
            } catch (Exception exception) {
                log.warn("Failed to reload claim: " + exception.getMessage(), exception);
                String displayName = DescriptorHelper.getDisplayName(InsuranceArchetypes.CLAIM,
                                                                     ServiceHelper.getArchetypeService());
                String message = ErrorFormatter.format(exception, displayName);
                ErrorDialog.newDialog()
                        .message(message)
                        .ok(this::onCancel)
                        .show();
            }
            if (retry) {
                updateClaimAndClose();
            }
        }
    }
}
