/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.workspace.customer.document.AbstractCustomerPatientDocumentBrowser;

import java.util.Date;

/**
 * A browser for customer and patient documents for insurance claims.
 *
 * @author Tim Anderson
 */
public class ClaimCustomerPatientDocumentBrowser extends AbstractCustomerPatientDocumentBrowser {

    /**
     * Constructs an {@link ClaimCustomerPatientDocumentBrowser}.
     *
     * @param customer      the customer. May be {@code null}
     * @param patient       the patient. May be {@code null}
     * @param customerFirst if {@code true} display the customer tab first, otherwise display the patient tab
     * @param from          the from date. May  be {@code null}
     * @param to            the to date. May be {@code null}
     * @param context       the layout context
     */
    public ClaimCustomerPatientDocumentBrowser(Party customer, Party patient, boolean customerFirst, Date from, Date to,
                                               LayoutContext context) {
        super(customer, patient, customerFirst, from, to, context);
    }
}
