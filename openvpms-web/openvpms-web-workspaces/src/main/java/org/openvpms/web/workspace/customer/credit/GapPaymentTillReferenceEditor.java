/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.credit;

import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.edit.AbstractIMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.till.TillQuery;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for gap payment till selection.
 *
 * @author Tim Anderson
 */
public class GapPaymentTillReferenceEditor extends AbstractIMObjectReferenceEditor<Entity> {

    /**
     * The practice location the till must belong to.
     */
    private final Party location;

    /**
     * Constructs an {@link GapPaymentTillReferenceEditor}.
     *
     * @param location the practice location the till must belong to
     * @param context  the layout context
     */
    public GapPaymentTillReferenceEditor(Party location, LayoutContext context) {
        super(createProperty(), null, context);
        this.location = location;
    }

    /**
     * Creates a query to select objects.
     *
     * @param name the name to filter on. May be {@code null}
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery(String name) {
        return new TillQuery(location);
    }

    /**
     * Creates a property to hold the selected till.
     *
     * @return the property
     */
    private static SimpleProperty createProperty() {
        SimpleProperty till = new SimpleProperty("gapTill", Reference.class);
        till.setDisplayName(Messages.get("patient.insurance.pay.till"));
        till.setRequired(true);
        till.setArchetypeRange(new String[]{TillArchetypes.TILL});
        return till;
    }
}
