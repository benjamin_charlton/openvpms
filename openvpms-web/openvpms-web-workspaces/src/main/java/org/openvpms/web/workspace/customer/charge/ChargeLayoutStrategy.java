/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.IMObjectTabPaneModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.act.ActLayoutStrategy;
import org.openvpms.web.component.property.Property;

import java.util.List;

/**
 * Layout strategy for charges.
 *
 * @author Tim Anderson
 */
public class ChargeLayoutStrategy extends ActLayoutStrategy {

    /**
     * The investigation manager, or {@code null} if the charge isn't an invoice.
     */
    private InvestigationManager investigationManager;

    /**
     * Constructs a {@link ChargeLayoutStrategy}.
     *
     * @param editor the act items editor
     */
    public ChargeLayoutStrategy(ChargeItemRelationshipCollectionEditor editor) {
        this(editor, ChargeArchetypeNodesFactory.create());
    }

    /**
     * Constructs a {@link ChargeLayoutStrategy}.
     *
     * @param editor the act items editor
     * @param nodes  the nodes to display
     */
    public ChargeLayoutStrategy(ChargeItemRelationshipCollectionEditor editor, ArchetypeNodes nodes) {
        super(editor, nodes);
        if (editor.getObject().isA(CustomerAccountArchetypes.INVOICE)) {
            investigationManager = editor.getEditContext().getInvestigations();
        }
    }

    /**
     * Lays out child components in a tab model.
     *
     * @param object     the parent object
     * @param properties the properties
     * @param model      the tab model
     * @param context    the layout context
     * @param shortcuts  if {@code true} include short cuts
     */
    @Override
    protected void doTabLayout(IMObject object, List<Property> properties, IMObjectTabPaneModel model,
                               LayoutContext context, boolean shortcuts) {
        super.doTabLayout(object, properties, model, context, shortcuts);
        if (investigationManager != null) {
            InvestigationCollectionEditor editor = new InvestigationCollectionEditor(investigationManager, object,
                                                                                     context);
            addTab(model, editor.getProperty(), new ComponentState(editor), shortcuts);
        }
    }
}
