/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.lookup;

import nextapp.echo2.app.Label;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.servlet.ServletHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.security.oauth.ClientRegistrationRepositoryImpl;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * Layout strategy for <em>lookup.oauth2ClientRegistration</em>.
 * <p/>
 * This suppresses the tenantId node for non-outlook lookups.
 *
 * @author Tim Anderson
 */
public class OAuth2ClientRegistrationLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The client registration repository.
     */
    private final ClientRegistrationRepositoryImpl repository;

    /**
     * The redirect URI.
     */
    private Label redirectURI;

    /**
     * The tenantId node.
     */
    private static final String TENANT_ID = "tenantId";

    /**
     * Constructs an {@link OAuth2ClientRegistrationLayoutStrategy}.
     */
    public OAuth2ClientRegistrationLayoutStrategy() {
        super(new ArchetypeNodes());
        repository = ServiceHelper.getBean(ClientRegistrationRepositoryImpl.class);
    }

    /**
     * Updates the redirect URI.
     *
     * @param object the lookup
     */
    public void updateRedirectURI(Lookup object) {
        redirectURI.setText(getRedirectURI(object));
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        Lookup lookup = (Lookup) object;
        if (!OAuth2ClientRegistrationEditor.OUTLOOK.equals(lookup.getCode())) {
            getArchetypeNodes().exclude(TENANT_ID);
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out components in a grid.
     *
     * @param object     the object to lay out
     * @param properties the properties
     * @param context    the layout context
     */
    @Override
    protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentGrid grid = super.createGrid(object, properties, context);
        if (redirectURI == null) {
            redirectURI = LabelFactory.create();
        }
        updateRedirectURI((Lookup) object);
        grid.add(new ComponentState(redirectURI, null, null, Messages.get("admin.lookup.oauth2.redirectUri")));
        return grid;
    }

    /**
     * Returns the redirect URI for an OAuth2 client registration.
     *
     * @param object the OAuth2 client registration
     * @return the corresponding redirect URI. May be {@code null}
     */
    private String getRedirectURI(Lookup object) {
        String result = null;
        if (object.getCode() != null) {
            result = repository.getRedirectURI(object.getCode(), ServletHelper.getRequest());
        }
        return result;
    }
}
