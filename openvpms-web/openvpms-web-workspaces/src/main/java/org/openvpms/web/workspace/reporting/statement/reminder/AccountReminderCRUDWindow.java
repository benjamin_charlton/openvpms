/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement.reminder;

import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.service.scheduler.JobScheduler;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.ActActions;
import org.openvpms.web.component.processor.BatchProcessorDialog;
import org.openvpms.web.component.workspace.AbstractViewCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;
import java.util.List;

import static org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes.ACCOUNT_REMINDER_JOB;

/**
 * CRUD window for <em>act.customerChargeReminder*</em> acts.
 *
 * @author Tim Anderson
 */
public class AccountReminderCRUDWindow extends AbstractViewCRUDWindow<Act> {

    /**
     * The account reminder rules.
     */
    private final AccountReminderRules rules;

    /**
     * Send all reminders button identifier.
     */
    private static final String SEND_ALL_ID = "button.sendAll";

    /**
     * Resolve error in selected reminder button identifier.
     */
    private static final String RESOLVE_ID = "button.resolve";

    /**
     * Resolve error in all reminders button identifier.
     */
    private static final String RESOLVE_ALL_ID = "button.resolveAll";

    /**
     * Disable reminders button identifier.
     */
    private static final String DISABLE_REMINDERS_ID = "button.disableReminders";

    /**
     * The actions that can be performed on reminders.
     */
    private static final ReminderActions actions = new ReminderActions();

    /**
     * Constructs an {@link AccountReminderCRUDWindow}.
     *
     * @param context the context
     * @param help    the help context
     */
    public AccountReminderCRUDWindow(Context context, HelpContext help) {
        super(Archetypes.create(AccountReminderArchetypes.CHARGE_REMINDER_SMS, Act.class), actions, context, help);
        rules = ServiceHelper.getBean(AccountReminderRules.class);
    }

    /**
     * Returns the charge associated with the selected reminder.
     *
     * @return the charge, or {@code null} if no reminder is selected
     */
    public Act getCharge() {
        Act reminder = getObject();
        return (reminder != null) ? getCharge(reminder) : null;
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(SEND_ALL_ID, new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onSendAll();
            }
        });
        buttons.add(RESOLVE_ID, action(AccountReminderArchetypes.CHARGE_REMINDER_SMS, this::onResolve,
                                       actions::canResolve,
                                       "reporting.statements.reminder.resolve.title"));
        buttons.add(RESOLVE_ALL_ID, new ActionListener() {
            public void onAction(ActionEvent event) {
                onResolveAll();
            }
        });
        buttons.add(DISABLE_REMINDERS_ID,
                    action(this::disableReminders, "customer.charge.disableReminders"));
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        Act object = getObject();
        buttons.setEnabled(RESOLVE_ID, enable && actions.canResolve(object));
        buttons.setEnabled(DISABLE_REMINDERS_ID, enable && canDisableReminders(object));
    }

    /**
     * View an object.
     *
     * @param object the object to view. May be {@code null}
     */
    @Override
    protected void view(Act object) {
        Act charge = (object != null) ? getCharge(object) : null;
        super.view(charge);
    }

    /**
     * Returns the charge associated with a reminder.
     *
     * @param reminder the reminder
     * @return the charge. May be {@code null}
     */
    protected FinancialAct getCharge(Act reminder) {
        return getBean(reminder).getSource("charge", FinancialAct.class);
    }

    /**
     * Triggers the configured account reminder job.
     * <p/>
     * Note that this is asynchronous and there is no facility to monitor its progress, so the workspace cannot
     * be refreshed on completion. TODO.
     */
    private void onSendAll() {
        JobScheduler scheduler = ServiceHelper.getBean(JobScheduler.class);
        List<Entity> jobs = scheduler.getJobs(ACCOUNT_REMINDER_JOB);
        String title = Messages.get("reporting.statements.reminder.send.title");
        if (!jobs.isEmpty()) {
            Entity job = jobs.get(0);
            Date run = scheduler.getNextRunTime(job);
            String scheduled = (run != null) ? DateFormatter.formatDateTimeAbbrev(run)
                                             : Messages.get("admin.job.run.never");

            ConfirmationDialog.newDialog()
                    .title(title)
                    .message(Messages.format("reporting.statements.reminder.send.message", job.getName(), scheduled))
                    .yesNo()
                    .yes(() -> scheduler.run(job))
                    .show();
        } else {
            InformationDialog.show(title, Messages.format("reporting.statements.reminder.send.nojob",
                                                          getDisplayName(ACCOUNT_REMINDER_JOB)));
        }
    }

    /**
     * Resolves a reminder error.
     *
     * @param reminder the reminder
     */
    private void onResolve(Act reminder) {
        rules.resolveError(reminder);
        onRefresh(reminder);
    }

    /**
     * Prompts to resolve errors in all reminders matching the query.
     */
    private void onResolveAll() {
        ConfirmationDialog.newDialog().title(Messages.get("reporting.statements.reminder.resolveall.prompt.title"))
                .message(Messages.get("reporting.statements.reminder.resolveall.prompt.message"))
                .yesNo()
                .yes(this::onResolveAllConfirmed)
                .show();
    }

    /**
     * Resolves all errors.
     */
    private void onResolveAllConfirmed() {
        AccountReminderQueryFactory factory = new AccountReminderQueryFactory();
        factory.setStatuses(new String[]{ReminderItemStatus.ERROR});
        ResolveAllProgressBarProcessor processor = new ResolveAllProgressBarProcessor(factory);
        BatchProcessorDialog dialog = new BatchProcessorDialog(
                Messages.get("reporting.statements.reminder.resolveall.run.title"),
                Messages.get("reporting.statements.reminder.resolveall.run.message"),
                processor, true, getHelpContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                onRefresh(getObject());
            }
        });
        dialog.show();
    }

    /**
     * Disables reminders for the charge associated with a reminder.
     *
     * @param reminder the reminder
     */
    private void disableReminders(Act reminder) {
        FinancialAct charge = getCharge(reminder);
        if (charge != null) {
            rules.disableReminders(charge);
        }
        onRefresh(reminder);
    }

    /**
     * Determines if reminders can be disabled for charge associated with the reminder.
     *
     * @param reminder the reminder
     * @return {@code true} if reminders can be disabled, otherwise {@code false}
     */
    private boolean canDisableReminders(Act reminder) {
        boolean result = false;
        if (reminder != null) {
            FinancialAct charge = getCharge(reminder);
            if (charge != null) {
                result = rules.canDisableReminders(charge);
            }
        }
        return result;
    }

    private class ResolveAllProgressBarProcessor extends AccountReminderProgressBarProcessor {

        /**
         * Constructs a {@link ResolveAllProgressBarProcessor}.
         */
        public ResolveAllProgressBarProcessor(AccountReminderQueryFactory factory) {
            super(factory);
        }

        /**
         * Processes a reminder.
         *
         * @param reminder the reminder
         * @param charge   the charge
         */
        @Override
        protected void process(Act reminder, FinancialAct charge) {
            if (rules.resolveError(reminder)) {
                updated();
            }
        }
    }

    private static final class ReminderActions extends ActActions<Act> {
        @Override
        public boolean canCreate() {
            return false;
        }

        @Override
        public boolean canDelete(Act act) {
            return false;
        }

        @Override
        public boolean canEdit(Act act) {
            return false;
        }

        @Override
        public boolean canPost(Act act) {
            return false;
        }

        @Override
        public boolean post(Act act) {
            return false;
        }

        public boolean canResolve(Act act) {
            return act != null && ReminderItemStatus.ERROR.equals(act.getStatus());
        }
    }
}