/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.investigation;

import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.order.WebOrderConfirmation;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.frame.MonitoredFrame;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * Dialog to confirm investigations with orders with status CONFIRM.
 *
 * @author Tim Anderson
 */
public class OrderConfirmationDialog extends ModalDialog {

    /**
     * The investigation being ordered.
     */
    private final Act investigation;

    /**
     * Determines if the order is confirmed.
     */
    private boolean confirmed = false;

    /**
     * Constructs an {@link OrderConfirmationDialog}.
     *
     * @param order         the order
     * @param confirmation  the confirmation
     * @param investigation the investigation being ordered
     * @param service       the laboratory service
     * @param help          the help context
     */
    public OrderConfirmationDialog(Order order, WebOrderConfirmation confirmation, Act investigation,
                                   LaboratoryService service, HelpContext help) {
        super(Messages.get("investigation.order.confirm.title"), new String[]{SKIP_ID}, help);
        this.investigation = investigation;
        setContentWidth(confirmation.getWidth());
        setContentHeight(confirmation.getHeight());
        MonitoredFrame frame = new MonitoredFrame(confirmation.getUrl());
        frame.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                if (confirmation.isConfirmed(event.getActionCommand())) {
                    confirmed = true;
                    check(order, service);
                    close(OK_ID);
                }
            }
        });
        getLayout().add(frame);
    }

    /**
     * Returns the investigation.
     *
     * @return the investigation
     */
    public Act getInvestigation() {
        return investigation;
    }

    /**
     * Invoked when the 'skip' button is pressed.
     */
    @Override
    protected void onSkip() {
        if (!confirmed) {
            ConfirmationDialog.show(Messages.get("investigation.order.skip.title"),
                                    Messages.get("investigation.order.skip.message"),
                                    ConfirmationDialog.YES_NO, new PopupDialogListener() {
                        @Override
                        public void onYes() {
                            confirmDeferred();
                            OrderConfirmationDialog.super.onSkip();
                        }
                    });
        } else {
            super.onSkip();
        }
    }

    /**
     * Defers confirmation of an order.
     */
    private void confirmDeferred() {
        if (InvestigationActStatus.CONFIRM.equals(investigation.getStatus2())) {
            investigation.setStatus2(InvestigationActStatus.CONFIRM_DEFERRED);
            ServiceHelper.getArchetypeService().save(investigation);
        }
    }

    private void check(Order order, LaboratoryService service) {
        try {
            if (service.canCheck(order)) {
                service.check(order);
            }
        } catch (Throwable exception) {
            ErrorHelper.show(exception.getMessage(), exception);
        }
    }
}
