/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.view.ReadOnlyComponentFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextComponent;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;

/**
 * Displays a summary of a gap claim.
 *
 * @author Tim Anderson
 */
public class GapClaimSummary {

    /**
     * The claim.
     */
    private final GapClaimImpl claim;

    /**
     * The amount already paid.
     */
    private final BigDecimal paid;

    /**
     * Determines if gap payment is allowed.
     */
    private final boolean canPayGap;

    /**
     * The amount remaining for gap payment.
     */
    private final BigDecimal remainingGap;

    /**
     * The amount to refund, if the paid amount is greater than the gap and a gap claim is being made.
     */
    private final BigDecimal refundAmount;

    /**
     * The amount remaining for full payment.
     */
    private final BigDecimal remainingFull;

    /**
     * The component.
     */
    private Grid component;


    /**
     * Constructs a {@link GapClaimSummary}.
     *
     * @param claim the claim
     */
    public GapClaimSummary(GapClaimImpl claim) {
        this(claim, claim.getInvoiceAllocation());
    }

    /**
     * Constructs a {@link GapClaimSummary}.
     *
     * @param claim     the claim
     * @param allocated the allocated amount. This is the amount already paid on the invoices on the claim
     */
    public GapClaimSummary(GapClaimImpl claim, BigDecimal allocated) {
        this.claim = claim;
        BigDecimal benefit = claim.getBenefitAmount();
        BigDecimal gapAmount = claim.getGapAmount();
        BigDecimal total = claim.getTotal();
        paid = allocated;

        GapClaim.GapStatus gapStatus = claim.getGapStatus();
        if (gapStatus != GapClaim.GapStatus.PENDING) {
            canPayGap = paid.compareTo(gapAmount) <= 0 && !MathRules.isZero(benefit);
            if (canPayGap) {
                remainingGap = gapAmount.subtract(paid);
                refundAmount = BigDecimal.ZERO;
            } else {
                remainingGap = BigDecimal.ZERO;
                if (paid.compareTo(gapAmount) > 0) {
                    refundAmount = paid.subtract(gapAmount);
                } else {
                    refundAmount = BigDecimal.ZERO;
                }
            }
        } else {
            canPayGap = false;
            remainingGap = BigDecimal.ZERO;
            refundAmount = BigDecimal.ZERO;
        }
        if (paid.compareTo(total) <= 0) {
            remainingFull = total.subtract(paid);
        } else {
            remainingFull = BigDecimal.ZERO;
        }
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        if (component == null) {
            ComponentGrid grid = new ComponentGrid();
            doLayout(claim, grid);
            component = grid.createGrid();

        }
        return component;
    }

    /**
     * The amount already paid towards the claim.
     *
     * @return the amount paid
     */
    public BigDecimal getPaid() {
        return paid;
    }

    /**
     * Determines if a gap payment can be made.
     *
     * @return {@code true} if a gap payment can be made, otherwise {@code false}
     */
    public boolean canPayGap() {
        return canPayGap;
    }

    /**
     * Determines if an amount needs to be refunded, in order to make a gap claim.
     * <p/>
     * This occurs if a payment has been made that exceeds the gap amount.
     *
     * @return {@code true} if a refund can be made to allow a gap claim, otherwise {@code false}
     */
    public boolean canRefund() {
        return !MathRules.isZero(refundAmount);
    }

    /**
     * Determines if the insurer can be notified that the gap has been paid.
     * <p/>
     * This doesn't apply when the full claim amount has been paid as gap claims are not available for these claims.
     *
     * @return {@code true} if the insurer can be notified that the gap has been paid
     */
    public boolean canConfirmGap() {
        return canRefund() && remainingFull.compareTo(BigDecimal.ZERO) > 0;
    }

    /**
     * The amount remaining to pay the gap.
     *
     * @return the remaining gap amount
     */
    public BigDecimal getRemainingGapAmount() {
        return remainingGap;
    }

    /**
     * Returns the amount to refund if making a gap claim, but the amount paid is greater than the gap.
     *
     * @return the refund amount
     */
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    /**
     * The amount remaining to pay the full claim.
     *
     * @return the remaining full claim amount
     */
    public BigDecimal getRemainingFullAmount() {
        return remainingFull;
    }

    /**
     * Lays out the summary in a grid.
     *
     * @param claim the claim
     * @param grid  the grid
     */
    protected void doLayout(GapClaimImpl claim, ComponentGrid grid) {
        Component benefitAmount;
        if (claim.getGapStatus() == GapClaim.GapStatus.PENDING) {
            TextField field = TextComponentFactory.create(20);
            field.setEnabled(false);
            field.setText(Messages.get("patient.insurance.pay.nobenefit"));
            benefitAmount = field;
        } else {
            benefitAmount = createAmount(claim.getBenefitAmount());
        }
        ArchetypeService service = ServiceHelper.getArchetypeService();
        grid.add(createLabel(DescriptorHelper.getDisplayName(InsuranceArchetypes.CLAIM, "patient", service)),
                 createLabel(claim.getPatient().getName()));
        grid.add(createLabel(DescriptorHelper.getDisplayName(InsuranceArchetypes.POLICY, "insurer", service)),
                 createLabel(claim.getInsurer().getName()));
        grid.add(LabelFactory.create("patient.insurance.pay.total"), createAmount(claim.getTotal()));
        grid.add(LabelFactory.create("patient.insurance.pay.benefit"), benefitAmount);
        String benefitNotes = claim.getBenefitNotes();
        if (!StringUtils.isEmpty(benefitNotes)) {
            int maxLength = Math.min(benefitNotes.length(), 100);
            TextComponent field = ReadOnlyComponentFactory.getText(benefitNotes, 20, maxLength, Styles.DEFAULT);
            grid.add(createLabel(DescriptorHelper.getDisplayName(InsuranceArchetypes.CLAIM, "benefitNotes", service)),
                     field);
        }
        grid.add(LabelFactory.create("patient.insurance.pay.gap"), createAmount(claim.getGapAmount()));
        grid.add(LabelFactory.create("patient.insurance.pay.paid"), createAmount(paid));
    }

    /**
     * Helper to create a read-only text field displaying a currency amount.
     *
     * @param amount the amount
     * @return a new text field
     */
    protected TextComponent createAmount(BigDecimal amount) {
        return TextComponentFactory.createAmount(amount, 10, true);
    }

    /**
     * Helper to create a label.
     *
     * @param text the label text
     * @return a new label
     */
    private Label createLabel(String text) {
        Label label = LabelFactory.create();
        label.setText(text);
        return label;
    }

}
