/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging;

import echopointng.TabbedPane;
import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.rules.workflow.MessageArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.DefaultContextSwitchListener;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.DefaultIMObjectTableBrowser;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.workspace.AbstractViewCRUDWindow;
import org.openvpms.web.component.workspace.BrowserCRUDWindowTab;
import org.openvpms.web.component.workspace.TabComponent;
import org.openvpms.web.component.workspace.TabbedWorkspace;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.tabpane.ObjectTabPaneModel;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.CustomerPatientSummary;
import org.openvpms.web.workspace.patient.summary.CustomerPatientSummaryFactory;
import org.openvpms.web.workspace.workflow.messaging.messages.MessageBrowser;
import org.openvpms.web.workspace.workflow.messaging.messages.MessageQuery;
import org.openvpms.web.workspace.workflow.messaging.messages.MessagingCRUDWindow;
import org.openvpms.web.workspace.workflow.messaging.sms.ReplyCRUDWindow;
import org.openvpms.web.workspace.workflow.messaging.sms.SMSCRUDWindow;
import org.openvpms.web.workspace.workflow.messaging.sms.SMSQuery;
import org.openvpms.web.workspace.workflow.messaging.sms.SMSReplyBrowser;
import org.openvpms.web.workspace.workflow.messaging.sms.SMSReplyQuery;
import org.openvpms.web.workspace.workflow.messaging.sms.SMSTableModel;


/**
 * Messaging workspace.
 *
 * @author Tim Anderson
 */
public class MessagingWorkspace extends TabbedWorkspace<Act> {

    /**
     * User preferences.
     */
    private final Preferences preferences;

    /**
     * The message tab index.
     */
    private int messageIndex = -1;

    /**
     * Constructs a {@link MessagingWorkspace}.
     *
     * @param context     the context
     * @param preferences the user preferences
     */
    public MessagingWorkspace(Context context, Preferences preferences) {
        super("workflow.messaging", context);
        this.preferences = preferences;
    }

    /**
     * Determines if the workspace can be updated with instances of the specified archetype.
     *
     * @param shortName the archetype's short name
     * @return {@code true} if {@code shortName} is one of {@link MessageArchetypes#MESSAGES}
     */
    @Override
    public boolean canUpdate(String shortName) {
        return TypeHelper.isA(shortName, MessageArchetypes.MESSAGES);
    }

    /**
     * Updates the workspace with the specified archetype.
     *
     * @param archetype the archetype
     */
    @Override
    public void update(String archetype) {
        if (TypeHelper.isA(archetype, MessageArchetypes.MESSAGES)) {
            TabbedPane pane = getTabbedPane();
            if (messageIndex != -1 && pane.getSelectedIndex() != messageIndex) {
                pane.setSelectedIndex(messageIndex);
            }
        }
    }

    /**
     * Renders the workspace summary.
     *
     * @return the component representing the workspace summary, or
     * {@code null} if there is no summary
     */
    @Override
    public Component getSummary() {
        Tab tab = (Tab) getSelected();
        Act object = tab.getWindow().getObject();
        if (object != null) {
            CustomerPatientSummaryFactory factory = ServiceHelper.getBean(CustomerPatientSummaryFactory.class);
            CustomerPatientSummary summary = factory.createCustomerPatientSummary(getContext(), getHelpContext(),
                                                                                  preferences);
            return summary.getSummary(object);
        }
        return null;
    }

    /**
     * Returns the class type that this operates on.
     *
     * @return the class type that this operates on
     */
    @Override
    protected Class<Act> getType() {
        return Act.class;
    }

    /**
     * Adds tabs to the tabbed pane.
     *
     * @param model the tabbed pane model
     */
    @Override
    protected void addTabs(ObjectTabPaneModel<TabComponent> model) {
        addMessagesBrowser(model);
        addSMSBrowser(model);
        addSMSRepliesBrowser(model);
    }

    /**
     * Adds a messages browser to the tabbed pane.
     *
     * @param model the tabbed pane model
     */
    private void addMessagesBrowser(ObjectTabPaneModel<TabComponent> model) {
        Archetypes<Act> archetypes = Archetypes.create(MessageArchetypes.MESSAGES, Act.class);
        Context context = getContext();
        HelpContext help = subtopic("message");
        MessageQuery query = new MessageQuery(context.getUser(), new DefaultLayoutContext(context, help));
        MessageBrowser browser = new MessageBrowser(query, new DefaultLayoutContext(context, help));
        MessagingCRUDWindow window = new MessagingCRUDWindow(archetypes, context, help);
        messageIndex = addTab("workflow.messaging.messages", model, new Tab(browser, window));
    }

    /**
     * Adds an SMS browser to the tabbed pane.
     *
     * @param model the tabbed pane model
     */
    private void addSMSBrowser(ObjectTabPaneModel<TabComponent> model) {
        Context context = getContext();
        HelpContext help = subtopic("sms");
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, help);
        SMSQuery query = new SMSQuery(layoutContext);

        layoutContext.setContextSwitchListener(DefaultContextSwitchListener.INSTANCE);
        QueryBrowser<Act> browser = new DefaultIMObjectTableBrowser<>(query, null, new SMSTableModel(layoutContext),
                                                                      layoutContext);
        Archetypes<Act> archetypes = Archetypes.create(SMSArchetypes.MESSAGE, Act.class);
        SMSCRUDWindow window = new SMSCRUDWindow(archetypes, context, help);
        addTab("workflow.messaging.sms", model, new Tab(browser, window));
    }

    /**
     * Adds an SMS replies browser to the tabbed pane.
     *
     * @param model the tabbed pane model
     */
    private void addSMSRepliesBrowser(ObjectTabPaneModel<TabComponent> model) {
        Context context = getContext();
        HelpContext help = subtopic("smsreply");
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, help);
        SMSReplyQuery query = new SMSReplyQuery(layoutContext);

        layoutContext.setContextSwitchListener(DefaultContextSwitchListener.INSTANCE);
        QueryBrowser<Act> browser = new SMSReplyBrowser(query, layoutContext);
        Archetypes<Act> archetypes = Archetypes.create(SMSArchetypes.REPLY, Act.class);
        ReplyCRUDWindow window = new ReplyCRUDWindow(archetypes, context, help);
        addTab("workflow.messaging.smsreply", model, new Tab(browser, window));
    }

    private class Tab extends BrowserCRUDWindowTab<Act> {

        /**
         * Constructs a {@link BrowserCRUDWindowTab}.
         *
         * @param browser the browser
         * @param window  the window
         */
        public Tab(Browser<Act> browser, AbstractViewCRUDWindow<Act> window) {
            super(browser, window);
        }

        /**
         * Invoked when the tab is displayed.
         */
        @Override
        public void show() {
            super.show();
            firePropertyChange(SUMMARY_PROPERTY, null, null);
        }

        /**
         * Selects the current object.
         *
         * @param object the selected object
         */
        @Override
        protected void select(Act object) {
            Act current = getWindow().getObject();
            super.select(object);
            if (getSelected() == this && current != object) {
                firePropertyChange(SUMMARY_PROPERTY, null, null);
            }
        }
    }
}
