/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemporaryDocumentHandler;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.internal.service.OrdersImpl;
import org.openvpms.laboratory.order.Document;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.report.ReportException;
import org.openvpms.report.openoffice.Converter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.print.IMObjectReportPrinter;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.report.StaticDocumentTemplateLocator;
import org.openvpms.web.component.print.AbstractPrinter;
import org.openvpms.web.component.print.InteractivePrinter;
import org.openvpms.web.component.print.Printer;
import org.openvpms.web.component.print.ProtectedPrinterServiceLocator;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.mr.PatientInvestigationReporter;

import java.io.IOException;

/**
 * A printer for {@link Document}s.
 *
 * @author Tim Anderson
 */
public class RequestFormPrinter extends AbstractPrinter {

    /**
     * The document to print.
     */
    private final Document document;

    /**
     * Constructs a {@link RequestFormPrinter}.
     *
     * @param document       the document to print
     * @param printerContext the printer context
     * @param context        the context
     */
    public RequestFormPrinter(Document document, PrinterContext printerContext, Context context) {
        super(printerContext, context);
        this.document = document;
    }

    /**
     * Prints the object.
     *
     * @param printer the printer
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print(DocumentPrinter printer) {
        print(getDocument(), printer);
    }

    /**
     * Returns a document corresponding to that which would be printed.
     *
     * @return a document
     * @throws OpenVPMSException for any error
     */
    @Override
    public org.openvpms.component.business.domain.im.document.Document getDocument() {
        try {
            TemporaryDocumentHandler handler = new TemporaryDocumentHandler(getService());
            return handler.create(document.getName(), document.getInputStream(), document.getMimeType(), -1);
        } catch (IOException exception) {
            throw new ReportException(ReportException.ErrorCode.FailedToCreateReport, document.getName(), exception);
        }
    }

    /**
     * Returns a document corresponding to that which would be printed.
     * <p/>
     * If the document cannot be converted to the specified mime-type, it will be returned unchanged.
     *
     * @param mimeType the mime type. If {@code null} the default mime type associated with the report will be used.
     * @param email    if {@code true} indicates that the document will be emailed. Documents generated from templates
     *                 can perform custom formatting
     * @return a document
     * @throws OpenVPMSException for any error
     */
    @Override
    public org.openvpms.component.business.domain.im.document.Document getDocument(String mimeType, boolean email) {
        org.openvpms.component.business.domain.im.document.Document document = getDocument();
        Converter converter = getConverter();
        if (mimeType != null && !mimeType.equals(document.getMimeType()) && converter.canConvert(document, mimeType)) {
            document = converter.convert(document, mimeType, email);
        }
        return document;
    }

    /**
     * Returns a display name for the objects being printed.
     *
     * @return a display name for the objects being printed
     */
    @Override
    public String getDisplayName() {
        return document.getDisplayName();
    }

    /**
     * Prints a submission document for an investigation, if one is available.
     *
     * @param investigation the investigation
     * @param context       the layout context
     */
    public static void print(Act investigation, LayoutContext context) {
        IArchetypeRuleService archetypeService = ServiceHelper.getArchetypeService();
        IMObjectBean bean = archetypeService.getBean(investigation);
        DocumentTemplate template = PatientInvestigationReporter.getTemplate(investigation, archetypeService);

        InteractivePrinter interactive = null;
        DocumentPrinterServiceLocator serviceLocator
                = ServiceHelper.getBean(DocumentPrinterServiceLocator.class);
        PrinterContext printerContext = new PrinterContext(new ProtectedPrinterServiceLocator(serviceLocator),
                                                           ServiceHelper.getArchetypeService(),
                                                           ServiceHelper.getBean(DocumentHandlers.class),
                                                           ServiceHelper.getBean(Converter.class),
                                                           ServiceHelper.getBean(DomainService.class));
        if (template != null) {
            DocumentTemplateLocator locator = new StaticDocumentTemplateLocator(template);
            ReporterFactory factory = ServiceHelper.getBean(ReporterFactory.class);
            IMObjectReportPrinter<IMObject> printer = new IMObjectReportPrinter<>(investigation, locator,
                                                                                  printerContext, context.getContext(),
                                                                                  factory);
            interactive = new InteractiveIMPrinter<>(printer, context.getContext(), context.getHelpContext());
        } else {
            Entity laboratory = bean.getTarget("laboratory", Entity.class);
            if (laboratory != null) {
                OrdersImpl orders = ServiceHelper.getBean(OrdersImpl.class);
                Order order = orders.getOrder(investigation);
                if (order != null) {
                    LaboratoryService service = ServiceHelper.getBean(LaboratoryServices.class).getService(laboratory);
                    Document document = service.getRequestForm(order);
                    if (document != null) {
                        Printer printer = new RequestFormPrinter(document, printerContext, context.getContext());
                        interactive = new InteractivePrinter(document.getDisplayName(), printer, context.getContext(),
                                                             context.getHelpContext());
                    }
                }
            }
        }
        if (interactive != null) {
            interactive.setMailContext(context.getMailContext());
            interactive.print();
        } else {
            InformationDialog.show(Messages.get("printdialog.title"), Messages.get("investigation.print.noform"));
        }
    }
}
