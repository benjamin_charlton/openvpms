/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.eftpos;

import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.service.object.DomainObjectService;
import org.openvpms.eftpos.internal.service.EFTPOSServices;
import org.openvpms.eftpos.service.EFTPOSService;
import org.openvpms.eftpos.service.Receipts;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Refund;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.report.openoffice.Converter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.ActActions;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.print.ProtectedPrinterServiceLocator;
import org.openvpms.web.component.workspace.ActCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * EFTPOS CRUD window.
 *
 * @author Tim Anderson
 */
public class EFTPOSCRUDWindow extends ActCRUDWindow<Act> {

    /**
     * Last receipt button identifier.
     */
    private static final String LAST_RECEIPT = "reporting.eftpos.print.lastReceipt";

    /**
     * Constructs an {@link EFTPOSCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param context    the context
     * @param help       the help context
     */
    public EFTPOSCRUDWindow(Archetypes<Act> archetypes, Context context, HelpContext help) {
        super(archetypes, ActActions.view(), context, help);
    }

    /**
     * Returns the customer associated with the selected transaction.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        Party result = null;
        Act object = getObject();
        if (object != null) {
            IMObjectBean bean = getBean(object);
            result = bean.getTarget("customer", Party.class);
        }
        return result;
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(LAST_RECEIPT, new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onPrintLastReceipt();
            }
        });
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        buttons.setEnabled(LAST_RECEIPT, enable);
    }

    /**
     * Invoked to reprint the last receipt(s).
     */
    private void onPrintLastReceipt() {
        Act object = getObject();
        if (object != null) {
            String merchantReceipt = null;
            String merchantReceiptWithSig = null;
            String customerReceipt = null;

            DomainObjectService domainObjectService = ServiceHelper.getBean(DomainObjectService.class);
            Transaction transaction;
            if (object.isA(EFTPOSArchetypes.PAYMENT)) {
                transaction = domainObjectService.create(object, Payment.class);
            } else {
                transaction = domainObjectService.create(object, Refund.class);
            }
            Terminal terminal = transaction.getTerminal();
            if (terminal != null) {
                EFTPOSServices services = ServiceHelper.getBean(EFTPOSServices.class);
                EFTPOSService service = services.getService(terminal);
                Receipts receipts = service.getLastReceipts(transaction);
                if (receipts != null) {
                    merchantReceipt = receipts.getMerchantReceipt(false);
                    merchantReceiptWithSig = receipts.getMerchantReceipt(true);
                    customerReceipt = receipts.getCustomerReceipt();
                }
            }
            if (merchantReceiptWithSig != null && merchantReceipt != null && customerReceipt != null) {
                DocumentPrinterServiceLocator serviceLocator
                        = ServiceHelper.getBean(DocumentPrinterServiceLocator.class);
                PrinterContext printerContext = new PrinterContext(new ProtectedPrinterServiceLocator(serviceLocator),
                                                                   ServiceHelper.getArchetypeService(),
                                                                   ServiceHelper.getBean(DocumentHandlers.class),
                                                                   ServiceHelper.getBean(Converter.class),
                                                                   ServiceHelper.getBean(DomainService.class));
                LastReceiptPrinter printer = new LastReceiptPrinter(merchantReceipt, merchantReceiptWithSig,
                                                                    customerReceipt, printerContext, getContext(),
                                                                    getHelpContext());
                printer.setMailContext(getMailContext());
                printer.print();
            } else {
                InformationDialog.show(Messages.get("reporting.eftpos.print.lastReceipt"),
                                       Messages.get("reporting.eftpos.print.noreceipts"));
            }
        }
    }
}
