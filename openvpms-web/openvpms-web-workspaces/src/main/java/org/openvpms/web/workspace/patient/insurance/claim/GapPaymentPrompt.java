/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.button.ButtonGroup;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextComponent;
import org.openvpms.web.resource.i18n.Messages;

import java.math.BigDecimal;

/**
 * Prompts to make a gap or full payment on an insurance claim.
 *
 * @author Tim Anderson
 */
class GapPaymentPrompt extends ModalDialog {

    /**
     * The gap payment button.
     */
    private final RadioButton gapButton;

    /**
     * The full payment button.
     */
    private final RadioButton fullButton;

    /**
     * The confirm gap payment button. This is available when an amount greater than the gap but less than the full
     * claim amount has been paid. It confirms gap payment, without issuing a refund down to the gap amount.
     * This is necessary when submitting multiple claims, as it avoid the need to reverse refunds.
     */
    private final RadioButton confirmGapButton;

    /**
     * The claim summary.
     */
    private final GapClaimSummary summary;

    /**
     * Determines if an amount needs to be refunded, in order to make a gap claim.
     */
    private final boolean refund;

    /**
     * Constructs a {@link GapPaymentPrompt}.
     *
     * @param claim        the claim
     * @param payFullClaim if {@code true}, select the 'full claim' option
     * @param help         the help context
     */
    public GapPaymentPrompt(GapClaimImpl claim, boolean payFullClaim, HelpContext help) {
        super(Messages.get("patient.insurance.pay.title"), OK_CANCEL, help);
        summary = new GapClaimSummary(claim);
        GapClaim.GapStatus gapStatus = claim.getGapStatus();

        ButtonGroup group = new ButtonGroup();
        ActionListener listener = new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                enableOK();
            }
        };
        refund = summary.canRefund();
        String gapText = (refund) ? "patient.insurance.pay.refund" : "patient.insurance.pay.gap";
        String fullText = (refund) ? "patient.insurance.pay.payfull" : "patient.insurance.pay.full";
        gapButton = ButtonFactory.create(gapText, group, listener);
        fullButton = ButtonFactory.create(fullText, group, listener);
        confirmGapButton = (summary.canConfirmGap())
                           ? ButtonFactory.create("patient.insurance.pay.confirmgap", group, listener) : null;
        boolean gapAllowed = summary.canPayGap();
        boolean disableGap = !gapAllowed && !refund;
        if (disableGap) {
            gapButton.setEnabled(false);
        }
        if (payFullClaim || disableGap) {
            fullButton.setSelected(true);
        }

        ComponentGrid grid = new ComponentGrid();
        BigDecimal paid = getPaid();
        if (MathRules.isZero(paid)) {
            grid.add(LabelFactory.create("patient.insurance.pay"));
        } else if (refund) {
            // refund gap or pay full amount
            grid.add(LabelFactory.create("patient.insurance.pay.refundorpay"));
        } else {
            grid.add(LabelFactory.create("patient.insurance.pay.remaining"));
        }
        BigDecimal gapAmount = (refund) ? summary.getRefundAmount() : summary.getRemainingGapAmount();
        TextComponent gapField = TextComponentFactory.createAmount(gapAmount, 10, true);
        if (disableGap) {
            gapField.setStyleName(Styles.EDIT); // will display as grey
        }
        grid.add(gapButton, gapField);
        grid.add(fullButton, TextComponentFactory.createAmount(summary.getRemainingFullAmount(), 10, true));
        if (confirmGapButton != null) {
            grid.add(confirmGapButton);
        }

        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, summary.getComponent(), grid.createGrid());
        Label message;
        if (gapAllowed) {
            if (gapStatus == GapClaim.GapStatus.PENDING) {
                message = LabelFactory.create("patient.insurance.pay.nobenefitdesc", true, true);
            } else {
                message = LabelFactory.create("patient.insurance.pay.gapdesc", true, true);
            }
        } else if (refund) {
            String text = Messages.get("patient.insurance.pay.paidmorethangapdesc");
            if (confirmGapButton != null) {
                text += "\n" + Messages.get("patient.insurance.pay.confirmdesc");
            }
            message = LabelFactory.text(text, true);
        } else {
            message = LabelFactory.create("patient.insurance.pay.fulldesc", true, true);
        }
        column.add(message);

        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
        enableOK();
        resize("GapPaymentPrompt.size");
    }

    /**
     * Determines if the gap payment option is selected.
     *
     * @return {@code true} if the gap payment option is selected
     */
    public boolean payGap() {
        return summary.canPayGap() && gapButton.isSelected();
    }

    /**
     * Determine if the confirm gap option is selected.
     * <p/>
     * @return {@code true} if the confirm gap option is selected
     */
    public boolean confirmGap() {
        return confirmGapButton != null && confirmGapButton.isSelected();
    }

    /**
     * Determines if the gap refund option is selected.
     * <p/>
     * This occurs when a payment has been made towards the claim that is greater than the gap,
     * and the user wants to make a gap claim. The difference needs to be refunded.
     *
     * @return {@code true} if the gap refund option is selected
     */
    public boolean refundGap() {
        return refund && gapButton.isSelected();
    }

    /**
     * Determines if the full payment option is selected.
     *
     * @return {@code true} if the full payment option is selected
     */
    public boolean payFull() {
        return fullButton.isSelected();
    }

    /**
     * Returns the amount already paid.
     *
     * @return the paid amount
     */
    public BigDecimal getPaid() {
        return summary.getPaid();
    }

    /**
     * Returns the amount to pay, based on the gap or full payment selection.
     *
     * @return the amount to pay
     */
    public BigDecimal getAmountToPay() {
        return (payGap()) ? summary.getRemainingGapAmount() : summary.getRemainingFullAmount();
    }

    /**
     * Returns the amount to refund.
     *
     * @return the amount to refund
     */
    public BigDecimal getAmountToRefund() {
        return summary.getRefundAmount();
    }


    /**
     * Determines if the gap payment option is selected.
     *
     * @param payGap if the gap payment option is selected
     */
    protected void setPayGap(boolean payGap) {
        if (gapButton.isEnabled()) {
            gapButton.setSelected(payGap);
            enableOK();
        }
    }

    /**
     * Determines if the full payment option is selected.
     *
     * @param payFull if the full payment option is selected
     */
    protected void setPayFull(boolean payFull) {
        if (fullButton.isEnabled()) {
            fullButton.setSelected(payFull);
            enableOK();
        }
    }

    /**
     * Enables/disables the OK button on selection based on the selection of the gap/full radio.
     */
    protected void enableOK() {
        getButtons().setEnabled(OK_ID, confirmGap() || payGap() || refundGap() || payFull());
    }

    /**
     * Invoked when the 'OK' button is pressed.
     */
    @Override
    protected void onOK() {
        if (confirmGap() || payGap() || refundGap() || payFull()) {
            super.onOK();
        }
    }
}
