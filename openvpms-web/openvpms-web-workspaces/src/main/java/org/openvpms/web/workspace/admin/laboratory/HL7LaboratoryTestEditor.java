/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.EntityIdentity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;

/**
 * Editor for <em>entity.laboratoryTestHL7</em>
 *
 * @author Tim Anderson
 */
public class HL7LaboratoryTestEditor extends AbstractIMObjectEditor {

    /**
     * Constructs an {@link HL7LaboratoryTestEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public HL7LaboratoryTestEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);

        IMObjectBean bean = getBean(object);
        EntityIdentity identity = bean.getObject("code", EntityIdentity.class);
        if (identity == null) {
            identity= create(LaboratoryArchetypes.TEST_CODE, EntityIdentity.class);
            bean.addValue("code", identity);
        }
    }
}
