/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.IMObjectProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.style.Styles;

import static org.openvpms.web.component.im.layout.ComponentGrid.layout;

/**
 * Layout strategy for <em>act.smsReply</em>.
 *
 * @author Tim Anderson
 */
public class SMSReplyLayoutStrategy extends AbstractSMSMessageLayoutStrategy {

    /**
     * The SMS node.
     */
    private static final String SMS = "sms";


    /**
     * Constructs an {@link SMSReplyLayoutStrategy}.
     */
    public SMSReplyLayoutStrategy() {
        this(true);
    }

    /**
     * Constructs an {@link SMSReplyLayoutStrategy}.
     *
     * @param showPatient if {@code true}, show the patient
     */
    public SMSReplyLayoutStrategy(boolean showPatient) {
        super(showPatient, START_TIME);
        ArchetypeNodes nodes = ArchetypeNodes.all()
                .exclude(CONTACT).exclude("status");
        setArchetypeNodes(nodes);
    }

    /**
     * Apply the layout strategy.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        if (parent != null) {
            // exclude details that are duplicated from the parent
            getArchetypeNodes().exclude(PHONE, CUSTOMER, PATIENT, LOCATION);
        } else {
            // NOTE: for unsolicited messages, the customer and patient are editable
            IMObject sms = getBean(object).getSource(SMS, IMObject.class);
            if (sms != null) {
                getArchetypeNodes().complex(SMS);
                addComponent(new ComponentState(getSMS(sms, context), properties.get(SMS)));
            }
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Returns a component representing the original SMS.
     *
     * @param sms     the SMS
     * @param context the layout context
     * @return the component
     */
    private Component getSMS(IMObject sms, LayoutContext context) {
        IMObjectBean bean = getBean(sms);
        Property message = new IMObjectProperty(sms, bean.getNode(MESSAGE));
        Property createdTime = new IMObjectProperty(sms, bean.getNode(CREATED_TIME));
        ComponentGrid componentGrid = new ComponentGrid();
        Label created = getCreated(createdTime);
        created.setLayoutData(layout(Alignment.ALIGN_RIGHT));
        ;
        componentGrid.add(created);
        componentGrid.add(createMultiLineText(message, 3, 10, Styles.FULL_WIDTH, context).getComponent());

        Grid grid = componentGrid.createGrid();
        grid.setWidth(new Extent(99, Extent.PERCENT)); // 100% screws up right insets on Firefox
        return ColumnFactory.create(Styles.INSET, grid);
    }
}
