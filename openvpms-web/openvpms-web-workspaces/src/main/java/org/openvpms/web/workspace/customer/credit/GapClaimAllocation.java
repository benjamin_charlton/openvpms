/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.credit;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.internal.claim.GapClaimImpl;

import java.math.BigDecimal;

/**
 * Associates a payment (or other credit) allocation with a gap claim.
 *
 * @author Tim Anderson
 */
public class GapClaimAllocation {

    /**
     * The allocation status.
     */
    public enum Status {
        NO_BENEFIT_PARTIAL_PAYMENT, // no benefit has been received from the insurer, and payment is less than the claim total
        NO_BENEFIT_FULL_PAYMENT,    // no benefit has been received from the insurer, and the claim has been fully paid
        ALLOCATION_LESS_THAN_GAP,   // benefit has been received from the insurer, but the allocation is less than the gap amount
        ALLOCATION_EQUAL_TO_GAP,    // benefit has been received from the insurer, and the allocation is equal to the the gap amount
        ALLOCATION_GREATER_THAN_GAP,// benefit has been received from the insurer, and the allocation is greater than the gap amount
        FULL_PAYMENT,               // benefit has been received from the insurer, and the claim has been fully paid
    }

    /**
     * The gap claim.
     */
    private final GapClaimImpl claim;

    /**
     * The gap claim act.
     */
    private final FinancialAct act;

    /**
     * The allocation. This is the sum of the allocations of the associated invoices, incorporating any new allocation.
     */
    private final BigDecimal allocation;

    /**
     * The sum of existing invoice allocations.
     */
    private final BigDecimal existingAllocation;

    /**
     * The sum of new credit allocations.
     */
    private final BigDecimal newAllocation;

    /**
     * The allocation status.
     */
    private final Status status;

    /**
     * The till to use when creating payments for gap claim benefit amounts.
     */
    private Entity till;

    /**
     * Constructs a {@link GapClaimAllocation}.
     *
     * @param claim              the gap claim
     * @param act                the claim act
     * @param existingAllocation the existing allocation. This is the sum of the allocations of the associated invoices
     * @param newAllocation      the sum of the new credit allocations
     * @param till               the till to use, if paying the gap
     */
    public GapClaimAllocation(GapClaimImpl claim, FinancialAct act, BigDecimal existingAllocation,
                              BigDecimal newAllocation, Entity till) {
        this.claim = claim;
        this.act = act;
        this.allocation = existingAllocation.add(newAllocation);
        this.existingAllocation = existingAllocation;
        this.newAllocation = newAllocation;
        this.till = till;
        if (benefitPending()) {
            if (allocation.compareTo(claim.getTotal()) < 0) {
                status = Status.NO_BENEFIT_PARTIAL_PAYMENT;
            } else {
                status = Status.NO_BENEFIT_FULL_PAYMENT;
            }
        } else {
            BigDecimal gapAmount = claim.getGapAmount();
            int compareTo = allocation.compareTo(gapAmount);
            if (compareTo < 0) {
                status = Status.ALLOCATION_LESS_THAN_GAP;
            } else if (compareTo == 0) {
                status = Status.ALLOCATION_EQUAL_TO_GAP;
            } else {
                // paying more than the gap amount
                if (allocation.compareTo(claim.getTotal()) < 0) {
                    // but less than the claim total
                    status = Status.ALLOCATION_GREATER_THAN_GAP;
                } else {
                    status = Status.FULL_PAYMENT;
                }
            }
        }
    }

    /**
     * Returns the claim total.
     *
     * @return the claim total
     */
    public BigDecimal getTotal() {
        return claim.getTotal();
    }

    /**
     * Returns the allocation status.
     *
     * @return the allocation
     */
    public GapClaimAllocation.Status getStatus() {
        return status;
    }

    /**
     * Returns the allocation.
     * <p>
     * This is the sum of the existing allocation, and any new payment allocation.
     *
     * @return the allocation
     */
    public BigDecimal getAllocation() {
        return allocation;
    }

    /**
     * Returns the existing claim allocation.
     * <p>
     * This is the sum of the existing invoice allocations.
     *
     * @return the existing allocation
     */
    public BigDecimal getExistingAllocation() {
        return existingAllocation;
    }

    /**
     * Returns the new credit allocation.
     * <p>
     * This is the sum of the existing allocation, and any new credit allocation.
     *
     * @return the new allocation
     */
    public BigDecimal getNewAllocation() {
        return newAllocation;
    }

    /**
     * Determines if any allocation has been made.
     *
     * @return {@code true} if an allocation has been made, otherwise {@code false}
     */
    public boolean isAllocated() {
        return !MathRules.isZero(allocation);
    }

    /**
     * Returns the gap amount.
     *
     * @return the gap amount
     */
    public BigDecimal getGapAmount() {
        return claim.getGapAmount();
    }

    /**
     * Returns the claim.
     *
     * @return the claim
     */
    public GapClaimImpl getClaim() {
        return claim;
    }

    /**
     * Returns the claim act.
     *
     * @return the claim act
     */
    public FinancialAct getAct() {
        return act;
    }

    /**
     * Returns the insurer.
     *
     * @return the insurer
     */
    public Party getInsurer() {
        return (Party) claim.getInsurer();
    }

    /**
     * Determines if a benefit amount has not yet been received from the insurer.
     *
     * @return {@code true} if a benefit hasn't been received, {@code false} if it has
     */
    public boolean benefitPending() {
        return claim.getGapStatus() == GapClaim.GapStatus.PENDING;
    }

    /**
     * Records the gap amount as being paid.
     * <p>
     * For non-zero benefit amounts, it creates a payment of that amount to adjust the customer balance.
     * This has a single Other line item, and Payment Type 'Gap Payment' which will be allocated against the claim
     * invoices, where possible.
     *
     * @param location the practice location
     * @return the payment adjustment, or {@code null} if none was created
     */
    public FinancialAct gapPaid(Party location) {
        return (FinancialAct) claim.gapPaid(till, location);
    }

    /**
     * Determines if a till is required.
     * <p/>
     * If the gap
     *
     * @return {@code true} if the status is {@link Status#ALLOCATION_EQUAL_TO_GAP}.
     */
    public boolean isTillRequired() {
        return status == Status.ALLOCATION_EQUAL_TO_GAP;
    }

    /**
     * Updates the till.
     *
     * @param till the till
     */
    public void setTill(Entity till) {
        this.till = till;
    }

    public Entity getTill() {
        return till;
    }

    /**
     * Marks the claim as fully paid.
     */
    public void fullyPaid() {
        claim.fullyPaid();
    }

}
