/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.workspace.customer.communication.CommunicationArchetypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

/**
 * A predicate that performs a text search on patient history acts.
 *
 * @author Tim Anderson
 */
public class TextSearch implements Predicate<Act> {

    /**
     * The search string.
     */
    private final String search;

    /**
     * Determines if clinician names should be checked.
     */
    private final boolean searchClinician;

    /**
     * Determines if batch numbers should be checked.
     */
    private final boolean searchBatch;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The matchers, keyed on archetype.
     */
    private static final Map<String, Matcher> matchers = new HashMap<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TextSearch.class);

    /**
     * The default matcher.
     */
    private static final Matcher DEFAULT_MATCHER = new Matcher();

    /**
     * The document matcher.
     */
    private static final DocumentMatcher DOCUMENT_MATCHER = new DocumentMatcher();

    /**
     * The investigation matcher.
     */
    private static final InvestigationMatcher INVESTIGATION_MATCHER = new InvestigationMatcher();

    /**
     * The note matcher.
     */
    private static final NoteMatcher NOTE_MATCHER = new NoteMatcher();

    /**
     * The communication matcher.
     */
    private static final CommunicationMatcher COMMUNICATION_MATCHER = new CommunicationMatcher();

    private static final LetterMatcher LETTER_MATCHER = new LetterMatcher();

    static {
        matchers.put(PatientArchetypes.CLINICAL_ADDENDUM, NOTE_MATCHER);
        matchers.put(PatientArchetypes.CLINICAL_EVENT, new EventMatcher());
        matchers.put(PatientArchetypes.CLINICAL_NOTE, NOTE_MATCHER);
        matchers.put(PatientArchetypes.CLINICAL_PROBLEM, new ProblemMatcher());
        matchers.put(CustomerAccountArchetypes.INVOICE_ITEM, new InvoiceItemMatcher());
        matchers.put(PatientArchetypes.DOCUMENT_ATTACHMENT, DOCUMENT_MATCHER);
        matchers.put(PatientArchetypes.DOCUMENT_ATTACHMENT_VERSION, DOCUMENT_MATCHER);
        matchers.put(PatientArchetypes.DOCUMENT_FORM, new FormMatcher());
        matchers.put(PatientArchetypes.DOCUMENT_IMAGE, DOCUMENT_MATCHER);
        matchers.put(PatientArchetypes.DOCUMENT_IMAGE_VERSION, DOCUMENT_MATCHER);
        matchers.put(PatientArchetypes.DOCUMENT_LETTER, LETTER_MATCHER);
        matchers.put(PatientArchetypes.DOCUMENT_LETTER_VERSION, LETTER_MATCHER);
        matchers.put(InvestigationArchetypes.PATIENT_INVESTIGATION, INVESTIGATION_MATCHER);
        matchers.put(InvestigationArchetypes.PATIENT_INVESTIGATION_VERSION, INVESTIGATION_MATCHER);
        matchers.put(PatientArchetypes.PATIENT_MEDICATION, new MedicationMatcher());
        matchers.put(PatientArchetypes.PATIENT_WEIGHT, DEFAULT_MATCHER);
        matchers.put(CommunicationArchetypes.EMAIL, new EmailMatcher());
        matchers.put(CommunicationArchetypes.NOTE, COMMUNICATION_MATCHER);
        matchers.put(CommunicationArchetypes.MAIL, COMMUNICATION_MATCHER);
        matchers.put(CommunicationArchetypes.PHONE, COMMUNICATION_MATCHER);
        matchers.put(SMSArchetypes.MESSAGE, new SMSMatcher());
    }

    /**
     * Constructs a {@link TextSearch}.
     *
     * @param search          the search string, case insensitive
     * @param searchClinician if {@code true}, search clinician names
     * @param searchBatch     if {@code true}, search batch numbers
     * @param service         the archetype service
     */
    public TextSearch(String search, boolean searchClinician, boolean searchBatch, IArchetypeService service) {
        this.search = search.toLowerCase();
        this.searchClinician = searchClinician;
        this.searchBatch = searchBatch;
        this.service = service;
    }

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param act the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    @Override
    public boolean test(Act act) {
        Matcher matcher = matchers.get(act.getArchetype());
        if (matcher == null) {
            matcher = DEFAULT_MATCHER;
        }
        IMObjectBean bean = service.getBean(act);
        return matcher.match(search, bean, searchClinician, searchBatch);
    }

    /**
     * Determines if an object matches the search criteria.
     */
    private static class Matcher {

        /**
         * The nodes to search.
         */
        private final String[] nodes;

        /**
         * The lookups to search.
         */
        private final String[] lookups;

        /**
         * The targets to search.
         */
        private final String[] targets;

        /**
         * Default constructor.
         */
        Matcher() {
            this(new String[0]);
        }

        /**
         * Constructs a {@link Matcher} with the nodes to search.
         *
         * @param nodes the nodes
         */
        Matcher(String[] nodes) {
            this(nodes, new String[0], new String[0]);
        }

        /**
         * Constructs a {@link Matcher} with the nodes and lookups to search.
         *
         * @param nodes   the nodes
         * @param lookups the lookups
         */
        Matcher(String[] nodes, String[] lookups) {
            this(nodes, lookups, new String[0]);
        }

        /**
         * Constructs a {@link Matcher} with the nodes, lookups and targets to search.
         *
         * @param nodes   the nodes
         * @param lookups the lookups
         * @param targets the targets
         */
        Matcher(String[] nodes, String[] lookups, String[] targets) {
            this.nodes = nodes;
            this.lookups = lookups;
            this.targets = targets;
        }

        /**
         * Determines if the object matches the search criteria.
         *
         * @param search          the search criteria
         * @param bean            the object
         * @param searchClinician if {@code true}, search the clinician
         * @param searchBatch     if {@code true}, search the batch
         * @return {@code true} if the object is a match
         */
        public boolean match(String search, IMObjectBean bean, boolean searchClinician, boolean searchBatch) {
            if (matchesSearch(search, bean.getObject().getDescription())) {
                return true;
            }
            if (matchesSearch(search, bean.getDisplayName())) {
                return true;
            }
            if (nodes.length != 0 && matchesNode(search, bean, nodes)) {
                return true;
            }
            if (lookups.length != 0 && matchesLookup(search, bean, lookups)) {
                return true;
            }
            if (targets.length != 0 && matchesTarget(search, bean, targets)) {
                return true;
            }
            if (searchClinician && matchesTarget(search, bean, "clinician")) {
                return true;
            }
            return searchBatch && matchesTarget(search, bean, "batch");
        }

        /**
         * Determines a node matches the search criteria.
         *
         * @param search the search criteria
         * @param bean   the object
         * @param nodes  the nodes to search
         * @return {@code true} if a node matches the search criteria
         */
        boolean matchesNode(String search, IMObjectBean bean, String... nodes) {
            for (String node : nodes) {
                if (bean.hasNode(node)) {
                    if (matchesSearch(search, bean.getString(node))) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Determines if a lookup matches the search criteria.
         *
         * @param search the search criteria
         * @param bean   the object
         * @param nodes  the lookup nodes
         * @return {@code true} if a lookup matches the search criteria on name
         */
        boolean matchesLookup(String search, IMObjectBean bean, String... nodes) {
            for (String node : nodes) {
                if (bean.hasNode(node) && matchesName(search, bean.getLookup(node))) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Determines if a target matches the search criteria.
         *
         * @param search the search criteria
         * @param bean   the object
         * @param nodes  the target nodes
         * @return {@code true} if a target matches the search criteria on name
         */
        boolean matchesTarget(String search, IMObjectBean bean, String... nodes) {
            for (String node : nodes) {
                if (bean.hasNode(node)) {
                    if (matchesName(search, bean.getTarget(node))) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Determines if an object matches on name.
         *
         * @param search the search criteria
         * @param object the object
         * @return {@code true} if the object matches the search criteria on name
         */
        boolean matchesName(String search, IMObject object) {
            return object != null && matchesSearch(search, object.getName());
        }

        /**
         * Determines if the search string is contained in the supplied text.
         *
         * @param search the search string
         * @param text   the text, Nay be {@code null}
         * @return {@code true} if the search string is in the text, case-insensitive
         */
        boolean matchesSearch(String search, String text) {
            return text != null && text.toLowerCase().contains(search);
        }

        /**
         * Determines if the search string is contained in the supplied text.
         * <p/>
         * TODO - add support to search via query.
         *
         * @param search the search string
         * @param bean   the object
         * @return {@code true} if the search string is in the text, case-insensitive
         */
        boolean matchesDocument(String search, IMObjectBean bean) {
            DocumentAct act = (DocumentAct) bean.getObject();
            Document document = (Document) IMObjectHelper.getObject(act.getDocument());
            if (document != null) {
                try (InputStream stream = document.getContent()) {
                    String content = IOUtils.toString(stream, StandardCharsets.UTF_8);
                    if (matchesSearch(search, content)) {
                        return true;
                    }
                } catch (Throwable exception) {
                    log.error("Failed to read document=" + document.getObjectReference() + ": "
                              + exception.getMessage(), exception);
                }
            }
            return false;
        }
    }

    private static class DocumentMatcher extends Matcher {
        DocumentMatcher() {
            super(new String[]{"fileName"});
        }

        DocumentMatcher(String[] nodes, String[] lookups, String[] targets) {
            super(nodes, lookups, targets);
        }
    }

    private static class CommunicationMatcher extends Matcher {

        CommunicationMatcher() {
            this(new String[]{"address", "message", "note"}, new String[]{"reason"});
        }

        CommunicationMatcher(String[] nodes) {
            this(nodes, new String[]{"reason"});
        }

        CommunicationMatcher(String[] nodes, String[] lookups) {
            super(nodes, lookups, new String[0]);
        }

        @Override
        public boolean match(String search, IMObjectBean bean, boolean searchClinician, boolean searchBatch) {
            if (super.match(search, bean, searchClinician, searchBatch)) {
                return true;
            }
            // communication acts will have a document if the content was too big to fit in the message node
            return matchesDocument(search, bean);
        }
    }

    private static class EmailMatcher extends CommunicationMatcher {
        EmailMatcher() {
            super(new String[]{"from", "address", "cc", "bcc", "message", "note"});
        }
    }

    private static class SMSMatcher extends Matcher {
        public SMSMatcher() {
            super(new String[]{"phone", "message", "note"}, new String[]{"reason"});
        }
    }

    private static class EventMatcher extends Matcher {

        EventMatcher() {
            super(new String[]{"title"}, new String[]{"reason"});
        }
    }

    private static class FormMatcher extends Matcher {

        FormMatcher() {
            super(new String[0], new String[0], new String[]{"documentTemplate", "product"});
        }
    }

    private static class LetterMatcher extends Matcher {

        LetterMatcher() {
            super(new String[]{"fileName"}, new String[0], new String[]{"documentTemplate", "product"});
        }
    }

    private static class InvoiceItemMatcher extends Matcher {

        InvoiceItemMatcher() {
            super(new String[0], new String[0], new String[]{"product"});
        }
    }

    private static class NoteMatcher extends Matcher {

        NoteMatcher() {
            super(new String[]{"note"});
        }

        /**
         * Determines if the object matches the search criteria.
         *
         * @param search          the search criteria
         * @param bean            the object
         * @param searchClinician if {@code true}, search the clinician
         * @param searchBatch     if {@code true}, search the batch
         * @return {@code true} if the object is a match
         */
        @Override
        public boolean match(String search, IMObjectBean bean, boolean searchClinician, boolean searchBatch) {
            boolean result = super.match(search, bean, searchClinician, searchBatch);
            if (!result) {
                result = matchesDocument(search, bean);
            }
            return result;
        }
    }

    private static class InvestigationMatcher extends Matcher {
        InvestigationMatcher() {
            super(new String[]{"fileName"}, new String[0], new String[]{"investigationType", "product"});
        }

        @Override
        public boolean match(String search, IMObjectBean bean, boolean searchClinician, boolean searchBatch) {
            if (matchesSearch(search, Long.toString(bean.getObject().getId()))) {
                return true;
            }
            return super.match(search, bean, searchClinician, searchBatch);
        }
    }

    private static class MedicationMatcher extends Matcher {

        MedicationMatcher() {
            super(new String[]{"label"}, new String[0], new String[]{"product"});
        }
    }

    private static class ProblemMatcher extends Matcher {

        ProblemMatcher() {
            super(new String[0], new String[]{"presentingComplaint"}, new String[0]);
        }
    }
}
