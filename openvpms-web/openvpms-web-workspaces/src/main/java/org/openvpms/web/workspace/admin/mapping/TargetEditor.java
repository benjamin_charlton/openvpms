/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.openvpms.web.component.im.edit.AbstractSelectorPropertyEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserDialog;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.select.AbstractQuerySelector;
import org.openvpms.web.component.property.AbstractEditor;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;

/**
 * An editor for {@link Target}s that displays a browser of available targets.
 *
 * @author Tim Anderson
 */
public class TargetEditor extends AbstractEditor {

    /**
     * The mappings.
     */
    private final Mappings<?> mappings;

    /**
     * The property editor.
     */
    private final AbstractSelectorPropertyEditor<Target> editor;

    /**
     * Constructs a {@link TargetEditor}.
     *
     * @param context the layout context
     */
    public TargetEditor(Target target, Mappings<?> mappings, LayoutContext context) {
        this.mappings = mappings;
        editor = new TargetPropertyEditor(target, mappings.getTargetDisplayName(), context);
        editor.addModifiableListener(modifiable -> notifyListeners());
    }

    /**
     * Returns the target.
     *
     * @return the target. May be {@code null}
     */
    public Target getObject() {
        return editor.getObject();
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        return editor.getComponent();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return editor.validate(validator);
    }

    private class TargetBrowser extends MappingBrowser<Target> {

        /**
         * Constructs a {@link TargetBrowser}.
         *
         * @param value   the initial value
         * @param context the layout context
         */
        TargetBrowser(String value, LayoutContext context) {
            super(value, new TargetTableModel(), context);
        }

        /**
         * Creates a new result set.
         *
         * @param value      the value to filter on. May be {@code null}
         * @param maxResults the maximum number of results per page
         * @return the result set
         */
        @Override
        protected ResultSet<Target> createResultSet(String value, int maxResults) {
            return new TargetResultSet(value, maxResults, mappings);
        }
    }

    private class TargetPropertyEditor extends AbstractSelectorPropertyEditor<Target> {

        TargetPropertyEditor(Target target, String displayName, LayoutContext context) {
            super(new SimpleProperty("target", target, Target.class, displayName), context);
            updateSelector();
        }

        /**
         * Returns the object corresponding to the property.
         *
         * @return the object. May be {@code null}
         */
        @Override
        public Target getObject() {
            return (Target) getProperty().getValue();
        }

        /**
         * Validates the object.
         *
         * @param validator the validator
         * @return {@code true} if the object and its descendants are valid otherwise {@code false}
         */
        @Override
        protected boolean doValidation(Validator validator) {
            boolean result = false;
            AbstractQuerySelector<Target> selector = getSelector();
            if (!selector.inSelect()) {
                if (!selector.isValid()) {
                    String message = Messages.format("mapping.targetnotfound", selector.getText(),
                                                     editor.getProperty().getDisplayName());
                    validator.add(this, new ValidatorError(message));
                } else {
                    result = super.doValidation(validator);
                }
            }
            return result;
        }

        /**
         * Creates a new selector.
         *
         * @param property    the property
         * @param context     the layout context
         * @param allowCreate determines if objects may be created
         * @return a new selector
         */
        @Override
        protected AbstractQuerySelector<Target> createSelector(Property property, LayoutContext context,
                                                               boolean allowCreate) {
            return new TargetSelector(property, allowCreate, context);
        }

        /**
         * Updates the underlying property with the specified value.
         *
         * @param property the property
         * @param value    the value to update with. May be {@code null}
         * @return {@code true} if the property was modified
         */
        @Override
        protected boolean updateProperty(Property property, Target value) {
            return property.setValue(value);
        }

        private class TargetSelector extends AbstractQuerySelector<Target> {
            TargetSelector(Property property, boolean allowCreate, LayoutContext context) {
                super(property.getDisplayName(), allowCreate, context);
            }

            @Override
            protected String getName(Target object) {
                return object.getName();
            }

            @Override
            protected String getDescription(Target object) {
                return null;
            }

            @Override
            protected boolean getActive(Target object) {
                return object.isActive();
            }

            /**
             * Creates a new browser.
             *
             * @param value    a value to filter results by. May be {@code null}
             * @param runQuery if {@code true} run the query
             * @return a return a new browser
             */
            @Override
            protected Browser<Target> createBrowser(String value, boolean runQuery) {
                return new TargetBrowser(value, getLayoutContext());
            }

            /**
             * Creates a new dialog to display a browser.
             *
             * @param browser the browser
             * @return the dialog
             */
            @Override
            protected BrowserDialog<Target> createDialog(Browser<Target> browser) {
                BrowserDialog<Target> dialog = super.createDialog(browser);
                dialog.resize("BrowserDialog.size.Mapping.Target");
                return dialog;
            }

            /**
             * Returns the results matching the specified value.
             *
             * @param value the value. May be {@code null}
             * @return the results matching the value
             */
            @Override
            protected ResultSet<Target> getMatches(String value) {
                return new TargetResultSet(value, MappingBrowser.MAX_RESULTS, mappings);
            }

            /**
             * Invoked when the text field is updated.
             * <p/>
             * This is overridden to set the modified flag.
             */
            @Override
            protected void onTextChanged() {
                super.onTextChanged();
                setModified();
                notifyListeners();
            }
        }
    }

    private static class TargetTableModel extends AbstractMappingTableModel<Target> {

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(Target object, TableColumn column, int row) {
            if (column.getModelIndex() == ID_INDEX) {
                return object.getId();
            }
            return object.getName();
        }
    }
}
