/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.event.Vetoable;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.credit.CreditActEditDialog;

import java.util.List;

/**
 * An edit dialog for customer payments and refunds.
 * <p/>
 * This manages EFT transactions.
 *
 * @author Tim Anderson
 */
public class CustomerPaymentEditDialog extends CreditActEditDialog {

    /**
     * Constructs a {@link CustomerPaymentEditDialog}.
     *
     * @param editor  the editor
     * @param context the context
     */
    public CustomerPaymentEditDialog(AbstractCustomerPaymentEditor editor, Context context) {
        super(editor, context);
        init(editor);
    }

    /**
     * Constructs a {@link CustomerPaymentEditDialog}.
     *
     * @param editor  the editor
     * @param debits  debits to allocate against
     * @param context the context
     */
    public CustomerPaymentEditDialog(AbstractCustomerPaymentEditor editor, List<FinancialAct> debits, Context context) {
        super(editor, debits, context);
        init(editor);
    }

    /**
     * Returns the editor.
     *
     * @return the editor
     */
    @Override
    public AbstractCustomerPaymentEditor getEditor() {
        return (AbstractCustomerPaymentEditor) super.getEditor();
    }

    /**
     * Saves the current object, if saving is enabled.
     */
    @Override
    protected void onApply() {
        if (save()) {
            AbstractCustomerPaymentEditor editor = getEditor();
            if (editor.requiresEFTTransaction()) {
                // if there are multiple EFT transactions, this will be called recursively
                editor.processEFTTransaction(this::onApply);
            }
        }
    }

    /**
     * Saves the current object, if saving is enabled, and closes the editor.
     */
    @Override
    protected void onOK() {
        if (save()) {
            AbstractCustomerPaymentEditor editor = getEditor();
            if (editor.requiresEFTTransaction()) {
                // call this recursively if the EFT transaction is successfully processed
                editor.processEFTTransaction(this::onOK);
            } else if (editor.postOnCompletion()) {
                editor.setStatus(ActStatus.POSTED);
                if (save()) {
                    onOKCompleted();
                }
            } else {
                onOKCompleted();
            }
        }
    }

    /**
     * Invoked after {@link #onOK()} has completed. i.e. after:
     * <ul>
     *     <li>all EFTPOS transactions have been processed, and</li>
     *     <li>the act has been POSTED if required</li>
     * </ul>
     * This closes the dialog.
     */
    protected void onOKCompleted() {
        close(OK_ID);
    }

    /**
     * Initialises this.
     *
     * @param editor the editor
     */
    private void init(AbstractCustomerPaymentEditor editor) {
        if (editor.isPosted() && editor.canChangeStatus()) {
            // allow Apply to be invoked to enable split transactions when performing EFT
            editor.makeSaveableAndPostOnCompletion();
        }
        setCancelListener(this::confirmCancel);
    }

    /**
     * Determines if the payment should be deleted on cancellation.
     *
     * @param action the action to veto or allow
     */
    private void confirmCancel(Vetoable action) {
        if (canSave()) {
            AbstractCustomerPaymentEditor editor = getEditor();
            if (FinancialActStatus.IN_PROGRESS.equals(editor.getSavedStatus())) {
                if (editor.canDelete() && editor.isEmptyOrOnlyHasEFTItems()) {
                    // If the editor is empty or the only transactions are EFT ones (that support deletion),
                    // prompt the user if they want to delete the payment instead.
                    ConfirmationDialog.newDialog().title(Messages.get("editor.cancel.title"))
                            .message(Messages.format("customer.payment.cancel.delete", editor.getDisplayName()))
                            .yesNoCancel()
                            .yes(true, () -> {
                                // disable shortcut to avoid accidental confirmation
                                editor.delete();
                                action.veto(false);
                            })
                            .no(() -> action.veto(false))
                            .cancel(() -> action.veto(true))
                            .show();
                } else {
                    // don't give the option to delete, but warn that it isn't finalised
                    ConfirmationDialog.newDialog().title(Messages.get("editor.cancel.title"))
                            .message(Messages.format("customer.payment.cancel.unfinalised", editor.getDisplayName()))
                            .yesNo()
                            .yes(() -> action.veto(false))
                            .no(() -> action.veto(true))
                            .show();
                }
            } else {
                action.veto(false);
            }
        } else {
            action.veto(false);
        }
    }
}