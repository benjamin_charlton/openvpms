/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.Color;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.im.table.DescriptorTableModel;
import org.openvpms.web.component.property.DocumentBackedTextProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;

import java.util.List;

/**
 * Table model for <em>act.patientInvestigationResultItem</em>.
 *
 * @author Tim Anderson
 */
public class ResultTableModel extends DescriptorTableModel<Act> {

    /**
     * The nodes to display.
     */
    private ArchetypeNodes nodes = ArchetypeNodes.nodes(ANALYTE_CODE, ANALYTE_NAME, RESULT, REFERENCE_RANGE, STATUS);

    /**
     * Determines if the units column is displayed.
     */
    private boolean showUnits = true;

    /**
     * Determines if the qualifier column is displayed.
     */
    private boolean showQualifier = true;

    /**
     * Determines if the reference range column is displayed.
     */
    private boolean showReferenceRange = true;

    /**
     * Analyte code node.
     */
    private static final String ANALYTE_CODE = "analyteCode";

    /**
     * Analyte name node.
     */
    private static final String ANALYTE_NAME = "name";


    /**
     * Result node.
     */
    private static final String RESULT = "result";

    /**
     * Long result relationship node.
     */
    private static final String LONG_RESULT = "longResult";

    /**
     * Image relationship node.
     */
    private static final String IMAGE = "image";

    /**
     * Out of range node.
     */
    private static final String OUT_OF_RANGE = "outOfRange";

    /**
     * Units node.
     */
    private static final String UNITS = "units";

    /**
     * Qualifier node.
     */
    private static final String QUALIFIER = "qualifier";

    /**
     * Reference range node.
     */
    private static final String REFERENCE_RANGE = "referenceRange";

    /**
     * Status node.
     */
    private static final String STATUS = "status";


    /**
     * Constructs a {@link ResultTableModel}.
     *
     * @param shortNames the archetype short names. May contain wildcards
     * @param context    the layout context
     */
    public ResultTableModel(String[] shortNames, LayoutContext context) {
        super(context);
        setTableColumnModel(createColumnModel(shortNames, context));
    }

    /**
     * Sets the objects to display.
     * <p/>
     * This shows/hides the units and/or qualifier columns, based on the presence of data.
     *
     * @param objects the objects to display
     */
    @Override
    public void setObjects(List<Act> objects) {
        boolean newShowUnits = false;
        boolean newShowQualifier = false;
        boolean newShowReferenceRange = false;
        for (Act act : objects) {
            IMObjectBean bean = getBean(act);
            if (!newShowUnits && bean.getString(UNITS) != null) {
                newShowUnits = true;
            }
            if (!newShowQualifier && bean.getString(QUALIFIER) != null) {
                newShowQualifier = true;
            }
            if (!newShowReferenceRange && bean.getString(REFERENCE_RANGE) != null) {
                newShowReferenceRange = true;
            }
            if (newShowUnits && newShowQualifier && newShowReferenceRange) {
                break;
            }
        }
        if (newShowUnits != showUnits || newShowQualifier != showQualifier
            || newShowReferenceRange != showReferenceRange) {
            // oh for a table model that can hide columns...
            nodes = ArchetypeNodes.nodes(ANALYTE_CODE, ANALYTE_NAME, RESULT);
            showUnits = newShowUnits;
            showQualifier = newShowQualifier;
            showReferenceRange = newShowReferenceRange;
            if (showUnits) {
                nodes.list(UNITS);
            }
            if (showQualifier) {
                nodes.list(QUALIFIER);
            }
            if (showReferenceRange) {
                nodes.list(REFERENCE_RANGE);
            }
            nodes.list(STATUS);
            setTableColumnModel(createColumnModel(new String[]{InvestigationArchetypes.RESULT}, getLayoutContext()));
        }
        super.setObjects(objects);
    }

    /**
     * Returns a value for a given column.
     *
     * @param object the object to operate on
     * @param column the column
     * @param row    the row
     * @return the value for the column
     */
    @Override
    protected Object getValue(Act object, DescriptorTableColumn column, int row) {
        Object result;
        boolean outOfRange = getBean(object).getBoolean(OUT_OF_RANGE);
        if (column.getName().equals(RESULT)) {
            // if the result is multi-line or long, limit it to a single line of up to 50 characters
            result = getResult(object);
        } else {
            result = super.getValue(object, column, row);
        }
        if (outOfRange && result instanceof Component) {
            Component component = (Component) result;
            component.setForeground(Color.RED);
            component.setStyleName(Styles.BOLD);
        }
        return result;
    }

    /**
     * Returns an {@link ArchetypeNodes} that determines what nodes appear in the table.
     *
     * @return the nodes to include
     */
    @Override
    protected ArchetypeNodes getArchetypeNodes() {
        return nodes;
    }

    /**
     * Returns a component representing the result, or {@code null} if there are none.
     * <p/>
     * This may include the result text and/or image if they are present.
     * <br/>
     * Any text will be truncated if it is too long.
     *
     * @param object the result act
     * @return the result component, or {@code null} if there are none
     */
    private Component getResult(Act object) {
        Component result = null;
        Property property = new DocumentBackedTextProperty(object, RESULT, LONG_RESULT);
        String text = StringUtils.abbreviate(property.getString(), 50);
        Label chart = null;
        IMObjectBean bean = getBean(object);
        if (bean.getTargetRef(IMAGE) != null) {
            chart = LabelFactory.create(null, "chart");
        }
        if (text != null && chart != null) {
            result = RowFactory.create(Styles.WIDE_CELL_SPACING, LabelFactory.text(text), chart);
        } else if (text != null) {
            result = LabelFactory.text(text);
        } else if (chart != null) {
            result = chart;
        }
        return result;
    }

}
