/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.Label;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.investigation.PatientInvestigationActEditor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Remove states for invoice items.
 * <p>
 * This determines if  investigations will be affected by removal of invoice items.
 *
 * @author Tim Anderson
 */
public class InvoiceItemRemovalStates extends ChargeRemovalStates {

    /**
     * The investigation manager.
     */
    private final InvestigationManager manager;

    /**
     * The remaining tests associated with an investigation, if associated invoice item(s) are removed.
     */
    private Map<PatientInvestigationActEditor, List<Entity>> testsByInvestigation = new LinkedHashMap<>();

    /**
     * The investigation editors associated with an invoice item.
     */
    private Map<FinancialAct, List<PatientInvestigationActEditor>> editorsByInvoiceItem = new LinkedHashMap<>();

    /**
     * The tests that an invoice item orders.
     */
    private Map<FinancialAct, List<Entity>> testsByInvoiceItem = new LinkedHashMap<>();

    /**
     * Constructs an {@link InvoiceItemRemovalStates}.
     *
     * @param manager the investigation manager
     */
    public InvoiceItemRemovalStates(InvestigationManager manager) {
        this.manager = manager;
    }

    /**
     * Determines the investigations affected by the removal of an invoice item.
     *
     * @param itemEditor the invoice item editor
     */
    public void determineAffectedInvestigations(CustomerChargeActItemEditor itemEditor) {
        FinancialAct item = itemEditor.getObject();
        List<PatientInvestigationActEditor> editors = new ArrayList<>();
        for (DocumentAct investigation : manager.getInvestigations(item)) {
            if (!InvestigationActStatus.CANCELLED.equals(investigation.getStatus())) {
                PatientInvestigationActEditor editor = manager.getEditor(investigation.getObjectReference());
                if (editor != null) {
                    editors.add(editor);
                    Product product = itemEditor.getProduct();
                    if (product != null) {
                        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(product);
                        if (bean.hasNode("tests")) {
                            List<Entity> tests = bean.getTargets("tests", Entity.class);
                            if (!tests.isEmpty()) {
                                testsByInvoiceItem.put(item, tests);
                                List<Entity> remaining = getRemainingTests(editor);
                                remaining.removeAll(tests);
                            }
                        }
                    }
                }
            }
        }
        if (!editors.isEmpty()) {
            editorsByInvoiceItem.put(item, editors);
        }
    }

    /**
     * Returns the investigation editors associated with invoice items being removed.
     *
     * @return the investigation editors, keyed on invoice item
     */
    public Map<FinancialAct, List<PatientInvestigationActEditor>> getAffectedEditorsByInvoiceItem() {
        return editorsByInvoiceItem;
    }

    /**
     * Determines if an investigation would have no tests after the removal of associated invoice items.
     *
     * @param editor the investigation editor
     * @return {@code true} if the investigation would have no more tests
     */
    public boolean hasNoMoreTests(PatientInvestigationActEditor editor) {
        List<Entity> tests = testsByInvestigation.get(editor);
        return tests != null && tests.isEmpty();
    }

    /**
     * Creates a table model to display charges.
     *
     * @param archetype the charge archetype
     * @param context   the layout context
     * @return the table model
     */
    @Override
    protected IMObjectTableModel<Act> createModel(String archetype, LayoutContext context) {
        return new InvoiceItemModel(archetype, context);
    }

    /**
     * Removes the remaining tests for an investigation.
     *
     * @param editor the investigation editor
     * @return the remaining tests
     */
    private List<Entity> getRemainingTests(PatientInvestigationActEditor editor) {
        return testsByInvestigation.computeIfAbsent(editor, e -> new ArrayList<>(e.getTests()));
    }

    /**
     * A {@link ChargeModel} that includes tests ordered by products.
     */
    private class InvoiceItemModel extends ChargeModel {

        /**
         * The tests model index.
         */
        private int testsIndex;

        /**
         * Constructs an {@link InvoiceItemModel}.
         *
         * @param archetype the charge archetype
         * @param context   the layout context
         */
        public InvoiceItemModel(String archetype, LayoutContext context) {
            super(archetype, context);
        }

        /**
         * Adds columns.
         *
         * @param model the column model
         */
        protected void addColumns(DefaultTableColumnModel model) {
            if (!testsByInvoiceItem.isEmpty()) {
                // if there are items being deleted with tests, include a column to display them
                testsIndex = getNextModelIndex(model);
                model.addColumn(createTableColumn(testsIndex, "customer.charge.delete.tests"));
            } else {
                testsIndex = -1;
            }
            super.addColumns(model);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the table column
         * @param row    the table row
         */
        @Override
        protected Object getValue(Act object, TableColumn column, int row) {
            Object result = null;
            if (column.getModelIndex() == testsIndex) {
                List<Entity> tests = testsByInvoiceItem.get((FinancialAct) object);
                StringBuilder builder = new StringBuilder();
                if (tests != null && !tests.isEmpty()) {
                    Label label = LabelFactory.create(true);
                    tests.sort(IMObjectSorter.getNameComparator(true));
                    for (Entity test : tests) {
                        if (builder.length() != 0) {
                            builder.append("\n");
                        }
                        builder.append(test.getName());
                    }
                    label.setText(builder.toString());
                    result = label;
                }
            } else {
                result = super.getValue(object, column, row);
            }
            return result;
        }
    }

}

