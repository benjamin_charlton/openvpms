/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.SplitPane;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.workspace.TabComponent;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.help.HelpContext;

/**
 * Account tab in the statement workspace.
 *
 * @author Tim Anderson
 */
class AccountTab implements TabComponent {

    /**
     * The customer balance query.
     */
    private final CustomerBalanceQuery query;

    /**
     * The customer balance browser.
     */
    private final CustomerBalanceBrowser browser;

    /**
     * The customer balance CRUD window.
     */
    private final CustomerBalanceCRUDWindow window;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * Determines if this is the first rendering of the workspace.
     */
    private boolean rendered;

    /**
     * Constructs an {@link AccountTab}.
     *
     * @param context the context
     * @param help    the help context
     */
    public AccountTab(Context context, HelpContext help) {
        this.help = help;
        query = new CustomerBalanceQuery(context.getPractice());
        query.getComponent();
        query.setDate(DateRules.getYesterday()); // default statement date to yesterday
        browser = new CustomerBalanceBrowser(query, new DefaultLayoutContext(context, help));
        window = new CustomerBalanceCRUDWindow(query, browser, context, help);
    }

    /**
     * Invoked when the tab is displayed.
     */
    @Override
    public void show() {
        if (rendered) {
            // make sure the account types are up-to-date
            query.refreshAccountTypes();
        } else {
            rendered = true;
        }
    }

    /**
     * Returns the tab component.
     *
     * @return the tab component
     */
    @Override
    public Component getComponent() {
        return SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP, "SplitPaneWithButtonRow",
                                       window.getComponent(), browser.getComponent());
    }

    /**
     * Returns the help context for the tab.
     *
     * @return the help context
     */
    @Override
    public HelpContext getHelpContext() {
        return help;
    }
}
