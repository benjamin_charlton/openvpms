/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.event.WindowPaneListener;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.EditActions;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.mr.PatientMedicationActEditor;

import java.util.LinkedList;
import java.util.Objects;
import java.util.function.Consumer;


/**
 * Helper to queue editing of patient medication, investigation and reminder popups, only showing one dialog at a time.
 *
 * @author Tim Anderson
 */
public class DefaultEditorQueue implements EditorQueue {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The queue of editors.
     */
    private final LinkedList<State> queue = new LinkedList<>();

    /**
     * Determines if the queue is started.
     */
    private boolean started;

    /**
     * Determines if an execution is in progress.
     */
    private boolean executing;

    /**
     * The current state.
     */
    private State current;

    /**
     * Constructs a {@link DefaultEditorQueue}.
     * <p/>
     * The queue is started.
     *
     * @param context the context
     */
    public DefaultEditorQueue(Context context) {
        this(true, context);
    }

    /**
     * Constructs a {@link DefaultEditorQueue}.
     *
     * @param started if {@code true}, the queue will process tasks immediately, otherwise it will be delayed until
     *                {@link #start} is invoked.
     * @param context the context
     */
    public DefaultEditorQueue(boolean started, Context context) {
        this.started = started;
        this.context = context;
    }

    /**
     * Start processing tasks in the queue.
     */
    public void start() {
        if (!started) {
            started = true;
            if (!executing) {
                next();
            }
        }
    }

    /**
     * Determines if an editor is in the queue.
     * <p/>
     * It may be being displayed.
     *
     * @param editor the editor
     * @return {@code true} if the editor is in the queue
     */
    @Override
    public boolean isQueued(IMObjectEditor editor) {
        if (current instanceof EditorState) {
            if (((EditorState) current).hasEditor(editor)) {
                return true;
            }
        }
        for (State state : queue) {
            if (state instanceof EditorState) {
                if (((EditorState) state).hasEditor(editor)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Queue an edit.
     *
     * @param owner    the owner of this editor
     * @param editor   the editor to queue
     * @param skip     if {@code true}, indicates that the edit can be skipped
     * @param cancel   if {@code true}, indicates that the edit can be cancelled
     * @param listener the listener to notify on completion
     */
    public void queue(IMObject owner, IMObjectEditor editor, boolean skip, boolean cancel, Listener listener) {
        queue(new EditorState(owner, editor, listener, skip, cancel));
    }

    /**
     * Queues a dialog.
     *
     * @param dialog the dialog to queue
     */
    @Override
    public void queue(PopupDialog dialog) {
        queue(null, dialog, null);
    }

    /**
     * Queues a dialog.
     *
     * @param owner  the owner of this dialog
     * @param dialog the dialog to queue
     */
    @Override
    public void queue(IMObject owner, PopupDialog dialog) {
        queue(owner, dialog, null);
    }

    /**
     * Queues a dialog.
     *
     * @param owner    the owner of this dialog
     * @param dialog   the dialog to queue
     * @param callback a callback to invoke once the dialog has closed
     */
    @Override
    public void queue(IMObject owner, PopupDialog dialog, Runnable callback) {
        queue(new DialogState(owner, dialog, callback));
    }

    /**
     * Queues a callback.
     * <p/>
     * These must execute synchronously.
     * <p/>
     * Note that calls to {@link #isComplete()} return {@code true} if the queue is empty but a callback is in progress.
     * <p/>
     * This is required so that callbacks can trigger automatic saves without affecting the valid status of editors.
     *
     * @param runnable the callback
     */
    @Override
    public void queue(Runnable runnable) {
        queue(new RunnableState(runnable));
    }

    /**
     * Queues an asynchronous callback.
     * <p/>
     * This must execute the supplied {@link Runnable} to move to the next item in the queue
     *
     * @param consumer the consumer to queue
     */
    @Override
    public void queue(Consumer<Runnable> consumer) {
        queue(new ConsumerState(consumer));
    }

    /**
     * Returns the current popup dialog.
     *
     * @return the current popup dialog. May be {@code null}
     */
    public PopupDialog getCurrent() {
        return current != null ? current.getDialog() : null;
    }

    /**
     * Cancels all editors, dialogs and callbacks for an object.
     *
     * @param owner the owner to cancel queued objects for
     */
    @Override
    public void cancel(IMObject owner) {
        boolean cancelled = false;
        if (current != null && current.isCancelledBy(owner)) {
            executing = false;
            current.cancel();
            current = null;
            cancelled = true;
        }
        if (!queue.isEmpty()) {
            cancelled |= queue.removeIf(state -> state.isCancelledBy(owner));
        }
        if (cancelled && !executing && queue.isEmpty()) {
            // nothing left to do
            completed();
        }
    }

    /**
     * Determines if editing is complete.
     *
     * @return {@code true} if there are no more popups
     */
    public boolean isComplete() {
        return !executing && queue.isEmpty();
    }

    /**
     * Processes the next queued item, if any.
     */
    protected void next() {
        executing = false;
        if (queue.isEmpty()) {
            completed();
            return;
        }
        State state = queue.removeFirst();
        state.show();
    }


    /**
     * Displays an edit dialog.
     *
     * @param dialog the dialog
     */
    protected void edit(EditDialog dialog) {
        show(dialog);
    }

    /**
     * Displays a dialog.
     *
     * @param dialog the dialog to display
     */
    protected void show(PopupDialog dialog) {
        dialog.show();
    }

    /**
     * Executes a callback.
     *
     * @param runnable the callback
     */
    protected void execute(Runnable runnable) {
        try {
            runnable.run();
            next();
        } catch (Throwable exception) {
            ErrorHelper.show(exception, (WindowPaneListener) windowPaneEvent -> next());
        }
    }

    /**
     * Invoked when an edit is skipped. Performs the next edit, if any.
     */
    protected void skipped() {
        next();
    }

    /**
     * Invoked when the edit is completed.
     */
    protected void completed() {
        current = null;
        executing = false;
    }

    /**
     * Invoked when an edit is cancelled. Skips all subsequent edits.
     */
    protected void cancelled() {
        while (!queue.isEmpty()) {
            State state = queue.removeFirst();
            if (state instanceof EditorState) {
                ((EditorState) state).skip();
            }
        }
        executing = false;
    }

    /**
     * Shows the dialog associated with a {@link State}.
     */
    private void show(State state) {
        setExecuting(state);
        show(state.getDialog());
    }

    /**
     * Shows an edit dialog.
     *
     * @param state the current state
     */
    private void edit(EditorState state) {
        setExecuting(state);
        edit(state.getDialog());
    }

    /**
     * Sets the current executing state.
     *
     * @param state the state
     */
    private void setExecuting(State state) {
        executing = true;
        current = state;
    }

    /**
     * Queues a state.
     *
     * @param state the state
     */
    private void queue(State state) {
        queue.addLast(state);
        if (!executing) {
            next();
        }
    }

    /**
     * Returns a title for the edit dialog.
     *
     * @param editor the editor
     * @return a title for the edit dialog
     */
    private String getTitle(IMObjectEditor editor) {
        String title = editor.getTitle();
        if (editor instanceof PatientMedicationActEditor) {
            PatientMedicationActEditor meditor = (PatientMedicationActEditor) editor;
            Party patient = meditor.getPatient();
            if (patient != null) {
                PatientRules rules = ServiceHelper.getBean(PatientRules.class);
                String name = patient.getName();
                String weight = rules.getPatientWeight(patient);
                if (weight == null) {
                    weight = Messages.get("patient.noweight");
                }
                title = Messages.format("patient.medication.dialog.title", title, name, weight);
            }
        }
        return title;
    }

    private class EditorState extends State {

        final IMObjectEditor editor;

        final Listener listener;

        final boolean skip;

        final boolean cancel;

        EditorState(IMObject owner, IMObjectEditor editor, Listener listener, boolean skip, boolean cancel) {
            super(owner);
            this.editor = editor;
            this.listener = listener;
            this.skip = skip;
            this.cancel = cancel;
        }

        @Override
        public void show() {
            EditActions actions = EditActions.ok().setSave(false).setShowCancel(cancel).setShowSkip(skip);
            EditDialog dialog;
            EditDialogFactory factory = ServiceHelper.getBean(EditDialogFactory.class);
            if (factory.supportsEditActions(editor)) {
                dialog = factory.create(editor, actions, context);
            } else {
                dialog = new EditDialog(editor, actions, context);
            }
            dialog.setTitle(getTitle(editor));
            dialog.setStyleName("ChildEditDialog");
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    doNext(false, false);
                }

                @Override
                public void onCancel() {
                    doNext(false, true);
                }

                @Override
                public void onSkip() {
                    doNext(true, false);
                }

                private void doNext(boolean skipped, boolean cancelled) {
                    executing = false;
                    current = null;
                    if (listener != null) {
                        listener.completed(skipped, cancelled);
                    }
                    if (skipped) {
                        skipped();
                    } else if (cancelled) {
                        cancelled();
                    } else {
                        next();
                    }
                }
            });
            setDialog(dialog);
            edit(this);
        }

        public void skip() {
            listener.completed(true, false);
        }

        @Override
        EditDialog getDialog() {
            return (EditDialog) super.getDialog();
        }

        boolean hasEditor(IMObjectEditor editor) {
            return this.editor.equals(editor);
        }

        /**
         * Determines if the state can be cancelled by the specified object.
         *
         * @param object the object
         * @return {@code true} if the object is the owner of the state, or the object is currently being edited
         */
        @Override
        boolean isCancelledBy(IMObject object) {
            return super.isCancelledBy(object) || editor.getObject().equals(object);
        }
    }

    private class DialogState extends State {

        private final Runnable callback;

        private final WindowPaneListener listener;

        DialogState(IMObject owner, PopupDialog dialog, Runnable callback) {
            super(owner);
            setDialog(dialog);
            this.callback = callback;
            listener = e -> onClose();
        }

        @Override
        public void show() {
            getDialog().addWindowPaneListener(listener);
            DefaultEditorQueue.this.show(this);
        }

        private void onClose() {
            getDialog().removeWindowPaneListener(listener);
            executing = false;
            if (callback != null) {
                callback.run();
            }
            next();
        }
    }

    private class RunnableState extends State {

        private final Runnable runnable;

        RunnableState(Runnable runnable) {
            super(null);
            this.runnable = runnable;
        }

        @Override
        public void show() {
            execute(runnable);
        }
    }

    private class ConsumerState extends State {

        private final Consumer<Runnable> consumer;

        /**
         * Used to prevent multiple calls to {@link #next()}.
         */
        private boolean next;

        ConsumerState(Consumer<Runnable> consumer) {
            super(null);
            this.consumer = consumer;
        }

        @Override
        public void show() {
            try {
                executing = true;
                consumer.accept(() -> {
                    if (!next) {
                        next = true;
                        next();
                    }
                });
            } catch (Throwable exception) {
                ErrorHelper.show(exception, (WindowPaneListener) windowPaneEvent -> next());
            }
        }
    }

    private static abstract class State {

        private final IMObject owner;

        private PopupDialog dialog;

        /**
         * Constructs a {@link State}.
         *
         * @param owner the owning object. May be {@code null}
         */
        State(IMObject owner) {
            this.owner = owner;
        }

        abstract void show();

        PopupDialog getDialog() {
            return dialog;
        }

        void setDialog(PopupDialog dialog) {
            this.dialog = dialog;
        }

        /**
         * Determines if the state can be cancelled by the specified object.
         *
         * @param object the object
         * @return {@code true} if the object is the owner of the state
         */
        boolean isCancelledBy(IMObject object) {
            return owner != null && Objects.equals(owner, object);
        }

        void cancel() {
            if (dialog != null && dialog.getParent() != null) {
                // remove the dialog rather than close it, to avoid WindowPaneListeners being invoked
                dialog.getParent().remove(dialog);
                dialog = null;
            }
        }
    }
}
