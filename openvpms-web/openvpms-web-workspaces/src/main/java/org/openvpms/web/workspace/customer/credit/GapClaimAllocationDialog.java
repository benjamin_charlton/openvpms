/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.credit;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.patient.insurance.claim.GapClaimSummary;

/**
 * Dialog to display allocation to a gap claim.
 * <p/>
 * Prompts for a till if required.
 *
 * @author Tim Anderson
 */
public class GapClaimAllocationDialog extends ModalDialog {

    /**
     * The allocation.
     */
    private final GapClaimAllocation allocation;

    /**
     * The message.
     */
    private final String message;

    /**
     * The editor to select the till, if the allocation would result in a benefit payment
     * being recorded.
     */
    private GapPaymentTillReferenceEditor tillEditor;

    /**
     * Constructs a {@link GapClaimAllocationDialog}.
     *
     * @param allocation the claim allocation
     * @param help       the help context
     */
    public GapClaimAllocationDialog(GapClaimAllocation allocation, HelpContext help) {
        super(Messages.get("patient.insurance.pay.title"), "MediumWidthHeightDialog", OK_CANCEL, help);

        this.allocation = allocation;
        message = getMessage(allocation);
        Label content = LabelFactory.create(true, true);
        content.setText(message);

        if (allocation.getTill() == null && allocation.isTillRequired()) {
            Party location = (Party) allocation.getClaim().getLocationParty();
            LayoutContext context = new DefaultLayoutContext(new LocalContext(), help);
            tillEditor = new GapPaymentTillReferenceEditor(location, context);
            tillEditor.addModifiableListener(modifiable -> allocation.setTill(tillEditor.getObject()));
        }

        Component summary = new AllocationSummary(allocation).getComponent();
        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, summary, content);
        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
    }

    /**
     * Returns the gap claim allocation.
     *
     * @return the gap claim allocation
     */
    public GapClaimAllocation getAllocation() {
        return allocation;
    }

    /**
     * Returns the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Invoked when the 'OK' button is pressed. This sets the action and closes
     * the window.
     */
    @Override
    protected void onOK() {
        if (tillEditor != null) {
            DefaultValidator validator = new DefaultValidator();
            if (!tillEditor.validate(validator)) {
                ValidationHelper.showError(validator);
            } else {
                super.onOK();
            }
        } else {
            super.onOK();
        }
    }

    /**
     * Returns a message describing the allocation.
     *
     * @param allocation the allocation
     * @return the message
     */
    protected String getMessage(GapClaimAllocation allocation) {
        StringBuilder result = new StringBuilder();
        GapClaimAllocation.Status status = allocation.getStatus();
        if (allocation.benefitPending()) {
            result.append(Messages.format("customer.credit.allocate.gap.nobenefit"));
            if (status == GapClaimAllocation.Status.NO_BENEFIT_PARTIAL_PAYMENT) {
                result.append(Messages.get("customer.credit.allocate.gap.nobenefit.partial"));
            } else {
                result.append(Messages.get("customer.credit.allocate.gap.nobenefit.full"));
            }
        } else {
            if (status == GapClaimAllocation.Status.ALLOCATION_LESS_THAN_GAP) {
                result.append(Messages.format("customer.credit.allocate.gap.less",
                                              allocation.getGapAmount().subtract(allocation.getAllocation())));
            } else if (status == GapClaimAllocation.Status.ALLOCATION_EQUAL_TO_GAP) {
                result.append(Messages.get("customer.credit.allocate.gap.equal"));
            } else if (status == GapClaimAllocation.Status.ALLOCATION_GREATER_THAN_GAP) {
                // paying more than the gap amount, but less than the claim total
                result.append(Messages.format("customer.credit.allocate.gap.partial",
                                              allocation.getTotal().subtract(allocation.getAllocation())));
            } else {
                // paying the claim
                result.append(Messages.get("customer.credit.allocate.gap.full"));
            }
        }
        return result.toString();
    }

    /**
     * Sets the till.
     *
     * @param till the till
     */
    void setTill(Entity till) {
        tillEditor.setObject((Party) till);
    }

    /**
     * Displays the allocation summary, and prompts for a till if required.
     */
    private class AllocationSummary extends GapClaimSummary {

        AllocationSummary(GapClaimAllocation allocation) {
            super(allocation.getClaim(), allocation.getExistingAllocation());
        }

        @Override
        protected void doLayout(GapClaimImpl claim, ComponentGrid grid) {
            super.doLayout(claim, grid);
            grid.add(LabelFactory.create("customer.credit.allocate.gap.currentpayment"),
                     createAmount(allocation.getNewAllocation()));
            if (tillEditor != null) {
                grid.add(new ComponentState(tillEditor));
            }
        }
    }
}
