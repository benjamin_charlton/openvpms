/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import org.openvpms.component.business.dao.im.Page;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.AbstractCachingResultSet;
import org.openvpms.web.component.im.query.ResultSet;

import java.util.List;

/**
 * Abstract implementation of {@link ResultSet} for mappings.
 *
 * @author Tim Anderson
 */
abstract class AbstractMappingResultSet<T> extends AbstractCachingResultSet<T> {

    /**
     * The value to filter on.
     */
    private final String value;

    /**
     * Constructs an {@link AbstractMappingResultSet}.
     *
     * @param value    the name to filter on. May be {@code null}
     * @param pageSize the maximum no. of results per page
     */
    public AbstractMappingResultSet(String value, int pageSize) {
        super(pageSize);
        this.value = value;
    }

    /**
     * Sorts the set. This resets the iterator.
     *
     * @param sort the sort criteria. May be {@code null}
     */
    @Override
    public void sort(SortConstraint[] sort) {
        reset();
    }

    /**
     * Determines if the node is sorted ascending or descending.
     *
     * @return {@code true} if the node is sorted ascending or no sort constraint was specified; {@code false} if it is
     * sorted descending
     */
    @Override
    public boolean isSortedAscending() {
        return true;
    }

    /**
     * Returns the sort criteria.
     *
     * @return the sort criteria. Never null
     */
    @Override
    public SortConstraint[] getSortConstraints() {
        return new SortConstraint[0];
    }

    /**
     * Determines if duplicate results should be filtered.
     *
     * @param distinct if true, remove duplicate results
     */
    @Override
    public void setDistinct(boolean distinct) {

    }

    /**
     * Determines if duplicate results should be filtered.
     *
     * @return {@code true} if duplicate results should be removed otherwise {@code false}
     */
    @Override
    public boolean isDistinct() {
        return true;
    }

    /**
     * Performs a query.
     *
     * @param firstResult the first result of the page to retrieve
     * @param maxResults  the maximum no. of results in the page
     * @return the page
     */
    @Override
    protected IPage<T> query(int firstResult, int maxResults) {
        List<T> matches = getMatches(value, firstResult, maxResults);
        return new Page<>(matches, firstResult, maxResults, -1);
    }

    /**
     * Returns the results matching the specified value.
     *
     * @param value       the value. May be {@code null}
     * @param firstResult the first result of the page to retrieve
     * @param maxResults  the maximum no. of results to retrieve
     * @return the matches
     */
    protected abstract List<T> getMatches(String value, int firstResult, int maxResults);

    /**
     * Counts the no. of results matching the query criteria.
     *
     * @return the total number of results
     */
    @Override
    protected int countResults() {
        return countResults(value);
    }

    /**
     * Counts the no. of results matching the query criteria.
     *
     * @param value the value to filter on. May be {@code null}
     * @return the total number of results
     */
    protected abstract int countResults(String value);

}
