/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.report.ParameterType;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.doc.ParameterDialog;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.report.DocumentActReporter;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.component.print.BatchPrinter;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;


/**
 * Helper to generate letters and print customer charge documents that are set for immediate printing.
 * <p/>
 * In the case of letters, these need to be generated after save to ensure that objects are accessible
 * (e.g. batch numbers associated with a charge).
 *
 * @author Tim Anderson
 */
public class ChargeDocumentManager {

    /**
     * The charge editor.
     */
    private final CustomerChargeActEditor editor;

    /**
     * The layout context.
     */
    private final LayoutContext layoutContext;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The file name formatter.
     */
    private final FileNameFormatter formatter;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The lookup service.
     */
    private final ILookupService lookups;

    /**
     * The references of ungenerated documents to exclude.
     */
    private final Set<Reference> excludeUngenerated = new HashSet<>();

    /**
     * The references of unprinted documents to exclude.
     */
    private Set<Reference> excludeUnprinted = new HashSet<>();

    /**
     * Constructs a {@link ChargeDocumentManager}.
     *
     * @param editor        the charge editor
     * @param layoutContext the layout context
     */
    public ChargeDocumentManager(CustomerChargeActEditor editor, LayoutContext layoutContext) {
        this.editor = editor;
        this.layoutContext = layoutContext;
        service = ServiceHelper.getBean(IArchetypeService.class);
        formatter = ServiceHelper.getBean(FileNameFormatter.class);
        rules = new DocumentRules(service);
        lookups = ServiceHelper.getLookupService();

        // exclude any existing unprinted or ungenerated documents
        addReferences(getUngeneratedLetters(), excludeUngenerated);
        addReferences(getUnprintedDocuments(), excludeUnprinted);
    }

    /**
     * Processes any ungenerated letters and unprinted documents.
     * <p/>
     * This is an asynchronous operation.
     *
     * @param queue the queue
     */
    public void process(EditorQueue queue) {
        generateLetters(queue);
        queue.queue(completed -> {
            printNew(queue);
            completed.run();
        });
    }

    /**
     * Generates any letters.
     *
     * @param queue the editor queue
     */
    private void generateLetters(EditorQueue queue) {
        Map<DocumentAct, FinancialAct> documents = getUngeneratedLetters();
        if (!documents.isEmpty()) {
            addReferences(documents, excludeUngenerated); // exclude from subsequent generation

            for (Map.Entry<DocumentAct, FinancialAct> entry : documents.entrySet()) {
                DocumentAct document = entry.getKey();
                FinancialAct invoiceItem = entry.getValue();
                DocumentTemplate template = getTemplate(document);
                if (template != null) {
                    addLetter(invoiceItem, document, template, queue);
                }
            }
        }
    }

    private void addLetter(IMObject invoiceItem, DocumentAct document, DocumentTemplate template, EditorQueue queue) {
        Context context = layoutContext.getContext();
        HelpContext help = layoutContext.getHelpContext();
        DocumentActReporter reporter = new DocumentActReporter(document, template, formatter, service, lookups);
        reporter.setFields(ReportContextFactory.create(context));
        Set<ParameterType> parameters = reporter.getParameterTypes();
        if (!parameters.isEmpty()) {
            String title = Messages.format("document.input.parameters", template.getName());
            MacroVariables variables = new MacroVariables(context, service, lookups);
            ParameterDialog dialog = new ParameterDialog(title, parameters, document, context,
                                                         help.subtopic("document"), variables, true, true);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    reporter.setParameters(dialog.getValues());
                    addLetter(document, reporter);
                    super.onOK();
                }
            });
            queue.queue(invoiceItem, dialog);
        } else {
            addLetter(document, reporter);
        }
    }

    /**
     * Prints new documents.
     *
     * @param queue the queue
     */
    private void printNew(EditorQueue queue) {
        Map<DocumentAct, FinancialAct> documents = getUnprintedDocuments();
        if (!documents.isEmpty()) {
            queue.queue(completed -> print(documents, completed));
        }
    }

    /**
     * Prints documents.
     *
     * @param documents the documents to print
     * @param listener  the listener to notify on completion
     */
    private void print(Map<DocumentAct, FinancialAct> documents, Runnable listener) {
        LocalContext context = new LocalContext();
        context.setCustomer(editor.getCustomer());
        context.setLocation(editor.getLocation());
        addReferences(documents, excludeUnprinted); // exclude these documents from subsequent prints
        BatchPrinter printer = new BatchPrinter<DocumentAct>(new ArrayList<>(documents.keySet()), context,
                                                             editor.getHelpContext()) {

            public void failed(Throwable cause) {
                ErrorHelper.show(cause, new WindowPaneListener() {
                    public void onClose(WindowPaneEvent event) {
                        print(); // print the next document
                    }
                });
            }

            /**
             * Invoked when printing completes. Closes the edit dialog if required.
             */
            @Override
            protected void completed() {
                listener.run();
            }
        };
        printer.print();
    }

    /**
     * Returns any ungenerated patient letter documents that haven't been processed before.
     *
     * @return the list of ungenerated documents, and their associated invoice items
     */
    private Map<DocumentAct, FinancialAct> getUngeneratedLetters() {
        return getDocuments(this::isUngeneratedLetter, excludeUngenerated);
    }

    /**
     * Returns any unprinted documents that are flagged for immediate printing.
     *
     * @return the list of unprinted documents, and their associated invoice items
     */
    private Map<DocumentAct, FinancialAct> getUnprintedDocuments() {
        return getDocuments(this::isPrintImmediate, excludeUnprinted);
    }

    /**
     * Determines if a document is a patient letter that hasn't been generated yet.
     *
     * @param document the document
     * @return {@code true} if the document is a letter that hasn't been generated
     */
    private boolean isUngeneratedLetter(DocumentAct document) {
        return document.isA(PatientArchetypes.DOCUMENT_LETTER) && document.getDocument() == null;
    }

    /**
     * Returns all of those documents matching a predicate, and not in the set of excluded documents.
     *
     * @param predicate the predicate
     * @param exclude   the excluded document references
     * @return the matching documents, and their corresponding invoice items
     */
    private Map<DocumentAct, FinancialAct> getDocuments(Predicate<DocumentAct> predicate, Set<Reference> exclude) {
        Map<DocumentAct, FinancialAct> result = new LinkedHashMap<>();
        ChargeItemRelationshipCollectionEditor items = editor.getItems();
        for (Act item : items.getActs()) {
            CustomerChargeActItemEditor itemEditor = items.getEditor(item);
            for (Act act : itemEditor.getDocuments()) {
                DocumentAct document = (DocumentAct) act;
                if (!exclude.contains(document.getObjectReference()) && predicate.test(document)) {
                    result.put(document, itemEditor.getObject());
                }
            }
        }
        return result;
    }

    /**
     * Adds references to the specified set.
     *
     * @param documents the documents to exclude
     */
    private void addReferences(Map<DocumentAct, FinancialAct> documents, Set<Reference> set) {
        for (DocumentAct document : documents.keySet()) {
            set.add(document.getObjectReference());
        }
    }

    /**
     * Determines if a document should be printed immediately.
     *
     * @param document the document
     * @return {@code true} if the document should be printed immediately
     */
    private boolean isPrintImmediate(DocumentAct document) {
        boolean result = false;
        if (!document.isPrinted()) {
            DocumentTemplate template = getTemplate(document);
            if (template != null) {
                result = (template.getPrintMode() == DocumentTemplate.PrintMode.IMMEDIATE);
            }
        }
        return result;
    }

    /**
     * Returns the template associated with a document.
     *
     * @param document the document
     * @return the template, or {@code null} if none is found
     */
    private DocumentTemplate getTemplate(DocumentAct document) {
        DocumentTemplate template = null;
        IMObjectBean bean = service.getBean(document);
        if (bean.hasNode("documentTemplate")) {
            Entity entity = (Entity) layoutContext.getCache().get(bean.getTargetRef("documentTemplate"));
            if (entity != null) {
                template = new DocumentTemplate(entity, service);
            }
        }
        return template;
    }

    /**
     * Adds a patient letter.
     *
     * @param act      the document act
     * @param reporter the reporter, used to generate the letter
     */
    private void addLetter(DocumentAct act, DocumentActReporter reporter) {
        Document document = reporter.getDocument();
        List<IMObject> changes = rules.addDocument(act, document, false);
        service.save(changes);
    }

}
