/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.vetcheck;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import nextapp.echo2.webcontainer.command.BrowserOpenWindowCommand;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

/**
 * VetCheck summary for a patient.
 * <p/>
 * This displays a button to launch VetCheck with  patient and customer details supplied, and lists
 * the most recent three links for the patient.
 *
 * @author Tim Anderson
 */
public class VetCheckSummary {

    /**
     * The patient.
     */
    private final Party patient;

    /**
     * The rules.
     */
    private final VetCheckRules rules;

    /**
     * The container.
     */
    private final Column container;

    /**
     * The button to launch VetCheck.
     */
    private final Button button;

    /**
     * Constructs a {@link VetCheckSummary}.
     *
     * @param patient the patient
     * @param context the context
     * @param help    the help context
     * @param rules   the rules
     */
    private VetCheckSummary(Party patient, Context context, HelpContext help, VetCheckRules rules) {
        this.patient = patient;
        this.rules = rules;
        container = ColumnFactory.create();
        button = ButtonFactory.create(null, "button.vetcheck", new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                VetCheckDialog dialog = new VetCheckDialog(patient, context, help);
                dialog.show();
                dialog.addWindowPaneListener(new WindowPaneListener() {
                    @Override
                    public void onClose(WindowPaneEvent event) {
                        refresh();
                    }
                });
            }
        });
        button.setLayoutData(ColumnFactory.layout(Alignment.ALIGN_CENTER));
        refresh();
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        return container;
    }

    /**
     * Returns the VetCheck summary for a patient.
     *
     * @param patient the patient
     * @param context the context
     * @param help    the help context
     * @return the VetCheck summary, or {@code null} if VetCheck is not enabled
     */
    public static Component getSummary(Party patient, Context context, HelpContext help) {
        Component result = null;
        VetCheckRules rules = ServiceHelper.getBean(VetCheckRules.class);
        if (rules.isVetCheckEnabled()) {
            VetCheckSummary summary = new VetCheckSummary(patient, context, help, rules);
            result = summary.getComponent();
        }
        return result;
    }

    /**
     * Refreshes the display.
     */
    private void refresh() {
        container.removeAll();
        container.add(button);
        for (Act act : rules.getMostRecentLinks(patient, 3)) {
            IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(act);
            String url = bean.getString("url");
            String description = bean.getString("description");
            if (url != null && description != null) {
                Button link = ButtonFactory.text(description, new ActionListener() {
                    @Override
                    public void onAction(ActionEvent event) {
                        launch(url);
                    }
                });
                link.setStyleName("hyperlink");
                container.add(link);
            }
        }
    }

    /**
     * Opens a window to display a URL.
     *
     * @param url the URL
     */
    private void launch(String url) {
        ApplicationInstance.getActive().enqueueCommand(new BrowserOpenWindowCommand(url, "", ""));
    }
}