/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.roster;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.workspace.admin.calendar.CalendarEventEditor;

import java.util.Date;

/**
 * Editor for roster events.
 * <p/>
 * This requires the location to be set.
 *
 * @author Tim Anderson
 */
public class RosterEventEditor extends CalendarEventEditor {

    /**
     * The duration, in minutes, used to calculate the end time when the start time changes.
     */
    private int duration;

    /**
     * Constructs a {@link RosterEventEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public RosterEventEditor(Act act, IMObject parent, LayoutContext context) {
        super(act, parent, false, context);
        calculateDuration();
        if (act.isNew()) {
            if (getStartTime() == null) {
                setStartTime(DateRules.getDate(DateRules.getToday(), 9, DateUnits.HOURS));
            }
            initParticipant("location", context.getContext().getLocation());
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new RosterEventEditor(reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Sets the user.
     *
     * @param user the user. May be {@code null}
     */
    public void setUser(User user) {
        setParticipant("user", user);
    }

    /**
     * Returns the user.
     *
     * @return the user. May be {@code null}
     */
    public User getUser() {
        return (User) getParticipant("user");
    }

    /**
     * Returns the slot size for the specified schedule.
     *
     * @param schedule the schedule. May be {@code null}
     * @return the slot size, in minutes
     */
    @Override
    protected int getSlotSize(Entity schedule) {
        return 1;
    }

    /**
     * Invoked when any of the child editors or properties update.
     * <p>
     * This resets the cached valid state
     *
     * @param modifiable the updated object
     */
    @Override
    protected void onModified(Modifiable modifiable) {
        super.onModified(modifiable);
        updateSynchronisation();
    }

    /**
     * Invoked when layout has completed. All editors have been created.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        Party location = (Party) getParticipant("location");
        AreaParticipationEditor area = getAreaEditor();
        area.setLocation(location);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new RosterLayoutStrategy();
    }

    /**
     * Calculates the default start time of an event, using the supplied date.
     *
     * @param date the start date
     * @return the start time
     */
    @Override
    protected Date getDefaultStartTime(Date date) {
        return DateRules.getDate(date, 8, DateUnits.HOURS);
    }

    /**
     * Calculates the end time from the start time.
     * If the start time >= end time, this calculates the new end time as start time + duration, else leaves
     * the end time unchanged.
     */
    @Override
    protected void calculateEndTime() {
        Date start = getStartTime();
        if (start != null) {
            Date end = getEndTime();
            if (end == null || DateRules.compareTo(start, end) >= 0) {
                end = DateRules.getDate(start, duration, DateUnits.MINUTES);
                setEndTime(end);
            } else {
                calculateDuration(); // update the duration
            }
        }
    }

    /**
     * Invoked when the end time changes. Recalculates the end time if it is less than the start time.
     */
    @Override
    protected void onEndTimeChanged() {
        super.onEndTimeChanged();
        calculateDuration();
    }

    /**
     * Calculates the event duration, in minutes.
     */
    private void calculateDuration() {
        Date startTime = getStartTime();
        Date endTime = getEndTime();
        if (startTime != null && endTime != null) {
            duration = DateRules.minutesBetween(startTime, endTime);
        } else {
            duration = 8 * 60; // 8 hours
        }
    }

    /**
     * Updates any synchronisation identity so that synchronisers can propagate changes.
     */
    private void updateSynchronisation() {
        CollectionProperty synchronisation = getCollectionProperty("synchronisation");
        if (synchronisation != null) {
            for (Object value : synchronisation.getValues()) {
                IMObjectBean bean = getBean((IMObject) value);
                if (bean.hasNode("status")) {
                    bean.setValue("status", WorkflowStatus.PENDING);
                }
            }
        }
    }

    /**
     * Returns the area participation editor.
     *
     * @return the area participation editor
     */
    private AreaParticipationEditor getAreaEditor() {
        return (AreaParticipationEditor) getParticipationEditor("schedule", false);
    }

    /**
     * Layout strategy that excludes the error node if it is empty.
     */
    private class RosterLayoutStrategy extends LayoutStrategy {

        public RosterLayoutStrategy() {
            getArchetypeNodes().simple("error").excludeIfEmpty("error");
        }
    }
}
