/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Renders a button to check for and launch external results in a new window.
 *
 * @author Tim Anderson
 */
public class CheckResultsViewer {

    /**
     * The result checker.
     */
    private final ResultChecker checker;

    /**
     * The component container.
     */
    private final Column container = new Column();

    /**
     * Constructs a {@link CheckResultsViewer}.
     *
     * @param checker the result checker
     */
    public CheckResultsViewer(ResultChecker checker) {
        this.checker = checker;
        Button button = ButtonFactory.create(null, "checkresults", new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                checkResults();
            }
        });
        button.setToolTipText(Messages.get("investigation.checkResults"));
        container.add(button);
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        return container;
    }


    /**
     * Checks for results. If there are results available, this launches a viewer.
     */
    protected void checkResults() {
        if (checker.checkResults()) {
            // replace the button with the external results viewer and launch the viewer
            container.removeAll();
            ExternalResultsViewer viewer = new ExternalResultsViewer(checker.getInvestigation());
            Component component = viewer.getComponent();
            container.add(component);
            viewer.view(checker.getOrder());
        }
    }

}
