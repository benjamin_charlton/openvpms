/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.style;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.list.ListCellRenderer;
import org.openvpms.web.echo.style.Theme;
import org.openvpms.web.echo.style.Themes;

/**
 * Renders theme names given their id.
 *
 * @author Tim Anderson
 */
class ThemeRenderer implements ListCellRenderer {

    /**
     * The themes.
     */
    private final Themes themes;

    /**
     * Constructs a {@link ThemeRenderer}.
     *
     * @param themes the available themes
     */
    public ThemeRenderer(Themes themes) {
        this.themes = themes;
    }

    /**
     * Renders an item in a list.
     *
     * @param list  the list component
     * @param value the item value. May be {@code null}
     * @param index the item index
     * @return the rendered form of the list cell
     */
    @Override
    public Object getListCellRendererComponent(Component list, Object value, int index) {
        Theme theme = themes.getTheme((String) value);
        return (theme != null) ? theme.getName() : null;
    }
}
