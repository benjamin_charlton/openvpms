/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.mail;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.practice.MailServer;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.IMObjectTabPane;
import org.openvpms.web.component.im.layout.IMObjectTabPaneModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetImpl;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

import static org.openvpms.web.workspace.admin.organisation.mail.MailSettingsEditor.AUTHENTICATION_METHOD;
import static org.openvpms.web.workspace.admin.organisation.mail.MailSettingsEditor.OAUTH2_CLIENT_REGISTRATION;
import static org.openvpms.web.workspace.admin.organisation.mail.MailSettingsEditor.PASSWORD;
import static org.openvpms.web.workspace.admin.organisation.mail.MailSettingsEditor.SECURITY;
import static org.openvpms.web.workspace.admin.organisation.mail.MailSettingsEditor.USERNAME;

/**
 * Layout strategy for mail settings.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMailSettingsLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The tab pane.
     */
    private IMObjectTabPane tabs;

    /**
     * Constructs an {@link AbstractMailSettingsLayoutStrategy}.
     */
    public AbstractMailSettingsLayoutStrategy() {
        this(false);
    }

    /**
     * Constructs an {@link AbstractMailSettingsLayoutStrategy}.
     *
     * @param keepState if {@code true} keep layout state. Use this if the same strategy will be used to layout a
     *                  component multiple times
     */
    public AbstractMailSettingsLayoutStrategy(boolean keepState) {
        super(new ArchetypeNodes().exclude(SECURITY, AUTHENTICATION_METHOD, USERNAME, PASSWORD,
                                           OAUTH2_CLIENT_REGISTRATION), keepState);
    }

    /**
     * Adds a tab containing the security settings.
     *
     * @param object     the settings
     * @param properties the object's properties
     * @param context    the layout context
     */
    protected void addSecuritySettings(IMObject object, PropertySet properties, LayoutContext context) {
        if (tabs != null) {
            IMObjectTabPaneModel model = tabs.getModel();
            addSecuritySettings(model, object, properties, context);
        }
    }

    /**
     * Adds a tab containing the security settings.
     *
     * @param model      the tab pane model
     * @param object     the object to lay out
     * @param properties the object's properties
     * @param context    the layout context
     */
    protected void addSecuritySettings(IMObjectTabPaneModel model, IMObject object, PropertySet properties,
                                       LayoutContext context) {
        model.addTab(Messages.get("admin.mail.securitySettings"),
                     ColumnFactory.create(Styles.INSET, getSecuritySettings(object, properties, context)));
    }

    /**
     * Lay out the object in the specified container.
     *
     * @param object     the object to lay out
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doLayout(IMObject object, PropertySet properties, IMObject parent, Component container,
                            LayoutContext context) {
        super.doLayout(object, properties, parent, container, context);
        addSecuritySettings(tabs.getModel(), object, properties, context);
    }

    /**
     * Lays out each child component in a tabbed pane.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doComplexLayout(IMObject object, IMObject parent, List<Property> properties, Component container,
                                   LayoutContext context) {
        IMObjectTabPaneModel model = doTabLayout(object, properties, container, context, false);
        tabs = new IMObjectTabPane(model);
        container.add(tabs);
    }

    /**
     * Creates a component displaying the security settings.
     *
     * @param object     the settings
     * @param properties the properties
     * @param context    the layout context
     * @return the security settings
     */
    protected Component getSecuritySettings(IMObject object, PropertySet properties, LayoutContext context) {
        PropertySet children;
        List<Property> list = new ArrayList<>();
        list.add(properties.get(SECURITY));
        Property authenticationMethod = properties.get(AUTHENTICATION_METHOD);
        list.add(authenticationMethod);
        Property username = properties.get(USERNAME);
        if (MailServer.AuthenticationMethod.OAUTH2.toString().equals(authenticationMethod.getString())) {
            ComponentState auth = createComponentPair(AUTHENTICATION_METHOD, OAUTH2_CLIENT_REGISTRATION, object,
                                                      properties, context);
            addComponent(auth);
            list.add(username);
            MutablePropertySet set = new MutablePropertySet(new PropertySetImpl(list));
            set.setReadOnly(USERNAME);
            children = set;
        } else {
            list.add(username);
            list.add(properties.get(PASSWORD));
            children = new PropertySetImpl(list);
        }
        ComponentGrid grid = createGrid(object, new ArrayList<>(children.getProperties()), context);
        return grid.createGrid();
    }
}
