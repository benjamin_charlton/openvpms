/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.scheduledreport;

import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.im.edit.DefaultEditableComponentFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.AbstractModifiable;
import org.openvpms.web.component.property.ErrorListener;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for {@link Parameters}.
 *
 * @author Tim Anderson
 */
class ParametersEditor extends AbstractModifiable {

    /**
     * The parameters.
     */
    private final Parameters parameters;

    /**
     * The parameter components.
     */
    private final ParameterComponents components;

    /**
     * Monitors the parameter editors.
     */
    private Editors editors = new Editors();

    /**
     * Constructs a {@link ParametersEditor}.
     *
     * @param parameters the parameters
     * @param context    the context
     */
    public ParametersEditor(Parameters parameters, LayoutContext context) {
        this.parameters = parameters;
        // use a localised component factory, to avoid registering editors on the parent
        // ScheduledReportJobConfigrationEditor
        context = new DefaultLayoutContext(context);
        context.setComponentFactory(new DefaultEditableComponentFactory(context));
        components = new ParameterComponents(parameters, context);
        for (Editor editor : components.getEditors()) {
            editors.add(editor);
        }
    }

    /**
     * Returns the parameters.
     *
     * @return the parameters
     */
    public ParameterComponents getParameters() {
        return components;
    }

    /**
     * Determines if the object has been modified.
     *
     * @return {@code true} if the object has been modified
     */
    @Override
    public boolean isModified() {
        return editors.isModified();
    }

    /**
     * Clears the modified status of the object.
     */
    @Override
    public void clearModified() {
        editors.clearModified();
    }

    /**
     * Adds a listener to be notified when this changes.
     * <p/>
     * Listeners will be notified in the order they were registered.
     * <p/>
     * Duplicate additions are ignored.
     *
     * @param listener the listener to add
     */
    @Override
    public void addModifiableListener(ModifiableListener listener) {
        editors.addModifiableListener(listener);
    }

    /**
     * Adds a listener to be notified when this changes, specifying the order of the listener.
     *
     * @param listener the listener to add
     * @param index    the index to add the listener at. The 0-index listener is notified first
     */
    @Override
    public void addModifiableListener(ModifiableListener listener, int index) {
        editors.addModifiableListener(listener, index);
    }

    /**
     * Removes a listener.
     *
     * @param listener the listener to remove
     */
    @Override
    public void removeModifiableListener(ModifiableListener listener) {
        editors.removeModifiableListener(listener);
    }

    /**
     * Sets a listener to be notified of errors.
     *
     * @param listener the listener to register. May be {@code null}
     */
    @Override
    public void setErrorListener(ErrorListener listener) {
        editors.setErrorListener(listener);
    }

    /**
     * Returns the listener to be notified of errors.
     *
     * @return the listener. May be {@code null}
     */
    @Override
    public ErrorListener getErrorListener() {
        return editors.getErrorListener();
    }

    /**
     * Disposes of the editors.
     */
    public void dispose() {
        editors.dispose();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return editors.validate(validator) && validateParameters(validator);
    }

    /**
     * Validates that the report doesn't have too many parameters.
     *
     * @param validator the validator
     * @return {@code true} if the report doesn't have too many parameters, otherwise {@code false}
     */
    private boolean validateParameters(Validator validator) {
        boolean result = false;
        int count = parameters.getParameterCount();
        int maxParameters = parameters.getMaxParameters();
        if (count <= maxParameters) {
            result = true;
        } else {
            String message = Messages.format("scheduledreport.toomanyparameters", count, maxParameters);
            validator.add(this, new ValidatorError(message));
        }
        return result;
    }

}
