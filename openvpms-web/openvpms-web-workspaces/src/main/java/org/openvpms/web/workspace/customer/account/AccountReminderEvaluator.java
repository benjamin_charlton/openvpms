/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.resource.i18n.Messages;

import java.math.BigDecimal;

/**
 * Expression evaluator for SMS account reminders.
 *
 * @author Tim Anderson
 */
public class AccountReminderEvaluator {

    /**
     * The practice.
     */
    private final Party practice;

    /**
     * The template evaluator.
     */
    private final SMSTemplateEvaluator evaluator;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;


    /**
     * Constructs an {@link AccountReminderEvaluator}.
     *
     * @param practice  the practice
     * @param evaluator the evaluator
     * @param service   the service
     * @param lookups   the lookup service
     */
    public AccountReminderEvaluator(Party practice, SMSTemplateEvaluator evaluator, ArchetypeService service,
                                    LookupService lookups) {
        this.practice = practice;
        this.evaluator = evaluator;
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Evaluates an SMS appointment reminder template against an appointment.
     * <p/>
     * The customer, patient and appointment are available as variables.
     *
     * @param template the template
     * @param charge   the charge
     * @param customer the customer
     * @param location the practice location
     * @return the result of the expression
     * @throws AccountReminderException if the expression cannot be evaluated
     */
    public String evaluate(Entity template, FinancialAct charge, Party customer, Party location) {
        String result;
        Context context = new LocalContext();
        context.setCustomer(customer);
        context.setLocation(location);
        context.setPractice(practice);
        MacroVariables variables = new MacroVariables(context, service, lookups);

        BigDecimal balance = MathRules.round(charge.getTotal().subtract(charge.getAllocatedAmount()));
        variables.add("balance", balance);
        variables.add("charge", charge);

        try {
            result = evaluator.evaluate(template, charge, variables);
        } catch (Throwable exception) {
            throw new AccountReminderException(Messages.format("reporting.reminder.smsevaluatefailed",
                                                               template.getName()), exception);
        }
        return result;
    }
}