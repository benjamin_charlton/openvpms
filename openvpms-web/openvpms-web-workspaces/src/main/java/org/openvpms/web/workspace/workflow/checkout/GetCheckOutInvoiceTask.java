/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.workspace.workflow.consult.GetConsultInvoiceTask;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Task to return an invoice for the customer.
 * <p/>
 * This will choose:
 * <ul>
 * <li>the invoice associated with the current event, if any; else</li>
 * <li>the invoice associated with any other events being checked out, if any; else</li>
 * <li>an IN_PROGRESS or COMPLETED invoice for the customer</li>
 * </ul>
 */
public class GetCheckOutInvoiceTask extends GetConsultInvoiceTask {

    /**
     * The visits being checked out.
     */
    private final Visits visits;

    /**
     * Constructs a {@link GetCheckOutInvoiceTask}
     *
     * @param visits the visits being checked out
     */
    GetCheckOutInvoiceTask(Visits visits) {
        this.visits = visits;
    }

    /**
     * Returns an invoice associated with the current event, or the other events if none is available.
     * <p/>
     * This will select non-POSTED invoices in preference to POSTED ones, if available.
     *
     * @param context the context
     * @return an invoice linked to the event, or {@code null} if none is found.
     */
    @Override
    protected FinancialAct getInvoiceForEvent(TaskContext context) {
        FinancialAct invoice = null;
        Set<Reference> seen = new HashSet<>();
        List<FinancialAct> posted = new ArrayList<>();
        Act defaultEvent = (Act) context.getObject(PatientArchetypes.CLINICAL_EVENT);
        if (defaultEvent != null) {
            invoice = getInvoiceForEvent(defaultEvent, context, seen, posted);
        }
        if (invoice == null) {
            // select a non-POSTED invoice associated with one of the other visits, if any
            for (Visit visit : visits) {
                if (defaultEvent == null || !defaultEvent.equals(visit.getEvent())) {
                    invoice = getInvoiceForEvent(visit.getEvent(), context, seen, posted);
                    if (invoice != null) {
                        break;
                    }
                }
            }
        }
        return (invoice != null) ? invoice : selectMostRecent(posted);
    }
}
