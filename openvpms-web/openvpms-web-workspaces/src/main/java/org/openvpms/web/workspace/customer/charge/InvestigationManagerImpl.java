/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.edit.Saveable;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.AbstractRemovableCollectionPropertyEditor;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.DelegatingModifiable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.workspace.patient.investigation.PatientInvestigationActEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Default implementation of {@link InvestigationManager}.
 *
 * @author Tim Anderson
 */
public class InvestigationManagerImpl extends DelegatingModifiable implements InvestigationManager, Saveable {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules laboratoryRules;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The collection editor.
     */
    private final InvestigationsCollectionPropertyEditor collectionEditor;

    /**
     * The investigations, keyed on reference.
     */
    private final Map<Reference, DocumentAct> investigationsByRef = new HashMap<>();

    /**
     * The investigations associated with each invoice item.
     */
    private final Map<FinancialAct, Set<DocumentAct>> investigationsByItem = new HashMap<>();

    /**
     * Listener to notify if an investigation is reloaded.
     */
    private Runnable reloadListener;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InvestigationManagerImpl.class);

    /**
     * Tests node.
     */
    private static final String TESTS = "tests";


    /**
     * Constructs an {@link InvestigationManagerImpl}.
     *
     * @param service         the archetype service
     * @param laboratoryRules the laboratory rules
     * @param context         the layout context
     */
    InvestigationManagerImpl(IArchetypeService service, LaboratoryRules laboratoryRules, LayoutContext context) {
        this.service = service;
        this.laboratoryRules = laboratoryRules;
        this.context = context;
        collectionEditor = new InvestigationsCollectionPropertyEditor();
        setModifiable(collectionEditor);
    }

    /**
     * Invoked when an invoice item patient changes.
     *
     * @param editor  the invoice item
     * @param patient the patient. May be {@code null}
     * @return any new investigations created as a result of the patient change
     */
    @Override
    public List<PatientInvestigationActEditor> updatePatient(CustomerChargeActItemEditor editor, Party patient) {
        List<PatientInvestigationActEditor> result = Collections.emptyList();
        removeInvoiceItem(editor);
        if (patient != null) {
            Product product = editor.getProduct();
            if (product != null) {
                result = generateInvestigations(editor, patient, product);
            }
        }
        return result;
    }

    /**
     * Updates the product for an invoice item.
     *
     * @param editor  the invoice item
     * @param product the product. May be {@code null}
     * @return any new investigations created as a result of the product change
     */
    @Override
    public List<PatientInvestigationActEditor> updateProduct(CustomerChargeActItemEditor editor, Product product) {
        List<PatientInvestigationActEditor> result = Collections.emptyList();
        removeInvoiceItem(editor);
        if (product != null) {
            Party patient = editor.getPatient();
            if (patient != null) {
                result = generateInvestigations(editor, patient, product);
            }
        }
        return result;
    }

    /**
     * Invoked when an invoice item clinician changes.
     *
     * @param item      the invoice item
     * @param clinician the clinician. May be {@code null}
     */
    @Override
    public void updateClinician(FinancialAct item, User clinician) {
        for (DocumentAct act : getInvestigations(item, true)) {
            // only update those investigations only linked to this invoice item
            PatientInvestigationActEditor editor = getEditor(act);
            editor.setClinician(clinician);
        }
    }

    /**
     * Updates the start time of any investigation only linked to a single invoice item.
     *
     * @param item the invoice item
     */
    @Override
    public void updateTime(FinancialAct item) {
        for (DocumentAct act : getInvestigations(item, true)) {
            // only update those investigations only linked to this invoice item
            PatientInvestigationActEditor editor = getEditor(act);
            editor.setStartTime(item.getActivityStartTime());
        }
    }

    /**
     * Adds an invoice item.
     *
     * @param item the invoice item
     */
    @Override
    public void addInvoiceItem(FinancialAct item) {
        IMObjectBean bean = service.getBean(item);
        for (Reference reference : bean.getTargetRefs("investigations")) {
            DocumentAct investigation = investigationsByRef.get(reference);
            if (investigation == null) {
                investigation = service.get(reference, DocumentAct.class);
                if (investigation != null) {
                    collectionEditor.add(investigation);
                    addEditor(investigation, item);
                }
            }
            if (investigation != null) {
                addItemInvestigationItem(item, investigation);
            }
        }
    }

    /**
     * Removes an invoice item. Relationships to existing investigations will be removed.
     * <p/>
     * If there are no invoice item relationships, and the investigation can be removed, it will be removed.
     *
     * @param item the invoice item
     */
    @Override
    public void removeInvoiceItem(FinancialAct item) {
        List<ActRelationship> relationships = removeItem(item);
        for (ActRelationship relationship : relationships) {
            item.removeActRelationship(relationship);
        }
    }

    /**
     * Removes an invoice item. Relationships to existing investigations will be removed.
     * <p/>
     * If there are no invoice item relationships, and the investigation can be removed, it will be removed.
     *
     * @param editor the invoice item editor
     */
    @Override
    public void removeInvoiceItem(CustomerChargeActItemEditor editor) {
        FinancialAct item = editor.getObject();
        CollectionProperty investigations = editor.getCollectionProperty("investigations");
        for (ActRelationship relationship : removeItem(item)) {
            investigations.remove(relationship);
        }
    }

    /**
     * Returns the editor for an investigation, given its reference.
     *
     * @param investigationRef the investigation reference
     * @return the corresponding editor, or {@code null} if none exists
     */
    @Override
    public PatientInvestigationActEditor getEditor(Reference investigationRef) {
        DocumentAct act = investigationsByRef.get(investigationRef);
        return (PatientInvestigationActEditor) collectionEditor.getEditor(act);
    }

    /**
     * Returns all investigations.
     *
     * @return the investigations
     */
    @Override
    public List<DocumentAct> getInvestigations() {
        return new ArrayList<>(investigationsByRef.values());
    }

    /**
     * Returns investigations.
     *
     * @param ignorePending if {@code true}, ignore investigations with a PENDING order status
     * @return the investigations
     */
    @Override
    public List<DocumentAct> getInvestigations(boolean ignorePending) {
        List<DocumentAct> result = getInvestigations();
        if (ignorePending) {
            result.removeIf(act -> InvestigationActStatus.PENDING.equals(act.getStatus2()));
        }
        return result;
    }

    /**
     * Returns all investigations associated with an invoice item.
     *
     * @param item the invoice item
     * @return the corresponding investigations
     */
    @Override
    public List<DocumentAct> getInvestigations(FinancialAct item) {
        return getInvestigations(item, false);
    }

    /**
     * Returns all investigations that are associated with a laboratory, that either haven't been submitted,
     * or need to be confirmed.
     * <p/>
     * Investigations for HL7 laboratories are excluded.
     *
     * @return the investigations
     */
    @Override
    public List<DocumentAct> getUnconfirmedLaboratoryInvestigations() {
        List<DocumentAct> result = new ArrayList<>();
        for (DocumentAct investigation : getInvestigations()) {
            if (laboratoryRules.isUnsubmittedInvestigation(investigation)) {
                result.add(investigation);
            }
        }
        return result;
    }

    /**
     * Returns all investigation editors.
     *
     * @return the investigation editors
     */
    @Override
    public List<PatientInvestigationActEditor> getEditors() {
        List<PatientInvestigationActEditor> result = new ArrayList<>();
        for (DocumentAct act : getInvestigations()) {
            PatientInvestigationActEditor editor = getEditor(act.getObjectReference());
            if (editor != null) {
                result.add(editor);
            }
        }
        return result;
    }

    /**
     * Removes an investigation.
     *
     * @param investigation the investigation to remove
     * @return {@code true} if the investigation was removed
     */
    @Override
    public boolean removeInvestigation(DocumentAct investigation) {
        return collectionEditor.remove(investigation);
    }

    /**
     * Reloads an investigation.
     * NOTE: this reloads the investigation, but doesn't rewire any relationships. This means that multiple
     * copies of a relationship will be present e.g. from an invoice item to the investigation, and from the
     * investigation to the invoice item. If the relationship hasn't changed, this won't be a problem.
     *
     * @param investigation the investigation to reload
     * @return the reloaded investigation or {@code null} if it doesn't exist
     */
    @Override
    public DocumentAct reload(DocumentAct investigation) {
        DocumentAct result = null;
        Reference ref = investigation.getObjectReference();
        DocumentAct reloaded = service.get(ref, DocumentAct.class);
        if (reloaded != null && reloaded.getVersion() != investigation.getVersion()) {
            result = reloaded;
            if (investigationsByRef.get(ref) != null) {
                collectionEditor.refresh(reloaded);
                for (Set<DocumentAct> set : investigationsByItem.values()) {
                    if (set.contains(investigation)) {
                        set.remove(investigation);
                        set.add(reloaded);
                    }
                }
            }
            if (reloadListener != null) {
                try {
                    reloadListener.run();
                } catch (Throwable exception) {
                    log.error("Failed to callback reload listener: " + exception.getMessage(), exception);
                }
            }
        } else {
            result = investigation;
        }
        return result;
    }

    /**
     * Adds a listener to be notified when an investigation is reloaded.
     * <p/>
     * This operates outside of the {@link ModifiableListener} API, as this affects validation.
     *
     * @param callback the callback to invoke when an investigation is reloaded
     */
    @Override
    public void setReloadListener(Runnable callback) {
        this.reloadListener = callback;
    }

    /**
     * Returns the underlying collection property editor.
     *
     * @return the collection property editor
     */
    @Override
    public CollectionPropertyEditor getEditor() {
        return collectionEditor;
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    public void save() {
        collectionEditor.save();
    }

    /**
     * Determines if any edits have been saved.
     *
     * @return {@code true} if edits have been saved.
     */
    @Override
    public boolean isSaved() {
        return collectionEditor.isSaved();
    }

    /**
     * Adds an investigation editor for an existing investigation and its associated invoice item.
     * <p/>
     * Note that an investigation may be associated with multiple invoice items.
     *
     * @param investigation the investigation
     * @param item          the invoice item
     */
    protected void addEditor(DocumentAct investigation, FinancialAct item) {
        PatientInvestigationActEditor editor = new PatientInvestigationActEditor(investigation, item, context);
        collectionEditor.setEditor(investigation, editor);
    }

    /**
     * Removes an invoice item.
     * <p/>
     * If there is an associated investigation and it is:
     * <ul>
     *     <li>editable, this removes the product and tests that the item contributed to the investigation.
     *     If there are no other items associated with an investigation, it will be removed.</li>
     *     <li>not editable, the investigation(s) will be cancelled</li>
     * </ul>
     * An invoice item may be associated with multiple investigations.
     *
     * @param item the invoice item
     * @return the investigation relationships associated with the item
     */
    private List<ActRelationship> removeItem(FinancialAct item) {
        List<ActRelationship> result = new ArrayList<>();
        IMObjectBean bean = service.getBean(item);
        List<DocumentAct> investigations = getInvestigations(item);
        if (!investigations.isEmpty()) {
            Product product = (Product) getEntity(bean.getTargetRef("product"));
            List<Entity> tests = (product != null) ? getTests(product) : Collections.emptyList();
            for (DocumentAct investigation : investigations) {
                ActRelationship relationship = bean.getValue("investigations", ActRelationship.class,
                                                             Predicates.targetEquals(investigation));
                if (relationship != null) {
                    result.add(relationship);
                    PatientInvestigationActEditor investigationEditor = getEditor(investigation);
                    if (investigationEditor != null) {
                        if (investigationEditor.canEdit()) {
                            if (product != null) {
                                investigationEditor.removeProduct(product);
                                for (Entity test : tests) {
                                    investigationEditor.removeTest(test);
                                }
                            }
                        } else {
                            // can't edit the investigation, so cancel it
                            investigationEditor.setStatus(ActStatus.CANCELLED);
                        }

                        CollectionProperty invoiceItems = investigationEditor.getCollectionProperty("invoiceItems");
                        invoiceItems.remove(relationship);
                        if (invoiceItems.getValues().isEmpty()) {
                            // investigation has no further invoice items associated with it, so remove it.
                            // If it is already in progress, this will result in it being cancelled
                            collectionEditor.remove(investigation);
                        }
                    }
                }
            }
        }
        investigationsByItem.remove(item);
        return result;
    }

    /**
     * Determines if an investigation can be removed.
     *
     * @param investigation the investigation
     * @return {@code true} if the investigation can be removed
     */
    private boolean canRemove(DocumentAct investigation) {
        return ActStatus.IN_PROGRESS.equals(investigation.getStatus()) && investigation.getDocument() == null;
    }

    /**
     * Updates the internal map of investigations by invoice item.
     *
     * @param item          the invoice item
     * @param investigation the investigation
     */
    private void addItemInvestigationItem(FinancialAct item, DocumentAct investigation) {
        Set<DocumentAct> investigations = investigationsByItem.computeIfAbsent(item, k -> new HashSet<>());
        investigations.add(investigation);
    }

    /**
     * Returns an investigation editor given its act.
     *
     * @param act the investigation act
     * @return the corresponding investigation editor. May be {@code null}
     */
    private PatientInvestigationActEditor getEditor(DocumentAct act) {
        return (PatientInvestigationActEditor) collectionEditor.getEditor(act);
    }

    /**
     * Adds an investigation.
     *
     * @param investigation the investigation
     */
    private void doAdd(DocumentAct investigation) {
        investigationsByRef.put(investigation.getObjectReference(), investigation);
    }

    /**
     * Removes an investigation.
     * <p/>
     * This unlinks it from any associated invoice items.
     *
     * @param investigation the investigation
     * @return {@code true} if the investigation was removed, otherwise {@code false}
     */
    private boolean doRemove(DocumentAct investigation) {
        boolean removed = investigationsByRef.remove(investigation.getObjectReference()) != null;
        if (removed) {
            IMObjectBean bean = service.getBean(investigation);
            for (ActRelationship relationship : bean.getValues("invoiceItems", ActRelationship.class)) {
                for (Map.Entry<FinancialAct, Set<DocumentAct>> entry : investigationsByItem.entrySet()) {
                    FinancialAct item = entry.getKey();
                    if (item.getObjectReference().equals(relationship.getSource())) {
                        item.removeActRelationship(relationship);
                        investigation.removeActRelationship(relationship);
                        entry.getValue().remove(investigation);
                    }
                }
            }
        }
        return removed;
    }

    /**
     * Returns the investigations associated with an invoice item.
     *
     * @param item   the invoice item
     * @param unique if {@code true}, only return investigations that are only associated with the one invoice item
     * @return the associated investigations
     */
    private List<DocumentAct> getInvestigations(FinancialAct item, boolean unique) {
        List<DocumentAct> result;
        Set<DocumentAct> acts = investigationsByItem.get(item);
        if (acts == null) {
            result = Collections.emptyList();
        } else if (!unique) {
            result = new ArrayList<>(acts);
        } else {
            result = new ArrayList<>();
            for (DocumentAct act : acts) {
                IMObjectBean bean = service.getBean(act);
                if (bean.getTargetRefs("invoiceItems").size() == 1) {
                    result.add(act);
                }
            }
        }
        return result;
    }

    /**
     * Generates investigations for tests associated with a product.
     *
     * @param item    the invoice item editor
     * @param patient the patient
     * @param product the product
     * @return the corresponding investigation editors
     */
    private List<PatientInvestigationActEditor> generateInvestigations(CustomerChargeActItemEditor item, Party patient,
                                                                       Product product) {
        Set<PatientInvestigationActEditor> result = new LinkedHashSet<>();
        // add a new investigation act for each unique investigation type
        for (Entity test : getTests(product)) {
            PatientInvestigationActEditor editor = generateInvestigation(item, patient, product, test);
            if (editor != null) {
                result.add(editor);
            }
        }
        return new ArrayList<>(result);
    }

    /**
     * Generates an investigation for a test.
     * <p/>
     * If the test supports grouping there is an existing investigation for the same investigation type that
     * hasn't been submitted, then the test will be added to it.
     * If not, a new investigation will be created.
     *
     * @param item    the invoice item editor
     * @param patient the patient
     * @param product the product
     * @param test    the test
     * @return the corresponding investigation editor, or {@code null} if one cannot be created
     */
    private PatientInvestigationActEditor generateInvestigation(CustomerChargeActItemEditor item, Party patient,
                                                                Product product, Entity test) {
        PatientInvestigationActEditor investigationEditor = null;
        IMObjectBean bean = service.getBean(test);
        Entity investigationType = getEntity(bean.getTargetRef("investigationType"));
        if (investigationType != null) {
            for (IMObjectEditor editor : collectionEditor.getEditors()) {
                PatientInvestigationActEditor candidate = (PatientInvestigationActEditor) editor;
                if (candidate.canAddTest(patient, test)) {
                    investigationEditor = candidate;
                    break;
                }
            }
            if (investigationEditor != null) {
                // add the test to an existing investigation
                investigationEditor.addProduct(product);
                investigationEditor.addTest(test);
                item.addInvestigation(investigationEditor);
            } else {
                DocumentAct act = service.create(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class);
                investigationEditor = new PatientInvestigationActEditor(act, item.getObject(), context);
                investigationEditor.getComponent();
                investigationEditor.setInvestigationType(investigationType);
                investigationEditor.setPatient(patient);
                investigationEditor.addProduct(product);
                investigationEditor.addTest(test);
                investigationEditor.setClinician(item.getClinician());
                item.addInvestigation(investigationEditor);
                collectionEditor.add(act);
                collectionEditor.setEditor(act, investigationEditor);
            }
            addItemInvestigationItem(item.getObject(), investigationEditor.getObject());
        }
        return investigationEditor;
    }

    /**
     * Returns the tests associated with a product.
     *
     * @param product the product
     * @return the <em>entity.laboratoryTest</em> instances associated with the product
     */
    private List<Entity> getTests(Product product) {
        List<Entity> result;
        IMObjectBean bean = service.getBean(product);
        if (bean.hasNode(TESTS)) {
            result = new ArrayList<>();
            for (Reference reference : bean.getTargetRefs(TESTS)) {
                Entity test = getEntity(reference);
                if (test != null) {
                    result.add(test);
                }
            }
        } else {
            result = Collections.emptyList();
        }
        return result;
    }

    /***
     * Returns an active entity, given its reference.
     * @param reference the reference. May be {@code null}
     * @return the corresponding entity, or {@code null} if does not exist or is inactive
     */
    private Entity getEntity(Reference reference) {
        return (Entity) context.getCache().get(reference, true);
    }

    /**
     * An {@link CollectionPropertyEditor} that protects investigations from removal if they are CANCELLED or
     * POSTED.
     */
    private class InvestigationsCollectionPropertyEditor extends AbstractRemovableCollectionPropertyEditor {
        InvestigationsCollectionPropertyEditor() {
            super(new InvestigationsCollection());
        }

        /**
         * Refreshes an investigation.
         * <p/>
         * NOTE: this does not notify any listeners, as that would mark the collection as dirty, after the parent
         * charge has been saved.
         *
         * @param investigation the investigation
         */
        public void refresh(DocumentAct investigation) {
            doAdd(investigation);
            PatientInvestigationActEditor existing = (PatientInvestigationActEditor) getEditor(investigation);
            if (existing != null) {
                PatientInvestigationActEditor newEditor = new PatientInvestigationActEditor(
                        investigation, (Act) existing.getParent(), context);
                setEditor(investigation, newEditor);
                getEditor(investigation);
            }
        }

        @Override
        protected void queueRemove(IMObject object, IMObjectEditor editor) {
            if (canRemove((DocumentAct) object)) {
                super.queueRemove(object, editor);
            }
        }
    }


    private class InvestigationsCollection extends SimpleProperty implements CollectionProperty {

        InvestigationsCollection() {
            super("investigations", DocumentAct.class);
            setArchetypeRange(new String[]{InvestigationArchetypes.PATIENT_INVESTIGATION});
        }

        /**
         * Add a value.
         *
         * @param value the value to add
         */
        @Override
        public void add(Object value) {
            doAdd((DocumentAct) value);
            refresh();
        }

        /**
         * Remove a value.
         *
         * @param value the value to remove
         * @return {@code true} if the value was removed
         */
        @Override
        public boolean remove(Object value) {
            boolean removed = doRemove((DocumentAct) value);
            if (removed) {
                refresh();
            }
            return removed;
        }

        /**
         * Returns the collection.
         *
         * @return the collection
         */
        @Override
        public List<?> getValues() {
            return getInvestigations();
        }

        /**
         * Returns the no. of elements in the collection
         *
         * @return the no. of elements in the collection
         */
        @Override
        public int size() {
            return investigationsByRef.size();
        }

        /**
         * Returns the minimum cardinality.
         *
         * @return the minimum cardinality
         */
        @Override
        public int getMinCardinality() {
            return 0;
        }

        /**
         * Returns the maximum cardinality.
         *
         * @return the maximum cardinality, or {@code -1} if it is unbounded
         */
        @Override
        public int getMaxCardinality() {
            return -1;
        }

        /**
         * Determines the relationship of the elements of the collection to the object.
         *
         * @return {@code true} if the objects are children of the parent object, or {@code false} if they are its peer
         */
        @Override
        public boolean isParentChild() {
            return false;
        }
    }
}
