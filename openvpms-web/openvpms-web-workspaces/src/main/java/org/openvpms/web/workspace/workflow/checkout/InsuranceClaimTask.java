/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.practice.Location;
import org.openvpms.insurance.internal.InsuranceFactory;
import org.openvpms.insurance.service.GapInsuranceService;
import org.openvpms.insurance.service.InsuranceService;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.workflow.AbstractTask;
import org.openvpms.web.component.workflow.ConditionalTask;
import org.openvpms.web.component.workflow.ConfirmationTask;
import org.openvpms.web.component.workflow.EditIMObjectTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.component.workflow.Tasks;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.insurance.claim.ClaimEditDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A task that prompts the user to claim against a patient's insurance policy if their provider supports it.
 *
 * @author Tim Anderson
 */
public class InsuranceClaimTask extends Tasks {

    /**
     * The policy.
     */
    private final Act policy;

    /**
     * Determines if gap claims can be submitted.
     */
    private final boolean gapClaim;

    /**
     * The insurance rules.
     */
    private final InsuranceRules rules;

    /**
     * The insurer.
     */
    private Party insurer;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InsuranceClaimTask.class);

    /**
     * Constructs an {@link InsuranceClaimTask}.
     *
     * @param policy   the policy
     * @param gapClaim if {@code true}, make a gap claim
     * @param help     the help context
     */
    public InsuranceClaimTask(Act policy, boolean gapClaim, HelpContext help) {
        super(help);
        this.policy = policy;
        this.gapClaim = gapClaim;
        rules = ServiceHelper.getBean(InsuranceRules.class);
        setRequired(false);
    }

    /**
     * Initialise any tasks.
     *
     * @param context the task context
     */
    @Override
    protected void initialise(TaskContext context) {
        insurer = rules.getInsurer(policy);
        if (insurer != null) {
            if (gapClaim) {
                // check if gap claims are available, and if so prompt to make a gap claim
                addTask(new GapClaimTask(context.getHelpContext()));
            } else {
                // prompt to make a standard claim
                ConfirmationTask confirm = new ConfirmClaimTask(insurer, context.getHelpContext());
                addTask(new ConditionalTask(confirm, new EditClaimTask()));
            }
        }
    }

    /**
     * Task to determine if gap claims are available for the policy and if so, prompts to make a claim.
     */
    private class GapClaimTask extends Tasks {

        public GapClaimTask(HelpContext context) {
            super(context);
            setRequired(false);
        }

        /**
         * Initialise any tasks.
         *
         * @param context the task context
         */
        @Override
        protected void initialise(TaskContext context) {
            addTask(new AbstractTask() {
                @Override
                public void start(TaskContext context) {
                    run(context);
                    notifyCompleted();
                }
            });
        }

        /**
         * Runs the task.
         *
         * @param context the task context
         */
        private void run(TaskContext context) {
            try {
                if (isGapClaimAvailable(context)) {
                    ConfirmationTask confirm = new ConfirmClaimTask(insurer, context.getHelpContext());
                    addTask(new ConditionalTask(confirm, new EditClaimTask()));
                }
            } catch (Throwable exception) {
                String preamble = Messages.format("patient.insurance.gapclaim.notavailable", insurer.getName());
                log.error(preamble + " " + exception.getMessage(), exception);
                ErrorTask task = new ErrorTask(preamble, exception, () -> {
                    // re-run the task
                    initialise(context);
                });
                addTask(task);
            }
        }

        /**
         * Determines if gap claims are available for the policy.
         *
         * @param context the task context
         * @return {@code true} if gap claims are available, otherwise {@code false}
         */
        private boolean isGapClaimAvailable(TaskContext context) {
            boolean result = false;
            String policyNumber = rules.getPolicyNumber(policy);
            InsuranceService insuranceService = ServiceHelper.getBean(InsuranceServices.class).getService(insurer);
            if (insuranceService instanceof GapInsuranceService) {
                InsuranceFactory factory = ServiceHelper.getBean(InsuranceFactory.class);
                GapInsuranceService gapService = (GapInsuranceService) insuranceService;
                Location location = factory.getLocation(context.getLocation());
                result = gapService.getGapClaimAvailability(insurer, policyNumber, location).isAvailable();
            }
            return result;
        }
    }

    /**
     * Task to edit a claim.
     */
    private class EditClaimTask extends EditIMObjectTask {

        /**
         * Constructs a new {@code EditIMObjectTask} to edit an object
         * in the {@link TaskContext}.
         */
        public EditClaimTask() {
            super(InsuranceArchetypes.CLAIM);
        }

        /**
         * Starts the task.
         *
         * @param context the task context
         */
        @Override
        public void start(TaskContext context) {
            FinancialAct claim = rules.createClaim(policy);
            if (gapClaim) {
                IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(claim);
                bean.setValue("gapClaim", true);
            }
            context.setObject(InsuranceArchetypes.CLAIM, (IMObject) claim);
            super.start(context);
        }

        /**
         * Invoked when the edit dialog closes to complete the task.
         *
         * @param action  the dialog action
         * @param editor  the editor
         * @param context the task context
         */
        @Override
        protected void onDialogClose(String action, IMObjectEditor editor, TaskContext context) {
            if (ClaimEditDialog.SUBMIT_ID.equals(action)) {
                super.onEditCompleted();
            } else {
                super.onDialogClose(action, editor, context);
            }
        }

        /**
         * Invoked when editing fails due to error.
         *
         * @param object    the object being edited.
         * @param context   the  task context
         * @param exception the cause of the error
         */
        @Override
        protected void onError(IMObject object, TaskContext context, Throwable exception) {
            String preamble = Messages.format("patient.insurance.claim.editerror", insurer.getName());
            log.error(preamble + " " + exception.getMessage(), exception);
            ErrorDialog.newDialog()
                    .preamble(preamble)
                    .message(ErrorFormatter.format(exception))
                    .buttons(ErrorDialog.RETRY_ID, ErrorDialog.SKIP_ID, ErrorDialog.CANCEL_ID)
                    .listener(new PopupDialogListener() {
                        @Override
                        public void onRetry() {
                            start(context);
                        }

                        @Override
                        public void onSkip() {
                            notifySkipped();
                        }

                        @Override
                        public void onCancel() {
                            notifyCancelled();
                        }
                    })
                    .show();
        }
    }

    private class ConfirmClaimTask extends ConfirmationTask {

        /**
         * Constructs a {@link ConfirmClaimTask}.
         *
         * @param insurer the insurer
         * @param help    the help context
         */
        public ConfirmClaimTask(Party insurer, HelpContext help) {
            super((gapClaim) ? Messages.get("patient.insurance.gapclaim.title")
                             : Messages.get("patient.insurance.claim.title"),
                  (gapClaim) ? Messages.format("patient.insurance.gapclaim.message", insurer.getName())
                             : Messages.format("patient.insurance.claim.message", insurer.getName()),
                  help);
            setRequired(false);
        }
    }

    /**
     * Tasks to display an error and prompt to retry.
     */
    private static class ErrorTask extends AbstractTask {

        /**
         * The message to display before the error.
         */
        private final String preamble;

        /**
         * The error.
         */
        private final Throwable error;

        /**
         * The listener to invoke to retry.
         */
        private final Runnable retryListener;

        /**
         * Constructs an {@link ErrorTask}.
         *
         * @param preamble      the message to display before the error
         * @param error         the error
         * @param retryListener the listener to invoke on retry
         */
        public ErrorTask(String preamble, Throwable error, Runnable retryListener) {
            this.preamble = preamble;
            this.error = error;
            this.retryListener = retryListener;
        }

        /**
         * Starts the task.
         *
         * @param context the task context
         */
        @Override
        public void start(TaskContext context) {
            ErrorDialog.newDialog()
                    .preamble(preamble)
                    .message(ErrorFormatter.format(error))
                    .buttons(ErrorDialog.RETRY_ID, ErrorDialog.SKIP_ID, ErrorDialog.CANCEL_ID)
                    .listener(new PopupDialogListener() {
                        @Override
                        public void onRetry() {
                            retryListener.run();
                            notifyCompleted();
                        }

                        @Override
                        public void onSkip() {
                            notifySkipped();
                        }

                        @Override
                        public void onCancel() {
                            notifyCancelled();
                        }
                    })
                    .show();
        }
    }

}
