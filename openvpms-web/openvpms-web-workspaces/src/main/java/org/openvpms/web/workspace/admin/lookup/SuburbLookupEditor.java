/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.admin.lookup;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.LookupRelationshipCollectionEditor;
import org.openvpms.web.component.im.relationship.LookupRelationshipEditor;
import org.openvpms.web.component.property.Property;


/**
 * Editor for <em>lookup.suburb</em> lookups.
 *
 * @author Tim Anderson
 */
public class SuburbLookupEditor extends AbstractLookupEditor {

    /**
     * Postcode node name.
     */
    private static final String POSTCODE = "postCode";

    /**
     * The state node name.
     */
    private static final String STATE = "target";

    /**
     * Constructs a {@link SuburbLookupEditor}.
     *
     * @param object  the object to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context. May be {@code null}.
     */
    public SuburbLookupEditor(Lookup object, IMObject parent, LayoutContext context) {
        super(object, parent, context);

        if (object.isNew()) {
            // update the code when the postcode or state changes
            Property postCode = getProperty(POSTCODE);
            if (postCode != null) {
                postCode.addModifiableListener(modifiable -> updateCode());
            }
            Property state = getProperty(STATE);
            if (state != null) {
                state.addModifiableListener(modifiable -> updateCode());
            }
        }
    }

    /**
     * Returns the postcode.
     *
     * @return the postcode. May be {@code null}
     */
    public String getPostcode() {
        Property property = getProperty(POSTCODE);
        return property != null ? property.getString() : null;
    }

    /**
     * Sets the postcode.
     *
     * @param postcode the postcode. May be {@code null}
     */
    public void setPostcode(String postcode) {
        Property property = getProperty(POSTCODE);
        if (property != null) {
            property.setValue(postcode);
        }
    }

    /**
     * Returns the state code.
     *
     * @return the state code. May be {@code null}
     */
    public String getState() {
        IMObjectBean bean = getBean(getObject());
        Lookup state = bean.getSource(STATE, Lookup.class);
        return (state != null) ? state.getCode() : null;
    }

    /**
     * Sets the state.
     *
     * @param lookup the state lookup. May be {@code null}
     */
    public void setState(Lookup lookup) {
        LookupRelationshipCollectionEditor collectionEditor
                = (LookupRelationshipCollectionEditor) getEditor(STATE, true);
        LookupRelationshipEditor editor = (LookupRelationshipEditor) collectionEditor.getCurrentEditor();
        editor.setSource(lookup);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new SuburbLookupEditor((Lookup) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Creates a code for the lookup based on the name and postcode, or name and state if no postcode is present.
     *
     * @return a new code
     */
    @Override
    protected String createCode() {
        String result = super.createCode();
        String postcode = getPostcode();
        if (!StringUtils.isEmpty(postcode)) {
            result += "_" + postcode;
        } else {
            String state = getState();
            if (!StringUtils.isEmpty(state)) {
                result += "_" + state;
            }
        }
        return result;
    }
}
