/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.scheduledreport;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.ChangeEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.function.date.RelativeDateParser;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.web.component.bound.BoundSelectFieldFactory;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.edit.AbstractPropertyEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.list.PairListModel;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.IntegerPropertyTransformer;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.ChangeListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.SelectFieldFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Date;

/**
 * Editor for a report date parameter.
 * <p/>
 * These can be absolute dates,or relative dates such as now or today . Relative dates can have a modifier e.g +3 days,
 * -1 years.
 * <p/>
 * Relative dates use a subset of the {@link RelativeDateParser} tokens, i.e. {@code [-](0..9)+(d|m|w|y)}
 *
 * @author Tim Anderson
 * @see RelativeDateParser
 */
class DateParameter extends AbstractPropertyEditor {

    /**
     * The expression type.
     */
    private final Property expressionType;

    /**
     * Listener for expression type changes.
     */
    private final ModifiableListener expressionTypeListener;

    /**
     * The parameter value.
     */
    private final Property value;

    /**
     * Property to hold a fixed date, used when the expressionType is {@link ExpressionType#VALUE}.
     */
    private final SimpleProperty date;

    /**
     * The component to hold the fixed date.
     */
    private final Component dateField;

    /**
     * Integer property to hold a relative date offset, when the the expressionType isn't {@link ExpressionType#VALUE}.
     */
    private final SimpleProperty offset;

    /**
     * The component to hold the relative offset.
     */
    private final Component offsetField;

    /**
     * The date units field, when relative date offsets are used.
     */
    private final SelectField unitsField;

    /**
     * The component.
     */
    private final Row component;

    /**
     * The focus group.
     */
    private final FocusGroup group;

    /**
     * Button displayed when entering dates that may have an offset added.
     */
    private final Button addOffset;

    /**
     * Constructs a {@link DateParameter}.
     *
     * @param name           the parameter name
     * @param displayName    the display name
     * @param expressionType the expression type
     * @param value          the parameter value
     * @param context        the layout context
     */
    public DateParameter(String name, String displayName, Property expressionType, Property value,
                         LayoutContext context) {
        super(new SimpleProperty(name, null, Date.class, displayName));
        this.value = value;
        date = new SimpleProperty(name, null, Date.class, displayName);
        offset = new SimpleProperty(name, null, Integer.class, displayName);
        offset.setTransformer(new IntegerPropertyTransformer(offset));

        this.expressionType = expressionType;
        expressionTypeListener = modifiable -> onExpressionTypeChanged();
        this.expressionType.addModifiableListener(expressionTypeListener);
        String currentValue = StringUtils.trimToNull(value.getString());

        addOffset = ButtonFactory.create(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                showDate(false, true);
            }
        });
        addOffset.setText(Messages.get("scheduledreport.expression.date.offset"));
        addOffset.setToolTipText(Messages.get("scheduledreport.expression.date.offset.tooltip"));
        offsetField = BoundTextComponentFactory.create(offset, 3);
        unitsField = createDateUnitField();
        IMObjectComponentFactory factory = context.getComponentFactory();
        dateField = factory.create(date);

        boolean showOffset = false;
        if (isValueExpression()) {
            date.setValue(value.getDate());
        } else {
            // relative date
            DateOffset duration = DateOffset.parse(currentValue);
            if (duration != null) {
                showOffset = true;
                offset.setValue(Integer.toString(duration.getOffset()));
                unitsField.setSelectedItem(duration.getUnits());
            } else {
                unitsField.setSelectedIndex(0);
            }
        }

        SelectField expressionTypeField = createExpressionTypeField(expressionType);
        if (expressionTypeField.getSelectedItem() == null) {
            expressionTypeField.setSelectedItem(ExpressionType.VALUE.toString());
        }

        if (context.isEdit()) {
            date.addModifiableListener(modifiable -> value.setValue(date.getValue()));
            offset.addModifiableListener(modifiable -> onDateOffsetChanged());
            unitsField.getSelectionModel().addChangeListener(new ChangeListener() {
                @Override
                public void onChange(ChangeEvent event) {
                    onDateOffsetChanged();
                }
            });
            unitsField.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                }
            });
        }
        showDate(isValueExpression(), showOffset);

        component = RowFactory.create(Styles.CELL_SPACING, expressionTypeField, dateField, addOffset, offsetField,
                                      unitsField);
        group = new FocusGroup("DateParameter", expressionTypeField, dateField, addOffset, offsetField, unitsField);
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        return component;
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group, or {@code null} if the editor hasn't been rendered
     */
    @Override
    public FocusGroup getFocusGroup() {
        return group;
    }

    /**
     * Disposes of the editor.
     * <br/>
     * Once disposed, the behaviour of invoking any method is undefined.
     */
    @Override
    public void dispose() {
        super.dispose();
        expressionType.removeModifiableListener(expressionTypeListener);
    }

    /**
     * Creates an field to change the expression type.
     *
     * @param expressionType the expression type property
     * @return a new field
     */
    private SelectField createExpressionTypeField(Property expressionType) {
        PairListModel model = new PairListModel();
        for (ExpressionType type : ExpressionType.values()) {
            model.add(type.toString(), Messages.get("scheduledreport.expression." + type.name()));
        }
        SelectField result = BoundSelectFieldFactory.create(expressionType, model);
        result.setCellRenderer(PairListModel.RENDERER);
        return result;
    }

    /**
     * Creates an field to change the date units.
     *
     * @return a new field
     */
    private SelectField createDateUnitField() {
        PairListModel model = new PairListModel();
        for (DateUnits units : new DateUnits[]{DateUnits.DAYS, DateUnits.WEEKS, DateUnits.MONTHS, DateUnits.YEARS}) {
            model.add(units, Messages.get("scheduledreport.dateunits." + units.name()));
        }
        SelectField result = SelectFieldFactory.create(model);
        result.setCellRenderer(PairListModel.RENDERER);
        return result;
    }

    /**
     * Invoked when the date offset changes. Updates the parameter value.
     */
    private void onDateOffsetChanged() {
        String offset = getDateOffset();
        value.setValue(offset);
    }

    /**
     * Determines the date offset.
     *
     * @return the date offset, May be {@code null}
     */
    private String getDateOffset() {
        int value = offset.getInt();
        String result = null;
        if (value != 0) {
            result = Integer.toString(value);
            DateUnits units = (DateUnits) unitsField.getSelectedItem();
            if (units != null) {
                switch (units) {
                    case DAYS:
                        result += "d";
                        break;
                    case WEEKS:
                        result += "w";
                        break;
                    case MONTHS:
                        result += "m";
                        break;
                    case YEARS:
                        result += "y";
                        break;
                    default:
                        result = null;
                }
            }
        }
        return result;
    }

    /**
     * Invoked when the expression type changes.
     */
    private void onExpressionTypeChanged() {
        boolean isDate = isValueExpression();
        showDate(isDate, offset.getInt() != 0);
        if (isDate) {
            value.setValue(date.getDate());
        } else {
            value.setValue(getDateOffset());
        }
    }

    /**
     * Shows/hides the date.
     *
     * @param isDate     if {@code true}, it is a fixed date expression, else it is a relative date expression
     * @param showOffset if {@code true}, and it is a relative date expression, show the offset fields
     */
    private void showDate(boolean isDate, boolean showOffset) {
        dateField.setVisible(isDate);
        boolean offset = !isDate && showOffset;
        addOffset.setVisible(!isDate && !showOffset);
        offsetField.setVisible(offset);
        unitsField.setVisible(offset);
    }

    /**
     * Determines if the selected expression is an {@link ExpressionType#VALUE}, representing a date value.
     *
     * @return {@code true} if the expression is a date value
     */
    private boolean isValueExpression() {
        return ExpressionType.VALUE.toString().equals(expressionType.getString());
    }

}
