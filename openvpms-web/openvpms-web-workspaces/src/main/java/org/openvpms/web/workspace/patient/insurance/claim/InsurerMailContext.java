/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.contact.ContactHelper;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.customer.CustomerMailContext;

import java.util.Collections;
import java.util.List;

/**
 * A {@link MailContext} for emailing claims to insurers.
 *
 * @author Tim Anderson
 */
public class InsurerMailContext extends CustomerMailContext {

    /**
     * Constructs a {@link InsurerMailContext} that registers an attachment browser.
     *
     * @param context the context
     * @param help    the help context
     */
    public InsurerMailContext(Context context, HelpContext help) {
        super(context, help);
    }

    /**
     * Returns the insurer.
     *
     * @return the insurer. May be {@code null}
     */
    public Party getInsurer() {
        return getContext().getSupplier();
    }

    /**
     * Creates the to-address state.
     *
     * @param builder the state builder
     * @return a new state
     */
    @Override
    protected AddressState createToAddressState(AddressStateBuilder builder) {
        Party insurer = getInsurer();
        builder.addParty(SupplierArchetypes.INSURER, insurer);

        List<Contact> contacts;
        if (insurer != null) {
            contacts = ContactHelper.getEmailContacts(insurer, getService());
        } else {
            contacts = Collections.emptyList();
        }
        Contact preferred = !contacts.isEmpty() ? contacts.get(0) : null;
        builder.addContacts(contacts);
        builder.setPreferred(preferred);
        return super.createToAddressState(builder);
    }

    /**
     * Determines if the to-address state is out of date.
     * <p/>
     * The state is considered out of date if the parties used to build it don't match, or have been changed.
     *
     * @param state the state
     * @return {@code true} if the state is out of date, otherwise {@code false}
     */
    @Override
    protected boolean toAddressesOutOfDate(AddressState state) {
        return super.toAddressesOutOfDate(state) || !state.matches(SupplierArchetypes.INSURER, getInsurer());
    }
}
