/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditor;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.act.ActLayoutStrategy;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.style.Styles;

import java.util.List;


/**
 * Layout strategy for <em>act.supplierDelivery</em> and <em>act.supplierReturn</em> acts.
 * <p/>
 * Displays the {@code supplierNotes} below the simple items, if non-null.
 */
public class DeliveryLayoutStrategy extends ActLayoutStrategy {

    /**
     * The supplier invoice identifier node.
     */
    private static final String SUPPLIER_INVOICE_ID = "supplierInvoiceId";

    /**
     * The supplier notes node.
     */
    private static final String SUPPLIER_NOTES = "supplierNotes";

    /**
     * Constructs a {@link DeliveryLayoutStrategy} for viewing deliveries.
     */
    public DeliveryLayoutStrategy() {
        this(null);
    }

    /**
     * Constructs a {@link DeliveryLayoutStrategy} for editing deliveries.
     *
     * @param editor the delivery items editor. May be {@code null}
     */
    public DeliveryLayoutStrategy(IMObjectCollectionEditor editor) {
        super(editor);
        getArchetypeNodes().excludeIfEmpty(SUPPLIER_INVOICE_ID);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        // exclude the supplier notes, as these are added manually
        Property supplierNotes = properties.get(SUPPLIER_NOTES);
        if (supplierNotes != null) {
            getArchetypeNodes().exclude(SUPPLIER_NOTES);
            if (supplierNotes.getValue() != null) {
                addComponent(createMultiLineText(supplierNotes, 2, 10, Styles.FULL_WIDTH, context));
            }
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out components in a grid.
     *
     * @param object     the object to lay out
     * @param properties the properties
     * @param context    the layout context
     */
    @Override
    protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentGrid grid = super.createGrid(object, properties, context);
        ComponentState supplierNotes = getComponent(SUPPLIER_NOTES);
        if (supplierNotes != null) {
            grid.add(supplierNotes, 2);
        }
        return grid;
    }
}
