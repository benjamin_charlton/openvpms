/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.web.component.im.doc.DocumentViewer;
import org.openvpms.web.component.im.doc.DocumentViewerFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Viewer for investigation documents and external results.
 *
 * @author Tim Anderson
 * @see DocumentViewerFactory
 */
public class InvestigationViewer extends DocumentViewer {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InvestigationViewer.class);

    /**
     * Constructs an {@link InvestigationViewer}.
     *
     * @param act     the document act
     * @param link    if {@code true} enable a hyperlink to the object
     * @param context the layout context
     */
    public InvestigationViewer(DocumentAct act, boolean link, LayoutContext context) {
        super(act, link, context);
    }

    /**
     * Constructs an {@link InvestigationViewer}.
     *
     * @param act      the document act
     * @param link     if {@code true} enable a hyperlink to the object
     * @param template if {@code true}, display as a template, otherwise generate the document if required
     * @param context  the layout context
     */
    public InvestigationViewer(DocumentAct act, boolean link, boolean template, LayoutContext context) {
        super(act, link, template, context);
    }

    /**
     * Constructs an {@link InvestigationViewer}.
     *
     * @param reference the reference to view
     * @param parent    the parent. May be {@code null}
     * @param link      if {@code true} enable a hyperlink to the object
     * @param context   the layout context
     */
    public InvestigationViewer(Reference reference, IMObject parent, boolean link, LayoutContext context) {
        super(reference, parent, link, context);
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    @Override
    public Component getComponent() {
        Component result = null;
        DocumentAct parent = (DocumentAct) getParent();
        if (parent != null && !parent.isNew()) {
            // can't link if the act hasn't been saved
            Component document = getDocument();
            Component external = (link()) ? getExternalResults() : null;
            if (document != null && external != null) {
                result = RowFactory.create(Styles.CELL_SPACING, document, external);
            } else if (document != null) {
                result = document;
            } else if (external != null) {
                result = external;
            }
        }
        if (result == null) {
            result = getPlaceholder();
        }
        return result;
    }

    /**
     * Returns a component to view external results, if available.
     *
     * @return a component to view the external results, or {@code null} if there are none
     */
    private Component getExternalResults() {
        IMObjectBean bean = getBean();
        DocumentAct investigation = (DocumentAct) getParent();
        Component result = null;
        Entity laboratory = getLaboratory(bean);
        if (laboratory != null && laboratory.isActive()) {
            try {
                if (bean.getBoolean("externalResults")) {
                    ExternalResultsViewer viewer = new ExternalResultsViewer(investigation);
                    result = viewer.getComponent();
                } else {
                    ResultChecker resultChecker = new ResultChecker(investigation);
                    if (resultChecker.canCheckResults()) {
                        CheckResultsViewer viewer = new CheckResultsViewer(resultChecker);
                        result = viewer.getComponent();
                    }
                }
            } catch (Throwable exception) {
                log.error("Failed to get results viewer for laboratory=" + laboratory.getName() + ": "
                          + exception.getMessage(), exception);
            }
            return result;
        }
        return result;
    }

    /**
     * Returns the laboratory associated with an investigation.
     *
     * @param bean the investigation bean
     * @return the laboratory, or {@code null} if the investigation has none
     */
    private Entity getLaboratory(IMObjectBean bean) {
        IMObjectCache cache = getContext().getCache();
        return (Entity) cache.get(bean.getTargetRef("laboratory"));
    }
}
