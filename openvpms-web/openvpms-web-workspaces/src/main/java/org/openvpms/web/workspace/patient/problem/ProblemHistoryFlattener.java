/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.problem;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.act.ActHierarchyIterator;
import org.openvpms.web.workspace.patient.history.AbstractPatientHistoryFlattener;

import java.util.List;
import java.util.function.Predicate;

/**
 * Flattens problem history for display in a table.
 * <p/>
 * This excludes communication acts if they are present.
 *
 * @author Tim Anderson
 */
class ProblemHistoryFlattener extends AbstractPatientHistoryFlattener {

    /**
     * Constructs a {@link ProblemHistoryFlattener}.
     *
     * @param service the archetype service
     */
    public ProblemHistoryFlattener(ArchetypeService service) {
        super(PatientArchetypes.CLINICAL_PROBLEM, false, service);
    }

    /**
     * Creates an iterator over the act hierarchy.
     *
     * @param objects            the top-level parent acts, minus communications
     * @param archetypes         the child archetypes to include
     * @param search             the search criteria. May be {@code null}
     * @param childSortAscending if {@code true} sort child acts on ascending timestamp; otherwise sort on descending
     *                           timestamp
     * @param communications     communications to merge
     * @param service            the archetype service
     * @return a new iterator
     */
    @Override
    protected ActHierarchyIterator<Act> createIterator(List<Act> objects, String[] archetypes,
                                                       Predicate<org.openvpms.component.model.act.Act> search,
                                                       boolean childSortAscending, List<Act> communications,
                                                       ArchetypeService service) {
        ProblemFilter filter = new ProblemFilter(archetypes, search, childSortAscending, service);
        return new ProblemHierarchyIterator(objects, filter);
    }
}
