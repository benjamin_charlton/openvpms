/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionListener;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.eftpos.internal.service.EFTPOSServices;
import org.openvpms.eftpos.internal.terminal.TerminalImpl;
import org.openvpms.eftpos.internal.transaction.EFTPOSTransactionFactory;
import org.openvpms.eftpos.internal.transaction.PaymentImpl;
import org.openvpms.eftpos.internal.transaction.RefundImpl;
import org.openvpms.eftpos.service.EFTPOSService;
import org.openvpms.eftpos.service.ManagedTransactionDisplay;
import org.openvpms.eftpos.service.TransactionDisplay;
import org.openvpms.eftpos.service.WebTransactionDisplay;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Refund;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.Transaction.Status;
import org.openvpms.web.component.im.edit.payment.PaymentItemEditor;
import org.openvpms.web.component.im.edit.payment.WebEFTPaymentDialog;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.list.IMObjectListCellRenderer;
import org.openvpms.web.component.im.list.IMObjectListModel;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.Vetoable;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.SelectFieldFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_EFT;


/**
 * An editor for <em>act.customerAccountPaymentEFT</em> and  <em>act.customerAccountRefundEFT</em>.
 *
 * @author Tim Anderson
 */
public class EFTPaymentItemEditor extends PaymentItemEditor {

    /**
     * Placeholder for when no terminal is available.
     */
    private final Entity noTerminal;

    /**
     * The EFTPOS transaction factory.
     */
    private final EFTPOSTransactionFactory transactionFactory;

    /**
     * The location rules.
     */
    private final LocationRules locationRules;

    /**
     * The initial till.
     */
    private final Entity initialTill;

    /**
     * The initial terminal. May be {@code null}
     */
    private final Entity initialTerminal;

    /**
     * The EFTPOS terminal configurations.
     */
    private List<Entity> terminals = Collections.emptyList();

    /**
     * EFTPOS terminal selector.
     */
    private SelectField terminalSelector;

    /**
     * Determines if EFT is complete. If so, the amount and cashout amount should be read-only.
     */
    private boolean approved;

    /**
     * The last selected terminal.
     */
    private Entity lastTerminal;

    /**
     * Determines if there is a prior EFTPOS transaction for the item.
     */
    private boolean hasTransaction;

    /**
     * Constructs {@link EFTPaymentItemEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent act
     * @param context the layout context
     */
    public EFTPaymentItemEditor(FinancialAct act, FinancialAct parent, LayoutContext context) {
        super(act, parent, context);
        IMObjectBean bean = getBean(parent);
        initialTill = (Entity) context.getCache().get(bean.getTargetRef("till"));
        initialTerminal = context.getContext().getTerminal();
        noTerminal = new Entity();
        noTerminal.setName(Messages.get("customer.payment.eft.noterminalname"));
        transactionFactory = ServiceHelper.getBean(EFTPOSTransactionFactory.class);
        locationRules = ServiceHelper.getBean(LocationRules.class);
        updateEFTStatus();
        setTill(initialTill);
    }

    /**
     * Returns the payment amount.
     *
     * @return the payment amount
     */
    public BigDecimal getAmount() {
        return getProperty("amount").getBigDecimal(BigDecimal.ZERO);
    }

    /**
     * Returns the cash out amount.
     *
     * @return the cash out amount
     */
    public BigDecimal getCashout() {
        return getProperty("cashout").getBigDecimal(BigDecimal.ZERO);
    }

    /**
     * Sets the till.
     *
     * @param till the till. May be {@code null}
     */
    public void setTill(Entity till) {
        terminals = (till != null) ? locationRules.getTerminals(till) : Collections.emptyList();
        if (!terminals.isEmpty()) {
            terminals.sort(IMObjectSorter.getNameComparator(true));
        }
        if (approved || terminals.isEmpty()) {
            terminalSelector = null;
        } else {
            Entity terminal = null;
            if (lastTerminal != null && !terminals.contains(lastTerminal)) {
                lastTerminal = null;
            }
            if (lastTerminal != null) {
                terminal = lastTerminal;
            } else if (initialTerminal != null && terminals.contains(initialTerminal)) {
                terminal = initialTerminal;
            } else if (initialTerminal == null && till.equals(initialTill)) {
                // only use No Terminal if the till hasn't changed
                terminal = noTerminal;
            }
            if (terminal == null) {
                // default to the first available
                terminal = terminals.get(0);
            }
            terminalSelector = createTerminalSelector(terminal);
        }
        if (getView().hasComponent()) {
            // need to display the new terminals
            onLayout();
        }
    }

    /**
     * Determines if a payment requires an EFT terminal transaction.
     *
     * @return {@code true} if the payment requires an EFT transaction
     */
    public boolean requiresEFTTransaction() {
        boolean result;
        if (approved) {
            result = false;
        } else {
            // there is no approved transaction. Need a transaction if there is a prior transaction,
            // a terminal has been selected, or terminals are available. If there is a prior transaction
            // but terminals are no longer available, need to record an approved No Terminal transaction
            result = hasTransaction || getTerminal() != null || !terminals.isEmpty();
        }
        return result;
    }

    /**
     * Determines if the editor can be deleted.
     * <p/>
     * The editor can be deleted if there is no EFTPOS transaction in progress, and none has been approved.
     *
     * @return {@code true} if the editor can be deleted
     */
    public boolean canDelete() {
        return getDeletionStatus() == null;
    }

    /**
     * Returns the status of any EFTPOS transaction that would prevent the deletion of this item.
     *
     * @return the status, or {@code null} if the item can be deleted
     */
    public String getDeletionStatus() {
        for (Act act : getBean(getObject()).getTargets("eft", Act.class)) {
            String status = act.getStatus();
            if (!Status.NO_TERMINAL.toString().equals(status) && !Status.DECLINED.toString().equals(status)
                && !Status.ERROR.toString().equals(status)) {
                return status;
            }
        }
        return null;
    }

    /**
     * Performs an EFT transaction.
     *
     * @param listener the listener to notify on success
     */
    public void eft(Runnable listener) {
        Validator validator = new DefaultValidator();
        boolean notifyListener = false;
        if (validate(validator)) {
            try {
                TransactionState state = getTransactionState();
                if (state != null) {
                    if (noTerminalTransactionRequired()) {
                        // the terminal is unavailable, so add a placeholder transaction
                        createNoTerminalTransaction(state.getSequence());
                        notifyListener = true;
                    } else {
                        // need to perform a transaction.
                        Act act = state.getAct();
                        Terminal terminal = getTerminal(state);
                        if (terminal == null) {
                            ErrorHelper.show(EFTHelper.getDialogTitle(getObject()),
                                             Messages.get("customer.payment.eft.noterminal"));
                        } else {
                            EFTPOSService service = ServiceHelper.getBean(EFTPOSServices.class).getService(terminal);
                            if (service.isAvailable(terminal)) {
                                boolean isNew = false;
                                if (act == null) {
                                    // need to create a new EFTPOS transaction
                                    isNew = true;
                                    act = createTransactionAct(terminal, state.getSequence());
                                }
                                performTransaction(act, isNew, service, listener);
                            } else {
                                ErrorHelper.show(EFTHelper.getDialogTitle(getObject()),
                                                 Messages.format("customer.payment.eft.terminalunavailable",
                                                                 terminal.getName()));
                            }
                        }
                    }
                }
            } catch (Exception exception) {
                ErrorHelper.show(EFTHelper.getDialogTitle(getObject()), exception);
            }
        } else {
            ValidationHelper.showError(validator);
        }
        onLayout();
        if (notifyListener) {
            listener.run();
        }
    }

    /**
     * Change the layout.
     */
    @Override
    protected void onLayout() {
        updateEFTStatus();
        super.onLayout();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && !isPostedButRequiresEFT(validator);
    }

    /**
     * Returns the terminal to use to perform an EFT transaction.
     *
     * @return the terminal, or {@code null} if none is selected
     */
    protected Entity getTerminal() {
        Entity result = null;
        if (terminalSelector != null) {
            Entity selected = (Entity) terminalSelector.getSelectedItem();
            result = (selected != noTerminal) ? selected : null;
        }
        return result;
    }

    /**
     * Returns the EFTPOS terminal to use for a transaction.
     *
     * @param state the transaction state
     * @return the EFTPOS terminal. May be {@code null}
     */
    protected Terminal getTerminal(TransactionState state) {
        Entity entity = null;
        if (state.getAct() != null) {
            IMObjectBean bean = getBean(state.getAct());
            entity = bean.getTarget("terminal", Entity.class);
        }
        if (entity == null) {
            entity = getTerminal();
        }
        return entity != null ? new TerminalImpl(entity, getService()) : null;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new EFTPaymentLayoutStrategy() {
            @Override
            public ComponentState apply(IMObject object, PropertySet properties, IMObject parent,
                                        LayoutContext context) {
                if (approved) {
                    MutablePropertySet set = new MutablePropertySet(properties);
                    set.setReadOnly("amount");
                    set.setReadOnly("cashout");
                    properties = set;
                }
                return super.apply(object, properties, parent, context);
            }

            /**
             * Lays out child components in a grid.
             *
             * @param object     the object to lay out
             * @param parent     the parent object. May be {@code null}
             * @param properties the properties
             * @param container  the container to use
             * @param context    the layout context
             */
            @Override
            protected void doSimpleLayout(IMObject object, IMObject parent, List<Property> properties,
                                          Component container, LayoutContext context) {
                Row row = RowFactory.create(Styles.CELL_SPACING);
                super.doSimpleLayout(object, parent, properties, row, context);
                if (terminalSelector != null) {
                    row.add(LabelFactory.create("customer.payment.eft.terminal"));
                    row.add(terminalSelector);
                }
                container.add(row);
            }
        };
    }

    /**
     * Determines if a 'No Terminal' transaction is required.
     * <p/>
     * This could be because:
     * <ul>
     *     <li>the user has selected No Terminal, indicating that an EFTPOS terminal cannot be used</li>
     *     <li>there is a pre-existing transaction and there are no terminals. <br/>
     *     This could occur if a transaction has been declined and the user has changed tills to one with no terminal.
     *     <br/>
     *     In this case, a No Terminal transaction is required to mark the transaction successful.
     *     </li>
     * </ul>
     *
     * @return {@code true} if a 'No Terminal' transaction is required, otherwise {@code false}
     */
    protected boolean noTerminalTransactionRequired() {
        boolean result = false;
        if (terminalSelector != null) {
            Entity selected = (Entity) terminalSelector.getSelectedItem();
            result = selected == noTerminal;
        } else if (hasTransaction && terminals.isEmpty()) {
            // there is a pre-existing transaction, but terminals aren't available
            result = true;
        }
        return result;
    }

    /**
     * Creates an EFTPOS terminal selector.
     *
     * @param defaultTerminal the default terminal
     * @return the selector
     */
    private SelectField createTerminalSelector(Entity defaultTerminal) {
        List<Entity> list = new ArrayList<>(terminals);
        list.add(noTerminal);
        IMObjectListModel model = new IMObjectListModel(list, false, false);
        SelectField field = SelectFieldFactory.create(model);
        field.setCellRenderer(IMObjectListCellRenderer.NAME);
        field.setSelectedItem(defaultTerminal);
        field.addActionListener((ActionListener) e -> {
            Entity selected = (Entity) field.getSelectedItem();
            if (selected == noTerminal) {
                // don't propagate dummy terminal to global context
                selected = null;
            }
            getLayoutContext().getContext().setTerminal(selected);
        });
        return field;
    }

    /**
     * Perform an EFTPOS transaction.
     *
     * @param act      the EFTPOS transaction act
     * @param isNew    if {@code true}, it has not been submitted to the service before
     * @param service  the EFTPOS service
     * @param listener the listener to notify on success
     */
    private void performTransaction(Act act, boolean isNew, EFTPOSService service, Runnable listener) {
        boolean isPayment = act.isA(EFTPOSArchetypes.PAYMENT);
        DomainService domainService = ServiceHelper.getBean(DomainService.class);
        PracticeService practiceService = ServiceHelper.getBean(PracticeService.class);
        TransactionDisplay display;
        Transaction transaction;
        if (isPayment) {
            Payment payment = new PaymentImpl(act, getService(), domainService, practiceService);
            transaction = payment;
            if (isNew) {
                display = service.pay(payment);
            } else {
                display = service.resume(payment);
            }
        } else {
            Refund refund = new RefundImpl(act, getService(), domainService, practiceService);
            transaction = refund;
            if (isNew) {
                display = service.refund(refund);
            } else {
                display = service.resume(refund);
            }
        }
        PopupDialog dialog;
        if (display instanceof WebTransactionDisplay) {
            dialog = new WebEFTPaymentDialog((WebTransactionDisplay) display, getHelpContext());
        } else if (display instanceof ManagedTransactionDisplay) {
            User user = getLayoutContext().getContext().getUser();
            dialog = new EFTPaymentDialog(act, (ManagedTransactionDisplay) display, user,
                                          getLayoutContext().getContext(), getHelpContext());
            dialog.setDefaultCloseAction(PopupDialog.CANCEL_ID);
            dialog.setCancelListener(action -> confirmCloseEFTDialog(transaction, action));
        } else if (display == null) {
            String title = EFTHelper.getDialogTitle(isPayment);
            if (transaction.getStatus() == Status.APPROVED) {
                dialog = new InformationDialog(title, Messages.get("customer.payment.eft.approved"));
            } else if (transaction.getStatus() == Status.DECLINED) {
                dialog = new InformationDialog(title, Messages.get("customer.payment.eft.declined"));
            } else {
                dialog = new ErrorDialog("Unexpected error");
            }
        } else {
            throw new IllegalStateException("EFTPOS service " + service.getClass().getName()
                                            + " returned unsupported transaction display: "
                                            + display.getClass().getName());
        }
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                onLayout();
                if (transaction.getStatus() == Status.APPROVED) {
                    listener.run();
                }
            }
        });
        dialog.show();
    }

    /**
     * Prompts to confirm closure of the EFT dialog if the transaction is incomplete.
     *
     * @param transaction the transaction
     * @param vetoable    the vetoable action
     */
    private void confirmCloseEFTDialog(Transaction transaction, Vetoable vetoable) {
        if (!transaction.isComplete()) {
            ConfirmationDialog.newDialog().title(EFTHelper.getDialogTitle(transaction))
                    .message(Messages.get("customer.payment.eft.incomplete"))
                    .yesNo()
                    .yes(() -> vetoable.veto(false))
                    .defaultListener(s -> vetoable.veto(true))
                    .show();
        } else {
            vetoable.veto(false);
        }
    }

    /**
     * Creates a new EFTPOS transaction act to pass to an EFTPOS service.
     *
     * @param terminal the terminal to use
     * @param sequence the relationship sequence
     * @return a new EFTPOS transaction
     */
    private Act createTransactionAct(Terminal terminal, int sequence) {
        Act act;
        boolean isPayment = getObject().isA(PAYMENT_EFT);
        FinancialAct parent = (FinancialAct) getParent();
        BigDecimal amount = getAmount();
        Party location = getLayoutContext().getContext().getLocation();
        if (isPayment) {
            act = (Act) transactionFactory.createPayment(parent, amount, getCashout(), terminal, location);
        } else {
            act = (Act) transactionFactory.createRefund(parent, amount, terminal, location);
        }
        act.setStatus(EFTPOSTransactionStatus.IN_PROGRESS);
        addTransaction(act, sequence);
        return act;
    }

    /**
     * Creates an EFTPOS transaction that indicates that the EFTPOS terminal(s) were unavailable.
     *
     * @param sequence the transaction sequence
     */
    private void createNoTerminalTransaction(int sequence) {
        Act act;
        boolean isPayment = getObject().isA(PAYMENT_EFT);
        FinancialAct parent = (FinancialAct) getParent();
        BigDecimal amount = getAmount();
        Party location = getLayoutContext().getContext().getLocation();
        if (isPayment) {
            act = (Act) transactionFactory.createNoTerminalPayment(parent, amount, getCashout(), location);
        } else {
            act = (Act) transactionFactory.createNoTerminalRefund(parent, amount, location);
        }
        addTransaction(act, sequence);
    }

    /**
     * Adds an EFTPOS transaction.
     *
     * @param act      the transaction act
     * @param sequence the transaction sequence
     */
    private void addTransaction(Act act, int sequence) {
        IMObjectBean bean = getBean(getObject());
        SequencedRelationship relationship = (SequencedRelationship) bean.addTarget("eft", act, "transaction");
        relationship.setSequence(sequence);
        bean.save(act);
    }

    /**
     * Determines if the parent act is POSTED, but an EFT transaction is required.
     * <p/>
     * The parent act cannot be POSTED until the EFT transaction is APPROVED.
     *
     * @param validator the validator
     * @return {@code true} if the parent act is POSTED but an EFT transaction is required, otherwise {@code false}
     */
    private boolean isPostedButRequiresEFT(Validator validator) {
        boolean result = false;
        Act parent = (Act) getParent();
        if (ActStatus.POSTED.equals(parent.getStatus()) && requiresEFTTransaction()) {
            validator.add(this, Messages.format("customer.payment.eft.postedWithOutstandingEFT",
                                                getDisplayName(parent)));
            result = true;
        }
        return result;
    }

    /**
     * Updates the EFT status.
     * <p/>
     * This retrieves the last EFT transaction, if any, to determine if:
     * <ul>
     *     <li>an EFT transaction is required</li>
     *     <li>fields should be read only</li>
     * </ul>
     */
    private void updateEFTStatus() {
        Act parent = (Act) getParent();
        if (FinancialActStatus.POSTED.equals(parent.getStatus()) && !parent.isNew()) {
            // editing a POSTED payment. Can only occur through administration. No EFT changes are possible,
            // so just flag it as approved
            approved = true;
            lastTerminal= null;
            hasTransaction = false;
        } else {
            Act transaction = getLastTransaction();
            if (transaction != null) {
                Status status = Status.valueOf(transaction.getStatus());
                approved = status == Status.APPROVED || status == Status.NO_TERMINAL;
                IMObjectBean bean = getBean(transaction);
                lastTerminal = bean.getTarget("terminal", Entity.class);
                hasTransaction = true;
            } else {
                approved = false;
                lastTerminal = null;
                hasTransaction = false;
            }
        }
        if (approved) {
            terminalSelector = null;
        }
    }

    /**
     * Returns the current EFTPOS transaction state.
     * <p/>
     * This returns:
     * <ul>
     *  <li>The most recent transaction, if it is incomplete; or</li>
     *  <li>The next sequence, if a new transaction is required; or</li>
     *  <li>No state, if EFT has been performed successfully.</li>
     * </ul>
     *
     * @return the transaction state. May be {@code null}
     */
    private TransactionState getTransactionState() {
        TransactionState result = null;
        IMObjectBean bean = getBean(getObject());
        TransactionState last = getLastTransaction(bean);
        int sequence = 0;
        if (last != null) {
            Act transaction = last.getAct();
            sequence = last.getSequence();
            Status status = Status.valueOf(transaction.getStatus());
            if (status == Status.ERROR || status == Status.DECLINED) {
                result = new TransactionState(null, ++sequence);
            } else if (status != Status.APPROVED && status != Status.NO_TERMINAL) {
                result = last;
            }
        } else {
            result = new TransactionState(null, ++sequence);
        }
        return result;
    }

    /**
     * Returns the last EFT transaction.
     *
     * @return the last EFT transaction, or {@code null} if none exists
     */
    private Act getLastTransaction() {
        TransactionState lastTransaction = getLastTransaction(getBean(getObject()));
        return lastTransaction != null ? lastTransaction.getAct() : null;
    }

    /**
     * Returns the last EFT transaction and its relationship sequence.
     *
     * @return the transaction state, or {@code null} if none exists
     */
    private TransactionState getLastTransaction(IMObjectBean bean) {
        int sequence = 0;
        Reference last = null;
        for (SequencedRelationship relationship : bean.getValues("eft", SequencedRelationship.class)) {
            if (relationship.getSequence() >= sequence) {
                last = relationship.getTarget();
                sequence = relationship.getSequence();
            }
        }

        Act transaction = null;
        if (last != null) {
            transaction = bean.getObject(last, Act.class);
        }
        return transaction != null ? new TransactionState(transaction, sequence) : null;
    }

    private static class TransactionState {

        private final Act act;

        private final int sequence;

        public TransactionState(Act act, int sequence) {
            this.act = act;
            this.sequence = sequence;
        }

        public Act getAct() {
            return act;
        }

        public int getSequence() {
            return sequence;
        }
    }

}
