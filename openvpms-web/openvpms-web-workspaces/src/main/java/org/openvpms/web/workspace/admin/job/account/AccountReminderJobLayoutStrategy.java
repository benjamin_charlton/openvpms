/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.account;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.PropertySet;

import static org.openvpms.web.workspace.admin.job.account.AccountReminderJobEditor.IGNORE_INTERVAL;
import static org.openvpms.web.workspace.admin.job.account.AccountReminderJobEditor.IGNORE_INTERVAL_UNITS;

/**
 * Layout strategy for <em>entity.jobAccountReminder</em>.
 *
 * @author Tim Anderson
 */
public class AccountReminderJobLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes nodes = ArchetypeNodes.all().exclude(IGNORE_INTERVAL_UNITS);

    /**
     * Constructs an {@link AccountReminderJobLayoutStrategy}.
     */
    public AccountReminderJobLayoutStrategy() {
        super(nodes);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        addComponent(createComponentPair(IGNORE_INTERVAL, IGNORE_INTERVAL_UNITS, object, properties, context));
        return super.apply(object, properties, parent, context);
    }
}
