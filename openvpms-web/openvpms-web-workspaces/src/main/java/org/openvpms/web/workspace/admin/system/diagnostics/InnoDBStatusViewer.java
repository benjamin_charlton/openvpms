/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Displays the InnoDB status using the MySQL {@code SHOW ENGINE INNODB STATUS} command.
 *
 * @author Tim Anderson
 */
class InnoDBStatusViewer extends AbstractDiagnosticTab {

    /**
     * A snapshot of the InnoDB status.
     */
    private String snapshot;

    InnoDBStatusViewer() {
        super("admin.system.diagnostic.innodb");
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        String data;
        try {
            data = getData(false);
        } catch (Throwable exception) {
            data = formatError(exception);
        }
        if (data != null) {
            result = toText("innodb.txt", data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component result;
        try {
            String data = getData(true);
            if (data != null) {
                result = LabelFactory.preformatted(data);
            } else {
                result = LabelFactory.create();
            }
        } catch (Throwable exception) {
            result = LabelFactory.text(formatError(exception));
        }
        return result;
    }

    /**
     * Returns the MySQL InnoDB status.
     *
     * @param refresh if {@code true}, refresh the data if it has been collected previously
     * @return the process data, or {@code null} if it cannot be retrieved
     * @throws SQLException for any SQL error
     */
    private String getData(boolean refresh) throws SQLException {
        if (snapshot == null || refresh) {
            snapshot = null;
            DataSource dataSource = ServiceHelper.getBean(DataSource.class);

            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {

                java.sql.ResultSet rs = statement.executeQuery("SHOW ENGINE INNODB STATUS");
                if (rs.next()) {
                    snapshot = rs.getString("status");
                }
            }
        }
        return snapshot;
    }

    /**
     * Formats an error.
     *
     * @param exception the exception
     * @return the formatted error
     */
    private String formatError(Throwable exception) {
        return Messages.format("admin.system.diagnostic.innodb.error", ErrorFormatter.format(exception));
    }

}
