/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.JoinConstraint;
import org.openvpms.component.system.common.query.ParticipationConstraint;
import org.openvpms.web.component.im.query.QueryHelper;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;

import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.ActShortName;


/**
 * Helper for task queries.
 *
 * @author Tim Anderson
 */
class TaskQueryHelper {

    /**
     * Determines there are too many outstanding tasks for a worklist associated with an act.
     *
     * @param act the act. An instance of <em>act.customerTask</em> with a non-null start date.
     * @return {@code true} if there are too many outstanding tasks; otherwise {@code false}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static boolean tooManyTasks(Act act) {
        boolean result = false;
        IArchetypeService service = ServiceHelper.getArchetypeService();
        IMObjectBean actBean = service.getBean(act);
        Date startTime = act.getActivityStartTime();
        Party workList = actBean.getTarget("worklist", Party.class);
        if (startTime != null && workList != null) {
            Date endTime = act.getActivityEndTime();
            IMObjectBean bean = service.getBean(workList);
            int maxSlots = bean.getInt("maxSlots");
            ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.TASK, false, true);
            JoinConstraint join = Constraints.join("worklist");
            join.add(Constraints.eq("entity", workList));

            // duplicate the archetype and date range on the participation to encourage MySQL to use the correct index
            join.add(new ParticipationConstraint(ActShortName, ScheduleArchetypes.TASK));
            join.add(QueryHelper.createParticipationDateRangeConstraint(startTime, endTime));
            query.add(join);

            query.add(Constraints.ne("id", act.getId()));
            query.add(Constraints.and(Constraints.ne("status", FinancialActStatus.CANCELLED),
                                      Constraints.ne("status", FinancialActStatus.COMPLETED)));
            query.add(QueryHelper.createDateRangeConstraint(startTime, endTime));
            query.setFirstResult(0);
            query.setMaxResults(0);  // forces count only
            query.setCountResults(true);
            IPage<IMObject> page = service.get(query);
            int totalResults = page.getTotalResults();
            if (totalResults >= maxSlots) {
                result = true;
            }
        }
        return result;
    }

}
