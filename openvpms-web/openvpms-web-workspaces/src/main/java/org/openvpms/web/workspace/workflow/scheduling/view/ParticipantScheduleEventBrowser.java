/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.scheduling.view;

import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextSwitchListener;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.BrowserListener;
import org.openvpms.web.component.im.query.IMObjectTableBrowser;
import org.openvpms.web.component.im.table.IMTableModel;

/**
 * A browser for schedule events returned by a {@link ParticipantScheduleEventQuery} that jumps to the event in
 * the appropriate workspace when selected.
 *
 * @author Tim Anderson
 */
public abstract class ParticipantScheduleEventBrowser extends IMObjectTableBrowser<Act> {

    /**
     * Constructs a {@link ParticipantScheduleEventBrowser}.
     *
     * @param query   the event query
     * @param model   the table model
     * @param context the layout context
     */
    public ParticipantScheduleEventBrowser(ParticipantScheduleEventQuery query, IMTableModel<Act> model,
                                           LayoutContext context) {
        super(query, model, context);
        addBrowserListener(new BrowserListener<Act>() {
            public void selected(Act object) {
                onEventSelected(object, context);
            }

            public void query() {
                // no-op
            }

            public void browsed(Act object) {
                // no-op
            }
        });
    }

    /**
     * Switches to the appropriate workspace when an event is selected, but only if the event is for the specified
     * location.
     *
     * @param event    the event
     * @param location the practice location
     * @param listener the context switch listener
     */
    protected abstract void switchTo(Act event, Party location, ContextSwitchListener listener);

    /**
     * Invoked when an event is selected.
     * <p>
     * If the event can be viewed at the current practice location, the appropriate workspace will be switched to.
     *
     * @param event         the event
     * @param layoutContext the layout context
     */
    private void onEventSelected(Act event, LayoutContext layoutContext) {
        Context context = layoutContext.getContext();
        ContextSwitchListener listener = layoutContext.getContextSwitchListener();
        Party location = context.getLocation();
        if (location != null && listener != null) {
            switchTo(event, location, listener);
        }
    }
}