/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory.io;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.product.PricingGroup;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.laboratory.internal.io.LaboratoryTestData;
import org.openvpms.laboratory.internal.io.LaboratoryTestDataMatcher;
import org.openvpms.laboratory.internal.io.LaboratoryTestDataSet;
import org.openvpms.laboratory.internal.io.LaboratoryTestDataUpdater;
import org.openvpms.laboratory.internal.io.LaboratoryTestProductData;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.MapBasedMarkablePagedIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.openvpms.web.echo.style.Styles.CELL_SPACING;
import static org.openvpms.web.echo.style.Styles.H3;
import static org.openvpms.web.echo.style.Styles.WIDE_CELL_SPACING;

/**
 * Laboratory test import dialog.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestImportDialog extends PopupDialog {

    /**
     * The context.
     */
    private final Context context;

    /**
     * Container for the pages.
     */
    private final Column container;

    /**
     * Investigation type display name.
     */
    private final String investigationTypeDisplayName;

    /**
     * The laboratory test data matcher.
     */
    private final LaboratoryTestDataMatcher matcher;

    /**
     * The location rules.
     */
    private final LocationRules locationRules;

    /**
     * The layout context.
     */
    private final LayoutContext layoutContext;

    /**
     * The imported laboratory test data.
     */
    private LaboratoryTestDataSet data;

    /**
     * The products selected for saving.
     */
    private Map<Product, LaboratoryTestProductData> selected;

    /**
     * Determines if laboratory test data has been saved.
     */
    private boolean testDataSaved = false;

    /**
     * The product data.
     */
    private List<LaboratoryTestProductData> productData;

    /**
     * Previous button id.
     */
    private static final String PREVIOUS_ID = "button.previous";

    /**
     * Next button id.
     */
    private static final String NEXT_ID = "button.next";

    /**
     * The buttons.
     */
    private static final String[] BUTTONS = {PREVIOUS_ID, NEXT_ID, OK_ID, CANCEL_ID};

    /**
     * Constructs a {@link LaboratoryTestImportDialog}.
     *
     * @param data the data to import
     * @param help the help context
     */
    public LaboratoryTestImportDialog(LaboratoryTestDataSet data, Context context, HelpContext help) {
        super(Messages.get("admin.laboratory.import.title"), "BrowserDialog", BUTTONS, help);
        setModal(true);
        this.data = data;
        this.context = context;
        layoutContext = new DefaultLayoutContext(context, help);
        container = ColumnFactory.create(CELL_SPACING);
        locationRules = ServiceHelper.getBean(LocationRules.class);
        ArchetypeService service = ServiceHelper.getArchetypeService();
        investigationTypeDisplayName = DescriptorHelper.getDisplayName(LaboratoryArchetypes.INVESTIGATION_TYPE,
                                                                       service);
        matcher = ServiceHelper.getBean(LaboratoryTestDataMatcher.class);
        ButtonSet buttons = getButtons();
        buttons.setEnabled(PREVIOUS_ID, false);
        buttons.setEnabled(OK_ID, false);
        setCancelListener(action -> {
            if (selected != null && !selected.isEmpty()) {
                ConfirmationDialog.newDialog().titleKey("admin.laboratory.import.title")
                        .messageKey("admin.laboratory.import.cancelImport")
                        .yesNo()
                        .yes(() -> action.veto(false))
                        .no(() -> action.veto(true))
                        .show();
            } else {
                action.veto(false);
            }
        });
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        getLayout().add(container);
        showTests();
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (PREVIOUS_ID.equals(button)) {
            onPrevious();
        } else if (NEXT_ID.equals(button)) {
            onNext();
        } else {
            super.onButton(button);
        }
    }

    /**
     * Invoked when the 'OK' button is pressed. This sets the action and closes
     * the window.
     */
    @Override
    protected void onOK() {
        if (saveProducts()) {
            super.onOK();
        }
    }

    /**
     * Invoked when the 'Previous' button is pressed.<br/>
     * Displays the tests.
     */
    private void onPrevious() {
        showTests();
    }

    /**
     * Invoked when the 'Next' button is pressed.<br/>
     * Prompts to confirm importing tests data if required, before displaying products.
     */
    private void onNext() {
        if (!testDataSaved) {
            confirmSaveTestData();
        } else {
            showProducts();
        }
    }

    /**
     * Updates the selected investigation type.
     *
     * @param investigationType the investigation type
     */
    private void onInvestigationTypeSelected(Entity investigationType) {
        data = new LaboratoryTestDataSet(data.getData(), data.getErrors(), investigationType);
    }

    /**
     * Displays the imported tests.
     */
    private void showTests() {
        Column column = ColumnFactory.create(WIDE_CELL_SPACING,
                                             LabelFactory.create("admin.laboratory.import.newAndUpdatedTests", H3));
        Label label = LabelFactory.text(investigationTypeDisplayName, Styles.BOLD);
        if (data.getInvestigationType() != null) {
            // an investigation type has already been selected, so make it read-only
            column.add(RowFactory.create(CELL_SPACING, label,
                                         LabelFactory.text(data.getInvestigationType().getName())));
        } else {
            // the user must select an investigation type
            Entity investigationType = (Entity) data.getInvestigationType();

            Property property = SimpleProperty.newProperty()
                    .name("investigationType")
                    .type(Reference.class)
                    .displayName(investigationTypeDisplayName)
                    .archetypeRange(LaboratoryArchetypes.INVESTIGATION_TYPE)
                    .build();

            RestrictedInvestigationTypeReferenceEditor editor
                    = new RestrictedInvestigationTypeReferenceEditor(property, layoutContext);
            editor.setObject(investigationType);
            column.add(RowFactory.create(CELL_SPACING, label, editor.getComponent()));
            property.addModifiableListener(modifiable -> {
                Entity object = editor.getObject();
                onInvestigationTypeSelected(object);
            });
        }
        ResultSet<LaboratoryTestData> resultSet = new ListResultSet<>(data.getData(), 20);
        LaboratoryTestDataModel model = new LaboratoryTestDataModel();
        PagedIMTable<LaboratoryTestData> table = new PagedIMTable<>(model, resultSet);
        column.add(table.getComponent());
        container.removeAll();
        container.add(ColumnFactory.create(Styles.INSET, column));
        enableButtons(true);
    }

    /**
     * Prompts to save test data, if it is valid, before navigating to the next page.
     */
    private void confirmSaveTestData() {
        if (data.getInvestigationType() == null) {
            ErrorDialog.newDialog()
                    .messageKey("property.error.required", investigationTypeDisplayName)
                    .show();
        } else {
            ConfirmationDialog.newDialog().titleKey("admin.laboratory.import.title")
                    .messageKey("admin.laboratory.import.save")
                    .yesNo()
                    .yes(() -> {
                        saveTestData();
                        showProducts();
                    })
                    .show();
        }
    }

    /**
     * Saves test data.
     */
    private void saveTestData() {
        LaboratoryTestDataUpdater updater = ServiceHelper.getBean(LaboratoryTestDataUpdater.class);
        data = updater.update(data);
        testDataSaved = true;
    }

    /**
     * Shows products affected by the imported test prices.
     */
    private void showProducts() {
        List<LaboratoryTestProductData> data = getProductData();
        Column column = ColumnFactory.create(WIDE_CELL_SPACING,
                                             LabelFactory.create("admin.laboratory.import.updatePrices", H3));
        enableButtons(false);
        if (data.isEmpty()) {
            column.add(LabelFactory.create("admin.laboratory.import.noProducts", Styles.BOLD));
        } else {
            column.add(LabelFactory.create("admin.laboratory.import.selectProducts"));
            ResultSet<LaboratoryTestProductData> resultSet = new ListResultSet<>(data, 20);
            LaboratoryTestProductDataModel model = new LaboratoryTestProductDataModel(layoutContext);
            MapBasedMarkablePagedIMTableModel<Product, LaboratoryTestProductData> pagedModel
                    = new MapBasedMarkablePagedIMTableModel<>(model, selected,
                                                              LaboratoryTestProductData::getNewProduct);
            PagedIMTable<LaboratoryTestProductData> table = new PagedIMTable<>(pagedModel, resultSet);
            column.add(table.getComponent());
        }
        container.removeAll();
        container.add(ColumnFactory.create(Styles.INSET, column));
        enableButtons(false);
    }

    /**
     * Saves products.
     *
     * @return {@code true} if the save was successful, otherwise {@code false}
     */
    private boolean saveProducts() {
        boolean result = true;
        if (selected != null && !selected.isEmpty()) {
            if (!SaveHelper.save(selected.keySet())) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Returns the product data of products affected by test price changes.
     *
     * @return the product data
     */
    private List<LaboratoryTestProductData> getProductData() {
        if (productData == null) {
            PricingGroup pricingGroup = new PricingGroup(locationRules.getPricingGroup(context.getLocation()));
            productData = matcher.matchProducts(data.getData(), new Date());
            // preselect products whose cost has increased
            selected = productData.stream()
                    .filter(LaboratoryTestProductData::costIncreased)
                    .collect(Collectors.toMap(LaboratoryTestProductData::getNewProduct,
                                              Function.identity()));
        }
        return productData;
    }

    /**
     * Enables/disables buttons if this is the first or second page.
     *
     * @param first if {@code true}, it's the first page, else its the second page
     */
    private void enableButtons(boolean first) {
        ButtonSet buttons = getButtons();
        buttons.setEnabled(PREVIOUS_ID, !first);
        buttons.setEnabled(NEXT_ID, first);
        buttons.setEnabled(OK_ID, !first);
    }
}
