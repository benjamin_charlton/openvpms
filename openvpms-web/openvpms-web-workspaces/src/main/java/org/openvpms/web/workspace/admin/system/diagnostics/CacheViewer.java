/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.table.DefaultTableModel;
import nextapp.echo2.app.table.TableCellRenderer;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.DefaultTableHeaderRenderer;
import org.openvpms.web.echo.table.KeyTable;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.workspace.admin.system.cache.CacheState;
import org.openvpms.web.workspace.admin.system.cache.Caches;

import java.util.List;

/**
 * Cache viewer.
 *
 * @author Tim Anderson
 */
class CacheViewer extends AbstractDiagnosticTab {

    /**
     * The caches.
     */
    private final Caches caches;

    /**
     * The column names.
     */
    private final String[] columns;

    /**
     * The data snapshot.
     */
    private String[][] snapshot;

    /**
     * The data table.
     */
    private KeyTable table;

    /**
     * Consructs a {@link CacheViewer}.
     */
    CacheViewer() {
        super("admin.system.diagnostic.cache");
        caches = new Caches();
        columns = new String[]{Messages.get("table.imobject.name"),
                               Messages.get("admin.system.cache.count"),
                               Messages.get("admin.system.cache.maxcount"),
                               Messages.get("admin.system.cache.edit.suggested"),
                               Messages.get("admin.system.cache.use"),
                               Messages.get("admin.system.cache.hits"),
                               Messages.get("admin.system.cache.misses"),
                               Messages.get("admin.system.cache.size")};
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        String[][] data = getData(false, false);
        if (data != null) {
            result = toCSV("caches.csv", columns, data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component result = null;
        String[][] data = getData(true, false);
        if (data != null) {
            table = new KeyTable();
            table.setStyleName(Styles.DEFAULT);
            table.setDefaultHeaderRenderer(DefaultTableHeaderRenderer.DEFAULT);
            table.setHeaderFixed(true);
            populateTable(data);
            Button calculate = ButtonFactory.create("button.calculatesize", new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    calculateSize();
                }
            });
            calculate.setLayoutData(RowFactory.layout(Alignment.ALIGN_TOP));
            result = RowFactory.create(Styles.CELL_SPACING, table, calculate);
        }
        return result;
    }

    /**
     * Populates the table with data.
     *
     * @param data the data
     */
    private void populateTable(String[][] data) {
        table.setModel(new DefaultTableModel(data, columns));
        TableCellRenderer align = (t, object, column, row) -> {
            String s = (object != null) ? object.toString() : null;
            if (s != null) {
                if ("-".equals(s)) {
                    return TableHelper.centreAlign(s);
                }
                return TableHelper.rightAlign(s);
            }
            return new Label();
        };
        TableColumnModel columnModel = table.getColumnModel();
        for (int i = 1; i < columns.length; ++i) {
            columnModel.getColumn(i).setCellRenderer(align);
        }
    }

    /**
     * Calculates the size for each cache, updating the table.
     */
    private void calculateSize() {
        String[][] data = getData(true, true);
        if (data != null) {
            populateTable(data);
        }
    }

    /**
     * Returns the data.
     *
     * @param refresh   if {@code true}, refresh the data if it has been collected previously
     * @param calculate if {@code true}, calculate cache sizes
     * @return the cache data
     */
    private String[][] getData(boolean refresh, boolean calculate) {
        if (snapshot == null || refresh) {
            List<CacheState> states = caches.getCaches();
            snapshot = new String[states.size()][];
            for (int i = 0; i < snapshot.length; ++i) {
                CacheState state = states.get(i);
                if (refresh) {
                    state.refreshStatistics();
                }
                if (calculate) {
                    state.refreshCacheSize();
                }
                String size = (state.getSize() != -1) ? NumberFormatter.getSize(state.getSize()) : "-";
                String[] data = {state.getDisplayName(),
                                 Long.toString(state.getCount()),
                                 Long.toString(state.getMaxCount()),
                                 Long.toString(caches.getSuggestedSize(state)),
                                 state.getUse() + "%",
                                 Long.toString(state.getHits()),
                                 Long.toString(state.getMisses()),
                                 size};
                snapshot[i] = data;
            }
        }
        return snapshot;
    }
}
