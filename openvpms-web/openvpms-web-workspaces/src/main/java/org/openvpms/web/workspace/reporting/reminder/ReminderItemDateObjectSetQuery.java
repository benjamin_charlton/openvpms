/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import echopointng.DateField;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderItemQueryFactory;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.bound.BoundCheckBox;
import org.openvpms.web.component.bound.BoundDateFieldFactory;
import org.openvpms.web.component.im.query.QueryState;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.util.ComponentHelper;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Date;

/**
 * Queries <em>act.patientReminderItem*</em> archetypes.
 *
 * @author Tim Anderson
 */
public class ReminderItemDateObjectSetQuery extends ReminderItemObjectSetQuery {

    /**
     * The 'all dates' checkbox.
     */
    private final SimpleProperty all;

    /**
     * The date.
     */
    private final SimpleProperty date;

    /**
     * Date change listener.
     */
    private final ModifiableListener dateListener;

    /**
     * The date label.
     */
    private final Label dateLabel;

    /**
     * The date field.
     */
    private final DateField dateField;

    /**
     * The 'all dates' listener.
     */
    private ModifiableListener allListener;

    /**
     * Constructs a {@link ReminderItemDateObjectSetQuery}.
     *
     * @param status  the reminder item status to query
     * @param context the context
     */
    public ReminderItemDateObjectSetQuery(String status, Context context) {
        this(status, false, context);
    }

    /**
     * Constructs a {@link ReminderItemDateObjectSetQuery}.
     *
     * @param status  the reminder item status to query
     * @param all     if {@code true}, query all dates
     * @param context the context
     */
    public ReminderItemDateObjectSetQuery(String status, boolean all, Context context) {
        super(status, context);
        date = new SimpleProperty("date", null, Date.class,
                                  DescriptorHelper.getDisplayName(ReminderArchetypes.PRINT_REMINDER, "startTime",
                                                                  getService()));
        date.setValue(DateRules.getToday());
        dateLabel = LabelFactory.create();
        dateLabel.setText(date.getDisplayName());
        dateField = BoundDateFieldFactory.create(date);
        if (all) {
            this.all = new SimpleProperty("all", true, Date.class, Messages.get("daterange.all"));
            allListener = modifiable -> onAllChanged();
            this.all.addModifiableListener(allListener);
            enableDate();
        } else {
            this.all = null;
        }
        dateListener = modifiable -> onQuery();
        date.addModifiableListener(dateListener);
    }

    /**
     * Returns the query state.
     *
     * @return the query state
     */
    @Override
    public QueryState getQueryState() {
        return new Memento(this);
    }

    /**
     * Sets the query state.
     *
     * @param state the query state
     */
    @Override
    public void setQueryState(QueryState state) {
        super.setQueryState(state);
        if (state instanceof Memento) {
            Memento memento = (Memento) state;
            setDate(memento.date);
            setAll(memento.all);
        }
    }

    /**
     * Creates the result set.
     *
     * @param sort the sort criteria. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<ObjectSet> createResultSet(SortConstraint[] sort) {
        if (all == null) {
            Date to = date.getDate();
            if (to == null) {
                to = DateRules.getToday();
                setDate(to);
            }
        }
        return super.createResultSet(sort);
    }

    /**
     * Lays out the component in a container, and sets focus on the instance
     * name.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        super.doLayout(container);
        if (all != null) {
            Label allLabel = LabelFactory.create();
            allLabel.setText(all.getDisplayName());
            container.add(allLabel);
            container.add(new BoundCheckBox(all));
        }
        container.add(dateLabel);
        container.add(dateField);
        addLocationSelector(container);
    }

    /**
     * Populates the query factory.
     *
     * @param factory the factory
     */
    @Override
    protected void populate(ReminderItemQueryFactory factory) {
        super.populate(factory);
        factory.setFrom(null);
        if (all != null && all.getBoolean()) {
            factory.setTo(null);
        } else {
            Date to = date.getDate();
            factory.setTo(DateRules.getNextDate(to));
        }
    }

    /**
     * Sets the date.
     *
     * @param date the date
     */
    private void setDate(Date date) {
        this.date.removeModifiableListener(dateListener);
        this.date.setValue(date);
        this.date.addModifiableListener(dateListener);
    }

    /**
     * Sets the 'all dates' checkbox.
     *
     * @param all if {@code true} select all dates else restrict to a single date
     */
    private void setAll(boolean all) {
        if (this.all != null) {
            this.all.removeModifiableListener(allListener);
            this.all.setValue(all);
            this.all.addModifiableListener(allListener);
        }
    }

    /**
     * Invoked when the All checkbox changes.
     */
    private void onAllChanged() {
        enableDate();
        onQuery();
    }

    /**
     * Enables the date field if All is unticked, otherwise disables it.
     */
    private void enableDate() {
        boolean enable = all != null && !all.getBoolean();
        ComponentHelper.enable(dateLabel, enable);
        ComponentHelper.enable(dateField, enable);
    }

    private static class Memento extends AbstractMemento {

        private final Date date;

        private boolean all;

        /**
         * Constructs a {@link Memento}.
         *
         * @param query the query
         */
        public Memento(ReminderItemDateObjectSetQuery query) {
            super(query);
            this.date = query.date.getDate();
            this.all = (query.all != null) && query.all.getBoolean();
        }
    }
}
