/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.relationship.RelationshipHelper;
import org.openvpms.web.component.im.table.BaseIMObjectTableModel;
import org.openvpms.web.echo.table.TableColumnFactory;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.ACCOUNT_ALLOCATION_RELATIONSHIP;

/**
 * Table to display customer account allocations.
 *
 * @author Tim Anderson
 */
public class AccountAllocationRelationshipTableModel extends BaseIMObjectTableModel<IMObject> {

    /**
     * The parent act.
     */
    private final Reference parent;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The acts, keyed on their relationships.
     */
    private final Map<IMObject, FinancialAct> acts = new HashMap<>();

    /**
     * Date column index.
     */
    private static final int DATE_INDEX = NEXT_INDEX;

    /**
     * Amount column index.
     */
    private static final int ALLOCATED_AMOUNT_INDEX = DATE_INDEX + 1;

    /**
     * The allocated amount node.
     */
    private static final String ALLOCATED_AMOUNT = "allocatedAmount";

    /**
     * Constructs an {@link AccountAllocationRelationshipTableModel}.
     *
     * @param parent the parent act
     */
    public AccountAllocationRelationshipTableModel(FinancialAct parent) {
        super(null);
        this.parent = parent.getObjectReference();
        service = ServiceHelper.getArchetypeService();
        setTableColumnModel(createTableColumnModel());
    }


    /**
     * Sets the objects to display.
     *
     * @param objects the objects to display
     */
    public void setObjects(List<IMObject> objects) {
        super.setObjects(objects);
        acts.clear();

        for (IMObject object : objects) {
            Reference reference = RelationshipHelper.getRelated(parent, (ActRelationship) object);
            FinancialAct other = service.get(reference, FinancialAct.class);
            acts.put(object, other);
        }
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the column
     * @param row    the row
     * @return the value at the given coordinate
     */
    @Override
    protected Object getValue(IMObject object, TableColumn column, int row) {
        Object result = null;
        if (column.getModelIndex() == ID_INDEX || column.getModelIndex() == ARCHETYPE_INDEX) {
            object = acts.get(object);
            result = (object != null) ? super.getValue(object, column, row) : null;
        } else if (column.getModelIndex() == DATE_INDEX) {
            Act act = acts.get(object);
            Date date = (act != null) ? act.getActivityStartTime() : null;
            if (date != null) {
                result = DateFormatter.formatDate(date, false);
            }
        } else if (column.getModelIndex() == ALLOCATED_AMOUNT_INDEX) {
            BigDecimal amount = service.getBean(object).getBigDecimal(ALLOCATED_AMOUNT, BigDecimal.ZERO);
            result = TableHelper.rightAlign(NumberFormatter.format(amount));
        }
        return result;
    }

    /**
     * Creates a new column model.
     *
     * @return a new column model
     */
    @Override
    protected TableColumnModel createTableColumnModel() {
        TableColumnModel model = new DefaultTableColumnModel();
        model.addColumn(createTableColumn(ID_INDEX, "table.imobject.id"));
        model.addColumn(createTableColumn(DATE_INDEX, "table.act.date"));
        model.addColumn(createTableColumn(ARCHETYPE_INDEX, "table.act.type"));
        String allocatedAmount = DescriptorHelper.getDisplayName(ACCOUNT_ALLOCATION_RELATIONSHIP, ALLOCATED_AMOUNT,
                                                                 service);
        model.addColumn(TableColumnFactory.create(ALLOCATED_AMOUNT_INDEX, allocatedAmount));
        return model;
    }
}