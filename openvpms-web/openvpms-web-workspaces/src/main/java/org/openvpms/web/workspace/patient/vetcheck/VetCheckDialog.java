/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.vetcheck;

import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.retry.Retryable;
import org.openvpms.web.component.retry.Retryer;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.frame.MonitoredFrame;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * VetCheck dialog.
 *
 * @author Tim Anderson
 */
public class VetCheckDialog extends AbstractVetCheckDialog {

    /**
     * The VetCheck rules.
     */
    private final VetCheckRules rules;

    /**
     * The patient.
     */
    private final Party patient;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VetCheckDialog.class);

    /**
     * Constructs a {@link VetCheckDialog}.
     *
     * @param patient the patient
     * @param context the context
     * @param help    the help context
     */
    public VetCheckDialog(Party patient, Context context, HelpContext help) {
        super(help);
        this.rules = ServiceHelper.getBean(VetCheckRules.class);
        this.patient = patient;
        this.context = context;
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        super.doLayout();
        String uri = rules.getURL(patient);
        MonitoredFrame frame = new MonitoredFrame(uri);
        frame.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                addLink(event.getActionCommand());
            }
        });
        getLayout().add(frame);
    }

    /**
     * Adds a link to patient history.
     *
     * @param data the VetCheck link data
     */
    private void addLink(String data) {
        MutableObject<VetCheckLink> holder = new MutableObject<>();
        Retryable action = () -> {
            VetCheckLink link = rules.addLink(patient, UserHelper.getClinician(context), data);
            holder.setValue(link);
            return true;
        };
        if (Retryer.run(action)) {
            VetCheckLink link = holder.getValue();
            if (link != null) {
                log.info("Added link for patient id={}, name={}, link={}", patient.getId(), patient.getName(),
                         link.getUrl());
                InformationDialog dialog = InformationDialog.newDialog()
                        .message(Messages.format("vetcheckdialog.link.success", link.getDescription()))
                        .style("InformationMessage")
                        .sizeToContent()
                        .position(10, 10)
                        .build();
                getContentPane().add(dialog); // make the popup relative to the VetCheck window
                dialog.show(3);
            } else {
                log.info("Failed to add link for patient id={}, name={}. Link not recognised: {}", patient.getId(),
                         patient.getName(), data);
                ErrorDialog.newDialog()
                        .title(Messages.get("vetcheckdialog.title"))
                        .message(Messages.get("vetcheckdialog.link.error"))
                        .show();
            }
        } else {
            // will have displayed an error dialog already
            log.error("Failed to add link for patient id={}, name={}, shareLink={}", patient.getId(),
                      patient.getName(), data);
        }
    }
}