/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Row;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentService;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleTimes;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.alert.Alert;
import org.openvpms.web.component.alert.AlertManager;
import org.openvpms.web.component.bound.BoundCheckBox;
import org.openvpms.web.component.bound.BoundDateTimeField;
import org.openvpms.web.component.bound.BoundDateTimeFieldFactory;
import org.openvpms.web.component.edit.AlertListener;
import org.openvpms.web.component.im.clinician.ClinicianParticipationEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.sms.SMSHelper;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusHelper;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.alert.AlertSummary;
import org.openvpms.web.workspace.workflow.appointment.repeat.AppointmentSeries;
import org.openvpms.web.workspace.workflow.appointment.repeat.ScheduleEventSeries;

import java.util.Date;
import java.util.List;

import static org.openvpms.archetype.rules.workflow.ScheduleEvent.REMINDER_ERROR;
import static org.openvpms.archetype.rules.workflow.ScheduleEvent.REMINDER_SENT;
import static org.openvpms.archetype.rules.workflow.ScheduleEvent.SEND_REMINDER;
import static org.openvpms.web.echo.style.Styles.CELL_SPACING;
import static org.openvpms.web.echo.style.Styles.INSET;


/**
 * An editor for <em>act.customerAppointment</em>s.
 *
 * @author Tim Anderson
 */
public class AppointmentEditor extends AbstractCalendarEventEditor {

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * Determines if SMS is enabled at the practice.
     */
    private final boolean smsPractice;

    /**
     * Determines if the sendReminder checkbox should be enabled.
     */
    private final Period noReminder;

    /**
     * The send reminder flag.
     */
    private final BoundCheckBox sendReminder;

    /**
     * Determines if the customer and patient should be read-only.
     */
    private final boolean customerPatientReadOnly;

    /**
     * The alerts row.
     */
    private Row alerts;

    /**
     * Determines if reminders are enabled on the schedule.
     */
    private boolean scheduleReminders;

    /**
     * Determines if reminders are enabled on the appointment type.
     */
    private boolean appointmentTypeReminders;

    /**
     * The roster alert identifier, used to cancel any existing alert.
     */
    private String rosterAlert;

    /**
     * The online booking notes.
     */
    private static final String BOOKING_NOTES = ScheduleEvent.BOOKING_NOTES;

    /**
     * The appointment type node name.
     */
    private static final String APPOINTMENT_TYPE = "appointmentType";

    /**
     * The clinician node name.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * The practice location node name.
     */
    private static final String LOCATION = "location";

    /**
     * The status node name.
     */
    private static final String STATUS = "status";

    /**
     * The confirmed node name.
     */
    private static final String CONFIRMED_TIME = "confirmedTime";

    /**
     * The arrival time node name.
     */
    private static final String ARRIVAL_TIME = "arrivalTime";

    /**
     * The reason node name.
     */
    private static final String REASON = "reason";

    /**
     * Constructs an {@link AppointmentEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public AppointmentEditor(Act act, IMObject parent, LayoutContext context) {
        this(act, parent, false, context);
    }

    /**
     * Constructs an {@link AppointmentEditor}.
     *
     * @param act        the act to edit
     * @param parent     the parent object. May be {@code null}
     * @param editSeries if {@code true}, edit the series
     * @param context    the layout context
     */
    public AppointmentEditor(Act act, IMObject parent, boolean editSeries, LayoutContext context) {
        super(act, parent, editSeries, context);
        rules = ServiceHelper.getBean(AppointmentRules.class);
        if (act.isNew()) {
            initParticipant(CUSTOMER, context.getContext().getCustomer());
        }

        Entity appointmentType = getAppointmentType();
        Entity schedule = getSchedule();
        smsPractice = SMSHelper.isSMSEnabled(getLayoutContext().getContext().getPractice());
        scheduleReminders = rules.isRemindersEnabled(schedule);
        noReminder = rules.getNoReminderPeriod();
        getSeries().setNoReminderPeriod(noReminder);

        if (appointmentType == null) {
            // set the appointment type to the default for the schedule
            if (schedule != null) {
                appointmentType = getDefaultAppointmentType(schedule);
                setAppointmentType(appointmentType);
            }
        }
        appointmentTypeReminders = rules.isRemindersEnabled(appointmentType);

        getProperty(STATUS).addModifiableListener(modifiable -> onStatusChanged());
        customerPatientReadOnly = makeCustomerPatientReadOnly();

        sendReminder = new BoundCheckBox(getProperty(SEND_REMINDER));
        addStartEndTimeListeners();
        if (act.isNew()) {
            updateSendReminder(true);
        }
        checkRoster();
    }

    /**
     * Sets the appointment type.
     *
     * @param appointmentType the appointment type. May be {@code null}
     */
    public void setAppointmentType(Entity appointmentType) {
        setParticipant(APPOINTMENT_TYPE, appointmentType);
        updateReason();
    }

    /**
     * Returns the appointment type.
     *
     * @return the appointment type. May be {@code null}
     */
    public Entity getAppointmentType() {
        return getParticipant(APPOINTMENT_TYPE);
    }

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    public User getClinician() {
        return (User) getParticipant(CLINICIAN);
    }

    /**
     * Returns the location associated with the schedule.
     *
     * @return the location. May be {@code null}
     */
    public Party getLocation() {
        Party location = null;
        Entity schedule = getSchedule();
        if (schedule != null) {
            IMObjectBean bean = getBean(schedule);
            location = (Party) getObject(bean.getTargetRef(LOCATION));
        }
        return location;
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        boolean editSeries = getSeriesEditor() != null;
        return new AppointmentEditor(reload(getObject()), getParent(), editSeries, getLayoutContext());
    }

    /**
     * Returns the event series.
     *
     * @return the series
     */
    @Override
    public AppointmentSeries getSeries() {
        return (AppointmentSeries) super.getSeries();
    }

    /**
     * Registers a listener to be notified of alerts.
     *
     * @param listener the listener. May be {@code null}
     */
    @Override
    public void setAlertListener(AlertListener listener) {
        super.setAlertListener(listener);
        checkRoster();
    }

    /**
     * Returns the appointment reason.
     * <p/>
     * This is a lookup.visitReason* code.
     *
     * @return the appointment reason. May be {@code null}
     */
    public String getReason() {
        return getProperty(REASON).getString();
    }

    /**
     * Sets the appointment reason.
     *
     * @param reason the reason. May be {@code null}
     */
    public void setReason(String reason) {
        getProperty(REASON).setValue(reason);
    }

    /**
     * Creates a new event series.
     *
     * @return a new event series
     */
    @Override
    protected ScheduleEventSeries createSeries() {
        return new AppointmentSeries(getObject(), getService());
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new AppointmentLayoutStrategy();
    }

    /**
     * Invoked when layout has completed. All editors have been created.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        Entity schedule = getSchedule();
        initSchedule(schedule);
        getAppointmentTypeEditor().addModifiableListener(modifiable -> onAppointmentTypeChanged());
        getClinicianEditor().addModifiableListener(modifiable -> checkRoster());

        if (getEndTime() == null) {
            calculateEndTime();
        }
        updateAlerts();
    }

    /**
     * Invoked when the start time changes. Calculates the end time.
     */
    @Override
    protected void onStartTimeChanged() {
        super.onStartTimeChanged();
        updateSendReminder(sendReminder.isSelected());
        checkRoster();
    }

    /**
     * Invoked when the end time changes. Recalculates the end time if it is less than the start time.
     */
    @Override
    protected void onEndTimeChanged() {
        super.onEndTimeChanged();
        checkRoster();
    }

    /**
     * Invoked when the customer changes. Sets the patient to null if no relationship exists between the two.
     * <p>
     * The alerts will be updated.
     */
    @Override
    protected void onCustomerChanged() {
        super.onCustomerChanged();
        updateSendReminder(true);
        getProperty(REMINDER_SENT).setValue(null);
        getProperty(REMINDER_ERROR).setValue(null);
        updateAlerts();
    }

    /**
     * Invoked when the patient changes. Sets the customer to the patient owner if required.
     */
    @Override
    protected void onPatientChanged() {
        super.onPatientChanged();
        updateAlerts();
    }

    /**
     * Calculates the end time if the start time and appointment type are set.
     */
    protected void calculateEndTime() {
        Date start = getStartTime();
        Entity schedule = getSchedule();
        AppointmentTypeParticipationEditor editor = getAppointmentTypeEditor();
        Entity appointmentType = editor.getEntity();
        if (start != null && schedule != null && appointmentType != null) {
            Date end = rules.calculateEndTime(start, schedule, appointmentType);
            setEndTime(end);
        }
    }

    /**
     * Invoked when the schedule is updated. This propagates it to the appointment type editor, and gets the new slot
     * size.
     *
     * @param schedule the schedule. May be {@code null}
     */
    protected void onScheduleChanged(Entity schedule) {
        super.onScheduleChanged(schedule);
        updateSendReminder(sendReminder.isSelected());
        checkRoster();
    }

    /**
     * Initialises the appointment type editor with the schedule, updates the slot size, and determines if schedule
     * reminders are enabled.
     *
     * @param schedule the schedule. May be {@code null}
     */
    @Override
    protected void initSchedule(Entity schedule) {
        AppointmentTypeParticipationEditor editor = getAppointmentTypeEditor();
        editor.setSchedule((org.openvpms.component.business.domain.im.common.Entity) schedule);
        scheduleReminders = schedule != null && rules.isRemindersEnabled(schedule);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return validateCustomer(validator) && super.doValidation(validator);
    }

    /**
     * Determines if the customer and patient should be read-only.
     * <p/>
     * They should be read-only if the appointment is associated with a visit or tasks, to avoid a mismatch in
     * details between the appointment and linked acts.
     *
     * @return {@code true} if they should be read-only, {@code false} if they should be editable
     */
    protected boolean makeCustomerPatientReadOnly() {
        IMObjectBean bean = getBean(getObject());
        return (bean.getTargetRef("event") != null || !bean.getValues("tasks").isEmpty());
    }

    /**
     * Checks that the clinician is rostered on.
     * <p/>
     * If rostering is enabled, and the selected clinician isn't rostered on during the appointment period,
     * or is rostered on to another schedule, this raises an alert.
     */
    private void checkRoster() {
        AlertListener listener = getAlertListener();
        Party location = getLocation();
        Entity schedule = getSchedule();
        if (listener != null && location != null && schedule != null && rules.checkRoster(getObject(), location)) {
            if (rosterAlert != null) {
                // cancel any existing alert
                listener.cancel(rosterAlert);
                rosterAlert = null;
            }
            User clinician = getClinician();
            if (clinician != null) {
                Date startTime = getStartTime();
                Date endTime = getEndTime();
                RosterService service = ServiceHelper.getBean(RosterService.class);
                List<RosterService.UserEvent> events = service.getUserEvents(clinician, location, startTime, endTime);
                if (events.isEmpty()) {
                    String message = Messages.format("workflow.scheduling.appointment.notrostered",
                                                     clinician.getName());
                    rosterAlert = listener.onAlert(message);
                } else {
                    // Determine if the roster events cover the appointment period
                    RangeSet<Date> set = TreeRangeSet.create();
                    set.add(Range.closed(startTime, endTime));
                    for (RosterService.UserEvent event : events) {
                        if (hasSchedule(event.getArea(), schedule)) {
                            set.remove(Range.closed(event.getStartTime(), event.getEndTime()));
                        }
                    }
                    if (!set.isEmpty()) {
                        String message = Messages.format("workflow.scheduling.appointment.partiallyrostered",
                                                         clinician.getName());
                        rosterAlert = listener.onAlert(message);
                    } else {
                        AppointmentService appointmentService = ServiceHelper.getBean(AppointmentService.class);
                        Act exclude = (getObject().isNew()) ? null : getObject();
                        List<ScheduleTimes> appointments = appointmentService.getAppointmentsForClinician(
                                clinician, startTime, endTime, exclude);
                        if (!appointments.isEmpty()) {
                            String scheduleName = IMObjectHelper.getName(appointments.get(0).getSchedule());
                            String message = Messages.format("workflow.scheduling.appointment.alreadyscheduled",
                                                             clinician.getName(), scheduleName);
                            rosterAlert = listener.onAlert(message);
                        }
                    }
                }
            }
        }
    }

    /**
     * Determines if a roster area has a schedule.
     *
     * @param areaRef  the roster area reference
     * @param schedule the schedule
     * @return {@code true} if the roster area has the schedule
     */
    private boolean hasSchedule(Reference areaRef, Entity schedule) {
        Entity area = (Entity) getObject(areaRef);
        return (area != null) && rules.rosterAreaHasSchedule(area, schedule);
    }

    /**
     * Returns the patient editor.
     *
     * @return the patient editor
     */
    private ClinicianParticipationEditor getClinicianEditor() {
        ParticipationEditor<User> result = getParticipationEditor(CLINICIAN, true);
        return (ClinicianParticipationEditor) result;
    }

    /**
     * Ensures a customer is selected.
     *
     * @param validator the validator
     * @return {@code true} if a customer is selected
     */
    private boolean validateCustomer(Validator validator) {
        boolean result = true;
        if (getCustomer() == null) {
            result = reportRequired(CUSTOMER, validator);
        }
        return result;
    }


    /**
     * Updates the alerts associated with the customer and patient.
     */
    private void updateAlerts() {
        Component container = getAlertsContainer();
        container.removeAll();
        Component alerts = createAlerts();
        if (alerts != null) {
            container.add(alerts);
        }
    }

    /**
     * Creates a component representing the customer and patient alerts.
     *
     * @return the alerts component or {@code null} if neither has alerts
     */
    private Component createAlerts() {
        Component result = null;
        Component customerSummary = null;
        Component patientSummary = null;
        Party customer = getCustomer();
        Party patient = getPatient();
        if (customer != null) {
            customerSummary = getCustomerAlerts(customer);
        }
        if (patient != null) {
            patientSummary = getPatientAlerts(patient);
        }
        if (customerSummary != null || patientSummary != null) {
            result = RowFactory.create(CELL_SPACING);
            if (customerSummary != null) {
                result.add(customerSummary);
            }
            if (patientSummary != null) {
                result.add(patientSummary);
            }
            result = RowFactory.create(INSET, result);
        }
        return result;
    }

    /**
     * Returns any alerts associated with the customer.
     *
     * @param customer the customer
     * @return any alerts associated with the customer, or {@code null} if the customer has no alerts
     */
    private Component getCustomerAlerts(Party customer) {
        AlertSummary alerts = getAlertSummary(customer, "alerts.customer");
        return (alerts != null) ? alerts.getComponent() : null;
    }

    /**
     * Returns any alerts associated with the patient.
     *
     * @param patient the patient
     * @return any alerts associated with the patient, or {@code null} if the patient has no alerts
     */
    private Component getPatientAlerts(Party patient) {
        AlertSummary alerts = getAlertSummary(patient, "alerts.patient");
        return (alerts != null) ? alerts.getComponent() : null;
    }

    /**
     * Returns the alert summary for a party.
     *
     * @param party the party. A customer or patient
     * @param key   the resource bundle key
     * @return the alert summary, or {@code null} if the party has no alerts
     */
    private AlertSummary getAlertSummary(Party party, String key) {
        AlertSummary result = null;
        LayoutContext context = getLayoutContext();
        List<Alert> alerts = ServiceHelper.getBean(AlertManager.class).getAlerts(party);
        if (!alerts.isEmpty()) {
            result = new AppointmentAlertSummary(party, alerts, key, context.getContext(), context.getHelpContext());
        }
        return result;
    }

    /**
     * Invoked when the appointment type changes. Calculates the end time if the start time is set,
     * and sets the appointment reason if a default is defined.
     */
    private void onAppointmentTypeChanged() {
        try {
            calculateEndTime();
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
        appointmentTypeReminders = rules.isRemindersEnabled(getAppointmentType());
        updateReason();
        updateSendReminder(true);
    }

    /**
     * Updates the appointment reason if there is a default reason associated with the appointment type.
     * <p/>
     * If not, and no reason has already been set, reverts to the default reason.
     */
    private void updateReason() {
        Entity appointmentType = getAppointmentType();
        if (appointmentType != null) {
            Component focus = FocusHelper.getFocus();
            IMObjectBean bean = getBean(appointmentType);
            Lookup reason = bean.getObject(REASON, Lookup.class);
            if (reason != null) {
                setReason(reason.getCode());
            } else if (getReason() == null) {
                // only use the default reason if there isn't an existing value set
                reason = ServiceHelper.getLookupService().getDefaultLookup(ScheduleArchetypes.VISIT_REASONS);
                if (reason != null) {
                    setReason(reason.getCode());
                }
            }
            if (focus != null) {
                // restore the focus, otherwise it shifts to the reason field
                FocusHelper.setFocus(focus);
            }
        }
    }

    /**
     * Invoked when the status changes. This:
     * <ul>
     *    <li>clears the confirmedTime and arrivalTime if the status is PENDING</li>
     *    <li>sets the confirmedTime to now if the status is CONFIRMED</li>
     *    <li>sets the arrivalTime to now if the status is CHECKED_IN.</li>
     * </ul
     */
    private void onStatusChanged() {
        String status = (String) getProperty(STATUS).getValue();
        if (AppointmentStatus.PENDING.equals(status)) {
            getProperty(CONFIRMED_TIME).setValue(null);
            getProperty(ARRIVAL_TIME).setValue(null);
        } else if (AppointmentStatus.CONFIRMED.equals(status)) {
            getProperty(CONFIRMED_TIME).setValue(new Date());
        } else if (AppointmentStatus.CHECKED_IN.equals(status)) {
            getProperty(ARRIVAL_TIME).setValue(new Date());
        }
    }

    /**
     * Returns the appointment type editor.
     *
     * @return the appointment type editor
     */
    private AppointmentTypeParticipationEditor getAppointmentTypeEditor() {
        ParticipationEditor<org.openvpms.component.business.domain.im.common.Entity> result
                = getParticipationEditor(APPOINTMENT_TYPE, true);
        return (AppointmentTypeParticipationEditor) result;
    }

    /**
     * Returns the default appointment type associated with a schedule.
     *
     * @param schedule the schedule
     * @return the default appointment type, or the the first appointment type
     * if there is no default, or {@code null} if none is found
     */
    private Entity getDefaultAppointmentType(Entity schedule) {
        return rules.getDefaultAppointmentType(schedule);
    }

    /**
     * Returns the alerts container.
     *
     * @return the alerts container
     */
    private Component getAlertsContainer() {
        if (alerts == null) {
            alerts = new Row();
        }
        return alerts;
    }

    /**
     * Updates the send reminder flag, based on the practice, schedule, customer and appointment type.
     * <p>
     * If SMS is disabled for the practice, schedule or customer, the flag is toggled off and disabled.
     * <p>
     * If not, it is enabled.
     *
     * @param select if {@code true} and reminders may be sent, select the flag, otherwise leave it
     */
    private void updateSendReminder(boolean select) {
        Date startTime = getStartTime();
        Date now = new Date();
        Party customer = getCustomer();
        boolean enabled = smsPractice && scheduleReminders && appointmentTypeReminders && noReminder != null
                          && customer != null && startTime != null && startTime.after(now)
                          && SMSHelper.canSMS(customer);
        if (!enabled) {
            sendReminder.setSelected(false);
        }
        sendReminder.setEnabled(enabled);
        if (enabled && select) {
            if (getObject().isNew()) {
                // for new appointments only select the reminder if the start time is after the no reminder period
                Date to = DateRules.plus(now, noReminder);
                select = startTime.after(to);
            }
            sendReminder.setSelected(select);
        }
    }

    private class AppointmentLayoutStrategy extends LayoutStrategy {

        /**
         * Constructs an {@link AppointmentLayoutStrategy}.
         */
        public AppointmentLayoutStrategy() {
            ArchetypeNodes archetypeNodes = getArchetypeNodes();
            archetypeNodes.excludeIfEmpty(REMINDER_SENT, REMINDER_ERROR, BOOKING_NOTES);
            if (!smsPractice || !scheduleReminders) {
                archetypeNodes.exclude(SEND_REMINDER);
            } else {
                addComponent(new ComponentState(sendReminder, sendReminder.getProperty()));
            }
            BoundDateTimeField reminderSent = BoundDateTimeFieldFactory.create(getProperty(REMINDER_SENT));
            reminderSent.setStyleName(Styles.EDIT);
            addComponent(new ComponentState(reminderSent));
        }

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            if (customerPatientReadOnly) {
                MutablePropertySet mutableProperties = new MutablePropertySet(properties);
                mutableProperties.setReadOnly(CUSTOMER);
                mutableProperties.setReadOnly(PATIENT);
                properties = mutableProperties;
            }
            Property bookingNotes = getProperty(BOOKING_NOTES);
            if (!StringUtils.isEmpty(bookingNotes.getString())) {
                addComponent(createMultiLineText(bookingNotes, 2, 10, new Extent(50, Extent.EX), context));
            }
            return super.apply(object, properties, parent, context);
        }

        /**
         * Lay out out the object in the specified container.
         *
         * @param object     the object to lay out
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param container  the container to use
         * @param context    the layout context
         */
        @Override
        protected void doLayout(IMObject object, PropertySet properties, IMObject parent, Component container,
                                LayoutContext context) {
            super.doLayout(object, properties, parent, container, context);
            container.add(getAlertsContainer());
        }

        /**
         * Returns the default focus component.
         * <p>
         * This implementation returns the customer component.
         *
         * @param components the components
         * @return the customer component, or {@code null} if none is found
         */
        @Override
        protected Component getDefaultFocus(ComponentSet components) {
            return components.getFocusable(CUSTOMER);
        }
    }
}
