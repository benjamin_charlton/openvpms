/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.document.Document;

/**
 * Diagnostic tab for the {@link DiagnosticsDialog}.
 *
 * @author Tim Anderson
 */
interface DiagnosticTab {

    /**
     * Returns the tab display name.
     *
     * @return the tab display name
     */
    String getName();

    /**
     * Returns the tab component
     *
     * @return the tab component
     */
    Component getComponent();

    /**
     * Refreshes the display.
     */
    void refresh();

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    Document getDocument();
}
