/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.IMObjectSelections;
import org.openvpms.web.component.im.query.ListQuery;
import org.openvpms.web.component.im.query.MultiSelectTableBrowser;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

import static org.openvpms.archetype.rules.patient.InvestigationArchetypes.PATIENT_INVESTIGATION;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.INVESTIGATION_TYPE;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.ORDER_STATUS;

/**
 * Dialog to select investigations that require laboratory orders.
 *
 * @author Tim Anderson
 */
class InvestigationOrderDialog extends PopupDialog {

    /**
     * Submit button identifier.
     */
    public static final String SUBMIT_ID = "button.submit";

    /**
     * The investigation manager.
     */
    private final InvestigationManager manager;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The selections.
     */
    private final IMObjectSelections<DocumentAct> selections;


    /**
     * Constructs an {@link InvestigationOrderDialog}.
     *
     * @param manager the investigation manager
     * @param context the layout context
     * @param skip    if {@code true}, display a 'Skip' button
     */
    public InvestigationOrderDialog(InvestigationManager manager, LayoutContext context, boolean skip) {
        super(Messages.get("customer.charge.investigation.submit.title"),
              (skip) ? new String[]{SUBMIT_ID, SKIP_ID, CANCEL_ID}
                     : new String[]{SUBMIT_ID, CANCEL_ID});
        setModal(true);

        this.manager = manager;
        this.context = new DefaultLayoutContext(context);
        context.setComponentFactory(new TableComponentFactory(this.context));
        selections = new IMObjectSelections<>();
        selections.setListener(this::updateSubmit);
        resize("InvestigationOrderDialog.size");
    }

    /**
     * Returns the selections.
     *
     * @return the selections
     */
    public List<DocumentAct> getSelections() {
        return new ArrayList<>(selections.getSelected());
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        List<DocumentAct> investigations = manager.getUnconfirmedLaboratoryInvestigations();
        for (DocumentAct act : investigations) {
            if (!InvestigationActStatus.CONFIRM_DEFERRED.equals(act.getStatus2())) {
                selections.setSelected(act, true);
            }
        }
        ListQuery<DocumentAct> query = new ListQuery<>(investigations, PATIENT_INVESTIGATION, DocumentAct.class);
        DefaultDescriptorTableModel<DocumentAct> model = new DefaultDescriptorTableModel<>(
                PATIENT_INVESTIGATION, context, "startTime", INVESTIGATION_TYPE, ORDER_STATUS, "clinician");
        MultiSelectTableBrowser<DocumentAct> browser = new MultiSelectTableBrowser<>(query, model, selections, context);
        getLayout().add(browser.getComponent());
        updateSubmit();
    }

    /**
     * Enables/disables the submit button.
     */
    private void updateSubmit() {
        getButtons().setEnabled(SUBMIT_ID, !selections.getSelected().isEmpty());
    }
}
