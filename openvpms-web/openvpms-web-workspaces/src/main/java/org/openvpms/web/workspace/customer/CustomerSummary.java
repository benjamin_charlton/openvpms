/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.layout.GridLayoutData;
import nextapp.echo2.app.layout.RowLayoutData;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.web.component.alert.Alert;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.component.app.ContextHelper;
import org.openvpms.web.component.app.GlobalContext;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.contact.ContactHelper;
import org.openvpms.web.component.im.contact.EmailEditor;
import org.openvpms.web.component.im.contact.EmailLauncher;
import org.openvpms.web.component.im.contact.PhoneLink;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.sms.SMSDialog;
import org.openvpms.web.component.im.sms.SMSHelper;
import org.openvpms.web.component.im.view.IMObjectReferenceViewer;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.StyleSheetHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.alert.AlertSummary;
import org.openvpms.web.workspace.customer.communication.CommunicationArchetypes;
import org.openvpms.web.workspace.summary.PartySummary;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * Renders customer summary information.
 *
 * @author Tim Anderson
 */
public class CustomerSummary extends PartySummary {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The customer rules.
     */
    private final CustomerRules partyRules;

    /**
     * The account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The maximum no. of characters to display for contacts, or {@code <= 0} to not restrict them.
     */
    private final int contactLength;

    /**
     * Constructs a {@link CustomerSummary}.
     *
     * @param context     the context
     * @param help        the help context
     * @param preferences the preferences
     */
    public CustomerSummary(Context context, HelpContext help, Preferences preferences) {
        super(context, help.topic("customer/summary"), preferences);
        service = ServiceHelper.getArchetypeService();
        partyRules = ServiceHelper.getBean(CustomerRules.class);
        accountRules = ServiceHelper.getBean(CustomerAccountRules.class);
        contactLength = StyleSheetHelper.getProperty("customer.summary.contact.maxlength", -1);
    }

    /**
     * Returns summary information for a party.
     *
     * @param party  the party
     * @param alerts the alerts
     * @return a summary component
     */
    @Override
    protected Component createSummary(Party party, List<Alert> alerts) {
        Component column = ColumnFactory.create();
        Component customerName = getCustomerName(party);
        column.add(ColumnFactory.create(Styles.SMALL_INSET, customerName));
        Component customerId = getCustomerId(party);
        column.add(ColumnFactory.create(Styles.SMALL_INSET, customerId));

        Context context = getContext();

        Grid grid = GridFactory.create(2);
        grid.setWidth(Styles.FULL_WIDTH);

        if (party.isA(CustomerArchetypes.PERSON)) { // exclude OTC customers
            addPhone(party, grid);
            addEmail(party, grid);
        }

        if (getPreferences().getBoolean(PreferenceArchetypes.SUMMARY, "showCustomerAccount", true)) {
            Label balanceTitle = create("customer.account.balance");
            BigDecimal balance = accountRules.getBalance(party);
            Label balanceValue = create(balance);

            Label overdueTitle = create("customer.account.overdue");
            BigDecimal overdue = accountRules.getOverdueBalance(party, new Date());
            Label overdueValue = create(overdue);

            Label currentTitle = create("customer.account.current");
            BigDecimal current = balance.subtract(overdue);
            Label currentValue = create(current);

            Label unbilledTitle = create("customer.account.unbilled");
            BigDecimal unbilled = accountRules.getUnbilledAmount(party);
            Label unbilledValue = create(unbilled);

            Label effectiveTitle = create("customer.account.effective");
            BigDecimal effective = balance.add(unbilled);
            Label effectiveValue = create(effective);

            grid.add(balanceTitle);
            grid.add(balanceValue);
            grid.add(overdueTitle);
            grid.add(overdueValue);
            grid.add(currentTitle);
            grid.add(currentValue);
            grid.add(unbilledTitle);
            grid.add(unbilledValue);
            grid.add(effectiveTitle);
            grid.add(effectiveValue);
        }
        column.add(grid);
        if (!alerts.isEmpty()) {
            AlertSummary alertSummary = new AlertSummary(party, alerts, context, getHelpContext());
            column.add(ColumnFactory.create(Styles.SMALL_INSET, alertSummary.getComponent()));
        }
        Column result = ColumnFactory.create("PartySummary", column);
        if (SMSHelper.isSMSEnabled(context.getPractice())) {
            List<Contact> contacts = ContactHelper.getSMSContacts(party);
            if (!contacts.isEmpty()) {
                Context local = new LocalContext(context);
                local.setCustomer(party);
                Button button = ButtonFactory.create("button.sms.send", new ActionListener() {
                    public void onAction(ActionEvent event) {
                        SMSDialog dialog = new SMSDialog(contacts, local, getHelpContext().subtopic("sms"));
                        dialog.show();
                    }
                });
                result.add(RowFactory.create(Styles.SMALL_INSET, button));
            }
        }

        return result;
    }

    /**
     * Returns a component representing the customer name.
     *
     * @param customer the customer
     * @return the customer name component
     */
    protected Component getCustomerName(Party customer) {
        IMObjectReferenceViewer viewer = new IMObjectReferenceViewer(customer.getObjectReference(),
                                                                     customer.getName(), true, getContext());
        viewer.setStyleName("hyperlink-bold");
        return viewer.getComponent();
    }

    /**
     * Returns a component representing the customer identifier.
     *
     * @param customer the customer
     * @return the customer identifier component
     */
    protected Component getCustomerId(Party customer) {
        Component result;
        Component customerId = createLabel("customer.id", customer.getId());
        if (customer.isA(CustomerArchetypes.PERSON)) {
            Button communication = ButtonFactory.create(null, "button.communication", new ActionListener() {
                public void onAction(ActionEvent event) {
                    ContextApplicationInstance instance = ContextApplicationInstance.getInstance();
                    ContextHelper.setCustomer(instance.getContext(), customer);
                    instance.switchTo(CommunicationArchetypes.ACTS);
                }
            });
            communication.setToolTipText(Messages.get("customer.communication.icon"));
            Row right = RowFactory.create(communication);

            RowLayoutData rightLayout = new RowLayoutData();
            rightLayout.setAlignment(Alignment.ALIGN_RIGHT);
            rightLayout.setWidth(Styles.FULL_WIDTH);
            right.setLayoutData(rightLayout);

            result = RowFactory.create(Styles.WIDE_CELL_SPACING, customerId, right);
        } else {
            // OTC customer
            result = customerId;
        }
        return result;
    }

    /**
     * Refreshes the global context customer after editing.
     *
     * @param customer the latest instance
     */
    private void refreshCustomer(Party customer) {
        // if the customer is selected, refresh it
        GlobalContext globalContext = ContextApplicationInstance.getInstance().getContext();
        if (Objects.equals(customer, globalContext.getCustomer())) {
            globalContext.setCustomer(customer);
        }
    }

    /**
     * Displays the customer phone number, if the customer has one, or a button to add a phone contact if if not.
     *
     * @param customer the customer
     * @param grid     the grid to add to
     */
    private void addPhone(Party customer, Grid grid) {
        Row container = new Row();
        addPhone(customer, container);
        grid.add(container);
    }

    /**
     * Displays the customer phone number, if the customer has one, or a button to add a phone contact if if not.
     *
     * @param customer  the customer
     * @param container the phone container
     */
    private void addPhone(Party customer, Row container) {
        // for historical reasons, phone contacts can be saved without a phone number, so if there is one present,
        // without a number, display an Add button, and populate it on edit.
        Contact contact = (Contact) partyRules.getTelephoneContact(customer);
        PhoneLink link = new PhoneLink(partyRules, service, contactLength);
        Component component = link.getLink(contact, true);
        if (component != null) {
            // add a tel: link for the phone
            addContact(component, null, container);
        } else {
            Button button = ButtonFactory.create("button.add", Styles.DEFAULT, false, new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    onCreatePhone(customer, contact, container);
                }
            });
            addContact(LabelFactory.create("customer.phone"), button, container);
        }
    }

    /**
     * Updates an existing empty phone contact, or creates a new one if not present, and displays it in the summary.
     *
     * @param customer  the customer
     * @param contact   the existing empty contact. May be {@code null}
     * @param container the container
     */
    private void onCreatePhone(Party customer, Contact contact, Row container) {
        if (contact == null) {
            contact = service.create(ContactArchetypes.PHONE, Contact.class);
        }
        DefaultLayoutContext layout = new DefaultLayoutContext(getContext(), getHelpContext());
        IMObjectEditor editor = ServiceHelper.getBean(IMObjectEditorFactory.class).create(contact, customer, layout);
        SummaryContactEditDialog dialog = new SummaryContactEditDialog(contact, editor, customer, getContext());
        dialog.addWindowPaneListener(
                new PopupDialogListener() {
                    @Override
                    public void onOK() {
                        Party latest = dialog.getCustomer();
                        addPhone(latest, container);
                        refreshCustomer(latest);
                    }
                }
        );
        dialog.show();
    }

    /**
     * Adds a button to launch an email dialog, if the customer has an email address, or a button to add an email
     * address if not.
     *
     * @param customer the customer
     * @param grid     the grid to add to
     */
    private void addEmail(Party customer, Grid grid) {
        Row container = new Row();
        addEmail(customer, container);
        grid.add(container);
    }

    /**
     * Adds a button to launch an email dialog, if the customer has an email address, or a button to add an email
     * address if not.
     *
     * @param customer  the customer
     * @param container the email container
     */
    private void addEmail(Party customer, Row container) {
        HelpContext help = getHelpContext().topic("email");
        MailContext mailContext = new CustomerMailContext(getContext(), true, help);
        EmailLauncher launcher = EmailLauncher.create(customer, service, getContext(), help, mailContext);
        if (launcher != null) {
            launcher.setEmailLength(contactLength);  // limit email length to avoid scrollbars
            Component mailToLink = launcher.getMailToLink();
            addContact(launcher.getWriteButton(), mailToLink, container);
        } else {
            Button button = ButtonFactory.create("button.add", Styles.DEFAULT, false, new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    onCreateEmail(customer, container);
                }
            });
            addContact(LabelFactory.create("customer.email"), button, container);
        }
    }

    /**
     * Creates a new phone contact, and displays it in the summary.
     *
     * @param customer  the customer
     * @param container the email container
     */
    private void onCreateEmail(Party customer, Row container) {
        Contact contact = service.create(ContactArchetypes.EMAIL, Contact.class);
        EmailEditor editor = new EmailEditor(contact, customer,
                                             new DefaultLayoutContext(getContext(), getHelpContext()));
        SummaryContactEditDialog dialog = new SummaryContactEditDialog(contact, editor, customer, getContext());
        dialog.addWindowPaneListener(
                new PopupDialogListener() {
                    @Override
                    public void onOK() {
                        Party latest = dialog.getCustomer();
                        addEmail(latest, container);
                        refreshCustomer(latest);
                    }
                }
        );
        dialog.show();
    }

    /**
     * Adds a contact to a row.
     * <p/>
     * This row is expected to be the child of a grid of two columns, and will be made to span both.
     * This ensures that when it grows, it doesn't push the right column out of the desired panel.
     *
     * @param left      the left component
     * @param right     the right component. May be {@code null}
     * @param container the contact container
     */
    private void addContact(Component left, Component right, Row container) {
        container.removeAll();
        if (container.getLayoutData() == null) {
            container.setLayoutData(ComponentGrid.layout(1, 2)); // span both cells
        }
        container.add(left);
        if (right != null) {
            Row wrapper = RowFactory.create(right);         // force the component to render minimum width
            wrapper.setLayoutData(RowFactory.rightAlign());
            container.add(wrapper);
        }
    }

    /**
     * Helper to create a label for the given key.
     *
     * @param key the key
     * @return a new label
     */
    private Label create(String key) {
        return LabelFactory.create(key);
    }

    /**
     * Creates a new label for a numeric value, to be right aligned in a cell.
     *
     * @param value the value
     * @return a new label
     */
    private Label create(BigDecimal value) {
        return LabelFactory.create(value, new GridLayoutData());
    }

}
