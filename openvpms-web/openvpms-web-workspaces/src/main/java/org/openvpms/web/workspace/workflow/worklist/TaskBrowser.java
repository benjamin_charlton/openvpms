/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.table.Cell;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleBrowser;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleColours;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleTableModel;

import java.util.Date;
import java.util.Map;


/**
 * Task browser.
 *
 * @author Tim Anderson
 */
public class TaskBrowser extends ScheduleBrowser {

    /**
     * The colour cache.
     */
    private final ScheduleColours colours;

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * Constructs a {@link TaskBrowser}.
     *
     * @param prefs   the user preferences
     * @param context the context
     */
    public TaskBrowser(Preferences prefs, Context context) {
        super(new TaskQuery(context, prefs), context);
        colours = ServiceHelper.getBean(ScheduleColours.class);
        rules = ServiceHelper.getBean(AppointmentRules.class);
    }

    /**
     * Updates the status range selector to include the specified status, if required.
     *
     * @param status the status. May be {@code null}
     */
    public void includeStatus(String status) {
        TaskQuery query = (TaskQuery) getQuery();
        query.includeStatus(status);
    }

    /**
     * Query using the specified criteria, and populate the table with matches.
     */
    @Override
    public void query() {
        doQuery(true);
    }

    /**
     * Attempts to select a task.
     *
     * @param task the task
     * @return {@code true} if the task was selected, {@code false} if it was not found
     */
    public boolean setSelected(Act task) {
        IMObjectBean bean = getBean(task);
        PropertySet selected = null;
        Reference worklist = bean.getTargetRef("worklist");
        if (worklist != null) {
            Reference taskRef = task.getObjectReference();
            ScheduleTableModel model = getModel();
            Cell cell = model.getCell(worklist, taskRef);
            if (cell != null) {
                model.setSelected(cell);
                selected = model.getEvent(cell);
            }
        }
        setSelected(selected);
        return selected != null;
    }

    /**
     * Returns the schedule associated with an event.
     *
     * @param act the event
     * @return the corresponding schedule, or {@code null} if there is none
     */
    @Override
    protected Entity getSchedule(Act act) {
        return getBean(act).getTarget("worklist", Entity.class);
    }

    /**
     * Creates a new grid for a set of events.
     *
     * @param date   the query date
     * @param events the events
     */
    protected ScheduleEventGrid createEventGrid(Date date, Map<Entity, ScheduleEvents> events) {
        return new TaskGrid(getScheduleView(), date, events, rules);
    }

    /**
     * Creates a new table model.
     *
     * @param grid the schedule event grid
     * @return the table model
     */
    protected ScheduleTableModel createTableModel(ScheduleEventGrid grid) {
        if (grid.getSchedules().size() == 1) {
            return new SingleScheduleTaskTableModel((TaskGrid) grid, getContext(), colours);
        }
        return new MultiScheduleTaskTableModel((TaskGrid) grid, getContext(), colours);
    }

}
