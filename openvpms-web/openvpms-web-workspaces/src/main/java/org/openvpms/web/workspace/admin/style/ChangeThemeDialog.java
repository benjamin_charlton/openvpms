/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.style;

import nextapp.echo2.app.ListBox;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.dialog.SelectionDialog;
import org.openvpms.web.echo.style.StyleSheetCache;
import org.openvpms.web.echo.style.ThemeResources;
import org.openvpms.web.echo.style.UserStyleSheets;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * Prompts to select from the list of available themes, and updates the practice with the selected theme.
 *
 * @author Tim Anderson
 */
public class ChangeThemeDialog extends SelectionDialog {

    /**
     * The theme node.
     */
    private static final String THEME = "theme";

    /**
     * Constructs a {@link SelectionDialog}.
     *
     * @param themes the themes to select from
     */
    private ChangeThemeDialog(ListBox themes) {
        super(Messages.get("stylesheet.theme.title"), Messages.get("stylesheet.theme.message"), themes);
    }

    /**
     * Shows the dialog.
     *
     * @param practice the practice
     */
    public static void show(Party practice) {
        ThemeResources themes = ServiceHelper.getBean(ThemeResources.class);
        themes.load();   // refresh
        ThemeListModel model = new ThemeListModel(themes);
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(practice);
        String theme = bean.getString(THEME);
        ListBox listBox = new ListBox(model);
        if (theme != null) {
            int index = model.indexOf(theme);
            if (index != -1) {
                listBox.setSelectedIndex(index);
            }
        }
        listBox.setCellRenderer(new ThemeRenderer(themes));
        ChangeThemeDialog dialog = new ChangeThemeDialog(listBox);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            public void onOK() {
                String newTheme = (String) dialog.getSelected();
                if (newTheme != null && !newTheme.equals(theme)) {
                    bean.setValue(THEME, newTheme);
                    bean.save();
                    ServiceHelper.getBean(StyleSheetCache.class).reset();
                    ServiceHelper.getBean(UserStyleSheets.class).reset();
                    ContextApplicationInstance.getInstance().setStyleSheet();
                }
            }
        });
        dialog.show();
    }
}
