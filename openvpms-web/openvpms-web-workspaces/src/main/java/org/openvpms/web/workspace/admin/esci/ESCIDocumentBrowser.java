/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SplitPane;
import org.openvpms.esci.adapter.dispatcher.Inbox;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.AbstractTableBrowser;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.StyleSheetHelper;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * ESCI document browser.
 *
 * @author Tim Anderson
 */
class ESCIDocumentBrowser extends AbstractTableBrowser<DocumentReference> {

    /**
     * The inbox to browse.
     */
    private final Inbox inbox;

    /**
     * The message.
     */
    private final Row message;

    /**
     * Constructs an {@link ESCIDocumentBrowser}.
     *
     * @param inbox   the inbox to browse
     * @param context the layout context
     */
    public ESCIDocumentBrowser(Inbox inbox, LayoutContext context) {
        super(new ESCIDocumentTableModel(), context);
        this.inbox = inbox;
        Label label = LabelFactory.text(Messages.format("admin.esci.inbox.message",
                                                        inbox.getSupplier().getName(),
                                                        inbox.getStockLocation().getName()));
        message = RowFactory.create(Styles.INSET, label);
    }

    /**
     * Query using the specified criteria, and populate the browser with matches.
     */
    @Override
    public void query() {
        Component component = getComponent();
        ResultSet<DocumentReference> set = getResultSet();
        boolean hasResults = hasResults(set);
        doLayout(component, hasResults);

        PagedIMTable<DocumentReference> table = getTable();
        table.setResultSet(set);
        setFocusOnResults();
    }

    /**
     * Determines if the first document is selected.
     *
     * @return {@code true} if the first document is selected, otherwise {@code false}
     */
    public boolean isFirstDocumentSelected() {
        boolean result = false;
        DocumentReference selected = getSelected();
        PagedIMTable<DocumentReference> table = getTable();
        if (selected != null && table != null) {
            if (table.getPage() == 0) {
                List<DocumentReference> objects = table.getTable().getObjects();
                if (objects.size() > 0 && objects.get(0).equals(selected)) {
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * Lay out this component.
     */
    @Override
    protected void doLayout() {
        super.doLayout();
        SplitPane container = (SplitPane) getComponent();
        int height = StyleSheetHelper.getProperty("font.size", -1);
        if (height > 0) {
            container.setSeparatorPosition(new Extent(height * 2)); // space for the message
        }
    }

    /**
     * Lays out this component.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        container.add(message);
    }

    /**
     * Creates a new result set.
     *
     * @return the result set
     */
    private ResultSet<DocumentReference> getResultSet() {
        List<DocumentReference> result = new ArrayList<>();
        List<DocumentReferenceType> documents = inbox.getDocuments();
        for (DocumentReferenceType reference : documents) {
            result.add(new DocumentReference(reference));
        }
        return new ListResultSet<>(result, 8);
    }

}
