/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.web.component.im.doc.DocumentViewer;
import org.openvpms.web.component.im.doc.DocumentViewerFactory;
import org.openvpms.web.component.im.edit.IMObjectTableCollectionEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.web.component.im.doc.DocumentActLayoutStrategy.DOCUMENT;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.INVESTIGATION_TYPE;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.ORDER_STATUS;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.STATUS;

/**
 * Editor for a collection of investigations.
 * <p/>
 * This disables the Add button, as investigations are created through charging products with tests attached.
 *
 * @author Tim Anderson
 * @see InvestigationManager
 */
public class InvestigationCollectionEditor extends IMObjectTableCollectionEditor {

    /**
     * The investigation manager.
     */
    private final InvestigationManager investigations;

    /**
     * Constructs a {@link InvestigationCollectionEditor}.
     *
     * @param investigations the investigations manager to delegate to
     * @param object         the object being edited
     * @param context        the layout context
     */
    public InvestigationCollectionEditor(InvestigationManager investigations, IMObject object, LayoutContext context) {
        super(investigations.getEditor(), object, context);
        this.investigations = investigations;
        investigations.setReloadListener(this::reload);
    }

    /**
     * Disposes of the editor.
     * <br/>
     * Once disposed the behaviour of invoking any method is undefined.
     */
    @Override
    public void dispose() {
        super.dispose();
        investigations.setReloadListener(null);
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        context = new DefaultLayoutContext(context);
        context.setComponentFactory(new TableComponentFactory(context));
        return new InvestigationTableModel(context);
    }

    /**
     * Adds add/delete buttons to the button row, and previous/next buttons, if the cardinality is > 1.
     *
     * @param buttons the buttons to add to
     */
    @Override
    protected void addButtons(ButtonRow buttons) {
        super.addButtons(buttons);
        buttons.getButtons().remove(ADD_ID); // direct addition not supported
    }

    /**
     * Invoked when the collection or an editor changes. Resets the cached valid status and notifies registered
     * listeners.
     *
     * @param modifiable the modifiable to pass to the listeners
     */
    @Override
    protected void onModified(Modifiable modifiable) {
        super.onModified(modifiable);
        refresh();
    }

    /**
     * Invoked after an investigation has been reloaded, to update the display.
     */
    private void reload() {
        IMObject object = getSelected();
        refresh(true);
        if (object != null) {
            removeCurrentEditor(); // ensures the current editor is discarded
            edit(object);
        }
    }

    /**
     * Investigation table model. This includes links to any external results.
     */
    private class InvestigationTableModel extends DefaultDescriptorTableModel<IMObject> {

        /**
         * The document viewer factory.
         */
        private final DocumentViewerFactory factory;

        /**
         * Constructs an {@link InvestigationTableModel}.
         *
         * @param context the layout context
         */
        public InvestigationTableModel(LayoutContext context) {
            super(InvestigationCollectionEditor.this.getProperty().getArchetypeRange(), context, "startTime",
                  INVESTIGATION_TYPE, STATUS, ORDER_STATUS, "clinician", DOCUMENT);
            factory = ServiceHelper.getBean(DocumentViewerFactory.class);
        }

        /**
         * Returns a value for a given column.
         *
         * @param object the object to operate on
         * @param column the column
         * @param row    the row
         * @return the value for the column
         */
        @Override
        protected Object getValue(IMObject object, DescriptorTableColumn column, int row) {
            Object result;
            if (DOCUMENT.equals(column.getName())) {
                DocumentViewer viewer = factory.create((DocumentAct) object, true, getContext());
                viewer.setShowNoDocument(false);
                result = viewer.getComponent();
            } else {
                result = super.getValue(object, column, row);
            }
            return result;
        }
    }
}
