/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import org.openvpms.archetype.component.processor.ProcessorListener;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.CustomerBalanceSummaryQuery;
import org.openvpms.archetype.rules.finance.statement.Statement;
import org.openvpms.archetype.rules.finance.statement.StatementProcessor;
import org.openvpms.archetype.rules.finance.statement.StatementProcessorException;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.mail.DefaultMailerFactory;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailerFactory;
import org.openvpms.web.component.service.CurrentLocationMailService;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.communication.CommunicationHelper;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.openvpms.web.workspace.customer.communication.LoggingMailerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Statement generator.
 *
 * @author Tim Anderson
 */
class StatementGenerator extends AbstractStatementGenerator {

    /**
     * The statement processor progress bar.
     */
    private StatementProgressBarProcessor progressBarProcessor;

    /**
     * The processor.
     */
    private StatementProcessor processor;

    /**
     * Constructs a new {@code StatementGenerator} for a single customer.
     *
     * @param customer    the customer reference
     * @param date        the statement date
     * @param printOnly   if {@code true} only print statements
     * @param context     the context
     * @param mailContext the mail context
     * @param help        the help context
     */
    public StatementGenerator(IMObjectReference customer, Date date, boolean printOnly, Context context,
                              MailContext mailContext, HelpContext help) {
        super(Messages.get("reporting.statements.run.title"),
              Messages.get("reporting.statements.run.cancel.title"),
              Messages.get("reporting.statements.run.cancel.message"),
              Messages.get("reporting.statements.run.retry.title"), help);
        List<Party> customers = new ArrayList<>();
        Party party = (Party) IMObjectHelper.getObject(customer, context);
        if (party != null) {
            customers.add(party);
        }
        init(customers, date, printOnly, context, mailContext, help);
    }

    /**
     * Constructs a new {@code StatementGenerator} for statements returned by a query.
     *
     * @param query       the query
     * @param context     the context
     * @param mailContext the mail context
     * @param help        the help context
     */
    public StatementGenerator(CustomerBalanceQuery query, Context context, MailContext mailContext, HelpContext help) {
        super(Messages.get("reporting.statements.run.title"),
              Messages.get("reporting.statements.run.cancel.title"),
              Messages.get("reporting.statements.run.cancel.message"),
              Messages.get("reporting.statements.run.retry.title"), help);
        List<ObjectSet> balances = query.getObjects();
        List<Party> customers = new ArrayList<>();
        for (ObjectSet set : balances) {
            BigDecimal balance
                    = set.getBigDecimal(CustomerBalanceSummaryQuery.BALANCE);
            if (BigDecimal.ZERO.compareTo(balance) != 0) {
                // only include customers with non-zero balances
                IMObjectReference ref = set.getReference(
                        CustomerBalanceSummaryQuery.CUSTOMER_REFERENCE);
                Party customer = (Party) IMObjectHelper.getObject(ref, context);
                if (customer != null) {
                    customers.add(customer);
                }
            }
        }
        init(customers, query.getDate(), false, context, mailContext, help);
    }

    /**
     * Determines if statements that have been printed should be reprinted.
     * A statement is printed if the printed flag of its
     * <em>act.customerAccountOpeningBalance</em> is {@code true}.
     * Defaults to {@code false}.
     *
     * @param reprint if {@code true}, process statements that have been printed.
     */
    public void setReprint(boolean reprint) {
        processor.setReprint(reprint);
    }

    /**
     * Returns the processor.
     *
     * @return the processor
     */
    protected StatementProgressBarProcessor getProcessor() {
        return progressBarProcessor;
    }

    /**
     * Initialises this.
     *
     * @param customers   the customers to generate statements for
     * @param date        the statement date
     * @param printOnly   if {@code true}, only print statements
     * @param context     the context
     * @param mailContext the mail context
     * @param help        the help context
     * @throws ArchetypeServiceException   for any archetype service error
     * @throws StatementProcessorException for any statement processor exception
     */
    private void init(List<Party> customers, Date date, boolean printOnly, Context context, MailContext mailContext,
                      HelpContext help) {
        Party practice = context.getPractice();
        if (practice == null) {
            throw new StatementProcessorException(StatementProcessorException.ErrorCode.InvalidConfiguration,
                                                  "Context has no practice");
        }

        IMPrinterFactory printerFactory = ServiceHelper.getBean(IMPrinterFactory.class);
        ReporterFactory reporterFactory = ServiceHelper.getBean(ReporterFactory.class);
        PracticeRules practiceRules = ServiceHelper.getBean(PracticeRules.class);
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        CustomerAccountRules customerAccountRules = ServiceHelper.getBean(CustomerAccountRules.class);
        MailPasswordResolver passwordResolver = ServiceHelper.getBean(MailPasswordResolver.class);
        processor = new StatementProcessor(date, practice, service, customerAccountRules);
        progressBarProcessor = new StatementProgressBarProcessor(processor, customers);

        CommunicationLogger logger = null;
        if (CommunicationHelper.isLoggingEnabled(practice, service)) {
            logger = ServiceHelper.getBean(CommunicationLogger.class);
        }
        StatementPrintProcessor printer = new StatementPrintProcessor(progressBarProcessor, printerFactory,
                                                                      getCancelListener(), practice, context,
                                                                      mailContext, help, logger);
        if (printOnly) {
            processor.addListener(printer);
            printer.setUpdatePrinted(false);
        } else {
            MailerFactory mailerFactory = getMailerFactory();
            EmailTemplateEvaluator evaluator = ServiceHelper.getBean(EmailTemplateEvaluator.class);
            Party location = context.getLocation();
            // the practice location to use if a customer doesn't have a preferred location.
            // TODO - this is not ideal. For reminders, ReminderConfiguration.getLocation() is used.
            if (location == null) {
                throw new StatementProcessorException(StatementProcessorException.ErrorCode.InvalidConfiguration,
                                                      "Context has no location");
            }
            StatementEmailProcessor mailer = new StatementEmailProcessor(mailerFactory, evaluator, reporterFactory,
                                                                         practice, location, practiceRules, service,
                                                                         logger, passwordResolver);
            processor.addListener(new StatementDelegator(printer, mailer));
        }
    }

    /**
     * Returns the mailer factory.
     * <p>
     * Note that the default factory isn't used as it could be an instance of {@link LoggingMailerFactory};
     * logging is handled by the {@link CommunicationLogger} when logging is enabled.
     *
     * @return the mailer factory
     */
    private MailerFactory getMailerFactory() {
        return new DefaultMailerFactory(ServiceHelper.getBean(CurrentLocationMailService.class),
                                        ServiceHelper.getBean(DocumentHandlers.class));
    }

    private static class StatementDelegator implements ProcessorListener<Statement> {

        private final ProcessorListener<Statement> printer;

        private final ProcessorListener<Statement> mailer;

        StatementDelegator(ProcessorListener<Statement> printer, ProcessorListener<Statement> mailer) {
            this.printer = printer;
            this.mailer = mailer;
        }

        /**
         * Process a statement.
         *
         * @param statement the statement to process
         * @throws OpenVPMSException for any error
         */
        public void process(Statement statement) {
            ProcessorListener<Statement> listener = printer;
            List<Contact> contacts = statement.getContacts();
            if (contacts.size() >= 1) {
                Contact contact = contacts.get(0);
                if (contact.isA(ContactArchetypes.EMAIL)) {
                    listener = mailer;
                }
            }
            listener.process(statement);
        }
    }

}
