/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Checks for the presence of external results for an investigation.
 *
 * @author Tim Anderson
 */
public class ResultChecker {

    /**
     * The investigation.
     */
    private final DocumentAct investigation;

    /**
     * The lab order associated with the investigation.
     */
    private Order order;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ResultChecker.class);

    /**
     * Constructs a {@link ResultChecker}.
     *
     * @param investigation the investigation
     */
    public ResultChecker(DocumentAct investigation) {
        this.investigation = investigation;
    }

    /**
     * Returns the investigation.
     *
     * @return the investigation
     */
    public DocumentAct getInvestigation() {
        return investigation;
    }

    /**
     * Determines if results can be checked for.
     * <p/>
     * Results can be checked if the investigation {@code checkResults} flag is set, and the investigation isn't
     * CANCELLED, and the investigation order has been successfully sent.
     *
     * @return {@code true} if results can be checked for
     */
    public boolean canCheckResults() {
        boolean result = false;
        String status = investigation.getStatus2();
        if (!InvestigationActStatus.CANCELLED.equals(investigation.getStatus())
            && !InvestigationActStatus.PENDING.equals(status)
            && !InvestigationActStatus.CONFIRM.equals(status)
            && !InvestigationActStatus.CONFIRM_DEFERRED.equals(status)
            && !InvestigationActStatus.ERROR.equals(status)) {
            result = ServiceHelper.getArchetypeService().getBean(investigation).getBoolean("checkResults");
        }
        return result;
    }

    /**
     * Checks for external results.
     *
     * @return {@code true} if external results are available
     */
    public boolean checkResults() {
        boolean results = false;
        try {
            Order order = getOrder();
            if (order != null) {
                results = order.getReport().hasExternalResults();
                if (!results) {
                    LaboratoryService service = InvestigationHelper.getLaboratoryService(order);
                    if (service.canCheck(order) && service.check(order)) {
                        results = order.getReport().hasExternalResults();
                    }
                }
            }
            if (!results) {
                InformationDialog.show(Messages.get("investigation.checkResults.title"),
                                       Messages.get("investigation.noresults"));
            }
        } catch (Throwable exception) {
            log.error("Failed to check results: " + exception.getMessage(), exception);
            ErrorDialog.newDialog().title(Messages.get("investigation.checkResults.title"))
                    .preamble(Messages.get("investigation.checkResults.error"))
                    .message(ErrorFormatter.format(exception))
                    .show();
        }
        return results;
    }

    /**
     * Returns the order associated with the investigation.
     *
     * @return the order. May be {@code null}
     */
    Order getOrder() {
        if (order == null) {
            order = InvestigationHelper.getOrder(investigation.getId());
        }
        return order;
    }

}
