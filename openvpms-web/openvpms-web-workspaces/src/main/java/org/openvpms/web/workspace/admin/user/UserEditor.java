/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.user;

import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.bound.BoundPasswordField;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;


/**
 * Editor for <em>security.user</em> instances.
 *
 * @author Tim Anderson
 */
public class UserEditor extends AbstractIMObjectEditor {

    /**
     * The confirm password property.
     */
    private final Property confirm;

    /**
     * The user rules.
     */
    private final UserRules rules;

    /**
     * The password editor.
     */
    private final PropertyComponentEditor passwordEditor;

    /**
     * The password field.
     */
    private final BoundPasswordField passwordField;

    /**
     * The confirm password editor.
     */
    private final PropertyComponentEditor confirmEditor;

    /**
     * The confirm password field.
     */
    private final BoundPasswordField confirmField;


    /**
     * Constructs an {@link UserEditor}.
     *
     * @param object  the object to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context. May be {@code null}.
     */
    public UserEditor(User object, IMObject parent, LayoutContext context) {
        super(object, parent, context);
        rules = ServiceHelper.getBean(UserRules.class);
        Property password = getPassword();
        confirm = new SimpleProperty("confirm", password.getString(), String.class,
                                     Messages.format("admin.user.password.confirm", password.getDisplayName()));

        passwordField = createPassword(password);
        passwordEditor = new PropertyComponentEditor(password, passwordField);
        addEditor(passwordEditor);

        confirmField = createPassword(confirm);
        confirm.clearModified(); // copy of the original password
        confirmEditor = new PropertyComponentEditor(confirm, confirmField);
        addEditor(confirmEditor);
    }

    /**
     * Sets the user's name.
     *
     * @param name the user's name
     */
    public void setName(String name) {
        getProperty("name").setValue(name);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateUniqueUserName(validator) && validatePassword(validator)
               && validateActive(validator);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LayoutStrategy();
    }

    /**
     * Creates a password field.
     *
     * @param property the password property
     * @return a new password field
     */
    private BoundPasswordField createPassword(Property property) {
        BoundPasswordField result = new BoundPasswordField(property);
        result.setStyleName(getLayoutContext().getComponentFactory().getStyle());
        return result;
    }

    /**
     * Validates that a user cannot be marked inactive while it is in use by a job.
     *
     * @param validator the validator
     * @return if the user is active, or
     */
    private boolean validateActive(Validator validator) {
        boolean valid = true;
        IMObject object = getObject();
        if (!object.isNew() && !getProperty("active").getBoolean()) {
            UserRules rules = ServiceHelper.getBean(UserRules.class);
            Entity job = rules.getJobUsedBy((User) object);
            if (job != null) {
                valid = false;
                validator.add(this, new ValidatorError(Messages.format("admin.user.requiredbyjob", job.getName())));
            }
        }
        return valid;
    }

    /**
     * Validates that the username is unique.
     *
     * @param validator the validator
     * @return {@code true} if the username is unique
     */
    private boolean validateUniqueUserName(Validator validator) {
        boolean valid = true;
        Property username = getProperty("username");
        String name = username.getString();
        if (name != null && rules.exists(name, (User) getObject())) {
            String message = Messages.format("admin.user.duplicate", name);
            ValidatorError error = new ValidatorError(getObject().getArchetype(), username.getName(), message);
            validator.add(username, error);
            valid = false;
        }
        return valid;
    }

    /**
     * Verifies that the passwords are the same.
     *
     * @param validator the validator
     * @return {@code true} if the passwords are the same
     */
    private boolean validatePassword(Validator validator) {
        boolean valid = false;
        Property password = getPassword();
        if (!password.isModified() && !confirm.isModified()) {
            // if neither has been modified, they are a copy of each other
            valid = true;
        } else if (passwordField.matches(confirmField)) {
            // need to compare the entered text. For hashed passwords, the hashed values aren't comparable
            valid = true;
        } else {
            ValidatorError error = new ValidatorError(getObject().getArchetype(), password.getName(),
                                                      Messages.get("admin.user.password.mismatch"));
            validator.add(password, error);
        }
        return valid;
    }

    /**
     * Returns the password property.
     *
     * @return the password property
     */
    private Property getPassword() {
        return getProperty("password");
    }

    /**
     * Layout strategy that adds a 'confirm password' field after the password.
     */
    private class LayoutStrategy extends UserLayoutStrategy {

        /**
         * Apply the layout strategy.
         * <p/>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            addComponent(new ComponentState(passwordEditor));
            return super.apply(object, properties, parent, context);
        }

        /**
         * Creates a set of components to be rendered from the supplied descriptors.
         *
         * @param object     the parent object
         * @param properties the properties
         * @param context    the layout context
         * @return the components
         */
        @Override
        protected ComponentSet createComponentSet(IMObject object, List<Property> properties,
                                                  LayoutContext context) {
            ComponentSet set = super.createComponentSet(object, properties, context);

            int index = set.indexOf("password");
            if (index != -1) {
                set.add(index + 1, new ComponentState(confirmEditor));
            }
            return set;
        }

    }
}
