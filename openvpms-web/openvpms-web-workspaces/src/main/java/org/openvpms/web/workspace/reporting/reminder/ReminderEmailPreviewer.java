/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailDialogFactory;
import org.openvpms.web.component.mail.MailEditor;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.reporting.ReportingException;
import org.openvpms.web.workspace.reporting.email.PracticeEmailAddresses;

import java.util.Collections;
import java.util.List;

/**
 * Previews email reminders.
 *
 * @author Tim Anderson
 */
public class ReminderEmailPreviewer extends AbstractPatientReminderPreviewer {

    /**
     * Constructs a {@link ReminderEmailPreviewer}.
     *
     * @param processor the processor to use to prepare reminders
     * @param help      the help context
     */
    public ReminderEmailPreviewer(ReminderEmailProcessor processor, HelpContext help) {
        super(processor, help);
    }

    /**
     * Previews reminders.
     *
     * @param reminders the reminders
     * @param processor the processor
     * @param help      the help context
     */
    @Override
    protected void preview(PatientReminders reminders, PatientReminderProcessor processor, HelpContext help) {
        EmailReminders state = (EmailReminders) reminders;
        Party practice = processor.getPractice();
        Party location = state.getLocation();
        Contact contact = state.getContact();
        Context context = state.createContext(practice);

        Contact from = null;

        try {
            PracticeEmailAddresses addresses = new PracticeEmailAddresses(
                    practice, Collections.singletonList(location), ContactArchetypes.REMINDER_PURPOSE,
                    ServiceHelper.getArchetypeService());
            from = addresses.getContact(location);
            if (from == null) {
                from = addresses.getContact(practice);
            }
        } catch (ReportingException ignore) {
            // no email address
        }

        // set up a MailContext restrict the from address to that which the reminder sending would use
        Contact available = from;
        MailContext mailContext = new CustomerMailContext(context, help) {
            @Override
            public List<Contact> getFromAddresses() {
                return (available != null) ? Collections.singletonList(available) : Collections.emptyList();
            }
        };
        MailDialogFactory factory = ServiceHelper.getBean(MailDialogFactory.class);
        MailDialog dialog = factory.create(mailContext, contact, new DefaultLayoutContext(context, help));
        MailEditor editor = dialog.getMailEditor();
        editor.setSubject(state.getSubject(context));
        editor.setMessage(state.getMessage(context));
        if (processor.getConfig().getEmailAttachments()) {
            editor.addAttachment(state.createAttachment(context));
        }
        dialog.show();
    }
}
