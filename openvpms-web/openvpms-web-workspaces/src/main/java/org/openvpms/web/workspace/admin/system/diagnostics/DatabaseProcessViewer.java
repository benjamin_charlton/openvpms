/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.table.DefaultTableModel;
import nextapp.echo2.app.table.TableModel;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.factory.TableFactory;
import org.openvpms.web.echo.table.DefaultTableHeaderRenderer;
import org.openvpms.web.echo.table.KeyTable;
import org.openvpms.web.system.ServiceHelper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates a listing of database processes using the MySQL {@code SHOW FULL PROCESSLIST} command.
 *
 * @author Tim Anderson
 */
class DatabaseProcessViewer extends AbstractDiagnosticTab {

    /**
     * The columns.
     */
    private final String[] columns = new String[]{"Host", "Id", "User", "Database", "Command", "State", "Info"};

    /**
     * Snapshot of the MySQL process data.
     */
    private List<String[]> snapshot;

    /**
     * Constructs a {@link DatabaseProcessViewer}.
     */
    DatabaseProcessViewer() {
        super("admin.system.diagnostic.dbprocess");
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        List<String[]> data = getData(false);
        if (data != null) {
            result = toCSV("database.csv", columns, data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component result = null;
        List<String[]> data = getData(true);
        if (data != null) {
            String[][] ts = data.toArray(new String[0][0]);
            TableModel model = new DefaultTableModel(ts, columns);
            KeyTable table = TableFactory.create(model);
            table.setDefaultHeaderRenderer(DefaultTableHeaderRenderer.DEFAULT);
            table.setHeaderFixed(true);
            result = table;
        }
        return result;
    }

    /**
     * Returns the MySQL process data.
     *
     * @param refresh if {@code true}, refresh the data if it has been collected previously
     * @return the process data, or {@code null} if it cannot be retrieved
     */
    private List<String[]> getData(boolean refresh) {
        if (snapshot == null || refresh) {
            DataSource dataSource = ServiceHelper.getBean(DataSource.class);
            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {
                java.sql.ResultSet rs = statement.executeQuery("SHOW FULL PROCESSLIST");
                snapshot = new ArrayList<>();
                while (rs.next()) {
                    snapshot.add(new String[]{rs.getString("host"), rs.getString("id"), rs.getString("user"),
                                              rs.getString("db"), rs.getString("command"), rs.getString("state"),
                                              rs.getString("info")});
                }
            } catch (Throwable exception) {
                snapshot = null;
                ErrorHelper.show(exception);
            }
        }
        return snapshot;
    }
}
