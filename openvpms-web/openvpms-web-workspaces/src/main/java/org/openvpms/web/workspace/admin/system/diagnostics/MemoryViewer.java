/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.workspace.admin.system.cache.Memory;

/**
 * Memory viewer.
 *
 * @author Tim Anderson
 */
class MemoryViewer extends AbstractDiagnosticTab {

    /**
     * The memory.
     */
    private final Memory memory;

    /**
     * The memory snapshot.
     */
    private String[][] snapshot;

    /**
     * The columns names.
     */
    private static final String[] COLUMNS = new String[]{"Name", "Value"};


    /**
     * Constructs a {@link MemoryViewer}.
     */
    MemoryViewer() {
        super("admin.system.diagnostic.memory");
        memory = new Memory(true); // include current allocation
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        String[][] data = getData(false);
        Document result = null;
        if (data != null) {
            return toCSV("memory.csv", COLUMNS, data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        getData(true);
        return memory.getComponent();
    }

    /**
     * Returns the memory data.
     *
     * @param refresh if {@code true}, refresh the data if it has been collected previously
     * @return the memory data
     */
    private String[][] getData(boolean refresh) {
        if (snapshot == null || refresh) {
            snapshot = new String[5][2];
            memory.refresh();
            populate(snapshot, 0, memory.getTotalMemory());
            populate(snapshot, 1, memory.getFreeMemory());
            populate(snapshot, 2, memory.getMemoryUse());
            populate(snapshot, 3, memory.getAllocatedTotal());
            populate(snapshot, 4, memory.getAllocatedFree());
        }
        return snapshot;
    }

    /**
     * Populates the data at the specified index with the name and value of the supplied property.
     *
     * @param data     the data
     * @param index    the index
     * @param property the property
     */
    private void populate(String[][] data, int index, Property property) {
        data[index][0] = property.getDisplayName();
        data[index][1] = property.getString();
    }

}
