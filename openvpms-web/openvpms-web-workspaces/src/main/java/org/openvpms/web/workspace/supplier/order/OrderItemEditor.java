/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2018 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.order;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.rules.supplier.OrderStatus;
import org.openvpms.archetype.rules.supplier.ProductOrder;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.supplier.SupplierStockItemEditor;

import java.math.BigDecimal;
import java.util.List;

import static org.openvpms.web.echo.style.Styles.CELL_SPACING;
import static org.openvpms.web.echo.style.Styles.WIDE_CELL_SPACING;


/**
 * An editor for {@link Act}s which have an archetype of <em>act.supplierOrderItem</em>.
 *
 * @author Tim Anderson
 */
public class OrderItemEditor extends SupplierStockItemEditor {

    /**
     * Determines if the act is IN_PROGRESS at construction.
     */
    private final boolean inProgress;

    /**
     * Determines if the act was POSTED, ACCEPTED or CANCELLED at construction. If so, only a limited set of properties
     * may be edited.
     */
    private final boolean limitEditing;

    /**
     * The stock location.
     */
    private final Reference stockLocation;

    /**
     * The edit context.
     */
    private final OrderEditContext editContext;

    /**
     * The stock on-hand property.
     */
    private final SimpleProperty onHand = new SimpleProperty("onHand", null, BigDecimal.class,
                                                             Messages.get("product.stock.onhand"), true);

    /**
     * Determines if the product-supplier details are read-only.
     */
    private boolean productSupplierReadOnly;

    /**
     * Quantity node name.
     */
    private static final String QUANTITY = "quantity";

    /**
     * Received quantity node name.
     */
    private static final String RECEIVED_QUANTITY = "receivedQuantity";

    /**
     * Cancelled quantity node name.
     */
    private static final String CANCELLED_QUANTITY = "cancelledQuantity";

    /**
     * Constructs an {@link OrderItemEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent act
     * @param context the layout context
     */
    public OrderItemEditor(FinancialAct act, Act parent, LayoutContext context) {
        this(act, parent, new OrderEditContext(), context);
    }

    /**
     * Constructs an {@link OrderItemEditor}.
     *
     * @param act           the act to edit
     * @param parent        the parent act
     * @param context       the edit context
     * @param layoutContext the layout context
     */
    public OrderItemEditor(FinancialAct act, Act parent, OrderEditContext context, LayoutContext layoutContext) {
        super(act, parent, layoutContext);
        if (!act.isA(SupplierArchetypes.ORDER_ITEM)) {
            throw new IllegalArgumentException("Invalid act type: " + act.getArchetype());
        }
        if (parent != null) {
            String status = parent.getStatus();
            inProgress = OrderStatus.IN_PROGRESS.equals(status);
            limitEditing = OrderStatus.POSTED.equals(status) || OrderStatus.ACCEPTED.equals(status)
                           || OrderStatus.CANCELLED.equals(status);
            stockLocation = getStockLocationRef();
        } else {
            inProgress = true;
            limitEditing = false;
            stockLocation = null;
        }
        productSupplierReadOnly = getMatchingProductSupplier() != null;
        editContext = context;
        updateStockOnHand();
    }

    /**
     * Returns the received quantity.
     *
     * @return the received quantity
     */
    public BigDecimal getReceivedQuantity() {
        return (BigDecimal) getProperty(RECEIVED_QUANTITY).getValue();
    }

    /**
     * Sets the received quantity.
     *
     * @param quantity the received quantity
     */
    public void setReceivedQuantity(BigDecimal quantity) {
        getProperty(RECEIVED_QUANTITY).setValue(quantity);
    }

    /**
     * Returns the cancelled quantity.
     *
     * @return the cancelled quantity
     */
    public BigDecimal getCancelledQuantity() {
        return (BigDecimal) getProperty(CANCELLED_QUANTITY).getValue();
    }

    /**
     * Sets the cancelled quantity.
     *
     * @param quantity the cancelled quantity
     */
    public void setCancelledQuantity(BigDecimal quantity) {
        getProperty(CANCELLED_QUANTITY).setValue(quantity);
    }

    /**
     * Determines if the product is restricted.
     *
     * @return {@code true} if the product is restricted, otherwise {@code false}
     */
    public boolean isRestrictedProduct() {
        Product product = getProduct();
        return (product != null && getProductRules().isRestricted(product));
    }

    /**
     * Invoked when the product is changed, to update prices.
     *
     * @param product the product. May be {@code null}
     */
    @Override
    protected void productModified(Product product) {
        super.productModified(product);
        if (changeLayoutRequired()) {
            onLayout();
        }
        updateStockOnHand();
        checkOrdered(product);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean valid = super.doValidation(validator);
        if (valid) {
            BigDecimal quantity = getQuantity();
            BigDecimal cancelled = getCancelledQuantity();
            if (cancelled.compareTo(quantity) > 0) {
                valid = false;
                Property property = getProperty(QUANTITY);
                String message = Messages.format("supplier.order.invalidCancelledQuantity", quantity, cancelled);
                ValidatorError error = new ValidatorError(property, message);
                validator.add(property, error);
            }
        }
        return valid;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LayoutStrategy();
    }

    /**
     * Determines if the layout needs to change after the product changes.
     * The layout needs to change if the package details need to be made read-only or editable.
     *
     * @return {@code true} if the layout needs to change
     */
    private boolean changeLayoutRequired() {
        boolean result = false;
        ProductSupplier current = getProductSupplier();
        if (productSupplierReadOnly && current == null) {
            productSupplierReadOnly = false;
            result = true;
        } else if (!productSupplierReadOnly && current != null) {
            productSupplierReadOnly = true;
            result = true;
        }
        return result;
    }

    /**
     * Determines if the product has already been ordered.
     * <p/>
     * If so, displays the orders. and clears the product if the user presses 'No'.
     *
     * @param product the product. May be {@code null}
     */
    private void checkOrdered(Product product) {
        if (product != null && stockLocation != null) {
            List<ProductOrder> productOrders = editContext.getOrders(product.getObjectReference(), stockLocation);
            if (!productOrders.isEmpty()) {
                ProductOnOrderDialog dialog = new ProductOnOrderDialog(product, productOrders);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onNo() {
                        setProduct(null);
                    }
                });
                dialog.show();
            }
        }
    }

    /**
     * Updates the stock on hand.
     */
    private void updateStockOnHand() {
        Reference product = getProductRef();
        if (product == null || stockLocation == null) {
            onHand.setValue(null);
        } else {
            BigDecimal stock = editContext.getStock().getStock(product, stockLocation);
            onHand.setValue(stock);
        }
    }

    protected class LayoutStrategy extends AbstractLayoutStrategy {

        /**
         * Apply the layout strategy.
         * <p/>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            if (inProgress) {
                // hide the received and cancelled quantity if the order is IN_PROGRESS
                setArchetypeNodes(ArchetypeNodes.all().exclude(RECEIVED_QUANTITY, CANCELLED_QUANTITY));
            }
            if (limitEditing || productSupplierReadOnly) {
                MutablePropertySet set = new MutablePropertySet(properties);
                if (limitEditing) {
                    for (Property property : set.getProperties()) {
                        if (!property.isReadOnly() && !property.getName().equals(CANCELLED_QUANTITY)) {
                            set.setReadOnly(property.getName());
                        }
                    }
                } else {
                    markReadOnlyIfSet(REORDER_CODE, set);
                    markReadOnlyIfSet(REORDER_DESCRIPTION, set);
                    if (markReadOnlyIfNonZero(PACKAGE_SIZE, set)) {
                        // only mark the units read-only if there is a package size, as the units default
                        markReadOnlyIfSet(PACKAGE_UNITS, set);
                    }
                    markReadOnlyIfNonZero(LIST_PRICE, set);
                    markReadOnlyIfNonZero(UNIT_PRICE, set);
                }
                properties = set;
            }
            ComponentState quantity = createComponent(properties.get(QUANTITY), object, context);
            ComponentState stockQuantity = createStockOnHand(context);
            Row onHand = RowFactory.create(CELL_SPACING, RowFactory.rightAlign(), stockQuantity.getLabel(),
                                           stockQuantity.getComponent());
            Row row = RowFactory.create(WIDE_CELL_SPACING, quantity.getComponent(), onHand);
            addComponent(new ComponentState(row, quantity.getProperty()));
            return super.apply(object, properties, parent, context);
        }

        /**
         * Creates a component for the stock on hand.
         *
         * @param context the layout context
         * @return a new component for the stock on hand
         */
        private ComponentState createStockOnHand(LayoutContext context) {
            Component component = context.getComponentFactory().create(onHand);
            return new ComponentState(component, onHand);
        }

        /**
         * Marks a property read-only if it has a value.
         *
         * @param name       the property name
         * @param properties the properties
         */
        private void markReadOnlyIfSet(String name, MutablePropertySet properties) {
            Property property = properties.get(name);
            if (property != null && property.getValue() != null) {
                properties.setReadOnly(name);
            }
        }

        /**
         * Marks a property read-only if it is non-zero.
         *
         * @param name       the property name
         * @param properties the properties
         * @return {@code true} if it was marked read-only, otherwise {@code false}
         */
        private boolean markReadOnlyIfNonZero(String name, MutablePropertySet properties) {
            Property property = properties.get(name);
            if (property != null && !MathRules.isZero(property.getBigDecimal(BigDecimal.ZERO))) {
                properties.setReadOnly(name);
                return true;
            }
            return false;
        }
    }
}