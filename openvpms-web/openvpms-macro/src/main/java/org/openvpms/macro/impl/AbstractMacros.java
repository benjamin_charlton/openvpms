/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.macro.impl;

import org.openvpms.component.system.common.util.Variables;
import org.openvpms.macro.MacroException;
import org.openvpms.macro.Macros;
import org.openvpms.macro.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Abstract implementation of {@link Macro}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMacros implements Macros {

    /**
     * The macros, keyed on code.
     */
    private final Map<String, Macro> macros = Collections.synchronizedMap(new HashMap<>());

    /**
     * The per-thread variables. These are required so that variables may be supplied to nested macros when they
     * are invoked via macro:eval().
     */
    private final ThreadLocal<ScopedVariables> scopedVariables = new ThreadLocal<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractMacros.class);

    /**
     * Determines if a macro exists.
     *
     * @param macro the macro name
     * @return {@code true} if the macro exists
     */
    @Override
    public boolean exists(String macro) {
        return macros.containsKey(macro);
    }

    /**
     * Runs a macro.
     * <p/>
     * If the macro code is preceded by a numeric expression, the value will be declared as a variable <em>$number</em>.
     *
     * @param macro  the macro code
     * @param object the object to evaluate the macro against. May be {@code null}
     * @return the result of the macro. May be {@code null} if the macro doesn't exist, or evaluates {@code null}
     * @throws MacroException for any error
     */
    @Override
    public String run(String macro, Object object) {
        return run(macro, object, null);
    }

    /**
     * Runs a macro.
     * <p/>
     * If the macro code is preceded by a numeric expression, the value will be declared as a variable <em>$number</em>.
     *
     * @param macro     the macro code
     * @param object    the object to evaluate the macro against
     * @param variables variables to supply to the macro. May be {@code null}
     * @return the result of the macro. May be {@code null} if the macro doesn't exist, or evaluates {@code null}
     * @throws MacroException for any error
     */
    @Override
    public String run(String macro, Object object, Variables variables) {
        String result = null;
        Token token = Token.parse(macro);
        Macro m = macros.get(token.getToken());
        if (m != null) {
            ScopedVariables scoped = pushVariables(variables);
            try {
                MacroContext context = createMacroContext(macros, object, scoped);
                result = context.run(m, token.getNumericPrefix());
            } catch (MacroException exception) {
                throw exception;
            } catch (Throwable exception) {
                throw new MacroException("Failed to evaluate macro=" + macro, exception);
            } finally {
                popVariables(variables, scoped);
            }
        }
        return result;
    }

    /**
     * Runs all macros in the supplied text.
     * <p/>
     * When a macro is encountered, it will be replaced with the macro value.
     * <p/>
     * If a macro is preceded by a numeric expression, the value will be declared as a variable <em>$number</em>.
     *
     * @param text   the text to parse
     * @param object the object to evaluate macros against
     * @return the text will macros substituted for their values
     */
    @Override
    public String runAll(String text, Object object) {
        return runAll(text, object, null, null);
    }

    /**
     * Runs all macros in the supplied text.
     * <p/>
     * When a macro is encountered, it will be replaced with the macro value.
     * <p/>
     * If a macro is preceded by a numeric expression, the value will be declared as a variable <em>$number</em>.
     *
     * @param text      the text to parse
     * @param object    the object to evaluate macros against
     * @param variables variables to supply to macros. May be {@code null}
     * @param position  tracks the cursor position. The cursor position will be moved if macros before it are expanded.
     *                  May be {@code null}
     * @return the text will macros substituted for their values
     */
    @Override
    public String runAll(String text, Object object, Variables variables, Position position) {
        return runAll(text, object, variables, position, false);
    }

    /**
     * Runs all macros in the supplied text.
     * <p/>
     * When a macro is encountered, it will be replaced with the macro value.
     * <p/>
     * If a macro is preceded by a numeric expression, the value will be declared as a variable <em>$number</em>.
     * <p/>
     * If a macro fails to expand, the macro will be left in the text, unless {@code failOnError} is {@code true},
     * where an exception will be thrown.
     *
     * @param text        the text to parse
     * @param object      the object to evaluate macros against. May be {@code null}
     * @param variables   variables to supply to macros. May be {@code null}
     * @param position    tracks the cursor position. The cursor position will be moved if macros before it are expanded.
     *                    May be {@code null}
     * @param failOnError if {@code true}, throw an exception if a macro fails to expand
     * @return the text will macros substituted for their values
     * @throws MacroException if an error occurs, and {@code failOnError == true}
     */
    @Override
    public String runAll(String text, Object object, Variables variables, Position position, boolean failOnError) {
        StringBuilder result = new StringBuilder();
        ScopedVariables scoped = pushVariables(variables);
        int oldPos = position != null ? position.getOldPosition() : -1;
        int index = 0;   // index into the text
        try {
            MacroContext context = createMacroContext(macros, object, scoped);

            StringTokenizer tokens = new StringTokenizer(text, " \t\n\r", true);
            while (tokens.hasMoreTokens()) {
                Token token = Token.parse(tokens.nextToken());
                Macro macro = macros.get(token.getToken());
                int tokenLength = token.getText().length();
                String value;
                boolean expanded = false;
                if (macro != null) {
                    try {
                        value = context.run(macro, token.getNumericPrefix());
                        expanded = true;
                    } catch (Throwable exception) {
                        if (failOnError) {
                            if (exception instanceof MacroException) {
                                throw exception;
                            } else {
                                throw new MacroException(exception.getMessage(), exception);
                            }
                        } else {
                            log.warn("Failed to evaluate macro: " + exception.getMessage(), exception);
                            value = token.getText();
                        }
                    }
                } else {
                    value = token.getText();
                }
                if (value != null) {
                    result.append(value);
                }
                if (oldPos != -1 && index <= oldPos) {
                    index += tokenLength;
                    if (index > oldPos) {
                        int newPos;
                        if (expanded) {
                            // move the position to the end of the expanded text
                            newPos = result.length();
                        } else if (result.length() > index) {
                            // new text is longer, so adjust the cursor position
                            newPos = oldPos + (result.length() - index);
                        } else if (index > result.length()) {
                            // new text is shorter, so adjust the cursor position
                            newPos = oldPos - (index - result.length());
                        } else {
                            newPos = oldPos;
                        }
                        position.setNewPosition(newPos);
                        oldPos = -1;
                    }
                }
            }
        } finally {
            popVariables(variables, scoped);
        }
        return result.toString();
    }

    /**
     * Adds a macro.
     *
     * @param macro the macro to add
     */
    protected void add(Macro macro) {
        macros.put(macro.getCode(), macro);
    }

    /**
     * Removes a macro.
     *
     * @param code the macro code
     */
    protected void remove(String code) {
        macros.remove(code);
    }

    /**
     * Creates a new macro context.
     *
     * @param macros    the macros, keyed on code
     * @param object    the object to evaluate macros against
     * @param variables the scoped variables
     * @return a new macro context
     */
    protected abstract MacroContext createMacroContext(Map<String, Macro> macros, Object object, Variables variables);

    private ScopedVariables pushVariables(Variables variables) {
        ScopedVariables scoped = scopedVariables.get();
        if (scoped == null && variables != null) {
            scoped = new ScopedVariables();
            scopedVariables.set(scoped);
        }
        if (scoped != null && variables != null) {
            scoped.push(variables);
        }
        return scoped;
    }

    private void popVariables(Variables variables, ScopedVariables scoped) {
        if (scoped != null && variables != null) {
            scoped.pop();
            if (scoped.isEmpty()) {
                scopedVariables.remove();
            }
        }
    }

    private static class ScopedVariables implements Variables {

        private final List<Variables> stack = new ArrayList<>();

        /**
         * Returns a variable value.
         *
         * @param name the variable name
         * @return the variable value. May be {@code null}
         */
        @Override
        public Object get(String name) {
            for (ListIterator<Variables> iterator = stack.listIterator(stack.size()); iterator.hasPrevious(); ) {
                Variables variables = iterator.previous();
                if (variables.exists(name)) {
                    return variables.get(name);
                }
            }
            return null;
        }

        /**
         * Determines if a variable exists.
         *
         * @param name the variable name
         * @return {@code true} if the variable exists
         */
        @Override
        public boolean exists(String name) {
            for (ListIterator<Variables> iterator = stack.listIterator(stack.size()); iterator.hasPrevious(); ) {
                Variables variables = iterator.previous();
                if (variables.exists(name)) {
                    return true;
                }
            }
            return false;
        }

        public boolean isEmpty() {
            return stack.isEmpty();
        }

        void push(Variables variables) {
            stack.add(variables);
        }

        void pop() {
            stack.remove(stack.size() - 1);
        }
    }

    /**
     * Helper to parse text into a token and optional number.
     */
    private static class Token {

        /**
         * The original text.
         */
        private final String text;

        /**
         * The numeric prefix parsed from the text. May be {@code null}
         */
        private final String numericPrefix;

        /**
         * The token parsed from the text.
         */
        private final String token;

        /**
         * Constructs a {@code Token}.
         *
         * @param text          the original text
         * @param token         the parsed token
         * @param numericPrefix the numeric prefix. May be {@code null}
         */
        Token(String text, String token, String numericPrefix) {
            this.text = text;
            this.token = token;
            this.numericPrefix = numericPrefix;
        }

        /**
         * Returns the original text.
         *
         * @return the text
         */
        public String getText() {
            return text;
        }

        /**
         * Returns the token.
         *
         * @return the token
         */
        public String getToken() {
            return token;
        }

        /**
         * Returns any numeric-like prefix before the token.
         *
         * @return the numeric prefix, or {@code null} if none was found
         */
        public String getNumericPrefix() {
            return numericPrefix;
        }

        /**
         * Parses a token from text.
         *
         * @param text the text to parse
         * @return a new {@link Token}
         */
        public static Token parse(String text) {
            // If the text starts with numbers strip numbers and create a number
            // variable. If any left pass token to test for macro.
            int index = 0;
            while (index < text.length() && isNumeric(text.charAt(index))) {
                ++index;
            }
            String token = text;
            String numeric = null;
            if (index != 0) {
                numeric = text.substring(0, index);
                token = token.substring(index);
            }
            return new Token(text, token, numeric);
        }

        /**
         * Determines if a character is numeric. This supports no.s in decimal
         * and fraction format.
         *
         * @param ch the character
         * @return {@code true} if {@code ch} is one of '0'..'9','.' or '/'
         */
        private static boolean isNumeric(char ch) {
            return Character.isDigit(ch) || ch == '.' || ch == '/';
        }
    }
}