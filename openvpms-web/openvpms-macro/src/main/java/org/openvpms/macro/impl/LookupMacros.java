/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.macro.impl;

import org.apache.commons.jxpath.FunctionLibrary;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.AbstractArchetypeServiceListener;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.IArchetypeServiceListener;
import org.openvpms.component.business.service.archetype.ReadOnlyArchetypeService;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.macro.Macros;
import org.openvpms.report.ReportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Collection;
import java.util.Map;


/**
 * An implementation of {@link Macros} that obtains macro definitions from <em>lookup.macro</em> and
 * <em>lookup.macroReport</em> lookups.
 * <p/>
 * These are monitored for updates to ensure that the macros reflect those in the database.
 *
 * @author Tim Anderson
 */
public class LookupMacros extends AbstractMacros implements DisposableBean {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The macro factory.
     */
    private final MacroFactory factory;

    /**
     * The factory for functions.
     */
    private final ArchetypeFunctionsFactory functionsFactory;

    /**
     * The listener to monitor macro updates.
     */
    private final IArchetypeServiceListener listener;

    /**
     * The JXPath extension functions that macros may invoke.
     */
    private FunctionLibrary functions;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LookupMacros.class);

    /**
     * Constructs a {@link LookupMacros}.
     *
     * @param lookups   the lookup service
     * @param service   the archetype service
     * @param factory   the document handlers
     * @param functions the JXPath extension functions that macros may invoke
     * @throws ArchetypeServiceException for any archetype service error
     */
    public LookupMacros(ILookupService lookups, IArchetypeService service, ReportFactory factory,
                        ArchetypeFunctionsFactory functions) {
        this.service = service;
        this.factory = new MacroFactory(service, factory);
        this.functionsFactory = functions;
        for (String shortName : MacroArchetypes.LOOKUP_MACROS) {
            addMacros(shortName, lookups);
        }
        listener = new AbstractArchetypeServiceListener() {
            public void saved(IMObject object) {
                onSaved((Lookup) object);
            }

            public void removed(IMObject object) {
                delete((Lookup) object);
            }
        };
        for (String shortName : MacroArchetypes.LOOKUP_MACROS) {
            service.addListener(shortName, listener);
        }
    }

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     */
    @Override
    public void destroy() {
        for (String shortName : MacroArchetypes.LOOKUP_MACROS) {
            service.removeListener(shortName, listener);
        }
    }

    /**
     * Creates a new macro context.
     *
     * @param macros    the macros, keyed on code
     * @param object    the object to evaluate macros against
     * @param variables the scoped variables
     * @return a new macro context
     */
    @Override
    protected MacroContext createMacroContext(Map<String, Macro> macros, Object object, Variables variables) {
        return new MacroContext(macros, factory, object, variables, getFunctions());
    }

    /**
     * Returns the function library.
     * <p/>
     * This needs to be lazily constructed as there is a circular dependency between the concrete functionsFactory and
     * this.
     *
     * @return function library
     */
    private synchronized FunctionLibrary getFunctions() {
        if (functions == null) {
            // Create a read-only archetype service to ensure that macros cannot update the database
            // Need to create the functions here in order to support registration of MacroFunctions
            functions = functionsFactory.create(new ReadOnlyArchetypeService(service), false);
        }
        return functions;
    }

    /**
     * Cache macros of the specified archetype short name.
     *
     * @param shortName the archetype short name
     * @param service   the lookup service
     */
    private void addMacros(String shortName, ILookupService service) {
        Collection<Lookup> macros = service.getLookups(shortName);
        for (Lookup lookup : macros) {
            if (lookup.isActive()) {
                add(lookup);
            }
        }
    }

    /**
     * Invoked when a lookup is saved.
     *
     * @param lookup the lookup
     */
    private void onSaved(Lookup lookup) {
        if (lookup.isActive()) {
            add(lookup);
        } else {
            delete(lookup);
        }
    }

    /**
     * Adds a macro to the cache.
     *
     * @param lookup the macro definition
     */
    private void add(Lookup lookup) {
        try {
            add(factory.create(lookup));
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
        }
    }

    /**
     * Removes a macro from the cache.
     *
     * @param lookup the macro definition
     */
    private void delete(Lookup lookup) {
        remove(lookup.getCode());
    }

}
