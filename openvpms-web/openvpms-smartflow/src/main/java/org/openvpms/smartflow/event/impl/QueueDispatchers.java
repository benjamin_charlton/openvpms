/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.event.impl;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.smartflow.event.EventDispatcher;
import org.openvpms.smartflow.event.EventStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Manages a set of {@link QueueDispatcher} instances.
 * <br/>
 * NOTE: whilst multiple practice locations sharing the one Smart Flow Sheet Clinic API Key is supported, the location
 * selected for treatment events is not guaranteed to be the same between restarts or practice location changes.
 * <br/>
 * E.g. give two locations A, and B, with the same API key, the location that is first registered will be the one
 * selected for use.
 *
 * @author Tim Anderson
 */
class QueueDispatchers {

    /**
     * The queue dispatcher factory.
     */
    private final QueueDispatcherFactory factory;

    /**
     * Practice locations keyed on clinic API key.
     */
    private final Map<String, Set<Party>> locationsByKey = new HashMap<>();

    /**
     * Clinic API key keyed on location.
     */
    private final Map<Party, String> keysByLocation = new HashMap<>();

    /**
     * The queue dispatchers, keyed on clinic API key.
     */
    private final Map<String, QueueDispatcher> dispatchers = new HashMap<>();

    /**
     * The executor service for starting dispatchers that failed to start.
     */
    private final ScheduledExecutorService executorService;

    /**
     * Determines if this has been started.
     */
    private volatile boolean started;

    /**
     * Determines if this has been destroyed
     */
    private volatile boolean destroyed;

    /**
     * The delay in seconds, before attempting to start a dispatcher that has failed previously.
     */
    private int startDelay = 60;

    /**
     * Thread factory for new threads.
     */
    private final static ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("SFS Queue Start")
            .setDaemon(true).build();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(QueueDispatchers.class);

    /**
     * Constructs a {@link QueueDispatchers}.
     *
     * @param factory the queue dispatcher factory
     */
    public QueueDispatchers(QueueDispatcherFactory factory) {
        this.factory = factory;
        executorService = Executors.newSingleThreadScheduledExecutor(threadFactory);
    }

    /**
     * Returns the queue dispatchers.
     *
     * @return the queue dispatchers
     */
    public synchronized List<QueueDispatcher> getDispatchers() {
        return new ArrayList<>(dispatchers.values());
    }

    /**
     * Adds a practice location.
     * <p>
     * If the practice location has an API key, this will create a {@link QueueDispatcher} to read the queue.
     * <br/>
     * If the practice location has no API key, any existing {@link QueueDispatcher} will be removed.
     *
     * @param location the practice location
     * @return the queue dispatcher, if one was added
     */
    public synchronized QueueDispatcher add(Party location) {
        QueueDispatcher dispatcher = null;
        String existingKey = keysByLocation.get(location);
        String newKey;
        try {
            newKey = (location.isActive()) ? factory.getClinicAPIKey(location) : null;
        } catch (Throwable exception) {
            newKey = null;
            log.error(exception.getMessage(), exception);
        }
        if (!Objects.equals(existingKey, newKey)) {
            if (existingKey == null) {
                if (!StringUtils.isEmpty(newKey)) {
                    dispatcher = addKey(newKey, location);
                }
            } else {
                removeKey(existingKey, location);
                if (!StringUtils.isEmpty(newKey)) {
                    dispatcher = addKey(newKey, location);
                }
            }
        }
        return dispatcher;
    }

    /**
     * Removes the {@link QueueDispatcher} associated with a practice location, if any.
     *
     * @param location the practice location
     */
    public synchronized void remove(Party location) {
        String existingKey = keysByLocation.get(location);
        if (existingKey != null) {
            removeKey(existingKey, location);
        }
    }

    /**
     * Returns the status of events at the specified location.
     *
     * @param location the location
     * @return the event status
     */
    public synchronized EventStatus getStatus(Party location) {
        QueueDispatcher dispatcher = getQueueDispatcher(location);
        return (dispatcher != null) ? dispatcher.getStatus() : new EventStatus(null, null, null);
    }

    /**
     * Starts the dispatchers.
     */
    public void start() {
        if (!started && !destroyed) {
            started = true;
            for (QueueDispatcher dispatcher : getDispatchers()) {
                start(dispatcher);
            }
        }
    }

    /**
     * Destroys this.
     */
    public void destroy() {
        if (!destroyed) {
            started = false;
            destroyed = true;
            try {
                for (QueueDispatcher dispatcher : getDispatchers()) {
                    dispatcher.destroy();
                }
            } finally {
                executorService.shutdown();
                try {
                    executorService.awaitTermination(30, TimeUnit.SECONDS);
                } catch (Throwable exception) {
                    log.error("Error waiting for threads to terminate: " + exception.getMessage(), exception);
                }
            }
        }
    }

    /**
     * Sets the delay for starting dispatchers after failure.
     *
     * @param seconds the seconds to delay
     */
    protected void setStartDelay(int seconds) {
        this.startDelay = seconds;
    }

    /**
     * Returns the {@link QueueDispatcher} for the specified location.
     *
     * @param location location
     * @return the dispatcher, or {@code null} if none exists
     */
    private QueueDispatcher getQueueDispatcher(Party location) {
        String key = keysByLocation.get(location);
        return (key != null) ? dispatchers.get(key) : null;
    }

    /**
     * Adds an API key for the specified practice location.
     * <p>
     * If the key is not already registered, this creates an {@link EventDispatcher} to handle messages read from
     * the SFS Azure Service Bus queue.
     *
     * @param key      the clinic API key
     * @param location the practice location
     * @return the queue dispatcher, or {@code null} if none was added
     */
    private QueueDispatcher addKey(String key, Party location) {
        QueueDispatcher dispatcher = null;
        try {
            Set<Party> locations = locationsByKey.computeIfAbsent(key, k -> new HashSet<>());
            if (locations.isEmpty()) {
                dispatcher = addDispatcher(key, location);
            }
            if (!locations.contains(location)) {
                if (dispatcher == null) {
                    StringBuilder names = new StringBuilder();
                    for (Party party : locations) {
                        if (names.length() != 0) {
                            names.append(", ");
                        }
                        names.append('\'').append(party.getName()).append('\'');
                    }
                    log.error("Practice location='" + location.getName() + "' shares a Smart Flow Sheet Clinic API Key"
                              + " with: " + names
                              + ".\nThe location associated with any treatments is non-deterministic");
                }
                locations.add(location);
            }
            keysByLocation.put(location, key);
        } catch (Throwable exception) {
            log.error("Failed to initialise Smart Flow Sheet queue for location=" + location.getName(), exception);
        }
        return dispatcher;
    }

    /**
     * Adds a dispatcher for the specified practice API key and practice location.
     * <p/>
     * If the dispatchers have been started, the new dispatcher will also be started.
     *
     * @param key      the API key
     * @param location the practice location
     * @return the queue dispatcher
     */
    private QueueDispatcher addDispatcher(String key, Party location) {
        QueueDispatcher dispatcher = factory.createQueueDispatcher(location);
        dispatchers.put(key, dispatcher);
        start(dispatcher);
        return dispatcher;
    }

    /**
     * Starts a dispatcher.
     * <p/>
     * The dispatcher will only be started if this is started, and it hasn't been destroyed.
     * </>
     * If the dispatcher fails to start, it will be scheduled for another attempt by
     * {@link #scheduleStart(QueueDispatcher)}
     *
     * @param dispatcher the dispatcher to start.
     */
    private void start(QueueDispatcher dispatcher) {
        if (started && !dispatcher.isDestroyed()) {
            if (!dispatcher.start() && !dispatcher.isDestroyed()) {
                // failed to start, so try again
                scheduleStart(dispatcher);
            }
        }
    }

    /**
     * Schedules a start of a dispatcher.
     *
     * @param dispatcher the dispatcher
     */
    private void scheduleStart(QueueDispatcher dispatcher) {
        log.info("Scheduling start of Azure Service Bus Queue for " + dispatcher.getLocation().getName()
                 + " in " + startDelay + "s");
        executorService.schedule(() -> start(dispatcher), startDelay, TimeUnit.SECONDS);
    }

    /**
     * Removes an an API key for the specified practice location.
     * <br/>
     * This only will destroy the associated {@link QueueDispatcher} when there are no more locations associated with
     * it.
     *
     * @param key      the API key
     * @param location the practice location
     */
    private void removeKey(String key, Party location) {
        boolean reset = false;
        Set<Party> locations = locationsByKey.get(key);
        if (locations != null) {
            if (locations.remove(location)) {
                if (locations.isEmpty()) {
                    reset = true;
                }
            }
        }
        keysByLocation.remove(location);
        if (reset) {
            // no more locations associated with the key, so remove the dispatcher.
            QueueDispatcher dispatcher = dispatchers.remove(key);
            if (dispatcher != null) {
                dispatcher.stop();
            }
        } else if (locations != null && !locations.isEmpty()) {
            QueueDispatcher dispatcher = dispatchers.get(key);
            if (dispatcher != null && Objects.equals(dispatcher.getLocation(), location)) {
                // the dispatcher was shared amongst multiple locations. Replace the existing one with a different
                // location.
                dispatcher.stop();
                Party nextLocation = getLocation(locations);
                addDispatcher(key, nextLocation);
            }
        }
    }

    /**
     * Returns the location with the lowest identifier from a set of locations.
     *
     * @param locations the locations
     * @return the location with the lowest identifier
     */
    private Party getLocation(Set<Party> locations) {
        List<Party> list = new ArrayList<>(locations);
        if (list.size() > 1) {
            list.sort(Comparator.comparingLong(IMObject::getId));
        }
        return list.get(0);
    }
}
