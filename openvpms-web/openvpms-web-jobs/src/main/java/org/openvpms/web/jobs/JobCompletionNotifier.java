/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.rules.workflow.MessageArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Helper to notify users on the completion of a job.
 *
 * @author Tim Anderson
 */
public class JobCompletionNotifier {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The user rules.
     */
    private final UserRules rules;


    /**
     * Constructs a {@link JobCompletionNotifier}.
     *
     * @param service the archetype service
     */
    public JobCompletionNotifier(IArchetypeService service) {
        this.service = service;
        rules = new UserRules(service);
    }

    /**
     * Returns the users to notify at the completion of a job.
     *
     * @param configuration the job configuration
     * @return the users
     */
    public Set<User> getUsers(Entity configuration) {
        IMObjectBean bean = service.getBean(configuration);
        Set<org.openvpms.component.business.domain.im.security.User> users
                = rules.getUsers(bean.getTargets("notify", Entity.class));
        return new HashSet<>(users);
    }

    /**
     * Sends a message to a set of users.
     *
     * @param users   the users
     * @param subject the subject
     * @param reason  the reason
     * @param message the message text
     */
    public void send(Set<User> users, String subject, String reason, String message) {
        for (User user : users) {
            send(user, subject, reason, message);
        }
    }

    /**
     * Sends a message to a user.
     * <p/>
     * Long messages will be automatically truncated.
     *
     * @param user    the user to send to
     * @param subject the subject
     * @param reason  the reason
     * @param text    the message text
     */
    protected void send(User user, String subject, String reason, String text) {
        Act act = service.create(MessageArchetypes.SYSTEM, Act.class);
        IMObjectBean bean = service.getBean(act);
        bean.setTarget("to", user);
        bean.setValue("reason", reason);
        bean.setValue("description", truncate(subject, bean.getMaxLength("description")));
        bean.setValue("message", truncate(text, bean.getMaxLength("message")));
        bean.save();
    }

    /**
     * Helper to truncate a string if it exceeds a maximum length.
     *
     * @param value     the value to truncate
     * @param maxLength the maximum length
     * @return the new value
     */
    private String truncate(String value, int maxLength) {
        return StringUtils.abbreviate(value, maxLength);
    }

}
