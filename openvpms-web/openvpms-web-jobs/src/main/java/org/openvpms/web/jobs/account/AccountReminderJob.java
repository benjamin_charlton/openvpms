/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.SystemMessageReason;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.NamedQuery;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.service.SimpleSMSService;
import org.openvpms.web.jobs.JobCompletionNotifier;
import org.openvpms.web.jobs.util.ArchetypeServices;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.workspace.customer.account.AccountReminderEvaluator;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Generates reminders for charges that are unpaid.
 *
 * @author Tim Anderson
 * @see AccountReminderSender
 * @see AccountReminderConfig
 * @see AccountReminderEvaluator
 */
@DisallowConcurrentExecution
public class AccountReminderJob implements InterruptableJob {

    /**
     * The job configuration.
     */
    private final Entity configuration;

    /**
     * The SMS service.
     */
    private final SimpleSMSService smsService;

    /**
     * The archetype services.
     */
    private final ArchetypeServices services;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The customer rules.
     */
    private final CustomerRules customerRules;

    /**
     * The reminder rules.
     */
    private final AccountReminderRules reminderRules;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The SMS template evaluator.
     */
    private final SMSTemplateEvaluator templateEvaluator;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Used to send messages to users on completion or failure.
     */
    private final JobCompletionNotifier notifier;

    /**
     * Determines if reminding should stop.
     */
    private volatile boolean stop;

    /**
     * The total no. of reminders processed.
     */
    private int total;

    /**
     * The no. of reminders sent.
     */
    private int sent;

    /**
     * The no. of reminders with errors.
     */
    private int errors;

    /**
     * The no. of cancelled reminders.
     */
    private int cancelled;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AccountReminderJob.class);


    /**
     * Constructs an {@link AccountReminderJob}.
     *
     * @param configuration      the job configuration
     * @param smsService         the SMS service
     * @param services           the archetype services
     * @param lookups            the lookup service
     * @param customerRules      the customer rules
     * @param reminderRules      the reminder rules
     * @param practiceService    the practice service
     * @param templateEvaluator  the SMS template evaluator
     * @param transactionManager the transaction manager
     */
    public AccountReminderJob(Entity configuration, SimpleSMSService smsService,
                              ArchetypeServices services, LookupService lookups, CustomerRules customerRules,
                              AccountReminderRules reminderRules, PracticeService practiceService,
                              SMSTemplateEvaluator templateEvaluator, PlatformTransactionManager transactionManager) {
        this.configuration = configuration;
        this.smsService = smsService;
        this.services = services;
        this.lookups = lookups;
        this.customerRules = customerRules;
        this.reminderRules = reminderRules;
        this.practiceService = practiceService;
        this.templateEvaluator = templateEvaluator;
        this.transactionManager = transactionManager;
        notifier = new JobCompletionNotifier(services.getRuleService());
    }

    /**
     * Called by the {@link Scheduler} when a user interrupts the {@code Job}.
     *
     * @throws UnableToInterruptJobException if there is an exception while interrupting the job.
     */
    @Override
    public void interrupt() throws UnableToInterruptJobException {
        stop = true;
    }

    /**
     * Called by the {@link Scheduler} when a {@link Trigger} fires that is associated with the {@code Job}.
     *
     * @throws JobExecutionException if there is an exception while executing the job.
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            execute();
            complete(null);
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
            complete(exception);
        }
    }

    /**
     * Sends account reminders.
     */
    protected void execute() {
        AccountReminderConfig config = new AccountReminderConfig(configuration, getStartDate(), services.getService());

        total = 0;
        sent = 0;
        errors = 0;
        cancelled = 0;
        Party practice = practiceService.getPractice();
        if (practice == null) {
            throw new IllegalStateException("No current practice");
        }
        AccountReminderEvaluator evaluator = new AccountReminderEvaluator(practice, templateEvaluator,
                                                                          services.getService(), lookups);
        AccountReminderSender sender = new AccountReminderSender(services.getService(), customerRules, reminderRules,
                                                                 smsService, evaluator, config, transactionManager);
        for (AccountReminderConfig.ReminderCount count : config.getReminderCounts()) {
            send(sender, config, count);
        }
        log.info("Processed {} charges: sent={}, errors={}, cancelled={}, skipped={}", total, sent, errors, cancelled,
                 total - sent - errors - cancelled);
    }

    /**
     * Returns the date/time to base date calculations on.
     *
     * @return the current date/time
     */
    protected Date getStartDate() {
        return DateRules.getToday();
    }

    /**
     * Determines the no. of charges to process at once.
     *
     * @return the page size
     */
    protected int getPageSize() {
        return 1000;
    }

    /**
     * Send reminders for charges.
     *
     * @param sender the sender
     * @param config the account reminder configuration
     * @param count  the reminder count
     */
    private void send(AccountReminderSender sender, AccountReminderConfig config,
                      AccountReminderConfig.ReminderCount count) {
        log.info("Sending reminder {} for charges finalised between {} and {}", count.getCount(),
                 DateFormatter.formatDateTime(count.getFrom()), DateFormatter.formatDateTime(count.getTo()));
        NamedQuery query = new NamedQuery("AccountReminderJob.getCharges", "id", "archetype");
        query.setParameter("from", count.getFrom());
        query.setParameter("to", count.getTo());
        query.setParameter("count", count.getCount());
        query.setParameter("minBalance", config.getMinBalance());

        int pageSize = getPageSize();
        query.setMaxResults(pageSize);
        // pull in pageSize results at a time. Note that sending updates the charge, which affects paging, so the
        // query needs to be re-issued from the start if any have updated
        boolean done = false;
        Set<Long> exclude = new HashSet<>(); // used to exclude charges that have already been processed
        IArchetypeService service = services.getService();
        while (!stop && !done) {
            IPage<ObjectSet> page = service.getObjects(query);
            boolean updated = false;  // flag to indicate if any charges were updated
            for (ObjectSet set : page.getResults()) {
                long id = set.getLong("id");
                if (exclude.add(id)) {
                    String archetype = set.getString("archetype");
                    updated |= send(id, archetype, sender, count);
                }
            }
            if (page.getResults().size() < pageSize) {
                done = true;
            } else if (!updated) {
                // nothing updated, so pull in the next page
                query.setFirstResult(query.getFirstResult() + page.getResults().size());
            }
        }
    }

    /**
     * Sends a reminder for a charge.
     *
     * @param id        the charge id
     * @param archetype the charge archetype
     * @param sender    the reminder sender
     * @param count     the reminder count
     * @return {@code true} if the reminder was sent
     */
    private boolean send(long id, String archetype, AccountReminderSender sender,
                         AccountReminderConfig.ReminderCount count) {
        boolean result = false;
        FinancialAct charge = services.getService().get(archetype, id, FinancialAct.class);
        if (charge != null) {
            ++total;
            AccountReminder.Status status = sender.send(charge, count);
            if (status.isPersistent()) {
                // only record stats for statuses that indicate the reminder is persistent
                result = true;
                if (status == AccountReminder.Status.SENT) {
                    ++sent;
                } else if (status == AccountReminder.Status.ERROR) {
                    ++errors;
                } else if (status == AccountReminder.Status.CANCELLED) {
                    ++cancelled;
                }
            }
        }
        return result;
    }

    /**
     * Invoked on completion of a job. Sends a message notifying the registered users of completion or failure of the
     * job if required.
     *
     * @param exception the exception, if the job failed, otherwise {@code null}
     */
    private void complete(Throwable exception) {
        if (exception != null || sent != 0 || errors != 0) {
            Set<User> users = notifier.getUsers(configuration);
            if (!users.isEmpty()) {
                notifyUsers(users, exception);
            }
        }
    }

    /**
     * Notifies users of completion or failure of the job.
     *
     * @param users     the users to notify
     * @param exception the exception, if the job failed, otherwise {@code null}
     */
    private void notifyUsers(Set<User> users, Throwable exception) {
        String subject;
        String reason;
        StringBuilder text = new StringBuilder();
        if (exception != null) {
            reason = SystemMessageReason.ERROR;
            subject = Messages.format("accountreminder.subject.exception", configuration.getName());
            text.append(Messages.format("accountreminder.exception", exception.getMessage()));
        } else if (errors != 0) {
            reason = SystemMessageReason.ERROR;
            subject = Messages.format("accountreminder.subject.errors", configuration.getName(), errors);
        } else {
            reason = SystemMessageReason.COMPLETED;
            subject = Messages.format("accountreminder.subject.success", configuration.getName(), sent);
        }
        text.append(Messages.format("accountreminder.sent", sent));
        text.append("\n");
        text.append(Messages.format("accountreminder.error", errors));
        if (errors != 0) {
            text.append("\n\n");
            text.append(Messages.format("accountreminder.error.see", errors));
        }

        notifier.send(users, subject, reason, text.toString());
    }
}