/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import com.sun.mail.imap.IMAPFolder;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Wrapper around a {@link Message}.
 *
 * @author Tim Anderson
 */
class Email {

    /**
     * The message.
     */
    private final Message message;

    /**
     * Constructs an {@link Email}.
     *
     * @param message the message
     */
    public Email(Message message) {
        this.message = message;
    }

    /**
     * Returns the message id.
     *
     * @return the message id. May be {@code null}
     */
    public String getMessageId() {
        return getHeader(() -> {
            if (message instanceof MimeMessage) {
                return ((MimeMessage) message).getMessageID();
            }
            return null;
        });
    }

    /**
     * Returns the message subject.
     *
     * @return the message subject. May be {@code null}
     */
    public String getSubject() {
        return getHeader(message::getSubject);
    }

    /**
     * Returns the from address(es).
     *
     * @return the from address. May be {@code null}
     */
    public String getFrom() {
        return getHeader(() -> {
            Address[] from = message.getFrom();
            return from != null && from.length > 0 ? from[0].toString() : null;
        });
    }

    /**
     * Returns the received date.
     *
     * @return the received date. May be {@code null}
     */
    public Date getDate() {
        try {
            return message.getReceivedDate();
        } catch (Exception ignore) {
            // no-op
        }
        return null;
    }

    /**
     * Returns the parts of a message, if the message is a multipart message.
     *
     * @return the parts, or {@code null} if the message content is not multipart
     * @throws MessagingException for any mail error
     * @throws IOException        for any I/O error
     */
    public Multipart getParts() throws MessagingException, IOException {
        Multipart result = null;
        Object content = message.getContent();
        if (content instanceof Multipart) {
            result = (Multipart) content;
        }
        return result;
    }

    /**
     * Marks the email as being processed, so the job can exclude it from subsequent processing.
     *
     * @throws MessagingException for any mail error
     */
    public void setProcessed() throws MessagingException {
        message.setFlags(new Flags(Flags.Flag.SEEN), true);
    }

    /**
     * Moves the email from one folder to another.
     *
     * @param source the source folder
     * @param target the target folder
     * @throws MessagingException for any mail error
     */
    public void move(Folder source, Folder target) throws MessagingException {
        boolean copy = true;
        if (source instanceof IMAPFolder) {
            IMAPFolder folder = (IMAPFolder) source;
            Message[] messages = {message};
            try {
                folder.moveMessages(messages, target);
                copy = false;
            } catch (MessagingException exception) {
                if (!"MOVE not supported".equals(exception.getMessage())) {
                    throw exception;
                }
            }
        }
        if (copy) {
            source.copyMessages(new Message[]{message}, target);
            message.setFlags(new Flags(Flags.Flag.DELETED), true);
        }
    }

    /**
     * Helper to invoke a call to get a message header for diagnostic purposes, trapping any exceptions.
     *
     * @param call the message header call
     * @return the header value
     */
    private String getHeader(Callable<String> call) {
        String result = null;
        try {
            result = call.call();
        } catch (Exception exception) {
            // no-op
        }
        return result != null ? result : null;
    }
}