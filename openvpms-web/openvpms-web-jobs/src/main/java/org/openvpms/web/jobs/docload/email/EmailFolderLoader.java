/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.archetype.rules.practice.MailServer;
import org.openvpms.web.security.mail.MailPasswordResolver;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.FromStringTerm;
import javax.mail.search.OrTerm;
import javax.mail.search.SearchTerm;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.openvpms.archetype.rules.practice.MailServer.AuthenticationMethod.OAUTH2;

/**
 * Loads documents from an email folder.
 *
 * @author Tim Anderson
 */
class EmailFolderLoader {

    /**
     * The mail server configuration.
     */
    private final MailServer mailServer;

    /**
     * The mail server password resolver.
     */
    private final MailPasswordResolver passwordResolver;

    /**
     * The listener.
     */
    private final EmailLoaderListener listener;

    /**
     * IMAP property prefix.
     */
    private static final String IMAP_PREFIX = "mail.imap.";

    /**
     * IMAPS property prefix.
     */
    private static final String IMAPS_PREFIX = "mail.imaps.";

    /**
     * Property name for the connection timeout. From the JavaMail docs:<br/>
     * Socket connection timeout value in milliseconds. This timeout is implemented by java.net.Socket.
     * Default is infinite timeout.
     */
    private static final String CONNECTION_TIMEOUT = "connectiontimeout";

    /**
     * Property name for the read timeout. From the JavaMail docs:<br/>
     * Socket read timeout value in milliseconds. This timeout is implemented by java.net.Socket.
     * Default is infinite timeout.
     */
    private static final String READ_TIMEOUT = "timeout";

    /**
     * Property name for the write timeout. From the JavaMail docs:<br/>
     * Socket write timeout value in milliseconds. This timeout is implemented by using a
     * java.util.concurrent.ScheduledExecutorService per connection that schedules a thread to close the socket if the
     * timeout expires. Thus, the overhead of using this timeout is one thread per connection.
     * Default is infinite timeout.
     */
    private static final String WRITE_TIMEOUT = "writetimeout";

    /**
     * IMAP store protocol.
     */
    private static final String IMAP_PROTOCOL = "imap";

    /**
     * IMAPS store protocol.
     */
    private static final String IMAPS_PROTOCOL = "imaps";

    /**
     * Constructs an {@link EmailFolderLoader}.
     *
     * @param mailServer       the mail server configuration
     * @param passwordResolver the mail server password resolver
     * @param listener         the listener
     */
    public EmailFolderLoader(MailServer mailServer, MailPasswordResolver passwordResolver,
                             EmailLoaderListener listener) {
        this.mailServer = mailServer;
        this.passwordResolver = passwordResolver;
        this.listener = listener;
    }

    /**
     * Processes all matching messages in the specified folder.
     *
     * @param loader     the loader
     * @param from       if non-null and non-empty, only process messages from the specified addresses
     * @param sourceName the source folder name
     * @param targetName the target folder name. May be {@code null}
     * @param errorName  the error folder name. May be {@code null}
     * @param stop       used to interrupt processing
     */
    public void process(EmailLoader loader, List<String> from, String sourceName, String targetName, String errorName,
                        AtomicBoolean stop) throws MessagingException {
        Session session = getSession();
        try (Store store = session.getStore()) {
            String password = passwordResolver.getPassword(mailServer);
            store.connect(mailServer.getHost(), mailServer.getPort(), mailServer.getUsername(), password);

            try (Folder source = store.getFolder(sourceName);
                 Folder target = (targetName != null) ? store.getFolder(targetName) : null;
                 Folder error = (errorName != null) ? store.getFolder(errorName) : null) {
                source.open(Folder.READ_WRITE);
                if (!source.isOpen()) {
                    throw new IllegalStateException("Failed to open folder " + sourceName);
                }
                Flags flags = source.getPermanentFlags();
                if (flags == null || !flags.contains(Flags.Flag.SEEN)) {
                    throw new IllegalStateException("Folder " + sourceName + " does not support the SEEN flag");
                }
                if (target != null) {
                    target.open(Folder.READ_WRITE);
                }
                if (error != null) {
                    error.open(Folder.READ_WRITE);
                }
                SearchTerm search = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
                if (from != null && !from.isEmpty()) {
                    search = new AndTerm(search, getFromTerm(from));
                }
                Message[] messages = source.search(search);
                for (Message message : messages) {
                    if (stop.get()) {
                        break;
                    }
                    Email email = new Email(message);
                    process(email, loader, source, target, error);
                }
            }
        }
    }

    /**
     * Processes a message.
     *
     * @param email  the email to process
     * @param loader the email loader
     * @param source the source folder
     * @param target the target folder, to move processed messages to. May be {@code null}
     * @param error  the error folder, to move messages with unprocessable attachments to. May be {@code null}
     */
    private void process(Email email, EmailLoader loader, Folder source, Folder target, Folder error) {
        try {
            boolean processed = loader.process(email);
            email.setProcessed();
            if (processed) {
                if (target != null) {
                    email.move(source, target);
                }
            } else if (error != null) {
                email.move(source, error);
            }
        } catch (Throwable exception) {
            listener.error(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate(), null, exception);
        }
    }

    /**
     * Returns a mail session.
     *
     * @return the mail session
     */
    private Session getSession() {
        String prefix = IMAP_PREFIX;
        String protocol = IMAP_PROTOCOL;
        Properties properties = new Properties();
        if (mailServer.getSecurity() == MailServer.Security.SSL_TLS) {
            properties.setProperty("mail.imap.ssl.enable", "true");
            prefix = IMAPS_PREFIX;
            protocol = IMAPS_PROTOCOL;
        } else if (mailServer.getSecurity() == MailServer.Security.STARTTLS) {
            properties.setProperty("mail.imap.starttls.enable", "true");
            prefix = IMAPS_PREFIX;
            protocol = IMAPS_PROTOCOL;
        }
        properties.setProperty("mail.store.protocol", protocol);
        if (mailServer.getAuthenticationMethod() == OAUTH2) {
            properties.setProperty(IMAPS_PREFIX + "auth.mechanisms", "XOAUTH2");
        }

        int timeout = mailServer.getTimeout();
        if (timeout > 0) {
            String millis = Long.toString(timeout * DateUtils.MILLIS_PER_SECOND);
            properties.setProperty(prefix + READ_TIMEOUT, millis);
            properties.setProperty(prefix + WRITE_TIMEOUT, millis);
            properties.setProperty(prefix + CONNECTION_TIMEOUT, millis);
        }
        return Session.getInstance(properties);
    }

    /**
     * Returns a search term to match on one or more from addresses.
     *
     * @param from the from-addresses
     * @return a search term matching on the from-addresses
     */
    private SearchTerm getFromTerm(List<String> from) {
        SearchTerm[] terms = new SearchTerm[from.size()];
        for (int i = 0; i < from.size(); ++i) {
            terms[i] = new FromStringTerm(from.get(i));
        }
        return terms.length == 1 ? terms[0] : new OrTerm(terms);
    }

}