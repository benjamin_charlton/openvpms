/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.util.PeriodHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Account reminder job configuration.
 *
 * @author Tim Anderson
 */
public class AccountReminderConfig {

    /**
     * The reminder count.
     */
    public static class ReminderCount {

        /**
         * The reminder count.
         */
        private final int count;

        /**
         * The interval after charge finalisation when this count is used.
         */
        private final Period interval;

        /**
         * The SMS template.
         */
        private final Entity smsTemplate;

        /**
         * The start of the date range from when this count applies.
         */
        private Date from;

        /**
         * The end of the date range from when this count applies.
         */
        private Date to;

        /**
         * Constructs a {@link ReminderCount}.
         *
         * @param config  the configuration. An <em>entity.accountReminderCount</em>
         * @param count   the count. Zero-based
         * @param service the archetype service
         */
        public ReminderCount(Entity config, int count, ArchetypeService service) {
            this.count = count;
            IMObjectBean bean = service.getBean(config);
            interval = PeriodHelper.getPeriod(bean, "interval", "units", DateUnits.DAYS);
            smsTemplate = bean.getTarget("smsTemplate", Entity.class);
        }

        /**
         * Returns the reminder count.
         *
         * @return the zero-based reminder count
         */
        public int getCount() {
            return count;
        }

        /**
         * Returns the interval after charge finalisation when this count is used.
         * <p/>
         * E.g, an interval of 2 days applies 2 days after an charge is finalised.
         *
         * @return the interval
         */
        public Period getInterval() {
            return interval;
        }

        /**
         * Returns the start of the date range from when this count applies.
         *
         * @return the start of the date range
         */
        public Date getFrom() {
            return from;
        }

        /**
         * Returns the end of the date range from when this count applies.
         *
         * @return the end of the date range
         */
        public Date getTo() {
            return to;
        }

        /**
         * Returns the SMS template.
         *
         * @return the SMS template
         */
        public Entity getSMSTemplate() {
            return smsTemplate;
        }

        /**
         * Sets the start of the date range from when this count applies.
         *
         * @param date the start of the date range
         */
        private void setFrom(Date date) {
            this.from = date;
        }

        /**
         * Sets the end of the date range from when this count applies.
         *
         * @param date the end of the date range
         */
        private void setTo(Date date) {
            this.to = date;
        }
    }

    /**
     * The configuration.
     */
    private final Entity config;

    /**
     * The minimum balance.
     */
    private final BigDecimal minBalance;

    /**
     * The reminder counts.
     */
    private final List<ReminderCount> counts = new ArrayList<>();


    /**
     * Constructs an {@link AccountReminderConfig}.
     *
     * @param config  the configuration. An <em>entity.jobAccountReminder</em>
     * @param date    the date to base date range calculations on
     * @param service the archetype service
     */
    public AccountReminderConfig(Entity config, Date date, ArchetypeService service) {
        this.config = config;
        IMObjectBean bean = service.getBean(config);
        Period ignoreInterval = PeriodHelper.getPeriod(bean, "ignoreInterval");
        if (ignoreInterval == null) {
            ignoreInterval = Period.years(1);
        }
        Date start = DateRules.minus(date, ignoreInterval);

        // order the entities on decreasing interval
        List<Entity> entities = new ArrayList<>(bean.getTargets("reminders", Entity.class));
        entities.sort((o1, o2) -> {
            Period interval1 = getInterval(o1, service);
            Period interval2 = getInterval(o2, service);
            return -DateRules.plus(date, interval1).compareTo(DateRules.plus(date, interval2));
        });
        int i = entities.size();
        for (Entity entity : entities) {
            ReminderCount count = new ReminderCount(entity, --i, service);
            count.setFrom(start);
            Date end = DateRules.minus(date, count.getInterval());
            count.setTo(end);
            counts.add(count);
            start = end;
        }
        Collections.reverse(counts);
        minBalance = bean.getBigDecimal("minBalance", BigDecimal.ZERO);
    }

    /**
     * Returns the name of the configuration.
     *
     * @return the name of the configuration
     */
    public String getName() {
        return config.getName();
    }

    /**
     * Returns the reminder counts.
     *
     * @return the reminder counts, in order of increasing interval.
     */
    public List<ReminderCount> getReminderCounts() {
        return counts;
    }

    /**
     * Returns the minimum balance.
     * <p/>
     * No reminder should be generated if the outstanding balance is less than this amount.
     *
     * @return the minimum balance
     */
    public BigDecimal getMinBalance() {
        return minBalance;
    }

    /**
     * Returns the configuration.
     *
     * @return the configuration. An <em>entity.jobAccountReminder</em>
     */
    public Entity getConfig() {
        return config;
    }

    /**
     * Returns the expected next reminder after the specified reminder count.
     *
     * @param from  the charge completion date. May be {@code null}
     * @param count the current reminder count
     * @return the next reminder, or {@code null} if no subsequent count is defined
     */
    public Date getNextReminder(Date from, int count) {
        Date result = null;
        if (from != null && count + 1 < counts.size()) {
            result = DateRules.plus(from, counts.get(count + 1).getInterval());
        }
        return result;
    }

    /**
     * Returns the interval of an <em>entity.accountReminderCount</em>.
     *
     * @param count   the count
     * @param service the archetype service
     * @return the interval
     */
    private Period getInterval(Entity count, ArchetypeService service) {
        Period period = PeriodHelper.getPeriod(service.getBean(count), "interval", "units");
        return period != null ? period : Period.days(0);
    }

}