/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.appointment;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.web.component.service.SimpleSMSService;
import org.openvpms.web.jobs.AbstractSMSJobTest;
import org.openvpms.web.workspace.workflow.appointment.reminder.AppointmentReminderEvaluator;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.ADMITTED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.BILLED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CANCELLED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CHECKED_IN;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.COMPLETED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CONFIRMED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.NO_SHOW;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.PENDING;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link AppointmentReminderJob}.
 *
 * @author Tim Anderson
 */
public class AppointmentReminderJobTestCase extends AbstractSMSJobTest {

    /**
     * The location rules.
     */
    @Autowired
    private LocationRules locationRules;

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules customerRules;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The practice service.
     */
    private TestPracticeService practiceService;

    /**
     * Test practice location 1.
     */
    private Party location1;

    /**
     * Test practice location 2.
     */
    private Party location2;

    /**
     * Test appointment type.
     */
    private Entity appointmentType;

    /**
     * Test appointment schedule 1.
     */
    private Entity schedule1;

    /**
     * Test appointment schedule 2.
     */
    private Entity schedule2;

    /**
     * The date to base tests around.
     */
    private Date dateFrom;

    /**
     * The default appointment reminder template.
     */
    private Entity template;

    /**
     * The evaluator.
     */
    private AppointmentReminderEvaluator evaluator;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        evaluator = new AppointmentReminderEvaluator(service, smsTemplateEvaluator);
        dateFrom = getDate("2015-11-01");

        location1 = practiceFactory.newLocation().name("Vets R Us").build();
        location2 = practiceFactory.newLocation().name("Vets Be Us").build();

        appointmentType = schedulingFactory.createAppointmentType();
        schedule1 = createSchedule(location1, appointmentType);
        schedule2 = createSchedule(location2, appointmentType);

        Entity scheduleView1 = schedulingFactory.createScheduleView(schedule1);
        Entity scheduleView2 = schedulingFactory.createScheduleView(schedule2);

        practiceFactory.updateLocation(location1).scheduleViews(scheduleView1).build();
        practiceFactory.updateLocation(location2).scheduleViews(scheduleView2).build();

        // remove SMS reminders for existing acts for the test period
        ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.APPOINTMENT);
        Date disableFrom = DateRules.getDate(dateFrom, -2, DateUnits.WEEKS);
        Date disableTo = getDate("2015-11-20");
        query.add(Constraints.between("startTime", disableFrom, disableTo));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);

        IMObjectQueryIterator<Act> iterator = new IMObjectQueryIterator<>(query);
        while (iterator.hasNext()) {
            Act act = iterator.next();
            service.remove(act);
        }

        template = createTemplate();

        practiceService = new TestPracticeService(template, location1, location2);
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        practiceService.destroy();
    }

    /**
     * Tests the job.
     *
     * @throws Exception for any error
     */
    @Test
    public void testJob() throws Exception {
        Entity config = createJobConfig();

        // create 15 appointments from dateFrom, with every even appointment flagged to have reminders sent
        Date date = dateFrom;
        List<Act> appointments = new ArrayList<>();
        for (int i = 0; i < 15; ++i) {
            Act appointment = createAppointment(date, schedule1, i % 2 == 0);
            appointments.add(appointment);
            date = DateRules.getDate(date, 1, DateUnits.DAYS);
        }

        // start sending reminders 3 days prior to the first reminder. The first 6 appointments flagged should be sent
        final Date startDate = DateRules.getDate(dateFrom, -3, DateUnits.DAYS);

        SimpleSMSService smsService = createSMSService();

        runJob(config, startDate, smsService);
        for (int i = 0; i < appointments.size(); ++i) {
            Act appointment = get(appointments.get(i));
            if (i % 2 == 0 && i < 12) {
                checkSent(appointment);
            } else {
                checkNotSent(appointment, null);
            }
        }

        Mockito.verify(smsService, Mockito.times(6)).send(Mockito.eq("Reminder: Vets R Us"), Mockito.<Contact>any(),
                                                          Mockito.any(), Mockito.any(),
                                                          Mockito.eq("SMS appointment reminder"),
                                                          Mockito.eq("APPOINTMENT_REMINDER"), Mockito.eq(location1));
    }

    /**
     * Verifies that when a schedule has {@code sendReminders == false}, no reminders are sent.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDisableSendRemindersOnSchedule() throws Exception {
        Entity config = createJobConfig();

        Date date = dateFrom;
        List<Act> appointments = new ArrayList<>();
        for (int i = 0; i < 12; ++i) {
            Entity schedule = (i % 2 == 0) ? schedule1 : schedule2;
            Act appointment = createAppointment(date, schedule, true);
            appointments.add(appointment);
            date = DateRules.getDate(date, 1, DateUnits.DAYS);
        }

        // disable reminders for schedule1
        schedulingFactory.updateSchedule(schedule1).sendReminders(false).build();

        // start sending reminders 2 days prior to the first reminder. All schedule2 reminders should be sent
        Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        for (int i = 0; i < appointments.size(); ++i) {
            Act appointment = get(appointments.get(i));
            if (i % 2 == 0) {
                checkNotSent(appointment, null);
            } else {
                checkSent(appointment);
            }
        }

        Mockito.verify(smsService, Mockito.times(6)).send(Mockito.eq("Reminder: Vets Be Us"), Mockito.<Contact>any(),
                                                          Mockito.any(), Mockito.any(),
                                                          Mockito.eq("SMS appointment reminder"),
                                                          Mockito.eq("APPOINTMENT_REMINDER"), Mockito.eq(location2));
    }

    /**
     * Verifies that only appointments with PENDING status have reminders sent.
     *
     * @throws Exception for any error
     */
    @Test
    public void testRemindersForPendingAppointments() throws Exception {
        Entity config = createJobConfig();

        Party customer = createCustomer();
        Party patient = patientFactory.createPatient(customer);
        Act appointment1 = createAppointment(dateFrom, schedule1, customer, patient, true, PENDING);
        Act appointment2 = createAppointment(dateFrom, schedule1, customer, patient, true, CONFIRMED);
        Act appointment3 = createAppointment(dateFrom, schedule1, customer, patient, true, CHECKED_IN);
        Act appointment4 = createAppointment(dateFrom, schedule1, customer, patient, true, IN_PROGRESS);
        Act appointment5 = createAppointment(dateFrom, schedule1, customer, patient, true, ADMITTED);
        Act appointment6 = createAppointment(dateFrom, schedule1, customer, patient, true, BILLED);
        Act appointment7 = createAppointment(dateFrom, schedule1, customer, patient, true, COMPLETED);
        Act appointment8 = createAppointment(dateFrom, schedule1, customer, patient, true, CANCELLED);
        Act appointment9 = createAppointment(dateFrom, schedule1, customer, patient, true, NO_SHOW);

        Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkSent(appointment1);
        checkNotSent(appointment2, null);
        checkNotSent(appointment3, null);
        checkNotSent(appointment4, null);
        checkNotSent(appointment5, null);
        checkNotSent(appointment6, null);
        checkNotSent(appointment7, null);
        checkNotSent(appointment8, null);
        checkNotSent(appointment9, null);
    }

    /**
     * Verifies that the reminderError node is populated if a customer is inactive.
     *
     * @throws Exception for any error
     */
    @Test
    public void testInactiveCustomer() throws Exception {
        Entity config = createJobConfig();

        Party customer1 = createCustomer();
        Party customer2 = createCustomer();
        customer2.setActive(false);
        save(customer2);
        Act appointment1 = createAppointment(dateFrom, schedule1, customer1, true);
        Act appointment2 = createAppointment(dateFrom, schedule1, customer2, true);

        Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkSent(appointment1);
        checkNotSent(appointment2, "Customer is inactive. No SMS will be sent");
    }

    /**
     * Verifies that the reminderError node is populated if a patient is inactive or deceased.
     *
     * @throws Exception for any error
     */
    @Test
    public void testInvalidPatient() throws Exception {
        Entity config = createJobConfig();

        Party customer1 = createCustomer();
        Party patient1 = patientFactory.createPatient(customer1);
        Party patient2 = patientFactory.createPatient(customer1);
        patient2.setActive(false);
        save(patient2);
        Party patient3 = patientFactory.createPatient(customer1);
        patientRules.setDeceased(patient3);
        Act appointment1 = createAppointment(dateFrom, schedule1, customer1, patient1, true);
        Act appointment2 = createAppointment(dateFrom, schedule1, customer1, patient2, true);
        Act appointment3 = createAppointment(dateFrom, schedule1, customer1, patient3, true);

        Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkSent(appointment1);
        checkNotSent(appointment2, "Patient is inactive. No SMS will be sent");
        checkNotSent(appointment3, "Patient is deceased. No SMS will be sent");
    }


    /**
     * Verifies that the reminderError node is populated if a customer doesn't have an SMS contact.
     *
     * @throws Exception for any error
     */
    @Test
    public void testCustomerNoSMSContact() throws Exception {
        Entity config = createJobConfig();

        Party customer1 = createCustomer();
        Party customer2 = customerFactory.createCustomer();
        Act appointment1 = createAppointment(dateFrom, schedule1, customer1, true);
        Act appointment2 = createAppointment(dateFrom, schedule1, customer2, true);

        final Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkSent(appointment1);
        checkNotSent(appointment2, "Customer has no SMS contact");
    }

    /**
     * Verifies that the reminderError node is populated if no location can be linked to the appointment schedule.
     *
     * @throws Exception for any error
     */
    @Test
    public void testScheduleWithNoLocation() throws Exception {
        Entity config = createJobConfig();

        final Act appointment1 = createAppointment(dateFrom, schedule1, true);
        Act appointment2 = createAppointment(dateFrom, schedule2, true);

        final Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();

        IArchetypeRuleService archetypeService = (IArchetypeRuleService) getArchetypeService();
        AppointmentReminderJob job = new AppointmentReminderJob(config, smsService, archetypeService, customerRules,
                                                                patientRules, practiceService, locationRules,
                                                                evaluator, transactionManager) {
            @Override
            protected Date getStartDate() {
                return startDate;
            }

            @Override
            protected int getPageSize() {
                return 5;
            }

            @Override
            protected boolean isPast(IMObjectBean bean) {
                return false;
            }

            @Override
            protected org.openvpms.component.business.domain.im.party.Party getLocation(IMObjectBean bean) {
                return bean.getObject().equals(appointment1) ? super.getLocation(bean) : null;
            }
        };
        job.execute(null);

        checkSent(appointment1);
        checkNotSent(appointment2, "Cannot determine the practice location of the Appointment");
    }

    /**
     * Verifies that the reminderError node is populated if there is no practice reminder template, and none for the
     * schedule location.
     *
     * @throws Exception for any error
     */
    @Test
    public void testLocationNoTemplate() throws Exception {
        IMObjectBean bean = getBean(location1);
        bean.setTarget("smsAppointment", template);
        save(location1, template);

        practiceService.setAppointmentSMSTemplate(null);

        Entity config = createJobConfig();
        Act appointment1 = createAppointment(dateFrom, schedule1, true);
        Act appointment2 = createAppointment(dateFrom, schedule2, true);


        final Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkSent(appointment1);
        checkNotSent(appointment2, "No appointment reminder template has been configured for Vets Be Us and " +
                                   "there is no default template.");
    }

    /**
     * Verifies that the reminderError node is populated if the expression produces no SMS text.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSMSEmptyText() throws Exception {
        practiceService.setAppointmentSMSTemplate(createTemplate("''"));
        Entity config = createJobConfig();
        Act appointment1 = createAppointment(dateFrom, schedule1, true);


        final Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkNotSent(appointment1, "Generated SMS text was empty");
    }

    /**
     * Verifies that the reminderError node is populated if the expression text longer than 160 characters.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSMSTextTooLong() throws Exception {
        String text = StringUtils.repeat("A", 161);
        practiceService.setAppointmentSMSTemplate(createTemplate("'" + text + "'"));
        Entity config = createJobConfig();
        Act appointment1 = createAppointment(dateFrom, schedule1, true);

        final Date startDate = DateRules.getDate(dateFrom, -2, DateUnits.DAYS);
        SimpleSMSService smsService = createSMSService();
        runJob(config, startDate, smsService);

        checkNotSent(appointment1, "SMS is too long: " + text);
    }

    /**
     * Verifies an appointment reminder has been sent.
     *
     * @param act the appointment act
     */
    private void checkSent(Act act) {
        IMObjectBean bean = getBean(get(act));
        assertNotNull("Expected appointment on " + act.getActivityStartTime() + " to be sent",
                      bean.getDate("reminderSent"));
        assertNull("Expected appointment have no error", bean.getString("reminderError"));
    }

    /**
     * Verifies an appointment reminder has not been sent.
     *
     * @param act   the appointment act
     * @param error the expected error, or {@code null} if no error is expected
     */
    private void checkNotSent(Act act, String error) {
        IMObjectBean bean = getBean(get(act));
        assertNull("Expected appointment on " + act.getActivityStartTime() + " to be unsent",
                   bean.getDate("reminderSent"));
        assertEquals(error, bean.getString("reminderError"));
    }

    /**
     * Runs the appointment reminder job.
     *
     * @param config     the job configuration
     * @param startDate  the date to start on
     * @param smsService the SMS service
     * @throws JobExecutionException for any job execution error
     */
    private void runJob(Entity config, final Date startDate, SimpleSMSService smsService) throws JobExecutionException {
        IArchetypeRuleService archetypeService = (IArchetypeRuleService) getArchetypeService();
        LocationRules locationRules = new LocationRules(getArchetypeService());
        AppointmentReminderJob job = new AppointmentReminderJob(config, smsService, archetypeService, customerRules,
                                                                patientRules, practiceService, locationRules,
                                                                evaluator, transactionManager) {
            @Override
            protected Date getStartDate() {
                return startDate;
            }

            @Override
            protected int getPageSize() {
                return 5;
            }

            @Override
            protected boolean isPast(IMObjectBean bean) {
                return false;
            }
        };
        job.execute(null);
    }

    /**
     * Creates a new job configuration.
     *
     * @return a new job configuration
     */
    private Entity createJobConfig() {
        Entity config = create("entity.jobAppointmentReminder", Entity.class);
        IMObjectBean bean = getBean(config);
        bean.setValue("smsFrom", 2);
        bean.setValue("smsFromUnits", DateUnits.WEEKS.toString());
        bean.setValue("smsTo", 1);
        bean.setValue("smsToUnits", DateUnits.DAYS.toString());
        return config;
    }

    /**
     * Creates a new appointment schedule, configured to send reminders.
     *
     * @param location        the practice location
     * @param appointmentType the default appointment type
     * @return a new schedule
     */
    private Entity createSchedule(Party location, Entity appointmentType) {
        return schedulingFactory.newSchedule()
                .location(location)
                .sendReminders(true)
                .addAppointmentType(appointmentType, 1, true)
                .build();
    }

    /**
     * Creates a new appointment.
     *
     * @param startTime    the appointment start time
     * @param schedule     the schedule
     * @param sendReminder if {@code true} indicates to send reminders
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Entity schedule, boolean sendReminder) {
        Party customer = createCustomer();
        return createAppointment(startTime, schedule, customer, sendReminder);
    }

    /**
     * Creates a PENDING appointment.
     *
     * @param startTime    the appointment start time
     * @param schedule     the schedule
     * @param customer     the customer
     * @param sendReminder if {@code true} indicates to send reminders
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Entity schedule, Party customer, boolean sendReminder) {
        Party patient = patientFactory.createPatient(customer);
        return createAppointment(startTime, schedule, customer, patient, sendReminder);
    }

    /**
     * Creates a PENDING appointment.
     *
     * @param startTime    the appointment start time
     * @param schedule     the schedule
     * @param customer     the customer
     * @param patient      the patient
     * @param sendReminder if {@code true} indicates to send reminders
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Entity schedule, Party customer, Party patient,
                                  boolean sendReminder) {
        return createAppointment(startTime, schedule, customer, patient, sendReminder, PENDING);
    }

    /**
     * Creates an appointment.
     *
     * @param startTime    the appointment start time
     * @param schedule     the schedule
     * @param customer     the customer
     * @param patient      the patient
     * @param sendReminder if {@code true} indicates to send reminders
     * @param status       the appointment status
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Entity schedule, Party customer, Party patient, boolean sendReminder,
                                  String status) {
        return schedulingFactory.newAppointment()
                .startTime(startTime)
                .schedule(schedule)
                .appointmentType(appointmentType)
                .customer(customer)
                .patient(patient)
                .sendReminder(sendReminder)
                .status(status)
                .build();
    }

    /**
     * Creates a new customer with an SMS contact.
     *
     * @return the new customer
     */
    private Party createCustomer() {
        return customerFactory.newCustomer()
                .newPhone().phone("123456789").sms().add()
                .build();
    }

    /**
     * Helper to create an appointment reminder SMS template.
     *
     * @return a new template
     */
    private Entity createTemplate() {
        return createTemplate("concat('Reminder: ', $location.name)");
    }

    /**
     * Helper to create an appointment reminder SMS template.
     *
     * @return a new template
     */
    private Entity createTemplate(String expression) {
        return newSMSTemplate(DocumentArchetypes.APPOINTMENT_SMS_TEMPLATE)
                .name("Test Appointment Reminder SMS Template")
                .contentType("XPATH")
                .content(expression)
                .build();
    }

    private static ThreadPoolTaskExecutor createPool() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.afterPropertiesSet();
        return executor;
    }

    private class TestPracticeService extends PracticeService {

        /**
         * The practice locations.
         */
        private final List<Party> locations;

        /**
         * The default appointment reminder SMS template.
         */
        private Entity template;

        TestPracticeService(Entity template, Party... locations) {
            super(getArchetypeService(), practiceRules, createPool());
            this.locations = Arrays.asList(locations);
            setAppointmentSMSTemplate(template);
        }

        @Override
        @SuppressWarnings("unchecked")
        public List<org.openvpms.component.business.domain.im.party.Party> getLocations() {
            return (List<org.openvpms.component.business.domain.im.party.Party>) (List<?>) locations;
        }

        @Override
        public org.openvpms.component.business.domain.im.common.Entity getAppointmentSMSTemplate() {
            return (org.openvpms.component.business.domain.im.common.Entity) template;
        }

        public void setAppointmentSMSTemplate(Entity template) {
            this.template = template;
        }
    }
}
