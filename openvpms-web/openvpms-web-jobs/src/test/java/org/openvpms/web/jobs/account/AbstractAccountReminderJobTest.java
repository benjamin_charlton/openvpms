/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.job.account.TestAccountReminderJobBuilder;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.jobs.AbstractSMSJobTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

/**
 * Base class for account reminder job tests.
 *
 * @author Tim Anderson
 */
public class AbstractAccountReminderJobTest extends AbstractSMSJobTest {

    /**
     * The user factory.
     */
    @Autowired
    protected TestUserFactory userFactory;


    /**
     * Creates a job configuration.
     * <p/>
     * This creates intervals of 2 days, 2 weeks and 4 weeks, with invoices older than 1 year ignored.
     *
     * @param minBalance the minimum balance
     * @param template   the SMS template
     * @return the job configuration
     */
    protected Entity createConfig(BigDecimal minBalance, Entity template) {
        return new TestAccountReminderJobBuilder(getArchetypeService())
                .minBalance(minBalance)
                .runAs(userFactory.createAdministrator())
                .count(2, DateUnits.DAYS, template)
                .count(2, DateUnits.WEEKS, template)
                .count(4, DateUnits.WEEKS, template)
                .build();
    }

    /**
     * Creates a template with an XPATH expression.
     *
     * @param template the template expression
     * @return a new template
     */
    protected Entity createXPathTemplate(String template) {
        return newSMSTemplate(DocumentArchetypes.ACCOUNT_SMS_TEMPLATE)
                .contentType("XPATH")
                .content(template)
                .build();
    }

}