/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.impl;

import ca.uhn.hl7v2.HapiContext;
import org.openvpms.archetype.component.dispatcher.Queues;
import org.openvpms.component.model.object.Reference;
import org.openvpms.hl7.io.MessageService;
import org.openvpms.hl7.io.Statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages {@link MessageQueue}s.
 *
 * @author Tim Anderson
 */
class MessageQueues implements Queues<MLLPSender, MessageQueue> {

    /**
     * The message service.
     */
    private final MessageService messageService;

    /**
     * The message context.
     */
    private final HapiContext messageContext;

    /**
     * The queues, keyed on connector reference.
     */
    private final Map<Reference, MessageQueue> queues = Collections.synchronizedMap(new HashMap<>());

    /**
     * Constructs a {@link MessageQueues}.
     *
     * @param messageService the message service
     * @param messageContext the message context
     */
    public MessageQueues(MessageService messageService, HapiContext messageContext) {
        this.messageService = messageService;
        this.messageContext = messageContext;
    }

    /**
     * Returns the queues.
     *
     * @return the queues
     */
    @Override
    public List<MessageQueue> getQueues() {
        return new ArrayList<>(queues.values());
    }

    /**
     * Returns a queue given its owner.
     *
     * @param owner the queue owner
     * @return the queue
     */
    @Override
    public MessageQueue getQueue(MLLPSender owner) {
        return getQueue(owner, true);
    }

    /**
     * Returns a queue given its owner.
     *
     * @param owner  the queue owner
     * @param create if {@code true}, and the queue doesn't exist, create it
     * @return the queue, or {@code null} if it doesn't exist and {@code create == false}
     */
    public MessageQueue getQueue(MLLPSender owner, boolean create) {
        MessageQueue queue;
        synchronized (queues) {
            Reference reference = owner.getReference();
            queue = queues.get(reference);
            if (queue == null && create) {
                queue = new MessageQueue(owner, messageService, messageContext);
                queues.put(reference, queue);
            }
        }
        return queue;
    }

    /**
     * Returns the statistics for a sender.
     *
     * @param sender the sender reference
     * @return the statistics, or {@code null} if the sender doesn't exist
     */
    public Statistics getStatistics(Reference sender) {
        return queues.get(sender);
    }

    /**
     * Removes a queue given its owner reference.
     *
     * @param reference the owner reference
     * @return the corresponding queue or {@code null} if none exists
     */
    public MessageQueue remove(Reference reference) {
        return queues.remove(reference);
    }

    /**
     * Destroys the queues.
     */
    @Override
    public void destroy() {
        queues.clear();
    }

}
