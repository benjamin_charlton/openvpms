/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;

/**
 * Table model to display <em>lookup.suburb</em>
 *
 * @author Tim Anderson
 */
public class SuburbTableModel extends DefaultDescriptorTableModel<Lookup> {

    /**
     * Constructs a {@link SuburbTableModel}.
     *
     * @param context the layout context
     */
    public SuburbTableModel(LayoutContext context) {
        super(new String[]{ContactArchetypes.SUBURB}, context, "name", "postCode", "target");
    }
}