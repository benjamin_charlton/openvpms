/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;

import java.util.Objects;


/**
 * An editor for <em>contact.location</em> contacts.
 *
 * @author Tim Anderson
 */
public class LocationEditor extends AbstractContactEditor {

    /**
     * The lookup service.
     */
    private final ILookupService lookups;

    /**
     * Suburb property.
     */
    private final Property suburb;

    /**
     * State property.
     */
    private final Property state;

    /**
     * Constructs a {@link LocationEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context. May be {@code null}.
     */
    public LocationEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        lookups = ServiceHelper.getLookupService();
        suburb = getProperty("suburb");
        suburb.addModifiableListener(modifiable -> onSuburbChanged());
        state = getProperty("state");
        state.addModifiableListener(modifiable -> onStateChanged());
    }

    /**
     * Updates the postcode and state when the suburb changes.
     */
    protected void onSuburbChanged() {
        Property postCode = getProperty("postcode");
        Lookup lookup = getSuburb();
        if (lookup != null) {
            IMObjectBean bean = getBean(lookup);
            postCode.setValue(bean.getValue("postCode"));
            Lookup stateLookup = bean.getSource("target", Lookup.class);
            state.setValue(stateLookup != null ? stateLookup.getCode() : null);
        } else {
            postCode.setValue(null);
        }
    }

    /**
     * Invoked when the state changes. Clears the suburb if it is no longer valid.
     */
    protected void onStateChanged() {
        Lookup stateLookup = getState();
        Lookup suburbLookup = getSuburb();
        if (suburbLookup != null && stateLookup != null && !Objects.equals(getState(suburbLookup), stateLookup)) {
            suburb.setValue(null);
        }
    }

    /**
     * Returns the selected suburb.
     *
     * @return the selected suburb. May be {@code null}
     */
    private Lookup getSuburb() {
        String code = suburb.getString();
        return code != null ? lookups.getLookup("lookup.suburb", code, false) : null;
    }

    /**
     * Returns the selected state.
     *
     * @return the selected state. May be {@code null}
     */
    private Lookup getState() {
        String code = getProperty("state").getString();
        return code != null ? lookups.getLookup("lookup.state", code, false) : null;
    }

    /**
     * Returns the state for a suburb.
     *
     * @param suburb the suburb
     * @return the corresponding state. May be {@code null}
     */
    private Lookup getState(Lookup suburb) {
        IMObjectBean bean = getBean(suburb);
        return bean.getSource("target", Lookup.class);
    }
}
