/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.springframework.context.ApplicationContext;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet invoked at the completion of the OAuth2 code grant flow.
 * <p/>
 * This updates the session bound {@link OAuth2ResponseManager} indicating the success or failure of the flow.
 * <p/>
 * It returns HTML to close the window it was redirected from.
 *
 * @author Tim Anderson
 * @see OAuth2RequestLauncher
 * @see OAuth2ResponseManager
 */
public class OAuth2CodeGrantCompleteServlet extends HttpServlet {

    /**
     * The Spring context.
     */
    private ApplicationContext context;

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    /**
     * Called by the server (via the <code>service</code> method) to allow a servlet to handle a GET request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException for any I/O error
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String error = request.getParameter(OAuth2ParameterNames.ERROR);
        String description = request.getParameter(OAuth2ParameterNames.ERROR_DESCRIPTION);
        String uri = request.getParameter(OAuth2ParameterNames.ERROR_URI);
        OAuth2ResponseManager state = context.getBean(OAuth2ResponseManager.class);
        if (error != null) {
            state.error(error, description, uri);
        } else {
            state.success();
        }
        // generate HTML to close the browser window
        response.getWriter().print("<html>\n" +
                                   "<body onload=\"window.close()\">\n" +
                                   "<div>OAuth2 complete. You may now close this window.</div>\n" +
                                   "</body>\n" +
                                   "</html>");
    }
}