/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.error;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Window;
import nextapp.echo2.app.event.WindowPaneEvent;
import nextapp.echo2.app.event.WindowPaneListener;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.ErrorDialogBuilder;
import org.openvpms.web.echo.error.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Displays errors in a dialog.
 * <p/>
 * This implementation will only display a single error dialog at a time; if an error dialog is displayed,
 * no subsequent errors will be displayed.
 * <p/>
 * This is to avoid popping up multiple dialogs relating to the same error.
 *
 * @author Tim Anderson
 */
public class DialogErrorHandler extends ErrorHandler {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DialogErrorHandler.class);

    /**
     * Handles an error.
     *
     * @param cause the cause of the error
     */
    @Override
    public void error(Throwable cause) {
        String message = ErrorFormatter.formatHTML(cause);
        error(null, message, true, cause, null);
    }

    /**
     * Handles an error.
     *
     * @param title    the error title. May be {@code null}
     * @param message  the error message
     * @param html     if {@code true}, the message is an html fragment, else it is plain text
     * @param cause    the cause. May be {@code null}
     * @param listener the listener. May be {@code null}
     */
    @Override
    public void error(String title, String message, boolean html, Throwable cause, WindowPaneListener listener) {
        log.error(html ? StringEscapeUtils.unescapeHtml4(message) : message, cause);
        if (canDisplay() && !inError()) {
            ErrorDialogBuilder builder;
            if (cause != null && ApplicationInstance.getActive() instanceof ContextApplicationInstance) {
                builder = ErrorReportingDialog.newDialog()
                        .cause(cause);
            } else {
                builder = ErrorDialog.newDialog();
            }
            builder.title(title)
                    .message(message, html)
                    .listener(listener);
            builder.show();
        } else if (listener != null) {
            // notify of immediate closure
            listener.windowPaneClosing(new WindowPaneEvent(this));
        }
    }

    /**
     * Determines if an error dialog is already being displayed.
     *
     * @return {@code true} if an error dialog is already being displayed
     */
    public boolean inError() {
        Window root = ApplicationInstance.getActive().getDefaultWindow();
        for (Component component : root.getContent().getComponents()) {
            if (component instanceof ErrorDialog) {
                return true;
            }
        }
        return false;
    }


    /**
     * Determines if the error can be displayed in the browser.
     *
     * @return {@code true} if the error can be displayed in the browser
     */
    private boolean canDisplay() {
        ApplicationInstance instance = ApplicationInstance.getActive();
        return instance != null && instance.getDefaultWindow() != null;
    }

}
