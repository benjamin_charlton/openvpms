/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.select;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.system.common.query.ArchetypeQueryException;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserFactory;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryFactory;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.property.Property;


/**
 * Selector that provides query support for partial/incorrect names.
 *
 * @author Tim Anderson
 */
public class IMObjectSelector<T extends IMObject> extends AbstractQuerySelector<T> {

    /**
     * The archetypes to query on.
     */
    private final String[] archetypes;

    /**
     * Constructs an {@link IMObjectSelector}.
     *
     * @param property the property
     * @param context  the layout context
     */
    public IMObjectSelector(Property property, LayoutContext context) {
        this(property, false, context);
    }

    /**
     * Constructs an {@link IMObjectSelector}.
     *
     * @param property    the property
     * @param allowCreate determines if objects may be created
     * @param context     the layout context
     */
    public IMObjectSelector(Property property, boolean allowCreate, LayoutContext context) {
        this(property.getDisplayName(), allowCreate, context, property.getArchetypeRange());
    }

    /**
     * Constructs an {@link IMObjectSelector}.
     *
     * @param type       display name for the types of objects this may select
     * @param context    the layout context
     * @param archetypes the archetype short names to query
     */
    public IMObjectSelector(String type, LayoutContext context, String... archetypes) {
        this(type, false, context, archetypes);
    }

    /**
     * Constructs an {@link IMObjectSelector}.
     *
     * @param type        display name for the types of objects this may select
     * @param allowCreate determines if objects may be created
     * @param context     the layout context
     * @param archetypes  the archetype short names to query
     */
    public IMObjectSelector(String type, boolean allowCreate, LayoutContext context, String... archetypes) {
        this(type, allowCreate, ButtonStyle.RIGHT, context, archetypes);
    }

    /**
     * Constructs an {@link IMObjectSelector}.
     *
     * @param type        display name for the types of objects this may select
     * @param allowCreate determines if objects may be created
     * @param style       the button style
     * @param context     the layout context
     * @param archetypes  the archetype short names to query
     */
    public IMObjectSelector(String type, boolean allowCreate, ButtonStyle style, LayoutContext context,
                            String... archetypes) {
        super(type, allowCreate, style, true, context);
        this.archetypes = archetypes;
    }

    /**
     * Returns the archetype short names to query on.
     *
     * @return the archetype short names to query on
     */
    public String[] getShortNames() {
        return archetypes;
    }

    /**
     * Returns the name of the object.
     *
     * @param object the object
     * @return the object name. May be {@code null}
     */
    @Override
    protected String getName(T object) {
        return object.getName();
    }

    /**
     * Returns the description of the object.
     *
     * @param object the object
     * @return the object description. May be {@code null}
     */
    @Override
    protected String getDescription(T object) {
        return object.getDescription();
    }

    /**
     * Determines if the object is active.
     *
     * @param object the object
     * @return {@code true} if the object is active, {@code false} if it is inactive
     */
    @Override
    protected boolean getActive(T object) {
        return object.isActive();
    }

    /**
     * Creates a new browser.
     *
     * @param value    a value to filter results by. May be {@code null}
     * @param runQuery if {@code true} run the query
     * @return a return a new browser
     */
    @Override
    protected Browser<T> createBrowser(String value, boolean runQuery) {
        Query<T> query = createQuery(value);
        if (runQuery) {
            query.setAuto(true);
        }
        return createBrowser(query);
    }

    /**
     * Returns the results matching the specified value.
     *
     * @param value the value. May be {@code null}
     * @return the results matching the value
     */
    @Override
    protected ResultSet<T> getMatches(String value) {
        return createQuery(value).query(null);
    }

    /**
     * Creates a query to select objects.
     *
     * @param value a value to filter on. May be {@code null}
     * @return a new query
     * @throws ArchetypeQueryException if the short names don't match any archetypes
     */
    protected Query<T> createQuery(String value) {
        Query<T> query = QueryFactory.create(archetypes, getContext().getContext());
        query.setValue(value);
        return query;
    }

    /**
     * Creates a new browser.
     *
     * @param query the query
     * @return a return a new browser
     */
    protected Browser<T> createBrowser(Query<T> query) {
        return BrowserFactory.create(query, getContext());
    }
}
