/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.web.component.bound.BoundTextField;
import org.openvpms.web.component.edit.AbstractPropertyEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.DefaultIMObjectTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.popup.DropDown;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;


/**
 * Editor for lookup properties that displays lookups in a drop-down.
 *
 * @author Tim Anderson
 */
public class DropdownLookupPropertyEditor extends AbstractPropertyEditor implements LookupPropertyEditor {

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The lookup query.
     */
    private final LookupQuery query;

    /**
     * The text input property.
     */
    private final SimpleProperty input;

    /**
     * The focus group.
     */
    private final FocusGroup focusGroup;

    /**
     * Input listener.
     */
    private final ModifiableListener listener;

    /**
     * The lookup drop-down component.
     */
    private final DropDown dropDown;

    /**
     * The current lookup.
     */
    private Lookup lookup;

    /**
     * The table containing matching lookups.
     */
    private PagedIMTable<Lookup> table;


    /**
     * Constructs a {@link DropdownLookupPropertyEditor}.
     *
     * @param object   the parent object
     * @param property the lookup property
     * @param context  the layout context
     */
    public DropdownLookupPropertyEditor(Property property, IMObject object, LayoutContext context) {
        super(property);
        this.context = context;
        query = createLookupQuery(property, object);
        focusGroup = new FocusGroup(getClass().getSimpleName());
        input = new SimpleProperty(property.getName(), null, String.class, property.getDisplayName());
        input.setDescription(property.getDescription());
        BoundTextField inputField = new BoundTextField(input);
        inputField.setStyleName("Selector");
        inputField.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                if (table != null && !dropDown.isExpanded()) {
                    dropDown.setExpanded(true);
                    table.getFocusGroup().setFocus();
                }
            }
        });
        dropDown = new DropDown();
        dropDown.setStyleName(Styles.DEFAULT);
        dropDown.setTarget(inputField);
        dropDown.setPopUpAlwaysOnTop(true);
        dropDown.setFocusOnExpand(true);
        listener = modifiable -> updateLookups(true);
        updateFromProperty();
    }

    /**
     * Refreshes the set of available lookups.
     */
    @Override
    public void refresh() {
        updateFromProperty();
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        return dropDown;
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group, or {@code null} if the editor hasn't been rendered
     */
    @Override
    public FocusGroup getFocusGroup() {
        return focusGroup;
    }

    /**
     * Sets the selected lookup.
     *
     * @param lookup the lookup. May be {@code null}
     */
    protected void setLookup(Lookup lookup) {
        this.lookup = lookup;
        updateText();
        Property property = getProperty();
        if (lookup == null) {
            property.setValue(null);
        } else if (property.isString()) {
            property.setValue(lookup.getCode());
        } else {
            property.setValue(lookup);
        }
    }

    /**
     * Creates a lookup query.
     *
     * @param property the lookup property
     * @param object   the parent object
     * @return a new lookup query
     */
    protected LookupQuery createLookupQuery(Property property, IMObject object) {
        return new NodeLookupQuery(object, property);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && isValidLookup(validator);
    }

    /**
     * Determines if the lookup is valid.
     *
     * @param validator the validator
     * @return {@code true} if the lookup is val id, otherwise {@code false}
     */
    protected boolean isValidLookup(Validator validator) {
        boolean result = false;
        String text = StringUtils.trimToNull(input.getString());
        if (text == null && lookup == null) {
            result = true;
        } else if (lookup != null && StringUtils.equalsIgnoreCase(lookup.getName(), text)) {
            result = true;
        } else {
            validator.add(getProperty(), Messages.format("property.error.invalidvalue", text,
                                                         getProperty().getDisplayName()));
        }
        return result;
    }

    /**
     * Determines if a lookup matches the supplied text.
     *
     * @param lookup the lookup
     * @param text   the text
     * @return {@code true} if they match, otherwise {@code false}
     */
    protected boolean matches(Lookup lookup, String text) {
        return StringUtils.startsWithIgnoreCase(lookup.getName(), text);
    }

    /**
     * Creates a table model to display lookups.
     *
     * @param context the layout context
     * @return a new table model
     */
    protected IMTableModel<Lookup> createTableModel(LayoutContext context) {
        return new DefaultIMObjectTableModel<>(true, false);
    }

    /**
     * Updates the display from the property.
     */
    private void updateFromProperty() {
        lookup = getLookup();
        updateText();
        updateLookups(false);
    }

    /**
     * Returns the lookup from the property.
     *
     * @return the lookup, or {@code null} if none exists
     */
    private Lookup getLookup() {
        Object object = getProperty().getValue();
        if (object instanceof String) {
            return (Lookup) query.getLookup((object.toString()));
        } else if (object instanceof Lookup) {
            return (Lookup) object;
        }
        return null;
    }

    /**
     * Updates the field with the current lookup.
     */
    private void updateText() {
        try {
            input.removeModifiableListener(listener);
            if (lookup != null) {
                input.setValue(lookup.getName());
            } else {
                input.setValue(null);
            }
        } finally {
            input.addModifiableListener(listener);
        }
    }

    /**
     * Updates the lookups.
     * <p>
     * If there are multiple lookups, renders a drop-down containing them beside the text field.
     *
     * @param showPopup if {@code true} display the popup if there is more than one match
     */
    private void updateLookups(boolean showPopup) {
        table = null;
        String text = StringUtils.trimToNull(input.getString());
        if (lookup != null) {
            if (StringUtils.equalsIgnoreCase(lookup.getName(), text)) {
                // already have a lookup, so list all lookups
                text = null;
            } else if (text == null) {
                // input text has been cleared. Clear the existing lookup
                setLookup(null);
            }
        }
        List<Lookup> lookups = getMatchingLookups(query, text);
        if (lookups.size() >= 1) {
            if (lookups.size() == 1) {
                setLookup(lookups.get(0));
                showPopup = false;
            }
            table = createTable(new ListResultSet<>(lookups, 5));
        }
        if (table != null) {
            table.getTable().setSelected(lookup);
            Column newValue = ColumnFactory.create(Styles.INSET, table.getComponent());
            dropDown.setPopUp(newValue);
            dropDown.setFocusComponent(table.getComponent());
        } else {
            Label component = LabelFactory.create("imobject.none");
            Column newValue = ColumnFactory.create(Styles.INSET, component);
            dropDown.setPopUp(newValue);
            dropDown.setFocusComponent(null);
        }
        if (showPopup) {
            dropDown.setExpanded(true);
        }
    }

    /**
     * Returns lookups matching the supplied text.
     *
     * @param query the lookup query
     * @param text  the text to match on. May be {@code null}
     * @return the matching lookups
     */
    private List<Lookup> getMatchingLookups(LookupQuery query, String text) {
        List<Lookup> result = new ArrayList<>();
        for (org.openvpms.component.model.lookup.Lookup lookup : query.getLookups()) {
            if (text == null || matches((Lookup) lookup, text)) {
                result.add((Lookup) lookup);
            }
        }
        return result;
    }

    /**
     * Creates a table of lookups.
     *
     * @param set the lookup result set
     * @return a new table
     */
    private PagedIMTable<Lookup> createTable(ResultSet<Lookup> set) {
        IMTableModel<Lookup> model = createTableModel(context);
        PagedIMTable<Lookup> table = new PagedIMTable<>(model, set);
        table.getTable().addActionListener(new ActionListener() {
            public void onAction(ActionEvent event) {
                Lookup selected = table.getTable().getSelected();
                setLookup(selected);
                dropDown.setExpanded(false);
            }
        });
        return table;
    }
}