/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.system.ServiceHelper;


/**
 * Context helper.
 *
 * @author Tim Anderson
 */
public class ContextHelper {

    /**
     * Sets the context customer. If the context patient doesn't have a relationship to it, sets it to {@code null}.
     *
     * @param context  the context
     * @param customer the customer
     */
    public static void setCustomer(Context context, Party customer) {
        context.setCustomer(customer);
        if (customer != null) {
            Party patient = context.getPatient();
            if (patient != null) {
                IMObjectReference ref = patient.getObjectReference();
                boolean found = false;
                for (Relationship relationship : customer.getEntityRelationships()) {
                    Reference target = relationship.getTarget();
                    if (target != null && target.equals(ref)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    context.setPatient(null);
                }
            }
        }
    }

    /**
     * Sets the context patient.
     * <p/>
     * When a patient is provided, it must have a relationship to the current customer, else the context will be
     * updated with {@code null}.
     *
     * @param context the context
     * @param patient the patient. May be {@code null}
     */
    public static void setPatient(Context context, Party patient) {
        setPatient(context, patient, true);
    }

    /**
     * Sets the context patient. If the patient doesn't have a relationship to the current customer, sets the customer
     * to the patient's owner.
     *
     * @param context                          the context
     * @param patient                          the patient. May be {@code null}
     * @param preserveCustomerIfNoRelationship if {@code true}, ignore the patient update if it doesn't have a
     *                                         relationship to the context customer. If {@code false}, and there
     *                                         is no relationship, the context customer will be set to the current
     *                                         owner, if available.
     */
    public static void setPatient(Context context, Party patient, boolean preserveCustomerIfNoRelationship) {
        if (patient == null) {
            context.setPatient(null);
        } else {
            Party customer = context.getCustomer();
            boolean found = false;
            if (customer != null) {
                Reference ref = customer.getObjectReference();
                for (Relationship relationship : patient.getEntityRelationships()) {
                    Reference source = relationship.getSource();
                    if (source != null && source.equals(ref)) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                context.setPatient(patient);
            } else if (preserveCustomerIfNoRelationship) {
                context.setPatient(null);
            } else {
                context.setPatient(patient);
                // look for an active patient-owner relationship
                for (Relationship relationship : patient.getEntityRelationships()) {
                    Reference source = relationship.getSource();
                    if (relationship.isA(PatientArchetypes.PATIENT_OWNER) && relationship.isActive()) {
                        Party owner = (Party) IMObjectHelper.getObject(source, context);
                        if (owner != null) {
                            context.setCustomer(owner);
                            found = true;
                            break;
                        }
                    }
                }
                if (!found) {
                    context.setCustomer(null);
                }
            }
        }
    }

    /**
     * Returns the currency associated with the context's practice.
     *
     * @return the currency, or {@code null} if none is found.
     */
    public static Currency getPracticeCurrency(Context context) {
        Currency result = null;
        Party practice = context.getPractice();
        if (practice != null) {
            PracticeRules rules = ServiceHelper.getBean(PracticeRules.class);
            result = rules.getCurrency(practice);
        }
        return result;
    }

}
