/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.SelectorLookupPropertyEditor;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * An editor for <em>lookup.suburb</em> properties that supports searching by postcode.
 *
 * @author Tim Anderson
 */
public class SuburbSelectorPropertyEditor extends SelectorLookupPropertyEditor {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link SelectorLookupPropertyEditor}.
     *
     * @param property the property being edited
     * @param parent   the parent object
     * @param context  the layout context
     */
    public SuburbSelectorPropertyEditor(Property property, IMObject parent, LayoutContext context) {
        super(ContactArchetypes.SUBURB, property, parent, context);
        service = ServiceHelper.getArchetypeService();
    }

    /**
     * Creates a query for the specified lookups.
     * <p/>
     * This implementation performs starts-with matches on the suburb name and postcode.
     *
     * @param archetype the lookup archetype
     * @param lookups   the lookups to query
     * @return a new query
     */
    @Override
    protected Query<Lookup> createLookupQuery(String archetype, List<Lookup> lookups) {
        return new LookupInMemoryQuery(archetype, lookups) {
            @Override
            protected boolean matches(Lookup object, String value) {
                return value == null || startsWith(object.getName(), value)
                       || startsWith(getPostcode(object), value);
            }
        };
    }

    /**
     * Returns the postcode for a suburb.
     *
     * @param lookup the suburb lookup
     * @return the postcode. May be {@code null}
     */
    private String getPostcode(Lookup lookup) {
        return service.getBean(lookup).getString("postCode");
    }
}