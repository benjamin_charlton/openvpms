/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import nextapp.echo2.app.event.ListDataEvent;
import nextapp.echo2.app.event.ListDataListener;
import nextapp.echo2.app.list.ListModel;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.list.ObjectListModel;
import org.openvpms.web.component.im.list.TextListCellRenderer;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.combo.ComboBox;

import java.util.List;

/**
 * Binds a {@link Property} to a {@link ComboBox}.
 *
 * @author Tim Anderson
 */
public abstract class BoundComboBox<T> extends ComboBox implements BoundProperty {

    /**
     * Binds the property to the field.
     */
    private final Binder binder;

    /**
     * Constructs an {@link BoundComboBox}.
     */
    public BoundComboBox(Property property, ListModel model) {
        setListModel(model);
        binder = createBinder(property);

        if (!StringUtils.isEmpty(property.getDescription())) {
            setToolTipText(property.getDescription());
        }
    }

    /**
     * Returns the selected object.
     *
     * @return the selected object. May be {@code null}
     */
    public abstract T getSelected();

    /**
     * Sets the selected object.
     *
     * @param object the selected object. May be {@code null}
     */
    public abstract void setSelected(T object);

    /**
     * Sets the default selection.
     * <p/>
     * This implementation clears the property and field.
     */
    public void setDefaultSelection() {
        getProperty().setValue(null);
        setText(null);
    }

    /**
     * Sets the list model.
     *
     * @param model the model
     */
    @Override
    public void setListModel(ListModel model) {
        super.setListModel(model);
        model.addListDataListener(new ListDataListener() {
            @Override
            public void contentsChanged(ListDataEvent e) {
                binder.setField();
            }

            @Override
            public void intervalAdded(ListDataEvent e) {
                binder.setField();
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                binder.setField();
            }
        });
    }

    /**
     * Sets the list cell renderer.
     *
     * @param renderer the list cell renderer
     */
    public void setListCellRenderer(TextListCellRenderer renderer) {
        super.setListCellRenderer(renderer);
    }

    /**
     * Returns the list cell renderer.
     *
     * @return the list cell renderer
     */
    @Override
    public TextListCellRenderer getListCellRenderer() {
        return (TextListCellRenderer) super.getListCellRenderer();
    }

    /**
     * Sets the style name.
     *
     * @param name the style name
     */
    @Override
    public void setStyleName(String name) {
        super.setStyleName(name);
        getTextField().setStyleName(name);
    }

    /**
     * Life-cycle method invoked when the {@code Component} is added to a registered hierarchy.
     */
    @Override
    public void init() {
        super.init();
        binder.bind();
    }

    /**
     * Life-cycle method invoked when the {@code Component} is removed from a registered hierarchy.
     */
    @Override
    public void dispose() {
        super.dispose();
        binder.unbind();
    }

    /**
     * Returns the property.
     *
     * @return the property
     */
    @Override
    public Property getProperty() {
        return binder.getProperty();
    }

    /**
     * Determines if a placeholder has been selected.
     * <p/>
     * A placeholder is not a real object, e.g. 'All' or 'None'.
     *
     * @return {@code true} if a placeholder has been selected
     */
    public boolean isPlaceholderSelected() {
        String text = StringUtils.trimToNull(getText());
        return getSelected() == null && text != null && isPlaceholder(text);
    }

    /**
     * Determines if the entered text represents a placeholder rather than an actual value.
     *
     * @param text the text
     * @return {@code true} if the text is a placeholder, otherwise {@code false}
     */
    protected abstract boolean isPlaceholder(String text);

    /**
     * Determines if the entered text represents a placeholder rather than an actual value.
     *
     * @param text the text
     * @return {@code true} if the text is a placeholder, otherwise {@code false}
     */
    protected boolean isPlaceholder(String text, ObjectListModel<T> model) {
        return isPlaceHolder(text, model, model.getAllIndex())
               || isPlaceHolder(text, model, model.getNoneIndex());
    }

    /**
     * Determines if the entered text represents a placeholder rather than an actual value.
     *
     * @param text  the text
     * @param model the list model
     * @param index the model index
     * @return {@code true} if the text is a placeholder, otherwise {@code false}
     */
    protected boolean isPlaceHolder(String text, ObjectListModel<T> model, int index) {
        if (index != -1) {
            String value = getListCellRenderer().getText(this, model.get(index), index);
            return text.equalsIgnoreCase(value);
        }
        return false;
    }

    /**
     * Creates a binder for the property.
     *
     * @param property the property
     * @return a new binder
     */
    protected abstract Binder createBinder(Property property);

    /**
     * Returns the binder.
     *
     * @return the binder
     */
    protected Binder getBinder() {
        return binder;
    }

    /**
     * Returns the first object with matching name, ignoring case.
     *
     * @param name    the name. May be {@code null}
     * @param objects the objects
     * @return the first match, or {@code null} if none is found
     */
    protected <R extends IMObject> R getObject(String name, List<R> objects) {
        R result = null;
        if (name != null) {
            for (R object : objects) {
                if (name.equalsIgnoreCase(getText(object))) {
                    result = object;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns the text of an object.
     * <p/>
     * This implementation returns {@link IMObject#getName()} when the object is an {@code IMObject}.
     *
     * @param object the object
     * @return the corresponding text. May be {@code null}
     */
    protected String getText(Object object) {
        return (object instanceof IMObject) ? ((IMObject) object).getName() : null;
    }

    /**
     * Returns the selected object.
     *
     * @param objects the objects to search
     * @return the selected object, or {@code null} if none is found
     */
    protected <R extends IMObject> R getSelected(List<R> objects) {
        return getObject(StringUtils.trimToNull(getText()), objects);
    }

    /**
     * Returns the text at the specified index.
     *
     * @param index the index
     * @param model the model
     * @return the corresponding text. May be {@code null}
     */
    protected String getText(int index, ObjectListModel<T> model) {
        return getListCellRenderer().getText(this, model.get(index), index);
    }

}
