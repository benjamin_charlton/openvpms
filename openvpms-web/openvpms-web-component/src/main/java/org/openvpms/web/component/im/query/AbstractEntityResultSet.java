/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.query;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.leftJoin;
import static org.openvpms.component.system.common.query.Constraints.shortName;


/**
 * Result set for {@link Entity} instances.
 *
 * @author Tim Anderson
 */
public abstract class AbstractEntityResultSet<T> extends AbstractIMObjectResultSet<T> {

    /**
     * Identity short names. Non-null if an instance name has been set, and identities are being searched,
     * and the entities have an "identities" node
     */
    private String[] identityShortNames;

    /**
     * Determines if both names and identities should be searched when searching on identity.
     */
    private boolean searchAll;


    /**
     * Constructs an {@link AbstractEntityResultSet}.
     *
     * @param archetypes       the archetypes to query
     * @param value            the value to query on. May be {@code null}
     * @param searchIdentities if {@code true} search on identity name
     * @param constraints      additional query constraints. May be {@code null}
     * @param sort             the sort criteria. May be {@code null}
     * @param rows             the maximum no. of rows per page
     * @param distinct         if {@code true} filter duplicate rows
     * @param executor         the query executor
     */
    public AbstractEntityResultSet(ShortNameConstraint archetypes, String value, boolean searchIdentities,
                                   IConstraint constraints, SortConstraint[] sort, int rows, boolean distinct,
                                   QueryExecutor<T> executor) {
        this(archetypes, value, searchIdentities, false, constraints, sort, rows, distinct, executor);
    }

    /**
     * Constructs an {@link AbstractEntityResultSet}.
     *
     * @param archetypes       the archetypes to query
     * @param value            the value to query on. May be {@code null}
     * @param searchIdentities if {@code true} search on identity name
     * @param searchAll        determines if both names and identities should be searched when {@code searchIdentities}
     *                         is set
     * @param constraints      additional query constraints. May be {@code null}
     * @param sort             the sort criteria. May be {@code null}
     * @param rows             the maximum no. of rows per page
     * @param distinct         if {@code true} filter duplicate rows
     * @param executor         the query executor
     */
    public AbstractEntityResultSet(ShortNameConstraint archetypes, String value, boolean searchIdentities,
                                   boolean searchAll, IConstraint constraints, SortConstraint[] sort, int rows,
                                   boolean distinct, QueryExecutor<T> executor) {
        super(archetypes, value, constraints, sort, rows, distinct, executor);
        if (searchIdentities) {
            // determine if "identities" nodes exist for each archetype.
            Set<String> shortNames = new HashSet<>();
            List<ArchetypeDescriptor> matches = getArchetypes(archetypes);
            boolean identities = true;
            ArchetypeService service = ServiceHelper.getArchetypeService();
            for (ArchetypeDescriptor archetype : matches) {
                NodeDescriptor descriptor = archetype.getNodeDescriptor("identities");
                if (descriptor == null) {
                    identities = false;
                    break;
                } else {
                    shortNames.addAll(Arrays.asList(DescriptorHelper.getShortNames(descriptor, service)));
                }
            }
            if (identities && !shortNames.isEmpty()) {
                identityShortNames = shortNames.toArray(new String[0]);
            }
        }
        this.searchAll = searchAll;
    }

    /**
     * Determines if the <em>identities</em> node is being searched.
     *
     * @return {@code true} if identities are being searched
     */
    public boolean isSearchingIdentities() {
        return identityShortNames != null;
    }

    /**
     * Creates a new archetype query.
     *
     * @param archetypes the archetypes
     * @return a new archetype query
     */
    @Override
    protected ArchetypeQuery createQuery(ShortNameConstraint archetypes) {
        ArchetypeQuery query = super.createQuery(archetypes);
        addIdentityConstraints(query);
        return query;
    }

    /**
     * Adds identity constraints, if searching on identity.
     *
     * @param query the query to add to
     */
    protected void addIdentityConstraints(ArchetypeQuery query) {
        if (isSearchingIdentities()) {
            query.add(leftJoin("identities", shortName("identity", identityShortNames, true)));
        }
    }

    /**
     * Creates constraints to query nodes on a value.
     *
     * @param value the value to query
     * @param nodes the nodes to query
     * @return the constraints
     */
    @Override
    protected List<IConstraint> createValueConstraints(String value, List<String> nodes) {
        boolean idSearch = isSearchingIdentities();
        List<IConstraint> constraints = (!idSearch || searchAll) ? super.createValueConstraints(value, nodes)
                                                                 : new ArrayList<>();
        if (idSearch) {
            if (!StringUtils.isEmpty(getValue())) {
                Long id = getId(value);
                if (id != null) {
                    constraints.add(eq("id", id));
                }
                constraints.add(eq("identity.name", value));
            }
        }
        return constraints;
    }
}
