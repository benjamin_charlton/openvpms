/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.audit;

import echopointng.PopUp;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.WindowPane;
import org.openvpms.component.model.object.AuditableIMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.util.IMObjectNames;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;

import java.util.Date;
import java.util.Objects;

/**
 * A popup that displays audit information for {@link AuditableIMObject}.
 * <p/>
 * For unsaved objects, the component defaults to hidden.
 * <p/>
 * Note that this subclasses {@link PopUp} in order for implementation convenience; there is currently no
 * way to render audit information on-demand in a popup without using a {@link WindowPane}, and these cannot be
 * rendered next to the button that launched them. This class pre-renders the audit information, and relies on the user
 * to invoke {@link #refresh(IMObjectNames)} if/when the {@link AuditableIMObject} changes.
 * <p/>
 * To avoid having to track each AuditInfo component to refresh it, the user can walk the Echo {@link Component}
 * hierarchy and invoke refresh on each AuditInfo component.
 * <p/>
 * This hack could be avoided if there was a PopUp implementation that could fetch the {@link #setPopUp(Component)}
 * on demand. TODO
 *
 * @author Tim Anderson
 */
public class AuditInfo extends PopUp {

    /**
     * The auditable object to render audit information for.
     */
    private final AuditableIMObject object;

    /**
     * The date the object was created.
     */
    private Date created;

    /**
     * The user the object was created by.
     */
    private Reference createdBy;

    /**
     * The date the object was updated.
     */
    private Date updated;

    /**
     * The user the object was updated by.
     */
    private Reference updatedBy;


    /**
     * Constructs an {@link AuditInfo}.
     *
     * @param object the object to render audit information for
     * @param names  the name cache
     */
    public AuditInfo(AuditableIMObject object, IMObjectNames names) {
        this.object = object;
        setStyleName("AuditInfo");
        doLayout(names);
    }

    /**
     * Refreshes audit information.
     *
     * @param names the names cache
     */
    public void refresh(IMObjectNames names) {
        if (!Objects.equals(created, object.getCreated()) || !Objects.equals(createdBy, object.getCreatedBy())
            || !Objects.equals(updated, object.getUpdated()) || !Objects.equals(updatedBy, object.getUpdatedBy())) {
            doLayout(names);
        }
    }

    /**
     * Lays out the component.
     *
     * @param names the name cache
     */
    private void doLayout(IMObjectNames names) {
        setPopUp(createPopup(object, names));
        created = object.getCreated();
        createdBy = object.getCreatedBy();
        updated = object.getUpdated();
        updatedBy = object.getUpdatedBy();
        setVisible(!object.isNew());
    }

    /**
     * Creates the audit info popup.
     *
     * @param object the object to render audit information for
     * @param names  the name cache
     * @return the popup component
     */
    private Component createPopup(AuditableIMObject object, IMObjectNames names) {
        Grid grid = GridFactory.create(3, LabelFactory.create("imobject.created"),
                                       getName(object.getCreatedBy(), names),
                                       getDate(object.getCreated()));
        if (object.getUpdated() != null && object.getUpdatedBy() != null) {
            grid.add(LabelFactory.create("imobject.updated"));
            grid.add(getName(object.getUpdatedBy(), names));
            grid.add(getDate(object.getUpdated()));
        }
        return grid;
    }

    /**
     * Returns a label for a user.
     *
     * @param user  the user reference. May be {@code null}
     * @param names the name cache
     * @return the user label
     */
    private Label getName(Reference user, IMObjectNames names) {
        String name = names.getName(user);
        Label label = LabelFactory.text(name);
        if (name == null) {
            label.setText(Messages.get("imobject.unknownauthor"));
            label.setStyleName(Styles.ITALIC);
        }
        return label;
    }

    /**
     * Formats a date label.
     *
     * @param date the date. May be {@code null}
     * @return the date label
     */
    private Label getDate(Date date) {
        String text = (date != null) ? DateFormatter.formatDateTimeAbbrev(date) : null;
        return LabelFactory.text(text);
    }

}
