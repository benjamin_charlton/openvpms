/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.error;

import org.apache.commons.codec.binary.Base64;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.service.PracticeMailService;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Sends an {@link ErrorReport} via email.
 *
 * @author Tim Anderson
 */
public class ErrorReporter {

    /**
     * Error reporter configuration.
     */
    private final ErrorReporterConfig config;

    /**
     * The to address.
     */
    private String to = "ZXJyb3ItcmVwb3J0c0BsaXN0cy5vcGVudnBtcy5vcmc=";

    /**
     * The logger.
     */
    private Logger log = LoggerFactory.getLogger(ErrorReporter.class);

    /**
     * The reporter configuration resource name.
     */
    private static final String RESOURCE = "/ErrorReporter.xml";


    /**
     * Constructs an {@link ErrorReporter}.
     */
    public ErrorReporter() {
        InputStream stream = getClass().getResourceAsStream(RESOURCE);
        if (stream == null) {
            throw new IllegalStateException("Failed to find error reporter configuration:" + RESOURCE);
        }
        config = ErrorReporterConfig.read(stream);
        to = new String(Base64.decodeBase64(to)); // poor persons anti spam....
    }

    /**
     * Reports an error.
     *
     * @param report the error report
     * @param from   the from address
     */
    public void report(ErrorReport report, String from) {
        try {
            JavaMailSender sender = ServiceHelper.getBean(PracticeMailService.class);
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            String subject = report.getVersion() + ": " + report.getMessage();
            helper.setSubject(subject);
            helper.setFrom(from);
            helper.setTo(to);
            String text = getText(report);
            if (text != null) {
                helper.setText(text);
            }
            InputStreamSource source = () -> new ByteArrayInputStream(report.toXML().getBytes());
            helper.addAttachment("error-report.xml", source, DocFormats.XML_TYPE);
            sender.send(message);
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
            ErrorDialog.show(Messages.get("errorreportdialog.senderror"));
        }
    }

    /**
     * Determines if an exception should be reported.
     *
     * @param exception the exception
     * @return {@code true} if the exception is reportable
     */
    public boolean isReportable(Throwable exception) {
        return !config.isExcluded(exception);
    }

    /**
     * Returns the message body from a report.
     *
     * @param report the report
     * @return the message body, or {@code null} if the report doesn't contain an exception
     */
    private String getText(ErrorReport report) {
        ThrowableAdapter exception = report.getException();
        if (exception != null) {
            StringWriter writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer, true);
            exception.printStackTrace(printWriter);
            return writer.getBuffer().toString();
        }
        return null;
    }

}
