/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.list;

import nextapp.echo2.app.Component;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.resource.i18n.Messages;


/**
 * A {@code ListCellRenderer} for a {@link LookupListModel}.
 *
 * @author Tim Anderson
 */
public class LookupListCellRenderer extends AllNoneListCellRenderer<String> {

    /**
     * The singleton instance.
     */
    public static LookupListCellRenderer INSTANCE = new LookupListCellRenderer();


    /**
     * Constructs a {@link LookupListCellRenderer}.
     */
    protected LookupListCellRenderer() {
        super(String.class);
    }

    /**
     * Returns the string form of the object at the specified cell.
     *
     * @param list   the list component
     * @param object the object to render. May be {@code null}
     * @param index  the object index
     * @return the text. May be {@code null}
     */
    @Override
    protected String toString(Component list, String object, int index) {
        String result = null;
        LookupListModel model = getModel(list);
        Lookup lookup = model.getLookup(index);
        if (lookup != null) {
            result = toString(lookup);
        }
        return result;
    }

    /**
     * Returns the string form of a lookup.
     *
     * @param lookup the lookup
     * @return the the string form of the lookup
     */
    protected String toString(Lookup lookup) {
        return lookup.isActive() ? lookup.getName() : Messages.format("lookup.deactivated", lookup.getName());
    }

    /**
     * Returns the list model.
     *
     * @param list the list
     * @return the list model
     */
    @Override
    protected LookupListModel getModel(Component list) {
        return (LookupListModel) super.getModel(list);
    }

}
