/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.list.ObjectListModel;
import org.openvpms.web.component.property.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Binds an {@link IMObject} {@link Property} to a combo box that displays their names.
 *
 * @author Tim Anderson
 */
public class BoundIMObjectComboBox<T extends IMObject> extends BoundComboBox<T> {

    /**
     * Constructs a {@link BoundIMObjectComboBox}.
     *
     * @param property the property
     * @param objects  the objects
     */
    public BoundIMObjectComboBox(Property property, List<? extends T> objects) {
        this(property, new ObjectListModel<>(objects, false, false));
    }

    /**
     * Constructs a {@link BoundIMObjectComboBox}.
     *
     * @param property the property
     * @param model    the model
     */
    public BoundIMObjectComboBox(Property property, ObjectListModel<T> model) {
        super(property, model);
        getBinder().bind();
    }

    /**
     * Returns the selected object.
     *
     * @return the selected object. May be {@code null}
     */
    @Override
    public T getSelected() {
        List<T> objects = getObjects();
        return getSelected(objects);
    }

    /**
     * Returns the list model.
     *
     * @return the list model
     */
    @Override
    @SuppressWarnings("unchecked")
    public ObjectListModel<T> getListModel() {
        return (ObjectListModel<T>) super.getListModel();
    }

    /**
     * Sets the selected object.
     *
     * @param object the selected object. May be {@code null}
     */
    @Override
    public void setSelected(T object) {
        getProperty().setValue(object);
    }

    /**
     * Sets the default selection.
     * <p/>
     * This implementation sets the field to either All or None, if either is present in the model, otherwise
     * it clears the field.
     */
    @Override
    public void setDefaultSelection() {
        ObjectListModel<T> model = getListModel();
        Property property = getProperty();
        if (model.getAllIndex() != -1) {
            property.setValue(null);
            setText(getText(model.getAllIndex(), model));
        } else if (model.getNoneIndex() != -1) {
            property.setValue(null);
            setText(getText(model.getNoneIndex(), model));
        } else {
            super.setDefaultSelection();
        }
    }

    /**
     * Determines if the entered text represents a placeholder rather than an actual value.
     *
     * @param text the text
     * @return {@code true} if the text is a placeholder, otherwise {@code false}
     */
    @Override
    protected boolean isPlaceholder(String text) {
        return isPlaceholder(text, getListModel());
    }

    /**
     * Returns the objects excluding any placeholders.
     *
     * @return the objects
     */
    protected List<T> getObjects() {
        ObjectListModel<T> model = getListModel();
        List<T> objects = new ArrayList<>(model.getObjects());
        objects.removeIf(Objects::isNull);   // remove all/none placeholders
        return objects;
    }

    /**
     * Creates a binder for the property.
     *
     * @param property the property
     * @return a new binder
     */
    @Override
    protected Binder createBinder(Property property) {
        return new ComboBoxBinder(property, this, false) {

            /**
             * Returns the object matching the specified text.
             *
             * @param text the text
             * @return the object, or {@code null} if there is no match
             */
            @Override
            protected T getObject(String text) {
                return BoundIMObjectComboBox.this.getObject(StringUtils.trimToNull(text), getObjects());
            }

            /**
             * Returns the text for the specified property value.
             *
             * @param value the property value
             * @return the corresponding text, or {@code null}
             */
            @Override
            protected String getText(Object value) {
                return BoundIMObjectComboBox.this.getText(value);
            }
        };
    }

}
