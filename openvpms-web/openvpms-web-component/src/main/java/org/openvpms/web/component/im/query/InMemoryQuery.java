/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.query;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.system.common.query.ArchetypeQueryException;
import org.openvpms.component.system.common.query.BaseArchetypeConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.component.system.common.util.StringUtilities;
import org.openvpms.web.component.im.util.IMObjectSorter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * An in-memory implementation of {@link Query}.
 *
 * @author Tim Anderson
 */
public class InMemoryQuery<T extends IMObject> extends AbstractArchetypeQuery<T> {

    /**
     * The objects to query.
     */
    private final List<T> objects;

    /**
     * Constructs an {@link InMemoryQuery}.
     *
     * @param archetype the archetype
     * @param type      the type that this query returns
     * @param objects   the objects to query
     * @throws ArchetypeQueryException if the short names don't match any archetypes
     */
    public InMemoryQuery(String archetype, Class<T> type, List<T> objects) {
        this(new String[]{archetype}, type, objects);
    }

    /**
     * Constructs an {@link InMemoryQuery}.
     *
     * @param archetypes the archetypes
     * @param type       the type that this query returns
     * @param objects    the objects to query
     * @throws ArchetypeQueryException if the short names don't match any archetypes
     */
    public InMemoryQuery(String[] archetypes, Class<T> type, List<T> objects) {
        super(archetypes, type);
        this.objects = objects;
    }

    /**
     * Returns the value being queried on.
     * <p>
     * This implementation does not append wildcards.
     *
     * @return the value. May be {@code null}
     */
    @Override
    public String getValue() {
        return StringUtils.trimToNull(getSearchField().getText());
    }

    /**
     * Creates the result set.
     *
     * @param sort the sort criteria. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<T> createResultSet(SortConstraint[] sort) {
        List<T> list = objects.stream()
                .filter(this::matches)
                .collect(Collectors.toList());
        SortableListResultSet<T> set = new SortableListResultSet<>(list, getMaxResults());
        set.sort(sort);
        return set;
    }

    /**
     * Determines if an object matches the query criteria.
     *
     * @param object the object to check
     * @return {@code true} if the object matches the criteria, otherwise {@code false}
     */
    protected boolean matches(T object) {
        boolean result = false;
        if (object.isA(getArchetypes().getShortNames())) {
            BaseArchetypeConstraint.State state = getActive();
            if (state == BaseArchetypeConstraint.State.ACTIVE) {
                result = object.isActive();
            } else if (state == BaseArchetypeConstraint.State.INACTIVE) {
                result = !object.isActive();
            } else {
                result = true;
            }
            if (result) {
                result = matches(object, StringUtils.trimToNull(getValue()));
            }
        }
        return result;
    }

    /**
     * Determines if an object matches the query value.
     * <p/>
     * This implementation returns {@code true} if no value is specified, or the object name starts with the value.
     *
     * @param object the object
     * @param value  the value. May be {@code null}
     * @return {@code true} if the object matches, otherwise {@code false}
     */
    protected boolean matches(T object, String value) {
        return value == null || startsWith(object.getName(), value);
    }

    /**
     * Determines if a field contains a value.
     * <p/>
     * This performs a case-insensitive search, and treats any * as a wildcard.
     *
     * @param field the field
     * @param value the value to search for
     * @return {@code true} if the field contains the value, otherwise {@code false}
     */
    protected boolean contains(String field, String value) {
        boolean result = false;
        if (field != null) {
            field = field.toLowerCase();
            value = value.toLowerCase();
            if (value.contains("*")) {
                result = StringUtilities.matches(field, value);
            } else {
                result = field.contains(value);
            }
        }
        return result;
    }

    /**
     * Determines if a field starts with a value.
     * <p/>
     * This performs a case-insensitive search, and treats any * as a wildcard.
     * <p/>
     * This enables users to override starts-with by prepending the string with a {@code *}.
     *
     * @param field the field
     * @param value the value to search for
     * @return {@code true} if the field contains the value, otherwise {@code false}
     */
    protected boolean startsWith(String field, String value) {
        boolean result = false;
        if (field != null) {
            field = field.toLowerCase();
            value = value.toLowerCase();
            if (value.contains("*")) {
                result = StringUtilities.matches(field, value);
            } else {
                result = field.startsWith(value);
            }
        }
        return result;
    }

    private static class SortableListResultSet<T extends IMObject> extends ListResultSet<T> {

        /**
         * Constructs a {@link SortableListResultSet}.
         *
         * @param objects  the objects
         * @param pageSize the maximum no. of results per page
         */
        public SortableListResultSet(List<T> objects, int pageSize) {
            super(objects, pageSize);
        }

        /**
         * Sorts the set.
         *
         * @param sort the sort criteria. May be {@code null}
         */
        @Override
        public void sort(SortConstraint[] sort) {
            if (sort != null) {
                IMObjectSorter.sort(getObjects(), sort);
            }
            super.sort(sort);
        }
    }
}