/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.query;

import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;


/**
 * Result set for {@link Entity} instances.
 *
 * @author Tim Anderson
 */
public class EntityResultSet<T extends Entity>
        extends AbstractEntityResultSet<T> {

    /**
     * Constructs an {@link EntityResultSet}.
     *
     * @param archetypes       the archetypes to query
     * @param value            the value to query on. May be {@code null}
     * @param searchIdentities if {@code true} search on identity name
     * @param constraints      additional query constraints. May be {@code null}
     * @param sort             the sort criteria. May be {@code null}
     * @param rows             the maximum no. of rows per page
     * @param distinct         if {@code true} filter duplicate rows
     */
    public EntityResultSet(ShortNameConstraint archetypes,
                           String value, boolean searchIdentities,
                           IConstraint constraints, SortConstraint[] sort,
                           int rows, boolean distinct) {
        super(archetypes, value, searchIdentities, constraints, sort,
              rows, distinct, new DefaultQueryExecutor<T>());
    }

    /**
     * Constructs an {@link EntityResultSet}.
     *
     * @param archetypes       the archetypes to query
     * @param value            the value to query on. May be {@code null}
     * @param searchIdentities if {@code true} search on identity name
     * @param searchAll        determines if both names and identities should be searched when {@code searchIdentities}
     *                         is set
     * @param constraints      additional query constraints. May be {@code null}
     * @param sort             the sort criteria. May be {@code null}
     * @param rows             the maximum no. of rows per page
     * @param distinct         if {@code true} filter duplicate rows
     */
    public EntityResultSet(ShortNameConstraint archetypes, String value, boolean searchIdentities, boolean searchAll,
                           IConstraint constraints, SortConstraint[] sort, int rows, boolean distinct) {
        super(archetypes, value, searchIdentities, searchAll, constraints, sort, rows, distinct,
              new DefaultQueryExecutor<>());
    }
}
