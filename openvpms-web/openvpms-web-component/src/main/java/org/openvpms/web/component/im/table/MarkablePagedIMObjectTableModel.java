/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.table;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.util.IMObjectHelper;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * A pageable table model that tracks marked rows across pages.
 * <p/>
 * An object's reference is used to determine if a row is marked, so only result sets containing unique objects
 * should be used.
 *
 * @author Tim Anderson
 */
public class MarkablePagedIMObjectTableModel<T extends IMObject>
        extends AbstractMarkablePagedIMTableModel<T> {

    /**
     * The references to the marked objects.
     */
    private final Set<Reference> marked = new LinkedHashSet<>();

    /**
     * Constructs a {@link MarkablePagedIMObjectTableModel}.
     *
     * @param model the underlying table model
     */
    public MarkablePagedIMObjectTableModel(IMObjectTableModel<T> model) {
        super(model);
    }

    /**
     * Returns the marked objects.
     * <p/>
     * This resolves the objects using their references.
     *
     * @return the marked objects
     */
    @SuppressWarnings("unchecked")
    public List<T> getMarked() {
        List<T> result = new ArrayList<>();
        for (Reference reference : marked) {
            T object = (T) IMObjectHelper.getObject(reference);
            if (object != null) {
                result.add(object);
            }
        }
        return result;
    }

    /**
     * Determines if at least one object is marked.
     *
     * @return {@code true} if one or more objects are marked.
     */
    @Override
    public boolean isMarked() {
        return !marked.isEmpty();
    }

    /**
     * Flags an object as marked/unmarked
     *
     * @param object the object
     * @param mark   if {@code true}, flag it is marked, else flag it as unmarked
     */
    @Override
    protected void mark(T object, boolean mark) {
        if (mark) {
            marked.add(object.getObjectReference());
        } else {
            marked.remove(object.getObjectReference());
        }
    }

    /**
     * Determines if an object is marked.
     *
     * @param object the object
     * @return {@code true if the object is marked}
     */
    @Override
    protected boolean isMarked(T object) {
        return marked.contains(object.getObjectReference());
    }

    /**
     * Clear all marks.
     */
    @Override
    protected void clearMarks() {
        marked.clear();
    }
}
