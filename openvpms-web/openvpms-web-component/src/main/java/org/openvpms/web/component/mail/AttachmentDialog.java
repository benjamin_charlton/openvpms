/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.openvpms.web.component.im.query.MultiSelectBrowser;
import org.openvpms.web.component.im.query.MultiSelectBrowserDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Mail attachment dialog.
 *
 * @author Tim Anderson
 */
class AttachmentDialog extends MultiSelectBrowserDialog<MailAttachment> {

    /**
     * Constructs an {@link AttachmentDialog}.
     *
     * @param browser the browser
     * @param help    the help context
     */
    public AttachmentDialog(MultiSelectBrowser<MailAttachment> browser, HelpContext help) {
        super(Messages.get("mail.attach.title"), browser, help);
    }

    /**
     * Updates the current selection using {@link #setSelected}.
     * <p/>
     * If the attachment is an {@link ContentAttachment}, this closes the browser.
     *
     * @param object the selected object
     */
    @Override
    protected void onBrowsed(MailAttachment object) {
        super.onBrowsed(object);
        if (object instanceof ContentAttachment) {
            onOK();
        }
    }
}
