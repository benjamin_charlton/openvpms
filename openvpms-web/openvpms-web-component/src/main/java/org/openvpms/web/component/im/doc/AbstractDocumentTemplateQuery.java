/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.ArchetypeQueryException;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.list.LookupListCellRenderer;
import org.openvpms.web.component.im.lookup.ArchetypeLookupQuery;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.query.AbstractIMObjectQuery;
import org.openvpms.web.component.im.query.EmptyResultSet;
import org.openvpms.web.component.im.query.EntityResultSet;
import org.openvpms.web.component.im.query.QueryFactory;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusHelper;

/**
 * Query for <em>entity.documentTemplate*</em> entities.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDocumentTemplateQuery extends AbstractIMObjectQuery<Entity> {

    /**
     * The types to filter on.
     */
    private String[] types = {};

    /**
     * The template type field label.
     */
    private Label typeFieldLabel;

    /**
     * The template type field.
     */
    private LookupField typeField;

    /**
     * Constructs an {@link AbstractDocumentTemplateQuery}.
     *
     * @param shortNames the short names
     * @throws ArchetypeQueryException if the short name don't match any archetypes
     */
    public AbstractDocumentTemplateQuery(String[] shortNames) {
        super(shortNames, Entity.class);
        setDefaultSortConstraint(NAME_SORT_CONSTRAINT);
        QueryFactory.initialise(this);
    }

    /**
     * Sets the document types to filter on.
     *
     * @param types the types to filter on. If empty, queries all types
     */
    public void setTemplateTypes(String... types) {
        this.types = types;
    }

    /**
     * Invoked when the short name is selected.
     */
    @Override
    protected void onShortNameChanged() {
        if (typeField != null) {
            String archetype = getShortName();
            boolean visible = archetype == null || DocumentArchetypes.DOCUMENT_TEMPLATE.equals(archetype);
            typeField.setVisible(visible);
            typeFieldLabel.setVisible(visible);
        }
    }

    /**
     * Lays out the component in a container, and sets focus on the instance
     * name.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        addShortNameSelector(container);
        addSearchField(container);
        addActive(container);
        addTypeSelector(container);
        FocusHelper.setFocus(getSearchField());
    }

    /**
     * Adds a template type selector, if <em>entity.documentTemplate</em> is among the archetypes being queried.
     *
     * @param container the container
     */
    protected void addTypeSelector(Component container) {
        if (ArrayUtils.contains(getShortNames(), DocumentArchetypes.DOCUMENT_TEMPLATE)) {
            // only add a template type selector if entity.documentTemplate is being selected
            typeFieldLabel = LabelFactory.create("document.template.type");
            ArchetypeLookupQuery lookups = new ArchetypeLookupQuery(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, types);
            typeField = LookupFieldFactory.create(lookups, true);
            typeField.setSelected((Lookup) null);
            typeField.setCellRenderer(LookupListCellRenderer.INSTANCE);
            typeField.addActionListener(new ActionListener() {
                public void onAction(ActionEvent event) {
                    onQuery();
                }
            });
            container.add(typeFieldLabel);
            container.add(typeField);
            getFocusGroup().add(typeField);
        }
    }

    /**
     * Creates the result set.
     *
     * @param sort the sort criteria. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Entity> createResultSet(SortConstraint[] sort) {
        ResultSet<Entity> result;
        String[] selectedTypes = getSelectedTypes();
        ShortNameConstraint archetypes = getArchetypeConstraint();
        if (selectedTypes.length > 0) {
            if (!ArrayUtils.contains(archetypes.getShortNames(), DocumentArchetypes.DOCUMENT_TEMPLATE)) {
                result = new EmptyResultSet<>(getMaxResults());
            } else if (archetypes.getShortNames().length > 1) {
                // the other archetypes don't have a type node, so restrict to entity.documentTemplate, which does
                archetypes = new ShortNameConstraint(DocumentArchetypes.DOCUMENT_TEMPLATE, getActive());
                result = createResultSet(archetypes, selectedTypes, sort);
            } else {
                // its an entity.documentTemplate
                result = createResultSet(archetypes, selectedTypes, sort);
            }
        } else {
            // not filtering by template type
            result = createResultSet(archetypes, selectedTypes, sort);
        }
        return result;
    }

    /**
     * Creates the result set.
     *
     * @param archetypes the archetypes to query
     * @param types      the template types. Empty if no filtering should occur
     * @param sort       the sort criteria. May be {@code null}
     * @return a new result set
     */
    protected ResultSet<Entity> createResultSet(ShortNameConstraint archetypes, String[] types, SortConstraint[] sort) {
        return new TemplateResultSet(archetypes, getValue(), sort, getMaxResults(), types, isDistinct());
    }

    /**
     * Returns the selected template types.
     *
     * @return the selected template types or an empty array if types aren't being filtered
     */
    protected String[] getSelectedTypes() {
        String type = typeField != null && typeField.isVisible() ? typeField.getSelectedCode() : null;
        String[] typeFilter;
        if (type != null) {
            typeFilter = new String[]{type};
        } else if (types.length > 0) {
            typeFilter = types;
        } else {
            typeFilter = new String[0];
        }
        return typeFilter;
    }

    protected static class TemplateResultSet extends EntityResultSet<Entity> {

        /**
         * The templates types to limit to, or an empty array to not limit.
         */
        private final String[] types;

        /**
         * Constructs a {@link TemplateResultSet}.
         *
         * @param archetypes the archetypes to query
         * @param value      the value to query on. May be {@code null}
         * @param sort       the sort criteria. May be {@code null}
         * @param rows       the maximum no. of rows per page
         * @param types      the document template types to restrict to. May be empty to not perform any restriction
         * @param distinct   if {@code true} filter duplicate rows
         */
        public TemplateResultSet(ShortNameConstraint archetypes, String value, SortConstraint[] sort, int rows,
                                 String[] types, boolean distinct) {
            super(archetypes, value, false, null, sort, rows, distinct);
            this.types = types;
        }

        /**
         * Creates a new archetype query.
         *
         * @return a new archetype query
         */
        @Override
        protected ArchetypeQuery createQuery() {
            ArchetypeQuery query = super.createQuery();
            if (types.length != 0) {
                query.add(Constraints.join("type").add(Constraints.in("code", (Object[]) types)));
            }
            return query;
        }
    }
}
