/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.macro.Macros;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.StringPropertyTransformer;
import org.openvpms.web.system.ServiceHelper;

/**
 * Helper to allow a text property belonging to a {@link DocumentAct} to be stored in a document if the content is too
 * long to fit in the underlying node.
 *
 *
 * @author Tim Anderson
 */
public class DocumentBackedTextNodeEditor {

    /**
     * The text property.
     */
    private final Property text;

    /**
     * A proxy for the text node, which can either be stored as a string, or a document
     * if its length exceeds that supported by the database.
     */
    private final SimpleProperty textProxy;

    /**
     * The document property.
     */
    private final Property document;

    /**
     * The document handler.
     */
    private final TextDocumentHandler handler;

    /**
     * Constructs a {@link DocumentBackedTextNodeEditor}.
     *
     * @param act      the document act
     * @param text     the text property
     * @param document the document property
     * @param context  the layout context
     */
    public DocumentBackedTextNodeEditor(DocumentAct act, Property text, Property document, LayoutContext context) {
        this.text = text;
        this.document = document;

        // set up a proxy for the text. This will be saved as a document if it exceeds the character limit of the
        // underlying property
        textProxy = new SimpleProperty(text.getName(), String.class);
        textProxy.setMaxLength(-1);
        textProxy.setDisplayName(text.getDisplayName());
        Macros macros = ServiceHelper.getMacros();
        textProxy.setTransformer(new StringPropertyTransformer(textProxy, false, macros, act, context.getVariables()));

        // disable macro expansion in the underlying property
        text.setTransformer(new StringPropertyTransformer(text, false));

        handler = new TextDocumentHandler(ServiceHelper.getArchetypeService());

        textProxy.setValue(text.getValue());
        if (!act.isNew()) {
            Document content = getDocument();
            if (content != null) {
                textProxy.setValue(handler.toString(content));
            }
        }
        textProxy.clearModified();
    }

    /**
     * Determines if the text has been modified.
     *
     * @return {@code true} if the text has been modified.
     */
    public boolean isModified() {
        return textProxy.isModified();
    }

    /**
     * Returns the text property.
     *
     * @return text property
     */
    public Property getText() {
        return textProxy;
    }

    /**
     * Saves the text.
     *
     * @param saveParent a callback to invoke to save the parent act
     */
    public void save(Runnable saveParent) {
        String value = textProxy.getString();
        boolean deleteDoc = false;
        Document content = getDocument();
        if (value == null || value.length() <= text.getMaxLength()) {
            text.setValue(value);
            document.setValue(null);
            if (content != null) {
                deleteDoc = true;
            }
        } else {
            text.setValue(null);
            if (content == null) {
                content = handler.create(text.getName(), value);
                document.setValue(content.getObjectReference());
            } else {
                handler.update(content, value);
            }
            ServiceHelper.getArchetypeService().save(content);
        }

        // need to save the parent before any document can be deleted to avoid foreign key errors
        saveParent.run();

        if (deleteDoc) {
            ServiceHelper.getArchetypeService().remove(content);
        }
    }

    /**
     * Returns the document.
     *
     * @return the document. May be {@code null}
     */
    private Document getDocument() {
        return (Document) IMObjectHelper.getObject(document.getReference());
    }
}
