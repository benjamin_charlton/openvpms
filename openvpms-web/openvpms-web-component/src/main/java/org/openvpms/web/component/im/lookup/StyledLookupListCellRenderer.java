/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import nextapp.echo2.app.Color;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Font;
import nextapp.echo2.app.list.AbstractListComponent;
import org.openvpms.web.component.im.list.LookupListCellRenderer;
import org.openvpms.web.component.im.list.StyledListCell;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.StyleSheetHelper;

/**
 * A {@link LookupListCellRenderer} that uses {@link StyledListCell}.
 *
 * @author Tim Anderson
 */
public class StyledLookupListCellRenderer extends LookupListCellRenderer {

    /**
     * The font.
     */
    private final Font font;

    /**
     * The background colour.
     */
    private final Color background;

    /**
     * The foreground colour.
     */
    private final Color foreground;

    public StyledLookupListCellRenderer() {
        font = (Font) StyleSheetHelper.getProperty(AbstractListComponent.class, Styles.EDIT, "font");
        background = (Color) StyleSheetHelper.getProperty(AbstractListComponent.class, Styles.EDIT, "background");
        foreground = (Color) StyleSheetHelper.getProperty(AbstractListComponent.class, Styles.EDIT, "foreground");
    }

    /**
     * Renders an object.
     *
     * @param list   the list component
     * @param object the object to render
     * @param index  the object index
     * @return the rendered object
     */
    @Override
    protected Object getComponent(Component list, String object, int index) {
        Object result = super.getComponent(list, object, index);
        if (result instanceof String) {
            result = new StyledListCell((String) result, background, foreground, font);
        }
        return result;
    }
}