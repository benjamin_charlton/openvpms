/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.Participation;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;


/**
 * Editor for {@link Act}s.
 *
 * @author Tim Anderson
 */
public abstract class AbstractActEditor extends AbstractIMObjectEditor {

    /**
     * The start time node name.
     */
    protected static final String START_TIME = "startTime";

    /**
     * The end time node name.
     */
    protected static final String END_TIME = "endTime";

    /**
     * Listener for <em>startTime</em> changes.
     */
    private final ModifiableListener startTimeListener;

    /**
     * Listener for <em>endTime</em> changes.
     */
    private final ModifiableListener endTimeListener;

    /**
     * Participation change monitors.
     */
    private final Map<String, ParticipationMonitor<?>> monitors = new HashMap<>();

    /**
     * The status the act was saved with.
     */
    private String savedStatus;

    /**
     * Constructs an {@link AbstractActEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public AbstractActEditor(Act act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        startTimeListener = modifiable -> onStartTimeChanged();
        endTimeListener = modifiable -> onEndTimeChanged();
        savedStatus = !act.isNew() ? act.getStatus() : null;
    }

    /**
     * Returns the object being edited.
     *
     * @return the object being edited
     */
    @Override
    public Act getObject() {
        return (Act) super.getObject();
    }

    /**
     * Returns the act start time.
     *
     * @return the act start time. May be {@code null}
     */
    public Date getStartTime() {
        return getObject().getActivityStartTime();
    }

    /**
     * Sets the act start time.
     *
     * @param time the start time
     */
    public void setStartTime(Date time) {
        setStartTime(time, false);
    }

    /**
     * Returns the end time.
     *
     * @return the end time. May be {@code null}
     */
    public Date getEndTime() {
        return getObject().getActivityEndTime();
    }

    /**
     * Sets the act end time.
     *
     * @param time the end time
     */
    public void setEndTime(Date time) {
        setEndTime(time, false);
    }

    /**
     * Updates the status.
     *
     * @param status the new status
     */
    public void setStatus(String status) {
        getProperty("status").setValue(status);
    }

    /**
     * Returns the status.
     *
     * @return the status. May be {@code null}
     */
    public String getStatus() {
        return getObject().getStatus();
    }

    /**
     * Returns the status the act was last saved with.
     *
     * @return the saved status. May be {@code null}
     */
    public String getSavedStatus() {
        return savedStatus;
    }

    /**
     * Validates the object.
     * <p>
     * This extends validation by ensuring that the start time is less than the end time, if non-null.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean result = super.doValidation(validator);
        if (result) {
            result = validateStartEndTimes(validator);
        }
        return result;
    }

    /**
     * Save any edits.
     * <p>
     * This uses {@link #saveChildren()} to save the children prior to invoking {@link #saveObject()}.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        super.doSave();
        updateSavedStatus();
    }

    /**
     * Updates the saved status. Should invoked by subclasses that replace the {@link #doSave()} implementation.
     */
    protected void updateSavedStatus() {
        savedStatus = getStatus();
    }

    /**
     * Deletes the object.
     * <p>
     * This uses {@link #deleteChildren()} to delete the children prior to invoking {@link #deleteObject()}.
     *
     * @throws OpenVPMSException     if the delete fails
     * @throws IllegalStateException if the act is POSTED
     */
    @Override
    protected void doDelete() {
        if (ActStatus.POSTED.equals(getSavedStatus())) {
            throw new IllegalStateException("Cannot delete " + ActStatus.POSTED + " act");
        }
        super.doDelete();
    }

    /**
     * Helper to initialises a participation, if it exists and is empty.
     *
     * @param name   the participation name
     * @param entity the participation entity. May be {@code null}
     */
    protected void initParticipant(String name, IMObject entity) {
        Reference ref = (entity != null) ? entity.getObjectReference() : null;
        initParticipant(name, ref);
    }

    /**
     * Helper to initialises a participant, if it exists and is empty.
     *
     * @param name   the participation property name
     * @param entity the entity reference. May be {@code null}
     */
    protected void initParticipant(String name, Reference entity) {
        CollectionProperty property = getCollectionProperty(name);
        if (property != null) {
            boolean create = entity != null || property.isRequired();
            Participation participation = ParticipationHelper.getParticipation(property, false);
            if (participation == null) {
                if (create) {
                    setParticipant(name, entity);
                }
            } else if (participation.getEntity() == null) {
                if (setParticipant(entity, participation)) {
                    property.refresh();
                }
            }
        }
    }

    /**
     * Sets a participant.
     *
     * @param name   the participation property name
     * @param entity the participant. May be {@code null}
     * @return {@code true} if the participant was modified, otherwise {@code false}
     */
    protected boolean setParticipant(String name, org.openvpms.component.model.entity.Entity entity) {
        boolean modified;
        ParticipationEditor<Entity> editor = getParticipationEditor(name, false);
        if (editor != null) {
            modified = editor.setEntity((Entity) entity);
        } else {
            Reference ref = (entity != null) ? entity.getObjectReference() : null;
            modified = setParticipant(name, ref);
        }
        return modified;
    }

    /**
     * Sets a participant.
     *
     * @param name   the participation property name
     * @param entity the participant. May be {@code null}
     * @return {@code true} if the participant was modified, otherwise {@code false}
     * @throws IllegalArgumentException if the name doesn't correspond to a valid node
     */
    protected boolean setParticipant(String name, Reference entity) {
        boolean modified = false;
        ParticipationEditor<?> editor = getParticipationEditor(name, false);
        if (editor != null) {
            modified = editor.setEntityRef(entity);
        } else {
            // no editor created yet. Set the participant via the corresponding property
            CollectionProperty property = getCollectionProperty(name);
            if (property == null) {
                throw new IllegalArgumentException("Invalid node: " + name);
            }
            boolean remove = entity == null && property.getMinCardinality() == 0 && property.getMaxCardinality() == 1;
            if (remove) {
                // the entity is null, and its an optional participation, so remove it if its present
                if (!property.getValues().isEmpty()) {
                    modified = property.remove(property.getValues().get(0));
                }
            } else {
                Participation participant = ParticipationHelper.getParticipation(property, false);
                if (participant == null) {
                    modified = addParticipant(property, entity);
                } else if (setParticipant(entity, participant)) {
                    modified = true;
                    property.refresh();
                }
            }
        }
        return modified;
    }

    /**
     * Monitor a participation relationship for entity changes.
     * <p/>
     * This must be invoked after layout has completed.
     *
     * @param name     the relationship name. Must refer to a collection property with max cardinality of 1
     * @param listener the listener to be notified of changes
     */
    protected <T extends Entity> void monitorParticipation(String name, Consumer<T> listener) {
        Editor editor = getEditor(name, false);
        if (editor instanceof ParticipationCollectionEditor) {
            ParticipationCollectionEditor participationEditor = (ParticipationCollectionEditor) editor;
            int maxCardinality = participationEditor.getCollection().getMaxCardinality();
            if (maxCardinality == 1) {
                ParticipationMonitor<?> existing = monitors.get(name);
                if (existing != null) {
                    existing.dispose();
                }
                monitors.put(name, new ParticipationMonitor<>(participationEditor, listener));
            }
        }
    }

    /**
     * Returns a participant reference.
     *
     * @param name the participation property name
     * @return a reference to the participant. May be {@code null}
     */
    protected Reference getParticipantRef(String name) {
        Reference result;
        ParticipationEditor<?> editor = getParticipationEditor(name, false);
        if (editor != null) {
            result = editor.getEntityRef();
        } else {
            IMObjectBean bean = getBean(getObject());
            if (bean.hasNode(name)) {
                result = bean.getTargetRef(name);
            } else {
                result = null;
            }
        }
        return result;
    }

    /**
     * Returns a participant.
     *
     * @param name the participation property name
     * @return the participant. May be {@code null}
     */
    protected Entity getParticipant(String name) {
        Reference ref = getParticipantRef(name);
        return (Entity) getObject(ref);
    }

    /**
     * Adds a participant.
     *
     * @param name   the participation collection to add to
     * @param entity the participant
     * @return {@code true} if the participant was added
     */
    protected boolean addParticipant(String name, Entity entity) {
        CollectionProperty property = getCollectionProperty(name);
        if (property == null) {
            throw new IllegalArgumentException("Argument 'name' does not refer to a valid node: " + name);
        }
        return addParticipant(property, entity);
    }

    /**
     * Removes a participant.
     *
     * @param name   the participation collection to remove from
     * @param entity the participant
     * @return {@code true} if the participant was removed
     */
    protected boolean removeParticipant(String name, Entity entity) {
        boolean removed = false;
        CollectionProperty property = getCollectionProperty(name);
        if (property == null) {
            throw new IllegalArgumentException("Argument 'name' does not refer to a valid node: " + name);
        }
        Reference reference = entity.getObjectReference();
        for (Object value : property.getValues()) {
            Participation participation = (Participation) value;
            if (Objects.equals(reference, participation.getEntity())) {
                removed = property.remove(participation);
                break;
            }
        }
        return removed;
    }


    /**
     * Returns all participants in a collection.
     *
     * @param name the collection name
     * @return the participants
     */
    protected List<Entity> getParticipants(String name) {
        List<Entity> result = new ArrayList<>();
        CollectionProperty property = getCollectionProperty(name);
        if (property == null) {
            throw new IllegalArgumentException("Argument 'name' does not refer to a valid node: " + name);
        }
        for (Object value : property.getValues()) {
            Participation participation = (Participation) value;
            Entity entity = (Entity) getObject(participation.getTarget());
            if (entity != null) {
                result.add(entity);
            }
        }
        return result;
    }

    /**
     * Returns the participation editor for the named participation node.
     * <p/>
     * NOTE: do not register listeners directly on optional participation editors, as these are destroyed when
     * the entity is cleared. Use {@link #monitorParticipation(String, Consumer)} instead to monitor changes to
     * these.
     *
     * @param name   the participation property name
     * @param create if {@code true} force creation of the edit components if it hasn't already been done
     * @return the editor corresponding to {@code name} or {@code null} if none exists or hasn't been created
     */
    @SuppressWarnings("unchecked")
    protected <T extends Entity> ParticipationEditor<T> getParticipationEditor(String name, boolean create) {
        ParticipationEditor<T> result = null;
        Editor editor = getEditor(name, create);
        if (editor instanceof ParticipationEditor) {
            result = (ParticipationEditor<T>) editor;
        } else if (editor instanceof ParticipationCollectionEditor) {
            // handle the case of an optional participation
            ParticipationCollectionEditor collectionEditor = ((ParticipationCollectionEditor) editor);
            if (collectionEditor.getEditor() instanceof SingleParticipationCollectionEditor) {
                IMObjectEditor current = collectionEditor.getCurrentEditor();
                if (current instanceof ParticipationEditor) {
                    result = (ParticipationEditor<T>) current;
                }
            }
        } else if (editor instanceof SingleParticipationCollectionEditor) {
            IMObjectEditor current = ((SingleParticipationCollectionEditor) editor).getCurrentEditor();
            if (current instanceof ParticipationEditor) {
                result = (ParticipationEditor<T>) current;
            }
        }
        return result;
    }

    /**
     * Sets the act start time.
     *
     * @param time    the start time
     * @param disable if {@code true} disable the {@link #onStartTimeChanged} callback
     */
    protected void setStartTime(Date time, boolean disable) {
        Property startTime = getProperty(START_TIME);
        if (disable) {
            removeStartEndTimeListeners();
        }
        startTime.setValue(time);
        if (disable) {
            addStartEndTimeListeners();
        }
    }

    /**
     * Sets the act end time.
     *
     * @param time    the end time
     * @param disable if {@code true} disable the {@link #onEndTimeChanged} callback
     */
    protected void setEndTime(Date time, boolean disable) {
        Property endTime = getProperty(END_TIME);
        if (disable) {
            removeStartEndTimeListeners();
        }
        endTime.setValue(time);
        if (disable) {
            addStartEndTimeListeners();
        }
    }

    /**
     * Adds act start/end time modification callbacks.
     * When enabled, changes to the <em>startTime</em> property trigger
     * {@link #onStartTimeChanged()} and changes to <em>endTime</em> trigger
     * {@link #onEndTimeChanged()}.
     */
    protected void addStartEndTimeListeners() {
        Property startTime = getProperty(START_TIME);
        if (startTime != null) {
            startTime.addModifiableListener(startTimeListener);
        }

        Property endTime = getProperty(END_TIME);
        if (endTime != null) {
            endTime.addModifiableListener(endTimeListener);
        }
    }

    /**
     * Removes act start/end time modification callbacks.
     */
    protected void removeStartEndTimeListeners() {
        Property startTime = getProperty(START_TIME);
        if (startTime != null) {
            startTime.removeModifiableListener(startTimeListener);
        }

        Property endTime = getProperty(END_TIME);
        if (endTime != null) {
            endTime.removeModifiableListener(endTimeListener);
        }
    }

    /**
     * Invoked when the start time changes. Sets the value to end time if
     * start time > end time.
     */
    protected void onStartTimeChanged() {
        Date start = getStartTime();
        Date end = getEndTime();
        if (start != null && end != null) {
            if (start.compareTo(end) > 0) {
                setStartTime(end, true);
            }
        }
    }

    /**
     * Invoked when the end time changes. Sets the value to start time if
     * end time < start time.
     */
    protected void onEndTimeChanged() {
        Date start = getStartTime();
        Date end = getEndTime();
        if (start != null && end != null) {
            if (end.compareTo(start) < 0) {
                setEndTime(start, true);
            }
        }
    }

    /**
     * Validates that the start and end times are valid.
     *
     * @param validator the validator
     * @return {@code true} if the start and end times are valid
     */
    protected boolean validateStartEndTimes(Validator validator) {
        boolean result = true;
        Date start = getStartTime();
        Date end = getEndTime();
        if (start != null && end != null) {
            if (start.getTime() > end.getTime()) {
                String startName = getPropertyDisplayName(START_TIME);
                String endName = getPropertyDisplayName(END_TIME);
                String message = Messages.format("act.validation.startGreaterThanEnd", startName, endName);
                validator.add(this, new ValidatorError(message));
                result = false;
            }
        }
        return result;
    }

    /**
     * Helper to return the display name of a property.
     *
     * @param name the property name
     * @return the property's display name, or {@code name} if the property doesn't exist
     */
    protected String getPropertyDisplayName(String name) {
        Property property = getProperty(name);
        return (property != null) ? property.getDisplayName() : name;
    }

    /**
     * Adds a participant.
     *
     * @param property the participation collection to add to
     * @param entity   the participant
     * @return {@code true} if the participant was added
     */
    private boolean addParticipant(CollectionProperty property, Entity entity) {
        return addParticipant(property, entity != null ? entity.getObjectReference() : null);
    }

    /**
     * Adds a participant.
     *
     * @param property the participation collection to add to
     * @param entity   the participant
     * @return {@code true} if the participant was added
     */
    private boolean addParticipant(CollectionProperty property, Reference entity) {
        boolean result = false;
        Participation participation = ParticipationHelper.create(property);
        if (participation != null) {
            setParticipant(entity, participation);
            property.add(participation);
            result = true;
        }
        return result;
    }

    /**
     * Sets the participant on a participation.
     *
     * @param entity        the participant. May be {@code null}
     * @param participation the participation to update
     * @return {@code true} if the participation was modified
     */
    private boolean setParticipant(Reference entity, Participation participation) {
        boolean modified = false;
        if (participation.getAct() == null) {
            participation.setAct(getObject().getObjectReference());
            modified = true;
        }
        if (!Objects.equals(participation.getEntity(), entity)) {
            participation.setEntity(entity);
            modified = true;
        }
        return modified;
    }
}
