/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.print;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.practice.Location;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.report.openoffice.Converter;

/**
 * Provides services to support printing.
 *
 * @author Tim Anderson
 */
public class PrinterContext {

    /**
     * The printer locator.
     */
    private final DocumentPrinterServiceLocator printerLocator;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document converter.
     */
    private final Converter converter;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link PrinterContext}.
     *
     * @param printerLocator the printer locator
     * @param service        the archetype service
     * @param handlers       the document handlers
     * @param converter      the document converter
     * @param domainService  the domain object service
     */
    public PrinterContext(DocumentPrinterServiceLocator printerLocator, IArchetypeService service,
                          DocumentHandlers handlers, Converter converter, DomainService domainService) {
        this.printerLocator = printerLocator;
        this.service = service;
        this.handlers = handlers;
        this.converter = converter;
        this.domainService = domainService;
    }

    /**
     * Returns a printer given its reference.
     *
     * @param reference the printer reference
     * @return the corresponding printer. May be {@code null}
     */
    public DocumentPrinter getPrinter(PrinterReference reference) {
        return printerLocator.getPrinter(reference.getArchetype(), reference.getId());
    }

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the printer. May be {@code null}
     */
    public DocumentPrinter getDefaultPrinter(Party location) {
        DocumentPrinter result;
        IMObjectBean bean = service.getBean(location);
        PrinterReference reference = PrinterReference.fromString(bean.getString("defaultPrinter"));
        if (reference != null) {
            result = getPrinter(reference);
        } else {
            result = printerLocator.getDefaultPrinter(domainService.create(location, Location.class));
        }
        return result;
    }

    /**
     * Returns the default printer.
     *
     * @return the default printer. May be {@code null}
     */
    public DocumentPrinter getDefaultPrinter() {
        return printerLocator.getDefaultPrinter();
    }

    /**
     * Returns the printer locator.
     *
     * @return the printer locator
     */
    public DocumentPrinterServiceLocator getPrinterLocator() {
        return printerLocator;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    public IArchetypeService getService() {
        return service;
    }

    /**
     * Returns the document handlers.
     *
     * @return the document handlers
     */
    public DocumentHandlers getDocumentHandlers() {
        return handlers;
    }

    /**
     * Returns the document converter.
     *
     * @return the document converter
     */
    public Converter getConverter() {
        return converter;
    }

}
