/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.help.HelpDialog;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.AbstractEntityQuery;
import org.openvpms.web.component.im.query.EntityResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.select.AbstractSelectorListener;
import org.openvpms.web.component.im.select.IMObjectSelector;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;

import static org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes.INVESTIGATION_TYPE;

/**
 * A query for <em>entity.laboratoryTest*</em>s that supports filtering tests by investigation type.
 *
 * @author Tim Anderson
 */
public class TestQuery extends AbstractEntityQuery<Entity> {

    /**
     * Investigation type selector.
     */
    private final IMObjectSelector<Entity> investigationType;

    /**
     * Constructs a {@link TestQuery}.
     *
     * @param context the context
     */
    public TestQuery(Context context) {
        this(new String[]{LaboratoryArchetypes.TEST, LaboratoryArchetypes.HL7_TEST}, context);
    }

    /**
     * Constructs a {@link TestQuery}.
     *
     * @param archetypes the test archetypes
     * @param context    the context
     */
    public TestQuery(String[] archetypes, Context context) {
        super(archetypes);
        HelpContext help = HelpDialog.getHelpContext("entity.laboratoryTest/*");
        investigationType = createInvestigationTypeSelector(new DefaultLayoutContext(context, help));
        setSearchAll(true);
    }


    /**
     * Lays out the component in a container, and sets focus on the instance
     * name.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        super.doLayout(container);
        container.add(LabelFactory.text(investigationType.getType()));
        Component component = investigationType.getComponent();
        container.add(component);
        investigationType.getSelect().setFocusTraversalParticipant(false);
        getFocusGroup().add(investigationType.getTextField());
    }

    /**
     * Creates the result set.
     *
     * @param sort the sort criteria. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Entity> createResultSet(SortConstraint[] sort) {
        Entity investigationType = this.investigationType.getObject();
        IConstraint constraints = null;
        if (investigationType != null) {
            constraints = Constraints.join("investigationType").add(Constraints.eq("target", investigationType));
        }

        ShortNameConstraint archetypes = getArchetypeConstraint();
        archetypes.setAlias("e"); // NOTE: need an alias for selects() to work
        return new EntityResultSet<>(archetypes, getValue(), isIdentitySearch(), isSearchAll(),
                                     constraints, sort, getMaxResults(), isDistinct());
    }

    /**
     * Creates a field to select the investigation type.
     *
     * @param context the layout context
     * @return a new selector
     */
    private IMObjectSelector<Entity> createInvestigationTypeSelector(LayoutContext context) {
        IMObjectSelector<Entity> selector = new IMObjectSelector<>(
                DescriptorHelper.getDisplayName(INVESTIGATION_TYPE, getService()), context, INVESTIGATION_TYPE);
        AbstractSelectorListener<Entity> listener = new AbstractSelectorListener<Entity>() {
            public void selected(Entity object) {
                onQuery();
            }
        };
        selector.setListener(listener);
        return selector;
    }

}
