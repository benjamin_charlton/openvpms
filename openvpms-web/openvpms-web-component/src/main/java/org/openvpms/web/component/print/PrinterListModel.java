/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.web.component.im.list.ObjectListModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * List model for {@link PrinterReference}s.
 *
 * @author Tim Anderson
 * @see PrinterListCellRenderer
 */
public class PrinterListModel extends ObjectListModel<PrinterReference> {

    /**
     * Constructs a {@link PrinterListModel}.
     *
     * @param references the printer references
     */
    public PrinterListModel(Collection<PrinterReference> references) {
        super(sort(references), false, false);
    }

    /**
     * Constructs a {@link PrinterListModel} for all available printers.
     */
    public PrinterListModel() {
        super(sort(PrintHelper.getPrinters()), false, false);
    }

    /**
     * Sorts references.
     *
     * @param references the references to sort
     * @return the sorted references
     */
    protected static List<PrinterReference> sort(Collection<PrinterReference> references) {
        List<PrinterReference> result = new ArrayList<>(references);
        Collections.sort(result);
        return result;
    }

}
