/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.filter;

import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;


/**
 * Basic implementation of the {@link NodeFilter} interface, that enables hidden
 * and optional fields to be included or excluded.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class BasicNodeFilter implements NodeFilter {

    /**
     * If {@code true} show optional fields, as well as mandatory ones.
     */
    private final boolean showOptional;

    /**
     * If {@code true}, show hidden fields.
     */
    private final boolean showHidden;


    /**
     * Construct a new {@code BasicNodeFilter}.
     *
     * @param showOptional if {@code true} show optional fields as well as
     *                     mandatory ones.
     */
    public BasicNodeFilter(boolean showOptional) {
        this(showOptional, false);
    }

    /**
     * Construct a new {@code BasicNodeFilter}.
     *
     * @param showOptional if {@code true} show optional fields as well as
     *                     mandatory ones.
     * @param showHidden   if {@code true} show hidden fields
     */
    public BasicNodeFilter(boolean showOptional, boolean showHidden) {
        this.showOptional = showOptional;
        this.showHidden = showHidden;
    }

    /**
     * Determines if optional fields should be displayed.
     *
     * @return {@code true} if optional fields should be displayed
     */
    public boolean showOptional() {
        return showOptional;
    }

    /**
     * Determines if hidden fields should be displayed.
     *
     * @return {@code true} if hidden fields should be displayed
     */
    public boolean showHidden() {
        return showHidden;
    }

    /**
     * Determines if a node should be included.
     *
     * @param descriptor the node descriptor
     * @param object     the object
     * @return {@code true} if the node should be included; otherwise
     *         {@code false}
     */
    public boolean include(NodeDescriptor descriptor, IMObject object) {
        boolean result = false;
        if (descriptor.isHidden()) {
            if (showHidden()) {
                result = true;
            }
        } else if (showOptional()) {
            result = true;
        } else {
            result = descriptor.isRequired();
        }
        return result;
    }
}
