/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.list.IMObjectListCellRenderer;
import org.openvpms.web.component.im.list.ObjectListModel;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;
import java.util.Objects;

/**
 * An editor for {@link IMObject} properties that uses a {@link BoundComboBox} to display the objects.
 *
 * @author Tim Anderson
 */
public class ComboBoxPropertyEditor<T extends IMObject> extends PropertyComponentEditor {

    /**
     * Constructs a {@link ComboBoxPropertyEditor}.
     *
     * @param property the property
     * @param objects  the available objects
     */
    public ComboBoxPropertyEditor(Property property, List<? extends T> objects) {
        this(property, new ObjectListModel<>(objects, false, false));
    }

    /**
     * Constructs a {@link ComboBoxPropertyEditor}.
     *
     * @param property the property
     * @param objects  the available objects
     */
    public ComboBoxPropertyEditor(Property property, ObjectListModel<T> objects) {
        this(createComboBox(property, objects));
    }

    /**
     * Constructs a {@link ComboBoxPropertyEditor}.
     *
     * @param comboBox the combo box
     */
    public ComboBoxPropertyEditor(BoundComboBox<T> comboBox) {
        super(comboBox.getProperty(), comboBox);
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    @SuppressWarnings("unchecked")
    public BoundComboBox<T> getComponent() {
        return (BoundComboBox<T>) super.getComponent();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateText(validator);
    }

    /**
     * Creates a new combo box.
     *
     * @param property the property to bind
     * @param objects  the available objects
     * @return a new combo box, bound to the property
     */
    protected static <R extends IMObject> BoundIMObjectComboBox<R> createComboBox(
            Property property, ObjectListModel<R> objects) {
        BoundIMObjectComboBox<R> result = new BoundIMObjectComboBox<>(property, objects);
        result.setListCellRenderer(IMObjectListCellRenderer.NAME);
        result.setStyleName(Styles.EDIT);
        return result;
    }

    /**
     * Verifies that the combobox text corresponds to a lookup.
     *
     * @param validator the validator
     * @return {@code true} if the text is valid, otherwise {@code false}
     */
    private boolean validateText(Validator validator) {
        boolean result = false;
        BoundComboBox<T> component = getComponent();
        String text = StringUtils.trimToNull(component.getText());
        T object = component.getSelected();
        String name = (object != null) ? component.getText(object) : null;
        if (Objects.equals(name, text)) {
            result = true;
        } else if (name != null && text != null) {
            result = name.equalsIgnoreCase(text);
        }
        if (!result && component.isPlaceholder(text)) {
            result = true;
        }
        if (!result) {
            validator.add(this, Messages.format("property.error.invalidvalue", text, getProperty().getDisplayName()));
        }
        return result;
    }
}
