/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A property set that allows properties to be redefined.
 *
 * @author Tim Anderson
 */
public class MutablePropertySet extends AbstractPropertySet {

    /**
     * The properties.
     */
    private final Map<String, Property> properties = new LinkedHashMap<>();

    /**
     * The underlying property set.
     */
    private final PropertySet set;

    /**
     * Constructs a {@link MutablePropertySet}.
     *
     * @param set the underlying property set
     */
    public MutablePropertySet(PropertySet set) {
        this.set = set;
        for (Property property : set.getProperties()) {
            properties.put(property.getName(), property);
        }
    }

    /**
     * Returns the named property.
     *
     * @param name the name
     * @return the property corresponding to {@code name}, or {@code null} if none exists
     */
    @Override
    public Property get(String name) {
        return properties.get(name);
    }

    /**
     * Returns the properties.
     *
     * @return the properties
     */
    @Override
    public Collection<Property> getProperties() {
        return properties.values();
    }

    /**
     * Updates derived properties. Any derived property that has changed
     * since the last call will notify their registered listeners.
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public void updateDerivedProperties() {
        set.updateDerivedProperties();
    }

    /**
     * Marks a property as required.
     *
     * @param name the property name
     * @return the property corresponding to {@code name}, or {@code null} if none exists
     */
    public Property setRequired(String name) {
        Property property = get(name);
        if (property != null) {
            property = new RequiredProperty(property);
            properties.put(name, property);
        }
        return property;
    }

    /**
     * Marks a property as read-only.
     *
     * @param name the property name
     * @return the property corresponding to {@code name}, or {@code null} if none exists
     */
    public Property setReadOnly(String name) {
        Property property = get(name);
        if (property != null) {
            property = new ReadOnlyProperty(property);
            properties.put(name, property);
        }
        return property;
    }

    /**
     * Marks a property as hidden.
     *
     * @param name the property name
     * @return the property corresponding to {@code name}, or {@code null} if none exists
     */
    public Property setHidden(String name) {
        Property property = get(name);
        if (property != null) {
            property = new DelegatingProperty(property) {
                @Override
                public boolean isHidden() {
                    return true;
                }
            };
            properties.put(name, property);
        }
        return property;
    }

}
