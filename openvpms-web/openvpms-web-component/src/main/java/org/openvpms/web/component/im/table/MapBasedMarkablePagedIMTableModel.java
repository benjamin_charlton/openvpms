/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.table;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * A {@link PagedIMTableModel} where the key and value are stored in a map.
 *
 * @author Tim Anderson
 */
public class MapBasedMarkablePagedIMTableModel<K, V> extends AbstractMapBasedMarkablePagedIMTableModel<K, V> {

    /**
     * The key accessor.
     */
    private final Function<V, K> accessor;

    /**
     * Constructs an {@link MapBasedMarkablePagedIMTableModel}.
     *
     * @param model    the underlying table model
     * @param accessor the function to get the key for a value
     */
    public MapBasedMarkablePagedIMTableModel(IMTableModel<V> model, Function<V, K> accessor) {
        this(model, new LinkedHashMap<>(), accessor);
    }

    /**
     * Constructs an {@link MapBasedMarkablePagedIMTableModel}.
     *
     * @param model    the underlying table model
     * @param map      the map to use
     * @param accessor the function to get the key for a value
     */
    public MapBasedMarkablePagedIMTableModel(IMTableModel<V> model, Map<K, V> map, Function<V, K> accessor) {
        super(model, map);
        this.accessor = accessor;
    }

    /**
     * Returns the key for a value.
     *
     * @param value the value
     * @return the corresponding key
     */
    @Override
    protected K getKey(V value) {
        return accessor.apply(value);
    }
}