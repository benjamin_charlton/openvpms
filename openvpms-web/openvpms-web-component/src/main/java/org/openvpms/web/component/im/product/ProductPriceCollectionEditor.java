/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.button.ButtonGroup;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.product.PricingGroup;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.product.ProductPrice;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.bound.BoundDateFieldFactory;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectTableCollectionEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.IMObjectListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An editor for {@link ProductPrice} collections.
 * <p/>
 * This enables new prices to be created from existing prices, and prices to be filtered by pricing location.
 *
 * @author Tim Anderson
 */
public class ProductPriceCollectionEditor extends IMObjectTableCollectionEditor {

    /**
     * The product price rules.
     */
    private final ProductPriceRules rules;

    /**
     * The pricing location filter.
     */
    private PricingGroupFilter filter;

    /**
     * Pricing groups node.
     */
    private static final String PRICING_GROUPS = "pricingGroups";

    /**
     * Copy button id.
     */
    private static final String COPY_ID = "button.copy";

    /**
     * Copy and close button id.
     */
    private static final String COPY_AND_CLOSE_ID = "button.copyAndClose";

    /**
     * Constructs a {@link ProductPriceCollectionEditor}.
     *
     * @param property the collection property
     * @param object   the object being edited
     * @param context  the layout context
     */
    public ProductPriceCollectionEditor(CollectionProperty property, IMObject object, LayoutContext context) {
        super(property, object, context);
        filter = new PricingGroupFilter(context);
        rules = ServiceHelper.getBean(ProductPriceRules.class);
    }

    /**
     * Returns the current editor.
     *
     * @return the current editor. May be {@code null}
     */
    @Override
    public ProductPriceEditor getCurrentEditor() {
        return (ProductPriceEditor) super.getCurrentEditor();
    }

    /**
     * Creates a new price, subject to a short name being selected.
     * <p/>
     * If there is a pricing group selected, this will be added to the price.
     *
     * @return a new price, or {@code null} if the price can't be created
     */
    @Override
    public IMObject create() {
        IMObject object = super.create();
        PricingGroup pricingGroup = filter.getPricingGroup();
        if (object != null && pricingGroup != null && pricingGroup.getGroup() != null) {
            IMObjectBean bean = getBean(object);
            bean.addValue("pricingGroups", pricingGroup.getGroup());
        }
        return object;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateUnitPrices(validator);
    }

    /**
     * Returns the selected object.
     *
     * @return the selected object. May be {@code null}
     */
    @Override
    protected ProductPrice getSelected() {
        return (ProductPrice) super.getSelected();
    }

    /**
     * Creates a new price from an existing price, and sets the end date of the existing price.
     */
    protected void copyAndClose() {
        ProductPrice selected = getSelected();
        if (selected != null) {
            NewPricePrompt prompt = new NewPricePrompt(selected);
            prompt.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    doCopyAndClose(selected, prompt.getFromDate());
                }
            });
            prompt.show();
        }
    }

    /**
     * Creates the row of controls.
     *
     * @param focus the focus group
     * @return the row of controls
     */
    @Override
    protected ButtonRow createControls(FocusGroup focus) {
        ButtonRow row = super.createControls(focus);
        if (filter.needsFilter()) {
            row.add(filter.getComponent());
            filter.setListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    populateTable();
                }
            });
        }
        return row;
    }

    /**
     * Adds add/delete buttons to the button row, and previous/next buttons, if the cardinality is > 1.
     *
     * @param buttons the buttons to add to
     */
    @Override
    protected void addButtons(ButtonRow buttons) {
        buttons.addButton(COPY_ID, new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                copy();
            }
        });
        buttons.addButton(COPY_AND_CLOSE_ID, new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                copyAndClose();
            }
        });
        super.addButtons(buttons);
    }

    /**
     * Enable/disables the buttons.
     *
     * @param buttons   the buttons
     * @param enable    if {@code true}, enable buttons (subject to criteria), otherwise disable them
     * @param enableAdd if {@code true}, enable the add button (subject to criteria), otherwise disable it
     */
    @Override
    protected void enableNavigation(ButtonSet buttons, boolean enable, boolean enableAdd) {
        if (enable) {
            ProductPrice selected = getSelected();
            if (selected != null) {
                boolean hasToDate = selected.getToDate() != null;
                buttons.setEnabled(COPY_AND_CLOSE_ID, !hasToDate);
                buttons.setEnabled(COPY_ID, hasToDate || selected.isFixed());
                // fixed prices can always be copied, as the user can select from them. For unit prices, there should
                // only be a single unit price for a given date (subject to pricing groups)
            }
        } else {
            buttons.setEnabled(COPY_AND_CLOSE_ID, false);
            buttons.setEnabled(COPY_ID, false);
        }
        super.enableNavigation(buttons, enable, enableAdd);
    }

    /**
     * Determines if the delete button should be enabled.
     * <p/>
     * This implementation disables delete if the {@link ProductPriceEditor#isReadOnly()} is {@code true}.
     *
     * @return {@code true} if the delete button should be enabled, {@code false} if it should be disabled
     */
    @Override
    protected boolean getEnableDelete() {
        boolean result = super.getEnableDelete();
        if (result) {
            ProductPriceEditor editor = getCurrentEditor();
            result = (editor != null && !editor.isReadOnly());
        }
        return result;
    }

    /**
     * Returns the buttons.
     *
     * @return the buttons. May be {@code null}
     */
    @Override
    protected ButtonSet getButtons() {
        return super.getButtons();
    }

    /**
     * Creates a new result set for display.
     *
     * @return a new result set
     */
    @Override
    protected ResultSet<IMObject> createResultSet() {
        CollectionPropertyEditor editor = getCollectionPropertyEditor();
        List<IMObject> objects = filter.getPrices(editor.getObjects());
        return new IMObjectListResultSet<>(objects, ROWS);
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        context = new DefaultLayoutContext(context);
        context.setComponentFactory(new TableComponentFactory(context));
        ProductPriceTableModel model = new ProductPriceTableModel(getProperty().getArchetypeRange(), context);
        if (filter.needsFilter()) {
            model.setShowPricingGroups(true);
        }
        return model;
    }

    /**
     * Selects a price and edits it.
     *
     * @param price the price
     */
    protected void selectAndEdit(IMObject price) {
        setSelected(price);
        edit(price);
    }

    /**
     * Copies the selected price, and sets the new price to start from the selected price's to-date, or now
     * if it doesn't have one.
     */
    private void copy() {
        ProductPrice selected = getSelected();
        if (selected != null) {
            Date date = selected.getToDate();
            if (date == null) {
                date = new Date();
            }
            ProductPrice newPrice = (ProductPrice) rules.copy(selected, date);
            editNew(newPrice);
        }
    }

    /**
     * Creates a new product price from an existing one, closing the existing one by setting its {@code toDate}
     * to the supplied {@code date}.
     *
     * @param price the existing price
     * @param date  the date used to end the price and start the new price
     */
    private void doCopyAndClose(ProductPrice price, Date date) {
        ProductPrice newPrice = (ProductPrice) rules.copyAndClose(price, date);
        editNew(newPrice);
    }

    /**
     * Edits a new price.
     * <p/>
     * This adds it to the product, refreshes the table, selects it and then edits it.
     *
     * @param newPrice the new price
     */
    private void editNew(ProductPrice newPrice) {
        add(newPrice);
        refresh(false);
        selectAndEdit(newPrice);
    }

    /**
     * Verifies that:
     * <ul>
     * <li>unit prices with no pricing groups don't overlap other unit prices with no pricing groups on date
     * range</li>
     * <li>unit prices with pricing groups that overlap on date range have different pricing groups</li>
     * </ul>
     *
     * @param validator the validator
     * @return {@code true} if the prices are valid
     */
    private boolean validateUnitPrices(Validator validator) {
        boolean valid = true;
        List<ProductPrice> unitPrices = new ArrayList<>();
        for (IMObject price : getCurrentObjects()) {
            if (price.isA(ProductArchetypes.UNIT_PRICE)) {
                unitPrices.add((ProductPrice) price);
            }
        }

        while (unitPrices.size() > 1) {
            ProductPrice price = unitPrices.remove(0);
            for (ProductPrice other : unitPrices) {
                if (DateRules.intersects(price.getFromDate(), price.getToDate(), other.getFromDate(),
                                         other.getToDate())) {
                    if (DateRules.dateEquals(price.getToDate(), other.getFromDate())) {
                        // intersection is on time only. Make them them so that they don't intersect
                        setToDate(price, other.getFromDate());
                    } else if (DateRules.dateEquals(price.getFromDate(), other.getToDate())) {
                        setToDate(other, price.getFromDate());
                    } else {
                        IMObjectBean priceBean = getBean(price);
                        IMObjectBean otherBean = getBean(other);
                        List<Lookup> priceGroups = priceBean.getValues(PRICING_GROUPS, Lookup.class);
                        List<Lookup> otherGroups = otherBean.getValues(PRICING_GROUPS, Lookup.class);
                        if (priceGroups.isEmpty() && otherGroups.isEmpty()) {
                            validator.add(getEditor(price), new ValidatorError(
                                    Messages.format("product.price.dateOverlap", formatPrice(price),
                                                    formatPrice(other))));
                            valid = false;
                            break;
                        } else if (priceGroups.removeAll(otherGroups)) {
                            validator.add(getEditor(price), new ValidatorError(
                                    Messages.format("product.price.groupOverlap", formatPrice(price),
                                                    formatPrice(other))));
                            valid = false;
                            break;
                        }
                    }
                }
            }
        }
        return valid;
    }

    /**
     * Sets the to-date of a price.
     *
     * @param price the price
     * @param date  the date
     */
    private void setToDate(ProductPrice price, Date date) {
        IMObjectEditor editor = getEditor(price);
        if (editor instanceof ProductPriceEditor) {
            ((ProductPriceEditor) editor).setToDate(date);
        } else {
            price.setToDate(date);
        }
    }

    /**
     * Formats a price for error reporting.
     *
     * @param price the price
     * @return the price text
     */
    private String formatPrice(org.openvpms.component.model.product.ProductPrice price) {
        String amount = NumberFormatter.formatCurrency(price.getPrice());
        if (price.getFromDate() != null) {
            String date = DateFormatter.formatDate(price.getFromDate(), false);
            return Messages.format("product.price.priceWithStartDate", amount, date);
        }
        return Messages.format("product.price.priceWithNoStartDate", amount);
    }

    /**
     * Prompt to start a new price from a particular date, closing the existing price.
     */
    private class NewPricePrompt extends ModalDialog {

        /**
         * The option to start the new price from now.
         */
        private final RadioButton nowOption;

        /**
         * The option to start the new price from th specified date.
         */
        private final RadioButton fromOption;

        /**
         * The prompt.
         */
        private final String message;

        /**
         * The footnote.
         */
        private String footnote;

        /**
         * The minimum date that may be entered. May be {@code null}
         */
        private Date minimum;

        /**
         * The from-date property.
         */
        private SimpleProperty fromDate = new SimpleProperty("fromDate", Date.class);

        /**
         * Constructs a {@link NewPricePrompt}.
         *
         * @param productPrice the price to clone from
         */
        public NewPricePrompt(ProductPrice productPrice) {
            super(null, "MessageDialog", OK_CANCEL);
            IMObjectBean bean = getBean(productPrice);
            String displayName = bean.getDisplayName();
            setTitle(Messages.format("editor.new.title", displayName));
            message = Messages.format("product.price.new.message", displayName);
            ButtonGroup group = new ButtonGroup();
            nowOption = ButtonFactory.create("product.price.start.now", group);
            fromOption = ButtonFactory.create("product.price.start.from", group);
            footnote = Messages.format("product.price.new.footnote", displayName);

            Date to = productPrice.getToDate();
            Date from = productPrice.getFromDate();
            if (to != null) {
                this.fromDate.setValue(to);
            } else {
                Date now = new Date();
                if (from != null && DateRules.compareTo(from, now) > 0) {
                    // future dated from-date
                    fromDate.setValue(from);
                } else {
                    // no date, so default to now
                    fromDate.setValue(now);
                }
            }
            minimum = from;
        }

        /**
         * Returns the from-date, if either the now or from option has been selected.
         *
         * @return the from-date. May be {@code null}
         */
        public Date getFromDate() {
            Date result = null;
            if (nowOption.isSelected()) {
                result = new Date();
            } else if (fromOption.isSelected()) {
                result = fromDate.getDate();
            }
            return result;
        }

        /**
         * Invoked when the 'OK' button is pressed. The dialog may only be closed if a valid from-date has been
         * selected.
         */
        @Override
        protected void onOK() {
            if (isValid()) {
                super.onOK();
            }
        }

        /**
         * Lays out the component prior to display.
         */
        @Override
        protected void doLayout() {
            Column inner = ColumnFactory.create(
                    Styles.CELL_SPACING,
                    LabelFactory.text(message, Styles.BOLD, true),
                    nowOption,
                    RowFactory.create(Styles.CELL_SPACING, fromOption, BoundDateFieldFactory.create(fromDate)));
            Column outer = ColumnFactory.create(Styles.WIDE_CELL_SPACING, inner, LabelFactory.text(footnote, true));
            getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, outer));
        }

        /**
         * Determines if the from-date is valid.
         * <p/>
         * The from-date is valid if {@link #getFromDate()} returns a date that is {@code >=} than the original prices
         * from-date.
         * <br/>
         * This is to ensure that when the original price is closed, it doesn't end up with a from-date {@code >}
         * to-date.
         *
         * @return {@code true} if the from-date is valid
         */
        private boolean isValid() {
            boolean valid = false;
            Date date = getFromDate();
            if (date != null) {
                if (minimum == null || DateRules.compareTo(date, minimum) >= 0) {
                    valid = true;
                } else {
                    ErrorDialog.show(Messages.get("product.price.start.error"));
                }
            }
            return valid;
        }
    }
}
