/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.query;

import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.EntityIdentity;
import org.openvpms.component.system.common.query.ArchetypeQueryException;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.util.IMObjectHelper;


/**
 * Query implementation that queries {@link Entity} instances on short name,
 * instance name, and active/inactive status.
 *
 * @author Tim Anderson
 */
public class EntityQuery<T extends Entity> extends QueryAdapter<ObjectSet, T> {

    /**
     * The context.
     */
    private final Context context;


    /**
     * Constructs an {@link EntityQuery} that queries entities with the specified short names.
     *
     * @param shortNames the short names
     * @param context    the context
     * @throws ArchetypeQueryException if the short names don't match any archetypes
     */
    public EntityQuery(String[] shortNames, Context context) {
        this(new EntityObjectSetQuery(shortNames), context);
    }

    /**
     * Constructs an {@link EntityQuery} that delegates to the supplied query.
     *
     * @param query   the query
     * @param context the context
     */
    public EntityQuery(EntityObjectSetQuery query, Context context) {
        super(query, IMObjectHelper.getType(query.getShortNames()));
        this.context = context;
        // verify that the specified type matches what the query actually returns
        if (!Entity.class.isAssignableFrom(getType())) {
            throw new QueryException(QueryException.ErrorCode.InvalidType, Entity.class, getType());
        }
    }

    /**
     * Returns the selected archetype short name.
     *
     * @return the archetype short name. May be {@code null}
     */
    public String getShortName() {
        return getQuery().getShortName();
    }

    /**
     * Determines if objects must be active.
     *
     * @param active if {@code true} only query active objects, otherwise query both active and inactive objects
     */
    public void setActiveOnly(boolean active) {
        getQuery().setActiveOnly(active);
    }

    /**
     * Determines if the query selects a particular object.
     *
     * @param object the object to check
     * @return {@code true} if the object is selected by the query
     */
    @Override
    public boolean selects(T object) {
        return getQuery().selects(object);
    }

    /**
     * Returns the underlying query.
     *
     * @return the underlying query
     */
    @Override
    public EntityObjectSetQuery getQuery() {
        return (EntityObjectSetQuery) super.getQuery();
    }

    /**
     * Determines if the query should be an identity search or name search.
     * If an identity search, the name is used to search for entities
     * with a matching {@link EntityIdentity}.
     *
     * @param searchIdentities if {@code true}, query should be an identity search
     */
    public void setIdentitySearch(boolean searchIdentities) {
        getQuery().setIdentitySearch(searchIdentities);
    }

    /**
     * Determines if the query should be an identity search or name search.
     * If an identity search, the name is used to search for entities
     * with a matching {@link EntityIdentity}.
     *
     * @return {@code true} if the query should be an identity search
     */
    public boolean isIdentitySearch() {
        return getQuery().isIdentitySearch();
    }

    /**
     * Determines if both names and identities should be searched.
     *
     * @param searchAll if {@code true}, search both names and identities when {@link #isIdentitySearch()} is set
     */
    public void setSearchAll(boolean searchAll) {
        getQuery().setSearchAll(searchAll);
    }

    /**
     * Determines if both names and identities should be searched.
     *
     * @return {@code true} if both names and identities should be searched when {@link #isIdentitySearch()} is set
     */
    public boolean isSearchAll() {
        return getQuery().isSearchAll();
    }
    /**
     * Converts a result set.
     *
     * @param set the set to convert
     * @return the converted set
     */
    @Override
    protected ResultSet<T> convert(ResultSet<ObjectSet> set) {
        return new EntityResultSetAdapter<>((EntityObjectSetResultSet) set, context);
    }

}
