/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import nextapp.echo2.app.Component;
import org.openvpms.web.component.im.view.Hint;


/**
 * Component factory to bind components to {@link Property} instances.
 *
 * @author Tim Anderson
 */
public interface PropertyComponentFactory {

    /**
     * Creates a new component bound to the specified property.
     *
     * @param property the property to bind
     * @return a new component, or {@code null} if the property type isn't supported
     */
    Component create(Property property);

    /**
     * Creates a new component bound to the specified property.
     *
     * @param property the property to bind
     * @param hint     a rendering hint to determine the layout of the component. May be {@code null}
     * @return a new component, or {@code null} if the property type isn't supported
     */
    Component create(Property property, Hint hint);

}
