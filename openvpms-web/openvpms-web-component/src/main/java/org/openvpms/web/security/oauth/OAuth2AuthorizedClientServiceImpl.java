/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of {@link OAuth2AuthorizedClientService} that persists {@link OAuth2AuthorizedClient}s using
 * {@code entity.oauth2AuthorizedClient}.
 *
 * @author Tim Anderson
 */
public class OAuth2AuthorizedClientServiceImpl implements OAuth2AuthorizedClientService {

    /**
     * The client registration repository.
     */
    private final ClientRegistrationRepository repository;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The password encryptor, used to encrypt/decrypt the client secret.
     */
    private final PasswordEncryptor encryptor;

    /**
     * Cache of {@code entity.oauth2AuthorizedClient} and their corresponding {@link OAuth2AuthorizedClient}.
     */
    private final Map<Entity, OAuth2AuthorizedClient> cache = Collections.synchronizedMap(new HashMap<>());

    /**
     * The OAuth2 authorized client entity archetype name.
     */
    private static final String OAUTH2_AUTHORIZED_CLIENT = "entity.oauth2AuthorizedClient";

    /**
     * Client registration id node name.
     */
    private static final String CLIENT_REGISTRATION_ID = "clientRegistrationId";

    /**
     * Principal node name.
     */
    private static final String PRINCIPAL_NAME = "name";

    /**
     * Access token node name.
     */
    private static final String ACCESS_TOKEN = "accessToken";

    /**
     * Access token issued at node name.
     */
    private static final String ACCESS_TOKEN_ISSUED_AT = "accessTokenIssuedAt";

    /**
     * Access token expires at node name.
     */
    private static final String ACCESS_TOKEN_EXPIRES_AT = "accessTokenExpiresAt";

    /**
     * Access token scopes node name.
     */
    private static final String ACCESS_TOKEN_SCOPES = "accessTokenScopes";

    /**
     * Refresh token node name.
     */
    private static final String REFRESH_TOKEN = "refreshToken";

    /**
     * Refresh token issued at node name.
     */
    private static final String REFRESH_TOKEN_ISSUED_AT = "refreshTokenIssuedAt";

    /**
     * Constructs an {@link OAuth2AuthorizedClientServiceImpl}.
     *
     * @param repository the client registration repository
     * @param service    the archetype service
     * @param encryptor  the password encryptor
     */
    public OAuth2AuthorizedClientServiceImpl(ClientRegistrationRepository repository, ArchetypeService service,
                                             PasswordEncryptor encryptor) {
        this.repository = repository;
        this.service = service;
        this.encryptor = encryptor;
    }

    /**
     * Returns the {@link OAuth2AuthorizedClient} associated to the provided client registration identifier and
     * End-User's {@code Principal} name or {@code null} if not available.
     *
     * @param clientRegistrationId the identifier for the client's registration
     * @param principalName        the name of the End-User {@code Principal} (Resource Owner)
     * @return the {@link OAuth2AuthorizedClient} or {@code null} if not available
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends OAuth2AuthorizedClient> T loadAuthorizedClient(String clientRegistrationId, String principalName) {
        OAuth2AuthorizedClient result;
        synchronized (cache) {
            result = cache.values().stream()
                    .filter(client -> clientRegistrationId.equals(client.getClientRegistration().getClientId())
                                      && principalName.equals(client.getPrincipalName()))
                    .findFirst()
                    .orElse(null);
        }
        if (result == null) {
            Entity entity = query(clientRegistrationId, principalName);
            if (entity != null) {
                result = map(entity);
                cache.put(entity, result);
            }
        }
        return (T) result;
    }

    /**
     * Saves the {@link OAuth2AuthorizedClient} associating it to the provided End-User {@link Authentication}
     * (Resource Owner).
     *
     * @param authorizedClient the authorized client
     * @param principal        the End-User {@link Authentication} (Resource Owner)
     */
    @Override
    public void saveAuthorizedClient(OAuth2AuthorizedClient authorizedClient, Authentication principal) {
        ClientRegistration clientRegistration = authorizedClient.getClientRegistration();
        String clientRegistrationId = clientRegistration.getRegistrationId();
        String principalName = principal.getName();
        Entity entity = query(clientRegistrationId, principalName);
        if (entity == null) {
            entity = service.create(OAUTH2_AUTHORIZED_CLIENT, Entity.class);
        }
        IMObjectBean bean = service.getBean(entity);
        if (entity.isNew()) {
            bean.setValue(CLIENT_REGISTRATION_ID, clientRegistrationId);
            bean.setValue(PRINCIPAL_NAME, principalName);
        }
        OAuth2AccessToken accessToken = authorizedClient.getAccessToken();
        OAuth2RefreshToken refreshToken = authorizedClient.getRefreshToken();

        bean.setValue(ACCESS_TOKEN, encryptor.encrypt(accessToken.getTokenValue()));
        bean.setValue(ACCESS_TOKEN_ISSUED_AT, toDate(accessToken.getIssuedAt()));
        bean.setValue(ACCESS_TOKEN_EXPIRES_AT, toDate(accessToken.getExpiresAt()));
        String accessTokenScopes = null;
        if (!CollectionUtils.isEmpty(accessToken.getScopes())) {
            accessTokenScopes = StringUtils.collectionToDelimitedString(accessToken.getScopes(), ",");
        }
        bean.setValue(ACCESS_TOKEN_SCOPES, accessTokenScopes);
        if (refreshToken != null) {
            bean.setValue(REFRESH_TOKEN, encryptor.encrypt(refreshToken.getTokenValue()));
            bean.setValue(REFRESH_TOKEN_ISSUED_AT, toDate(refreshToken.getIssuedAt()));
        }
        bean.save();
        cache.put(entity, authorizedClient);
    }

    /**
     * Removes the {@link OAuth2AuthorizedClient} associated to the provided client
     * registration identifier and End-User's {@code Principal} name.
     *
     * @param clientRegistrationId the identifier for the client's registration
     * @param principalName        the name of the End-User {@code Principal} (Resource Owner)
     */
    @Override
    public void removeAuthorizedClient(String clientRegistrationId, String principalName) {
        Entity entity;
        synchronized (cache) {
            entity = cache.entrySet().stream()
                    .filter(entry -> clientRegistrationId.equals(entry.getValue().getClientRegistration().getClientId())
                                     && principalName.equals(entry.getValue().getPrincipalName()))
                    .map(Map.Entry::getKey)
                    .findFirst()
                    .orElse(null);
            if (entity != null) {
                cache.remove(entity);
            }
        }
        if (entity == null) {
            entity = query(clientRegistrationId, principalName);
        }
        if (entity != null) {
            // delete by reference to ensure not attempting to delete a stale copy
            service.remove(entity.getObjectReference());
        }
    }

    /**
     * Maps an {@code entity.oauth2AuthorizedClient} to an {@link OAuth2AuthorizedClient}.
     *
     * @param entity the entity to map
     * @return the corresponding client
     */
    private OAuth2AuthorizedClient map(Entity entity) {
        IMObjectBean bean = service.getBean(entity);
        String clientRegistrationId = bean.getString(CLIENT_REGISTRATION_ID);
        ClientRegistration clientRegistration = repository.findByRegistrationId(clientRegistrationId);
        if (clientRegistration == null) {
            throw new DataRetrievalFailureException("The ClientRegistration with id '" + clientRegistrationId
                                                    + "' exists in the data source, "
                                                    + "however, it was not found in the ClientRegistrationRepository.");
        }
        String tokenValue = encryptor.decrypt(bean.getString(ACCESS_TOKEN));
        Instant issuedAt = bean.getDate(ACCESS_TOKEN_ISSUED_AT).toInstant();
        Instant expiresAt = bean.getDate(ACCESS_TOKEN_EXPIRES_AT).toInstant();
        Set<String> scopes = Collections.emptySet();
        String accessTokenScopes = bean.getString(ACCESS_TOKEN_SCOPES);
        if (accessTokenScopes != null) {
            scopes = StringUtils.commaDelimitedListToSet(accessTokenScopes);
        }
        OAuth2AccessToken accessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, tokenValue, issuedAt,
                                                              expiresAt, scopes);
        OAuth2RefreshToken refreshToken = null;
        String refreshTokenValue = bean.getString(REFRESH_TOKEN);
        if (refreshTokenValue != null) {
            issuedAt = null;
            Date refreshTokenIssuedAt = bean.getDate(REFRESH_TOKEN_ISSUED_AT);
            if (refreshTokenIssuedAt != null) {
                issuedAt = refreshTokenIssuedAt.toInstant();
            }
            refreshToken = new OAuth2RefreshToken(encryptor.decrypt(refreshTokenValue), issuedAt);
        }
        return new OAuth2AuthorizedClient(clientRegistration, entity.getName(), accessToken, refreshToken);
    }

    /**
     * Converts an instant to a date.
     *
     * @param instant the instant. May be {@code null}
     * @return the corresponding date. May be {@code null}
     */
    private Date toDate(Instant instant) {
        return instant != null ? Date.from(instant) : null;
    }

    /**
     * Queries an {@code entity.oauth2AuthorizedClient} given the client registration id and principal name.
     *
     * @param clientRegistrationId the client registration id
     * @param principalName        the principal name
     * @return the corresponding entity, or {@code null} if none is found
     */
    private Entity query(String clientRegistrationId, String principalName) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> from = query.from(Entity.class, OAUTH2_AUTHORIZED_CLIENT);
        query.where(builder.equal(from.get(PRINCIPAL_NAME), principalName),
                    builder.equal(from.get(CLIENT_REGISTRATION_ID), clientRegistrationId));
        query.orderBy(builder.asc(from.get("id")));
        return service.createQuery(query).getFirstResult();
    }
}
