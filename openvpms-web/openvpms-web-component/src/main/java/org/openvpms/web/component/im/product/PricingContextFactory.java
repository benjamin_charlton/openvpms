package org.openvpms.web.component.im.product;

import org.openvpms.archetype.rules.finance.tax.CustomerTaxRules;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.PricingGroup;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.product.ServiceRatioService;
import org.openvpms.component.business.domain.im.party.Party;

/**
 * Factory for {@link PricingContext}s.
 *
 * @author Tim Anderson
 */
public class PricingContextFactory {

    /**
     * The practice rules.
     */
    private final PracticeRules practiceRules;

    /**
     * The location rules.
     */
    private final LocationRules locationRules;

    /**
     * The product price rules.
     */
    private final ProductPriceRules priceRules;

    /**
     * The service ratio service.
     */
    private final ServiceRatioService serviceRatioService;

    /**
     * Constructs a {@link PricingContextFactory}.
     *
     * @param practiceRules       the practice rules
     * @param locationRules       the location rules
     * @param priceRules          the product price rules
     * @param serviceRatioService the service ratio service
     */
    public PricingContextFactory(PracticeRules practiceRules, LocationRules locationRules, ProductPriceRules priceRules,
                                 ServiceRatioService serviceRatioService) {
        this.practiceRules = practiceRules;
        this.locationRules = locationRules;
        this.priceRules = priceRules;
        this.serviceRatioService = serviceRatioService;
    }

    /**
     * Creates a {@link ProductPricingContext}.
     *
     * @param practice the practice
     * @param location the practice location
     * @return a new pricing context
     */
    public ProductPricingContext createProductPricingContext(Party practice, Party location) {
        Currency currency = getCurrency(practice);
        return new ProductPricingContext(currency, practice, location, priceRules, locationRules, serviceRatioService);
    }

    /**
     * Creates a {@link ProductPricingContext}.
     *
     * @param pricingGroup the pricing group
     * @param practice     the practice
     * @param location     the practice location
     * @return a new pricing context
     */
    public ProductPricingContext createProductPricingContext(PricingGroup pricingGroup, Party practice,
                                                             Party location) {
        Currency currency = getCurrency(practice);
        return new ProductPricingContext(currency, pricingGroup, practice, location, priceRules, serviceRatioService);
    }

    /**
     * Creates a new {@link CustomerPricingContext}.
     *
     * @param customer   the customer
     * @param practice   the practice
     * @param location   the practice location
     * @param priceRules the product price rules
     * @param taxRules   the tax rules
     * @return a new pricing context
     */
    public CustomerPricingContext createCustomerPricingContext(Party customer, Party practice, Party location,
                                                               ProductPriceRules priceRules,
                                                               CustomerTaxRules taxRules) {
        Currency currency = getCurrency(practice);
        return new CustomerPricingContext(customer, location, currency, priceRules, locationRules, taxRules,
                                          serviceRatioService);
    }

    /**
     * Returns the currency.
     *
     * @param practice the practice
     * @return the currency
     */
    private Currency getCurrency(Party practice) {
        return practiceRules.getCurrency(practice);
    }
}
