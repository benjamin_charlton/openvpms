/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.till;

import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.Participation;
import org.openvpms.web.component.im.edit.IMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;


/**
 * Participation editor for tills. This updates the context when a till is selected.
 *
 * @author Tim Anderson
 */
public class TillParticipationEditor extends ParticipationEditor<Entity> {

    /**
     * Constructs a {@link TillParticipationEditor}.
     *
     * @param participation the object to edit
     * @param parent        the parent object
     * @param context       the layout context. May be {@code null}
     */
    public TillParticipationEditor(Participation participation, Act parent, LayoutContext context) {
        super(participation, parent, context);
        if (!participation.isA(TillArchetypes.TILL_PARTICIPATION)) {
            throw new IllegalArgumentException("Invalid participation type: " + participation.getArchetype());
        }
        if (participation.isNew() && participation.getEntity() == null) {
            Entity till = getLayoutContext().getContext().getTill();
            setEntity(till);
        }
    }

    /**
     * Creates a new object reference editor.
     *
     * @param property the reference property
     * @return a new object reference editor
     */
    @Override
    protected IMObjectReferenceEditor<Entity> createEntityEditor(Property property) {
        return new TillReferenceEditor(property, getParent(), getLayoutContext()) {

            @Override
            public boolean setObject(Entity object) {
                getLayoutContext().getContext().setTill(object);
                return super.setObject(object);
            }
        };
    }
}
