/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * Print helper.
 *
 * @author Tim Anderson
 */
public class PrintHelper {

    /**
     * Returns a list of the available printers.
     *
     * @return a list of the available printers
     */
    public static List<PrinterReference> getPrinters() {
        List<PrinterReference> result = new ArrayList<>();
        ProtectedPrinterServiceLocator locator
                = new ProtectedPrinterServiceLocator(ServiceHelper.getBean(DocumentPrinterServiceLocator.class));
        for (DocumentPrinterService service : locator.getServices()) {
            try {
                String serviceName = service.getName();
                String archetype = service.getArchetype();
                for (DocumentPrinter printer : service.getPrinters()) {
                    try {
                        result.add(new PrinterReference(archetype, serviceName, printer.getId(), printer.getName()));
                    } catch (Throwable ignore) {
                        // no-op
                    }
                }
            } catch (Throwable ignore) {
                // no-op
            }
        }
        return result;
    }

    /**
     * Returns the names of the Java Print Service printers.
     *
     * @return the printer names
     */
    public static List<String> getJavaPrintServiceNames() {
        List<String> result = new ArrayList<>();
        DocumentPrinterServiceLocator locator = ServiceHelper.getBean(DocumentPrinterServiceLocator.class);
        for (DocumentPrinter printer : locator.getJavaPrintService().getPrinters()) {
            result.add(printer.getId());
        }
        return result;
    }

}
