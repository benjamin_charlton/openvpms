/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.edit.AbstractSelectorPropertyEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.InMemoryQuery;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.select.AbstractQuerySelector;
import org.openvpms.web.component.im.select.IMObjectSelector;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Collection;
import java.util.List;

/**
 * An editor for lookup properties that provides an {@link IMObjectSelector} to display and query lookups.
 *
 * @author Tim Anderson
 */
public class SelectorLookupPropertyEditor extends AbstractSelectorPropertyEditor<Lookup>
        implements LookupPropertyEditor {

    /**
     * The archetype to query.
     */
    private final String archetype;

    /**
     * The parent object.
     */
    private final IMObject parent;


    /**
     * Constructs a {@link SelectorLookupPropertyEditor}.
     *
     * @param archetype the lookup archetype to query
     * @param property  the property being edited
     * @param parent    the parent object
     * @param context   the layout context
     */
    public SelectorLookupPropertyEditor(String archetype, Property property, IMObject parent, LayoutContext context) {
        super(property, context);
        this.archetype = archetype;
        this.parent = parent;
        updateSelector();
    }

    /**
     * Returns the object corresponding to the property.
     *
     * @return the object. May be {@code null}
     */
    @Override
    public Lookup getObject() {
        Property property = getProperty();
        if (property.isCollection()) {
            List<?> values = ((CollectionProperty) property).getValues();
            return (!values.isEmpty()) ? (Lookup) values.get(0) : null;
        }
        return (Lookup) ServiceHelper.getLookupService().getLookup(parent, property.getName());
    }

    /**
     * Refreshes the set of available lookups.
     */
    @Override
    public void refresh() {
        // no-op
    }

    /**
     * Creates a new selector.
     *
     * @param property    the property
     * @param context     the layout context
     * @param allowCreate determines if objects may be created
     * @return a new selector
     */
    @Override
    protected AbstractQuerySelector<Lookup> createSelector(Property property, LayoutContext context,
                                                           boolean allowCreate) {
        return new LookupSelector(property, allowCreate, context);
    }

    /**
     * Returns the available lookups for a property.
     *
     * @param property the property
     * @return the available lookups
     */
    @SuppressWarnings("unchecked")
    protected List<Lookup> getLookups(Property property) {
        NodeLookupQuery nodeLookupQuery = new NodeLookupQuery(parent, property);
        return (List<Lookup>) (List<?>) nodeLookupQuery.getLookups();
    }

    /**
     * Updates the underlying property with the specified value.
     *
     * @param property the property
     * @param lookup   the value to update with. May be {@code null}
     * @return {@code true} if the property was modified
     */
    @Override
    protected boolean updateProperty(Property property, Lookup lookup) {
        boolean modified = false;
        if (!property.isCollection()) {
            String value = (lookup != null) ? lookup.getCode() : null;
            modified = property.setValue(value);
        } else {
            // if it's a collection property add the selected value to the collection, replacing any existing values
            CollectionProperty collection = (CollectionProperty) property;
            Collection values = collection.getValues();
            if (lookup == null) {
                // nothing selected, so remove any existing value
                if (!values.isEmpty()) {
                    for (Object value : values) {
                        collection.remove(value);
                    }
                    modified = true;
                }
            } else {
                // replace any existing values with the selected value
                if (!values.contains(lookup)) {
                    for (Object value : values) {
                        collection.remove(value);
                    }
                    collection.add(lookup);
                    modified = true;
                }
            }
        }
        return modified;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean result = false;
        AbstractQuerySelector<Lookup> selector = getSelector();
        if (!selector.inSelect()) {
            // only raise validation errors if a dialog is not displayed
            if (!selector.isValid()) {
                Property property = getProperty();
                String message = Messages.format("property.error.invalidvalue", selector.getText(),
                                                 property.getDisplayName());
                validator.add(this, new ValidatorError(property, message));
            } else {
                result = super.doValidation(validator);
            }
        }
        return result;
    }

    /**
     * Creates a query for the specified lookups.
     *
     * @param archetype the lookup archetype
     * @param lookups   the lookups to query
     * @return a new query
     */
    protected Query<Lookup> createLookupQuery(String archetype, List<Lookup> lookups) {
        return new LookupInMemoryQuery(archetype, lookups);
    }

    /**
     * Selector for lookups.
     */
    protected class LookupSelector extends IMObjectSelector<Lookup> {
        private final Property property;

        public LookupSelector(Property property, boolean allowCreate, LayoutContext context) {
            super(property, allowCreate, context);
            this.property = property;
        }

        /**
         * Invoked when the text field is updated.
         * <p/>
         * This implementation resets the validity flag to force-revalidation.
         */
        @Override
        protected void onTextChanged() {
            resetValid();
            super.onTextChanged();
        }

        /**
         * Creates a query to select objects.
         *
         * @param value a value to filter on. May be {@code null}
         * @return a new query
         */
        @Override
        protected Query<Lookup> createQuery(String value) {
            List<Lookup> lookups = getLookups(property);
            Query<Lookup> query = createLookupQuery(archetype, lookups);
            query.setValue(value);
            return query;
        }
    }

    /**
     * An {@link InMemoryQuery} for lookups.
     */
    protected static class LookupInMemoryQuery extends InMemoryQuery<Lookup> {

        /**
         * Constructs a {@link LookupInMemoryQuery}.
         *
         * @param archetype the lookup archetype
         * @param lookups   the lookups to query
         */
        public LookupInMemoryQuery(String archetype, List<Lookup> lookups) {
            super(archetype, Lookup.class, lookups);
            setAuto(true);
            setDefaultSortConstraint(new SortConstraint[]{new NodeSortConstraint("name"),
                                                          new NodeSortConstraint("id")});
        }

        /**
         * Determines if an object matches the query value.
         * <p/>
         * This implementation returns {@code true} if no value is specified, or the object name contains with the value.
         *
         * @param object the object
         * @param value  the value. May be {@code null}
         * @return {@code true} if the object matches, otherwise {@code false}
         */
        @Override
        protected boolean matches(Lookup object, String value) {
            return value == null || contains(object.getName(), value);
        }
    }
}
