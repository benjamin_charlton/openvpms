/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.web.component.edit.AlertListener;
import org.openvpms.web.component.edit.Cancellable;
import org.openvpms.web.component.edit.Deletable;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.edit.Saveable;
import org.openvpms.web.component.i18n.WebComponentMessages;
import org.openvpms.web.component.im.audit.AuditInfo;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.ExpandableLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategyFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.LookupPropertyEditor;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.util.IMObjectNames;
import org.openvpms.web.component.im.view.AbstractIMObjectView;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.im.view.IMObjectView;
import org.openvpms.web.component.im.view.Selection;
import org.openvpms.web.component.im.view.layout.EditLayoutStrategyFactory;
import org.openvpms.web.component.im.view.layout.ViewLayoutStrategyFactory;
import org.openvpms.web.component.property.AbstractModifiable;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.ErrorListener;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.ModifiableListeners;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetBuilder;
import org.openvpms.web.component.property.PropertyTransformer;
import org.openvpms.web.component.property.StringPropertyTransformer;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.focus.FocusHelper;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Abstract implementation of the {@link IMObjectEditor} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectEditor extends AbstractModifiable
        implements IMObjectEditor {

    /**
     * The object being edited.
     */
    private final IMObject object;

    /**
     * The parent object. May be {@code null}.
     */
    private final IMObject parent;

    /**
     * The object's descriptor.
     */
    private final ArchetypeDescriptor archetype;

    /**
     * The editors.
     */
    private final Editors editors;

    /**
     * Tracks the editors created via the factory. By default, these are disposed if the layout changes.
     */
    private final Set<Editor> createdEditors = new HashSet<>();

    /**
     * The object properties.
     */
    private final PropertySet properties;

    /**
     * Lookup editors. These may need to be refreshed.
     */
    private final Map<LookupPropertyEditor, ModifiableListener> lookups = new HashMap<>();

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The listeners.
     */
    private final ModifiableListeners listeners = new ModifiableListeners();

    /**
     * The object viewer.
     */
    private IMObjectView viewer;

    /**
     * Action listener for layout changes.
     */
    private ActionListener layoutChangeListener;

    /**
     * Indicates if the object has been saved.
     */
    private boolean saved = false;

    /**
     * Indicates if the object was deleted.
     */
    private boolean deleted = false;

    /**
     * Indicates if editing was cancelled.
     */
    private boolean cancelled = false;

    /**
     * Property change listener notifier.
     */
    private PropertyChangeSupport propertyChangeNotifier;

    /**
     * The archetype service.
     */
    private IArchetypeService service;

    /**
     * Determines if singleton validation should be performed.
     */
    private boolean checkSingleton = true;


    /**
     * Constructs an {@link AbstractIMObjectEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AbstractIMObjectEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        this.object = object;
        this.parent = parent;

        context = new DefaultLayoutContext(layoutContext);
        // don't increase the layout depth.
        context.setLayoutDepth(layoutContext.getLayoutDepth());

        archetype = context.getArchetypeDescriptor(object);
        properties = createPropertySet(object, archetype, context.getVariables());
        editors = new Editors(properties, listeners);

        IMObjectLayoutStrategyFactory strategyFactory = context.getLayoutStrategyFactory();
        if (strategyFactory == null || strategyFactory instanceof ViewLayoutStrategyFactory) {
            context.setLayoutStrategyFactory(new EditLayoutStrategyFactory(getService()));
        }

        IMObjectComponentFactory factory = new DefaultEditableComponentFactory(context, this::registerEditor);
        context.setComponentFactory(factory);

        editors.addModifiableListener(this::onModified);
    }

    /**
     * Disposes of the editor.
     * <br/>
     * Once disposed, the behaviour of invoking any method is undefined.
     */
    public void dispose() {
        editors.dispose();
        disposeLookups(true);
        listeners.removeAll();

        if (viewer != null && viewer.hasComponent()) {
            Component component = viewer.getComponent();
            Component parent = component.getParent();
            if (parent != null) {
                parent.remove(component);
            }
        }
    }

    /**
     * Returns a title for the editor.
     *
     * @return a title for the editor
     */
    public String getTitle() {
        String title;
        if (object.isNew()) {
            title = Messages.format("editor.new.title", getDisplayName());
        } else {
            title = Messages.format("editor.edit.title", getDisplayName());
        }
        return title;
    }

    /**
     * Returns a display name for the object being edited.
     *
     * @return a display name for the object
     */
    public String getDisplayName() {
        return getArchetypeDescriptor().getDisplayName();
    }

    /**
     * Returns the object being edited.
     *
     * @return the object being edited
     */
    public IMObject getObject() {
        return object;
    }

    /**
     * Returns the parent object.
     *
     * @return the parent object. May be {@code null}
     */
    public IMObject getParent() {
        return parent;
    }

    /**
     * Returns the archetype descriptor of the object.
     *
     * @return the object's archetype descriptor
     */
    public ArchetypeDescriptor getArchetypeDescriptor() {
        return archetype;
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    public void save() {
        if (cancelled) {
            throw new IllegalStateException("Editor has been cancelled");
        }
        boolean isNew = object.isNew();
        if (isNew || isModified()) {
            doSave();
            saved = true;
            clearModified();
        }
    }

    /**
     * Invoked after changes have been committed.
     * <p/>
     * This can be used to refresh fields populated on commit.
     */
    @Override
    public void committed() {
        refreshAuditInfo();
    }

    /**
     * Determines if any edits have been saved.
     *
     * @return {@code true} if edits have been saved.
     */
    public boolean isSaved() {
        return saved;
    }

    /**
     * Perform deletion.
     *
     * @throws OpenVPMSException if the delete fails
     */
    public void delete() {
        if (cancelled) {
            throw new IllegalStateException("Editor has been cancelled");
        }
        doDelete();
        deleted = true;
    }

    /**
     * Determines if the object has been deleted.
     *
     * @return {@code true} if the object has been deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * Determines if the object has been changed.
     *
     * @return {@code true} if the object has been changed
     */
    public boolean isModified() {
        return editors.isModified();
    }

    /**
     * Clears the modified status of the object.
     */
    public void clearModified() {
        editors.clearModified();
    }

    /**
     * Adds a listener to be notified when this changes.
     *
     * @param listener the listener to add
     */
    public void addModifiableListener(ModifiableListener listener) {
        editors.addModifiableListener(listener);
    }

    /**
     * Adds a listener to be notified when this changes, specifying the order of the listener.
     *
     * @param listener the listener to add
     * @param index    the index to add the listener at. The 0-index listener is notified first
     */
    public void addModifiableListener(ModifiableListener listener, int index) {
        editors.addModifiableListener(listener, index);
    }

    /**
     * Removes a listener.
     *
     * @param listener the listener to remove
     */
    public void removeModifiableListener(ModifiableListener listener) {
        editors.removeModifiableListener(listener);
    }

    /**
     * Sets a listener to be notified of errors.
     *
     * @param listener the listener to register. May be {@code null}
     */
    @Override
    public void setErrorListener(ErrorListener listener) {
        editors.setErrorListener(listener);
    }

    /**
     * Returns the listener to be notified of errors.
     *
     * @return the listener. May be {@code null}
     */
    @Override
    public ErrorListener getErrorListener() {
        return editors.getErrorListener();
    }

    /**
     * Registers a listener to be notified of alerts.
     *
     * @param listener the listener. May be {@code null}
     */
    @Override
    public void setAlertListener(AlertListener listener) {
        editors.setAlertListener(listener);
    }

    /**
     * Returns the listener to be notified of alerts.
     *
     * @return the listener. May be {@code null}
     */
    @Override
    public AlertListener getAlertListener() {
        return editors.getAlertListener();
    }

    /**
     * Cancel any edits. Once complete, query methods may be invoked, but the behaviour of other methods is undefined.
     */
    public void cancel() {
        try {
            cancelled = true;
            for (Cancellable cancellable : editors.getCancellable()) {
                cancellable.cancel();
            }
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Determines if editing was cancelled.
     *
     * @return {@code true} if editing was cancelled
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     * Sets the selection path.
     *
     * @param path the path
     */
    @Override
    public void setSelectionPath(List<Selection> path) {
        getView().setSelectionPath(path);
    }

    /**
     * Returns the selection path.
     *
     * @return the selection path
     */
    @Override
    public List<Selection> getSelectionPath() {
        return getView().getSelectionPath();
    }

    /**
     * Returns the rendered object.
     *
     * @return the rendered object
     */
    public Component getComponent() {
        return getView().getComponent();
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group, or {@code null} if the editor hasn't been
     * rendered
     */
    public FocusGroup getFocusGroup() {
        return getView().getFocusGroup();
    }

    /**
     * Add a property change listener.
     *
     * @param name     the property name to listen on
     * @param listener the listener
     */
    public void addPropertyChangeListener(String name,
                                          PropertyChangeListener listener) {
        if (propertyChangeNotifier == null) {
            propertyChangeNotifier = new PropertyChangeSupport(this);
        }
        propertyChangeNotifier.addPropertyChangeListener(name, listener);
    }

    /**
     * Remove a property change listener.
     *
     * @param name     the property name to remove the listener for
     * @param listener the listener to remove
     */
    public void removePropertyChangeListener(String name,
                                             PropertyChangeListener listener) {
        if (propertyChangeNotifier != null) {
            propertyChangeNotifier.removePropertyChangeListener(
                    name, listener);
        }
    }

    /**
     * Returns the help context for the editor.
     *
     * @return the help context
     */
    public HelpContext getHelpContext() {
        return context.getHelpContext();
    }

    /**
     * Returns a property, given its node descriptor's name.
     *
     * @param name the descriptor's name
     * @return the property corresponding to {@code name} or {@code null} if none exists
     */
    public Property getProperty(String name) {
        return properties.get(name);
    }

    /**
     * Returns a collection property, given its node descriptor's name.
     *
     * @param name the descriptor's name
     * @return the property corresponding to {@code name} or {@code null} if none exists
     */
    public CollectionProperty getCollectionProperty(String name) {
        return (CollectionProperty) properties.get(name);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return null;
    }

    /**
     * Creates the property set.
     *
     * @param object    the object being edited
     * @param archetype the object archetype
     * @param variables the variables for macro expansion. May be {@code null}
     * @return the property set
     */
    protected PropertySet createPropertySet(IMObject object, ArchetypeDescriptor archetype, Variables variables) {
        return new PropertySetBuilder(object, archetype, variables).build();
    }

    /**
     * Resets the cached validity state of the object.
     *
     * @param descendants if {@code true} reset the validity state of any descendants as well
     */
    protected void resetValid(boolean descendants) {
        super.resetValid(descendants);
        if (descendants) {
            editors.resetValid();
        }
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    protected boolean doValidation(Validator validator) {
        boolean result;
        result = validator.validate(editors) && validateSingleton(validator);
        if (result) {
            // run it by the archetype service to pick up validation issues
            // not detected by the app e.g expression assertions
            List<ValidatorError> errors = ValidationHelper.validate(object, getService());
            if (errors != null) {
                validator.add(this, errors);
                result = false;
            }
        }
        return result;
    }

    /**
     * Save any edits.
     * <p>
     * This uses {@link #saveChildren()} to save the children prior to invoking {@link #saveObject()}.
     *
     * @throws OpenVPMSException if the save fails
     */
    protected void doSave() {
        if (activeSingletonExists()) {
            throw new EditException(WebComponentMessages.activeSingletonExists(archetype.getDisplayName()));
        }
        saveChildren();
        saveObject();
    }

    /**
     * Saves the object.
     *
     * @throws OpenVPMSException if the save fails
     */
    protected void saveObject() {
        IMObject object = getObject();
        getService().save(object);
    }

    /**
     * Save any modified child Saveable instances.
     *
     * @throws OpenVPMSException if the save fails
     */
    protected void saveChildren() {
        for (Saveable saveable : editors.getModifiedSaveable()) {
            saveable.save();
        }
    }

    /**
     * Deletes the object.
     * <p>
     * This uses {@link #deleteChildren()} to delete the children prior to invoking {@link #deleteObject()}.
     *
     * @throws OpenVPMSException if the delete fails
     */
    protected void doDelete() {
        deleteChildren();
        deleteObject();
    }

    /**
     * Deletes the object.
     *
     * @throws OpenVPMSException if the delete fails
     */
    protected void deleteObject() {
        IMObject object = getObject();
        if (!object.isNew()) {
            getService().remove(object);
        }
    }

    /**
     * Deletes any child Deletable instances.
     *
     * @throws OpenVPMSException if the delete fails
     */
    protected void deleteChildren() {
        for (Deletable deletable : editors.getDeletable()) {
            deletable.delete();
        }
    }

    /**
     * Returns the properties.
     *
     * @return the properties
     */
    protected PropertySet getProperties() {
        return properties;
    }

    /**
     * Adds an editor.
     * <br/>
     * Any editors added via this will not automatically be disposed of if the layout changes.
     *
     * @param editor the editor to add
     */
    protected void addEditor(Editor editor) {
        if (editor instanceof LookupPropertyEditor) {
            // need to track lookup editor separately, as these can be refreshed
            LookupPropertyEditor lookup = (LookupPropertyEditor) editor;
            ModifiableListener listener = modifiable -> refreshLookups(lookup);
            lookup.getProperty().addModifiableListener(listener);
            lookups.put((LookupPropertyEditor) editor, listener);
        }
        editors.add(editor);
    }

    /**
     * Returns the child editors.
     *
     * @return the child editors
     */
    protected Editors getEditors() {
        return editors;
    }

    /**
     * Report a bound property update to any registered listeners. No event is
     * fired if old and new are equal and non-null.
     *
     * @param name     the name of the property that was changed
     * @param oldValue the old value of the property
     * @param newValue the new value of the property
     */
    protected void firePropertyChange(String name, Object oldValue, Object newValue) {
        if (propertyChangeNotifier != null) {
            propertyChangeNotifier.firePropertyChange(name, oldValue, newValue);
        }
    }

    /**
     * Returns the view, creating it if it doesn't exist.
     *
     * @return the view
     */
    protected IMObjectView getView() {
        if (viewer == null) {
            viewer = createView(object);
        }
        return viewer;
    }

    /**
     * Creates an {@link IMObjectView} to render the object.
     *
     * @param object the object to view
     * @return a new object view
     */
    protected IMObjectView createView(IMObject object) {
        IMObjectLayoutStrategy layout = createLayoutStrategy();
        IMObjectView view;
        view = new AbstractIMObjectView(object, properties, parent, layout) {
            @Override
            protected Component createComponent() {
                disposeLookups(false);
                return super.createComponent();
            }

            protected LayoutContext getLayoutContext() {
                return context;
            }
        };
        view.setLayoutListener(new ActionListener() {
            public void onAction(ActionEvent e) {
                onLayoutCompleted();
            }
        });
        if (layout instanceof ExpandableLayoutStrategy) {
            view.getComponent(); // make sure the component is rendered.
            ExpandableLayoutStrategy exp = (ExpandableLayoutStrategy) layout;
            Button button = exp.getButton();
            if (button != null) {
                button.addActionListener(getLayoutChangeListener());
            }
        }
        return view;
    }

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getLayoutContext() {
        return context;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategyFactory layoutStrategy = context.getLayoutStrategyFactory();
        return layoutStrategy.create(getObject(), getParent());
    }

    /**
     * Change the layout.
     * <p>
     * This disposes created editors first, using {@link #disposeOnChangeLayout()}.
     */
    protected void onLayout() {
        disposeOnChangeLayout();
        if (getView().getLayout() instanceof ExpandableLayoutStrategy) {
            ExpandableLayoutStrategy expandable = (ExpandableLayoutStrategy) getView().getLayout();
            expandable.setShowOptional(!expandable.isShowOptional());
            getView().setLayout(expandable);
            Button button = expandable.getButton();
            if (button != null) {
                button.addActionListener(getLayoutChangeListener());
            }
        } else {
            IMObjectLayoutStrategy layout = createLayoutStrategy();
            getView().setLayout(layout);
            if (layout instanceof ExpandableLayoutStrategy) {
                ExpandableLayoutStrategy exp = (ExpandableLayoutStrategy) layout;
                Button button = exp.getButton();
                if (button != null) {
                    button.addActionListener(getLayoutChangeListener());
                }
            }
        }
        firePropertyChange(COMPONENT_CHANGED_PROPERTY, null, this);
    }

    /**
     * Invoked by {@link #onLayout} to dispose of existing editors.
     * <p>
     * This implementation disposes each editor for which {@link #disposeOnChangeLayout(Editor)} returns {@code true}.
     */
    protected void disposeOnChangeLayout() {
        Set<Editor> set = editors.getEditors();
        for (Editor editor : set.toArray(new Editor[0])) {
            if (disposeOnChangeLayout(editor)) {
                editors.remove(editor);
                createdEditors.remove(editor);
                editor.dispose();
            }
        }
    }

    /**
     * Determines if an editor should be disposed on layout change.
     * This implementation returns {@code true} for editors created via the editor factory.
     *
     * @param editor the editor
     * @return {@code true} if the editor should be disposed
     */
    protected boolean disposeOnChangeLayout(Editor editor) {
        return createdEditors.contains(editor);
    }

    /**
     * Invoked when layout has completed.
     * <p>
     * This can be used to perform processing that requires all editors to be created.
     */
    protected void onLayoutCompleted() {
    }

    /**
     * Invoked when any of the child editors or properties update.
     * <p>
     * This resets the cached valid state
     *
     * @param modifiable the updated object
     */
    protected void onModified(Modifiable modifiable) {
        resetValid(false);
    }

    /**
     * Refreshes lookups that may be linked to the specified source lookup.
     *
     * @param source the source lookup
     */
    protected void refreshLookups(LookupPropertyEditor source) {
        for (LookupPropertyEditor editor : lookups.keySet()) {
            if (source != editor) {
                editor.refresh();
            }
        }
        // need to reset focus, otherwise focus traversal is non-deterministic. Note that this has implications
        // for tabbing through lookup fields. It requires 2 tabs to leave a lookup field that has changed
        // instead of one. OVPMS-1397
        FocusHelper.setFocus(source.getComponent());
    }

    /**
     * Returns the layout change action listener.
     *
     * @return the layout change listener
     */
    protected ActionListener getLayoutChangeListener() {
        if (layoutChangeListener == null) {
            layoutChangeListener = new ActionListener() {
                public void onAction(ActionEvent event) {
                    onLayout();
                }
            };
        }
        return layoutChangeListener;
    }

    /**
     * Helper to return a node descriptor from the archetype, given its name.
     *
     * @param name the node descriptor's name
     * @return the corresponding node descriptor, or {@code null} if it doesn't exist
     */
    protected NodeDescriptor getDescriptor(String name) {
        return getArchetypeDescriptor().getNodeDescriptor(name);
    }

    /**
     * Helper to return an editor associated with a property, given the property
     * name.
     * <p>
     * This performs a layout of the component if it hasn't already been done, to ensure the editors are created
     *
     * @param name the property name
     * @return the editor corresponding to {@code name} or {@code null} if none exists
     */
    protected Editor getEditor(String name) {
        return getEditor(name, true);
    }

    /**
     * Helper to return an editor associated with a property, given the property
     * name.
     *
     * @param name   the property name
     * @param create if {@code true} force creation of the edit components if it hasn't already been done
     * @return the editor corresponding to {@code name} or {@code null} if none exists or hasn't been created
     */
    protected Editor getEditor(String name, boolean create) {
        if (create) {
            // make sure the component has been laid out to ensure
            // the editors are created
            getComponent();
        }
        return editors.getEditor(name);
    }

    /**
     * Returns the target of a collection property relationship.
     * <p/>
     * If the collection has multiple relationships, the first will be returned.
     * <p/>
     * NOTE: this operates on the underlying collection, and not any editor responsible for that collection;
     * if an editor has uncommitted changes, these won't be returned.
     *
     * @param name the name of the property
     * @return the target of the relationship, or {@code null} if there is none
     */
    protected IMObject getTarget(String name) {
        IMObject result = null;
        CollectionProperty property = getCollectionProperty(name);
        List<?> values = property.getValues();
        if (!values.isEmpty()) {
            Relationship relationship = (Relationship) values.get(0);
            result = getObject(relationship.getTarget());
        }
        return result;
    }

    /**
     * Returns the listeners.
     *
     * @return the listeners
     */
    protected ModifiableListeners getListeners() {
        return listeners;
    }

    /**
     * Helper to return an object given its reference.
     * <p>
     * This implementation uses the cache associated with the layout context.
     *
     * @param reference the reference. May be {@code null}
     * @return the object corresponding to {@code reference} or {@code null} if none exists
     */
    protected IMObject getObject(Reference reference) {
        return (IMObject) getLayoutContext().getCache().get(reference);
    }

    /**
     * Attempts to set focus on the component representing the supplied property.
     *
     * @param property the property
     * @return {@code true} if the focus was set, {@code false} if no component could be found
     */
    protected boolean setFocus(Property property) {
        boolean result = false;
        Editor editor = editors.getEditor(property.getName());
        if (editor != null && editor.getFocusGroup() != null) {
            editor.getFocusGroup().setFocus();
            result = true;
        }
        return result;
    }

    /**
     * Reloads an object, if it has been saved previously.
     *
     * @param object the object to reload. May be {@code null}
     * @return the reloaded object
     * @throws IllegalStateException if the object no longer exists
     */
    protected <T extends IMObject> T reload(T object) {
        return IMObjectHelper.reload(object, true);
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return a bean wrapping the object
     */
    protected IMObjectBean getBean(org.openvpms.component.model.object.IMObject object) {
        return getService().getBean(object);
    }

    /**
     * Returns the name of an object, given its reference.
     *
     * @param reference the reference. May be {@code null}
     * @return the corresponding name, or {@code null} if none is found
     */
    protected String getName(Reference reference) {
        return (reference != null) ? getLayoutContext().getNames().getName(reference) : null;
    }

    /**
     * Returns the display name for an object.
     *
     * @param object the object
     * @return the display name for the object
     */
    protected String getDisplayName(org.openvpms.component.model.object.IMObject object) {
        return getDisplayName(object.getArchetype());
    }

    /**
     * Returns the display name for an archetype.
     *
     * @param archetype the archetype
     * @return a display name for the archetype
     */
    protected String getDisplayName(String archetype) {
        String displayName = DescriptorHelper.getDisplayName(archetype, getService());
        return displayName != null ? displayName : archetype;
    }

    /**
     * Returns the display name for a node.
     *
     * @param archetype the archetype
     * @param node      the node name
     * @return a display name for the node
     */
    protected String getDisplayName(String archetype, String node) {
        String displayName = DescriptorHelper.getDisplayName(archetype, node, getService());
        return displayName != null ? displayName : node;
    }


    /**
     * Creates an object of the specified archetype.
     *
     * @param archetype the archetype
     * @param type      the object type
     * @return the new object
     */
    protected <T extends IMObject> T create(String archetype, Class<T> type) {
        return getService().create(archetype, type);
    }

    /**
     * Disables macro expansion of a node, to avoid it expanding itself.
     *
     * @param name the node name
     */
    protected void disableMacroExpansion(String name) {
        Property property = getProperty(name);
        if (property != null) {
            PropertyTransformer transformer = property.getTransformer();
            if (transformer instanceof StringPropertyTransformer) {
                ((StringPropertyTransformer) transformer).setExpandMacros(false);
            }
        }
    }

    /**
     * Checks that the object isn't a duplicate of a singleton archetype.
     *
     * @param validator the validator
     * @return {@code true} if the object isn't a duplicate of a singleton archetype, {@code false} if it is
     */
    protected boolean validateSingleton(Validator validator) {
        boolean result = true;
        if (checkSingleton) {
            if (activeSingletonExists()) {
                validator.add(this, Messages.format("imobject.singletonexists", archetype.getDisplayName()));
                result = false;
            } else {
                // don't check duplicates again until save, as its an expensive operation
                checkSingleton = false;
            }
        }
        return result;
    }

    /**
     * Determines if an active singleton exists of the same archetype.
     *
     * @return {@code true} if the archetype is a singleton, and an instance already exists, otherwise {@code false}
     */
    protected boolean activeSingletonExists() {
        boolean result = false;
        if (archetype.isSingleton() && object.isActive()) {
            result = IMObjectHelper.hasActiveInstance(object.getArchetype(), object.getId());
        }
        return result;
    }

    /**
     * Helper to add a validation error for a required property.
     *
     * @param name      the required property name
     * @param validator the validator
     * @return {@code false}
     */
    protected boolean reportRequired(String name, Validator validator) {
        Property property = getProperty(name);
        if (property != null) {
            reportRequired(property, validator);
        }
        return false;
    }

    /**
     * Helper to add a validation error for a required property.
     *
     * @param property  the required property
     * @param validator the validator
     * @return {@code false}
     */
    protected boolean reportRequired(Property property, Validator validator) {
        String message = Messages.format("property.error.required", property.getDisplayName());
        validator.add(property, new ValidatorError(property, message));
        return false;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        if (service == null) {
            service = ServiceHelper.getArchetypeService();
        }
        return service;
    }

    /**
     * Refreshes audit information.
     * <p/>
     * This walks the component hierarchy looking for {@link AuditInfo} components to refresh. This is required
     * as there is no popup component that can be used to render audit information on demand; popups have to be
     * pre-rendered.
     */
    private void refreshAuditInfo() {
        if (viewer != null && viewer.hasComponent()) {
            Component component = viewer.getComponent();
            refreshAuditInfo(component, context.getNames());
        }
    }

    /**
     * Refreshes audit information for a component and its children.
     *
     * @param component the component
     * @param names     the object names
     */
    private void refreshAuditInfo(Component component, IMObjectNames names) {
        if (component instanceof AuditInfo) {
            ((AuditInfo) component).refresh(names);
        }
        for (Component child : component.getComponents()) {
            refreshAuditInfo(child, names);
        }
    }

    /**
     * Registers an editor created via the editor factory.
     * <p/>
     * These will be disposed of when the layout changes.
     *
     * @param editor the editor
     */
    private void registerEditor(Editor editor) {
        addEditor(editor);
        createdEditors.add(editor);
    }

    /**
     * Removes listeners associated with lookup nodes
     *
     * @param all if {@code true} remove all listeners, else only remove only those created via the editor factory
     */
    private void disposeLookups(boolean all) {
        List<Map.Entry<LookupPropertyEditor, ModifiableListener>> entries = new ArrayList<>(lookups.entrySet());
        for (Map.Entry<LookupPropertyEditor, ModifiableListener> entry : entries) {
            LookupPropertyEditor editor = entry.getKey();
            boolean dispose = all || createdEditors.contains(editor);
            if (dispose) {
                editor.removeModifiableListener(entry.getValue());
                lookups.remove(editor);
            }
        }
    }

}
