/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.web.component.app.Context;

/**
 * A popup window that displays an {@link IMObjectEditor}.
 *
 * @author Tim Anderson
 */
public class EditDialog extends AbstractEditDialog {

    /**
     * Constructs an {@link EditDialog}.
     *
     * @param editor  the editor
     * @param context the context
     */
    public EditDialog(IMObjectEditor editor, Context context) {
        this(editor, EditActions.applyOKCancel(), context);
    }

    /**
     * Constructs an {@link EditDialog}.
     *
     * @param editor  the editor
     * @param actions the supported dialog actions
     * @param context the context
     */
    public EditDialog(IMObjectEditor editor, EditActions actions, Context context) {
        super(editor, actions, context);
    }

}
