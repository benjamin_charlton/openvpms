/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;

/**
 * Helper to generate tel: links for phone contacts..
 *
 * @author Tim Anderson
 */
public class PhoneLink {

    /**
     * The party rules.
     */
    private final PartyRules rules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The maximum no. of characters to display.
     */
    private final int length;

    /**
     * Constructs a {@link PhoneLink}.
     *
     * @param rules   the party rules
     * @param service the archetype service
     */
    public PhoneLink(PartyRules rules, ArchetypeService service) {
        this(rules, service, -1);
    }

    /**
     * Constructs a {@link PhoneLink}.
     *
     * @param rules   the party rules
     * @param service the archetype service
     * @param length  the maximum no. of characters to display, or {@code <= 0} to not restrict them
     */
    public PhoneLink(PartyRules rules, ArchetypeService service, int length) {
        this.rules = rules;
        this.service = service;
        this.length = length;
    }

    /**
     * Creates a tel: link for a phone contact.
     *
     * @param contact     the phone contact. May be {@code null}
     * @param includeName if {@code true} includes the name, if it is not the default value for the contact
     * @return a link, or {@code null} if the contact is missing or unpopulated
     */
    public Component getLink(Contact contact, boolean includeName) {
        Component result = null;
        if (contact != null) {
            String phone;
            String formattedPhone;

            Contacts contacts = new Contacts(service);
            phone = contacts.getPhone(contact);
            if (phone != null) {
                // is populated (to some degree)
                formattedPhone = rules.formatPhone(contact, includeName);
                String tooltip = null;
                if (length > 0 && formattedPhone.length() > length) {
                    tooltip = formattedPhone;
                    formattedPhone = ContactHelper.abbreviatePhone(contact, includeName, length, rules, service);
                }
                // add a tel: link for the phone
                Label html = LabelFactory.html("<a href=\"tel:" + StringEscapeUtils.escapeXml11(phone) + "\">"
                                               + StringEscapeUtils.escapeXml11(formattedPhone) + "</a>");
                html.setStyleName(Styles.DEFAULT);
                if (tooltip != null) {
                    html.setToolTipText(tooltip);
                }
                result = html;
            }
        }
        return result;
    }

}