/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.edit.Deletable;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Collection editor for document versions.
 *
 * @author Tim Anderson
 */
public class DocumentVersionsCollectionEditor extends ActRelationshipCollectionEditor implements Deletable {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link DocumentVersionsCollectionEditor}.
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public DocumentVersionsCollectionEditor(CollectionProperty property, Act act, LayoutContext context) {
        super(property, act, context);
        service = ServiceHelper.getArchetypeService();
    }

    /**
     * Perform deletion.
     */
    @Override
    public void delete() {
        Act object = (Act) getObject();
        IMObjectBean bean = getBean(object);
        List<IMObject> toSave = new ArrayList<>();
        // need to remove relationships to the parent act and save it, before removing this
        for (Act act : getCurrentActs()) {
            ActRelationship relationship = (ActRelationship) bean.removeTarget("versions", act);
            if (relationship != null) {
                act.removeActRelationship(relationship);
                toSave.add(act);
            }
        }
        if (!toSave.isEmpty()) {
            toSave.add(object);
            service.save(toSave);

            // remove the versions
            for (Act act : getCurrentActs()) {
                IMObjectEditor editor = getEditor(act);
                if (editor != null) {
                    editor.delete();
                }
            }
        }
    }
}
