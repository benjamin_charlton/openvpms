/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.web.component.im.list.AllNoneListCellRenderer;

/**
 * A ListCellRender for a {@link PrinterListModel}.
 *
 * @author Tim Anderson
 */
public class PrinterListCellRenderer extends AllNoneListCellRenderer<PrinterReference> {

    /**
     * The singleton instance.
     */
    public static PrinterListCellRenderer INSTANCE = new PrinterListCellRenderer();

    /**
     * Constructs a {@link PrinterListCellRenderer}.
     */
    private PrinterListCellRenderer() {
        super(PrinterReference.class);
    }

    /**
     * Returns the string form of the object at the specified cell.
     *
     * @param list   the list component
     * @param object the object to render. May be {@code null}
     * @param index  the object index
     * @return the text. May be {@code null}
     */
    @Override
    protected String toString(Component list, PrinterReference object, int index) {
        if (object.getServiceName() != null && object.getArchetype() != null) {
            return object.getServiceName() + ": " + object.getName();
        }
        return object.getName();
    }
}
