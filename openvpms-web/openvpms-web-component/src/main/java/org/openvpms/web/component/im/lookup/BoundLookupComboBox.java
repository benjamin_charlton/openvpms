/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.component.bound.Binder;
import org.openvpms.web.component.bound.BoundComboBox;
import org.openvpms.web.component.bound.ComboBoxBinder;
import org.openvpms.web.component.im.list.LookupListCellRenderer;
import org.openvpms.web.component.im.list.LookupListModel;
import org.openvpms.web.component.property.Property;

/**
 * Binds a lookup {@link Property} to a combo box that displays lookups.
 *
 * @author Tim Anderson
 */
public class BoundLookupComboBox extends BoundComboBox<Lookup> {

    /**
     * The available lookups.
     */
    private final LookupQuery lookups;

    /**
     * Constructs a {@link BoundLookupComboBox}.
     *
     * @param property the property to bind to the combobox
     * @param parent   the parent object
     */
    public BoundLookupComboBox(Property property, IMObject parent) {
        this(property, new NodeLookupQuery(parent, property));
    }

    /**
     * Constructs a {@link BoundLookupComboBox}.
     *
     * @param property the property to bind to the combobox
     * @param lookups  the available lookups
     */
    public BoundLookupComboBox(Property property, LookupQuery lookups) {
        super(property, new LookupListModel(lookups));
        this.lookups = lookups;
        setListCellRenderer(new StyledLookupListCellRenderer());
        setListRowCount(10);
        getBinder().bind();
        if (getSelected() == null && getText() == null) {
            setDefaultSelection();
        }
    }

    /**
     * Returns the selected lookup.
     *
     * @return the selected lookup, or {@code null} if no lookup is selected.
     */
    public Lookup getSelected() {
        return getSelected(lookups.getLookups());
    }

    /**
     * Returns the selected lookup's code.
     *
     * @return the selected lookup's code, or {@code null} if no lookup is selected, or the selected entry is 'All' or
     * 'None'
     */
    public String getSelectedCode() {
        Lookup result = getSelected();
        return result != null ? result.getCode() : null;
    }

    /**
     * Sets the selected lookup.
     *
     * @param lookup the lookup. May be {@code null}
     */
    public void setSelected(Lookup lookup) {
        setSelected(lookup != null ? lookup.getCode() : null);
    }

    /**
     * Sets the selected lookup code.
     *
     * @param code the code. May be {@code null}
     */
    public void setSelected(String code) {
        getProperty().setValue(code);
    }

    /**
     * Refreshes the model if required.
     * <p/>
     * If the model refreshes {@link #setDefaultSelection()} is invoked.
     *
     * @return {@code true} if the model refreshed
     */
    public boolean refresh() {
        LookupListModel model = getListModel();
        boolean refreshed = model.refresh();
        if (refreshed) {
            setDefaultSelection();
        }
        return refreshed;
    }

    /**
     * Returns the list model.
     *
     * @return the list model
     */
    @Override
    public LookupListModel getListModel() {
        return (LookupListModel) super.getListModel();
    }

    /**
     * Returns the list cell renderer.
     *
     * @return list cell renderer
     */
    @Override
    public LookupListCellRenderer getListCellRenderer() {
        return (LookupListCellRenderer) super.getListCellRenderer();
    }

    /**
     * Sets the default selection.
     * <p/>
     * This:
     * <ol>
     * <li>selects the default lookup is if present, otherwise;</li>
     * <li>selects <em>All</em> is selected if present, otherwise;</li>
     * <li>selects <em>None</em> is selected if present, otherwise;</li>
     * <li>clears the selection</li>
     * </ol>
     */
    @Override
    public void setDefaultSelection() {
        Property property = getProperty();
        LookupListModel model = getListModel();
        Lookup lookup = model.getDefaultLookup();
        if (lookup != null) {
            setSelected(lookup);
        } else if (model.getAllIndex() != -1) {
            property.setValue(null);
            setText(getText(model.getAllIndex(), model));
        } else if (model.getNoneIndex() != -1) {
            property.setValue(null);
            setText(getText(model.getNoneIndex(), model));
        } else {
            super.setDefaultSelection();
        }
    }

    /**
     * Creates a binder for the property.
     *
     * @param property the property
     * @return a new binder
     */
    @Override
    protected Binder createBinder(Property property) {
        return new ComboBoxBinder(property, this, false) {
            /**
             * Returns the object matching the specified text.
             *
             * @param text the text
             * @return the object, or {@code null} if there is no match
             */
            @Override
            protected String getObject(String text) {
                Lookup object = BoundLookupComboBox.this.getObject(StringUtils.trimToNull(text), lookups.getLookups());
                return object != null ? object.getCode() : null;
            }

            /**
             * Returns the text for the specified property value.
             *
             * @param value the property value
             * @return the corresponding text, or {@code null}
             */
            @Override
            protected String getText(Object value) {
                String result = null;
                if (value instanceof String) {
                    Lookup lookup = getLookup((String) value);
                    if (lookup != null) {
                        result = lookup.getName();
                    }
                }
                return result;
            }
        };
    }

    /**
     * Determines if the entered text represents a placeholder rather than an actual value.
     *
     * @param text the text
     * @return {@code true} if the text is a placeholder, otherwise {@code false}
     */
    @Override
    protected boolean isPlaceholder(String text) {
        return isPlaceholder(text, getListModel());
    }

    /**
     * Returns the lookups.
     *
     * @return the lookups
     */
    protected LookupQuery getLookups() {
        return lookups;
    }

    /**
     * Returns the lookup with the specified code.
     *
     * @param code the code
     * @return the corresponding lookup, or {@code null} if none is found
     */
    private Lookup getLookup(String code) {
        for (Lookup lookup : lookups.getLookups()) {
            if (code.equals(lookup.getCode())) {
                return lookup;
            }
        }
        return null;
    }

}
