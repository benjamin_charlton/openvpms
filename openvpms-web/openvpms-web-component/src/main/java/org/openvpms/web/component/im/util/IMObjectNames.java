/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.util;

import org.apache.commons.collections4.map.LRUMap;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.cache.IMObjectCache;

import java.util.Map;

/**
 * Caches {@link IMObjectNames} names.
 *
 * @author Tim Anderson
 */
public class IMObjectNames {

    /**
     * The names, keyed on reference.
     */
    private final Map<Reference, String> names;

    /**
     * The backing cache.
     */
    private final IMObjectCache cache;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs a {@link IMObjectNames}, backed by an {@link IMObjectCache}.
     *
     * @param cache     the cache
     * @param service   the archetype service
     * @param cacheSize the number of names to cache
     */
    public IMObjectNames(IMObjectCache cache, IArchetypeService service, int cacheSize) {
        this.names = new LRUMap<>(cacheSize);
        this.cache = cache;
        this.service = service;
    }

    /**
     * Returns a name given its reference.
     *
     * @param reference the reference. May be {@code null}
     * @return the corresponding name, or {@code null} if none is found
     */
    public String getName(Reference reference) {
        String name = names.get(reference);
        if (name == null) {
            IMObject object = cache.getCached(reference);
            if (object != null) {
                name = object.getName();
            } else {
                name = IMObjectHelper.getName(reference, service);
            }
            if (name != null) {
                names.put(reference, name);
            }
        }
        return name;
    }

    /**
     * Clears the name cache.
     * <p/>
     * Note: this does not clear any underlying {@link IMObjectCache}.
     */
    public void clear() {
        names.clear();
    }
}
