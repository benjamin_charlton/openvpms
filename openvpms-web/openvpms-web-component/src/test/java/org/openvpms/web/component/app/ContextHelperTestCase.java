/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.party.Party;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link ContextHelper}.
 *
 * @author Tim Anderson
 */
public class ContextHelperTestCase extends ArchetypeServiceTest {

    /**
     * Tests the {@link ContextHelper#setCustomer(Context, Party)} method.
     */
    @Test
    public void testSetCustomer() {
        Party customer1 = TestHelper.createCustomer();
        Party patientA = TestHelper.createPatient(customer1);
        Party patientB = TestHelper.createPatient(customer1);
        Party customer2 = TestHelper.createCustomer();

        Context context = new LocalContext();
        context.setCustomer(customer1);
        context.setPatient(patientA);
        assertEquals(customer1, context.getCustomer());
        assertEquals(patientA, context.getPatient());

        context.setPatient(patientB);
        ContextHelper.setCustomer(context, customer1);
        assertEquals(customer1, context.getCustomer());
        assertEquals(patientB, context.getPatient());

        ContextHelper.setCustomer(context, customer2);
        assertEquals(customer2, context.getCustomer());
        assertNull(context.getPatient());
    }

    /**
     * Tests the {@link ContextHelper#setPatient(Context, Party)} method.
     * This is equivalent to {@code ContextHelper#setPatient(Context, Party, true)}.
     */
    @Test
    public void testSetPatient() {
        Party customer1 = TestHelper.createCustomer();
        Party patient1 = TestHelper.createPatient(customer1);
        Party customer2 = TestHelper.createCustomer();
        Party patient2 = TestHelper.createPatient(customer2);
        Party patient3 = TestHelper.createPatient();

        Context context = new LocalContext();
        context.setCustomer(customer1);
        context.setPatient(patient1);
        assertEquals(customer1, context.getCustomer());
        assertEquals(patient1, context.getPatient());

        // patient2 has no relationship to the current customer so will be set to null
        ContextHelper.setPatient(context, patient2);
        assertEquals(customer1, context.getCustomer());
        assertNull(context.getPatient());

        // patient3 has no relationship to the current customer so will be set to null
        ContextHelper.setPatient(context, patient3);
        assertEquals(customer1, context.getCustomer());
        assertNull(context.getPatient());

        context.setCustomer(customer1);
        context.setPatient(patient1);
        ContextHelper.setPatient(context, null);
        assertEquals(customer1, context.getCustomer());
        assertNull(context.getPatient());
    }

    /**
     * Tests the {@link ContextHelper#setPatient(Context, Party, boolean)} where
     * {@code preserveCustomerIfNoRelationship == true}.
     */
    @Test
    public void testSetPatientWithoutPreservingCurrentCustomer() {
        Party customer1 = TestHelper.createCustomer();
        Party patient1 = TestHelper.createPatient(customer1);
        Party customer2 = TestHelper.createCustomer();
        Party patient2 = TestHelper.createPatient(customer2);
        Party patient3 = TestHelper.createPatient();

        Context context = new LocalContext();
        context.setCustomer(customer1);
        context.setPatient(patient1);
        assertEquals(customer1, context.getCustomer());
        assertEquals(patient1, context.getPatient());

        ContextHelper.setPatient(context, patient2, false);
        assertEquals(customer2, context.getCustomer());
        assertEquals(patient2, context.getPatient());

        ContextHelper.setPatient(context, patient3, false);
        assertNull(context.getCustomer());
        assertEquals(patient3, context.getPatient());

        ContextHelper.setPatient(context, patient1, false);
        assertEquals(customer1, context.getCustomer());
        assertEquals(patient1, context.getPatient());
    }
}