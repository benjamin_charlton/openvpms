/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.business.service.archetype.helper.ActBean;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.edit.AbstractCollectionPropertyEditorTest;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.property.CollectionProperty;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;


/**
 * {@link ActRelationshipCollectionPropertyEditor} test case.
 *
 * @author Tim Anderson
 */
public class ActRelationshipCollectionPropertyEditorTestCase
        extends AbstractCollectionPropertyEditorTest {

    /**
     * The product.
     */
    private Product product;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        product = TestHelper.createProduct();
    }

    /**
     * Tests {@link CollectionPropertyEditor#getArchetypeRange()}.
     */
    @Test
    public void testGetArchetypeRange() {
        IMObject parent = createParent();
        CollectionProperty property = getCollectionProperty(parent);
        CollectionPropertyEditor editor = createEditor(property, parent);
        String[] range = editor.getArchetypeRange();
        assertEquals(1, range.length);
        assertEquals("act.customerEstimationItem", range[0]);
    }

    /**
     * Verifies that the parent and child of a collection can be saved twice
     * without losing the relationship between them.
     * This verifies the fix for OVPMS-710.
     */
    @Test
    public void testSaveTwice() {
        IMObject parent = createParent();
        CollectionProperty property = getCollectionProperty(parent);
        CollectionPropertyEditor editor = createEditor(property, parent);

        IMObject element = createObject(parent);
        editor.add(element);
        assertEquals(1, editor.getObjects().size());
        assertSame(element, editor.getObjects().get(0));

        // save the parent and child, verifying the versions have incremented
        execute(transactionStatus -> {
            assertTrue(SaveHelper.save(parent));
            editor.save();
            return null;
        });
        assertEquals(0, parent.getVersion());
        assertEquals(0, element.getVersion());

        // now save the parent and child again
        assertTrue(SaveHelper.save(parent));
        editor.add(element);
        modify(element);
        editor.save();
        assertEquals(0, parent.getVersion()); // parent not changed, so version not incremented
        assertEquals(1, element.getVersion());

        // now retrieve parent and verify collection matches the original
        IMObject savedParent = get(parent);
        assertNotNull(savedParent);
        CollectionPropertyEditor saved = createEditor(
                getCollectionProperty(savedParent), savedParent);
        assertEquals(1, saved.getObjects().size());
        assertTrue(saved.getObjects().contains(element));
    }

    /**
     * Verifies that relationships are sequenced.
     */
    @Test
    public void testSequence() {
        IMObject parent = createParent();
        CollectionProperty property = getCollectionProperty(parent);
        ActRelationshipCollectionPropertyEditor editor1 = createEditor(property, parent);

        IMObject object1 = createObject(parent);
        IMObject object2 = createObject(parent);
        IMObject object3 = createObject(parent);
        IMObject object4 = createObject(parent);
        IMObject object5 = createObject(parent);

        // add 3 objects and verify they are sequenced in order of addition
        editor1.add(object1);
        editor1.add(object2);
        editor1.add(object3);
        assertEquals(3, editor1.getObjects().size());
        assertEquals(0, editor1.getRelationship(object1).getSequence());
        assertEquals(1, editor1.getRelationship(object2).getSequence());
        assertEquals(2, editor1.getRelationship(object3).getSequence());

        // remove the middle object and add a new one. There should be a gap in sequences.
        editor1.remove(object2);
        editor1.add(object4);
        assertEquals(3, editor1.getObjects().size());
        assertEquals(0, editor1.getRelationship(object1).getSequence());
        assertNull(editor1.getRelationship(object2));
        assertEquals(2, editor1.getRelationship(object3).getSequence());
        assertEquals(3, editor1.getRelationship(object4).getSequence());

        // remove the last object, and add a new one. It should have the same sequence
        editor1.remove(object4);
        editor1.add(object5);
        assertEquals(3, editor1.getObjects().size());
        assertEquals(0, editor1.getRelationship(object1).getSequence());
        assertNull(editor1.getRelationship(object2));
        assertEquals(2, editor1.getRelationship(object3).getSequence());
        assertNull(editor1.getRelationship(object4));
        assertEquals(3, editor1.getRelationship(object5).getSequence());

        // save the parent and children
        execute(transactionStatus -> {
            assertTrue(SaveHelper.save(parent));
            editor1.save();
            return null;
        });

        // verifies that when the editor is recreated, the existing sequences are preserved
        ActRelationshipCollectionPropertyEditor editor2 = createEditor(property, parent);
        assertEquals(3, editor2.getObjects().size());
        assertEquals(0, editor2.getRelationship(object1).getSequence());
        assertEquals(2, editor2.getRelationship(object3).getSequence());
        assertEquals(3, editor2.getRelationship(object5).getSequence());

        // add a new object, and verify it gets the next sequence
        IMObject object6 = createObject(parent);
        editor2.add(object6);
        assertEquals(4, editor2.getObjects().size());
        assertEquals(0, editor2.getRelationship(object1).getSequence());
        assertEquals(2, editor2.getRelationship(object3).getSequence());
        assertEquals(3, editor2.getRelationship(object5).getSequence());
        assertEquals(4, editor2.getRelationship(object6).getSequence());
    }

    /**
     * Returns the parent of the collection.
     *
     * @return the parent object
     */
    protected IMObject createParent() {
        Act act = (Act) create("act.customerEstimation");
        ActBean bean = new ActBean(act);

        Party customer = TestHelper.createCustomer(true);
        bean.addParticipation("participation.customer", customer);
        act.setStatus(FinancialActStatus.IN_PROGRESS);
        return act;
    }

    /**
     * Returns the name of the collection node.
     *
     * @return the node name
     */
    protected String getCollectionNode() {
        return "items";
    }

    /**
     * Returns an editor for a collection property.
     *
     * @param property the collection property
     * @param parent   the parent of the collection
     * @return a new editor for the property
     */
    @Override
    protected ActRelationshipCollectionPropertyEditor createEditor(CollectionProperty property, IMObject parent) {
        return new ActRelationshipCollectionPropertyEditor(property, (Act) parent);
    }

    /**
     * Returns an object to add to the collection.
     *
     * @param parent the parent of the collection
     * @return a new object to add to the collection
     */
    protected IMObject createObject(IMObject parent) {
        Act act = (Act) create("act.customerEstimationItem");

        Party patient = TestHelper.createPatient(true);
        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);
        bean.setTarget("product", product);
        act.setStatus(FinancialActStatus.IN_PROGRESS);
        return act;
    }

    /**
     * Makes an object valid or invalid.
     *
     * @param object the object
     * @param valid  if {@code true}, make it valid, otherwise make it invalid
     */
    @Override
    protected void makeValid(IMObject object, boolean valid) {
        IMObjectBean bean = getBean(object);
        if (!valid) {
            bean.setTarget("product", (Product) null);
        } else {
            bean.setTarget("product", product);
        }
    }

    /**
     * Modify an object so that its version will increment on save.
     *
     * @param object the object to modify
     */
    @Override
    protected void modify(IMObject object) {
        IMObjectBean bean = getBean(object);
        bean.setValue("highTotal", bean.getBigDecimal("highTotal").add(BigDecimal.ONE));
    }
}
