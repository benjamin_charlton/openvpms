/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException;
import org.apache.commons.lang3.mutable.MutableObject;
import org.hibernate.exception.LockAcquisitionException;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.DefaultActEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link EditIMObjectTask} class.
 *
 * @author Tim Anderson
 */
public class EditIMObjectTaskTestCase extends AbstractAppTest {

    /**
     * Test customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * Test customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * Verifies that if a background edit fails, it cancels the task.
     */
    @Test
    public void testBackgroundEditFailureCancelsTask() {
        List<String> errors = new ArrayList<>();
        initErrorHandler(errors);

        // set up a task to POST an invoice in the background, but use an editor that fails on save
        EditIMObjectTask task = new EditIMObjectTask(CustomerAccountArchetypes.INVOICE, false, false) {
            @Override
            protected IMObjectEditor createEditor(IMObject object, TaskContext context) {
                return new TestChargeEditor((Act) object, createLayoutContext(context));
            }

            /**
             * Edits an object in the background.
             *
             * @param editor  the editor
             * @param context the task context
             */
            @Override
            protected void edit(IMObjectEditor editor, TaskContext context) {
                ((TestChargeEditor) editor).setStatus(ActStatus.POSTED);
            }
        };
        TaskContext context = new DefaultTaskContext(new HelpContext("foo", null));
        FinancialAct invoice = accountFactory.newInvoice().customer(customerFactory.createCustomer()).build();
        context.addObject((IMObject) invoice);
        MutableObject<TaskEvent> mutableObject = new MutableObject<>();
        task.addTaskListener(new DefaultTaskListener() {
            @Override
            public void taskEvent(TaskEvent event) {
                mutableObject.setValue(event);
            }
        });
        task.start(context);

        TaskEvent event = mutableObject.getValue();
        assertNotNull(event);
        assertEquals(TaskEvent.Type.CANCELLED, event.getType());
        assertEquals(1, errors.size());
        assertEquals("Failed to save Invoice due to deadlock. Try restarting the operation.", errors.get(0));
    }

    /**
     * A test ediitor that throws an exception on save.
     */
    private static class TestChargeEditor extends DefaultActEditor {
        public TestChargeEditor(Act object, LayoutContext context) {
            super(object, null, context);
        }

        /**
         * Creates a new instance of the editor, with the latest instance of the object to edit.
         *
         * @return {@code null}
         */
        @Override
        public IMObjectEditor newInstance() {
            return new TestChargeEditor(reload(getObject()), getLayoutContext());
        }

        /**
         * Save any edits.
         */
        @Override
        protected void doSave() {
            // simulate a deadlock
            throw new CannotAcquireLockException(
                    "could not execute batch",
                    new LockAcquisitionException(
                            "could not execute batch",
                            new BatchUpdateException(
                                    new MySQLTransactionRollbackException(
                                            "Deadlock found when trying to get lock; try restarting transaction"))));
        }
    }
}