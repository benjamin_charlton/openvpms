/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ValidatorError;

import static org.junit.Assert.fail;

/**
 * Editor test helper methods.
 *
 * @author Tim Anderson
 */
public class EditorTestHelper {

    /**
     * Asserts that a {@link Modifiable} is valid.
     *
     * @param modifiable the modifiable
     */
    public static void assertValid(Modifiable modifiable) {
        DefaultValidator validator = new DefaultValidator();
        if (!modifiable.validate(validator)) {
            ValidatorError firstError = validator.getFirstError();
            if (firstError == null) {
                fail("Expected " + modifiable.getClass().getName()
                     + " to be valid but validation failed. No validation error raised");
            } else {
                fail("Expected " + modifiable.getClass().getName() +  " to be valid but validation failed with:  "
                     + firstError);
            }
        }
    }

}
