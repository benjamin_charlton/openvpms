/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryTestBuilder;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.query.AbstractEntityQueryTest;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link TestQuery} class.
 *
 * @author Tim Anderson
 */
public class TestQueryTestCase extends AbstractEntityQueryTest<Entity> {

    /**
     * Test laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory factory;

    /**
     * Verifies that {@link TestQuery} is created by {@link QueryFactory} for <em>entity.entityLaboratoryTest</em> and
     * <em>entity.entityLaboratoryTestHL7</em>
     */
    @Test
    public void testQueryFactory() {
        checkCreate("entity.laboratoryTest*", LaboratoryArchetypes.TEST, LaboratoryArchetypes.HL7_TEST);
        checkCreate(LaboratoryArchetypes.TEST, LaboratoryArchetypes.TEST);
        checkCreate(LaboratoryArchetypes.HL7_TEST, LaboratoryArchetypes.HL7_TEST);
    }

    /**
     * Verifies that tests can be queried by both name and identity.
     */
    @Test
    public void testSearchAll() {
        TestQuery query = (TestQuery) createQuery();
        assertTrue(query.isSearchAll());
        String test1Name = "1" + getUniqueValue();
        String test1Code = "2" + getUniqueValue();
        String test2Name = "3" + getUniqueValue();
        String test2Code = "4" + getUniqueValue();
        Entity test1 = createTest(test1Name, test1Code, true);
        Entity test2 = createTest(test2Name, test2Code, true);

        assertTrue(query.isIdentitySearch());
        query.setIdentitySearch(false);
        query.setValue(test1Name);

        checkExists(test1, query, true);
        checkExists(test2, query, false);

        query.setValue(test1Code);
        checkExists(test1, query, false);
        checkExists(test2, query, false);

        query.setIdentitySearch(true);
        checkExists(test1, query, true); // should now find by identity
        checkExists(test2, query, false);

        query.setValue(test2Name);
        checkExists(test1, query, false);
        checkExists(test2, query, true);

        query.setValue(test2Code);
        checkExists(test1, query, false);
        checkExists(test2, query, true);

        query.setValue(Long.toString(test1.getId()));
        checkExists(test1, query, true);
        checkExists(test2, query, false);
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery() {
        return new TestQuery(new LocalContext());
    }

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if {@code true} save the object, otherwise don't save it
     * @return the new object
     */
    @Override
    protected Entity createObject(String value, boolean save) {
        return createTest(value, null, save);
    }

    /**
     * Generates a unique value which may be used for querying objects on.
     *
     * @return a unique value
     */
    @Override
    protected String getUniqueValue() {
        return getUniqueValue("ZTest");
    }

    /**
     * Verifies that the query implementation returned by {@link QueryFactory#create(String[], Context)}
     * is a {@link TestQuery}.
     *
     * @param archetype the test archetype(s)
     * @param expected  the expected archetypes
     */
    private void checkCreate(String archetype, String... expected) {
        Query<Entity> query = QueryFactory.create(archetype, new LocalContext(), Entity.class);
        assertTrue(query instanceof TestQuery);
        String[] archetypes = query.getShortNames();
        assertEquals(expected.length, archetypes.length);
        for (String expectedArchetype : expected) {
            assertTrue(ArrayUtils.contains(archetypes, expectedArchetype));
        }
    }

    /**
     * Creates a new test.
     *
     * @param name the test name
     * @param code the test identity
     * @param save if {@code true}, save the test
     * @return the test
     */
    private Entity createTest(String name, String code, boolean save) {
        TestLaboratoryTestBuilder builder = factory.newTest()
                .name(name)
                .investigationType(factory.createInvestigationType());
        if (code != null) {
            builder.code(LaboratoryArchetypes.TEST_CODE, code);
        }
        return (Entity) builder.build(save);
    }
}
