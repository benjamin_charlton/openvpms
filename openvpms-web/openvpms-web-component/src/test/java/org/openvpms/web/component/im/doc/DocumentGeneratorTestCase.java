/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.report.DocumentActReporter;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link DocumentGenerator}.
 *
 * @author Tim Anderson
 */
public class DocumentGeneratorTestCase extends AbstractAppTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules rules;

    /**
     * Verifies that {@link Context} fields are available to documents generated via {@link DocumentGenerator}.
     *
     * @throws Exception for any error
     */
    @Test
    public void testContextFields() throws Exception {
        Party customer = (Party) customerFactory.createCustomer("Foo", "Bar");
        Entity template = (Entity) documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document("/customerformtemplate.jrxml")
                .build();
        DocumentAct act = (DocumentAct) create(CustomerArchetypes.DOCUMENT_FORM);
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", customer);
        bean.setTarget("documentTemplate", template);

        // set up the Context
        Context context = DocumentTestHelper.createReportContext();

        DocumentGenerator.Listener listener = Mockito.mock(DocumentGenerator.Listener.class);
        FileNameFormatter formatter = new FileNameFormatter(getArchetypeService(), getLookupService(), rules);
        DocumentGenerator generator = new DocumentGenerator(act, context, new HelpContext("foo", null),
                                                            formatter, getArchetypeService(), getLookupService(),
                                                            listener) {
            @Override
            protected Document generate(DocumentActReporter reporter) {
                // generate the document as a CSV to allow comparison
                return reporter.getDocument(DocFormats.CSV_TYPE, false);
            }
        };

        // generate the document, and convert it to a string
        generator.generate();
        Document document = generator.getDocument();
        assertNotNull(document);
        String result = documentFactory.toString(document).trim();

        // the customer name should be followed by each of the context fields.
        assertEquals("Foo,Bar,Vets R Us,Main Clinic,Main Stock,\"Smith,J\",Fido,Vet Supplies,Acepromazine,"
                     + "Main Deposit,Main Till,Vet,User,Visit,Invoice,Appointment,Task", result);
    }

    /**
     * Verifies that the outputFormat is used when generating documents.
     */
    @Test
    public void testOutputFormat() {
        checkOutputFormat("ODT", ".odt");
        checkOutputFormat("PDF", ".pdf");
    }

    /**
     * Verifies that the output is used when generating documents.
     *
     * @param outputFormat the output format
     * @param extension    the expected file name extension
     */
    private void checkOutputFormat(String outputFormat, String extension) {
        Party customer = (Party) customerFactory.createCustomer("Foo", "Bar");
        Entity template = (Entity) documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_LETTER)
                .document("/customerlettertemplate.jrxml")
                .outputFormat(outputFormat)
                .build();
        DocumentAct act = (DocumentAct) create(CustomerArchetypes.DOCUMENT_LETTER);
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", customer);
        bean.setTarget("documentTemplate", template);

        DocumentGenerator.Listener listener = Mockito.mock(DocumentGenerator.Listener.class);
        FileNameFormatter formatter = new FileNameFormatter(getArchetypeService(), getLookupService(), rules);
        DocumentGenerator generator = new DocumentGenerator(act, new LocalContext(), new HelpContext("foo", null),
                                                            formatter, getArchetypeService(), getLookupService(),
                                                            listener);

        // generate the document and verify it is of the correct type
        generator.generate();
        Document document = generator.getDocument();
        assertEquals(template.getName() + extension, document.getName());
    }
}
