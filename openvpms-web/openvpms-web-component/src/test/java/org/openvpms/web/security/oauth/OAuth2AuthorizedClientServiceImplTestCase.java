/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link OAuth2AuthorizedClientServiceImpl} class.
 *
 * @author Tim Anderson
 */
public class OAuth2AuthorizedClientServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * Tests the service.
     */
    @Test
    public void testService() {
        String registrationId = TestHelper.randomName("gmail");
        ClientRegistration registration = CommonOAuth2Provider.GOOGLE.getBuilder(registrationId)
                .clientId("aclientId")
                .clientSecret("aclientsecret")
                .build();
        PasswordEncryptor encryptor = new PasswordEncryptor() {
            @Override
            public String encrypt(String password) {
                return password;
            }

            @Override
            public String decrypt(String encryptedPassword) {
                return encryptedPassword;
            }
        };
        ClientRegistrationRepository repository = new InMemoryClientRegistrationRepository(registration);
        OAuth2AuthorizedClientServiceImpl clientService = new OAuth2AuthorizedClientServiceImpl(
                repository, getArchetypeService(),encryptor);
        String email = "foo@bar.com";
        assertNull(clientService.loadAuthorizedClient(registrationId, email));
        Date issuedAt1 = TestHelper.getDatetime("2023-01-01 10:01");
        Date issuedAt2 = TestHelper.getDatetime("2023-01-01 10:02");
        Date expiresAt = TestHelper.getDate("2023-01-02");
        OAuth2AccessToken accessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, "abc123",
                                                              issuedAt1.toInstant(), expiresAt.toInstant(),
                                                              new HashSet<>(Arrays.asList("a", "b")));
        OAuth2RefreshToken refreshToken = new OAuth2RefreshToken("def567", issuedAt2.toInstant());
        OAuth2AuthorizedClient client = new OAuth2AuthorizedClient(registration, email, accessToken,
                                                                   refreshToken);
        clientService.saveAuthorizedClient(client, new PreAuthenticatedAuthenticationToken(email, null));
        OAuth2AuthorizedClient loaded = clientService.loadAuthorizedClient(registrationId, email);
        assertNotNull(loaded);

        assertEquals(registration, loaded.getClientRegistration());
        OAuth2AccessToken loadedAccessToken = loaded.getAccessToken();
        assertEquals(accessToken.getTokenType(), loadedAccessToken.getTokenType());
        assertEquals(accessToken.getTokenValue(), loadedAccessToken.getTokenValue());
        assertEquals(accessToken.getScopes(), loadedAccessToken.getScopes());
        assertEquals(accessToken.getIssuedAt(), loadedAccessToken.getIssuedAt());
        assertEquals(accessToken.getExpiresAt(), loadedAccessToken.getExpiresAt());

        OAuth2RefreshToken loadedRefreshToken = loaded.getRefreshToken();
        assertNotNull(loadedRefreshToken);
        assertEquals(refreshToken.getTokenValue(), loadedRefreshToken.getTokenValue());
        assertEquals(refreshToken.getIssuedAt(), loadedRefreshToken.getIssuedAt());
    }
}
