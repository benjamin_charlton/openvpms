/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.msword;

import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XEnumeration;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.uno.UnoRuntime;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.report.openoffice.OOConnection;
import org.openvpms.report.openoffice.OpenOfficeDocument;
import org.openvpms.report.openoffice.OpenOfficeException;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToGetField;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToGetUserFields;


/**
 * Thin wrapper around a Microsoft Word document.
 *
 * @author Tim Anderson
 */
public class MsWordDocument extends OpenOfficeDocument {

    /**
     * Constructs an {@link MsWordDocument}.
     *
     * @param document   the source document
     * @param connection the connection to the OpenOffice service
     * @param handlers   the document handlers
     * @throws OpenOfficeException for any error
     */
    public MsWordDocument(Document document, OOConnection connection, DocumentHandlers handlers) {
        super(document, connection, handlers);
    }


    /**
     * Returns the content of a field.
     * <p/>
     * This implementation uses the 'FieldCode' property value if the field
     * is a user field that hasn't been changed.
     *
     * @param field the field
     * @return the field content
     * @throws OpenOfficeException if the field cannot be accessed
     */
    @Override
    protected String getContent(Field field) {
        if (!isInputField(field.getPropertySet()) && !field.isChanged()) {
            // use the original value derived from the FieldCode property
            return field.getValue();
        }
        return super.getContent(field);
    }

    /**
     * Returns the user text fields.
     * <p/>
     * This implementation uses the 'FieldCode' property as the field value.
     *
     * @return the user text fields, keyed on name
     * @throws OpenOfficeException if the fields can't be accessed
     */
    @Override
    protected Map<String, Field> getUserTextFields() {
        Map<String, Field> result = new LinkedHashMap<>();
        XEnumeration fields = getTextFieldsEnumeration();
        if (fields != null) {
            int seed = 0;
            try {
                while (fields.hasMoreElements()) {
                    Object field = fields.nextElement();
                    if (isDatabaseField(field)) {
                        XPropertySet set = UnoRuntime.queryInterface(XPropertySet.class, field);
                        if (set != null) {
                            String name = "userField" + (++seed);
                            String fieldCode = getFieldCode(name, set);
                            if (!StringUtils.isEmpty(fieldCode) && !result.containsKey(fieldCode)) {
                                // if the field code is non empty, and is unique, use it as the name, otherwise use
                                // the generated name
                                name = fieldCode;
                            }
                            result.put(name, new Field(name, fieldCode, set));
                        }
                    }
                }
            } catch (Exception exception) {
                throw new OpenOfficeException(exception, FailedToGetUserFields);
            }
        }
        return result;
    }

    /**
     * Helper to return the property set of a field, if the supplied field is an input field.
     *
     * @param field the field
     * @return the input field's property set, or {@code null} if it isn't an input field
     */
    @Override
    protected XPropertySet getInputFieldPropertySet(Object field) {
        return isInputField(field) ? UnoRuntime.queryInterface(XPropertySet.class, field) : null;
    }

    /**
     * Returns the value of the FieldCode property.
     *
     * @param name the field name
     * @param set  the property set
     * @return the value of the FieldCode property
     */
    protected String getFieldCode(String name, XPropertySet set) {
        try {
            String value = (String) set.getPropertyValue("FieldCode");
            value = value.replaceAll("(MERGEFIELD|MERGEFORMAT|\\*|\\\\)", "").trim();
            return value;
        } catch (Exception exception) {
            throw new OpenOfficeException(exception, FailedToGetField, name);
        }
    }

    /**
     * Determines if a field is a database (or merge) field .
     *
     * @param field the field
     * @return {@code true} if the field is a database field
     */
    private boolean isDatabaseField(Object field) {
        XServiceInfo info = UnoRuntime.queryInterface(XServiceInfo.class, field);
        return info != null && info.supportsService("com.sun.star.text.TextField.Database");
    }

}
