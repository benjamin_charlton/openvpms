/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import org.apache.commons.collections4.ComparatorUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.apache.commons.collections4.comparators.TransformingComparator;
import org.apache.commons.jxpath.Functions;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.NodeResolver;
import org.openvpms.component.business.service.archetype.helper.PropertyResolverException;
import org.openvpms.component.business.service.archetype.helper.sort.IMObjectSorter;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.report.Parameters;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


/**
 * Implementation of the {@code JRDataSource} interface, for collections of {@link IMObject}s.
 *
 * @author Tim Anderson
 */
public class IMObjectCollectionDataSource extends AbstractDataSource implements JRRewindableDataSource {

    /**
     * The collection.
     */
    private final Iterable<IMObject> collection;

    /**
     * The collection iterator.
     */
    private Iterator<IMObject> iterator;

    /**
     * The current object.
     */
    private IMObjectDataSource current;


    /**
     * Constructs a {@link IMObjectCollectionDataSource} for a collection of objects.
     *
     * @param objects    the objects
     * @param parameters the report parameters. May be {@code null}
     * @param fields     additional report fields. These override any in the report. May be {@code null}
     * @param reportName the report name, for error reporting purposes
     * @param service    the archetype service
     * @param lookups    the lookup service
     * @param handlers   the document handlers
     * @param functions  the JXPath extension functions
     */
    public IMObjectCollectionDataSource(Iterable<IMObject> objects, Parameters parameters, PropertySet fields,
                                        String reportName, IArchetypeService service, ILookupService lookups,
                                        DocumentHandlers handlers, Functions functions) {
        this(objects, new ReportContext(parameters, fields, reportName, service, lookups, handlers, functions));
    }

    /**
     * Constructs a {@link IMObjectCollectionDataSource} for a collection of objects.
     *
     * @param objects the objects
     * @param context the report context
     */
    public IMObjectCollectionDataSource(Iterable<IMObject> objects, ReportContext context) {
        super(context);
        collection = objects;
        iterator = collection.iterator();
    }

    /**
     * Constructs a {@link IMObjectCollectionDataSource} for a collection node.
     *
     * @param parent     the parent object
     * @param descriptor the collection descriptor
     * @param context    the report context
     * @param sortNodes  the sort nodes
     */
    protected IMObjectCollectionDataSource(IMObject parent, NodeDescriptor descriptor, ReportContext context,
                                           String... sortNodes) {
        super(context);
        List<IMObject> values = descriptor.getChildren(parent);
        if (sortNodes.length > 0) {
            sort(values, sortNodes);
        } else {
            values.sort(IMObjectSorter.getIdComparator());
        }
        collection = values;
        iterator = collection.iterator();
    }

    /**
     * Tries to position the cursor on the next element in the data source.
     *
     * @return true if there is a next record, false otherwise
     */
    public boolean next() {
        boolean result = iterator.hasNext();
        if (result) {
            current = new IMObjectDataSource(iterator.next(), getContext());
        }
        return result;
    }

    /**
     * Returns a data source for a collection node.
     *
     * @param name      the collection node name
     * @param sortNodes the list of nodes to sort on
     * @throws JRException for any error
     */
    public JRRewindableDataSource getDataSource(String name, String[] sortNodes) throws JRException {
        return current.getDataSource(name, sortNodes);
    }

    /**
     * Returns a data source for the given jxpath expression.
     *
     * @param expression the expression. Must return an {@code Iterable} or {@code Iterator} returning {@code IMObjects}
     * @return the data source
     * @throws JRException for any error
     */
    @Override
    public JRRewindableDataSource getExpressionDataSource(String expression) throws JRException {
        return current.getExpressionDataSource(expression);
    }

    /**
     * Gets the field value for the current position.
     *
     * @return an object containing the field value. The object type must be the field object type.
     * @throws JRException for any error
     */
    public Object getFieldValue(JRField field) throws JRException {
        return getValue(field);
    }

    /**
     * Evaluates an xpath expression.
     *
     * @param expression the expression
     * @return the result of the expression. May be {@code null}
     */
    public Object evaluate(String expression) {
        return current != null ? current.evaluate(expression) : null;
    }

    /**
     * Evaluates an xpath expression against an object.
     *
     * @param object     the object
     * @param expression the expression
     * @return the result of the expression. May be {@code null}
     */
    @Override
    public Object evaluate(Object object, String expression) {
        return current != null ? current.evaluate(object, expression) : null;
    }

    /**
     * Moves back to the first element in the data source.
     */
    @Override
    public void moveFirst() {
        iterator = collection.iterator();
    }

    /**
     * Gets the field value for the current position.
     * <p/>
     * Implementers of this method aren't required to do any exception handling or verifying that the value is of
     * the correct type.
     *
     * @param field the field
     * @return an object containing the field value
     * @throws JRException for any error
     */
    @Override
    protected Object getValue(JRField field) throws JRException {
        return (current != null) ? current.getFieldValue(field) : null;
    }

    /**
     * Sorts a list of IMObjects on a node in the form specified by {@link NodeResolver}.
     * <p/>
     * A final implicit sort on the object id is added to guarantee reproducibility (where there are no duplicates).
     *
     * @param objects  the objects to sort
     * @param sortNodes the nodes to sort on
     */
    private void sort(List<IMObject> objects, String[] sortNodes) {
        Comparator<Comparable<Object>> comparator = ComparatorUtils.naturalComparator();
        comparator = ComparatorUtils.nullLowComparator(comparator);

        ReportContext context = getContext();
        ComparatorChain<IMObject> chain = new ComparatorChain<>();
        for (String sortNode : sortNodes) {
            NodeTransformer transformer = new NodeTransformer(sortNode, context.getArchetypeService(),
                                                              context.getLookupService());
            TransformingComparator<IMObject, Comparable<Object>> transComparator
                    = new TransformingComparator<>(transformer, comparator);
            chain.addComparator(transComparator);
        }
        chain.addComparator(IMObjectSorter.getIdComparator());
        objects.sort(chain);
    }

    private static class NodeTransformer implements Transformer<IMObject, Comparable<Object>> {

        /**
         * The field name.
         */
        private final String name;

        /**
         * The archetype service.
         */
        private final IArchetypeService service;

        /**
         * The lookup service.
         */
        private final ILookupService lookups;

        /**
         * Constructs a {@code NodeTransformer}.
         *
         * @param name    the field name
         * @param service the archetype service
         * @param lookups the lookup service
         */
        public NodeTransformer(String name, IArchetypeService service, ILookupService lookups) {
            this.name = name;
            this.service = service;
            this.lookups = lookups;
        }

        /**
         * Transforms the input object (leaving it unchanged) into some output
         * object.
         *
         * @param input the object to be transformed, should be left unchanged
         */
        @SuppressWarnings("unchecked")
        public Comparable<Object> transform(IMObject input) {
            Comparable<Object> result = null;
            NodeResolver resolver = new NodeResolver(input, service, lookups);
            try {
                Object object = resolver.getObject(name);
                if (object instanceof Comparable) {
                    result = (Comparable<Object>) object;
                }
            } catch (PropertyResolverException ignore) {
                // node node found
            }
            return result;
        }
    }

}
