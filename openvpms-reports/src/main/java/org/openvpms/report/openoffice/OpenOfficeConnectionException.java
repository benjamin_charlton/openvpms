/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

/**
 * An exception thrown when OpenOffice cannot be connected to.
 *
 * @author Tim Anderson
 */
public class OpenOfficeConnectionException extends OpenOfficeException {

    /**
     * Constructs an  {@link OpenOfficeConnectionException}.
     *
     * @param args  a list of arguments to format the error message with
     */
    public OpenOfficeConnectionException(Object... args) {
        super(ErrorCode.FailedToConnect, args);
    }

    /**
     * Constructs an  {@link OpenOfficeConnectionException}.
     *
     * @param cause the cause of the exception
     * @param args  a list of arguments to format the error message with
     */
    public OpenOfficeConnectionException(Throwable cause, Object... args) {
        super(cause, ErrorCode.FailedToConnect, args);
    }
}
