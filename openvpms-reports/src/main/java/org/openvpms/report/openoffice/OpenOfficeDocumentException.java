/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

/**
 * OpenOffice document exception.
 *
 * @author Tim Anderson
 */
public class OpenOfficeDocumentException extends OpenOfficeException {

    /**
     * Constructs an {@link OpenOfficeDocumentException}.
     *
     * @param errorCode the error code
     * @param args      a list of arguments to format the error message with
     */
    public OpenOfficeDocumentException(ErrorCode errorCode, Object... args) {
        super(errorCode, args);
    }

    /**
     * Constructs an {@link OpenOfficeDocumentException}.
     *
     * @param cause     the cause of the exception
     * @param errorCode the error code
     * @param args      a list of arguments to format the error message with
     */
    public OpenOfficeDocumentException(Throwable cause, ErrorCode errorCode, Object... args) {
        super(cause, errorCode, args);
    }
}
