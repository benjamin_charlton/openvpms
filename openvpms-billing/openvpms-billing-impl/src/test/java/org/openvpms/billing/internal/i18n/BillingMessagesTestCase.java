/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.i18n;

import org.junit.Test;
import org.openvpms.component.i18n.Message;
import org.openvpms.component.math.Weight;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link BillingMessages} class.
 *
 * @author Tim Anderson
 */
public class BillingMessagesTestCase {

    /**
     * Tests the {@link BillingMessages#valueRequired(String)} method.
     */
    @Test
    public void testValueRequired() {
        check("BILLING-0001: A value is required for foo", BillingMessages.valueRequired("foo"));
    }

    /**
     * Tests the {@link BillingMessages#customerNotOwner(String, String)} method.
     */
    @Test
    public void testCustomerNotOwner() {
        check("BILLING-0002: foo is not the owner of bar", BillingMessages.customerNotOwner("foo", "bar"));
    }

    /**
     * Tests the {@link BillingMessages#templateExpansionGeneratedNoItems(String, Weight)} method.
     */
    @Test
    public void testTemplateExpansionGeneratedNoItems() {
        check("BILLING-0003: foo has no products for the specified patient weight",
              BillingMessages.templateExpansionGeneratedNoItems("foo", Weight.ZERO));
    }

    /**
     * Tests the {@link BillingMessages#weightRequired(String, String)} method.
     */
    @Test
    public void testWeightRequired() {
        check("BILLING-0004: The bar included by foo requires a patient weight, but no weight has been given.",
              BillingMessages.weightRequired("foo", "bar"));
    }

    /**
     * Tests the {@link BillingMessages#templateProductNotAvailableAtLocation} method.
     */
    @Test
    public void testTemplateProductNotAvailableAtLocation() {
        check("BILLING-0005: The bar included by foo is not available at clinic",
              BillingMessages.templateProductNotAvailableAtLocation("foo", "bar", "clinic"));
    }

    /**
     * Tests the {@link BillingMessages#recursiveTemplateExpansion(String, String, List)} method.
     */
    @Test
    public void testRecursiveTemplateExpansion() {
        check("BILLING-0006: Failed to expand product template top.\n" +
              "\n" +
              "The template nested1 is included recursively:\n" +
              "top, nested1, nested2", BillingMessages.recursiveTemplateExpansion(
                      "top", "nested1", Arrays.asList("top", "nested1", "nested2")));
    }

    /**
     * Verifies a message matches that expected.
     *
     * @param expected the expected message
     * @param actual   the actual message
     */
    private void check(String expected, Message actual) {
        assertEquals(expected, actual.toString());
    }

}
