/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.i18n;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.component.math.Weight;

import java.util.List;

/**
 * Messages reported by the billing API.
 *
 * @author Tim Anderson
 */
public class BillingMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("BILLING", BillingMessages.class.getName());

    /**
     * Returns a message indicating that a value is required but not present.
     *
     * @param name the value name
     * @return a new message
     */
    public static Message valueRequired(String name) {
        return messages.create(1, name);
    }

    /**
     * Returns a message indicating that a customer is not the owner of a patient.
     *
     * @param customer the customer name
     * @param patient  the patient name
     * @return a new message
     */
    public static Message customerNotOwner(String customer, String patient) {
        return messages.create(2, customer, patient);
    }

    /**
     * Returns a message indicating that a template expansion generated no line items for the given patient weight.
     *
     * @param template the template
     * @param weight   the patient weight
     * @return a new message
     */
    public static Message templateExpansionGeneratedNoItems(String template, Weight weight) {
        return messages.create(3, template, weight.getWeight());
    }

    /**
     * Returns a message indicating that the patient must have a weight recorded when charging a product
     * expanded from a template.
     *
     * @param product  the product
     * @param template the template
     * @return a new message
     */
    public static Message weightRequired(String product, String template) {
        return messages.create(4, product, template);
    }

    /**
     * Returns a message indicating that a product expanded from a template is not available at a practice location.
     *
     * @param product  the product
     * @param template the template
     * @param location the location
     * @return a new message
     */
    public static Message templateProductNotAvailableAtLocation(String product, String template, String location) {
        return messages.create(5, product, template, location);
    }

    /**
     * Returns a message indicating that a template is included recursively.
     *
     * @param template       the parent template
     * @param nestedTemplate the included template
     * @param names          the list of parent templates names between the parent and the nested template
     * @return a new message
     */
    public static Message recursiveTemplateExpansion(String template, String nestedTemplate, List<String> names) {
        return messages.create(6, template, nestedTemplate, StringUtils.join(names, ", "));
    }

}
