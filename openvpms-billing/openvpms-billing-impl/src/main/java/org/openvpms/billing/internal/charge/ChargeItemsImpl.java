/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.billing.charge.ChargeBuilder;
import org.openvpms.billing.charge.ChargeItemBuilder;
import org.openvpms.billing.charge.ChargeItems;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Default implementation of {@link ChargeItems}.
 *
 * @author Tim Anderson
 */
public class ChargeItemsImpl<C extends Charge<I>, I extends ChargeItem, CB extends ChargeBuilder<C, I, CB, IB>,
        IB extends ChargeItemBuilder<C, I, CB, IB>>
        implements ChargeItems<C, I, CB, IB> {

    /**
     * The items.
     */
    private final List<IB> items;

    /**
     * Constructs a {@link ChargeItemsImpl}.
     *
     * @param items the items
     */
    public ChargeItemsImpl(List<IB> items) {
        this.items = items;
    }

    /**
     * Returns a builder for the item with the specified product.
     *
     * @param product the product
     * @return an {@code Optional} containing the first builder with matching product, or an empty {@code Optional} if
     * none is found
     */
    @Override
    public Optional<IB> getItem(Product product) {
        return items.stream()
                .filter(item -> Objects.equals(product, item.getProduct()))
                .findFirst();
    }

    /**
     * Returns the item builders.
     *
     * @return the item builders
     */
    @Override
    public List<IB> getItems() {
        return items;
    }
}