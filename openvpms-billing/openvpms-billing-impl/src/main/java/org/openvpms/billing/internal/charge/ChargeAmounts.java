/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.patient.Patient;

import java.math.BigDecimal;
import java.util.Date;

import static java.math.BigDecimal.ZERO;

/**
 * Calculates charge amounts for a customer patient product and quantity, for a given date.
 * <p/>
 * This takes into account any service ratio that may be applicable for the product.
 *
 * @author Tim Anderson
 */
class ChargeAmounts {

    /**
     * The service ratio.
     */
    private final BigDecimal serviceRatio;

    /**
     * The fixed cost.
     */
    private final BigDecimal fixedCost;

    /**
     * The fixed price.
     */
    private final BigDecimal fixedPrice;

    /**
     * The unit cost.
     */
    private final BigDecimal unitCost;

    /**
     * The unit price.
     */
    private final BigDecimal unitPrice;

    /**
     * The discount.
     */
    private final BigDecimal discount;

    /**
     * The total amount, tax-inclusive.
     */
    private final BigDecimal total;

    /**
     * The tax amount.
     */
    private final BigDecimal tax;

    /**
     * Constructs a {@link ChargeAmounts}.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param product  the product
     * @param quantity the quantity
     * @param date     the date to determine amounts for
     * @param context  the pricing context
     */
    public ChargeAmounts(Customer customer, Patient patient, Product product, BigDecimal quantity, Date date,
                         PricingContext context) {
        serviceRatio = context.getServiceRatio(product, date);

        ProductPrice fixedProductPrice = context.getFixedPrice(product, date);
        fixedCost = getCost(fixedProductPrice, context);
        fixedPrice = getPrice(product, customer, fixedProductPrice, serviceRatio, context);

        ProductPrice unitProductPrice = context.getUnitPrice(product, date);
        unitCost = getCost(unitProductPrice, context);
        unitPrice = getPrice(product, customer, unitProductPrice, serviceRatio, context);

        BigDecimal maxFixedPriceDiscount = getMaxDiscount(fixedProductPrice, context);
        BigDecimal maxUnitPriceDiscount = getMaxDiscount(unitProductPrice, context);
        discount = context.getDiscount(customer, patient, product, fixedCost, fixedPrice, maxFixedPriceDiscount,
                                       unitCost, unitPrice, maxUnitPriceDiscount, quantity, date);

        total = MathRules.calculateTotal(fixedPrice, unitPrice, quantity, discount, 2);
        tax = context.getTax(total, product, customer);
    }

    /**
     * Returns the service ratio.
     *
     * @return the service ratio, or {@code null} if no service ratio applies
     */
    public BigDecimal getServiceRatio() {
        return serviceRatio;
    }

    /**
     * Returns the fixed cost.
     *
     * @return the fixed cost
     */
    public BigDecimal getFixedCost() {
        return fixedCost;
    }

    /**
     * Returns the fixed price.
     *
     * @return the fixed price
     */
    public BigDecimal getFixedPrice() {
        return fixedPrice;
    }

    /**
     * Returns the unit cost.
     *
     * @return the unit cost
     */
    public BigDecimal getUnitCost() {
        return unitCost;
    }

    /**
     * Returns the unit price.
     *
     * @return the unit price
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * Returns the discount,
     *
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Returns the total amount, tax-inclusive.
     *
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Returns the tax amount.
     *
     * @return the tax
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * Returns the cost of a product.
     *
     * @param price   the product price. May be {@code null}
     * @param context the pricing context
     * @return the cost
     */
    private BigDecimal getCost(ProductPrice price, PricingContext context) {
        return price != null ? context.getCost(price) : ZERO;
    }

    /**
     * Returns the tax-inclusive price given a tax-exclusive price and service ratio.
     * <p>
     * This takes into account customer tax exclusions.
     *
     * @param product      the product
     * @param customer     the customer
     * @param productPrice the product price. May be {@code null}
     * @param serviceRatio the service ratio
     * @param context      the pricing context
     * @return the tax-inclusive price, rounded according to the practice currency conventions
     */
    private BigDecimal getPrice(Product product, Customer customer, ProductPrice productPrice, BigDecimal serviceRatio,
                                PricingContext context) {
        BigDecimal result = ZERO;
        if (productPrice != null) {
            result = context.getPrice(product, customer, productPrice, serviceRatio);
        }
        return result;
    }

    /**
     * Determines the maximum discount for a price.
     *
     * @param price   the price
     * @param context the pricing context
     * @return the maximum discount
     */
    private BigDecimal getMaxDiscount(ProductPrice price, PricingContext context) {
        return price != null ? context.getMaxDiscount(price) : ProductPriceRules.DEFAULT_MAX_DISCOUNT;
    }

}