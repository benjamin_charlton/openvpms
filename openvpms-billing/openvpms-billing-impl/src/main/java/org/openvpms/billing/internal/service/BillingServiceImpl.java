/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.service;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.billing.charge.InvoiceBuilder;
import org.openvpms.billing.internal.charge.BuilderServices;
import org.openvpms.billing.internal.charge.InvoiceBuilderImpl;
import org.openvpms.billing.service.BillingService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.internal.query.IdQuery;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;

import java.math.BigDecimal;

/**
 * Default implementation of {@link BillingService}.
 *
 * @author Tim Anderson
 */
public class BillingServiceImpl implements BillingService {

    /**
     * The builder services.
     */
    private final BuilderServices services;

    /**
     * Helper to query charges by external id.
     */
    private final IdQuery query;


    /**
     * Constructs a {@link BillingServiceImpl}.
     *
     * @param services the builder services
     */
    public BillingServiceImpl(BuilderServices services) {
        this.services = services;
        query = new IdQuery(services.getArchetypeService());
    }

    /**
     * Returns an invoice given its identifier.
     *
     * @param id the invoice id
     * @return the invoice, or {@code null} if none is found
     */
    @Override
    public Invoice getInvoice(long id) {
        return services.getDomainService().get(CustomerAccountArchetypes.INVOICE, id, Invoice.class);
    }

    /**
     * Returns an invoice given an external id.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.*</em> prefix.
     * @param id        the identifier
     * @return the invoice or {@code null} if none is found
     */
    @Override
    public Invoice getInvoice(String archetype, String id) {
        Act act = query.getObject(CustomerAccountArchetypes.INVOICE, Act.class, archetype, id);
        return (act != null) ? services.getDomainService().create(act, Invoice.class) : null;
    }

    /**
     * Returns the id of an invoice given an external identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.*</em> prefix.
     * @param id        the identifier
     * @return the corresponding invoice id, or {@code -1} if none is found
     */
    @Override
    public long getInvoiceId(String archetype, String id) {
        Long result = query.getId(CustomerAccountArchetypes.INVOICE, Act.class, archetype, id);
        return result != null ? result : -1;
    }

    /**
     * Invoices a product to a customer.
     * <p/>
     * If there is an existing invoice for the customer, the product will be added to that, else a new invoice will
     * be created.
     *
     * @param customer  the customer
     * @param patient   the patient
     * @param product   the product. If it is a template product, it will be expanded
     * @param quantity  the quantity to invoice
     * @param location  the practice location where the product is being invoiced
     * @param clinician the responsible clinician. Required for medication products, optional otherwise.
     * @return the invoice
     */
    @Override
    public Invoice invoice(Customer customer, Patient patient, Product product, BigDecimal quantity, Location location,
                           User clinician) {
        return getInvoiceBuilder()
                .customer(customer)
                .clinician(clinician)
                .location(location)
                .newItem()
                .patient(patient)
                .product(product)
                .quantity(quantity)
                .clinician(clinician)
                .add()
                .build();
    }

    /**
     * Returns a builder to invoice a customer.
     * <p/>
     * If there is an existing invoice for the customer, items will be added to that, else a new invoice will be
     * created.
     *
     * @return an invoice builder
     */
    @Override
    public InvoiceBuilder getInvoiceBuilder() {
        return new InvoiceBuilderImpl(services);
    }

}
