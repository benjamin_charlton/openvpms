/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.billing.charge.ChargeBuilder;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Services required by {@link ChargeBuilder} implementations.
 *
 * @author Tim Anderson
 */
public class BuilderServices {

    /**
     * THe customer account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The medical record rules.
     */
    private final MedicalRecordRules medicalRecordRules;

    /**
     * The product rules.
     */
    private final ProductRules productRules;

    /**
     * The reminder rules.
     */
    private final ReminderRules reminderRules;

    /**
     * The stock rules.
     */
    private final StockRules stockRules;

    /**
     * The pricing context factory.
     */
    private final PricingContextFactory pricingContextFactory;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs a {@link BuilderServices}.
     *
     * @param accountRules          the customer account rules
     * @param patientRules          the patient rules
     * @param medicalRecordRules    the medical record rules
     * @param reminderRules         the reminder rules
     * @param stockRules            the stock rules
     * @param pricingContextFactory the pricing context factory
     * @param service               the archetype service
     * @param domainService         the domain service
     * @param transactionManager    the transaction manager
     */
    public BuilderServices(CustomerAccountRules accountRules, PatientRules patientRules, ProductRules productRules,
                           MedicalRecordRules medicalRecordRules, ReminderRules reminderRules, StockRules stockRules,
                           PricingContextFactory pricingContextFactory, ArchetypeService service,
                           DomainService domainService, PlatformTransactionManager transactionManager) {
        this.accountRules = accountRules;
        this.patientRules = patientRules;
        this.productRules = productRules;
        this.medicalRecordRules = medicalRecordRules;
        this.reminderRules = reminderRules;
        this.stockRules = stockRules;
        this.pricingContextFactory = pricingContextFactory;
        this.service = service;
        this.domainService = domainService;
        this.transactionManager = transactionManager;
    }

    /**
     * Returns the customer account rules.
     *
     * @return the customer account rules
     */
    public CustomerAccountRules getCustomerAccountRules() {
        return accountRules;
    }

    /**
     * Returns the patient rules.
     *
     * @return the patient rules
     */
    public PatientRules getPatientRules() {
        return patientRules;
    }

    /**
     * Returns the product rules.
     *
     * @return the product rules
     */
    public ProductRules getProductRules() {
        return productRules;
    }

    /**
     * Returns the medical record rules.
     *
     * @return the medical record rules
     */
    public MedicalRecordRules getMedicalRecordRules() {
        return medicalRecordRules;
    }

    /**
     * The reminder rules.
     *
     * @return the reminder rules
     */
    public ReminderRules getReminderRules() {
        return reminderRules;
    }

    /**
     * Returns the stock rules.
     *
     * @return the stock rules.
     */
    public StockRules getStockRules() {
        return stockRules;
    }

    /**
     * Returns the pricing context factory.
     *
     * @return the pricing context factory
     */
    public PricingContextFactory getPricingContextFactory() {
        return pricingContextFactory;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    public ArchetypeService getArchetypeService() {
        return service;
    }

    /**
     * Returns the domain service.
     *
     * @return the domain service
     */
    public DomainService getDomainService() {
        return domainService;
    }

    /**
     * Returns the transaction manager.
     *
     * @return the transaction manager
     */
    public PlatformTransactionManager getTransactionManager() {
        return transactionManager;
    }

}