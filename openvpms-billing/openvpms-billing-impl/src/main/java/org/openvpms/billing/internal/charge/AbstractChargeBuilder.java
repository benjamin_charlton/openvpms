/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Base charge builder.
 *
 * @author Tim Anderson
 */
public abstract class AbstractChargeBuilder {

    /**
     * The archetype being built.
     */
    private final String archetype;

    /**
     * The build services.
     */
    private final BuilderServices services;

    /**
     * Constructs an {@link AbstractChargeBuilder}.
     *
     * @param archetype the charge archetype
     * @param services  the build services
     */
    public AbstractChargeBuilder(String archetype, BuilderServices services) {
        this.archetype = archetype;
        this.services = services;
    }

    /**
     * Returns the charge to update.
     * <p/>
     * This implementation returns a new instance.
     *
     * @return the charge
     */
    protected FinancialAct getObject() {
        return create(archetype, FinancialAct.class);
    }

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @param type      the expected type of the object
     * @return a new object
     */
    protected <T extends IMObject> T create(String archetype, Class<T> type) {
        return getArchetypeService().create(archetype, type);
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return getArchetypeService().getBean(object);
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getArchetypeService() {
        return services.getArchetypeService();
    }

    /**
     * Returns the build service.
     *
     * @return the services
     */
    protected BuilderServices getServices() {
        return services;
    }
}
