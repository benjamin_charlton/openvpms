/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.component.business.service.archetype.functor.ActComparator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.patient.Patient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Invoice state to share between invoice builders.
 *
 * @author Tim Anderson
 */
class InvoiceContext extends BuildContext {

    /**
     * The reminder rules.
     */
    private final ReminderRules reminderRules;

    /**
     * The patient history changes.
     */
    private final PatientHistoryChanges historyChanges;

    /**
     * The reminders.
     */
    private final List<Act> reminders = new ArrayList<>();

    /**
     * The alerts.
     */
    private final List<Act> alerts = new ArrayList<>();

    /**
     * Constructs a {@link BuildContext}.
     *
     * @param charge         the charge being built
     * @param pricingContext the pricing context
     * @param service        the archetype service
     * @param reminderRules  the reminder rules
     */
    public InvoiceContext(IMObjectBean charge, PricingContext pricingContext, ArchetypeService service,
                          ReminderRules reminderRules) {
        super(charge, pricingContext, service);
        this.reminderRules = reminderRules;
        historyChanges = new PatientHistoryChanges(pricingContext.getLocation(), service);
    }

    /**
     * Adds an object that has changed.
     *
     * @param object the object
     */
    @Override
    public void addChange(IMObject object) {
        super.addChange(object);
    }

    /**
     * Adds a record to patient history.
     * <p/>
     * This links the record and invoice item into the patient history.
     *
     * @param record the record
     * @param item   the invoice item
     * @param date   the date, used to select the visit
     */
    public void addRecord(Act record, FinancialAct item, Date date) {
        addChange(record);
        historyChanges.addToEvents(Arrays.asList(record, item), date);
    }

    /**
     * Adds a record to patient history.
     * <p/>
     * This links the record and invoice item into the patient history.
     *
     * @param record the record
     * @param date   the date, used to select the visit
     */
    public void addRecord(Act record, Date date) {
        addChange(record);
        historyChanges.addToEvents(Collections.singletonList(record), date);
    }

    /**
     * Adds a reminder.
     *
     * @param reminder the reminder
     */
    public void addReminder(Act reminder) {
        reminders.add(reminder);
        addChange(reminder);
    }

    /**
     * Adds an alert.
     *
     * @param alert the alert
     */
    public void addAlert(Act alert) {
        alerts.add(alert);
        addChange(alert);
    }

    /**
     * Saves changes.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void save() {
        super.save();
        historyChanges.save();
        if (!reminders.isEmpty()) {
            reminderRules.markMatchingRemindersCompleted((List)reminders); // TODO - remove cast in 2.4
        }
        if (!alerts.isEmpty()) {
            reminderRules.markMatchingAlertsCompleted((List) alerts); // TODO - remove cast in 2.4
        }
    }

    /**
     * Returns the most recent visit for a patient that was updated during the build.
     *
     * @param patient the patient
     * @return the most recent visit for the patient. May be {@code null}
     */
    public Act getVisit(Patient patient) {
        Act result = null;
        List<Act> visits = historyChanges.getEvents(patient.getObjectReference());
        if (visits.size() > 1) {
            visits.sort(ActComparator.descending());
        }
        if (!visits.isEmpty()) {
            result = visits.get(0);
        }
        return result;
    }

    /**
     * Returns all visits that were updated during the build.
     *
     * @return the visits
     */
    public List<Act> getVisits() {
        return historyChanges.getEvents();
    }
}
