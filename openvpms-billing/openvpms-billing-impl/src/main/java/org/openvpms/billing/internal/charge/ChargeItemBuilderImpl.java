/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.billing.charge.ChargeBuilder;
import org.openvpms.billing.charge.ChargeItemBuilder;
import org.openvpms.billing.exception.BillingException;
import org.openvpms.billing.internal.i18n.BillingMessages;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.product.BaseProduct;
import org.openvpms.domain.product.Template;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Default implementation of {@link ChargeItemBuilder}.
 *
 * @author Tim Anderson
 */
public class ChargeItemBuilderImpl<C extends Charge<I>, I extends ChargeItem,
        CB extends ChargeBuilder<C, I, CB, IB>, IB extends ChargeItemBuilder<C, I, CB, IB>>
        extends AbstractChargeBuilder
        implements ChargeItemBuilder<C, I, CB, IB> {

    /**
     * The parent charge builder.
     */
    private final ChargeBuilderImpl<C, I, CB, IB> parent;

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * The product.
     */
    private Product product;

    /**
     * The template.
     */
    private Template template;

    /**
     * The template expansion group.
     */
    private int group;

    /**
     * Determines if the item should be printed. Only applies to template items.
     */
    private boolean print;

    /**
     * The quantity.
     */
    private BigDecimal quantity;

    /**
     * The minimum quantity, if this item was generated from a template.
     */
    private BigDecimal minQuantity;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Patient node.
     */
    private static final String PATIENT = "patient";

    /**
     * Product node.
     */
    private static final String PRODUCT = "product";

    /**
     * Stock location node.
     */
    private static final String STOCK_LOCATION = "stockLocation";

    /**
     * Quantity node.
     */
    private static final String QUANTITY = "quantity";

    /**
     * Clinician node.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Fixed cost node.
     */
    private static final String FIXED_COST = "fixedCost";

    /**
     * Fixed price node.
     */
    private static final String FIXED_PRICE = "fixedPrice";

    /**
     * Unit cost node.
     */
    private static final String UNIT_COST = "unitCost";

    /**
     * Unit price node.
     */
    private static final String UNIT_PRICE = "unitPrice";

    /**
     * Service ratio node.
     */
    private static final String SERVICE_RATIO = "serviceRatio";

    /**
     * Discount node.
     */
    private static final String DISCOUNT = "discount";

    /**
     * Total node.
     */
    private static final String TOTAL = "total";

    /**
     * Tax node.
     */
    private static final String TAX = "tax";

    /**
     * Template node.
     */
    private static final String TEMPLATE = "template";

    /**
     * Template expansion group node.
     */
    private static final String GROUP = "group";

    /**
     * Minimum quantity node.
     */
    private static final String MIN_QUANTITY = "minQuantity";

    /**
     * Print node.
     */
    private static final String PRINT = "print";


    /**
     * Constructs a {@link ChargeItemBuilder}.
     *
     * @param archetype the archetype being built
     * @param parent    the parent builder
     * @param services  the build services
     */
    public ChargeItemBuilderImpl(String archetype, ChargeBuilderImpl<C, I, CB, IB> parent, BuilderServices services) {
        super(archetype, services);
        this.parent = parent;
    }

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    @Override
    public Patient getPatient() {
        return patient;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    @Override
    public IB patient(Patient patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Returns the product.
     *
     * @return the product. May be {@code null}
     */
    @Override
    public Product getProduct() {
        return product;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    @Override
    public IB product(Product product) {
        // make sure the product is a domain object instance, as they are easier to work with
        if (product instanceof BaseProduct) {
            this.product = product;
        } else {
            this.product = getServices().getDomainService().create(product, BaseProduct.class);
        }

        // reset product related attributes.
        template = null;
        minQuantity = null;
        print = true;
        group = 0;
        return getThis();
    }

    /**
     * Sets the template, if this item was generated from a template.
     *
     * @param template    the template
     * @param minQuantity the minimum quantity
     * @param print       if {@code true}, print the item, else suppress it when reporting
     * @param group       the template expansion group
     * @return this
     */
    public IB template(Template template, BigDecimal minQuantity, boolean print, int group) {
        this.template = template;
        this.minQuantity = minQuantity;
        this.print = print;
        return group(group);
    }

    /**
     * Returns the template.
     *
     * @return the template, or {@code null} if this item wasn't generated from a template
     */
    public Template getTemplate() {
        return template;
    }

    /**
     * Sets the template expansion group.
     *
     * @param group the group
     * @return this
     */
    public IB group(int group) {
        this.group = group;
        return getThis();
    }

    /**
     * Returns the quantity.
     *
     * @return the quantity
     */
    @Override
    public BigDecimal getQuantity() {
        return quantity != null ? quantity : BigDecimal.ZERO;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    @Override
    public IB quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    @Override
    public IB quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return getThis();
    }

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public User getClinician() {
        return clinician;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    @Override
    public IB clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Add this item to the charge.
     * <p/>
     * If the product is a template, it will be expanded.
     *
     * @return the charge builder
     */
    @Override
    public CB add() {
        parent.add(getThis());
        return parent.getThis();
    }

    /**
     * Builds the charge item.
     *
     * @param date    the date for determining prices and discounts
     * @param context the build context
     * @return the charge item
     */
    public FinancialAct build(Date date, BuildContext context) {
        checkPreconditions();
        FinancialAct act = getObject();
        context.addChange(act);
        IMObjectBean bean = getBean(act);

        build(act, bean, date, context);
        return act;
    }

    /**
     * Verifies that preconditions are met.
     *
     * @throws BillingException if preconditions are not met
     */
    protected void checkPreconditions() {
        if (product == null) {
            throw new BillingException(BillingMessages.valueRequired(PRODUCT));
        }
        if (patient == null) {
            throw new BillingException(BillingMessages.valueRequired(PATIENT));
        }
        if (quantity == null) {
            throw new BillingException(BillingMessages.valueRequired(QUANTITY));
        }
        Reference owner = getServices().getPatientRules().getOwnerReference(patient);
        Customer customer = parent.getCustomer();
        if (!customer.getObjectReference().equals(owner)) {
            throw new BillingException(BillingMessages.customerNotOwner(customer.getName(), patient.getName()));
        }
    }

    /**
     * Builds the charge item.
     *
     * @param act     the charge act
     * @param bean    a bean wrapping the act
     * @param date    the date for determining prices and discounts
     * @param context the build context
     */
    protected void build(FinancialAct act, IMObjectBean bean, Date date, BuildContext context) {
        act.setActivityStartTime(date);
        bean.setTarget(PATIENT, patient);
        bean.setTarget(PRODUCT, product);
        if (product.isA(ProductArchetypes.MEDICATION, ProductArchetypes.MERCHANDISE)) {
            Party stockLocation = getServices().getStockRules().getStockLocation(product, parent.getLocation());
            if (stockLocation != null) {
                bean.setTarget(STOCK_LOCATION, stockLocation);
            } else {
                bean.removeValues(STOCK_LOCATION);
            }
        }

        bean.setValue(QUANTITY, quantity);

        if (clinician != null) {
            bean.setTarget(CLINICIAN, clinician);
        }
        ChargeAmounts prices = new ChargeAmounts(parent.getCustomer(), patient, product, quantity, date,
                                                 context.getPricingContext());

        bean.setValue(FIXED_COST, prices.getFixedCost());
        bean.setValue(FIXED_PRICE, prices.getFixedPrice());
        bean.setValue(UNIT_COST, prices.getUnitCost());
        bean.setValue(UNIT_PRICE, prices.getUnitPrice());
        bean.setValue(SERVICE_RATIO, prices.getServiceRatio());
        bean.setValue(DISCOUNT, prices.getDiscount());
        BigDecimal total = prices.getTotal();
        bean.setValue(TOTAL, total);
        bean.setValue(TAX, prices.getTax());

        if (template != null) {
            Relationship participation = bean.setTarget(TEMPLATE, template);
            IMObjectBean participationBean = bean.getBean(participation);
            participationBean.setValue(GROUP, group);
            bean.setValue(MIN_QUANTITY, minQuantity);
            if (!print && MathRules.isZero(total)) {
                bean.setValue(PRINT, false);
            }
        }
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected IB getThis() {
        return (IB) this;
    }

    /**
     * Returns the parent builder.
     *
     * @return the parent builder
     */
    protected ChargeBuilderImpl<C, I, CB, IB> getParent() {
        return parent;
    }
}