/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.billing.charge.InvoiceObjects;
import org.openvpms.component.model.act.Act;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link InvoiceObjects}.
 *
 * @author Tim Anderson
 */
class InvoiceObjectsImpl implements InvoiceObjects {

    /**
     * The build context.
     */
    private final InvoiceContext context;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * Constructs an {@link InvoiceObjectsImpl}.
     *
     * @param context       the build context
     * @param domainService the domain service
     */

    public InvoiceObjectsImpl(InvoiceContext context, DomainService domainService) {
        this.context = context;
        this.domainService = domainService;
    }

    /**
     * Returns the charge.
     *
     * @return the charge
     */
    @Override
    public Invoice getCharge() {
        return domainService.create(context.getCharge(), Invoice.class);
    }

    /**
     * Returns the new and updated charge items.
     * <p/>
     * Unmodified items are excluded.
     *
     * @return the charge items
     */
    @Override
    public List<InvoiceItem> getItems() {
        return context.getChanges(CustomerAccountArchetypes.INVOICE_ITEM)
                .stream().map(object -> domainService.create(object, InvoiceItem.class))
                .collect(Collectors.toList());
    }

    /**
     * Returns the patient visits that were updated.
     *
     * @return the patient visits
     */
    @Override
    public List<Visit> getVisits() {
        return context.getVisits()
                .stream().map(object -> domainService.create(object, Visit.class))
                .collect(Collectors.toList());
    }

    /**
     * Returns the most recent visit that was updated for a patient.
     *
     * @param patient the patient
     * @return an {@code Optional} containing the most recent visit, or an empty {@code Optional} if none is found
     */
    @Override
    public Optional<Visit> getVisit(Patient patient) {
        Act act = context.getVisit(patient);
        return (act != null) ? Optional.of(domainService.create(act, Visit.class)) : Optional.empty();
    }
}
