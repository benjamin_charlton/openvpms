/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.charge;

import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.product.Template;

import java.math.BigDecimal;

/**
 * A builder for {@link Charge} instances.
 *
 * @author Tim Anderson
 */
public interface ChargeBuilder<C extends Charge<I>, I extends ChargeItem, CB extends ChargeBuilder<C, I, CB, IB>,
        IB extends ChargeItemBuilder<C, I, CB, IB>> {

    /**
     * Returns the location.
     *
     * @return the location. May be {@code null}
     */
    Location getLocation();

    /**
     * Sets the location.
     *
     * @param location the location
     * @return this
     */
    CB location(Location location);

    /**
     * Returns the customer.
     *
     * @return the customer. May be {@code null}
     */
    Customer getCustomer();

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    CB customer(Customer customer);

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    User getClinician();

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    CB clinician(User clinician);

    /**
     * Adds an identity.
     * <p/>
     * The identity must be unique for the charge type.
     *
     * @param archetype the identity archetype
     * @param identity  the identity
     * @return this
     */
    CB addIdentity(String archetype, String identity);

    /**
     * Returns a builder to add a charge item.
     * <p/>
     * Use {@link ChargeItemBuilder#add()} to add the item.
     *
     * @return the builder
     */
    IB newItem();

    /**
     * Expands a template.
     * <p/>
     * All items will be added to the charge.
     *
     * @param template  the product template
     * @param patient   the patient
     * @param quantity  the template quantity
     * @param clinician the clinician
     * @return the charge items generated from the template
     */
    ChargeItems<C, I, CB, IB> expand(Template template, Patient patient, BigDecimal quantity, User clinician);

    /**
     * Returns the charge item builders, for new and updated items.
     *
     * @return the charge item builders
     */
    ChargeItems<C, I, CB, IB> getChangedItems();

    /**
     * Builds the charge.
     *
     * @return the charge
     */
    C build();

    /**
     * Builds the charge, and returns the built objects.
     *
     * @return the built objects
     */
    ChargeObjects<C, I> buildObjects();
}