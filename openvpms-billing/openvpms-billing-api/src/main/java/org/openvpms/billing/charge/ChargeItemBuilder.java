/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.charge;

import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;
import org.openvpms.domain.patient.Patient;

import java.math.BigDecimal;

/**
 * A builder for {@link ChargeItem} instances.
 *
 * @author Tim Anderson
 */
public interface ChargeItemBuilder<C extends Charge<I>, I extends ChargeItem,
        CB extends ChargeBuilder<C, I, CB, IB>, IB extends ChargeItemBuilder<C, I, CB, IB>> {

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    Patient getPatient();

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    IB patient(Patient patient);

    /**
     * Returns the product.
     *
     * @return the product. May be {@code null}
     */
    Product getProduct();

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    IB product(Product product);

    /**
     * Returns the quantity.
     *
     * @return the quantity
     */
    BigDecimal getQuantity();

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    IB quantity(int quantity);

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    IB quantity(BigDecimal quantity);

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    User getClinician();

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    IB clinician(User clinician);

    /**
     * Builds the item, and adds it to the charge.
     * <p/>
     * If the product is a template, it will be expanded.
     *
     * @return the charge builder
     */
    CB add();

}