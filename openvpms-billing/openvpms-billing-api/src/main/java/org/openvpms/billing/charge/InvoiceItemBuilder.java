/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.charge;

import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.product.Batch;

/**
 * Invoice item builder.
 *
 * @author Tim Anderson
 */
public interface InvoiceItemBuilder
        extends ChargeItemBuilder<Invoice, InvoiceItem, InvoiceBuilder, InvoiceItemBuilder> {

    /**
     * Returns the product batch.
     *
     * @return the product batch. May be {@code null}
     */
    Batch getBatch();

    /**
     * Sets the product batch.
     *
     * @param batch the batch
     * @return this
     */
    InvoiceItemBuilder batch(Batch batch);

}