/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.charge;

import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;

import java.util.List;

/**
 * Tracks the objects created and updated by a {@link ChargeBuilder}.
 *
 * @author Tim Anderson
 */
public interface ChargeObjects<C extends Charge<I>, I extends ChargeItem> {

    /**
     * Returns the charge.
     *
     * @return the charge
     */
    C getCharge();

    /**
     * Returns the new and updated charge items.
     * <p/>
     * Unmodified items are excluded.
     *
     * @return the charge items
     */
    List<I> getItems();

}
