/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.db.migration.R__ArchetypeLoader;
import org.openvpms.db.migration.R__PluginLoader;
import org.openvpms.db.service.DatabaseService;
import org.openvpms.db.service.impl.DatabaseServiceImpl;
import org.openvpms.db.service.impl.NoOpMigrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Base class for tests that verify changes to the migration checksum.
 *
 * @author Tim Anderson
 */
@ContextConfiguration(locations = {"/applicationContext.xml"})
public abstract class AbstractMigrationChecksumTest extends AbstractJUnit4SpringContextTests {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseService service;

    /**
     * The JDBC connection.
     */
    private Connection connection;

    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        connection = dataSource.getConnection();
        // required by 2.1.0.10 OVPMS-2233 migration
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
    }

    /**
     * Close the connection.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        if (connection != null) {
            connection.close();
        }
        connection = null;
    }

    /**
     * Verifies that if the V2_1_0_3 script has not yet run, the migration info will not be present, but will have
     * the correct checksum after migration has completed.
     *
     * @throws SQLException for any error
     */
    @Test
    public void testMigrate() throws SQLException {
        load(getPreMigrationDump());

        MigrationInfo preupdate = getMigrationInfo();
        assertNull(preupdate);

        assertTrue(service.needsUpdate());
        service.update(null, null);

        assertFalse(service.needsUpdate());

        // verify the checksum has updated
        MigrationInfo postupdate = getMigrationInfo();
        assertNotNull(postupdate);
        assertEquals(Integer.valueOf(getUpdatedMigrationChecksum()), postupdate.getChecksum());
    }

    /**
     * Verifies that the checksum is updated when the script has already been applied.
     *
     * @throws SQLException for any error
     */
    @Test
    public void testUpdateChecksum() throws SQLException {
        load(getPostMigrationDump());

        //verify that the old migration script has been applied.
        MigrationInfo preupdate = getMigrationInfo();
        assertNotNull(preupdate);
        assertEquals(Integer.valueOf(getOriginalMigrationChecksum()), preupdate.getChecksum());

        assertTrue(service.needsUpdate());
        service.update(null, null);

        assertFalse(service.needsUpdate());

        // verify the checksum has updated
        MigrationInfo postupdate = getMigrationInfo();
        assertNotNull(postupdate);
        assertEquals(Integer.valueOf(getUpdatedMigrationChecksum()), postupdate.getChecksum());
    }

    /**
     * Returns the migration info.
     *
     * @return the migration info, or {@code null} if it doesn't exist
     */
    protected MigrationInfo getMigrationInfo() {
        MigrationInfo result = null;
        try {
            R__ArchetypeLoader.setMigrator(new NoOpMigrator());
            R__PluginLoader.setMigrator(new NoOpMigrator());
            MigrationInfoService info = ((DatabaseServiceImpl) service).getInfo();
            String version = getMigrationVersion();
            String description = getMigrationDescription();
            for (MigrationInfo applied : info.applied()) {
                if (applied.getVersion().getVersion().equals(version)) {
                    assertEquals(description, applied.getDescription());
                    result = applied;
                    break;
                }
            }
        } finally {
            R__ArchetypeLoader.setMigrator(null);
        }
        return result;
    }

    /**
     * Returns the path to a dump prior to the migration.
     *
     * @return the resource path
     */
    protected abstract String getPreMigrationDump();

    /**
     * Returns the checksum of the original migration script.
     *
     * @return the checksum of the original migration script
     */
    protected abstract int getOriginalMigrationChecksum();

    /**
     * Returns the path to a dump done after the original migration.
     *
     * @return the resource path
     */
    protected abstract String getPostMigrationDump();

    /**
     * Returns the checksum of the updated migration script.
     *
     * @return the checksum of the updated migration script
     */
    protected abstract int getUpdatedMigrationChecksum();

    /**
     * Returns the migration version.
     *
     * @return the migration version
     */
    protected abstract String getMigrationVersion();

    /**
     * Returns the migration description.
     *
     * @return the migration description
     */
    protected abstract String getMigrationDescription();

    /**
     * Loads a database dump.
     *
     * @param dump the resource path to the dump
     */
    private void load(String dump) {
        DbSupport support = DbSupportFactory.createDbSupport(connection, true);
        ClassPathResource sql = new ClassPathResource(dump, getClass().getClassLoader());
        SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
        script.execute(support.getJdbcTemplate());
    }

}
