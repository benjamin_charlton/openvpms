/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.db.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the migration for OVPMS-2208 Fix entityLink.productStockLocation relationships where the
 * idealQty is < criticalQty.
 *
 * @author Tim Anderson
 */
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class V2_1_0_3__OVPMS_2208TestCase extends AbstractJUnit4SpringContextTests {

    /**
     * Stock location data.
     */
    public static class Data {

        private long productId;

        private long linkId;

        private String idealQty; // should be BigDecimal but MySQL Connector/J 8 loses precision

        private String criticalQty;

        public long getProductId() {
            return productId;
        }

        public void setProductId(long productId) {
            this.productId = productId;
        }

        public long getLinkId() {
            return linkId;
        }

        public void setLinkId(long linkId) {
            this.linkId = linkId;
        }

        public String getIdealQty() {
            return idealQty;
        }

        public void setIdealQty(String idealQty) {
            this.idealQty = idealQty;
        }

        public String getCriticalQty() {
            return criticalQty;
        }

        public void setCriticalQty(String criticalQty) {
            this.criticalQty = criticalQty;
        }
    }

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseService service;

    /**
     * The JDBC connection.
     */
    private Connection connection;

    /**
     * Open a connection to database for test execution statements
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        connection = dataSource.getConnection();
        DbSupport support = DbSupportFactory.createDbSupport(connection, true);

        // load up test database baselined on V2.0.0.6
        ClassPathResource sql = new ClassPathResource("db-V2.0.0.6__OVPMS-2208.sql", getClass().getClassLoader());
        SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
        script.execute(support.getJdbcTemplate());
    }

    /**
     * Close the connection.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        if (connection != null) {
            connection.close();
        }
        connection = null;
    }

    /**
     * Verifies that entityLink.productStockLocation instances that have criticalQty > idealQty are migrated correctly.
     * <p/>
     * Any quantities that exceed DECIMAL(18,3) will be converted to 999999999999999.999.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMigrate() throws Exception {
        List<Data> preMigrate = getData();
        assertEquals(30, preMigrate.size());
        check(preMigrate, 125, 57, BigDecimal.ZERO, BigDecimal.ZERO);
        check(preMigrate, 128, 58, BigDecimal.valueOf(10), BigDecimal.valueOf(5));
        check(preMigrate, 130, 60, BigDecimal.valueOf(10), BigDecimal.valueOf(10));
        check(preMigrate, 912, 1251, BigDecimal.valueOf(10),
              new BigDecimal("100000000000000007629769841091887003294964970946560.00"));
        check(preMigrate, 912, 1252, BigDecimal.valueOf(5), new BigDecimal("1000000000000000042420637374017961984.00"));
        check(preMigrate, 913, 1253, BigDecimal.valueOf(5), BigDecimal.valueOf(10));
        check(preMigrate, 913, 1255, BigDecimal.valueOf(6), BigDecimal.valueOf(12));

        service.update("2.1.0.3", null, null);

        List<Data> postMigrate = getData();
        assertEquals(30, postMigrate.size());
        check(postMigrate, 125, 57, BigDecimal.ZERO, BigDecimal.ZERO);
        check(postMigrate, 128, 58, BigDecimal.valueOf(10), BigDecimal.valueOf(5));
        check(postMigrate, 130, 60, BigDecimal.valueOf(10), BigDecimal.valueOf(10));
        check(postMigrate, 912, 1251, new BigDecimal("999999999999999.999"), BigDecimal.valueOf(10));
        check(postMigrate, 912, 1252, new BigDecimal("999999999999999.999"), BigDecimal.valueOf(5));
        check(postMigrate, 913, 1253, BigDecimal.valueOf(10), BigDecimal.valueOf(5));
        check(postMigrate, 913, 1255, BigDecimal.valueOf(12), BigDecimal.valueOf(6));
    }

    /**
     * Verifies that data with the expected values is present in the list.
     *
     * @param list        the data list
     * @param id          the product id
     * @param linkId      the entityLink.productStockLocation id
     * @param idealQty    the expected ideal quantity
     * @param criticalQty the expected critical quantity
     */
    private void check(List<Data> list, int id, long linkId, BigDecimal idealQty, BigDecimal criticalQty) {
        Data match = find(list, data -> data.getLinkId() == linkId);
        assertEquals(id, match.getProductId());
        assertEquals(0, idealQty.compareTo(new BigDecimal(match.getIdealQty())));
        assertEquals(0, criticalQty.compareTo(new BigDecimal(match.getCriticalQty())));
    }

    /**
     * Finds data in a list.
     *
     * @param list      the list
     * @param predicate the predicate to match
     * @return the first match
     */
    private Data find(List<Data> list, Predicate<Data> predicate) {
        Data match = list.stream().filter(predicate).findFirst().orElse(null);
        assertNotNull(match);
        return match;
    }

    /**
     * Queries stock location data.
     *
     * @return the matching data
     */
    private List<Data> getData() {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(
                "SELECT product_id, r.id link_id, ideal_qty.value ideal_qty, critical_qty.value critical_qty " +
                "FROM products p " +
                "JOIN entity_links r " +
                "ON p.product_id = r.source_id " +
                "AND r.arch_short_name = 'entityLink.productStockLocation' " +
                "JOIN entity_link_details ideal_qty " +
                "ON ideal_qty.id = r.id " +
                "AND ideal_qty.name = 'idealQty' " +
                "JOIN entity_link_details critical_qty " +
                "ON critical_qty.id = r.id " +
                "AND critical_qty.name = 'criticalQty' " +
                "WHERE r.arch_short_name = 'entityLink.productStockLocation'",
                BeanPropertyRowMapper.newInstance(Data.class));
    }

}
