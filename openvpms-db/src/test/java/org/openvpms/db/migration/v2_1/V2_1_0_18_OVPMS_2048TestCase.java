/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openvpms.db.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * Tests the migration for OVPMS-2292 Add support for clinical notes and addenda of unlimited length.
 *
 * @author Tim Anderson
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class V2_1_0_18_OVPMS_2048TestCase {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The database service.
     */
    @Autowired
    private DatabaseService service;

    /**
     * The JDBC template.
     */
    private JdbcTemplate template;


    /**
     * Open a connection to database for test execution statements.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            DbSupport support = DbSupportFactory.createDbSupport(connection, true);

            // load up test database baselined on V2.1.0.6
            ClassPathResource sql = new ClassPathResource("db-V2.1.0.6__OVPMS-2238.sql", getClass().getClassLoader());
            SqlScript script = new SqlScript(sql.loadAsString("UTF-8"), support);
            script.execute(support.getJdbcTemplate());
        }
        template = new JdbcTemplate(dataSource);
    }

    /**
     * Verifies that act.supplierDelivery rows with supplierInvoiceIds get replaced with
     * actIdentity.supplierInvoiceESCI.
     *
     * @throws SQLException for any error
     */
    @Test
    public void testMigration() throws SQLException {
        // check the expected supplierInvoiceId for each delivery
        checkDetails(287, "987654321");
        checkDetails(289, "123456789");
        checkDetails(291, null);

        // run the migration
        System.setProperty("openvpms.key", "cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        service.update("2_1_0_18", null, null);

        // verify the supplierInvoiceId has been moved to the act_identities table
        checkIdentity(287, "987654321");
        checkIdentity(289, "123456789");
        checkIdentity(291, null);
    }

    /**
     * Check the supplierInvoiceId for a specific delivery in the act_details table.
     *
     * @param deliveryId the delivery identifier
     * @param invoiceId  the expected invoice identifier. May be {@code null}
     */
    private void checkDetails(long deliveryId, String invoiceId) {
        String sql = "SELECT supplier_invoice_id.value " +
                     "FROM acts delivery " +
                     "LEFT JOIN act_details supplier_invoice_id " +
                     "ON delivery.act_id = supplier_invoice_id.act_id " +
                     "AND supplier_invoice_id.name = 'supplierInvoiceId'" +
                     "WHERE delivery.arch_short_name = 'act.supplierDelivery' and delivery.act_id = " + deliveryId;
        assertEquals(invoiceId, template.queryForObject(sql, String.class));
    }

    /**
     * Checks that the expected act identity is present or not, and that there is no longer an act_details record
     * for a specific delivery.
     *
     * @param deliveryId the delivery identifier
     * @param invoiceId  the expected invoice identifier. May be {@code null}
     */
    private void checkIdentity(long deliveryId, String invoiceId) {
        checkDetails(deliveryId, null);  // should be deleted
        String sql = "SELECT supplier_invoice_id.identity " +
                     "FROM acts delivery " +
                     "LEFT JOIN act_identities supplier_invoice_id " +
                     "ON delivery.act_id = supplier_invoice_id.act_id " +
                     "AND supplier_invoice_id.arch_short_name = 'actIdentity.supplierInvoiceESCI' " +
                     "WHERE delivery.arch_short_name = 'act.supplierDelivery' and delivery.act_id = " + deliveryId;
        assertEquals(invoiceId, template.queryForObject(sql, String.class));
    }
}
