#
# OBF-258 Add sequence column to act_relationships to allow act relationships to be ordered
#

DROP PROCEDURE IF EXISTS sp_add_sequence_to_act_relationships;

DELIMITER $$

CREATE PROCEDURE sp_add_sequence_to_act_relationships()
BEGIN
DECLARE colName TEXT;
SELECT column_name INTO colName
FROM information_schema.columns 
WHERE table_schema = database()
    AND table_name = 'act_relationships'
AND column_name = 'sequence';

IF colName is null THEN 
	alter table act_relationships add column `sequence` int(11) not null after `description`;
    END IF; 
END$$

DELIMITER ;

CALL sp_add_sequence_to_act_relationships;

DROP PROCEDURE sp_add_sequence_to_act_relationships;
