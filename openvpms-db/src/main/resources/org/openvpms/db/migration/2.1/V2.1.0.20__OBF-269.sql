#
# OBF-269 Back out audit column changes to documents
#

DROP PROCEDURE IF EXISTS sp_remove_created_updated_columns;

DELIMITER  $$

CREATE PROCEDURE sp_remove_created_updated_columns()
BEGIN
    DECLARE colName VARCHAR(255);
    SELECT COLUMN_NAME
    INTO colName
    FROM information_schema.COLUMNS
    WHERE TABLE_SCHEMA = database()
      AND TABLE_NAME = 'documents'
      AND COLUMN_NAME = 'created';

    IF colName IS NOT NULL THEN
        SET @sql = 'ALTER TABLE documents
                DROP COLUMN `created`,
                DROP COLUMN `created_id`,
                DROP COLUMN `updated`,
                DROP COLUMN `updated_id`,
                DROP KEY `FK383D52B8B96D33A7`,
                DROP KEY `FK383D52B8562D1CF4`,
                DROP FOREIGN KEY `FK383D52B8B96D33A7`,
                DROP FOREIGN KEY `FK383D52B8562D1CF4`';
        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END $$

DELIMITER ;

CALL sp_remove_created_updated_columns();

DROP PROCEDURE IF EXISTS sp_remove_created_updated_columns;