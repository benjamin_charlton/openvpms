#
# Add GAP_BENEFIT_PAYMENT lookup.customPaymentType for OVPMS-2217 Copy the link to this issue Gap claim accounting
# improvement
#

INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.customPaymentType',
       1,
       '1.0',
       'GAP_BENEFIT_PAYMENT',
       'Gap Benefit Payment',
       NULL,
       0
FROM dual
WHERE NOT exists(SELECT *
                 FROM lookups e
                 WHERE e.arch_short_name = 'lookup.customPaymentType'
                   AND e.code = 'GAP_BENEFIT_PAYMENT');

