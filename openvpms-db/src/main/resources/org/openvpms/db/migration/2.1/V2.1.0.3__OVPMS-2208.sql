#
# Ensure idealQty >= criticalQty for OVPMS-2208 Fix entityLink.productStockLocation relationships where the 
# idealQty is < criticalQty
#
DROP TABLE IF EXISTS ideal_critical_qty;
CREATE TEMPORARY TABLE ideal_critical_qty (
  id          BIGINT NOT NULL PRIMARY KEY,
  idealQty    DECIMAL(18, 3),
  criticalQty DECIMAL(18, 3)
);

#
# IGNORE will ensure values exceeding the range of DECIMAL(18, 3) are set to 999999999999999.999
#
INSERT IGNORE INTO ideal_critical_qty (id, idealQty, criticalQty)
  SELECT
    r.id,
    cast(idealQty.value AS DECIMAL(18, 3)),
    cast(criticalQty.value AS DECIMAL(18, 3))
  FROM entity_links r
    JOIN entity_link_details criticalQty
      ON r.id = criticalQty.id
         AND criticalQty.name = 'criticalQty'
    JOIN entity_link_details idealQty
      ON r.id = idealQty.id
         AND idealQty.name = 'idealQty'
  WHERE r.arch_short_name = 'entityLink.productStockLocation'
        AND cast(idealQty.value AS DECIMAL(18, 3)) < cast(criticalQty.value AS DECIMAL(18, 3));

UPDATE entity_links r
  JOIN ideal_critical_qty i
    ON i.id = r.id
  JOIN entity_link_details criticalQty
    ON r.id = criticalQty.id
       AND criticalQty.name = 'criticalQty'
  JOIN entity_link_details idealQty
    ON r.id = idealQty.id
       AND idealQty.name = 'idealQty'
SET idealQty.value  = i.criticalQty,
  criticalQty.value = i.idealQty;

DROP TABLE ideal_critical_qty;
