#
# OBF-272 Fix foreign key constraints from 1.2 to 1.3 and 1.7 to 1.8 migrations
#

DROP PROCEDURE IF EXISTS sp_add_key;

DELIMITER $$

CREATE PROCEDURE sp_add_key(IN tableName varchar(64), IN name varchar(64), IN columnName VARCHAR(64))
BEGIN
    IF NOT EXISTS(SELECT *
                  FROM information_schema.STATISTICS
                  WHERE TABLE_SCHEMA = DATABASE()
                    AND TABLE_NAME = tableName
                    AND INDEX_NAME = name)
    THEN
        SET @query = CONCAT('ALTER TABLE `', tableName, '` ADD KEY `', name, '` (`', columnName, '`);');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_add_foreign_key;

DELIMITER $$
CREATE PROCEDURE sp_add_foreign_key(IN tableName VARCHAR(64), IN name VARCHAR(64), IN columnName VARCHAR(64),
                                    IN referenceTable VARCHAR(64), IN referenceColumnName varchar(64))
BEGIN
    CALL sp_add_key(tableName, name, columnName);
    IF NOT EXISTS(SELECT *
                  FROM information_schema.TABLE_CONSTRAINTS
                  WHERE TABLE_SCHEMA = DATABASE()
                    AND TABLE_NAME = tableName
                    AND CONSTRAINT_NAME = name
                    AND CONSTRAINT_TYPE = 'FOREIGN KEY')
    THEN
        SET @query = CONCAT('ALTER TABLE `', tableName, '` ADD CONSTRAINT `', name, '` FOREIGN KEY (`', columnName,
                            '`) REFERENCES `', referenceTable, '` (`', referenceColumnName, '`);');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_drop_index;

DELIMITER $$
CREATE PROCEDURE sp_drop_index(IN tableName VARCHAR(64), IN name VARCHAR(64))
BEGIN
    IF EXISTS(SELECT *
              FROM information_schema.STATISTICS
              WHERE TABLE_SCHEMA = DATABASE()
                AND TABLE_NAME = tableName
                AND INDEX_NAME = name)
    THEN
        SET @query = CONCAT('ALTER TABLE `', tableName, '` DROP INDEX `', name, '`;');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_drop_foreign_key;

DELIMITER $$
CREATE PROCEDURE sp_drop_foreign_key(IN tableName VARCHAR(64), IN keyName VARCHAR(64))
BEGIN
    IF EXISTS(SELECT *
              FROM information_schema.TABLE_CONSTRAINTS
              WHERE TABLE_SCHEMA = DATABASE()
                AND TABLE_NAME = tableName
                AND CONSTRAINT_NAME = keyName
                AND CONSTRAINT_TYPE = 'FOREIGN KEY')
    THEN
        SET @query = CONCAT('ALTER TABLE `', tableName, '` DROP FOREIGN KEY `', keyName, '`;');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_drop_foreign_key_and_index;

DELIMITER $$
CREATE PROCEDURE sp_drop_foreign_key_and_index(IN tableName VARCHAR(64), IN keyName VARCHAR(64))
BEGIN
    CALL sp_drop_foreign_key(tableName, keyName);
    CALL sp_drop_index(tableName, keyName);
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS sp_add_on_delete_cascade;

DELIMITER $$
CREATE PROCEDURE sp_add_on_delete_cascade(IN tableName VARCHAR(64), IN keyName VARCHAR(64), IN columnName VARCHAR(64),
                                          IN referenceTable VARCHAR(64), IN referenceColumn VARCHAR(64))
BEGIN
    IF EXISTS(
               SELECT *
               FROM information_schema.REFERENTIAL_CONSTRAINTS
               WHERE CONSTRAINT_SCHEMA = DATABASE()
                 AND TABLE_NAME = tableName
                 AND CONSTRAINT_NAME = keyName
                 AND DELETE_RULE = 'RESTRICT')
        OR NOT EXISTS(SELECT *
                      FROM information_schema.REFERENTIAL_CONSTRAINTS
                      WHERE CONSTRAINT_SCHEMA = DATABASE()
                        AND TABLE_NAME = tableName
                        AND CONSTRAINT_NAME = keyName)
    THEN
        CALL sp_drop_foreign_key(tableName, keyName);

        SET @query = CONCAT('ALTER TABLE ', tableName, ' ADD CONSTRAINT `', keyName, '` FOREIGN KEY (`', columnName,
                            '`) REFERENCES `', referenceTable, '` (`', referenceColumn, '`) ON DELETE CASCADE');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END $$

DELIMITER ;

#
# Fix 1.2 to 1.3 migration.
#
CALL sp_add_foreign_key('act_details', 'FKFB795F95D8B907FA', 'act_id', 'acts', 'act_id');
CALL sp_add_foreign_key('act_relationship_details', 'FKFF1068C87D8180BF', 'act_relationship_id', 'act_relationships',
                        'act_relationship_id');
CALL sp_add_foreign_key('action_type_descriptors', 'FK7974D848936158C3', 'assertion_type_desc_id',
                        'assertion_type_descriptors', 'assertion_type_desc_id');
CALL sp_add_foreign_key('assertion_descriptors', 'FKA1159D27273C243C', 'node_desc_id', 'node_descriptors',
                        'node_desc_id');
CALL sp_add_foreign_key('contact_classifications', 'FK5AC8832E3B64C74E', 'contact_id', 'contacts', 'contact_id');
CALL sp_add_foreign_key('contact_classifications', 'FK5AC8832E6AE6B6CA', 'lookup_id', 'lookups', 'lookup_id');
CALL sp_add_foreign_key('contact_details', 'FKA3499D233B64C74E', 'contact_id', 'contacts', 'contact_id');
CALL sp_add_key('document_acts', 'FK5E78FAC5D1AECF9E', 'document_act_id');
CALL sp_add_foreign_key('document_details', 'FK829C1F1E92203729', 'document_id', 'documents', 'document_id');
CALL sp_add_foreign_key('entity_classifications', 'FK76B55CF14372B7A1', 'entity_id', 'entities', 'entity_id');
CALL sp_add_foreign_key('entity_classifications', 'FK76B55CF16AE6B6CA', 'lookup_id', 'lookups', 'lookup_id');
CALL sp_add_foreign_key('entity_details', 'FKD621E9E64372B7A1', 'entity_id', 'entities', 'entity_id');
CALL sp_add_foreign_key('entity_identity_details', 'FK4794CC9D3C2625E8', 'entity_identity_id', 'entity_identities',
                        'entity_identity_id');
CALL sp_add_foreign_key('entity_relationship_details', 'FKBB44EA17AB042EA8', 'entity_relationship_id',
                        'entity_relationships', 'entity_relationship_id');
CALL sp_add_foreign_key('entity_relationships', 'FK861BFDDF529A044', 'identity_id', 'entity_identities',
                        'entity_identity_id');
CALL sp_add_key('financial_acts', 'FK3D69E417B80AAC70', 'financial_act_id');
CALL sp_add_foreign_key('lookup_details', 'FKB2E8287D6AE6B6CA', 'lookup_id', 'lookups', 'lookup_id');
CALL sp_add_foreign_key('lookup_relationship_details', 'FK40E558E08B49F65F', 'lookup_relationship_id',
                        'lookup_relationships', 'lookup_relationship_id');
CALL sp_add_foreign_key('participation_details', 'FK64ED55446614381A', 'participation_id', 'participations',
                        'participation_id');
CALL sp_add_key('parties', 'FKD0BCCA04B67A7DBE', 'party_id');
CALL sp_add_foreign_key('product_price_classifications', 'FK9EC6BF076AE6B6CA', 'lookup_id', 'lookups', 'lookup_id');
CALL sp_add_foreign_key('product_price_classifications', 'FK9EC6BF07521C9574', 'product_price_id', 'product_prices',
                        'product_price_id');
CALL sp_add_foreign_key('product_price_details', 'FKF9A9C1FC521C9574', 'product_price_id', 'product_prices',
                        'product_price_id');
CALL sp_add_key('products', 'FKC42BD164D813A315', 'product_id');
CALL sp_add_foreign_key('roles_authorities', 'FKE9CCCC9F2FE5EADB', 'authority_id', 'granted_authorities',
                        'granted_authority_id');
CALL sp_add_foreign_key('roles_authorities', 'FKE9CCCC9F844DFA25', 'security_role_id', 'security_roles',
                        'security_role_id');
CALL sp_add_foreign_key('user_roles', 'FK734299495F0477E4', 'user_id', 'users', 'user_id');
CALL sp_add_foreign_key('user_roles', 'FK73429949844DFA25', 'security_role_id', 'security_roles', 'security_role_id');

CALL sp_drop_foreign_key_and_index('parties', 'FKD0BCCA04D7D260F7');
CALL sp_drop_foreign_key_and_index('act_details', 'FKFB795F95ACF0613B');
CALL sp_drop_foreign_key_and_index('act_relationship_details', 'FKFF1068C841148A00');
CALL sp_drop_foreign_key_and_index('action_type_descriptors', 'FK7974D84887521755');
CALL sp_drop_foreign_key_and_index('assertion_descriptors', 'FKA1159D27C550DB74');
CALL sp_drop_foreign_key_and_index('contact_classifications', 'FK5AC8832E519E590F');
CALL sp_drop_foreign_key_and_index('contact_classifications', 'FK5AC8832ECEABD10B');
CALL sp_drop_foreign_key_and_index('contact_details', 'FKA3499D23519E590F');
CALL sp_drop_foreign_key_and_index('contacts', 'FKDE2D605349856C8F');
CALL sp_drop_foreign_key_and_index('document_acts', 'FK5E78FAC5A5E628DF');
CALL sp_drop_foreign_key_and_index('document_details', 'FK829C1F1E423E836A');
CALL sp_drop_foreign_key_and_index('entity_classifications', 'FK76B55CF1CEABD10B');
CALL sp_drop_foreign_key_and_index('entity_classifications', 'FK76B55CF164CA9ADA');
CALL sp_drop_foreign_key_and_index('entity_details', 'FKD621E9E664CA9ADA');
CALL sp_drop_foreign_key_and_index('entity_identities', 'FKB1D93FB864CA9ADA');
CALL sp_drop_foreign_key_and_index('entity_identity_details', 'FK4794CC9DBDCAE3A1');
CALL sp_drop_foreign_key_and_index('entity_relationship_details', 'FKBB44EA1739208DE1');
CALL sp_drop_foreign_key_and_index('entity_relationships', 'FK861BFDDF86CE5DFD');
CALL sp_drop_foreign_key_and_index('financial_acts', 'FK3D69E4178C4205B1');
CALL sp_drop_foreign_key_and_index('lookup_details', 'FKB2E8287DCEABD10B');
CALL sp_drop_foreign_key_and_index('lookup_relationship_details', 'FK40E558E0BAD140A0');
CALL sp_drop_foreign_key_and_index('node_descriptors', 'FKAB46B4278E2DBBF8');
CALL sp_drop_foreign_key_and_index('node_descriptors', 'FKAB46B42741D3C6F4');
CALL sp_drop_foreign_key_and_index('participation_details', 'FK64ED55445A24061A');
CALL sp_drop_foreign_key_and_index('product_price_classifications', 'FK9EC6BF07CEABD10B');
CALL sp_drop_foreign_key_and_index('product_price_classifications', 'FK9EC6BF07BCF3BC5D');
CALL sp_drop_foreign_key_and_index('product_price_details', 'FKF9A9C1FCBCF3BC5D');
CALL sp_drop_foreign_key_and_index('product_prices', 'FKFBD40D9A43C96078');
CALL sp_drop_foreign_key_and_index('products', 'FKC42BD164F96B864E');
CALL sp_drop_foreign_key_and_index('roles_authorities', 'FKE9CCCC9FEF0F2E9F');
CALL sp_drop_foreign_key_and_index('roles_authorities', 'FKE9CCCC9F9A38A0E6');
CALL sp_drop_foreign_key_and_index('user_roles', 'FK734299497649F665');
CALL sp_drop_foreign_key_and_index('user_roles', 'FK734299499A38A0E6');

CALL sp_drop_index('participations', 'participation_entity_linkId_act_sn_index_idx');

CALL sp_add_on_delete_cascade('contacts', 'FKDE2D60537649DB4E', 'party_id', 'parties', 'party_id');
CALL sp_add_on_delete_cascade('document_acts', 'FK5E78FAC5D1AECF9E', 'document_act_id', 'acts', 'act_id');
CALL sp_add_on_delete_cascade('entity_identities', 'FKB1D93FB84372B7A1', 'entity_id', 'entities', 'entity_id');
CALL sp_add_on_delete_cascade('financial_acts', 'FK3D69E417B80AAC70', 'financial_act_id', 'acts', 'act_id');
CALL sp_add_on_delete_cascade('node_descriptors', 'FKAB46B42775928C22', 'archetype_desc_id', 'archetype_descriptors',
                              'archetype_desc_id');
CALL sp_add_on_delete_cascade('node_descriptors', 'FKAB46B427F01904C0', 'parent_id', 'node_descriptors',
                              'node_desc_id');
CALL sp_add_on_delete_cascade('parties', 'FKD0BCCA04B67A7DBE', 'party_id', 'entities', 'entity_id');
CALL sp_add_on_delete_cascade('product_prices', 'FKFBD40D9AE51F1FB7', 'product_id', 'products', 'product_id');
CALL sp_add_on_delete_cascade('products', 'FKC42BD164D813A315', 'product_id', 'entities', 'entity_id');

ALTER TABLE assertion_descriptors
    MODIFY property_map longtext;

#
# Fix 1.7 to 1.8 migration.
#
CALL sp_drop_foreign_key_and_index('users', 'FK6A68E0866D1C6B9');
CALL sp_add_on_delete_cascade('users', 'FK6A68E0826A12449', 'user_id', 'parties', 'party_id');

DROP PROCEDURE IF EXISTS sp_add_key;
DROP PROCEDURE IF EXISTS sp_add_foreign_key;
DROP PROCEDURE IF EXISTS sp_drop_index;
DROP PROCEDURE IF EXISTS sp_drop_foreign_key;
DROP PROCEDURE IF EXISTS sp_drop_foreign_key_and_index;
DROP PROCEDURE IF EXISTS sp_add_on_delete_cascade;
