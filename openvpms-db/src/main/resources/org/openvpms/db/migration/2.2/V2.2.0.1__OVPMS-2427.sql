#
# OVPMS-2427 Revert changes to Schedule Manager in roles.xml
#

DELETE role_auth
FROM security_roles r
         JOIN roles_authorities role_auth
              ON r.security_role_id = role_auth.security_role_id
         JOIN granted_authorities auth
              ON auth.granted_authority_id = role_auth.authority_id
WHERE r.name = 'Schedule Manager'
  AND ((auth.archetype = 'act.rosterEvent' AND auth.method = 'remove')
    OR auth.archetype IN ('act.bankDeposit',
                          'act.customerOrder*',
                          'act.customerReturn*',
                          'act.documentLogo',
                          'act.HL7Message',
                          'party.organisation*',
                          'act.patientInsurancePolicy',
                          'entity.preference*',
                          'act.tillBalance',
                          'act.tillBalanceAdjustment',
                          'act.laboratoryOrder',
                          'act.userMessage'));
