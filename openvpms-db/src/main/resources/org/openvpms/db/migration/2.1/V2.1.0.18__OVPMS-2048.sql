#
# OVPMS-2048 Replace supplierInvoiceId node on act.supplierDelivery with an ActIdentity
#

# Create an identity for each supplierInvoiceId
INSERT INTO act_identities (version, linkId, act_id, arch_short_name, arch_version, name, active, identity)
SELECT 0,
       delivery.linkId,
       delivery.act_id,
       'actIdentity.supplierInvoiceESCI',
       '1.0',
       NULL,
       1,
       supplier_invoice_id.value
FROM acts delivery
         JOIN act_details supplier_invoice_id
              ON delivery.act_id = supplier_invoice_id.act_id
                  AND supplier_invoice_id.name = 'supplierInvoiceId'
WHERE delivery.arch_short_name = 'act.supplierDelivery'
  AND NOT EXISTS(SELECT *
                 FROM act_identities i
                 WHERE i.arch_short_name = 'actIdentity.supplierInvoiceESCI'
                   AND i.act_id = delivery.act_id
                   AND i.identity = supplier_invoice_id.value);

# Delete the supplierInvoiceId from act_details
DELETE supplier_invoice_id
FROM acts delivery
         JOIN act_details supplier_invoice_id
              ON delivery.act_id = supplier_invoice_id.act_id
                  AND supplier_invoice_id.name = 'supplierInvoiceId'
WHERE delivery.arch_short_name = 'act.supplierDelivery';
