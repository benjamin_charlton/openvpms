#
# OVPMS-428 User password hashing.
#
ALTER TABLE users
    MODIFY password varchar(255) DEFAULT NULL;
