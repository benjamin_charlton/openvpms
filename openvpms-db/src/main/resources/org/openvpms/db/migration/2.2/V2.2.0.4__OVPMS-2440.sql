#
# OVPMS-2440 Add support to filter document templates by type.
#
DROP TABLE IF EXISTS tmp_lookups;

CREATE TABLE tmp_lookups
(
    code varchar(100) PRIMARY KEY,
    name varchar(100) NOT NULL
);

INSERT INTO tmp_lookups (code, name)
VALUES ('act.patientDocumentForm', 'Patient Form'),
       ('act.patientDocumentImage', 'Patient Image'),
       ('act.patientDocumentLetter', 'Patient Letter'),
       ('act.customerDocumentForm', 'Customer Form'),
       ('act.customerDocumentLetter', 'Customer Letter'),
       ('act.supplierDocumentForm', 'Supplier Form'),
       ('act.supplierDocumentLetter', 'Supplier Letter'),
       ('act.customerAccountPayment', 'Customer Payment'),
       ('act.customerAccountRefund', 'Customer Refund'),
       ('act.customerAccountChargesInvoice', 'Customer Invoice'),
       ('act.customerAccountChargesCredit', 'Customer Credit'),
       ('act.customerAccountCreditAdjust', 'Customer Credit Adjustment'),
       ('act.customerAccountChargesCounter', 'Customer Counter Sale'),
       ('act.customerAccountDebitAdjust', 'Customer Debit Adjustment'),
       ('act.customerEstimation', 'Customer Estimate'),
       ('act.customerOrderPharmacy', 'Customer Pharmacy Order'),
       ('act.customerReturnPharmacy', 'Customer Pharmacy Return'),
       ('act.customerReturnInvestigation', 'Investigation Return'),
       ('act.patientMedication', 'Patient Medication Label'),
       ('act.patientPrescription', 'Prescription'),
       ('act.patientReminder', 'Reminder Report'),
       ('act.patientClinicalEvent', 'Patient Visit'),
       ('act.patientClinicalProblem', 'Patient Problem'),
       ('act.patientInsuranceClaim', 'Insurance Claim'),
       ('act.tillBalance', 'Till Balance'),
       ('act.bankDeposit', 'Bank Deposit'),
       ('act.customerAccountOpeningBalance', 'Customer Statement'),
       ('act.supplierOrder', 'Supplier Order'),
       ('act.supplierDelivery', 'Supplier Delivery'),
       ('act.supplierAccountChargesInvoice', 'Supplier Invoice'),
       ('act.supplierAccountChargesCredit', 'Supplier Credit'),
       ('act.supplierAccountPayment', 'Supplier Remittance'),
       ('act.supplierAccountRefund', 'Supplier Refund'),
       ('act.supplierReturn', 'Supplier Return'),
       ('act.stockAdjust', 'Stock Adjustment'),
       ('act.stockTransfer', 'Stock Transfer'),
       ('act.customerAppointment', 'Appointment'),
       ('act.customerTask', 'Task'),
       ('act.auditMessage', 'Audit Message'),
       ('act.systemMessage', 'System Message'),
       ('act.userMessage', 'Message'),
       ('act.HL7Message', 'HL7 Message'),
       ('CUSTOMER_BALANCE', 'Customer Account Balance'),
       ('WORK_IN_PROGRESS_CHARGES', 'Work in Progress Charges'),
       ('CHARGES', 'Charges'),
       ('GROUPED_REMINDERS', 'Grouped Reminders'),
       ('REPORT', 'Report'),
       ('SUBREPORT', 'Sub Report'),
       ('INSURANCE_CLAIM_INVOICE', 'Insurance Claim Invoice'),
       ('INSURANCE_CLAIM_MEDICAL_RECORDS', 'Insurance Claim Medical Records'),
       ('INSURANCE_CLAIM', 'Insurance Claim - Custom'),
       ('INSURANCE_CLAIMS_REPORT', 'Insurance Claims Report');

#
# Insert custom template types. The name can't be populated from the descriptor, so these will need to be
# renamed post migration
#       
INSERT INTO tmp_lookups (code, name)
SELECT DISTINCT archetype.value, archetype.value
FROM entities template
         JOIN entity_details archetype
              ON archetype.entity_id = template.entity_id
                  AND archetype.name = 'archetype'
WHERE template.arch_short_name = 'entity.documentTemplate'
  AND NOT EXISTS(SELECT *
                 FROM tmp_lookups l
                 WHERE l.code = archetype.value);

INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version,
                     code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.documentTemplateType',
       TRUE,
       '1.0',
       l.code,
       l.name,
       NULL,
       FALSE
FROM tmp_lookups l
WHERE NOT EXISTS(SELECT * FROM lookups t WHERE t.code = l.code);

DROP TABLE tmp_lookups;

#
# Link entity.documentTemplate to lookup.documentTemplateType.
# 
INSERT INTO entity_classifications (entity_id, lookup_id)
SELECT template.entity_id, template_type.lookup_id
FROM entities template
         JOIN entity_details archetype
              ON archetype.entity_id = template.entity_id
                  AND archetype.name = 'archetype'
         JOIN lookups template_type
              ON archetype.value = template_type.code
                  AND template_type.arch_short_name = 'lookup.documentTemplateType'
WHERE template.arch_short_name = 'entity.documentTemplate'
  AND NOT EXISTS(SELECT *
                 FROM entity_classifications ec
                 WHERE ec.entity_id = template.entity_id
                   AND ec.lookup_id = template_type.lookup_id);

#
# Delete the archetype nodes.
#
DELETE archetype
FROM entities template
         JOIN entity_details archetype
              ON template.entity_id = archetype.entity_id
                  AND archetype.name = 'archetype'
WHERE template.arch_short_name = 'entity.documentTemplate'
  AND template.entity_id = archetype.entity_id;