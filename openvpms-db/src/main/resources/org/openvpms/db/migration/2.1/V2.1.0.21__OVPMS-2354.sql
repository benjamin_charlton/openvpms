#
# OVPMS-2354 Change password column to allow nulls.
#
# Required for sites that have applied the original V2.1.0.11__OVPMS-428.sql
#
ALTER TABLE users
    MODIFY password varchar(255) DEFAULT NULL;
