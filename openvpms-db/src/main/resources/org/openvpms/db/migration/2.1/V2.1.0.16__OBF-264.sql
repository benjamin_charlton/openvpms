#
# OBF-264 Add indexes on acts.name, documents_acts.file_name
#

ALTER TABLE acts
    ADD INDEX act_name_idx (name);

ALTER TABLE document_acts
    ADD INDEX document_act_file_name_idx (file_name);
