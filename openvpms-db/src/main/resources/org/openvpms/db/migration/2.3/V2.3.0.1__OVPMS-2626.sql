#
# OVPMS-2626 EmailDocumentLoaderJob not cleaning mime type
#

#
# Only perform the migration if the job is deployed.
# This is because it can take a long time, particularly when updating the documents table.
#
UPDATE document_acts
SET mime_type = SUBSTRING_INDEX(mime_type, ';', 1)
WHERE EXISTS(SELECT *
             FROM entities
             WHERE arch_short_name = 'entity.jobEmailDocumentLoader');

UPDATE documents
SET mime_type = SUBSTRING_INDEX(mime_type, ';', 1)
WHERE EXISTS(SELECT *
             FROM entities
             WHERE arch_short_name = 'entity.jobEmailDocumentLoader');
