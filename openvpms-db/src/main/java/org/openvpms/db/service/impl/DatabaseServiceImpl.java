/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.apache.commons.dbcp2.BasicDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.api.MigrationVersion;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.Schema;
import org.flywaydb.core.internal.dbsupport.SqlScript;
import org.flywaydb.core.internal.dbsupport.Table;
import org.flywaydb.core.internal.info.MigrationInfoServiceImpl;
import org.flywaydb.core.internal.util.PlaceholderReplacer;
import org.flywaydb.core.internal.util.scanner.Resource;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.system.common.crypto.DefaultPasswordEncryptorFactory;
import org.openvpms.component.system.common.crypto.PasswordEncryptorFactory;
import org.openvpms.db.migration.R__ArchetypeLoader;
import org.openvpms.db.migration.R__PluginLoader;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.DatabaseService;
import org.openvpms.db.service.Migrator;
import org.openvpms.db.service.PluginMigrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Default implementation of the {@link DatabaseService}.
 *
 * @author Tim Anderson
 */
public class DatabaseServiceImpl implements DatabaseService {

    /**
     * The database driver class name.
     */
    private final String driver;

    /**
     * The database URL.
     */
    private final String url;

    /**
     * The root database URL.
     */
    private final String rootURL;

    /**
     * The database schema name.
     */
    private final String schemaName;

    /**
     * Flyway.
     */
    private final Flyway flyway;

    /**
     * The data source.
     */
    private final DataSource dataSource;

    /**
     * Changed checksums, keyed on version.
     */
    private final Map<String, Integer> changedChecksums;

    /**
     * The archetype and plugin checksums.
     */
    private final Checksums checksums;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DatabaseServiceImpl.class);


    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver   the driver class name
     * @param url      the driver url
     * @param user     the database user name
     * @param password the database password
     * @param listener the listener to notify of Flyway events. May be {@code null}
     */
    public DatabaseServiceImpl(String driver, String url, String user, String password, FlywayCallback listener) {
        this(driver, url, createDataSource(driver, url, user, password), new ChecksumsImpl(), listener);
    }

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver    the driver class name
     * @param url       the driver url
     * @param user      the database user name
     * @param password  the database password
     * @param checksums determines the archetype and plugin checksums
     * @param listener  the listener to notify of Flyway events. May be {@code null}
     */
    public DatabaseServiceImpl(String driver, String url, String user, String password,
                               Checksums checksums, FlywayCallback listener) {
        this(driver, url, createDataSource(driver, url, user, password), checksums, listener);
    }

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver     the driver class name
     * @param url        the driver url
     * @param dataSource the data source
     */
    public DatabaseServiceImpl(String driver, String url, DataSource dataSource) {
        this(driver, url, dataSource, new ChecksumsImpl());
    }

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver     the driver class name
     * @param url        the driver url
     * @param dataSource the data source
     * @param checksums  determines the archetype and plugin checksums
     */
    public DatabaseServiceImpl(String driver, String url, DataSource dataSource, Checksums checksums) {
        this(driver, url, dataSource, checksums, null);
    }

    /**
     * Constructs a {@link DatabaseServiceImpl}.
     *
     * @param driver     the driver class name
     * @param url        the driver url
     * @param dataSource the data source
     * @param checksums  determines the archetype and plugin checksums
     * @param listener   the listener to notify of Flyway events. May be {@code null}
     */
    public DatabaseServiceImpl(String driver, String url, DataSource dataSource, Checksums checksums,
                               FlywayCallback listener) {
        this.driver = driver;
        DbURLParser parser = new DbURLParser(url);
        this.url = url;
        rootURL = parser.getRootUrl();
        schemaName = parser.getSchemaName();
        this.dataSource = dataSource;
        this.checksums = checksums;
        flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setLocations("org/openvpms/db/migration");
        if (listener != null) {
            flyway.setCallbacks(listener);
        }
        changedChecksums = getChangedChecksums();
        log.info("Using database {}, URL={}", schemaName, url);
    }

    /**
     * Returns the schema name.
     *
     * @return the schema name
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Determines if the database exists.
     *
     * @param adminUser     the database administrator user name
     * @param adminPassword the database administrator password
     * @return {@code true} if the database exists, otherwise {@code false}
     * @throws SQLException for any SQL error
     */
    @Override
    public boolean exists(String adminUser, String adminPassword) throws SQLException {
        DataSource rootAdmin = createDataSource(driver, rootURL, adminUser, adminPassword);
        try (Connection connection = rootAdmin.getConnection()) {
            return exists(connection);
        }
    }

    /**
     * Creates the database.
     *
     * @param adminUser     the database administrator user name
     * @param adminPassword the database administrator password
     * @param user          the user to create
     * @param password      the password of the user
     * @param host          the host the user is connecting from
     * @param createTables  if {@code true}, create the tables, and base-line
     * @throws SQLException for any SQL error
     */
    @Override
    public void create(String adminUser, String adminPassword, String user, String password, String host,
                       boolean createTables)
            throws SQLException {
        DataSource rootAdmin = createDataSource(driver, rootURL, adminUser, adminPassword);
        try (Connection connection = rootAdmin.getConnection()) {
            boolean found = exists(connection);
            if (!found) {
                DbSupport support = DbSupportFactory.createDbSupport(connection, true);
                Resource resource = getResource("org/openvpms/db/schema/database.sql");
                Map<String, String> placeholders = new HashMap<>();
                placeholders.put("db.name", schemaName);
                placeholders.put("db.user", user);
                placeholders.put("db.password", password);
                placeholders.put("db.userhost", host);
                SqlScript script = new SqlScript(support, resource, new PlaceholderReplacer(placeholders, "${", "}"),
                                                 "UTF-8", false);
                script.execute(support.getJdbcTemplate());
            } else if (!createTables) {
                throw new SQLException("Cannot create " + schemaName + " as it already exists");
            }
        }
        if (createTables) {
            DataSource dbAdmin = createDataSource(driver, url, adminUser, adminPassword);
            // register no-op migrators, to avoid loading archetypes and plugins
            R__ArchetypeLoader.setMigrator(new NoOpMigrator());
            R__PluginLoader.setMigrator(new NoOpMigrator());
            try (Connection connection = dbAdmin.getConnection()) {
                DbSupport support = DbSupportFactory.createDbSupport(connection, true);
                Schema<?> schema = support.getOriginalSchema();
                if (schema.allTables().length == 0) {
                    Resource resource = getResource("org/openvpms/db/schema/schema.sql");
                    SqlScript script = new SqlScript(resource.loadAsString("UTF-8"), support);
                    script.execute(support.getJdbcTemplate());
                    MigrationInfo version = getBaselineVersion();
                    if (version != null) {
                        baseline(version.getVersion(), version.getDescription());
                    }
                } else {
                    throw new SQLException("Cannot create " + schemaName + " as there are tables already present");
                }
            } finally {
                resetMigrators();
            }
        }
    }

    /**
     * Determines if the database needs migration.
     *
     * @return {@code true} if the database needs migration
     */
    @Override
    public boolean needsUpdate() {
        return getChangesToApply() != 0;
    }

    /**
     * Returns the number of migration steps that need to be applied to bring the database up-to-date.
     *
     * @return the number of migration steps, or {@code 0} if no update is required
     */
    @Override
    public int getChangesToApply() {
        MigrationInfoServiceImpl info = (MigrationInfoServiceImpl) getInfo();
        int result = info.pending().length + info.failed().length;
        if (needsChecksumUpdate()) {
            result++;
        }
        return result;
    }

    /**
     * Returns the current database version.
     *
     * @return the current database version, or {@code null} if it is not known
     */
    @Override
    public String getVersion() {
        String result = null;
        MigrationInfo current = getInfo().current();
        if (current != null) {
            result = current.getVersion().toString();
        }
        return result;
    }

    /**
     * Returns the version that the database needs to be migrated to, if it is out of date.
     * <p/>
     * If {@link #needsUpdate()} returns {@code true}, but no version is returned by this method, a repeatable migration
     * may need to be run.
     *
     * @return the version to migrate to, or {@code null} if the database doesn't need migration
     */
    public String getMigrationVersion() {
        MigrationInfo info = getNewestVersion();
        return info != null && info.getVersion() != null ? info.getVersion().toString() : null;
    }

    /**
     * Migrates the database to the latest version.
     *
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    @Override
    public void update(ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException {
        update(null, archetypeMigrator, pluginMigrator);
    }

    /**
     * Updates the database to a specific version.
     *
     * @param version           the migration version
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    @Override
    public void update(String version, ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator)
            throws SQLException {
        checkPreconditions();
        baseline();
        if (needsChecksumUpdate()) {
            repair(); // this will repair all checksums
        }
        try {
            R__ArchetypeLoader.setMigrator(archetypeMigrator != null ? getArchetypeMigrator(archetypeMigrator)
                                                                     : new NoOpMigrator());
            R__PluginLoader.setMigrator(pluginMigrator != null ? getPluginMigrator(pluginMigrator) :
                                        new NoOpMigrator());
            if (version != null) {
                flyway.setTargetAsString(version);
            }
            flyway.migrate();
        } finally {
            resetMigrators();
        }
    }

    /**
     * Repairs the Flyway metadata table. This will perform the following actions:
     * <ul>
     * <li>Remove any failed migrations on databases without DDL transactions (User objects left behind must still be
     * cleaned up manually)</li>
     * <li>Correct wrong checksums</li>
     * </ul>
     */
    public void repair() {
        try {
            installDisabledMigrators();
            flyway.repair();
        } finally {
            resetMigrators();
        }
    }

    /**
     * Returns the migration info.
     *
     * @return the migration info
     */
    public MigrationInfoService getInfo() {
        try {
            installDisabledMigrators();
            return flyway.info();
        } finally {
            resetMigrators();
        }
    }

    /**
     * Returns the database size.
     *
     * @return the database size, in bytes
     * @throws SQLException for any SQL error
     */
    public long getSize() throws SQLException {
        long result = -1;
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "SELECT SUM(data_length + index_length) AS size " +
                    "FROM information_schema.tables " +
                    "WHERE TABLE_SCHEMA = ?")) {
                statement.setString(1, schemaName);
                ResultSet set = statement.executeQuery();
                if (set.next()) {
                    result = set.getLong(1);
                }
            }
        }
        return result;
    }

    /**
     * Returns the schema version.
     *
     * @param schema the schema
     * @return the schema version, or {@code null} if it cannot be determined
     */
    protected String getExistingVersion(Schema<?> schema) {
        Table acts = schema.getTable("acts");
        if (acts != null && acts.hasColumn("status2")) {
            return "1.9";
        }
        return null;
    }

    /**
     * Verifies that strong password encryption is supported.
     *
     * @throws RuntimeException if strong password encryption is not supported
     */
    protected void checkPasswordEncryptionSupport() {
        PasswordEncryptorFactory factory
                = new DefaultPasswordEncryptorFactory("cWdFYkhCOFhoK3BsUzNNZjVXZEhaUTo7LllzXUpsRy9g");
        // NOTE: the above key is not used for anything other than testing
        PasswordEncryptor encryptor = factory.create();
        encryptor.encrypt("dummy");
    }

    /**
     * Determines if the schema exists.
     *
     * @param connection the database connection
     * @return {@code true} if the schema exists
     * @throws SQLException for any error
     */
    private boolean exists(Connection connection) throws SQLException {
        boolean found = false;
        DatabaseMetaData metaData = connection.getMetaData();
        try (ResultSet set = metaData.getCatalogs()) {
            while (set.next()) {
                String schema = set.getString("TABLE_CAT");
                if (schemaName.equalsIgnoreCase(schema)) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    /**
     * Installs migrators that return checksums but cannot be used for migration.
     */
    private void installDisabledMigrators() {
        R__ArchetypeLoader.setMigrator(disabledMigrator(checksums::getArchetypeChecksum));
        R__PluginLoader.setMigrator(disabledMigrator(checksums::getPluginChecksum));
    }

    /**
     * Reest the singleton migrators.
     */
    private void resetMigrators() {
        R__ArchetypeLoader.setMigrator(null);
        R__PluginLoader.setMigrator(null);
    }

    /**
     * Creates a migrator that returns the checksum, but cannot be used to perform migration.
     *
     * @return a new migrator
     */
    private Migrator disabledMigrator(Supplier<Integer> checksum) {
        return new Migrator() {
            @Override
            public Integer getChecksum() {
                return checksum.get();
            }

            @Override
            public void migrate() {
                throw new IllegalStateException("Cannot perform migration");
            }
        };
    }

    /**
     * Verifies that preconditions are met before performing a database update.
     *
     * @throws SQLException for any error
     */
    private void checkPreconditions() throws SQLException {
        // verify strong encryption is supported. This is required by the migration for OVPMS-2233.
        try {
            checkPasswordEncryptionSupport();
        } catch (Exception exception) {
            throw new SQLException(
                    "Unable to perform database migration. Strong password encryption is not supported.\n" +
                    "Please update to a newer version of Java.", exception);
        }
    }

    /**
     * Returns the checksums of any migration that has changed subsequent to being released.
     * <p/>
     * If the migration has been successfully applied, its checksum will need to be updated in order for Flyway
     * to subsequently migrate the database as it aborts with an error if an applied checksum has changed.
     *
     * @return the changed checksums, keyed on migration version
     */
    private Map<String, Integer> getChangedChecksums() {
        Map<String, Integer> result = new HashMap<>();
        result.put("2.1.0.3", -427272242);  // migration for OVPMS-2208
        result.put("2.1.0.5", 229982045);   // migration for OVPMS-2252, OVPMS-2265
        result.put("2.1.0.9", 1610869084);  // migration for OVPMS-2082, updated to allow for longer
        // universalServiceIdentifier
        result.put("2.1.0.11", 1116763144); // migration for OVPMS-428, updated by OVPMS-2354
        result.put("2.1.0.13", 1611424731);  // migration for OVPMS-2241, updated by OBF-269 and OVPMS-2450
        return result;
    }

    /**
     * Determines if a checksum needs to be updated.
     * <p/>
     * This only evaluates checksums if there are no failed migrations.
     * <p/>
     * This is because invoking {@link #repair()} to correct checksums also removes failed migrations, which
     * is probably not desirable to do on an automatic basis.
     *
     * @return {@code true} if a checksum needs to be updated, otherwise {@code false}
     */
    private boolean needsChecksumUpdate() {
        for (MigrationInfo info : getInfo().all()) {
            if (info.getState().isFailed()) {
                return false;
            }
        }
        for (MigrationInfo info : getInfo().applied()) {
            if (info.getVersion() != null) {
                // need to exclude repeatable migrations, which have no version
                String version = info.getVersion().getVersion();
                Integer newCheckSum = changedChecksums.get(version);
                if (newCheckSum != null && !newCheckSum.equals(info.getChecksum())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Creates a new data source.
     *
     * @param driver   the driver class name
     * @param url      the database URL
     * @param user     the user
     * @param password the password
     * @return a new data source
     */
    private static DataSource createDataSource(String driver, String url, String user, String password) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    /**
     * Helper to create a resource.
     *
     * @param path the resource path
     * @return a new resource
     */
    private Resource getResource(String path) {
        return new ClassPathResource(path, getClass().getClassLoader());
    }

    /**
     * Baselines the database, if there are existing tables.
     *
     * @throws SQLException for any error
     */
    private void baseline() throws SQLException {
        MigrationInfo current = getInfo().current();
        if (current == null) {
            // no schema version information
            try (Connection connection = dataSource.getConnection()) {
                DbSupport support = DbSupportFactory.createDbSupport(connection, true);
                Schema<?> schema = support.getOriginalSchema();
                if (schema.allTables().length != 0) {
                    // there are tables in the db. Make sure they belong to the most recent version
                    String existing = getExistingVersion(schema);
                    if (existing != null) {
                        // pre-existing schema. Don't want to create it again
                        baseline(MigrationVersion.fromVersion(existing), "Initial schema");
                    } else {
                        throw new SQLException("This database needs to be manually migrated to OpenVPMS 1.9");
                    }
                }
            }
        }
    }

    /**
     * <p>Baselines an existing database, excluding all migrations up to and including version.</p>
     *
     * @param version     the version
     * @param description the description
     * @throws FlywayException if the schema baselining failed
     */
    private void baseline(MigrationVersion version, String description) {
        flyway.setBaselineVersion(version);
        flyway.setBaselineDescription(description);
        flyway.baseline();
    }

    /**
     * Returns the most recent version of the database that needs to be updated to.
     *
     * @return the most recent version of the database, or {@code null} if no update is required
     */
    private MigrationInfo getNewestVersion() {
        MigrationInfo[] info = getInfo().pending();
        return info.length > 0 ? info[info.length - 1] : null;
    }

    /**
     * Returns the version to use for when marking the baseline after database creation.
     *
     * @return the version, or {@code null} if there is none
     */
    private MigrationInfo getBaselineVersion() {
        MigrationInfo result = null;
        MigrationInfo[] info = getInfo().pending();
        int index = info.length - 1;
        while (index >= 0) {
            if (info[index].getVersion() != null) {
                // need to exclude repeatable migrations which have no version
                result = info[index];
                break;
            } else {
                index--;
            }
        }
        return result;
    }

    /**
     * Returns a {@link Migrator} to migrate plugins.
     *
     * @param migrator the migrator to delegate to
     * @return a new migrator
     */
    private Migrator getArchetypeMigrator(ArchetypeMigrator migrator) {
        return new Migrator() {
            @Override
            public Integer getChecksum() {
                return checksums.getArchetypeChecksum();
            }

            @Override
            public void migrate() {
                migrator.migrate();
            }
        };
    }

    /**
     * Returns a {@link Migrator} to migrate plugins.
     *
     * @param migrator the migrator to delegate to
     * @return a new migrator
     */
    private Migrator getPluginMigrator(PluginMigrator migrator) {
        return new Migrator() {
            @Override
            public Integer getChecksum() {
                return checksums.getPluginChecksum();
            }

            @Override
            public void migrate() {
                migrator.migrate();
            }
        };
    }

}
