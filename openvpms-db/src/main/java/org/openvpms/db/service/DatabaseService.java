/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service;

import java.sql.SQLException;

/**
 * The {@link DatabaseService} provides database version information and migration support.
 *
 * @author Tim Anderson
 */
public interface DatabaseService {

    /**
     * Returns the schema name.
     *
     * @return the schema name
     */
    String getSchemaName();

    /**
     * Determines if the database exists.
     *
     * @param adminUser     the database administrator user name
     * @param adminPassword the database administrator password
     * @return {@code true} if the database exists, otherwise {@code false}
     * @throws SQLException for any SQL error
     */
    boolean exists(String adminUser, String adminPassword) throws SQLException;

    /**
     * Creates the database.
     *
     * @param adminUser     the database administrator user name
     * @param adminPassword the database administrator password
     * @param user          the user to create
     * @param password      the password of the user
     * @param host          the host the user is connecting from
     * @param createTables  if {@code true}, create the tables, and base-line
     * @throws SQLException for any SQL error
     */
    void create(String adminUser, String adminPassword, String user, String password, String host, boolean createTables)
            throws SQLException;

    /**
     * Determines if the database needs migration.
     *
     * @return {@code true} if the database needs migration
     */
    boolean needsUpdate();

    /**
     * Returns the number of migration steps that need to be applied to bring the database up-to-date.
     *
     * @return the number of migration steps, or {@code 0} if no update is required
     */
    int getChangesToApply();

    /**
     * Returns the current database version.
     *
     * @return the current database version, or {@code null} if it is not known
     */
    String getVersion();

    /**
     * Returns the version that the database needs to be migrated to, if it is out of date.
     * <p/>
     * If {@link #needsUpdate()} returns {@code true}, but no version is returned by this method, a repeatable migration
     * may need to be run.
     *
     * @return the version to migrate to, or {@code null} if a repeatable migration needs to be run or no migration is
     * required
     */
    String getMigrationVersion();

    /**
     * Updates the database to the latest version.
     *
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    void update(ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException;

    /**
     * Updates the database to a specific version.
     *
     * @param version           the migration version
     * @param archetypeMigrator the archetype migrator, or {@code null} if archetypes aren't being updated
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being updated
     * @throws SQLException for any error
     */
    void update(String version, ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException;
}