/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.v2_1;

import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.system.common.crypto.DefaultPasswordEncryptorFactory;
import org.openvpms.component.system.common.crypto.PasswordEncryptorFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Encrypts 3rd-party passwords of OVPMS-2233.
 *
 * @author Tim Anderson
 */
public class V2_1_0_10__OVPMS_2233 extends BaseJdbcMigration {

    /**
     * Executes this migration. The execution will automatically take place within a transaction, when the underlying
     * database supports it.
     *
     * @param connection The connection to use to execute statements.
     * @throws Exception when the migration failed.
     */
    @Override
    public void migrate(Connection connection) throws Exception {
        String key = System.getProperty("openvpms.key");
        if (key == null) {
            throw new SQLException("openvpms.key has not been defined");
        }
        PasswordEncryptorFactory factory = new DefaultPasswordEncryptorFactory(key);
        PasswordEncryptor encryptor = factory.create();

        encrypt("entities", "entity_details", "entity_id",
                "entity.mailServer", "password", connection, encryptor);
        encrypt("entity_relationships", "entity_relationship_details", "entity_relationship_id",
                "entityRelationship.supplierStockLocationESCI", "password", connection, encryptor);
        encrypt("entities", "entity_details", "entity_id",
                "party.organisationLocation", "smartFlowSheetKey", connection, encryptor);
        encrypt("entities", "entity_details", "entity_id",
                "entity.pluginDeputy", "accessToken", connection, encryptor);
        encrypt("entities", "entity_details", "entity_id",
                "entity.insuranceServicePetSure", "password", connection, encryptor);
    }

    /**
     * Encrypts passwords.
     *
     * @param table      the primary table
     * @param details    the details table
     * @param idColumn   the id column to join on
     * @param archetype  the archetype to update
     * @param name       the name of the node to update
     * @param connection the database connection
     * @param encryptor  the password encryptor
     * @throws SQLException for any SQL error
     */
    private void encrypt(String table, String details, String idColumn, String archetype,
                         String name, Connection connection, PasswordEncryptor encryptor) throws SQLException {
        Map<Long, String> passwords = new HashMap<>();
        String tableId = table + "." + idColumn;
        String detailsId = details + "." + idColumn;
        String select = "select " + tableId + ", " + details + ".value " +
                        "from " + table + " " +
                        "join " + details + " on " + tableId + " = " + detailsId + " " +
                        "where " + table + ".arch_short_name = ? and " + details + ".name = ?";
        try (PreparedStatement statement = connection.prepareStatement(select)) {
            statement.setString(1, archetype);
            statement.setString(2, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    long id = resultSet.getLong(1);
                    String password = resultSet.getString(2);
                    if (password != null && password.length() > 0) {
                        passwords.put(id, password);
                    }
                }
            }
        }
        if (!passwords.isEmpty()) {
            String update = "update " + table + " " +
                            "join " + details + " on " + tableId + " = " + detailsId + " " +
                            " set " + details + ".value = ? " +
                            "where " + tableId + " = ? and " + details + ".name = ? and " + details + ".value = ?";

            try (PreparedStatement statement = connection.prepareStatement(update)) {
                for (Map.Entry<Long, String> entry : passwords.entrySet()) {
                    long id = entry.getKey();
                    String plain = entry.getValue();
                    String encrypted = encryptor.encrypt(plain);
                    statement.setString(1, encrypted);
                    statement.setLong(2, id);
                    statement.setString(3, name);
                    statement.setString(4, plain);
                    statement.executeUpdate();
                }
            }
        }
    }

}
