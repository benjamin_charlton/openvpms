/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.service;

import org.openvpms.component.model.document.Document;
import org.openvpms.print.exception.PrinterException;

import javax.print.PrintService;


/**
 * Printer for {@link Document}s.
 *
 * @author Tim Anderson
 */
public interface DocumentPrinter {

    /**
     * Returns the printer identifier.
     *
     * @return the printer identifier
     */
    String getId();

    /**
     * Returns the printer name.
     *
     * @return the printer name
     */
    String getName();

    /**
     * Returns the archetype of the {@link DocumentPrinterService} that provides this printer.
     *
     * @return the printer service archetype, or {@code null} if the Java Print Service API provides the printer
     */
    String getArchetype();

    /**
     * Determines if the printer can be printed to using the Java Print Service API.
     * <p/>
     * When supported, the {@link #getId() identifier} can be used to look up the corresponding {@link PrintService}
     * by name.
     *
     * @return {@code true} if the printer supports the Java Print Service API, otherwise {@code false}
     */
    boolean canUseJavaPrintServiceAPI();

    /**
     * Determines if the printer can print the specified mime type.
     *
     * @param mimeType the mime type
     * @return {@code true} if the printer can print the specified mime type
     */
    boolean canPrint(String mimeType);

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param attributes the print attributes
     * @throws PrinterException for any printer error
     */
    void print(Document document, PrintAttributes attributes);

}