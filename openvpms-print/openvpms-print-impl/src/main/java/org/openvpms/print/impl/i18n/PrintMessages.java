/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;

/**
 * Print API messages.
 *
 * @author Tim Anderson
 */
public class PrintMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("PRT", PrintMessages.class.getName());

    /**
     * Messages to indicate that a document couldn't be printed.
     *
     * @param name      the document name
     * @param printName the printer name
     * @param message   the reason
     * @return a new message
     */
    public static Message failedToPrintDocument(String name, String printName, String message) {
        return messages.create(1, name, printName, message);
    }

    /**
     * Messages to indicate that a document is unsupported by a printer.
     *
     * @param name        the document name
     * @param printerName the printer name
     * @return a new message
     */
    public static Message unsupportedDocument(String name, String printerName) {
        return messages.create(2, name, printerName);
    }
}
