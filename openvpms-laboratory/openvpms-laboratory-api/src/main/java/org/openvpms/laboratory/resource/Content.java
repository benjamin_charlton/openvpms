/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.resource;

import java.io.InputStream;

/**
 * Content resource.
 *
 * @author Tim Anderson
 */
public interface Content extends Resource {

    /**
     * Returns a description of the content.
     *
     * @return the content description. May be {@code null}
     */
    String getDescription();

    /**
     * Returns the mime type of the content.
     *
     * @return the mime type
     */
    String getMimeType();

    /**
     * Returns the content size.
     *
     * @return the content size in bytes, or {@code -1} if not known
     */
    long getSize();

    /**
     * Returns the mime-types that this content may be retrieved as.
     *
     * @return the supported mime types
     */
    String[] getSupportedMimeTypes();

    /**
     * Returns a stream to the content.
     *
     * @return the content stream
     */
    InputStream getContent();

    /**
     * Returns a stream to the content, as the requested mime type.
     *
     * @param mimeType the mime type
     * @return the content stream
     */
    InputStream getContent(String mimeType);

    /**
     * Determines if the content can be scaled.
     *
     * @return {@code true} if the content can be scaled
     */
    boolean canScale();

    /**
     * Scales the content to the specified width and height, in pixels.
     * <p/>
     * This should preserve the aspect ratio of the original image.
     *
     * @param width  the width
     * @param height the height
     * @return the scaled content
     */
    InputStream getContent(int width, int height);

    /**
     * Scales the content to the specified width and height, in pixels.
     * <p/>
     * This should preserve the aspect ratio of the original image.
     *
     * @param width    the width
     * @param height   the height
     * @param mimeType the mime type
     * @return the scaled content
     */
    InputStream getContent(int width, int height, String mimeType);
}
