/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.object.Reference;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.patient.Patient;
import org.openvpms.laboratory.order.Order;

import java.time.OffsetDateTime;

/**
 * Executes queries for {@link Orders}.
 *
 * @author Tim Anderson
 */
public interface OrderQuery {

    /**
     * Returns orders for the specified investigation id.
     *
     * @param id the investigation identifier
     * @return this
     */
    OrderQuery investigationId(long id);

    /**
     * Returns orders for the specified order id.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @return this
     */
    OrderQuery orderId(String archetype, String id);

    /**
     * Returns orders for the specified laboratory.
     *
     * @param laboratory the laboratory
     * @return this
     */
    OrderQuery laboratory(Laboratory laboratory);

    /**
     * Returns orders for the specified laboratory.
     *
     * @param laboratory the laboratory reference
     * @return this
     */
    OrderQuery laboratory(Reference laboratory);

    /**
     * Returns orders that have a device.
     *
     * @return this
     */
    OrderQuery requireDevice();

    /**
     * Returns orders with devices of the specified type.
     *
     * @param archetype the device archetype
     * @return this
     */
    OrderQuery requireDevice(String archetype);

    /**
     * Returns orders for the specified device.
     * <p/>
     * This implies {@link #requireDevice()}.
     *
     * @param device the device
     * @return this
     */
    OrderQuery device(Device device);

    /**
     * Returns orders for the specified device.
     * <p/>
     * This implies {@link #requireDevice()}.
     *
     * @param device the device reference
     * @return this
     */
    OrderQuery device(Reference device);

    /**
     * Returns orders with the specified status.
     *
     * @param status the status
     * @return this
     */
    OrderQuery status(Order.Status status);

    /**
     * Returns orders created between the specified dates.
     *
     * @param from the start of the range, inclusive, or {@code null} if there is no restriction
     * @param to   the end of the range, exclusive, or {@code null} if there is no restriction
     * @return this
     */
    OrderQuery created(OffsetDateTime from, OffsetDateTime to);

    /**
     * Returns orders for the specified patient.
     *
     * @param patient the patient
     * @return this
     */
    OrderQuery patient(Patient patient);

    /**
     * Returns orders for patients matching the specified id.
     *
     * @param id the patient identifier
     * @return this
     */
    OrderQuery patient(long id);

    /**
     * Returns orders for patients matching the specified name.
     *
     * @param name the patient name
     * @param like if {@code true}, perform a 'like' match, else perform a case-insensitive equality match
     * @return this
     */
    OrderQuery patientName(String name, boolean like);

    /**
     * Runs the query.
     *
     * @return the matching orders
     */
    Iterable<Order> getResults();

    /**
     * Returns the first matching result.
     *
     * @return the first matching result, or {@code null} if there is none
     */
    Order getFirstResult();
}
