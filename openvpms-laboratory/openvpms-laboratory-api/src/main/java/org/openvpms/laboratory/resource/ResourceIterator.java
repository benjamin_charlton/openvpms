/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.resource;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Resource iterator.
 * <p/>
 * This supports random access to resources.
 *
 * @author Tim Anderson
 */
public interface ResourceIterator extends Iterator<Resource> {

    /**
     * Returns the position within the iterator. This is the 0-based index of the next element in the iterator, i.e.
     * the one that will be returned on the subsequent call to {@link #next()}.
     * <p/>
     * An empty iterator will always return 0.
     *
     * @return the current position
     */
    int getPosition();

    /**
     * Sets the position of the iterator. This is the 0-based index of the next element in the iterator, i.e.
     * the one that will be returned on the subsequent call to {@link #next()}.
     *
     * @param position the iterator position
     * @throws NoSuchElementException if skipped past the last element
     */
    void setPosition(int position);

    /**
     * Returns the number of resources available through this iterator.
     *
     * @return the number of resources or {@code -1} if it is not known
     */
    int getSize();

}
