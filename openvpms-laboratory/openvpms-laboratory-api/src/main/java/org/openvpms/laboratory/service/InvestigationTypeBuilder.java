/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.sync.Changes;

import java.util.List;

/**
 * Builder for {@link InvestigationType}s.
 *
 * @author Tim Anderson
 */
public interface InvestigationTypeBuilder {

    /**
     * Track changes.
     *
     * @param changes the changes
     * @return this
     */
    InvestigationTypeBuilder changes(Changes<Entity> changes);

    /**
     * Sets the investigation type id, used for identifying the investigation type.
     *
     * @param archetype the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param id        the investigation type id. This must be unique to the archetype
     * @return this
     */
    InvestigationTypeBuilder typeId(String archetype, String id);

    /**
     * Sets the investigation type id, used for identifying the investigation type.
     *
     * @param archetype the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param id        the investigation type id. This must be unique to the archetype
     * @param name      the investigation type id name
     * @return this
     */
    InvestigationTypeBuilder typeId(String archetype, String id, String name);

    /**
     * Sets the investigation type name.
     *
     * @param name the investigation type name
     * @return this
     */
    InvestigationTypeBuilder name(String name);

    /**
     * Sets the investigation type description.
     *
     * @param description the investigation type description
     * @return this
     */
    InvestigationTypeBuilder description(String description);

    /**
     * Determines if the investigation type is active or not.
     *
     * @param active if {@code true}, the investigation type is active and may be used. If {@code false} it may not be
     *               used
     * @return this
     */
    InvestigationTypeBuilder active(boolean active);

    /**
     * Sets the laboratory that manages the investigation type.
     *
     * @param laboratory the laboratory
     * @return this
     */
    InvestigationTypeBuilder laboratory(Laboratory laboratory);

    /**
     * Sets the devices that can perform tests for the investigation type.
     *
     * @param devices the devices
     * @return this
     */
    InvestigationTypeBuilder devices(List<Device> devices);

    /**
     * Builds the investigation type.
     * <p/>
     * The builder is reset.
     *
     * @return the investigation type
     */
    InvestigationType build();
}
