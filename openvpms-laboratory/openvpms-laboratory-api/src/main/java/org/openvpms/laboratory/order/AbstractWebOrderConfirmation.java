/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.order;

/**
 * Abstract implementation of {@link WebOrderConfirmation}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractWebOrderConfirmation implements WebOrderConfirmation {

    /**
     * The confirmation URL.
     */
    private final String url;

    /**
     * The window width.
     */
    private final int width;

    /**
     * The window height.
     */
    private final int height;

    /**
     * Constructs a {@link AbstractWebOrderConfirmation}.
     *
     * @param url     confirmation URL
     * @param width   the window width, in pixels
     * @param height  the window height, in pixels
     */
    public AbstractWebOrderConfirmation(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the order confirmation URL.
     *
     * @return the order confirmation URL
     */
    @Override
    public String getUrl() {
        return url;
    }

    /**
     * Returns the window width.
     *
     * @return the window width, in pixels
     */
    @Override
    public int getWidth() {
        return width;
    }

    /**
     * Returns the window height.
     *
     * @return the window height, in pixels
     */
    @Override
    public int getHeight() {
        return height;
    }
}
