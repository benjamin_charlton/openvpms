/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.order;

import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.practice.Location;
import org.openvpms.laboratory.exception.InvestigationNotFound;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * Laboratory order.
 *
 * @author Tim Anderson
 */
public interface Order {

    enum Type {
        NEW,        // new order
        CANCEL      // cancel order request
    }

    enum Status {
        PENDING,             // the request is yet to be submitted
        QUEUED,              // the request is queued for automatic submission
        CONFIRM,             // a request has been made, but needs further intervention before it can be submitted
        SUBMITTING,          // the request is being submitted. This is to support asynchronous laboratories
        SUBMITTED,           // the request has been submitted
        ERROR,               // the request has an unresolvable error
        COMPLETED,           // the request is complete
        CANCELLED            // the request is cancelled
    }

    /**
     * Returns the investigation identifier.
     *
     * @return the investigation identifier
     */
    long getInvestigationId();

    /**
     * Returns a universally unique identifier (UUID) for the order.
     *
     * @return the stringified form of the UUID
     */
    String getUUID();

    /**
     * Returns the order identifier, issued by the laboratory.
     * <p/>
     * This is short for: {@code getOrderIdentity().getIdentity()}
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    String getOrderId();

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @throws LaboratoryException if the identifier cannot be set
     */
    void setOrderId(String archetype, String id);

    /**
     * Returns the identity for the order, issued by the laboratory.
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    ActIdentity getOrderIdentity();

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param identity the order identifier
     * @throws LaboratoryException if the identifier cannot be set
     */
    void setOrderIdentity(ActIdentity identity);

    /**
     * Returns the order type.
     *
     * @return the order type
     */
    Type getType();

    /**
     * Returns the request status.
     *
     * @return the request status
     */
    Status getStatus();

    /**
     * Sets the request status.
     *
     * @param status the new request status
     * @throws LaboratoryException if the status cannot be set
     */
    void setStatus(Status status);

    /**
     * Sets the request status, along with any message from the laboratory.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     * @throws LaboratoryException if the status cannot be set
     */
    void setStatus(Status status, String message);

    /**
     * Returns the date when the order was created.
     *
     * @return the date
     */
    OffsetDateTime getCreated();

    /**
     * Returns the investigation type.
     *
     * @return the investigation type.
     */
    InvestigationType getInvestigationType();

    /**
     * Returns the tests being ordered.
     *
     * @return the tests being ordered
     */
    List<Test> getTests();

    /**
     * Returns the device to use to perform the tests.
     *
     * @return the device, {@code null} if no device was specified
     */
    Device getDevice();

    /**
     * Returns the laboratory used to perform the tests.
     *
     * @return the laboratory
     */
    Laboratory getLaboratory();

    /**
     * Returns the patient that the order is for.
     *
     * @return the patient
     */
    Patient getPatient();

    /**
     * Returns the customer responsible for the patient.
     *
     * @return the customer. May be {@code null}
     */
    Customer getCustomer();

    /**
     * Returns the clinician responsible for the order.
     *
     * @return the clinician. May be {@code null}
     */
    User getClinician();

    /**
     * Returns the practice location where the order was placed.
     *
     * @return the practice location
     */
    Location getLocation();

    /**
     * Returns the patient visit.
     *
     * @return the patient visit. May be {@code null}
     */
    Visit getVisit();

    /**
     * Returns the user that placed the order.
     *
     * @return the user that placed the order
     */
    User getUser();

    /**
     * Returns the order notes.
     *
     * @return the order notes. May be {@code null}
     */
    String getNotes();

    /**
     * Returns the laboratory report for the order.
     *
     * @return the report
     * @throws InvestigationNotFound if the investigation cannot be found
     */
    Report getReport();

    /**
     * Returns a report builder to build the report for this order.
     *
     * @return the report builder
     */
    ReportBuilder getReportBuilder();

}
