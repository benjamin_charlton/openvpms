/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.resource;

import java.time.OffsetDateTime;

/**
 * Laboratory resource.
 *
 * @author Tim Anderson
 */
public interface Resource {

    /**
     * Returns the resource identifier.
     *
     * @return the resource identifier
     */
    String getId();

    /**
     * Returns the resource name.
     *
     * @return the resource name
     */
    String getName();

    /**
     * Returns the date/time when the resource was created.
     *
     * @return the date/time. May be {@code null}
     */
    OffsetDateTime getCreated();

    /**
     * Returns the parent collection.
     *
     * @return the parent collection, or {@code null} if there is none
     */
    Resources getParent();

}
