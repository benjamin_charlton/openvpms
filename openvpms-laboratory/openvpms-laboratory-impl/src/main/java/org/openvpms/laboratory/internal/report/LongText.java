/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.archetype.rules.doc.LongTextReader;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Objects;

/**
 * Manages text nodes.
 * <p/>
 * If the text is too long to fit in the specified node, it is stored in a text document and linked via an
 * act relationship.
 *
 * @author Tim Anderson
 */
class LongText {

    /**
     * The parent bean.
     */
    private final IMObjectBean bean;

    /**
     * The text node.
     */
    private final String node;

    /**
     * The act relationship node, used when storing the text in an associated document act.
     */
    private final String longNode;

    /**
     * The document act archetype.
     */
    private final String archetype;

    /**
     * The relationship node on the document act.
     */
    private final String targetNode;

    /**
     * The name for the document used to store the text.
     */
    private final String documentName;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document act.
     */
    private DocumentAct act;

    /**
     * The current text.
     */
    private MutableObject<String> text;

    /**
     * Constructs a {@link LongText}.
     *
     * @param bean         the parent bean
     * @param node         the text node
     * @param longNode     the act relationship node, used when storing the text in an associated document act
     * @param archetype    the document act archetype
     * @param targetNode   the relationship node on the document act
     * @param documentName the name for the document used to store the text
     * @param service      the archetype service
     */
    public LongText(IMObjectBean bean, String node, String longNode, String archetype, String targetNode,
                    String documentName, ArchetypeService service) {
        this.bean = bean;
        this.node = node;
        this.longNode = longNode;
        this.archetype = archetype;
        this.targetNode = targetNode;
        this.documentName = documentName;
        this.service = service;
        act = bean.getTarget(longNode, DocumentAct.class);
    }

    /**
     * Returns the text.
     *
     * @return the text. May be {@code null}
     */
    public String getText() {
        if (text == null) {
            String value = bean.getString(node);
            if (act != null) {
                LongTextReader reader = new LongTextReader(service);
                value = reader.getText(act);
            }
            text = new MutableObject<>(value);
        }
        return text.getValue();
    }

    /**
     * Populates the text node, falling back to storing it in a document act if it exceeds the maximum length of the
     * node.
     *
     * @param value the text value. May be {@code null}
     * @param state the builder state
     */
    public void setText(String value, State state) {
        String current = getText();
        value = StringUtils.trimToNull(value);
        if (!Objects.equals(current, value)) {
            if (value != null) {
                if (value.length() <= bean.getMaxLength(node)) {
                    bean.setValue(node, value);
                    removeLongText(state);
                } else {
                    bean.setValue(node, null);
                    if (act == null) {
                        act = service.create(archetype, DocumentAct.class);
                        state.add(act);
                        bean.addTarget(longNode, act, targetNode);
                    }
                    if (act.getDocument() != null) {
                        // remove the existing document
                        state.remove(act.getDocument());
                    }
                    TextDocumentHandler handler = new TextDocumentHandler(service);
                    Document document = handler.create(documentName, value);
                    act.setDocument(document.getObjectReference());
                    state.add(document);
                    state.add(act);
                }
            } else {
                bean.setValue(node, null);
                removeLongText(state);
            }
            text.setValue(value);
        }
    }

    /**
     * Removes a long text act, if present.
     *
     * @param state the builder state
     */
    protected void removeLongText(State state) {
        if (act != null) {
            bean.removeTargets(longNode, act, targetNode);
            Reference document = act.getDocument();
            if (document != null) {
                act.setDocument(null);
                state.add(act);             // needs to be saved before the document can be removed
                state.remove(document);
            }
            state.remove(act);
        }
    }
}
