/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Path;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.patient.Patient;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.OrderQuery;

import javax.persistence.criteria.Predicate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Default implementation of {@link OrderQuery}.
 *
 * @author Tim Anderson
 */
public class OrderQueryImpl implements OrderQuery {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The order factory.
     */
    private final OrderFactory factory;

    /**
     * The investigation identifier.
     */
    private Long investigationId;

    /**
     * The order identifier archetype.
     */
    private String orderIdArchetype;

    /**
     * The order identifier.
     */
    private String orderId;

    /**
     * The laboratory.
     */
    private Reference laboratory;

    /**
     * Determines if a device is required.
     */
    private boolean requireDevice;

    /**
     * The device.
     */
    private Reference device;

    /**
     * The device archetype.
     */
    private String deviceArchetype;

    /**
     * The order status.
     */
    private Order.Status status;

    /**
     * If specified, only returns those orders created on or after the specified time.
     */
    private OffsetDateTime createdFrom;

    /**
     * If specified, only returns those orders up to the specified time (exclusive).
     */
    private OffsetDateTime createdTo;

    /**
     * The patient identifier.
     */
    private Long patientId;

    /**
     * The patient name.
     */
    private String patientName;

    /**
     * Determines if the patient name should be a 'like' match.
     */
    private boolean patientNameLike;

    /**
     * Constructs an {@link OrderQueryImpl}.
     *
     * @param service the archetype service
     * @param factory the order factory
     */
    public OrderQueryImpl(ArchetypeService service, OrderFactory factory) {
        this.service = service;
        this.factory = factory;
    }

    /**
     * Returns orders for the specified investigation id.
     *
     * @param id the investigation identifier
     * @return this
     */
    @Override
    public OrderQuery investigationId(long id) {
        this.investigationId = id;
        return this;
    }

    /**
     * Returns orders for the specified order id.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @return this
     */
    @Override
    public OrderQuery orderId(String archetype, String id) {
        if (!TypeHelper.matches(archetype, LaboratoryArchetypes.LAB_ORDER_IDS)) {
            throw new IllegalStateException("Invalid laboratory order archetype: " + archetype);
        }
        this.orderIdArchetype = archetype;
        this.orderId = id;
        return this;
    }

    /**
     * Returns orders for the specified laboratory.
     *
     * @param laboratory the laboratory
     * @return this
     */
    @Override
    public OrderQuery laboratory(Laboratory laboratory) {
        return laboratory(laboratory != null ? laboratory.getObjectReference() : null);
    }

    /**
     * Returns orders for the specified laboratory.
     *
     * @param laboratory the laboratory reference
     * @return this
     */
    @Override
    public OrderQuery laboratory(Reference laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Returns orders for the specified device.
     *
     * @param device the device
     * @return this
     */
    @Override
    public OrderQuery device(Device device) {
        return device(device != null ? device.getObjectReference() : null);
    }

    /**
     * Returns orders for the specified device.
     *
     * @param device the device reference
     * @return this
     */
    @Override
    public OrderQuery device(Reference device) {
        this.device = device;
        return this;
    }

    /**
     * Returns orders that have a device.
     *
     * @return this
     */
    @Override
    public OrderQuery requireDevice() {
        return requireDevice(null);
    }

    /**
     * Returns orders with devices of the specified type.
     *
     * @param archetype the device archetype
     * @return this
     */
    @Override
    public OrderQuery requireDevice(String archetype) {
        requireDevice = true;
        deviceArchetype = archetype;
        return this;
    }

    /**
     * Returns orders with the specified status.
     *
     * @param status the status
     * @return this
     */
    @Override
    public OrderQuery status(Order.Status status) {
        this.status = status;
        return this;
    }

    /**
     * Returns orders created between the specified dates.
     *
     * @param from the start of the range, inclusive, or {@code null} if there is no restriction
     * @param to   the end of the range, exclusive, or {@code null} if there is no restriction
     * @return this
     */
    @Override
    public OrderQuery created(OffsetDateTime from, OffsetDateTime to) {
        this.createdFrom = from;
        this.createdTo = to;
        return this;
    }

    /**
     * Returns orders for the specified patient.
     *
     * @param patient the patient
     * @return this
     */
    @Override
    public OrderQuery patient(Patient patient) {
        if (patient == null) {
            patientId = null;
            return this;
        }  else {
            return patient(patient.getId());
        }
    }

    /**
     * Returns orders for patients matching the specified id.
     *
     * @param id the patient identifier
     * @return this
     */
    @Override
    public OrderQuery patient(long id) {
        this.patientId = id;
        return this;
    }

    /**
     * Returns orders for patients matching the specified name.
     *
     * @param name  the patient name
     * @param like if {@code true}, perform a 'like' match, else perform a case-insensitive equality match
     * @return this
     */
    @Override
    public OrderQuery patientName(String name, boolean like) {
        this.patientName = name;
        this.patientNameLike = like;
        return this;
    }

    /**
     * Runs the query.
     *
     * @return the matching orders
     */
    @Override
    public Iterable<Order> getResults() {
        TypedQuery<Act> query = build();
        return () -> new OrderIterator(new TypedQueryIterator<>(query, 100), factory);
    }

    /**
     * Returns the first matching result.
     *
     * @return the first matching result, or {@code null} if there is none
     */
    @Override
    public Order getFirstResult() {
        TypedQuery<Act> query = build();
        Act act = query.getFirstResult();
        return act != null ? factory.create(act) : null;
    }

    /**
     * Builds the query.
     *
     * @return the query
     */
    protected TypedQuery<Act> build() {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, LaboratoryArchetypes.ORDER).alias("act");
        query.select(root);
        if (investigationId != null) {
            Join<Act, IMObject> idJoin = root.join("investigationId").alias("investigationId");
            idJoin.on(builder.equal(idJoin.get("identity"), investigationId.toString()));
        }
        if (orderId != null) {
            Join<Act, IMObject> idJoin = root.join("orderId", orderIdArchetype).alias("orderId");
            idJoin.on(builder.equal(idJoin.get("identity"), orderId));
        }
        if (laboratory != null) {
            Join<Act, EntityLink> laboratoryJoin = root.join("laboratory");
            laboratoryJoin.on(builder.equal(laboratoryJoin.get("entity").alias("laboratory"), laboratory));
        }
        if (device != null || requireDevice) {
            Join<Act, EntityLink> deviceJoin = root.join("device");
            if (device != null) {
                deviceJoin.on(builder.equal(deviceJoin.get("entity"), device));
            }
            if (deviceArchetype != null) {
                deviceJoin.join("entity", deviceArchetype).alias("device");
            }
        }
        if (patientId != null || patientName != null) {
            Join<IMObject, IMObject> patientJoin = root.join("patient").join("entity").alias("patient");
            if (patientId != null) {
                patientJoin.on(builder.equal(patientJoin.get("id"), patientId));
            }
            if (patientName != null) {
                Path<String> name = patientJoin.get("name");
                if (patientNameLike) {
                    patientJoin.on(builder.like(name, patientName));
                } else {
                    patientJoin.on(builder.equal(name, patientName));
                }
            }
        }
        List<Predicate> predicates = new ArrayList<>();
        if (status != null) {
            predicates.add(builder.equal(root.get("status"), status.toString()));
        }
        if (createdFrom != null) {
            predicates.add(builder.greaterThanOrEqualTo(root.get("startTime"), DateRules.toDate(createdFrom)));
        }
        if (createdTo != null) {
            predicates.add(builder.lessThan(root.get("startTime"), DateRules.toDate(createdTo)));
        }
        if (!predicates.isEmpty()) {
            query.where(predicates);
        }
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query);
    }

    /**
     * Adapts an iterator of <em>act.supplierOrder</em> instances to their corresponding {@link Order}.
     */
    private static class OrderIterator implements Iterator<Order> {

        private Iterator<Act> iterator;

        private OrderFactory factory;

        public OrderIterator(Iterator<Act> iterator, OrderFactory factory) {
            this.iterator = iterator;
            this.factory = factory;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         */
        @Override
        public Order next() {
            Act act = iterator.next();
            return factory.create(act);
        }
    }
}
