/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.openvpms.archetype.rules.doc.LongTextReader;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.act.BeanActDecorator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.laboratory.report.Result;
import org.openvpms.laboratory.report.Results;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Default implementation of {@link Results}.
 *
 * @author Tim Anderson
 */
public class ResultsImpl implements Results {

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The underlying act.
     */
    private final BeanActDecorator act;

    /**
     * The long text node reader.
     */
    private final LongTextReader reader;

    /**
     * The results.
     */
    private List<Result> results;

    /**
     * Results id node name.
     */
    static final String RESULTS_ID = "resultsId";

    /**
     * Test node name.
     */
    static final String TEST = "test";

    /**
     * Category code node name.
     */
    static final String CATEGORY_CODE = "categoryCode";

    /**
     * Category node name.
     */
    static final String CATEGORY_NAME = "categoryName";

    /**
     * Result items node name.
     */
    static final String ITEMS = "items";


    /**
     * Notes node name.
     */
    static final String NOTES = "notes";

    /**
     * Long notes node name.
     */
    static final String LONG_NOTES = "longNotes";

    /**
     * Constructs a {@link ResultsImpl}.
     *
     * @param act           the act to delegate to
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public ResultsImpl(Act act, ArchetypeService service, DomainService domainService) {
        this.act = new BeanActDecorator(act, domainService);
        this.domainService = domainService;
        reader = new LongTextReader(service);
    }

    /**
     * Constructs a {@link ResultsImpl}.
     *
     * @param bean          the bean wrapping the act
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public ResultsImpl(IMObjectBean bean, ArchetypeService service, DomainService domainService) {
        this.act = new BeanActDecorator(bean);
        this.domainService = domainService;
        reader = new LongTextReader(service);
    }

    /**
     * Returns the results identifier.
     *
     * @return the results identifier
     */
    @Override
    public String getResultsId() {
        return act.getBean().getString(RESULTS_ID);
    }

    /**
     * Returns the results status.
     *
     * @return the results status
     */
    @Override
    public Result.Status getStatus() {
        return ResultImpl.getStatus(act.getStatus());
    }

    /**
     * Returns the date/time when the results where produced.
     *
     * @return the date/time
     */
    @Override
    public OffsetDateTime getDate() {
        return DateRules.toOffsetDateTime(act.getActivityStartTime());
    }

    /**
     * Returns the test.
     *
     * @return the test. May be {@code null}
     */
    @Override
    public Test getTest() {
        Entity test = act.getBean().getTarget(TEST, Entity.class);
        return test != null ? domainService.create(test, Test.class) : null;
    }

    /**
     * Returns the result category code.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @return the result category code. May be {@code null}
     */
    @Override
    public String getCategoryCode() {
        return act.getBean().getString(CATEGORY_CODE);
    }

    /**
     * Returns the result category name.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @return the result category name. May be {@code null}
     */
    @Override
    public String getCategoryName() {
        return act.getBean().getString(CATEGORY_NAME);
    }

    /**
     * Returns a short narrative of the results.
     *
     * @return the notes. May be {@code null}
     */
    @Override
    public String getNotes() {
        return reader.getText(act, NOTES, LONG_NOTES);
    }

    /**
     * Returns the results.
     *
     * @return the results, in the order they were added
     */
    @Override
    public List<Result> getResults() {
        if (results == null) {
            List<Result> list = new ArrayList<>();
            Comparator<SequencedRelationship> comparator = Comparator.comparingInt(SequencedRelationship::getSequence);
            Policy<SequencedRelationship> policy = Policies.all(SequencedRelationship.class, comparator);
            for (Act act : act.getBean().getTargets(ITEMS, Act.class, policy)) {
                list.add(domainService.create(act, Result.class));
            }
            results = list;
        }
        return results;
    }

    /**
     * Returns the underlying act.
     *
     * @return the act
     */
    public Act getAct() {
        return act;
    }
}
