/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.laboratory.Test.UseDevice;
import org.openvpms.laboratory.service.TestBuilder;
import org.openvpms.laboratory.service.Tests;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Default implementation of {@link TestBuilder}.
 *
 * @author Tim Anderson
 */
class TestBuilderImpl extends EntityBuilder<Test, TestBuilderImpl> implements TestBuilder {

    /**
     * The tests.
     */
    private final Tests tests;

    /**
     * The investigation type.
     */
    private InvestigationType investigationType;

    /**
     * Determines if the test can be grouped.
     */
    private Boolean group;

    /**
     * Determines if a device is required when ordering the test.
     */
    private UseDevice useDevice;

    /**
     * The turnaround notes.
     */
    private String turnaround;

    /**
     * Determines if the turnaround notes were supplied.
     */
    private boolean turnaroundSet;

    /**
     * The specimen notes.
     */
    private String specimen;

    /**
     * Determines if the specimen notes were supplied.
     */
    private boolean specimenSet;

    /**
     * The price, excluding tax.
     */
    private BigDecimal price;

    /**
     * Constructs a {@link TestBuilderImpl}.
     *
     * @param tests              the tests
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    TestBuilderImpl(Tests tests, ArchetypeService service, PlatformTransactionManager transactionManager,
                    DomainService domainService) {
        super(LaboratoryArchetypes.TEST, LaboratoryArchetypes.TEST_CODES, Test.class, service, transactionManager,
              domainService);
        this.tests = tests;
        reset();
    }

    /**
     * Sets the test code, used for identifying the test.
     *
     * @param archetype the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param code      the test code
     * @return this
     */
    @Override
    public TestBuilder testCode(String archetype, String code) {
        return entityId(archetype, code);
    }

    /**
     * Sets the display name for the test code.
     *
     * @param name the display name for the test code
     * @return this
     */
    @Override
    public TestBuilder testCodeName(String name) {
        return entityIdName(name);
    }

    /**
     * Sets the test's investigation type.
     *
     * @param type the investigation type
     * @return this
     */
    @Override
    public TestBuilder investigationType(InvestigationType type) {
        investigationType = type;
        return this;
    }

    /**
     * Determines if the test can be grouped with other tests in an order.
     *
     * @param group if {@code true}, the test can be grouped with other tests, otherwise it must be submitted
     *              individually
     * @return this
     */
    @Override
    public TestBuilder group(boolean group) {
        this.group = group;
        return this;
    }

    /**
     * Determines if a laboratory device must be specified when this test is ordered.
     *
     * @param useDevice if {@link UseDevice#YES YES}, a device must be specified; if {@link UseDevice#NO NO} no device
     *                  should be specified; if {@link UseDevice#OPTIONAL OPTIONAL}, a device may be specified
     */
    @Override
    public TestBuilder useDevice(UseDevice useDevice) {
        this.useDevice = useDevice;
        return this;
    }

    /**
     * Sets the test's turnaround notes.
     * <p/>
     * This is free-form text indicating how long the test is expected to take.
     *
     * @param notes the notes
     * @return this
     */
    @Override
    public TestBuilder turnaround(String notes) {
        this.turnaround = StringUtils.trimToNull(notes);
        turnaroundSet = true;
        return this;
    }

    /**
     * Sets the test's specimen collection notes.
     *
     * @param notes the notes
     * @return this
     */
    @Override
    public TestBuilder specimen(String notes) {
        this.specimen = StringUtils.trimToNull(notes);
        specimenSet = true;
        return this;
    }

    /**
     * Sets the test price.
     *
     * @param price the price, excluding tax
     * @return this
     */
    @Override
    public TestBuilder price(BigDecimal price) {
        this.price = price;
        return this;
    }

    /**
     * Returns the entity corresponding to the identifier.
     *
     * @param archetype the entity id archetype
     * @param id        the entity id
     * @return the entity, or {@code null} if none exists
     */
    @Override
    protected Entity getEntity(String archetype, String id) {
        return tests.getTest(archetype, id);
    }

    /**
     * Populates a test.
     *
     * @param test the test to populate
     * @return {@code true} if the test was updated
     */
    @Override
    protected boolean populate(Entity test) {
        boolean changed = super.populate(test);
        IMObjectBean bean = getService().getBean(test);

        if (investigationType != null) {
            Reference typeRef = investigationType.getObjectReference();
            if (!Objects.equals(typeRef, bean.getTargetRef("investigationType"))) {
                bean.setTarget("investigationType", typeRef);
                changed = true;
            }
        }

        if (group != null) {
            changed |= change("group", group, bean);
        }

        if (turnaroundSet) {
            changed |= change("turnaround", turnaround, bean);
        }

        if (specimenSet) {
            changed |= change("specimen", specimen, bean);
        }

        if (price != null) {
            changed |= change("price", price, bean);
        }

        if (useDevice != null) {
            changed |= change("useDevice", useDevice.toString(), bean);
        }

        return changed;
    }

    /**
     * Resets the builder.
     */
    @Override
    protected void reset() {
        super.reset();
        group = null;
        turnaround = null;
        turnaroundSet = false;
        specimen = null;
        specimenSet = false;
        useDevice = null;
        price = null;
    }
}
