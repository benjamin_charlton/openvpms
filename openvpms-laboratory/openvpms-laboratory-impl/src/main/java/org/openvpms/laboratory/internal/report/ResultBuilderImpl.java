/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.ImageDocumentHandler;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.report.Result;
import org.openvpms.laboratory.report.ResultBuilder;
import org.openvpms.laboratory.report.ResultsBuilder;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import static org.openvpms.laboratory.internal.report.ResultImpl.ANALYTE_CODE;
import static org.openvpms.laboratory.internal.report.ResultImpl.ANALYTE_NAME;
import static org.openvpms.laboratory.internal.report.ResultImpl.EXTREME_HIGH_RANGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.EXTREME_LOW_RANGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.HIGH_RANGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.IMAGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.LONG_RESULT;
import static org.openvpms.laboratory.internal.report.ResultImpl.LOW_RANGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.OUT_OF_RANGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.QUALIFIER;
import static org.openvpms.laboratory.internal.report.ResultImpl.REFERENCE_RANGE;
import static org.openvpms.laboratory.internal.report.ResultImpl.RESULT;
import static org.openvpms.laboratory.internal.report.ResultImpl.RESULT_ID;
import static org.openvpms.laboratory.internal.report.ResultImpl.STATUS;
import static org.openvpms.laboratory.internal.report.ResultImpl.UNITS;
import static org.openvpms.laboratory.internal.report.ResultImpl.VALUE;

/**
 * Default implementation of {@link ResultBuilder}.
 *
 * @author Tim Anderson
 */
class ResultBuilderImpl extends Builder<Result> implements ResultBuilder {

    /**
     * The builder state.
     */
    private final State state;

    /**
     * The objects to save.
     */
    private final List<Act> toSave;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The act to update.
     */
    private IMObjectBean bean;

    /**
     * The result identifier.
     */
    private String resultId;

    /**
     * The result status.
     */
    private Result.Status status;

    /**
     * The analyte code.
     */
    private String analyteCode;

    /**
     * The analyte name.
     */
    private String analyteName;

    /**
     * The numeric result for the analyte.
     */
    private BigDecimal value;

    /**
     * The text result for the analyte.
     */
    private String textResult;

    /**
     * The text result, stored in a document act if they are too long.
     */
    private LongText longTextResult;

    /**
     * The units of measure for the result.
     */
    private String units;

    /**
     * The qualifier.
     */
    private String qualifier;

    /**
     * The low reference range value for the analyte.
     */
    private BigDecimal lowRange;

    /**
     * The high reference range value for the analyte.
     */
    private BigDecimal highRange;

    /**
     * The extreme low reference range value for the analyte.
     */
    private BigDecimal extremeLowRange;

    /**
     * The extreme high reference range value for the analyte.
     */
    private BigDecimal extremeHighRange;

    /**
     * Determines if the result is out of range. May be {@code null}
     */
    private Boolean outOfRange;

    /**
     * The reference range.
     */
    private String referenceRange;

    /**
     * The notes.
     */
    private String notes;

    /**
     * The notes, stored in a document act if they are too long.
     */
    private LongText longNotes;

    /**
     * The image act.
     */
    private DocumentAct imageAct;

    /**
     * The image.
     */
    private Document image;

    /**
     * Constructs a {@link ResultBuilderImpl}.
     *
     * @param parent   the parent builder
     * @param id       the result identifier
     * @param results  the parent results act, if it is being updated, otherwise {@code null}
     * @param state    the builder state
     * @param service  the archetype service
     * @param handlers the document handlers
     * @param toSave   the objects to save
     */
    ResultBuilderImpl(ResultsBuilderImpl parent, String id, Act results, State state, ArchetypeService service,
                      DocumentHandlers handlers, List<Act> toSave) {
        super(parent);
        this.state = state;
        this.toSave = toSave;
        this.service = service;
        this.handlers = handlers;
        bean = (results != null) ? state.getResultItem(results, id) : null;
        if (bean != null) {
            extract(bean);
        } else {
            reset();
            resultId = id;
        }
    }

    /**
     * Sets the result status.
     *
     * @param status the result status
     * @return this
     */
    @Override
    public ResultBuilder status(Result.Status status) {
        this.status = status;
        return this;
    }

    /**
     * Sets the identifier for the analyte.
     *
     * @param code the analyte identifier. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder analyteCode(String code) {
        this.analyteCode = StringUtils.trimToNull(code);
        return this;
    }

    /**
     * Sets the name of the analyte for which the result is being reported.
     *
     * @param name the analyte name
     * @return this
     */
    @Override
    public ResultBuilder analyteName(String name) {
        this.analyteName = StringUtils.trimToNull(name);
        return this;
    }

    /**
     * Sets the numerical result for the analyte.
     *
     * @param value the numerical result for the analyte. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder value(BigDecimal value) {
        this.value = value;
        return this;
    }

    /**
     * Sets the result for the analyte, as text.
     *
     * @param result the result text. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder result(String result) {
        this.textResult = StringUtils.trimToNull(result);
        return this;
    }

    /**
     * Sets the unit of measure for the analyte result.
     *
     * @param units the unit of measure. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder units(String units) {
        this.units = StringUtils.trimToNull(units);
        return this;
    }

    /**
     * Sets the qualifier.
     *
     * @param qualifier the qualifier. May be {@code null}
     */
    @Override
    public ResultBuilder qualifier(String qualifier) {
        this.qualifier = StringUtils.trimToNull(qualifier);
        return this;
    }

    /**
     * Sets the low reference range value for the analyte.
     *
     * @param lowRange the low reference range value. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder lowRange(BigDecimal lowRange) {
        this.lowRange = lowRange;
        return this;
    }

    /**
     * Sets the high reference range value for the analyte.
     *
     * @param highRange the high reference range value. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder highRange(BigDecimal highRange) {
        this.highRange = highRange;
        return this;
    }

    /**
     * Sets the extreme low reference range value for the analyte.
     *
     * @param extremeLowRange the extreme low reference range value. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder extremeLowRange(BigDecimal extremeLowRange) {
        this.extremeLowRange = extremeLowRange;
        return this;
    }

    /**
     * Sets the extreme high reference range value for the analyte.
     *
     * @param extremeHighRange the extreme high reference range value. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder extremeHighRange(BigDecimal extremeHighRange) {
        this.extremeHighRange = extremeHighRange;
        return this;
    }

    /**
     * Determines if the result is out of range.
     *
     * @param outOfRange if {@code true}, the result is out of range
     */
    @Override
    public ResultBuilder outOfRange(boolean outOfRange) {
        this.outOfRange = outOfRange;
        return this;
    }

    /**
     * Sets the reference range.
     *
     * @param range the reference range. May be {@code null}
     */
    @Override
    public ResultBuilder referenceRange(String range) {
        this.referenceRange = StringUtils.trimToNull(range);
        return this;
    }

    /**
     * Sets a short narrative of the result.
     *
     * @param notes the result narrative. May be {@code null}
     * @return this
     */
    @Override
    public ResultBuilder notes(String notes) {
        this.notes = StringUtils.trimToNull(notes);
        return this;
    }

    /**
     * Sets the image associated with the results.
     *
     * @param fileName the file name
     * @param mimeType the mime type
     * @param stream   the stream to the image content
     * @return this
     */
    @Override
    public ResultBuilder image(String fileName, String mimeType, InputStream stream) {
        ImageDocumentHandler handler = new ImageDocumentHandler(service);
        if (StringUtils.isEmpty(mimeType) || !handler.canHandle(fileName, mimeType)) {
            throw new LaboratoryException(LaboratoryMessages.unsupportedImage(fileName, mimeType));
        }
        image = handler.create(fileName, stream, mimeType, -1);
        return this;
    }

    /**
     * Builds the result and adds it to the set of results.
     * <p/>
     * The builder is reset.
     *
     * @return the result
     */
    @Override
    public Result build() {
        return new ResultImpl(buildResult(), service, handlers);
    }

    /**
     * Builds the result and adds it to the set of results.
     *
     * @return the parent builder
     */
    @Override
    public ResultsBuilder add() {
        buildResult();
        return (ResultsBuilder) getParent();
    }

    /**
     * Populates a result.
     *
     * @param bean the result to populate
     */
    protected void populate(IMObjectBean bean) {
        bean.setValue(RESULT_ID, resultId);
        if (status != null) {
            bean.setValue(STATUS, status.toString());
        }
        bean.setValue(ANALYTE_CODE, analyteCode);
        bean.setValue(ANALYTE_NAME, analyteName);
        bean.setValue(VALUE, value);

        if (longTextResult == null) {
            longTextResult = createLongTextResult(bean);
        }
        if (textResult == null && value != null) {
            textResult = new DecimalFormat("#,###.#####").format(value);
        }
        longTextResult.setText(textResult, state);

        bean.setValue(UNITS, units);
        bean.setValue(QUALIFIER, qualifier);
        bean.setValue(LOW_RANGE, lowRange);
        bean.setValue(HIGH_RANGE, highRange);
        bean.setValue(EXTREME_LOW_RANGE, extremeLowRange);
        bean.setValue(EXTREME_HIGH_RANGE, extremeHighRange);
        bean.setValue(OUT_OF_RANGE, getOutOfRange());
        bean.setValue(REFERENCE_RANGE, getReferenceRange());

        if (longNotes == null) {
            longNotes = createLongNotes(bean);
        }
        longNotes.setText(notes, state);
        if (image != null) {
            if (imageAct == null) {
                imageAct = create(InvestigationArchetypes.RESULT_IMAGE, DocumentAct.class);
                bean.addTarget(IMAGE, imageAct, PARENT);
            } else if (imageAct.getDocument() != null) {
                // need to remove the existing document
                state.remove(imageAct.getDocument());
            }
            imageAct.setFileName(image.getName());
            imageAct.setMimeType(image.getMimeType());
            imageAct.setDocument(image.getObjectReference());
            state.add(image);
            state.add(imageAct);
        }
    }

    /**
     * Use the values from the specified result.
     *
     * @param bean the result bean
     */
    private void extract(IMObjectBean bean) {
        resultId = bean.getString(RESULT_ID);
        status = ResultImpl.getStatus(bean.getString(STATUS));
        analyteCode = bean.getString(ANALYTE_CODE);
        analyteName = bean.getString(ANALYTE_NAME);
        value = bean.getBigDecimal(VALUE);
        longTextResult = createLongTextResult(bean);
        textResult = longTextResult.getText();
        units = bean.getString(UNITS);
        qualifier = bean.getString(QUALIFIER);
        lowRange = bean.getBigDecimal(LOW_RANGE);
        highRange = bean.getBigDecimal(HIGH_RANGE);
        extremeLowRange = bean.getBigDecimal(EXTREME_LOW_RANGE);
        extremeHighRange = bean.getBigDecimal(EXTREME_HIGH_RANGE);
        outOfRange = (Boolean) bean.getValue(OUT_OF_RANGE);
        referenceRange = bean.getString(REFERENCE_RANGE);
        longNotes = createLongNotes(bean);
        notes = longNotes.getText();
        imageAct = bean.getTarget(IMAGE, DocumentAct.class);
    }

    /**
     * Resets the builder.
     */
    private void reset() {
        resultId = null;
        status = null;
        analyteCode = null;
        analyteName = null;
        value = null;
        textResult = null;
        longTextResult = null;
        units = null;
        qualifier = null;
        lowRange = null;
        highRange = null;
        extremeLowRange = null;
        extremeHighRange = null;
        outOfRange = null;
        referenceRange = null;
        notes = null;
        longNotes = null;
        image = null;
        imageAct = null;
    }

    /**
     * Creates a {@link LongText} for the result node.
     *
     * @param bean the result bean
     * @return a {@link LongText} to manage the result node
     */
    private LongText createLongTextResult(IMObjectBean bean) {
        return new LongText(bean, RESULT, LONG_RESULT, InvestigationArchetypes.RESULT_TEXT, PARENT, "Long Result",
                            getService());
    }

    /**
     * Determines if the result is out of range, if no flag was specified, but the value and either
     * the extreme low/high or low/high ranges are.
     *
     * @return {@code true} if the result is out of range, {@code false} if it is in range, or {@code null} if it cannot
     * be determined
     */
    private Boolean getOutOfRange() {
        if (outOfRange == null && value != null) {
            if (extremeLowRange != null && extremeHighRange != null) {
                return (value.compareTo(extremeLowRange) < 0 || value.compareTo(extremeHighRange) > 0);
            } else if (lowRange != null && highRange != null) {
                return (value.compareTo(lowRange) < 0 || value.compareTo(highRange) > 0);
            }
        }
        return outOfRange;
    }

    /**
     * Returns the reference range, deriving it from the extreme low/high range or low/high range and units if none is
     * specified.
     *
     * @return the reference range. May be {@code null}
     */
    private String getReferenceRange() {
        String result = referenceRange;
        if (result == null) {
            if (extremeLowRange != null && extremeHighRange != null) {
                result = getReferenceRange(extremeLowRange, extremeHighRange);
            } else if (lowRange != null && highRange != null) {
                result = getReferenceRange(lowRange, highRange);
            }
        }
        return result;
    }

    /**
     * Formats a reference range.
     *
     * @param low  the low value
     * @param high the high value
     * @return the formatted range
     */
    private String getReferenceRange(BigDecimal low, BigDecimal high) {
        DecimalFormat format = new DecimalFormat("#,###.#####");
        StringBuilder builder = new StringBuilder();
        builder.append(format.format(low))
                .append(" - ")
                .append(format.format(high));
        if (units != null) {
            builder.append(' ').append(units);
        }
        return builder.toString();
    }

    /**
     * Builds the result.
     *
     * @return the result bean
     */
    private IMObjectBean buildResult() {
        if (resultId == null) {
            throw new LaboratoryException(LaboratoryMessages.noResultId());
        }
        Act act;
        if (bean == null) {
            act = create(InvestigationArchetypes.RESULT, Act.class);
            bean = getBean(act);
        } else {
            act = bean.getObject(Act.class);
        }
        populate(bean);
        toSave.add(act);
        IMObjectBean result = bean;
        reset();
        return result;
    }

}
