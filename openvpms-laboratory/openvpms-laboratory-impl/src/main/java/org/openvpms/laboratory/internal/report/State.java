/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Builder state.
 *
 * @author Tim Anderson
 */
class State {

    /**
     * The investigation to update.
     */
    private final Act investigation;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The results.
     */
    private Set<Act> results;

    /**
     * The result items, keyed on result.
     */
    private final Map<Act, Set<Act>> items = new HashMap<>();

    /**
     * The objects to save. This is a LinkedHashSet so that act relationships can be sequenced in the order
     * that the objects were added.
     */
    private final Set<IMObject> toSave = new LinkedHashSet<>();

    /**
     * The objects to remove.
     */
    private final Set<Reference> toRemove = new LinkedHashSet<>();

    /**
     * Constructs a {@link State}
     *
     * @param investigation the investigation to update
     * @param service       the archetype service
     */
    State(Act investigation, ArchetypeService service) {
        this.investigation = investigation;
        this.service = service;
    }

    /**
     * Adds an object to save.
     *
     * @param object the object to save
     */
    public void add(IMObject object) {
        toSave.add(object);
    }

    /**
     * Adds objects to save.
     *
     * @param objects the objects to save
     */
    public void add(Collection<? extends IMObject> objects) {
        toSave.addAll(objects);
    }

    /**
     * Adds an object to remove.
     *
     * @param object the object to remove
     */
    public void remove(IMObject object) {
        remove(object.getObjectReference());
    }

    /**
     * Adds an object to remove.
     *
     * @param reference the object reference
     */
    public void remove(Reference reference) {
        toRemove.add(reference);
    }

    /**
     * Adds objects to remove.
     *
     * @param references the references to the objects to remove
     */
    public void remove(Collection<? extends Reference> references) {
        toRemove.addAll(references);
    }

    /**
     * Returns the new objects for a given archetype.
     *
     * @param archetype the archetype
     * @param type      the expected type
     * @return the new objects for the archetype
     */
    public <T> List<T> getNew(String archetype, Class<T> type) {
        List<T> result = new ArrayList<>();
        for (IMObject object : toSave) {
            if (object.isNew() && object.isA(archetype)) {
                result.add(type.cast(object));
            }
        }
        return result;
    }

    /**
     * Returns the report results.
     *
     * @return the results
     */
    public Set<Act> getResults() {
        if (results == null) {
            results = new HashSet<>(service.getBean(investigation).getTargets("results", Act.class));
        }
        return results;
    }

    /**
     * Returns the result for the given identifier.
     *
     * @param id the results identifier
     * @return the results bean or {@code null} if none is found
     */
    public IMObjectBean getResults(String id) {
        for (Act act : getResults()) {
            IMObjectBean bean = service.getBean(act);
            if (id.equals(bean.getString("resultsId"))) {
                return bean;
            }
        }
        return null;
    }

    /**
     * Returns the result item from a given results act.
     *
     * @param act the results act
     * @param id  the result identifier
     * @return the corresponding result bean or {@code null} if none is found
     */
    public IMObjectBean getResultItem(Act act, String id) {
        Set<Act> set = getResultItems(act);
        for (Act item : set) {
            IMObjectBean bean = service.getBean(item);
            if (id.equals(bean.getString("resultId"))) {
                return bean;
            }
        }
        return null;
    }

    /**
     * Returns the result items for a results act..
     *
     * @param act the results act
     * @return the corresponding result bean or {@code null} if none is found
     */
    public Set<Act> getResultItems(Act act) {
        Set<Act> set = items.get(act);
        if (set == null) {
            IMObjectBean bean = service.getBean(act);
            set = new HashSet<>(bean.getTargets("items", Act.class));
            items.put(act, set);
        }
        return set;
    }

    /**
     * Saves the state.
     */
    public void save() {
        service.save(toSave);
        for (Reference reference : toRemove) {
            service.remove(reference);
        }
    }
}
