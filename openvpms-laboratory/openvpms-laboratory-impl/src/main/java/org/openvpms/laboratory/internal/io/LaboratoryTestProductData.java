/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Laboratory test product data.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestProductData {

    /**
     * The old version of the product, without prices updated.
     */
    private final Product oldProduct;

    /**
     * The new version of the product, with prices updated.
     */
    private final Product newProduct;

    /**
     * The tests associated with the product.
     */
    private final List<Entity> tests;

    /**
     * The old cost.
     */
    private final BigDecimal oldCost;

    /**
     * The new cost.
     */
    private final BigDecimal newCost;

    /**
     * Constructs a {@link LaboratoryTestProductData}.
     *
     * @param oldProduct the saved version of the product, without cost changes applied
     * @param oldCost    the old cost of the product
     * @param newProduct the new version of the product, with cost updates applied but not saved
     * @param newCost    the new cost of the product. This is the summed test prices
     * @param tests      the tests
     */
    public LaboratoryTestProductData(Product oldProduct, BigDecimal oldCost, Product newProduct, BigDecimal newCost,
                                     List<Entity> tests) {
        this.oldProduct = oldProduct;
        this.newProduct = newProduct;
        this.oldCost = oldCost;
        this.newCost = newCost;
        this.tests = tests;
    }

    /**
     * Returns the old version of the product.
     * <p/>
     * This is the persistent version, without cost changes applied.
     *
     * @return the old version of the product
     */
    public Product getOldProduct() {
        return oldProduct;
    }

    /**
     * Returns the old product cost.
     *
     * @return the old product cost
     */
    public BigDecimal getOldCost() {
        return oldCost;
    }

    /**
     * Returns the new version of the product.
     * <p/>
     * This is the product with cost changes applied, but not saved.
     *
     * @return the new version of the product
     */
    public Product getNewProduct() {
        return newProduct;
    }

    /**
     * The new product cost.
     *
     * @return the new product cost
     */
    public BigDecimal getNewCost() {
        return newCost;
    }

    /**
     * Returns the tests associated with the product.
     *
     * @return the tests
     */
    public List<Entity> getTests() {
        return tests;
    }

    /**
     * Determines if the product cost has increased.
     *
     * @return {@code true} if the product cost has increased
     */
    public boolean costIncreased() {
        return oldCost.compareTo(newCost) < 0;
    }
}