/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.product.ProductPriceUpdater;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Matches laboratory test data.
 *
 * @author Tim Anderson
 * @see LaboratoryTestDataReader
 */
public class LaboratoryTestDataMatcher {

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules rules;

    /**
     * The product price rules
     */
    private final ProductPriceRules productPriceRules;

    /**
     * The practice rules.
     */
    private final PracticeRules practiceRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The test identity archetype factory.
     */
    private final LaboratoryTestIdentityArchetypeFactory factory;

    /**
     * Constructs a {@link LaboratoryTestDataMatcher}.
     *
     * @param rules             the laboratory rules
     * @param productPriceRules the product price rules
     * @param practiceRules     the practice rules
     * @param service           the archetype service
     */
    public LaboratoryTestDataMatcher(LaboratoryRules rules, ProductPriceRules productPriceRules,
                                     PracticeRules practiceRules, IArchetypeService service) {
        this.rules = rules;
        this.productPriceRules = productPriceRules;
        this.practiceRules = practiceRules;
        this.service = service;
        factory = new LaboratoryTestIdentityArchetypeFactory(service);
    }

    /**
     * Matches laboratory test data.
     *
     * @param input the input data
     * @return the matched data
     */
    public LaboratoryTestDataSet matchTests(List<LaboratoryTestData> input) {
        List<LaboratoryTestData> output = new ArrayList<>();
        List<LaboratoryTestData> errors = new ArrayList<>();
        Entity investigationType = null;
        for (LaboratoryTestData data : input) {
            Entity test = getTest(data);
            BigDecimal oldPrice = null;
            boolean valid = false;
            if (test != null) {
                if (test.getName().equalsIgnoreCase(data.getName())) {
                    IMObjectBean bean = service.getBean(test);
                    Reference ref = bean.getTargetRef("investigationType");
                    if (ref != null) {
                        if (investigationType == null) {
                            investigationType = service.get(ref, Entity.class);
                        }
                        if (investigationType == null) {
                            errors.add(new LaboratoryTestData(data, "Investigation type not found"));
                        } else if (!ref.equals(investigationType.getObjectReference())) {
                            Entity other = service.get(ref, Entity.class);
                            String name = other != null ? other.getName() : "not found";
                            errors.add(new LaboratoryTestData(data, "Cannot load to multiple investigation types: "
                                                                    + name));
                        } else {
                            valid = true;
                        }
                    } else {
                        valid = true;
                    }
                    if (valid) {
                        oldPrice = bean.getBigDecimal("price");
                    }
                } else {
                    errors.add(new LaboratoryTestData(
                            data, "The code '" + data.getCode()
                                  + "' for test '" + data.getName() + "' matches a different test: '"
                                  + test.getName() + "'"));
                }
            } else {
                // new test
                valid = true;
            }
            if (valid) {
                output.add(new LaboratoryTestData(data, test, oldPrice, investigationType));
            }
        }
        return new LaboratoryTestDataSet(output, errors, investigationType);
    }

    /**
     * Matches test data to products, calculating new prices.
     *
     * @param input        the laboratory test data to match
     * @param date         the date to determine the current unit price
     * @return the corresponding product data
     */
    public List<LaboratoryTestProductData> matchProducts(List<LaboratoryTestData> input, Date date) {
        ProductPriceUpdater priceUpdater = new ProductPriceUpdater(productPriceRules, practiceRules, service);
        Map<Product, LaboratoryTestProductData> products = new LinkedHashMap<>();
        for (LaboratoryTestData data : input) {
            Entity test = data.getTest();
            if (test != null) {
                List<Product> matches = rules.getProducts(test);
                for (Product product : matches) {
                    if (products.get(product) == null) {
                        LaboratoryTestProductData productData = getProductData(product, priceUpdater, date);
                        if (productData != null) {
                            products.put(product, productData);
                        }
                    }
                }
            }
        }
        return new ArrayList<>(products.values());
    }

    /**
     * Returns the test corresponding to the data.
     *
     * @param data the test data
     * @return the corresponding test, or {@code null} if there is no match
     */
    private Entity getTest(LaboratoryTestData data) {
        Entity test = null;
        String archetype = factory.getArchetype(data.getLaboratory());
        if (archetype != null) {
            test = rules.getTest(archetype, data.getCode());
        }
        return test;
    }

    /**
     * Determines the new unit prices for a product, based on the updated test prices.
     *
     * @param product      the product
     * @param priceUpdater the product price updater
     * @param date         the data to determine the current unit price
     * @return a corresponding {@link LaboratoryTestProductData}
     */
    private LaboratoryTestProductData getProductData(Product product, ProductPriceUpdater priceUpdater, Date date) {
        LaboratoryTestProductData result = null;
        IMObjectBean bean = service.getBean(product);
        List<Entity> tests = bean.getTargets("tests", Entity.class, Policies.active());
        BigDecimal newCost = getCostPrice(tests);
        ProductPrice unitPrice = productPriceRules.getProductPrice(product, ProductArchetypes.UNIT_PRICE, date, null);
        BigDecimal oldCost = unitPrice != null ? productPriceRules.getCostPrice(unitPrice) : BigDecimal.ZERO;
        List<ProductPrice> prices = priceUpdater.update(product, newCost, false, false);
        if (!prices.isEmpty()) {
            Product oldProduct = service.get(product.getObjectReference(), Product.class);
            if (oldProduct != null) {
                for (ProductPrice price : prices) {
                    product.addProductPrice(price);
                }
                result = new LaboratoryTestProductData(oldProduct, oldCost, product, newCost, tests);
            }
        }
        return result;
    }

    /**
     * Returns the cost price for a collection of tests.
     * <br/>
     * This is the sum of their prices.
     *
     * @param tests the tests
     * @return the cost price
     */
    private BigDecimal getCostPrice(List<Entity> tests) {
        BigDecimal result = BigDecimal.ZERO;
        for (Entity test : tests) {
            IMObjectBean bean = service.getBean(test);
            result = result.add(bean.getBigDecimal("price", BigDecimal.ZERO));
        }
        return result;
    }
}