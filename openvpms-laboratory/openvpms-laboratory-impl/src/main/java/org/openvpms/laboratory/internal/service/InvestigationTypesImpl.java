/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.laboratory.service.InvestigationTypeBuilder;
import org.openvpms.laboratory.service.InvestigationTypes;
import org.openvpms.laboratory.service.LaboratoryService;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link InvestigationTypes}.
 *
 * @author Tim Anderson
 */
public class InvestigationTypesImpl implements InvestigationTypes {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link InvestigationTypesImpl}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public InvestigationTypesImpl(ArchetypeService service, PlatformTransactionManager transactionManager,
                                  DomainService domainService) {
        this.service = service;
        this.transactionManager = transactionManager;
        this.domainService = domainService;
    }

    /**
     * Returns an investigation type.
     * <p>
     * Investigation types managed by an {@link LaboratoryService} must have a unique code.
     *
     * @param archetype the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param typeId    the investigation type identifier identifier
     * @return the investigation type, or {@code null} if none is found. The returned type may be inactive
     */
    @Override
    public InvestigationType getInvestigationType(String archetype, String typeId) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, LaboratoryArchetypes.INVESTIGATION_TYPE);
        Join identity = root.join("typeId", archetype);
        identity.on(builder.equal(identity.get("identity"), typeId));
        query.orderBy(builder.asc(root.get("id")));
        Entity match = service.createQuery(query).getFirstResult();
        return (match != null) ? domainService.create(match, InvestigationType.class) : null;
    }

    /**
     * Returns all investigation types with the specified test code archetype.
     *
     * @param archetype  the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param activeOnly if {@code true}, only return active investigation types
     * @return the investigation types
     */
    @Override
    public List<InvestigationType> getInvestigationTypes(String archetype, boolean activeOnly) {
        List<InvestigationType> result = new ArrayList<>();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, LaboratoryArchetypes.INVESTIGATION_TYPE);
        root.join("typeId", archetype);
        if (activeOnly) {
            query.where(builder.equal(root.get("active"), true));
        }
        query.orderBy(builder.asc(root.get("id")));
        for (Entity entity : service.createQuery(query).getResultList()) {
            result.add(domainService.create(entity, InvestigationType.class));
        }
        return result;
    }

    /**
     * Returns a builder to build investigation types.
     *
     * @return the investigation type builder
     */
    @Override
    public InvestigationTypeBuilder getInvestigationTypeBuilder() {
        return new InvestigationTypeBuilderImpl(this, service, transactionManager, domainService);
    }
}
