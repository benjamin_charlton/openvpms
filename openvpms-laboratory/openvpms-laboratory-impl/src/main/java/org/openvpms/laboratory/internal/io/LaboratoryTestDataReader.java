/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.document.Document;

/**
 * Reads laboratory test data.
 *
 * @author Tim Anderson
 * @see LaboratoryTestCSVReader
 * @see LaboratoryTestDataMatcher
 */
public class LaboratoryTestDataReader {

    /**
     * The document handlers.
     */
    private final DocumentHandlers documentHandlers;

    /**
     * The test data matcher.
     */
    private final LaboratoryTestDataMatcher matcher;

    /**
     * Constructs a {@link LaboratoryTestDataReader}.
     *
     * @param matcher          the data matcher
     * @param documentHandlers the document handlers
     */
    public LaboratoryTestDataReader(LaboratoryTestDataMatcher matcher, DocumentHandlers documentHandlers) {
        this.documentHandlers = documentHandlers;
        this.matcher = matcher;
    }

    /**
     * Reads laboratory test data from a CSV document.
     *
     * @param document the document
     * @return the corresponding data, matched to existing tests and products where applicable
     */
    public LaboratoryTestDataSet read(Document document) {
        LaboratoryTestCSVReader reader = new LaboratoryTestCSVReader(documentHandlers, ',');
        LaboratoryTestDataSet data = reader.read(document);
        if (data.getErrors().isEmpty()) {
            data = matcher.matchTests(data.getData());
        }
        return data;
    }
}