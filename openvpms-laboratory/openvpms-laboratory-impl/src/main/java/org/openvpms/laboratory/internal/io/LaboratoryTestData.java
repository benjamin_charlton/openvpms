/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.openvpms.component.model.entity.Entity;

import java.math.BigDecimal;

/**
 * Laboratory test data, read from CSV.
 *
 * @author Tim Anderson
 * @see LaboratoryTestCSVReader
 */
public class LaboratoryTestData {

    /**
     * The test provider.
     */
    private final String laboratory;

    /**
     * The test code.
     */
    private final String code;

    /**
     * The test name.
     */
    private final String name;

    /**
     * The test description.
     */
    private final String description;

    /**
     * The test specimen collection details.
     */
    private final String specimen;

    /**
     * The turnaround time details for the test.
     */
    private final String turnaround;

    /**
     * The old price.
     */
    private final BigDecimal oldPrice;

    /**
     * The new price.
     */
    private final BigDecimal newPrice;

    /**
     * The line the test was read from.
     */
    private final int line;

    /**
     * The associated test.
     */
    private final Entity test;

    /**
     * The associated investigation type.
     */
    private final Entity investigationType;

    /**
     * An error message, if the line is invalid.
     */
    private final String error;

    /**
     * Constructs a {@link LaboratoryTestData}.
     *
     * @param laboratory  the test provider name
     * @param code        the laboratory's unique identifier for the test
     * @param name        the test name
     * @param description the test description. May be {@code null}
     * @param specimen    the specimen collection details. May be {@code null}
     * @param turnaround  the test turnaround time details. May be {@code null}
     * @param oldPrice    the old price, or {@code null} if this is a new test
     * @param newPrice    the new price
     * @param line        the line that the test was read from
     * @param error       any error message generated while processing the line
     */
    public LaboratoryTestData(String laboratory, String code, String name, String description, String specimen,
                              String turnaround, BigDecimal oldPrice, BigDecimal newPrice, int line, String error) {
        this.laboratory = laboratory;
        this.code = code;
        this.name = name;
        this.description = description;
        this.specimen = specimen;
        this.turnaround = turnaround;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
        this.line = line;
        this.test = null;
        this.investigationType = null;
        this.error = error;
    }

    /**
     * Constructs a {@link LaboratoryTestData}.
     *
     * @param source            the source data to copy
     * @param test              the test
     * @param investigationType the investigation type
     */
    public LaboratoryTestData(LaboratoryTestData source, Entity test, Entity investigationType) {
        this(source, test, source.getOldPrice(), investigationType);
    }

    /**
     * Constructs a {@link LaboratoryTestData}.
     *
     * @param source            the source data to copy
     * @param test              the test
     * @param oldPrice          the old price, or {@code null} if this is a new test
     * @param investigationType the investigation type
     */
    public LaboratoryTestData(LaboratoryTestData source, Entity test, BigDecimal oldPrice, Entity investigationType) {
        this(source, oldPrice, test, investigationType, source.getError());
    }

    /**
     * Constructs a {@link LaboratoryTestData}.
     *
     * @param source the source data to copy
     * @param error  any error message generated while processing the line
     */
    public LaboratoryTestData(LaboratoryTestData source, String error) {
        this(source, source.getOldPrice(), source.getTest(), source.getInvestigationType(), error);
    }

    /**
     * Constructs a {@link LaboratoryTestData}.
     *
     * @param source            the source data to copy
     * @param oldPrice          the old price, or {@code null} if this is a new test
     * @param test              the test. May be {@code null}
     * @param investigationType the investigation type. May be {@code null}
     * @param error             any error message generated while processing the line. May be {@code null}
     */
    private LaboratoryTestData(LaboratoryTestData source, BigDecimal oldPrice, Entity test, Entity investigationType,
                               String error) {
        this.laboratory = source.getLaboratory();
        this.code = source.getCode();
        this.name = source.getName();
        this.description = source.getDescription();
        this.specimen = source.getSpecimen();
        this.turnaround = source.getTurnaround();
        this.oldPrice = oldPrice;
        this.newPrice = source.getNewPrice();
        this.line = source.getLine();
        this.test = test;
        this.investigationType = investigationType;
        this.error = error;
    }

    /**
     * Returns the test provider name.
     *
     * @return the test provider name
     */
    public String getLaboratory() {
        return laboratory;
    }

    /**
     * Returns the provider test code.
     * <p/>
     * This should be unique to the provider.
     *
     * @return the provider test code
     */
    public String getCode() {
        return code;
    }

    /**
     * Returns the test name.
     *
     * @return the test name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the test description.
     *
     * @return the test description. May be {@code null}
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the test specimen collection details.
     *
     * @return the specimen collection details. May be {@code null}
     */
    public String getSpecimen() {
        return specimen;
    }

    /**
     * Returns the test turnaround time details.
     *
     * @return the turnaround time details. May be {@code null}
     */
    public String getTurnaround() {
        return turnaround;
    }

    /**
     * Returns the old price for the test, tax exclusive.
     *
     * @return the old price, or {@code null} if the test is new or didn't have a price
     */
    public BigDecimal getOldPrice() {
        return oldPrice;
    }

    /**
     * Returns the new price for the test, tax exclusive.
     *
     * @return the new price
     */
    public BigDecimal getNewPrice() {
        return newPrice;
    }

    /**
     * Returns the line where the data was read from.
     *
     * @return the line
     */
    public int getLine() {
        return line;
    }

    /**
     * Returns any error message generated while processing the line.
     *
     * @return the error message, or {@code null} if there was no error
     */
    public String getError() {
        return error;
    }

    /**
     * Returns the test associated with this data.
     *
     * @return the test, or {@code null} if no existing test corresponds to the {@link #getCode()} test identifier}
     */
    public Entity getTest() {
        return test;
    }

    /**
     * Returns the investigation type associated with this test.
     *
     * @return the investigation type. May be {@code null}
     */
    public Entity getInvestigationType() {
        return investigationType;
    }

}