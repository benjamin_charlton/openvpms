/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;

import java.util.List;

import static org.openvpms.laboratory.internal.report.ResultsImpl.LONG_NOTES;
import static org.openvpms.laboratory.internal.report.ResultsImpl.NOTES;

/**
 * Base class for report builders.
 *
 * @author Tim Anderson
 */
abstract class Builder<R> {

    /**
     * Parent act node name.
     */
    protected static final String PARENT = "parent";

    /**
     * The parent builder. May be {@code null}
     */
    private final Builder<?> parent;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;


    /**
     * Constructs a {@link Builder}.
     *
     * @param parent the parent builder
     */
    Builder(Builder<?> parent) {
        this.parent = parent;
        this.service = parent.service;
        this.domainService = parent.domainService;
    }

    /**
     * Constructs a {@link Builder}.
     *
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    Builder(ArchetypeService service, DomainService domainService) {
        this.parent = null;
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Builds the object
     *
     * @return the object
     */
    public abstract R build();

    /**
     * Creates a {@link LongText} for the notes node.
     *
     * @param bean the result bean
     * @return a {@link LongText} to manage the notes node
     */
    protected LongText createLongNotes(IMObjectBean bean) {
        return new LongText(bean, NOTES, LONG_NOTES, InvestigationArchetypes.RESULT_NOTE, PARENT, "Long Notes",
                            getService());
    }

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @param type      the expected type of the object
     * @return a new object
     * @throws OpenVPMSException  for any error
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    <O extends IMObject> O create(String archetype, Class<O> type) {
        return service.create(archetype, type);
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     * @throws OpenVPMSException for any error
     */
    IMObjectBean getBean(IMObject object) {
        return service.getBean(object);
    }

    /**
     * Returns the parent builder.
     *
     * @return the parent builder, or {@code null} if there is no parent.
     */
    Builder<?> getParent() {
        return parent;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the domain object service.
     *
     * @return the the domain object service
     */
    DomainService getDomainService() {
        return domainService;
    }

    /**
     * Returns the next sequence number in a set of sequenced relationships.
     *
     * @param relationships the relationships
     * @return the next sequence number
     */
    int getNextSequence(List<ActRelationship> relationships) {
        int sequence = 0;
        for (ActRelationship relationship : relationships) {
            if (relationship.getSequence() >= sequence) {
                sequence = relationship.getSequence() + 1;
            }
        }
        return sequence;
    }
}
