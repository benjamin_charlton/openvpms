/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;


import org.openvpms.component.model.entity.Entity;

import java.util.List;

/**
 * Laboratory test data.
 *
 * @author Tim Anderson
 * @see LaboratoryTestCSVReader
 * @see LaboratoryTestDataMatcher
 * @see LaboratoryTestDataReader
 */
public class LaboratoryTestDataSet {

    /**
     * The product data.
     */
    private final List<LaboratoryTestData> data;

    /**
     * The erroneous data.
     */
    private final List<LaboratoryTestData> errors;

    /**
     * The investigation type.
     */
    private final Entity investigationType;

    /**
     * Constructs a {@link LaboratoryTestDataSet}.
     *
     * @param data              the filtered data
     * @param errors            the erroneous data
     * @param investigationType the common investigation type. May be {@code null}
     */
    public LaboratoryTestDataSet(List<LaboratoryTestData> data, List<LaboratoryTestData> errors,
                                 Entity investigationType) {
        this.data = data;
        this.errors = errors;
        this.investigationType = investigationType;
    }

    /**
     * Returns the test data.
     *
     * @return the test data
     */
    public List<LaboratoryTestData> getData() {
        return data;
    }

    /**
     * Returns the test data that contains errors.
     *
     * @return the erroneous data
     */
    public List<LaboratoryTestData> getErrors() {
        return errors;
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type. May be {@code null}
     */
    public Entity getInvestigationType() {
        return investigationType;
    }
}