/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.order.OrderConfirmation;
import org.openvpms.laboratory.service.LaboratoryService;

/**
 * Confirmation for a {@link Order}.
 *
 * @author Tim Anderson
 */
public class Confirmation {

    /**
     * The order to confirm.
     */
    private final Order order;

    /**
     * The confirmation launcher.
     */
    private final OrderConfirmation confirmation;

    /**
     * The laboratory service.
     */
    private final LaboratoryService service;

    /**
     * Constructs a {@link Confirmation}.
     *
     * @param order the order to confirm
     * @param confirmation the confirmation
     * @param service the laboratory service
     */
    public Confirmation(Order order, OrderConfirmation confirmation, LaboratoryService service) {
        this.order = order;
        this.confirmation = confirmation;
        this.service = service;
    }

    /**
     * Returns the order to confirm.
     *
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * Returns the confirmation.
     *
     * @return the confirmation
     */
    public OrderConfirmation getConfirmation() {
        return confirmation;
    }

    /**
     * Returns the laboratory service used to place the order.
     *
     * @return the laboratory service
     */
    public LaboratoryService getService() {
        return  service;
    }

}
