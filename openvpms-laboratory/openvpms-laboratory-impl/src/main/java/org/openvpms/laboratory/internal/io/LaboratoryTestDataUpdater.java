/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.service.object.DomainObjectService;
import org.openvpms.laboratory.service.Tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Updates laboratory test data from and associated product prices from {@link LaboratoryTestDataSet}.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestDataUpdater {

    /**
     * The laboratory tests service.
     */
    private final Tests tests;

    /**
     * The domain object service.
     */
    private final DomainObjectService domainService;

    /**
     * The factory for creating laboratory test code archetypes.
     */
    private final LaboratoryTestIdentityArchetypeFactory factory;

    /**
     * Constructs a {@link LaboratoryTestDataUpdater}.
     *
     * @param tests         the laboratory test service
     * @param domainService the domain object service
     * @param service       the archetype service
     */
    public LaboratoryTestDataUpdater(Tests tests, DomainObjectService domainService, ArchetypeService service) {
        this.tests = tests;
        this.domainService = domainService;
        factory = new LaboratoryTestIdentityArchetypeFactory(service);
    }

    /**
     * Updates tests.
     *
     * @param dataSet the data set
     * @return the updated data
     */
    public LaboratoryTestDataSet update(LaboratoryTestDataSet dataSet) {
        List<LaboratoryTestData> result = new ArrayList<>();
        Entity investigationType = dataSet.getInvestigationType();
        for (LaboratoryTestData data : dataSet.getData()) {
            LaboratoryTestData updated = updateTest(data, investigationType);
            result.add(updated);
        }
        return new LaboratoryTestDataSet(result, Collections.emptyList(), investigationType);
    }

    /**
     * Updates a test.
     *
     * @param data                     the data to update from
     * @param defaultInvestigationType the investigation type to use if one isn't present
     * @return the updated data
     */
    private LaboratoryTestData updateTest(LaboratoryTestData data, Entity defaultInvestigationType) {
        Entity type = data.getInvestigationType();
        if (type == null) {
            type = defaultInvestigationType;
        }
        String archetype = factory.getArchetype(data.getLaboratory());
        if (archetype == null) {
            // archetype doesn't exist, so create it
            archetype = factory.create(data.getLaboratory());
        }
        InvestigationType investigationType = domainService.create(type, InvestigationType.class);
        Test test = tests.getTestBuilder()
                .testCode(archetype, data.getCode())
                .name(data.getName())
                .description(data.getDescription())
                .specimen(data.getSpecimen())
                .turnaround(data.getTurnaround())
                .investigationType(investigationType)
                .price(data.getNewPrice())
                .build();
        return new LaboratoryTestData(data, test, investigationType);
    }
}