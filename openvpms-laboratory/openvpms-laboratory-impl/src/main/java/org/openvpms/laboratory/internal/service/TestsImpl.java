/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.TestBuilder;
import org.openvpms.laboratory.service.Tests;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of the {@link Tests} interface.
 *
 * @author Tim Anderson
 */
public class TestsImpl implements Tests {

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules rules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link TestsImpl}.
     *
     * @param rules              the laboratory rules
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public TestsImpl(LaboratoryRules rules, ArchetypeService service, PlatformTransactionManager transactionManager,
                     DomainService domainService) {
        this.rules = rules;
        this.service = service;
        this.transactionManager = transactionManager;
        this.domainService = domainService;
    }

    /**
     * Returns a laboratory test.
     * <p>
     * Tests managed by an {@link LaboratoryService} must have a unique code.
     *
     * @param archetype the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param code      the test code
     * @return the test, or {@code null} if none is found. The returned test may be inactive
     */
    @Override
    public Test getTest(String archetype, String code) {
        checkTestCode(archetype);
        Entity match = rules.getTest(archetype, code);
        return (match != null) ? domainService.create(match, Test.class) : null;
    }

    /**
     * Returns all tests with the specified archetype.
     *
     * @param archetype  the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param activeOnly if {@code true}, only return active tests
     * @return the tests
     */
    @Override
    public List<Test> getTests(String archetype, boolean activeOnly) {
        checkTestCode(archetype);
        List<Test> result = new ArrayList<>();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, LaboratoryArchetypes.TEST);
        root.join("code", archetype);
        if (activeOnly) {
            query.where(builder.equal(root.get("active"), true));
        }
        query.orderBy(builder.asc(root.get("id")));
        for (Entity entity : service.createQuery(query).getResultList()) {
            result.add(domainService.create(entity, Test.class));
        }
        return result;
    }

    /**
     * Returns a test builder to build tests.
     *
     * @return the test builder
     */
    @Override
    public TestBuilder getTestBuilder() {
        return new TestBuilderImpl(this, service, transactionManager, domainService);
    }

    /**
     * Verifies that an archetype is an <em>entityIdentity.laboratoryTest*</em>.
     *
     * @param archetype the archetype
     */
    private void checkTestCode(String archetype) {
        if (!TypeHelper.matches(archetype, LaboratoryArchetypes.TEST_CODES)) {
            throw new IllegalStateException("Invalid laboratory test code archetype: " + archetype);
        }
    }
}

