/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.csv.AbstractCSVReader;
import org.openvpms.archetype.csv.CSVReaderException;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.component.model.document.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.openvpms.archetype.csv.CSVReaderException.ErrorCode.DuplicateValue;

/**
 * Reads a CSV containing laboratory test data.
 * <p/>
 * This contains the following columns:
 * <ul>
 *     <li>Laboratory - the laboratory test provider. Mandatory</li>
 *     <li>Code - uniquely identifies the test for the provider. Mandatory</li>
 *     <li>Name - the test name. Mandatory</li>
 *     <li>Description - the test description. Optional</li>
 *     <li>Specimen - specimen collection details. Optional</li>
 *     <li>Turnaround Time - turnaround time details. Optional</li>
 *     <li>Price - the test price. Mandatory</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class LaboratoryTestCSVReader extends AbstractCSVReader {

    /**
     * Laboratory column index.
     */
    private static final int LABORATORY = 0;

    /**
     * Code column index.
     */
    private static final int CODE = 1;

    /**
     * Test name column index.
     */
    private static final int NAME = CODE + 1;

    /**
     * Test description column index.
     */
    private static final int DESCRIPTION = NAME + 1;

    /**
     * Specimen collection details index.
     */
    private static final int SPECIMEN = DESCRIPTION + 1;

    /**
     * Turnaround time column index.
     */
    private static final int TURNAROUND_TIME = SPECIMEN + 1;

    /**
     * Price column index.
     */
    private static final int PRICE = TURNAROUND_TIME + 1;

    /**
     * The CSV header line.
     */
    private static final String[] HEADER = {"Laboratory", "Code", "Name", "Description", "Specimen", "Turnaround Time",
                                            "Price"};


    /**
     * Constructs an {@link LaboratoryTestCSVReader}.
     *
     * @param handlers  the document handlers
     * @param separator the field separator
     */
    public LaboratoryTestCSVReader(DocumentHandlers handlers, char separator) {
        super(handlers, HEADER, separator);
    }

    /**
     * Reads the laboratory test data.
     *
     * @param document the CSV document
     * @return the data set
     */
    public LaboratoryTestDataSet read(Document document) {
        List<String[]> lines = readLines((org.openvpms.component.business.domain.im.document.Document) document);
        List<LaboratoryTestData> data = new ArrayList<>();
        List<LaboratoryTestData> errors = new ArrayList<>();

        int lineNo = 2; // line 1 is the header

        String expectedLaboratory = null; // expect the laboratory to be the same for every line
        for (String[] line : lines) {
            expectedLaboratory = parse(line, data, errors, expectedLaboratory, lineNo);
            ++lineNo;
        }
        return new LaboratoryTestDataSet(data, errors, null);
    }

    /**
     * Parses a line.
     *
     * @param line               the line to parse
     * @param valid              the valid test data
     * @param errors             the test data with errors
     * @param expectedLaboratory the expected laboratory, or {@code null} if none has been read yet
     * @param lineNo             the line number
     * @return the laboratory
     */
    private String parse(String[] line, List<LaboratoryTestData> valid, List<LaboratoryTestData> errors,
                         String expectedLaboratory, int lineNo) {
        String result = expectedLaboratory;
        String laboratory = null;
        String code = null;
        String name = null;
        String description = null;
        String specimen = null;
        String turnaround = null;
        BigDecimal price = null;

        try {
            checkFields(line, lineNo);
            laboratory = getLaboratory(line, lineNo, expectedLaboratory);
            code = getString(line, CODE, lineNo, true);
            name = getString(line, NAME, lineNo, true);
            description = getString(line, DESCRIPTION, lineNo, false);
            specimen = getString(line, SPECIMEN, lineNo, false);
            turnaround = getString(line, TURNAROUND_TIME, lineNo, false);
            price = getPrice(line, lineNo);
            result = laboratory;
            LaboratoryTestData data = new LaboratoryTestData(laboratory, code, name, description, specimen,
                                                             turnaround, null, price, lineNo, null);
            checkMaxLength(code, CODE, lineNo, 100);
            checkMaxLength(name, NAME, lineNo, 100);
            checkDuplicateCode(data, valid);
            valid.add(data);
        } catch (Exception exception) {
            LaboratoryTestData invalid = new LaboratoryTestData(laboratory, code, name, description, specimen,
                                                                turnaround, null, price, lineNo,
                                                                exception.getMessage());
            errors.add(invalid);
        }
        return result;
    }

    /**
     * Verifies that a code is not duplicated.
     *
     * @param data     the data to check
     * @param existing the existing data
     */
    private void checkDuplicateCode(LaboratoryTestData data, List<LaboratoryTestData> existing) {
        String code = data.getCode();
        for (LaboratoryTestData other : existing) {
            if (code.equals(other.getCode())) {
                throw new CSVReaderException(DuplicateValue, data.getLine(), HEADER[CODE], other.getLine(),
                                             data.getLine());
            }
        }
    }

    /**
     * Returns the laboratory name, verifying it is alphanumeric.
     *
     * @param line               the line
     * @param lineNo             the line no.
     * @param expectedLaboratory the expected laboratory, or {@code null} if none has been read yet
     * @return the laboratory
     */
    private String getLaboratory(String[] line, int lineNo, String expectedLaboratory) {
        String result = getString(line, LABORATORY, lineNo, true);
        if (!StringUtils.isAlphanumeric(result)) {
            reportInvalid(HEADER[LABORATORY], result, lineNo);
        }
        if (expectedLaboratory != null && !result.equals(expectedLaboratory)) {
            reportInvalid(HEADER[LABORATORY], result, lineNo);
        }
        return result;
    }

    /**
     * Returns the price.
     *
     * @param line   the line
     * @param lineNo the line no.
     * @return the price
     */
    private BigDecimal getPrice(String[] line, int lineNo) {
        BigDecimal price = getDecimal(line, PRICE, lineNo, true);
        if (!MathRules.isPositive(price)) {
            reportInvalid(HEADER[PRICE], price.toPlainString(), lineNo);
        }
        return price;
    }
}