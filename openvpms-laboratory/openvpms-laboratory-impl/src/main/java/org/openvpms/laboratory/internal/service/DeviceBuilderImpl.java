/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.practice.Location;
import org.openvpms.laboratory.service.DeviceBuilder;
import org.openvpms.laboratory.service.Devices;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Default implementation of {@link DeviceBuilder}.
 *
 * @author Tim Anderson
 */
public class DeviceBuilderImpl extends EntityBuilder<Device, DeviceBuilderImpl> implements DeviceBuilder {

    /**
     * The devices.
     */
    private final Devices devices;

    /**
     * The laboratory that manages the device.
     */
    private Laboratory laboratory;

    /**
     * The locations to replace.
     */
    private Location[] locations;

    /**
     * The locations to add.
     */
    private Set<Location> addLocations = new LinkedHashSet<>();

    /**
     * Constructs a {@link DeviceBuilderImpl}.
     *
     * @param devices            the devices
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    DeviceBuilderImpl(Devices devices, ArchetypeService service, PlatformTransactionManager transactionManager,
                      DomainService domainService) {
        super(LaboratoryArchetypes.DEVICE, LaboratoryArchetypes.DEVICE_IDS, Device.class, service, transactionManager,
              domainService);
        this.devices = devices;
        reset();
    }

    /**
     * Sets the device id, used for identifying the device.
     *
     * @param archetype the device identity archetype. Must be an <em>entityIdentity.laboratoryDevice*</em>
     * @param id        the device id. This must be unique to the archetype
     * @return this
     */
    @Override
    public DeviceBuilder deviceId(String archetype, String id) {
        return entityId(archetype, id);
    }

    /**
     * Sets the laboratory that manages the device.
     *
     * @param laboratory the laboratory
     * @return this
     */
    @Override
    public DeviceBuilder laboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Sets the locations where the device may be used.
     * <p/>
     * This replaces any other locations.
     *
     * @param locations the practice locations
     * @return this
     */
    @Override
    public DeviceBuilder locations(Location... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Adds a practice location where the device may be used.
     *
     * @param location the practice location
     * @return this
     */
    @Override
    public DeviceBuilder addLocation(Location location) {
        addLocations.add(location);
        return this;
    }

    /**
     * Returns the entity corresponding to the identifier.
     *
     * @param archetype the entity id archetype
     * @param id        the entity id
     * @return the entity, or {@code null} if none exists
     */
    @Override
    protected Entity getEntity(String archetype, String id) {
        return devices.getDevice(archetype, id);
    }

    /**
     * Populates a device.
     *
     * @param entity the device to populate
     * @return {@code true} if the device was updated
     */
    @Override
    protected boolean populate(Entity entity) {
        boolean changed = super.populate(entity);
        IMObjectBean bean = getService().getBean(entity);
        if (laboratory != null && !Objects.equals(bean.getTargetRef("laboratory"), laboratory.getObjectReference())) {
            bean.setTarget("laboratory", laboratory);
            changed = true;
        }
        if (locations != null) {
            List<Reference> existing = bean.getTargetRefs("locations");
            for (Location location : locations) {
                if (!existing.remove(location.getObjectReference())) {
                    bean.addTarget("locations", location);
                    changed = true;
                }
            }
            if (!existing.isEmpty()) {
                for (Reference ref : existing) {
                    bean.removeTarget("locations", ref);
                    changed = true;
                }
            }
        }
        if (!addLocations.isEmpty()) {
            List<Reference> existing = bean.getTargetRefs("locations");
            for (Location location : addLocations) {
                if (!existing.contains(location.getObjectReference())) {
                    bean.addTarget("locations", location);
                    changed = true;
                }
            }
        }
        return changed;
    }

    /**
     * Resets the builder.
     */
    @Override
    protected void reset() {
        super.reset();
        laboratory = null;
        locations = null;
        addLocations.clear();
    }

}
