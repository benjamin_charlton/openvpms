/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.junit.Test;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.service.object.DomainObjectService;
import org.openvpms.laboratory.service.Tests;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link LaboratoryTestDataUpdater}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class LaboratoryTestDataUpdaterTestCase extends AbstractLaboratoryIOTestCase {

    /**
     * The domain service.
     */
    @Autowired
    private DomainObjectService domainService;

    /**
     * The tests.
     */
    @Autowired
    private Tests tests;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * Tests {@link LaboratoryTestDataUpdater#update(LaboratoryTestDataSet)} method.
     */
    @Test
    public void testUpdate() {
        LaboratoryTestDataUpdater updater = new LaboratoryTestDataUpdater(tests, domainService, getArchetypeService());
        String laboratory = ValueStrategy.randomString("ZLab");
        String code1 = ValueStrategy.randomString("AAA");
        String code2 = ValueStrategy.randomString("BBB");
        String code3 = ValueStrategy.randomString("CCC");
        Entity investigationType = laboratoryFactory.createInvestigationType();

        // set up 3 new tests
        LaboratoryTestData data1A = new LaboratoryTestData(laboratory, code1, "AAA Test", "AAA Test Description",
                                                           "AAA Test Specimen details",
                                                           "AAA Test Turnaround", null, BigDecimal.ONE, 1, null);
        LaboratoryTestData data2 = new LaboratoryTestData(laboratory, code2, "BBB Test", "BBB Test Description",
                                                          "BBB Test Specimen details",
                                                          "BBB Test Turnaround", new BigDecimal(4), new BigDecimal(2),
                                                          2, null);
        LaboratoryTestData data3 = new LaboratoryTestData(laboratory, code3, "CCC Test", "CCC Test Description",
                                                          "CCC Test Specimen details",
                                                          "CCC Test Turnaround", BigDecimal.ONE, BigDecimal.ONE,
                                                          3, null);

        LaboratoryTestDataSet set1
                = new LaboratoryTestDataSet(Arrays.asList(data1A, data2, data3), Collections.emptyList(),
                                            investigationType);

        // import them and verify the created test matches the data
        LaboratoryTestDataSet saved1 = updater.update(set1);
        assertEquals(3, saved1.getData().size());
        checkTest(saved1.getData().get(0));
        checkTest(saved1.getData().get(1));
        checkTest(saved1.getData().get(2));

        // now simulate importing changes to the test imported by data1A
        LaboratoryTestData data1B = new LaboratoryTestData(
                new LaboratoryTestData(laboratory, code1, "AAA Test", "Updated description",
                                       "Updated Specimen details",
                                       "Updated Turnaround", null, BigDecimal.TEN, 1, null),
                saved1.getData().get(0).getTest(), investigationType);

        LaboratoryTestDataSet set2 = new LaboratoryTestDataSet(Collections.singletonList(data1B),
                                                               Collections.emptyList(), investigationType);
        LaboratoryTestDataSet saved2 = updater.update(set2);
        assertEquals(1, saved2.getData().size());
        checkTest(saved2.getData().get(0));
    }

    /**
     * Verifies the test associated with a {@link LaboratoryTestData} matches the data.
     *
     * @param data the data
     */
    private void checkTest(LaboratoryTestData data) {
        assertNotNull(data.getTest());
        assertNotNull(data.getInvestigationType());
        org.openvpms.domain.laboratory.Test test
                = domainService.create(data.getTest(), org.openvpms.domain.laboratory.Test.class);
        assertEquals(data.getCode(), test.getCode());
        String archetype = LaboratoryArchetypes.TEST_CODE + data.getLaboratory();
        assertNotNull(getArchetypeService().getArchetypeDescriptor(archetype));
        assertEquals(archetype, test.getCodeIdentity().getArchetype());
        assertEquals(data.getName(), test.getName());
        assertEquals(data.getDescription(), test.getDescription());
        assertEquals(data.getNewPrice(), test.getPrice());
        assertEquals(data.getInvestigationType(), test.getInvestigationType());
        assertEquals(data.getSpecimen(), test.getSpecimen());
        assertEquals(data.getTurnaround(), test.getTurnaround());
    }
}