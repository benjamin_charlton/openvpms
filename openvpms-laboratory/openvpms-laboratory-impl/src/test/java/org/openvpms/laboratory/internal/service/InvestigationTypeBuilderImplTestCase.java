/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.sync.DefaultChanges;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.laboratory.service.InvestigationTypeBuilder;
import org.openvpms.laboratory.service.InvestigationTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link InvestigationTypeBuilderImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class InvestigationTypeBuilderImplTestCase extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Tests the builder.
     */
    @Test
    public void testBuilder() {
        InvestigationTypes types = new InvestigationTypesImpl(getArchetypeService(), transactionManager, domainService);
        InvestigationTypeBuilder builder = new InvestigationTypeBuilderImpl(types, getArchetypeService(),
                                                                            transactionManager, domainService);
        Entity laboratory = laboratoryFactory.createLaboratory();
        Device device1 = domainService.create(laboratoryFactory.createDevice(laboratory), Device.class);
        Device device2 = domainService.create(laboratoryFactory.createDevice(laboratory), Device.class);

        String id = ValueStrategy.randomString();
        String idName = "Id " + id;
        DefaultChanges<Entity> changes = new DefaultChanges<>();
        String name = "Z Investigation Type";
        InvestigationType investigationType = builder.typeId("entityIdentity.investigationTypeTest", id, idName)
                .name(name)
                .laboratory(domainService.create(laboratory, Laboratory.class))
                .devices(Arrays.asList(device1, device2))
                .changes(changes)
                .build();

        assertEquals(id, investigationType.getTypeId());
        assertEquals(id, investigationType.getTypeIdentity().getIdentity());
        assertEquals(idName, investigationType.getTypeIdentity().getName());
        assertEquals(name, investigationType.getName());
        assertEquals(laboratory, investigationType.getLaboratory());
        assertEquals(2, investigationType.getDevices().size());
        assertTrue(investigationType.getDevices().contains(device1));
        assertTrue(investigationType.getDevices().contains(device2));

        assertEquals(1, changes.getAdded().size());
        assertEquals(investigationType, changes.getAdded().get(0));
    }

}
