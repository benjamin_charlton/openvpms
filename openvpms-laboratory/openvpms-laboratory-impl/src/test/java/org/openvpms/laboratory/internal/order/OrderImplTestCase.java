/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.dispatcher.OrderService;
import org.openvpms.laboratory.order.Order;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link OrderImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OrderImplTestCase extends ArchetypeServiceTest {

    /**
     * The practice rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The order service.
     */
    private OrderService orderService;

    /**
     * The plugin archetype service.
     */
    private PluginArchetypeService service;

    /**
     * The laboratory.
     */
    private Entity laboratory;

    /**
     * The investigation.
     */
    private Act investigation;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The investigation type.
     */
    private Entity investigationType;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * Sets up the tests.
     */
    @Before
    public void setUp() {
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getServiceUser()).thenReturn(
                new org.openvpms.component.business.domain.im.security.User());

        patient = patientFactory.createPatient();
        clinician = userFactory.createClinician();
        location = practiceFactory.createLocation();
        investigationType = laboratoryFactory.createInvestigationType();
        orderService = new OrderService(getArchetypeService(), new LaboratoryRules(getArchetypeService()));
        laboratory = LaboratoryTestHelper.createLaboratory(location);
        service = new PluginArchetypeService((IArchetypeRuleService) getArchetypeService(),
                                             getLookupService(), practiceService);
        investigation = patientFactory.newInvestigation().patient(patient).clinician(clinician)
                .location(location).investigationType(investigationType).laboratory(laboratory)
                .startTime(new Date()).build();
    }

    /**
     * Verifies that the {@link Order#getUser()} method is mapped to the {@link Act#getCreatedBy()} method.
     */
    @Test
    public void testUser() {
        User user = TestHelper.createUser();
        new AuthenticationContextImpl().setUser(user);
        Act act = orderService.order(investigation);
        Order order = getOrder(act);
        assertEquals(user, order.getUser());
    }

    /**
     * Tests status changes.
     */
    @Test
    public void testStatusChange() {
        Act act = orderService.order(investigation);
        Order order = getOrder(act);
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.QUEUED, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);

        order.setStatus(Order.Status.SUBMITTING, "Sending");
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.SUBMITTING, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);
        // IN_PROGRESS status doesn't propagate to investigation, so neither does message

        order.setStatus(Order.Status.SUBMITTED, "Submitted OK");
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.SUBMITTED, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);
        // investigation already SENT, so message not propagated

//        order.setStatus(Order.Status.WAITING_FOR_SAMPLE, "Provide sample");
//        checkStatus(order, investigation, Order.Type.NEW, Order.Status.WAITING_FOR_SAMPLE,
//                    InvestigationActStatus.WAITING_FOR_SAMPLE, "Provide sample");
//
//        order.setStatus(Order.Status.PARTIAL_RESULTS);
//        checkStatus(order, investigation, Order.Type.NEW, Order.Status.PARTIAL_RESULTS,
//                    InvestigationActStatus.PARTIAL_RESULTS, null);

        order.setStatus(Order.Status.COMPLETED);
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.COMPLETED,
                    ActStatus.IN_PROGRESS, InvestigationActStatus.RECEIVED, null);
    }

    /**
     * Verfies that orders with ERROR status can only be cancelled.
     */
    @Test
    public void testOrderWithErrorStatus() {
        Act act = orderService.order(investigation);
        Order order = getOrder(act);

        order.setStatus(Order.Status.ERROR, "error");
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.ERROR, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.ERROR, "error");

        try {
            order.setStatus(Order.Status.COMPLETED);
            fail("Expected exception");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0011: Orders with ERROR status must be cancelled", expected.getMessage());
        }

        orderService.cancel(act);
        checkStatus(order, investigation, Order.Type.CANCEL, Order.Status.QUEUED, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.ERROR, "error");
    }

    /**
     * Tests order cancellation.
     */
    @Test
    public void testCancellation() {
        Act act = orderService.order(investigation);
        Order newOrder = getOrder(act);
        newOrder.setStatus(Order.Status.SUBMITTED);

        checkStatus(newOrder, investigation, Order.Type.NEW, Order.Status.SUBMITTED, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);

        assertTrue(orderService.cancel(act));
        Order cancellation = getOrder(act);
        assertEquals(Order.Type.CANCEL, cancellation.getType());

        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.QUEUED, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);

        cancellation.setStatus(Order.Status.SUBMITTING);
        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.SUBMITTING,
                    ActStatus.IN_PROGRESS, InvestigationActStatus.SENT, null);

        try {
            cancellation.setStatus(Order.Status.SUBMITTED);
            fail("Expected setting of status to SUBMITTED for a cancellation to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0010: Cannot assign status SUBMITTED to order cancellation", expected.getMessage());
        }

        cancellation.setStatus(Order.Status.ERROR, "some error");
        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.ERROR,
                    ActStatus.IN_PROGRESS, InvestigationActStatus.ERROR, "some error");

//        try {
//            cancellation.setStatus(Order.Status.WAITING_FOR_SAMPLE);
//            fail("Expected exception");
//        } catch (LaboratoryException expected) {
//            assertEquals("LAB-0010: Cannot assign status WAITING_FOR_SAMPLE to order cancellation",
//                         expected.getMessage());
//        }

//        try {
//            cancellation.setStatus(Order.Status.PARTIAL_RESULTS);
//            fail("Expected exception");
//        } catch (LaboratoryException expected) {
//            assertEquals("LAB-0010: Cannot assign status PARTIAL_RESULTS to order cancellation", expected.getMessage());
//        }

        cancellation.setStatus(Order.Status.CANCELLED);
        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.CANCELLED,
                    ActStatus.CANCELLED, InvestigationActStatus.ERROR, null);
    }

    /**
     * Tests cancellation of an order linked to a POSTED investigation.
     * <p/>
     * Investigations are locked from editing after a period of time if record locking is enabled, by setting the
     * status to POSTED. This doesn't prevent them from being cancelled by labs.
     */
    @Test
    public void testCancelPostedInvestigation() {
        Act act = orderService.order(investigation);
        Order order = getOrder(act);
        order.setStatus(Order.Status.SUBMITTED);

        investigation = get(investigation);
        assertEquals(ActStatus.IN_PROGRESS, investigation.getStatus());
        investigation.setStatus(ActStatus.POSTED);
        save(investigation);

        order.setStatus(Order.Status.CANCELLED);

        checkStatus(order, investigation, Order.Type.CANCEL, Order.Status.CANCELLED, ActStatus.CANCELLED,
                    InvestigationActStatus.SENT, null);
    }

    /**
     * Tests the {@link OrderImpl#getVisit()} method.
     */
    @Test
    public void testGetVisit() {
        Act act = orderService.order(investigation);
        Order order1 = getOrder(act);
        assertNull(order1.getVisit());

        Act visit = patientFactory.newVisit().patient(patient).addItem(investigation).build();
        Order order2 = getOrder(act);
        assertEquals(visit, order2.getVisit());
    }

    /**
     * Tests the {@link OrderImpl#equals(Object)} an {@link OrderImpl#hashCode()} methods.
     */
    @Test
    public void testEquals() {
        Act act1 = orderService.order(investigation);
        Order orderA = getOrder(act1);
        Order orderB = getOrder(act1);
        assertEquals(orderA, orderA);
        assertEquals(orderA, orderB);

        Act investigation2 = patientFactory.newInvestigation().patient(patient).clinician(clinician)
                .location(location).investigationType(investigationType).laboratory(laboratory)
                .startTime(new Date()).build();

        Act act2 = orderService.order(investigation2);
        Order orderC = getOrder(act2);
        assertNotEquals(orderA, orderC);

        assertEquals(orderA.hashCode(), orderA.hashCode());
        assertEquals(orderA.hashCode(), orderB.hashCode());
        assertNotEquals(orderA.hashCode(), orderC.hashCode());
    }

    /**
     * Creates an {@link Order} given the act.
     *
     * @param act the act
     * @return the corresponding order
     */
    private OrderImpl getOrder(Act act) {
        return new OrderImpl(act, service, patientRules, domainService, transactionManager, handlers, documentRules);
    }

    /**
     * Verifies that the order and investigation state match that expected.
     *
     * @param order                    the order
     * @param investigation            the investigation
     * @param type                     the expected order type
     * @param status                   the expected order status
     * @param investigationStatus      the expected investigation status
     * @param investigationOrderStatus the expected investigation order status
     * @param message                  the expected message. May be {@code null}
     */
    private void checkStatus(Order order, Act investigation, Order.Type type, Order.Status status,
                             String investigationStatus, String investigationOrderStatus, String message) {
        Act act = get(investigation);
        assertEquals(type, order.getType());
        assertEquals(status, order.getStatus());
        assertEquals(investigationStatus, act.getStatus());
        assertEquals(investigationOrderStatus, act.getStatus2());
        IMObjectBean bean = getBean(act);
        assertEquals(message, bean.getString("message"));
    }
}
