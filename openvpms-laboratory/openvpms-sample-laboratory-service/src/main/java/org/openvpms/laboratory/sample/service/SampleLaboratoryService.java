/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.sample.service;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.sync.Changes;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Document;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.order.OrderConfirmation;
import org.openvpms.laboratory.report.ExternalResults;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;
import org.openvpms.laboratory.report.Result;
import org.openvpms.laboratory.report.ResultsBuilder;
import org.openvpms.laboratory.sample.internal.SpeciesTargets;
import org.openvpms.laboratory.service.InvestigationTypes;
import org.openvpms.laboratory.service.Laboratories;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.Orders;
import org.openvpms.laboratory.service.Tests;
import org.openvpms.mapping.model.Cardinality;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.service.MappingProvider;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.service.archetype.ArchetypeInstaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Sample laboratory service.
 *
 * @author Tim Anderson
 */
public class SampleLaboratoryService implements LaboratoryService, MappingProvider, DisposableBean {

    /**
     * The laboratories.
     */
    private final Laboratories laboratories;

    /**
     * The investigation types.
     */
    private final InvestigationTypes investigationTypes;

    /**
     * The tests.
     */
    private final Tests tests;

    /**
     * The orders.
     */
    private final Orders orders;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The mapping service.
     */
    private final MappingService mappingService;

    /**
     * The executor service for asynchronous processing of claims.
     */
    private final ScheduledExecutorService executorService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SampleLaboratoryService.class);

    /**
     * Investigation type archetype.
     */
    private static final String TYPE_ID = "entityIdentity.investigationTypeSample";

    /**
     * Test code archetype.
     */
    private static final String TEST_CODE = "entityIdentity.laboratoryTestSample";

    /**
     * Sample laboratory service archetype.
     */
    private static final String SAMPLE_LABORATORY_SERVICE = "entity.laboratoryServiceSample";

    /**
     * Laboratory order Id archetype.
     */
    private static final String ORDER_ID = "actIdentity.laboratoryOrderSample";

    /**
     * Constructs a {@link SampleLaboratoryService}.
     *
     * @param laboratories       the laboratories
     * @param investigationTypes the investigation types
     * @param tests              the tests
     * @param orders             the orders
     * @param service            the archetype service
     * @param installer          the archetype installer
     * @param mappingService     the mapping service
     */
    public SampleLaboratoryService(Laboratories laboratories, InvestigationTypes investigationTypes, Tests tests,
                                   Orders orders, ArchetypeService service,
                                   ArchetypeInstaller installer, MappingService mappingService) {
        this.laboratories = laboratories;
        this.investigationTypes = investigationTypes;
        this.tests = tests;
        this.orders = orders;
        this.service = service;
        this.mappingService = mappingService;
        executorService = Executors.newSingleThreadScheduledExecutor();
        install(installer, TYPE_ID);
        install(installer, TEST_CODE);
        install(installer, SAMPLE_LABORATORY_SERVICE);
        install(installer, ORDER_ID);
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     * @throws LaboratoryException for any error
     */
    @Override
    public String getName() {
        return "Sample Laboratory Service";
    }

    /**
     * Returns the laboratory device archetype that this supports.
     *
     * @return an <em>entity.laboratoryDevice*</em> archetype
     * @throws LaboratoryException for any error
     */
    @Override
    public String getLaboratoryArchetype() {
        return SAMPLE_LABORATORY_SERVICE;
    }

    /**
     * Determines if orders for a test require confirmation after {@link #order(Order)}.
     *
     * @param test the test
     * @return {@code false}
     */
    @Override
    public boolean confirmOrders(Test test) {
        return false;
    }

    /**
     * Determines if the service can check if an order has updated.
     *
     * @param order the order
     * @return {@code true} if the service can check if the order has updated
     */
    @Override
    public boolean canCheck(Order order) {
        return false;
    }

    /**
     * Checks an order status.
     * <p/>
     * This can be invoked:
     * <ul>
     *     <li>after an order is confirmed to trigger an immediate update of the order status</li>
     *     <li>to add any new results for an order</li>
     * </ul>
     *
     * @param order the order
     * @return {@code true} if the order was updated
     * @throws LaboratoryException for any error
     */
    @Override
    public boolean check(Order order) {
        return false;
    }

    /**
     * Returns the document to print and include with any sample submitted for an order.
     *
     * @param order the order
     * @return the document, or {@code null} if none is required
     * @throws LaboratoryException for any error
     */
    @Override
    public Document getRequestForm(Order order) {
        return null;
    }

    /**
     * Place an order.
     *
     * @param order the order to place
     * @throws LaboratoryException for any error
     */
    @Override
    public void order(Order order) {
        Laboratory laboratory = order.getLaboratory();
        IMObjectBean bean = service.getBean(laboratory);
        int orderDelay = bean.getInt("orderDelay");
        if (orderDelay == 0) {
            // synchronous laboratory
            submitted(order, laboratory);
        } else {
            // asynchronous laboratory
            order.setStatus(Order.Status.SUBMITTING);
            executorService.schedule(() -> asyncSubmit(order, laboratory), orderDelay, TimeUnit.SECONDS);
        }
    }

    /**
     * Invoked when an order has {@link Order.Status#CONFIRM}, to enable the user to confirm the order.
     *
     * @param order the order
     * @return {@code null}
     */
    @Override
    public OrderConfirmation getOrderConfirmation(Order order) {
        return null;
    }

    /**
     * Cancels an order.
     *
     * @param order the order to cancel
     * @throws LaboratoryException for any error
     */
    @Override
    public void cancel(Order order) {
        Laboratory laboratory = order.getLaboratory();
        IMObjectBean bean = service.getBean(laboratory);
        int cancelDelay = bean.getInt("cancelDelay");
        if (cancelDelay == 0) {
            order.setStatus(Order.Status.CANCELLED);
        } else {
            // the service processes cancellation asynchronously
            order.setStatus(Order.Status.SUBMITTING);
            executorService.schedule(() -> order.setStatus(Order.Status.CANCELLED), cancelDelay, TimeUnit.SECONDS);
        }
    }

    /**
     * Returns external results for an order.
     *
     * @param order the order
     * @return the external results, or {@code null} if there are none, or external results are not supported
     * @throws LaboratoryException for any error
     */
    @Override
    public ExternalResults getExternalResults(Order order) {
        return null;
    }

    /**
     * Synchronises data.
     * <p>
     * This adds investigation type and laboratories that aren't already present, updates existing instances if
     * required, and deactivates those that are no longer relevant.
     *
     * @param changes tracks the changes that were made
     * @throws LaboratoryException for any error
     */
    @Override
    public synchronized void synchroniseData(Changes<Entity> changes) {
        List<Laboratory> list = laboratories.getLaboratories(SAMPLE_LABORATORY_SERVICE, true);
        Laboratory laboratory = !list.isEmpty() ? list.get(0) : null;
        InvestigationType type = investigationTypes.getInvestigationTypeBuilder()
                .changes(changes)
                .typeId(TYPE_ID, "INHOUSE_ID")
                .name("Sample Laboratory Service")
                .description("Investigation Type for tests performed by the Sample Laboratory Service")
                .laboratory(laboratory)
                .build();
        addTest("HEM", "Sample Haematology", "Sample Haematology Test", type, changes);
        addTest("FLVFIV", "Sample FelV/FIV", "Sample Feline FeLV/FIV Test", type, changes);
        addTest("AVI", "Sample Avian Wellness Profile",
                "Includes K+, GLOB, NA+, AST, TP, ALB, UA, BA, CK, CA, PHOS, GLU", type, changes);
        addTest("CAN", "Sample Canine Wellness Profile",
                "Includes GLOB, ALP, ALT, TP, CHW, ALB, CRE, TBIL, BUN, CA, PHOS, GLU", type,
                changes);
    }

    /**
     * Returns the mappings.
     *
     * @return the mappings
     */
    @Override
    public List<Mappings<?>> getMappings() {
        IMObject config = mappingService.getMappingConfiguration("org.openvpms.laboratory.sample.species", true,
                                                                 Cardinality.MANY_TO_ONE);
        SpeciesTargets species = new SpeciesTargets("Sample Laboratory Species", "DOG", "CAT", "COW", "HORSE", "PIG",
                                                    "OTHER");
        Mappings<Lookup> mappings = mappingService.createMappings(config, Lookup.class, "lookup.species",
                                                                  "Species Mapping", species);
        return Collections.singletonList(mappings);
    }

    /**
     * Deactivates the service.
     */
    @Override
    public void destroy() {
        executorService.shutdown();
    }

    /**
     * Invoked after a delay to submit an order.
     *
     * @param order      the order
     * @param laboratory the laboratory
     */
    private void asyncSubmit(Order order, Laboratory laboratory) {
        try {
            order = orders.getOrder(order.getInvestigationId());
            // reload the order, as it may have been changed
            if (order != null && order.getType() != Order.Type.CANCEL) {
                submitted(order, laboratory);
            }
        } catch (Throwable exception) {
            log.error("Failed to submit order", exception);
        }
    }

    /**
     * Marks an order as submitted, and schedules result generation.
     *
     * @param order      the order
     * @param laboratory the laboratory
     */
    private void submitted(Order order, Laboratory laboratory) {
        order.setOrderId(ORDER_ID, UUID.randomUUID().toString());
        // NOTE: the lab is not required to provide its internal order identifier. This is provided for labs
        // that aren't able to use the investigation identifier

        order.setStatus(Order.Status.SUBMITTED);
        IMObjectBean bean = service.getBean(laboratory);
        int resultsDelay = bean.getInt("resultsDelay");
        if (resultsDelay > 0) {
            // simulate a Waiting For Sample status
            executorService.schedule(() -> waitingForSample(order.getOrderId()), resultsDelay / 2,
                                     TimeUnit.SECONDS);
        }
        executorService.schedule(() -> generateResults(order.getOrderId()), resultsDelay, TimeUnit.SECONDS);
    }

    /**
     * Sets the report status to {@link Report.Status#WAITING_FOR_SAMPLE}.
     *
     * @param orderId the laboratory identifier for the order
     */
    private void waitingForSample(String orderId) {
        Order order = orders.getOrder(ORDER_ID, orderId);
        if (order != null && order.getType() != Order.Type.CANCEL) {
            order.getReportBuilder().status(Report.Status.WAITING_FOR_SAMPLE).build();
        }
    }

    /**
     * Generates the results for an order.
     *
     * @param orderId the laboratory identifier for the order
     */
    private void generateResults(String orderId) {
        try {
            Order order = orders.getOrder(ORDER_ID, orderId);
            int id = 1;
            if (order != null && order.getType() != Order.Type.CANCEL) {
                ReportBuilder builder = order.getReportBuilder();
                ThreadLocalRandom random = ThreadLocalRandom.current();
                for (Test test : order.getTests()) {
                    ResultsBuilder resultsBuilder = builder.results(Integer.toString(id++))
                            .test(test)
                            .date(OffsetDateTime.now())
                            .notes("Some notes for " + test.getName());
                    int count = random.nextInt(1, 11);
                    for (int i = 0; i < count; ++i) {
                        BigDecimal extremeLowRange = new BigDecimal(0);
                        BigDecimal lowRange = new BigDecimal(10);
                        BigDecimal highRange = new BigDecimal(20);
                        BigDecimal extremeHighRange = new BigDecimal(30);
                        BigDecimal value = BigDecimal.valueOf(random.nextDouble(0, extremeHighRange.intValue()));
                        String note;
                        if (value.compareTo(lowRange) < 0) {
                            note = "Below low range";
                        } else if (value.compareTo(highRange) <= 0) {
                            note = "Within range";
                        } else {
                            note = "Above high range";
                        }
                        int code = i + 1;
                        resultsBuilder.result(Integer.toString(code))
                                .status(Result.Status.COMPLETED)
                                .analyteCode(test.getCode() + "_ANALYTE" + code)
                                .analyteName(test.getName() + " Analyte " + code)
                                .value(value)
                                .units("g/L")
                                .lowRange(lowRange)
                                .highRange(highRange)
                                .extremeLowRange(extremeLowRange)
                                .extremeHighRange(extremeHighRange)
                                .notes(note)
                                .add();
                    }
                    resultsBuilder.add();
                }
                builder.status(Report.Status.COMPLETED).build();
            }
        } catch (Throwable exception) {
            log.error("Failed to generate results", exception);
        }
    }

    /**
     * Installs an archetype.
     *
     * @param installer the archetype installer
     * @param archetype the archetype to install
     */
    private void install(ArchetypeInstaller installer, String archetype) {
        String path = "/org/openvpms/laboratory/sample/internal/" + archetype + ".adl";
        installer.install(getClass(), path);
    }

    /**
     * Adds tests if it doesn't exist.
     *
     * @param testCode    the test code
     * @param name        the test name
     * @param description the test description
     * @param type        the investigation type
     * @param changes     the list to add new investigation types to
     */
    private void addTest(String testCode, String name, String description, InvestigationType type,
                         Changes<Entity> changes) {
        tests.getTestBuilder()
                .changes(changes)
                .testCode(TEST_CODE, testCode)
                .name(name)
                .description(description)
                .investigationType(type)
                .build();
    }

}
