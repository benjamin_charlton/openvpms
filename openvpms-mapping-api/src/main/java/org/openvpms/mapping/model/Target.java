/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.model;

/**
 * Represents a target object in an external system that will be mapped to an OpenVPMS object.
 *
 * @author Tim Anderson
 */
public interface Target {

    /**
     * Returns the target object identifier.
     *
     * @return the target object identifier
     */
    String getId();

    /**
     * Returns a display name for the target object.
     *
     * @return a display name for the target object
     */
    String getName();

    /**
     * Determines if the target object is active.
     *
     * @return {@code true} if the target object is active, {@code false} if it is inactive
     */
    boolean isActive();
}
