/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.model;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.exception.MappingException;

import java.util.List;

/**
 * A collection of mappings.
 *
 * @author Tim Anderson
 */
public interface Mappings<T extends IMObject> {

    /**
     * Returns the type being mapped.
     *
     * @return the type
     */
    Class<T> getType();

    /**
     * Returns the mapping cardinality.
     *
     * @return the mapping cardinality
     */
    Cardinality getCardinality();

    /**
     * Returns a display name for the mappings.
     *
     * @return the display name
     */
    String getDisplayName();

    /**
     * Returns a display name for the source object type.
     *
     * @return the display name
     */
    String getSourceDisplayName();

    /**
     * Returns a display name for the target object type.
     *
     * @return the display name
     */
    String getTargetDisplayName();

    /**
     * Returns the mappings.
     *
     * @return the mappings
     */
    List<Mapping> getMappings();

    /**
     * Returns a mapping given the source.
     * <p/>
     * If there are multiple mappings, the first available will be returned.
     *
     * @param source the source
     * @return the corresponding mapping, or {@code null} if none is found
     */
    Mapping getMapping(T source);

    /**
     * Returns a mapping given the source.
     * <p/>
     * If there are multiple mappings, the first available will be returned.
     *
     * @param source the source
     * @return the corresponding mapping, or {@code null} if none is found
     */
    Mapping getMapping(Reference source);

    /**
     * Returns a mapping given the target identifier.
     * <p/>
     * If there are multiple mappings, the first available will be returned.
     *
     * @param target the target identifier
     * @return the corresponding mapping, or {@code null} if none is found
     */
    Mapping getMapping(String target);

    /**
     * Returns all mappings for the given target identifier.
     *
     * @param target the target identifier
     * @return the corresponding mappings
     */
    List<Mapping> getMappings(String target);

    /**
     * Adds a mapping between a source object and a target.
     *
     * @param source the source object
     * @param target the target
     * @return the mapping
     */
    Mapping add(T source, Target target);

    /**
     * Replaces an existing mapping.
     *
     * @param mapping the mapping
     * @param source  the new source
     * @param target  the new target
     */
    void replace(Mapping mapping, T source, Target target);

    /**
     * Removes a mapping.
     *
     * @param mapping the mapping to remove
     */
    void remove(Mapping mapping);

    /**
     * Makes the mapping changes persistent.
     *
     * @throws MappingException if cardinality constraints are not met
     */
    void save();

    /**
     * Returns the source for a given target.
     *
     * @param target the target identifier
     * @return the source, or {@code null} if no mapping exists
     */
    Reference getSource(String target);

    /**
     * Returns the target for the given source.
     *
     * @param source the source
     * @return the target, or {@code null} if no mapping exists
     */
    Target getTarget(T source);

    /**
     * Returns the target for the given source.
     *
     * @param source the source
     * @return the target, or {@code null} if no mapping exists
     */
    Target getTarget(Reference source);

    /**
     * Returns the target given its identifier.
     *
     * @param target the target identifier
     */
    Target getTarget(String target);

    /**
     * Returns the available sources.
     *
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve
     * @return the available sources
     */
    List<Source> getSources(String name, boolean unmapped, int firstResult, int maxResults);

    /**
     * Returns a count of the sources matching the criteria.
     *
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of sources matching the criteria
     */
    long getSourceCount(String name, boolean unmapped);

    /**
     * Returns the available targets.
     *
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve, or {@code -1} to retrieve all results
     * @return the available targets
     */
    List<Target> getTargets(String name, boolean unmapped, int firstResult, int maxResults);

    /**
     * Returns a count of the targets matching the criteria.
     *
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of targets matching the criteria
     */
    int getTargetCount(String name, boolean unmapped);

}
