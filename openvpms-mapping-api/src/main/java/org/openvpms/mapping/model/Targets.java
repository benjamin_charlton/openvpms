/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.model;

import java.util.List;

/**
 * Targets available for mapping.
 *
 * @author Tim Anderson
 * @see Mappings
 */
public interface Targets {

    /**
     * Returns a display name for the target object type.
     *
     * @return the display name
     */
    String getDisplayName();

    /**
     * Creates a target.
     *
     * @param identity the target object identifier
     * @param name     the target object display name
     * @param active   determines if the target object is active
     * @return a new target
     */
    Target create(String identity, String name, boolean active);

    /**
     * Returns a target given its identifier.
     *
     * @param identity the target object identifier
     * @return the target, or {@code null} if none exists
     */
    Target getTarget(String identity);

    /**
     * Returns the available targets.
     *
     * @param mappings    the mappings
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve, or {@code -1} to retrieve all results
     * @return the available targets
     */
    List<Target> getTargets(Mappings mappings, String name, boolean unmapped, int firstResult, int maxResults);

    /**
     * Returns a count of the targets matching the criteria.
     *
     * @param mappings the mappings
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of targets matching the criteria
     */
    int count(Mappings mappings, String name, boolean unmapped);

}
