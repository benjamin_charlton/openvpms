/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.service;

import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.mapping.model.Cardinality;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Targets;

/**
 * Mapping service.
 *
 * @author Tim Anderson
 * @see Mappings
 * @see Targets
 */
public interface MappingService {

    /**
     * Create a configuration to record mappings between the specified archetype and a target, using the default
     * cardinality.
     *
     * @param type the type being mapped
     * @return a new configuration to record mappings
     */
    <T extends IMObject> IMObject createMappingConfiguration(Class<T> type);

    /**
     * Create a configuration to record mappings between the specified archetype and a target.
     *
     * @param type        the type being mapped
     * @param cardinality the mapping cardinality
     * @return a new configuration to record mappings
     */
    <T extends IMObject> IMObject createMappingConfiguration(Class<T> type, Cardinality cardinality);

    /**
     * Returns a mapping configuration for mapping lookups, optionally creating it if it doesn't exist.
     * <p/>
     * The specified lookup code must be unique. A simple way of achieving this is to encode the domain name with
     * an internal identifier. e.g. <em>com.mydomain.myplugin.id</em>
     *
     * @param code        the lookup code
     * @param create      if {@code true}, create and save the configuration if it does not exist
     * @param cardinality the cardinality to use if creating the configuration, use {@code null} to use the default
     * @return the configuration, or {@code null} if it does not exist, and {@code create} is {@code false}
     */
    Lookup getMappingConfiguration(String code, boolean create, Cardinality cardinality);

    /**
     * Creates mappings.
     *
     * @param config      the mapping configuration
     * @param type        the type being mapped
     * @param archetype   the archetype being mapped
     * @param displayName the mapping display name
     * @param targets     the targets
     * @return the mappings
     */
    <T extends IMObject> Mappings<T> createMappings(IMObject config, Class<T> type, String archetype,
                                                    String displayName, Targets targets);

    /**
     * Creates mappings.
     *
     * @param config            the mapping configuration
     * @param type              the type being mapped
     * @param archetype         the archetype being mapped
     * @param displayName       the mapping display name
     * @param sourceDisplayName the source object type display name
     * @param targets           the targets
     * @return the mappings
     */
    <T extends IMObject> Mappings<T> createMappings(IMObject config, Class<T> type, String archetype,
                                                    String displayName, String sourceDisplayName, Targets targets);

}
