/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util.filter;

import org.junit.Test;

import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.core.MediaType;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link PlainTextErrorMessageReader}.
 *
 * @author Tim Anderson
 */
public class PlainTextErrorMessageReaderTestCase {

    /**
     * Tests the {@link PlainTextErrorMessageReader#canRead(ClientResponseContext)} method.
     */
    @Test
    public void testCanRead() {
        checkCanRead(MediaType.TEXT_PLAIN_TYPE, true);
        checkCanRead(MediaType.APPLICATION_JSON_TYPE, false);
        checkCanRead(MediaType.APPLICATION_XML_TYPE, false);
        checkCanRead(MediaType.TEXT_XML_TYPE, false);
        checkCanRead(null, false);
    }

    /**
     * Tests that messages can be read.
     */
    @Test
    public void testReader() {
        checkRead("test", "test");
        checkRead("test", "\n\ntest\n\n");
        checkRead(null, "");
    }

    /**
     * Tests the {@link PlainTextErrorMessageReader#canRead(ClientResponseContext)} method.
     *
     * @param type    the media type
     * @param canRead if {@code true}, the type should be readable, otherwise not
     */
    private void checkCanRead(MediaType type, boolean canRead) {
        ClientResponseContext response = mock(ClientResponseContext.class);
        when(response.getMediaType()).thenReturn(type);
        assertEquals(canRead, new PlainTextErrorMessageReader().canRead(response));
    }

    /**
     * Tests the {@link JAXBErrorMessageReader#read(ClientResponseContext)} method.
     *
     * @param expected the expected message
     * @param body     the response body
     */
    private void checkRead(String expected, String body) {
        ClientResponseContext response = mock(ClientResponseContext.class);
        when(response.getMediaType()).thenReturn(MediaType.TEXT_PLAIN_TYPE);
        when(response.getEntityStream()).thenReturn(new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8)));

        PlainTextErrorMessageReader reader = new PlainTextErrorMessageReader();
        assertEquals(expected, reader.read(response));
    }

}
