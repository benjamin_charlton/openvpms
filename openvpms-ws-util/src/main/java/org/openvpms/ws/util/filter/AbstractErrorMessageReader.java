/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util.filter;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.message.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Reads an error message from a {@link ClientResponseContext}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractErrorMessageReader implements ErrorMessageReader {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractErrorMessageReader.class);

    /**
     * Reads an error message from a response.
     *
     * @param response the response
     * @return the error message. May be {@code null}
     */
    @Override
    public String read(ClientResponseContext response) {
        String message = null;
        try {
            message = readProtected(response);
        } catch (Throwable exception) {
            log.error("Failed to read error from response: " + exception.getMessage(), exception);
        }
        return message;
    }

    /**
     * Reads an error message from a response.
     *
     * @param response the response
     * @return the error message. May be {@code null}
     * @throws Throwable for any error
     */
    protected String readProtected(ClientResponseContext response) throws Throwable {
        return getString(response);
    }

    /**
     * Reads a string from the response.
     *
     * @param response the response
     * @return the corresponding string. May be {@code null}
     * @throws IOException for any I/O error
     */
    protected String getString(ClientResponseContext response) throws IOException {
        Charset charset = MessageUtils.getCharset(getMediaType(response));
        if (charset == null) {
            charset = StandardCharsets.UTF_8;
        }
        InputStreamReader reader = new InputStreamReader(response.getEntityStream(), charset);
        return IOUtils.toString(reader);
    }

    /**
     * Returns the media type from the response.
     *
     * @param response the response
     * @return the media type, or {@code null} if it is not present or malformed
     */
    protected MediaType getMediaType(ClientResponseContext response) {
        try {
            return response.getMediaType();
        } catch (Throwable exception) {
            return null;
        }
    }

}
