/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.ws.util.filter;

import org.openvpms.ws.util.MediaTypes;

import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

/**
 * Reads an error message from a {@link ClientResponseContext} serialised to XML using a JAXB class.
 * <p/>
 * If the error type doesn't provide a {@code toString()} method to access the error message,
 * the {@link #toString(Object)} method should be overridden to provide the functionality.
 *
 * @author Tim Anderson
 */
public class JAXBErrorMessageReader<T> extends AbstractSerialisedErrorMessageReader<T> {

    /**
     * The JAXB context.
     */
    private final JAXBContext context;

    /**
     * Constructs a {@link JAXBErrorMessageReader}.
     *
     * @param type the type of the error to deserialise
     * @throws JAXBException for any JAXB error
     */
    public JAXBErrorMessageReader(Class<T> type) throws JAXBException {
        this(JAXBContext.newInstance(type), type);
    }

    /**
     * Constructs a {@link JAXBErrorMessageReader}.
     *
     * @param context the JAXB context
     * @param type    the type of the error to deserialise
     */
    public JAXBErrorMessageReader(JAXBContext context, Class<T> type) {
        super(type);
        this.context = context;
    }

    /**
     * Determines if this reader can read messages from a response.
     *
     * @param response the response
     * @return {@code true} if the response is supported, otherwise {@code false}
     */
    @Override
    public boolean canRead(ClientResponseContext response) {
        return MediaTypes.isA(getMediaType(response), MediaType.APPLICATION_XML_TYPE, MediaType.TEXT_XML_TYPE);
    }

    /**
     * Deserialises an instance of the specified type from a string.
     *
     * @param value the string to deserialise
     * @param type  the type
     * @return the deserialised object. May be {@code null}
     * @throws JAXBException if the object cannot be deserialised
     */
    @Override
    protected T deserialise(String value, Class<T> type) throws JAXBException {
        StreamSource source = new StreamSource(new StringReader(value));
        JAXBElement<T> element = context.createUnmarshaller().unmarshal(source, type);
        return (element != null) ? element.getValue() : null;
    }

}
