/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.ws.util.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openvpms.ws.util.MediaTypes;

import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Reads an error message from a {@link ClientResponseContext} serialised as JSON.
 * <p/>
 * If the error type doesn't provide a {@code toString()} method to access the error message,
 * the {@link #toString(Object)} method should be overridden to provide the functionality.
 *
 * @author Tim Anderson
 */
public class JSONErrorMessageReader<T> extends AbstractSerialisedErrorMessageReader<T> {

    /**
     * The object mapper.
     */
    private final ObjectMapper mapper;

    /**
     * Constructs an {@link JSONErrorMessageReader}.
     *
     * @param mapper the object mapper
     * @param type   the type to deserialise
     */
    public JSONErrorMessageReader(ObjectMapper mapper, Class<T> type) {
        super(type);
        this.mapper = mapper;
    }

    /**
     * Determines if this reader can read messages from a response.
     *
     * @param response the response
     * @return {@code true} if the response is supported, otherwise {@code false}
     */
    @Override
    public boolean canRead(ClientResponseContext response) {
        return MediaTypes.isA(getMediaType(response), MediaType.APPLICATION_JSON_TYPE);
    }

    /**
     * Deserialises an instance of the specified type from a string.
     *
     * @param value the string to deserialise
     * @param type  the type
     * @return the deserialised object. May be {@code null}
     * @throws IOException if the object cannot be deserialised
     */
    @Override
    protected T deserialise(String value, Class<T> type) throws IOException {
        return mapper.readValue(value, type);
    }
}
