/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util;

import org.glassfish.jersey.logging.LoggingFeature;
import org.slf4j.Logger;

import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.regex.Pattern;

/**
 * An {@link LoggingFeature} that can log to SLF4J.
 * <p/>
 * The feature is only enabled if the supplied logger has debug level enabled.
 *
 * @author Tim Anderson
 */
public class SLF4JLoggingFeature extends LoggingFeature {

    /**
     * The log to use.
     */
    private final Logger log;

    /**
     * Constructs an {@link SLF4JLoggingFeature}.
     *
     * @param log              the log to use
     * @param protectedHeaders headers whose values should be replaced with placeholders to avoid logging sensitive
     *                         details
     */
    public SLF4JLoggingFeature(Logger log, String... protectedHeaders) {
        super(new Log(log, (protectedHeaders.length > 0) ? new HeaderReplacer(protectedHeaders) : null));
        this.log = log;
    }

    /**
     * Constructs an {@link SLF4JLoggingFeature}.
     *
     * @param log           the log to use
     * @param maxEntitySize maximum number of entity bytes to be logged (and buffered) - if the entity is larger,
     *                      it will print (and buffer in memory) only the specified number of bytes
     */
    public SLF4JLoggingFeature(Logger log, int maxEntitySize, String... protectedHeaders) {
        super(new Log(log, (protectedHeaders.length > 0) ? new HeaderReplacer(protectedHeaders) : null), maxEntitySize);
        this.log = log;
    }

    /**
     * Constructs a {@link SLF4JLoggingFeature}.
     *
     * @param log      the log to use
     * @param replacer a function to replace sensitive text in log messages
     */
    public SLF4JLoggingFeature(Logger log, Function<String, String> replacer) {
        super(new Log(log, replacer));
        this.log = log;
    }

    /**
     * Constructs a {@link SLF4JLoggingFeature}.
     *
     * @param log           the log to use
     * @param maxEntitySize maximum number of entity bytes to be logged (and buffered) - if the entity is larger,
     *                      it will print (and buffer in memory) only the specified number of bytes
     * @param replacer      a function to replace sensitive text in log messages
     */
    public SLF4JLoggingFeature(Logger log, int maxEntitySize, Function<String, String> replacer) {
        super(new Log(log, replacer), maxEntitySize);
        this.log = log;
    }

    /**
     * A call-back method called when the feature is to be enabled in a given
     * runtime configuration scope.
     * <p>
     * The responsibility of the feature is to properly update the supplied runtime configuration context
     * and return {@code true} if the feature was successfully enabled or {@code false} otherwise.
     * <p>
     * Note that under some circumstances the feature may decide not to enable itself, which
     * is indicated by returning {@code false}. In such case the configuration context does
     * not add the feature to the collection of enabled features and a subsequent call to
     * {@link Configuration#isEnabled(Feature)} or {@link Configuration#isEnabled(Class)} method
     * would return {@code false}.
     * </p>
     *
     * @param context configurable context in which the feature should be enabled.
     * @return {@code true} if the feature was successfully enabled, {@code false}
     * otherwise.
     */
    @Override
    public boolean configure(FeatureContext context) {
        boolean enabled = false;
        if (log.isDebugEnabled()) {
            enabled = super.configure(context);
        }
        return enabled;
    }

    /**
     * Routes java.util.logging.Logger to SL4J.
     */
    private static final class Log extends java.util.logging.Logger {

        /**
         * The logger to delegate to.
         */
        private final Logger log;

        /**
         * A function to replace sensitive text in log messages.
         */
        private final Function<String, String> replacer;

        /**
         * Constructs a {@link Log}.
         *
         * @param log      the logger to delegate to
         * @param replacer a function to replace sensitive content. May be {@code null}
         */
        Log(Logger log, Function<String, String> replacer) {
            super(GLOBAL_LOGGER_NAME, null);
            setLevel(Level.FINE);
            this.log = log;
            this.replacer = replacer;
        }

        /**
         * Log a message, with no arguments.
         * <p>
         * If the logger is currently enabled for the given message
         * level then the given message is forwarded to all the
         * registered output Handler objects.
         * <p>
         *
         * @param level One of the message level identifiers, e.g., SEVERE
         * @param msg   The string message (or a key in the message catalog)
         */
        @Override
        public void log(Level level, String msg) {
            if (msg != null && replacer != null) {
                msg = replacer.apply(msg);
            }
            log.debug(msg);
        }
    }

    /**
     * Replaces protected headers in log messages.
     * <p/>
     * NOTE - this is brittle, but LoggingFeature does not expose any means to exclude particular headers.
     */
    private static class HeaderReplacer implements Function<String, String> {

        private final Pattern pattern;

        public HeaderReplacer(String[] protectedHeaders) {
            StringBuilder builder = new StringBuilder();
            for (String header : protectedHeaders) {
                if (builder.length() > 0) {
                    builder.append("|");
                }
                builder.append("> (").append(header).append("):.*");
            }
            pattern = Pattern.compile(builder.toString());
        }

        /**
         * Applies this function to the given argument.
         *
         * @param string the function argument
         * @return the function result
         */
        @Override
        public String apply(String string) {
            return pattern.matcher(string).replaceAll("> $1: xxxxxxxx");
        }
    }

}
