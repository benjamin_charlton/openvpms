<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Invoice" pageWidth="421" pageHeight="595" columnWidth="381" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" isSummaryWithPageHeaderAndFooter="true" resourceBundle="localisation.reports" uuid="581e7115-62b6-496f-b5ae-1a20c9e025ad">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="org.openvpms.archetype.function.party.PartyFunctions"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="7"/>
	<parameter name="dataSource" class="org.openvpms.report.jasper.IMObjectCollectionDataSource" isForPrompting="false"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="claim" class="java.lang.Object" isForPrompting="false"/>
	<field name="createdBy.name" class="java.lang.String"/>
	<field name="customer.entity.name" class="java.lang.String"/>
	<field name="customer.entity.companyName" class="java.lang.String"/>
	<field name="customer.entity.lastName" class="java.lang.String"/>
	<field name="customer.entity.firstName" class="java.lang.String"/>
	<field name="customer.entity.title" class="java.lang.String"/>
	<field name="customer.entity.initials" class="java.lang.String"/>
	<field name="customer.entity.id" class="java.lang.Long"/>
	<field name="startTime" class="java.util.Date"/>
	<field name="id" class="java.lang.Long"/>
	<field name="tax" class="java.math.BigDecimal"/>
	<field name="amount" class="java.math.BigDecimal"/>
	<field name="notes" class="java.lang.String"/>
	<field name="[party:getBillingAddress(.)]" class="java.lang.String"/>
	<field name="allocatedAmount" class="java.math.BigDecimal"/>
	<field name="[party:getAccountBalance(.)]" class="java.math.BigDecimal"/>
	<field name="[party:getPartyFullName(.)]" class="java.lang.String"/>
	<field name="status.code" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.appointmentsInvoice" class="java.lang.Boolean"/>
	<field name="OpenVPMS.location.letterhead.target.remindersInvoice" class="java.lang.Boolean"/>
	<field name="OpenVPMS.location.letterhead.target.subreport" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.lastPageFooter" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.invoiceMsg" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.invoicePay" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.plainPaper" class="java.lang.String"/>
	<field name="reference" class="java.lang.String"/>
	<field name="[count(list:distinct(., &quot;items.target.patient.entity.id&quot;))]" class="java.lang.Double">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="OpenVPMS.location.letterhead.target.useProductTypes" class="java.lang.Boolean"/>
	<field name="OpenVPMS.practice.showPricesTaxInclusive" class="java.lang.Boolean">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="hide" class="java.lang.Boolean"/>
	<variable name="TOTAL_DISCOUNT" class="java.math.BigDecimal"/>
	<variable name="CustomerFullName" class="java.lang.String" resetType="None">
		<variableExpression><![CDATA[(($F{customer.entity.title} == null) ? "": $F{customer.entity.title}) + " " + (($F{customer.entity.firstName} == null) ? "": $F{customer.entity.firstName}) + " " + $F{customer.entity.lastName}]]></variableExpression>
	</variable>
	<variable name="incTax" class="java.lang.Boolean">
		<variableExpression><![CDATA[$F{OpenVPMS.practice.showPricesTaxInclusive}]]></variableExpression>
		<initialValueExpression><![CDATA[Boolean.FALSE]]></initialValueExpression>
	</variable>
	<variable name="isPaid" class="java.lang.Boolean">
		<variableExpression><![CDATA[$F{amount}.compareTo($F{allocatedAmount} )==0]]></variableExpression>
		<initialValueExpression><![CDATA[Boolean.FALSE]]></initialValueExpression>
	</variable>
	<variable name="isFinalised" class="java.lang.Boolean" calculation="First">
		<variableExpression><![CDATA["POSTED".equals($F{status.code})]]></variableExpression>
		<initialValueExpression><![CDATA[Boolean.FALSE]]></initialValueExpression>
	</variable>
	<variable name="showInvPay" class="java.lang.Boolean">
		<variableExpression><![CDATA[($F{OpenVPMS.location.letterhead.target.invoicePay}==null)?Boolean.FALSE:
($F{OpenVPMS.location.letterhead.target.invoicePay}.trim().length()>0)]]></variableExpression>
	</variable>
	<variable name="RefName" class="java.lang.String">
		<variableExpression><![CDATA[($F{customer.entity.companyName} != null) ? ($F{customer.entity.companyName}+"     ").substring(0,5).toUpperCase(): ($F{customer.entity.lastName}+"     ").substring(0,5).toUpperCase()]]></variableExpression>
	</variable>
	<group name="Dummy group" footerPosition="ForceAtBottom">
		<groupFooter>
			<band height="71"/>
		</groupFooter>
	</group>
	<group name="Summary group">
		<groupFooter>
			<band height="55" splitType="Prevent">
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField-5" positionType="Float" x="19" y="16" width="344" height="25" isRemoveLineWhenBlank="true" uuid="6ada29e1-4b4d-4bfe-8fd5-57c0f45b4fd9">
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="7"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{notes}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-8" positionType="Float" x="201" y="0" width="115" height="11" isRemoveLineWhenBlank="true" uuid="d6b38d2e-ffaf-47e1-9b13-fe5ab7b2113f">
						<property name="local_mesure_unitx" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.x" value="px"/>
						<property name="local_mesure_unitheight" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font size="7" isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Balance Owing]]></text>
				</staticText>
				<textField pattern="¤ #,##0.00" isBlankWhenNull="false">
					<reportElement key="textField-7" positionType="Float" x="321" y="0" width="55" height="11" isRemoveLineWhenBlank="true" uuid="39e58f46-927d-4737-b66f-976dc8fae767">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font size="7" isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{amount}.subtract($F{allocatedAmount})]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band height="424" splitType="Stretch">
			<staticText>
				<reportElement x="127" y="21" width="127" height="403" forecolor="#FF0000" uuid="4dffbaff-e7d3-45ed-8527-cb27eb0281d7">
					<printWhenExpression><![CDATA[$F{hide}]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="Right">
					<font size="70" isBold="true"/>
				</textElement>
				<text><![CDATA[Cancelled]]></text>
			</staticText>
		</band>
	</background>
	<title>
		<band height="110" splitType="Stretch">
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="381" height="18" isPrintWhenDetailOverflows="true" uuid="7c3f6962-4794-4970-85c4-ed947a50dd68">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" positionType="Float" x="325" y="56" width="56" height="10" uuid="916d78ee-e9df-4f6a-b0f8-815dee55ca60">
					<property name="local_mesure_unitheight" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{startTime})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" positionType="Float" x="275" y="66" width="49" height="10" uuid="43236ac5-f08a-4d34-8635-c55cf60b5d06">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Reference]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="275" y="56" width="49" height="10" uuid="ff9f9542-7d52-4a89-a396-854c81c32451">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-4" positionType="Float" x="325" y="66" width="56" height="10" uuid="8ddafe7e-46aa-4163-9a9c-b4cc87e1ec0d">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reference}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="275" y="46" width="49" height="10" uuid="6f0d43cb-595a-4674-9797-400100e9d68c">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer ID]]></text>
			</staticText>
			<textField>
				<reportElement positionType="Float" x="325" y="46" width="56" height="10" uuid="99788e56-3d50-4d5e-b754-2c628edb9ae9">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer.entity.id}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Master">
				<reportElement positionType="Float" x="325" y="86" width="56" height="9" uuid="2e89cc29-099b-4b73-8df7-a13e8fd4e1c9"/>
				<textElement textAlignment="Left"/>
				<textFieldExpression><![CDATA["Page 1 of "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement key="staticText-1" positionType="Float" x="267" y="18" width="114" height="20" uuid="02f67b9f-2ef1-4eef-adc3-0ed9216c1d66"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{text.invoiceTitle}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" positionType="Float" x="275" y="76" width="49" height="10" uuid="cef68f2a-22fb-4bce-a4ce-686bff68902a">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Invoice No]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-4" positionType="Float" x="325" y="76" width="56" height="10" uuid="dddaf9d5-1ced-45d7-a092-22e57b03253c">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement key="" positionType="Float" mode="Opaque" x="267" y="86" width="57" height="24" forecolor="#FF0000" backcolor="#FFFFFF" uuid="4c23c9f5-7e76-4ff3-9114-31c0c6384d3c">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineColor="#FFFFFF"/>
					<leftPen lineColor="#FFFFFF"/>
					<bottomPen lineColor="#FFFFFF"/>
					<rightPen lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" markup="html">
					<font size="14" isBold="true" isItalic="false" isUnderline="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{isFinalised} ? "":"DRAFT"]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement positionType="Float" x="0" y="18" width="262" height="77" uuid="f13e4b5b-4269-46fd-b8b8-a54e5d8457e9">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="Addr1">
					<subreportParameterExpression><![CDATA[$F{customer.entity.companyName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="Addr2">
					<subreportParameterExpression><![CDATA[$V{CustomerFullName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="Addr3">
					<subreportParameterExpression><![CDATA[$F{[party:getBillingAddress(.)]}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+" AddressBlock"+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</title>
	<pageHeader>
		<band height="50" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER}>1]]></printWhenExpression>
			<textField>
				<reportElement key="staticText-1" positionType="Float" x="120" y="18" width="140" height="20" uuid="050468c9-0d64-4233-bea9-501173d8a06d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{text.invoiceTitle}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement positionType="Float" x="350" y="18" width="31" height="9" uuid="be6f6ed6-e2f7-41c9-acce-fbe94c90a25a"/>
				<textElement textAlignment="Left">
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[" of " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="304" y="18" width="46" height="9" uuid="6e137b7f-d25f-4e15-b09b-cf9113211d01">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Right">
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="381" height="18" isPrintWhenDetailOverflows="true" uuid="52a2672e-a6f2-4503-8ee2-f16297b1ced8">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="26" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" x="0" y="0" width="381" height="25" uuid="51316fe0-9cf1-43c6-b495-44df156ff4b7">
					<property name="local_mesure_unitwidth" value="pixel"/>
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<subreportParameter name="incTax">
					<subreportParameterExpression><![CDATA[$V{incTax}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="patientCount">
					<subreportParameterExpression><![CDATA[$F{[count(list:distinct(., "items.target.patient.entity.id"))]}.intValue()]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="claim">
					<subreportParameterExpression><![CDATA[$P{claim}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("items")]]></dataSourceExpression>
				<returnValue subreportVariable="SUM_target.discount" toVariable="TOTAL_DISCOUNT"/>
				<subreportExpression><![CDATA["Insurance Claim Invoice Items.jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
</jasperReport>
