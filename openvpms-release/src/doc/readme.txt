OpenVPMS ${project.version} (${buildNumber}) Release
===============================================================================

For installation instructions see:
     http://openvpms.org/documentation/csh/2.3/topics/install

There is comprehensive documentation available at:
     http://openvpms.org/documentation/csh/2.3/topics

For a list of new features in this release, see:
     http://openvpms.org/documentation/csh/2.3/new-features



