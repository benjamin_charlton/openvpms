#!/bin/sh

. ./setenv.sh

java -Dlog4j.configurationFile=file:../conf/log4j2.xml -classpath $CLASSPATH org.openvpms.etl.tools.doc.DocumentLoader -c ../conf/applicationContext.xml $*
