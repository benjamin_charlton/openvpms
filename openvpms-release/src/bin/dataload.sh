#!/bin/sh

. ./setenv.sh

if [ "$1" = "base" ] ;
then
    java -Dlog4j.configurationFile=file:../conf/log4j2.xml -classpath $CLASSPATH org.openvpms.tools.data.loader.StaxArchetypeDataLoader -c ../conf/applicationContext.xml -f ../import/data/base.xml,../import/data/roles.xml -b 1000
elif [ "$1" = "setup" ] ;
then
    java -Dlog4j.configurationFile=file:../conf/log4j2.xml -classpath $CLASSPATH org.openvpms.tools.data.loader.StaxArchetypeDataLoader -c ../conf/applicationContext.xml -f ../import/data/base.xml,../import/data/setup.xml,../import/data/roles.xml -b 1000
else
    java -Dlog4j.configurationFile=file:../conf/log4j2.xml -classpath $CLASSPATH org.openvpms.tools.data.loader.StaxArchetypeDataLoader -c ../conf/applicationContext.xml $* -b 1000
fi
