/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.dispatcher;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.SystemMessageReason;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.user.User;
import org.openvpms.esci.FutureValue;
import org.openvpms.esci.adapter.dispatcher.order.OrderResponseListener;
import org.openvpms.esci.adapter.dispatcher.order.OrderResponseProcessor;
import org.openvpms.esci.adapter.dispatcher.order.SystemMessageOrderResponseListener;
import org.openvpms.esci.adapter.map.order.AbstractOrderResponseTest;
import org.openvpms.esci.adapter.map.order.OrderResponseMapper;
import org.openvpms.esci.adapter.util.ESCIAdapterException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Tests the {@link OrderResponseProcessor} class.
 *
 * @author Tim Anderson
 */
public class OrderResponseProcessorTestCase extends AbstractOrderResponseTest {

    /**
     * Tests processing an order response.
     *
     * @throws Exception for any error
     */
    @Test
    public void testProcess() throws Exception {
        User user = TestHelper.createUser();
        new AuthenticationContextImpl().setUser(user); // will be set on the createdBy node on the order

        // set up the listener
        final FutureValue<FinancialAct> future = new FutureValue<>();
        SystemMessageOrderResponseListener listener = new SystemMessageOrderResponseListener(getArchetypeService()) {
            @Override
            public void receivedResponse(FinancialAct order) {
                super.receivedResponse(order);
                future.set(order);
            }
        };

        // set up the processor
        OrderResponseProcessor processor = createProcessor(listener);

        // create an order and refer to it in the response
        FinancialAct order = createOrder();
        InboxDocument response = createOrderResponseDocument(order.getId(), true);
        String accountId = null;
        processor.process(response, getSupplier(), getStockLocation(), accountId, null);

        FinancialAct updatedOrder = future.get(1000);
        assertNotNull(updatedOrder);
        assertEquals(order, updatedOrder);

        // verify a system message was sent to the user
        checkSystemMessage(user, updatedOrder, SystemMessageReason.ORDER_ACCEPTED);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is raised if {@link DocumentProcessor#process}
     * encounters an unexpected exception.
     */
    @Test
    public void testFailedToProcess() {
        getStockLocation().setName("Main Stock");
        getSupplier().setName("Vetshare");

        // create an order and refer to it in the response
        FinancialAct order = createOrder();
        InboxDocument response = createOrderResponseDocument(order.getId(), true);
        OrderResponseProcessor processor = new OrderResponseProcessor(createOrderResponseMapper(), null,
                                                                      getArchetypeService()) {
            @Override
            protected void notifyListener(FinancialAct order) {
                throw new RuntimeException("Foo");
            }
        };

        String expected = "ESCIA-0500: Failed to process OrderResponseSimple 12345 for supplier Vetshare ("
                          + getSupplier().getId() + ") and stock location Main Stock ("
                          + getStockLocation().getId() + "): Foo";
        try {
            processor.process(response, getSupplier(), getStockLocation(), null, null);
        } catch (ESCIAdapterException exception) {
            assertEquals(expected, exception.getMessage());
        }
    }

    /**
     * Creates a new order response processor.
     *
     * @param listener the response listener
     * @return a new processor
     */
    private OrderResponseProcessor createProcessor(OrderResponseListener listener) {
        OrderResponseMapper mapper = createOrderResponseMapper();
        return new OrderResponseProcessor(mapper, listener, getArchetypeService());
    }

}
