/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.map.invoice;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.esci.adapter.i18n.ESCIAdapterMessages;
import org.openvpms.esci.adapter.map.AbstractUBLMapper;
import org.openvpms.esci.adapter.map.ErrorContext;
import org.openvpms.esci.adapter.map.UBLHelper;
import org.openvpms.esci.adapter.map.UBLType;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.ubl.invoice.InvoiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * Maps UBL invoices to <em>act.supplierDelivery</em> acts.
 *
 * @author Tim Anderson
 */
public class InvoiceMapperImpl extends AbstractUBLMapper implements InvoiceMapper {

    /**
     * The practice rules.
     */
    private final PracticeRules practiceRules;

    /**
     * The supplier rules.
     */
    private final SupplierRules supplierRules;

    /**
     * The product rules.
     */
    private final ProductRules productRules;

    /**
     * The currencies.
     */
    private final Currencies currencies;

    /**
     * The lookup service.
     */
    private final ILookupService lookupService;

    /**
     * The package helper.
     */
    private final PackageHelper packageHelper;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InvoiceMapperImpl.class);

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * Constructs an {@link InvoiceMapperImpl}.
     *
     * @param practiceRules the practice rules
     * @param supplierRules the supplier rules
     * @param productRules  the product rules
     * @param currencies    the currencies service
     * @param service       the archetype service
     * @param lookups       the lookup service
     */
    public InvoiceMapperImpl(PracticeRules practiceRules, SupplierRules supplierRules, ProductRules productRules,
                             Currencies currencies, IArchetypeService service, ILookupService lookups) {
        super(service);
        this.practiceRules = practiceRules;
        this.supplierRules = supplierRules;
        this.productRules = productRules;
        this.currencies = currencies;
        this.lookupService = lookups;
        packageHelper = new PackageHelper(productRules, service, lookupService);
    }

    /**
     * Maps an UBL invoice to an <em>act.supplierDelivery</em>.
     *
     * @param invoice       the invoice to map
     * @param supplier      the supplier that submitted the invoice
     * @param stockLocation the stock location
     * @param accountId     the supplier assigned account identifier. May be {@code null}
     * @return the acts produced in the mapping. The first element is always the <em>act.supplierDelivery</em>
     * @throws ESCIAdapterException if the invoice cannot be mapped
     * @throws OpenVPMSException    for any OpenVPMS error
     */
    public Delivery map(InvoiceType invoice, Party supplier, Party stockLocation, String accountId) {
        return map(invoice, supplier, stockLocation, accountId, new DefaultOrderResolver(getService()));
    }

    /**
     * Maps an UBL invoice to an <em>act.supplierDelivery</em>.
     *
     * @param invoice       the invoice to map
     * @param supplier      the supplier that submitted the invoice
     * @param stockLocation the stock location
     * @param accountId     the supplier assigned account identifier. May be {@code null}
     * @param orderResolver the order resolver to use
     * @return the acts produced in the mapping. The first element is always the <em>act.supplierDelivery</em>
     * @throws ESCIAdapterException if the invoice cannot be mapped
     * @throws OpenVPMSException    for any OpenVPMS error
     */
    public Delivery map(InvoiceType invoice, Party supplier, Party stockLocation, String accountId,
                        OrderResolver orderResolver) {
        Delivery result = new Delivery();
        IArchetypeService service = getService();
        Currency practiceCurrency = UBLHelper.getCurrency(practiceRules, currencies, service);
        TaxRates rates = new TaxRates(service, lookupService);
        UBLInvoice wrapper = new UBLInvoice(invoice, practiceCurrency.getCode(), service, supplierRules);
        String invoiceId = wrapper.getMandatoryID();
        checkUBLVersion(wrapper);
        Date issueDatetime = wrapper.getIssueDatetime();
        String notes = wrapper.getNotes();
        wrapper.checkSupplier(supplier, accountId);
        wrapper.checkStockLocation(stockLocation, accountId);

        checkDuplicateInvoice(supplier, invoiceId);

        InvoiceOrderMatcher matcher = new InvoiceOrderMatcher(getService(), orderResolver);
        MappingContext context = matcher.match(wrapper, supplier, stockLocation);

        result.setOrder(context.getDocumentOrder());

        BigDecimal payableAmount = wrapper.getPayableAmount();
        BigDecimal invoiceLineExtensionAmount = wrapper.getLineExtensionAmount();
        BigDecimal chargeTotal = wrapper.getChargeTotal();
        BigDecimal taxExclusiveAmount = wrapper.getTaxExclusiveAmount();
        BigDecimal taxTotal = wrapper.getTaxAmount();

        IMObjectBean delivery = service.getBean(service.create(SupplierArchetypes.DELIVERY));
        delivery.setValue(START_TIME, issueDatetime);
        delivery.setValue("supplierNotes", notes);
        delivery.setValue("amount", payableAmount);
        delivery.setValue("tax", taxTotal);
        ActIdentity identity = service.create(SupplierArchetypes.ESCI_INVOICE_ID, ActIdentity.class);
        identity.setIdentity(invoiceId);
        delivery.addValue("supplierInvoiceId", identity);
        delivery.setTarget("supplier", supplier);
        delivery.setTarget("stockLocation", stockLocation);
        result.setDelivery(delivery.getObject(FinancialAct.class));
        List<InvoiceLineState> lines = context.getInvoiceLines();
        if (lines.isEmpty()) {
            throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidCardinality(
                    "InvoiceLine", "Invoice", invoiceId, "1..*", 0));
        }
        BigDecimal itemTaxIncTotal;
        BigDecimal itemTaxExTotal = BigDecimal.ZERO;
        BigDecimal itemTax = BigDecimal.ZERO;
        BigDecimal itemLineExtensionAmount = BigDecimal.ZERO;
        BigDecimal itemCharge = BigDecimal.ZERO;

        for (InvoiceLineState state : lines) {
            UBLInvoiceLine line = state.getLine();
            BigDecimal amount = line.getLineExtensionAmount();
            FinancialAct item = mapInvoiceLine(state, issueDatetime, rates, context);
            service.deriveValues(item);
            ActRelationship relationship = (ActRelationship) delivery.addTarget("items", item);
            item.addActRelationship(relationship);
            result.addDeliveryItem(item);

            itemLineExtensionAmount = itemLineExtensionAmount.add(amount);
            itemTax = itemTax.add(item.getTaxAmount());
            itemTaxExTotal = itemTaxExTotal.add(item.getTotal().subtract(item.getTaxAmount()));
        }
        for (FinancialAct relatedOrder : context.getOrders()) {
            delivery.addTarget("orders", relatedOrder, "deliveries");
        }

        // map any charges
        for (UBLAllowanceCharge allowanceCharge : wrapper.getAllowanceCharges()) {
            // only support charges at present
            FinancialAct item = mapCharge(allowanceCharge, issueDatetime, invoiceId, rates);
            service.deriveValues(item);
            ActRelationship relationship = (ActRelationship) delivery.addTarget("items", item);
            item.addActRelationship(relationship);
            result.addDeliveryItem(item);

            itemTax = itemTax.add(item.getTaxAmount());
            itemCharge = itemCharge.add(item.getTotal()).subtract(item.getTaxAmount());
        }
        if (chargeTotal.compareTo(itemCharge) != 0) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceInvalidChargeTotal(invoiceId, chargeTotal,
                                                                                         itemCharge));
        }

        itemTaxExTotal = itemTaxExTotal.add(itemCharge);
        itemTaxIncTotal = itemTaxExTotal.add(itemTax);

        if (taxTotal.compareTo(itemTax) != 0) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceInvalidTax(invoiceId, taxTotal, itemTax));
        }
        if (itemLineExtensionAmount.compareTo(invoiceLineExtensionAmount) != 0) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceInvalidLineExtensionAmount(
                    invoiceId, invoiceLineExtensionAmount, itemLineExtensionAmount));
        }
        if (itemTaxExTotal.compareTo(taxExclusiveAmount) != 0) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceInvalidTaxExclusiveAmount(
                    invoiceId, taxExclusiveAmount, itemTaxExTotal));
        }
        if (payableAmount.compareTo(itemTaxIncTotal) != 0) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceInvalidPayableAmount(invoiceId, payableAmount,
                                                                                           itemTaxIncTotal));
        }
        return result;
    }

    /**
     * Determines if the invoice is a duplicate.
     *
     * @param supplier  the supplier
     * @param invoiceId the invoice identifier
     * @throws ESCIAdapterException if the invoice is a duplicate
     */
    protected void checkDuplicateInvoice(Party supplier, String invoiceId) {
        ArchetypeQuery query = new ArchetypeQuery(SupplierArchetypes.DELIVERY);
        query.getArchetypeConstraint().setAlias("delivery");
        query.add(Constraints.join("supplier").add(Constraints.eq("entity", supplier)));
        query.add(Constraints.join("supplierInvoiceId").add(Constraints.eq("identity", invoiceId)));
        query.setMaxResults(1);
        IArchetypeService service = getService();
        IMObjectQueryIterator<FinancialAct> iter = new IMObjectQueryIterator<>(service, query);
        if (iter.hasNext()) {
            FinancialAct delivery = iter.next();
            throw new ESCIAdapterException(ESCIAdapterMessages.duplicateInvoice(invoiceId, delivery.getId()));
        }
    }

    /**
     * Verifies that the TaxTotal/TaxAmount corresponds to that of the TaxTotal/TaxSubtotal.
     * <p/>
     * This implementation only supports a single TaxTotal/Subtotal and expects the TaxTotal/Subtotal/TaxableAmount
     * to match that supplied.
     *
     * @param line  the invoice line
     * @param rates the tax rates
     * @throws ESCIAdapterException if the tax is incorrectly specified
     */
    protected void checkTax(UBLInvoiceLine line, TaxRates rates) {
        BigDecimal expectedTaxAmount = line.getTaxAmount();
        UBLTaxSubtotal subtotal = line.getTaxSubtotal();
        checkTax(subtotal, expectedTaxAmount, line.getLineExtensionAmount(), rates, line);
    }

    /**
     * Verifies that a tax subtotal matches that expected, and has a valid rate.
     *
     * @param subtotal          the subtotal
     * @param expectedTaxAmount the expected tax amount
     * @param amount            the line extension amount
     * @param rates             the tax rates
     * @param parent            the parent element
     * @throws ESCIAdapterException if the subtotal is invalid
     */
    protected void checkTax(UBLTaxSubtotal subtotal, BigDecimal expectedTaxAmount, BigDecimal amount, TaxRates rates,
                            UBLType parent) {
        if (subtotal != null) {
            BigDecimal taxAmount = subtotal.getTaxAmount();
            if (expectedTaxAmount.compareTo(taxAmount) != 0) {
                ErrorContext context = new ErrorContext(subtotal, "TaxAmount");
                throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                        context.getPath(), context.getType(), context.getID(), expectedTaxAmount.toString(),
                        taxAmount.toString()));
            }
            UBLTaxCategory category = subtotal.getTaxCategory();
            BigDecimal rate = checkTaxCategory(category, rates);
            BigDecimal divisor = BigDecimal.valueOf(100);
            if (taxAmount.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal calc = MathRules.divide(amount.multiply(rate), divisor, 2);
                if (calc.compareTo(expectedTaxAmount) != 0) {
                    ErrorContext context = new ErrorContext(subtotal, "TaxTotal/TaxAmount");
                    throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                            context.getPath(), context.getType(), context.getID(), calc.toString(),
                            expectedTaxAmount.toString()));
                }
            }
        } else if (expectedTaxAmount.compareTo(BigDecimal.ZERO) != 0) {
            ErrorContext context = new ErrorContext(parent, "TaxTotal");
            throw new ESCIAdapterException(ESCIAdapterMessages.ublElementRequired(context.getPath(), context.getType(),
                                                                                  context.getID()));
        }
    }

    /**
     * Verifies that a tax category matches that expected.
     *
     * @param category the tax category
     * @param rates    the tax rates
     * @return the tax rate used
     * @throws ESCIAdapterException if the category is invalid
     */
    protected BigDecimal checkTaxCategory(UBLTaxCategory category, TaxRates rates) {
        String taxCategory = category.getMandatoryID();
        BigDecimal rate = category.getTaxRate();
        String taxScheme = category.getTaxTypeCode();
        BigDecimal expectedRate = rates.getTaxRate(taxScheme, taxCategory);
        if (expectedRate == null) {
            ErrorContext context = new ErrorContext(category);
            throw new ESCIAdapterException(ESCIAdapterMessages.invalidTaxSchemeAndCategory(
                    context.getPath(), context.getType(), context.getID(), taxScheme, taxCategory));
        }
        if (expectedRate.compareTo(rate) != 0) {
            ErrorContext context = new ErrorContext(category, "Percent");
            throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                    context.getPath(), context.getType(), context.getID(), expectedRate.toString(), rate.toString()));
        }
        return rate;
    }

    /**
     * Returns the associated order item for an invoice line, if an order line reference is specified.
     * <p/>
     * If there is:
     * <ul>
     * <li>a document-level order reference, then all order lines must reference this order
     * <li>no document level order reference, order references must be fully qualified i.e must specify both
     * the order line and order
     * <li>a document-level order reference, but no order line reference, the first order item matching the invoice item
     * will be returned
     * <ul>
     *
     * @param line the invoice line
     * @return the corresponding order item, or {@code null} if none is present
     */
    protected FinancialAct mapOrderItem(InvoiceLineState line) {
        FinancialAct item = line.getOrderItem();
        Product invoiced = line.getProduct();
        if (item != null && invoiced != null) {
            // verify that the invoice item has the same product as ordered. If not, don't want to refer
            // to the order item in the delivery item.
            IMObjectBean itemBean = getService().getBean(item);
            Reference orderedProduct = itemBean.getTargetRef("product");
            if (!Objects.equals(orderedProduct, invoiced.getObjectReference())) {
                log.warn("Invoiced product doesn't refer to that ordered"); // TODO - log context
                item = null;
            }
        }
        return item;
    }

    /**
     * Maps an {@code InvoiceLineType} to an <em>act.supplierDeliveryItem</em>.
     *
     * @param lineState the invoice line state
     * @param startTime the invoice start time
     * @param rates     the tax rates
     * @param context   the mapping context
     * @return a new <em>act.supplierDeliveryItem</em> corresponding to the invoice line
     * @throws ESCIAdapterException      if the order wasn't submitted by the supplier
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected FinancialAct mapInvoiceLine(InvoiceLineState lineState, Date startTime, TaxRates rates,
                                          MappingContext context) {
        IArchetypeService service = getService();
        IMObjectBean deliveryItem = service.getBean(service.create(SupplierArchetypes.DELIVERY_ITEM));

        UBLInvoiceLine line = lineState.getLine();
        BigDecimal quantity = line.getInvoicedQuantity();
        String invoicedUnitCode = line.getInvoicedQuantityUnitCode();
        checkPackQuantity(line, invoicedUnitCode);
        checkBaseQuantity(line, invoicedUnitCode);

        Party supplier = context.getSupplier();
        Product product = lineState.getProduct();
        BigDecimal lineExtensionAmount = line.getLineExtensionAmount();
        String reorderCode = line.getSellersItemID();
        String reorderDescription = line.getItemName();

        BigDecimal tax = line.getTaxAmount();

        FinancialAct orderItem = mapOrderItem(lineState);

        Package pkg = packageHelper.getPackage(orderItem, product, supplier);
        ProductSupplier ps = (pkg != null) ? pkg.getProductSupplier() : null;

        BigDecimal unitPrice = line.getPriceAmount();
        String packageUnits = getPackageUnits(invoicedUnitCode, pkg);
        int packageSize = getPackageSize(line, pkg);
        BigDecimal listPrice = getListPrice(line, product, ps, reorderCode, packageSize, packageUnits, context);

        BigDecimal calcLineExtensionAmount = unitPrice.multiply(quantity);
        if (calcLineExtensionAmount.compareTo(lineExtensionAmount) != 0) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceLineInvalidLineExtensionAmount(
                    line.getID(), lineExtensionAmount, calcLineExtensionAmount));
        }
        checkTax(line, rates);

        deliveryItem.setValue("supplierInvoiceLineId", line.getMandatoryID());
        deliveryItem.setValue(START_TIME, startTime);
        deliveryItem.setValue("quantity", quantity);
        deliveryItem.setValue("unitPrice", unitPrice);
        deliveryItem.setValue("listPrice", listPrice);
        deliveryItem.setValue("tax", tax);
        deliveryItem.setValue("packageUnits", packageUnits);
        deliveryItem.setValue("packageSize", packageSize);
        if (product != null) {
            deliveryItem.setTarget("product", product);
        }
        deliveryItem.setValue("reorderCode", reorderCode);
        deliveryItem.setValue("reorderDescription", reorderDescription);

        if (orderItem != null) {
            deliveryItem.addTarget("order", orderItem, "deliveries");
        }

        deliveryItem.deriveValues();
        return deliveryItem.getObject(FinancialAct.class);
    }

    /**
     * Maps a charge to a delivery item.
     *
     * @param charge    the allowance/charge
     * @param startTime the invoice start time
     * @param invoiceId the invoice identifier
     * @param rates     the tax rates
     * @return a new delivery item
     * @throws ESCIAdapterException if the allowance/charge cannot be mapped
     */
    protected FinancialAct mapCharge(UBLAllowanceCharge charge, Date startTime, String invoiceId, TaxRates rates) {
        if (!charge.isCharge()) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invoiceAllowanceNotSupported(invoiceId));
        }
        BigDecimal unitPrice = charge.getAmount();
        IArchetypeService service = getService();
        IMObjectBean deliveryItem = service.getBean(service.create(SupplierArchetypes.DELIVERY_ITEM));
        BigDecimal tax = charge.getTaxAmount();
        UBLTaxCategory taxCategory = charge.getTaxCategory();
        if (taxCategory != null) {
            BigDecimal rate = checkTaxCategory(taxCategory, rates);
            BigDecimal divisor = BigDecimal.valueOf(100);
            if (tax.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal expectedTax = MathRules.divide(unitPrice.multiply(rate), divisor, 2);
                if (expectedTax.compareTo(tax) != 0) {
                    ErrorContext context = new ErrorContext(charge, "TaxTotal/TaxAmount");
                    throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                            context.getPath(), context.getType(), context.getID(), expectedTax.toString(),
                            tax.toString()));
                }
            }
        }
        deliveryItem.setValue(START_TIME, startTime);
        deliveryItem.setValue("quantity", BigDecimal.ONE);
        deliveryItem.setValue("packageUnits", null); // override default
        deliveryItem.setValue("unitPrice", unitPrice);
        deliveryItem.setValue("tax", tax);
        deliveryItem.setValue("reorderDescription", charge.getAllowanceChargeReason());  // TODO - not ideal

        deliveryItem.deriveValues();
        return deliveryItem.getObject(FinancialAct.class);
    }

    /**
     * Verifies that the UBL version matches that expected.
     *
     * @param invoice the invoice
     */
    protected void checkUBLVersion(UBLInvoice invoice) {
        if (!UBL_VERSION.equals(invoice.getUBLVersionID())) {
            throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                    "UBLVersionID", "Invoice", invoice.getID(), UBL_VERSION, invoice.getUBLVersionID()));
        }
    }

    /**
     * Returns the list price.
     * <p/>
     * This is available as the wholesale price in the invoice line.
     * <p/>
     * If it is not present:
     * <ol>
     * <li>it will be defaulted to that from {@code ps}, if available
     * <li>if ps is not present or the value is 0, it will be defaulted to the unit price from the invoice line.
     * </ol>
     *
     * @param line         the invoice line
     * @param product      the product. May be {@code null}
     * @param ps           the product/supplier relationship. May be {@code null}
     * @param reorderCode  the reorder code. May be {@code null}
     * @param packageSize  the package size
     * @param packageUnits the package units
     * @param context      the mapping context
     * @return the list price
     */
    private BigDecimal getListPrice(UBLInvoiceLine line, Product product, ProductSupplier ps,
                                    String reorderCode, int packageSize, String packageUnits, MappingContext context) {
        BigDecimal listPrice = line.getWholesalePrice();
        if (listPrice != null && listPrice.compareTo(BigDecimal.ZERO) == 0) {
            log.warn("Received 0.0 list price from supplier.");
            listPrice = null;
        }
        if (listPrice == null) {
            if (ps == null && product != null) {
                ps = productRules.getProductSupplier(product, context.getSupplier(), reorderCode, packageSize,
                                                     packageUnits);
            }
            if (ps != null) {
                listPrice = ps.getListPrice();
            }
            if (listPrice == null || listPrice.compareTo(BigDecimal.ZERO) == 0) {
                log.warn("Product/supplier list price is 0.0");
            }
        }
        if (listPrice == null || listPrice.compareTo(BigDecimal.ZERO) == 0) {
            // no list price in the invoice line, nor from a product/supplier. Default to the unit price
            listPrice = line.getPriceAmount();
        }
        return listPrice;
    }

    /**
     * Returns the package units.
     *
     * @param invoicedUnitCode the invoiced quantity unit code
     * @param pkg              the package information. May be {@code null}
     * @return the package units, or {@code null} if they are not known
     */
    private String getPackageUnits(String invoicedUnitCode, Package pkg) {
        String result = null;
        String expected = (pkg != null) ? pkg.getPackageUnits() : null;

        List<String> matches = packageHelper.getPackageUnits(invoicedUnitCode);
        if (expected != null) {
            if (!matches.contains(expected)) {
                log.warn("Invoice package units ({}) don't match that expected: {}",
                         StringUtils.join(matches.iterator(), ", "), expected); // TODO - log context
            }
            result = expected;
        } else if (matches.size() == 1) {
            result = matches.get(0);
        } else if (matches.size() > 1) {
            log.warn("Cannot determine package units. {} package units match unit code: {}",
                     matches.size(), invoicedUnitCode); // TODO - log context
        }
        return result;
    }

    /**
     * Returns the package size.
     *
     * @param line the invoice line
     * @param pkg  the expected package, or {@code null} if it is not known
     * @return the package size, or {@code 0} if it is not known
     * @throws ESCIAdapterException if the package size is incorrectly specified
     */
    private int getPackageSize(UBLInvoiceLine line, Package pkg) {
        int result;
        BigDecimal packageSize = line.getPackSizeNumeric();
        int expectedSize = (pkg != null) ? pkg.getPackageSize() : 0;
        int invoiceSize;
        try {
            invoiceSize = packageSize.intValueExact();
        } catch (ArithmeticException exception) {
            ErrorContext context = new ErrorContext(line, "PackSizeNumeric");
            String intValue = Integer.toString(packageSize.intValue());
            throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                    context.getPath(), context.getType(), context.getID(), intValue, packageSize.toString()));
        }
        if (expectedSize != 0) {
            if (invoiceSize != 0 && invoiceSize != expectedSize) {
                log.warn("Different package size received for invoice. Expected package size={}, " +
                         "invoiced package size={}", invoiceSize, expectedSize); // TODO - log context
            }
            result = expectedSize;
        } else {
            result = invoiceSize;
        }
        return result;
    }

    /**
     * Verifies that the invoice line item's <em>PackQuantity</em> is specified correctly, if present.
     *
     * @param line     the invoice line
     * @param unitCode the expected unit code
     */
    private void checkPackQuantity(UBLInvoiceLine line, String unitCode) {
        BigDecimal quantity = line.getPackQuantity();
        if (quantity != null) {
            if (quantity.compareTo(BigDecimal.ONE) != 0) {
                ErrorContext context = new ErrorContext(line, "PackQuantity");
                throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                        context.getPath(), context.getType(), context.getID(), "1", quantity.toString()));
            }
            String packageUnits = line.getPackQuantityUnitCode();
            if (packageUnits != null && !Objects.equals(unitCode, packageUnits)) {
                ErrorContext context = new ErrorContext(line, "PackQuantity@unitCode");
                throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                        context.getPath(), context.getType(), context.getID(), unitCode, packageUnits));
            }
        }
    }

    /**
     * Verifies that the invoice line's <em>BaseQuantity</em> is specified correctly, if present.
     *
     * @param line     the invoice line
     * @param unitCode the expected unit code
     */
    private void checkBaseQuantity(UBLInvoiceLine line, String unitCode) {
        BigDecimal quantity = line.getBaseQuantity();
        if (quantity != null) {
            if (quantity.compareTo(BigDecimal.ONE) != 0) {
                ErrorContext context = new ErrorContext(line, "BaseQuantity");
                throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                        context.getPath(), context.getType(), context.getID(), "1", quantity.toString()));
            }
            String baseQuantityUnitCode = line.getBaseQuantityUnitCode();
            if (!StringUtils.equals(unitCode, baseQuantityUnitCode)) {
                ErrorContext context = new ErrorContext(line, "BaseQuantity@unitCode");
                throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue(
                        context.getPath(), context.getType(), context.getID(), unitCode, baseQuantityUnitCode));
            }
        }
    }

}
