/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.map;

/**
 * Helper to determine the context of an error in an UBL document.
 *
 * @author Tim Anderson
 */
public class ErrorContext {

    /**
     * The path to the UBL element.
     */
    private final String path;


    /**
     * The type of the root element.
     */
    private final String type;

    /**
     * The identifier of the root element. May be {@code null}
     */
    private final String id;

    /**
     * Constructs an {@link ErrorContext}.
     *
     * @param type the UBL type
     */
    public ErrorContext(UBLType type) {
        this(type, null);
    }

    /**
     * Constructs an {@link ErrorContext}.
     *
     * @param type the UBL type
     * @param path the path to the element, relative to {@code type}
     */
    public ErrorContext(UBLType type, String path) {
        UBLType root = type;
        StringBuilder fullPath = new StringBuilder((path != null) ? root.getPath() + "/" + path : root.getPath());
        while (!root.useForErrorReporting() && root.getParent() != null) {
            root = root.getParent();
            fullPath.insert(0, root.getPath() + "/");
        }
        this.path = fullPath.toString();
        this.type = root.getType();
        id = root.getID();
    }

    /**
     * Returns the path to the element.
     *
     * @return the path to the element
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns the type of the parent element.
     *
     * @return the type of the parent element
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the ID of the parent element
     *
     * @return the ID of the parent element. May be {@code null}
     */
    public String getID() {
        return id;
    }

}
