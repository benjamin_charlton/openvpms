/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.invoice;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.esci.adapter.i18n.ESCIAdapterMessages;
import org.openvpms.esci.adapter.map.UBLDocument;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.ubl.common.aggregate.OrderReferenceType;

import java.util.List;
import java.util.Objects;

/**
 * Abstract implementation of the {@link OrderResolver} interface.
 *
 * @author Tim Anderson
 */
public class AbstractOrderResolver implements OrderResolver {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link AbstractOrderResolver}.
     *
     * @param service the archetype service
     */
    public AbstractOrderResolver(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns an order associated with an invoice.
     *
     * @param invoice       the invoice
     * @param supplier      the expected supplier of the order
     * @param stockLocation the expected stock location of the order
     * @return the corresponding order. May be {@code null}
     * @throws ESCIAdapterException      if the order reference is malformed, or the expected supplier and stock
     *                                   location don't match the actual order
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public FinancialAct getOrder(UBLInvoice invoice, Party supplier, Party stockLocation) {
        FinancialAct order = getOrder(invoice);
        if (order != null && !checkOrder(order, supplier, stockLocation, invoice)) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invalidOrder(invoice.getType(), invoice.getID(),
                                                                            Long.toString(order.getId())));
        }
        return order;
    }

    /**
     * Verifies that an order has a relationship to the expected supplier and stock location.
     *
     * @param order         the order
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param document      the document where the order is referenced
     * @return {@code true} if the order is for the correct supplier and stock location, otherwise {@code false}
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public boolean checkOrder(FinancialAct order, Party supplier, Party stockLocation, UBLDocument document) {
        IMObjectBean bean = service.getBean(order);
        Reference supplierRef = bean.getTargetRef("supplier");
        Reference locationRef = bean.getTargetRef("stockLocation");
        return Objects.equals(supplierRef, supplier.getObjectReference())
               && Objects.equals(locationRef, stockLocation.getObjectReference());
    }

    /**
     * Returns the order item associated with an invoice line.
     * <p/>
     * If there is no order item specified, but there is a document-level order, the result will contain it.
     * <p/>
     * This can be used when matching unreferenced invoice lines.
     *
     * @param line    the invoice line
     * @param context the mapping context
     * @return the corresponding order item, or {@code null} if the line isn't associated with an order item
     * @throws ESCIAdapterException      if the order item reference is malformed or references an invalid order
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public OrderItem getOrderItem(UBLInvoiceLine line, MappingContext context) {
        FinancialAct docOrder = context.getDocumentOrder();
        FinancialAct lineOrder;
        FinancialAct item = null;
        Reference orderRef = line.getOrderReference();
        Reference orderItemRef = line.getOrderItemReference();

        if (orderItemRef != null) {
            // invoice line is referring to an order line
            if (orderRef != null) {
                // referencing an order. Make sure it can be retrieved
                lineOrder = getReferencedOrder(orderRef, line, context);
            } else {
                // no order reference specified, so must be working with the document level order
                if (docOrder == null) {
                    // no order was specified in the invoice line, and no document level order specified
                    // Expected 0 cardinality
                    throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidCardinality(
                            "OrderLineReference", "InvoiceLine", line.getID(), "0", 1));
                }
                lineOrder = docOrder;
            }
            // NOTE: an id of -1 is the special case where a supplier doesn't provide order line identifiers, but
            // does provide the order identifier. This is used in the (fugly) multiple order per invoice scenario.
            if (orderItemRef.getId() != -1) {
                item = context.getOrderItem(lineOrder, orderItemRef);
                if (item == null) {
                    // no relationship between the order and the order item
                    throw new ESCIAdapterException(ESCIAdapterMessages.invoiceInvalidOrderItem(
                            line.getID(), Long.toString(orderItemRef.getId())));
                }
            }
        } else if (orderRef != null) {
            // referencing an order but no order item specified.
            throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidCardinality(
                    "OrderLineReference/LineID", "InvoiceLine", line.getID(), "0", 1));
        } else {
            lineOrder = docOrder;
        }
        return (lineOrder != null) ? new OrderItem(lineOrder, item) : null;
    }

    /**
     * Returns an order associated with an invoice.
     *
     * @param invoice the invoice
     * @return the corresponding order. May be {@code null}
     * @throws ESCIAdapterException      if the order reference is malformed
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected FinancialAct getOrder(UBLInvoice invoice) {
        FinancialAct order = null;
        OrderReferenceType orderReference = invoice.getOrderReference();
        if (orderReference != null) {
            order = (FinancialAct) invoice.getObject(SupplierArchetypes.ORDER, orderReference.getID(),
                                                     "OrderReference");
            if (order == null) {
                throw new ESCIAdapterException(ESCIAdapterMessages.invalidOrder("Invoice", invoice.getID(),
                                                                                orderReference.getID().getValue()));
            }
        }
        return order;
    }

    /**
     * Retrieves an order referenced by an invoice line.
     *
     * @param orderRef the order reference
     * @param line     the invoice line
     * @param context  the mapping context
     * @return the corresponding order
     */
    private FinancialAct getReferencedOrder(Reference orderRef, UBLInvoiceLine line, MappingContext context) {
        FinancialAct childOrder = context.getOrder(orderRef);
        if (childOrder == null) {
            // get the order and ensure it was submitted to the supplier from the same stock location
            childOrder = line.getOrder();
            checkOrder(childOrder, context);
            context.addOrder(childOrder);
            context.addOrderItems(childOrder);
        }
        FinancialAct docOrder = context.getDocumentOrder();
        if (docOrder != null && !Objects.equals(docOrder.getObjectReference(), childOrder.getObjectReference())) {
            // top-level order specified, but the child order is different.
            UBLInvoice invoice = context.getInvoice();
            throw new ESCIAdapterException(ESCIAdapterMessages.invalidOrder(invoice.getType(), invoice.getID(),
                                                                            Long.toString(childOrder.getId())));
        }
        return childOrder;
    }

    /**
     * Verifies that an order has a relationship to the expected supplier and stock location and is not
     * already associated with the invoice.
     *
     * @param order   the order
     * @param context the mapping context
     * @throws ESCIAdapterException      if the order wasn't submitted by the supplier or the invoice is a duplicate
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void checkOrder(FinancialAct order, MappingContext context) {
        checkOrder(order, context.getSupplier(), context.getStockLocation(), context.getInvoice());
        String invoiceId = context.getInvoice().getMandatoryID();
        IMObjectBean orderBean = service.getBean(order);
        List<FinancialAct> deliveries = orderBean.getSources("deliveries", FinancialAct.class);
        for (FinancialAct delivery : deliveries) {
            IMObjectBean deliveryBean = service.getBean(delivery);
            ActIdentity supplierInvoiceId = deliveryBean.getObject("supplierInvoiceId", ActIdentity.class);
            if (supplierInvoiceId != null && Objects.equals(invoiceId, supplierInvoiceId.getIdentity())) {
                throw new ESCIAdapterException(ESCIAdapterMessages.duplicateInvoiceForOrder(invoiceId, order.getId()));
            }
        }
    }
}