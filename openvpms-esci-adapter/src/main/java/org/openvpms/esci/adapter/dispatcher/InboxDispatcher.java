/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.component.model.party.Party;
import org.openvpms.esci.adapter.i18n.ESCIAdapterMessages;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;

import java.util.Iterator;
import java.util.List;


/**
 * Reads documents from an {@link Inbox} and dispatches them to {@link DocumentProcessor}s.
 *
 * @author Tim Anderson
 */
public class InboxDispatcher {

    /**
     * The inbox service.
     */
    private final Inbox inbox;

    /**
     * The document processors.
     */
    private final List<DocumentProcessor> processors;

    /**
     * The document references.
     */
    private Iterator<DocumentReferenceType> references;


    /**
     * Constructs an {@link InboxDispatcher}.
     *
     * @param inbox      the inbox to read documents from
     * @param processors the document processors
     */
    public InboxDispatcher(Inbox inbox, List<DocumentProcessor> processors) {
        this.inbox = inbox;
        this.processors = processors;
    }

    /**
     * Determines if there are documents to dispatch.
     *
     * @return {@code true} if there are more documents to dispatch
     */
    public boolean hasNext() {
        if (references == null || !references.hasNext()) {
            references = inbox.getDocuments().iterator();
        }
        return references.hasNext();
    }

    /**
     * Dispatches the next document, if any.
     * <p/>
     * This fetches the next available document from the {@code InboxService}, and dispatches it to its corresponding
     * {@link DocumentProcessor}.
     * <p/>
     * If the document is successfully processed, it will be acknowledged.
     *
     * @return {@code true} if a documents was processed, otherwise {@code false}
     * @throws ESCIAdapterException if the document cannot be processed, or if there is an error communicating with the
     *                              InboxService.
     */
    public boolean dispatch() {
        boolean result = false;
        if (hasNext()) {
            DocumentReferenceType reference = references.next();
            process(reference, null);
            result = true;
        }
        return result;
    }

    /**
     * Processes a document.
     * <p/>
     * If the document is successfully processed, it will be acknowledged.
     *
     * @param reference the document reference
     * @param config    configures the document processing behaviour. May be {@code null}
     * @throws ESCIAdapterException if the document cannot be processed, or if there is an error communicating with the
     *                              InboxService.
     */
    public void process(DocumentReferenceType reference, ProcessingConfig config) {
        Document doc = inbox.getDocument(reference);
        if (doc != null) {
            InboxDocument wrapper = new InboxDocument(reference, doc.getAny());
            DocumentProcessor processor = getProcessor(inbox.getSupplier(), wrapper);
            processor.process(wrapper, inbox.getSupplier(), inbox.getStockLocation(), inbox.getAccountId(), config);
            try {
                inbox.acknowledge(reference);
            } catch (DocumentNotFoundException exception) {
                throw new ESCIAdapterException(ESCIAdapterMessages.failedToAcknowledgeDocument(inbox.getSupplier(),
                                                                                               reference));
            }
        } else {
            throw new ESCIAdapterException(ESCIAdapterMessages.documentNotFound(inbox.getSupplier(), reference));
        }
    }

    /**
     * Remove a document without processing it.
     *
     * @param reference the document reference
     */
    public void delete(DocumentReferenceType reference) {
        try {
            inbox.acknowledge(reference);
        } catch (DocumentNotFoundException exception) {
            throw new ESCIAdapterException(ESCIAdapterMessages.documentNotFound(inbox.getSupplier(), reference));
        }
    }

    /**
     * Returns a processor for the supplied document,
     *
     * @param supplier the sender of the document
     * @param document the document
     * @return a processor for the document
     * @throws ESCIAdapterException if no processor can be found
     */
    protected DocumentProcessor getProcessor(Party supplier, InboxDocument document) {
        for (DocumentProcessor documentProcessor : processors) {
            if (documentProcessor.canHandle(document)) {
                return documentProcessor;
            }
        }
        throw new ESCIAdapterException(ESCIAdapterMessages.unsupportedDocument(supplier,
                                                                               document.getDocumentReference(),
                                                                               document.getContent()));
    }

}
