/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.component.model.party.Party;
import org.openvpms.esci.service.InboxService;
import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;

import java.util.List;


/**
 * Associates a supplier with an {@code InboxService}.
 *
 * @author Tim Anderson
 */
public class Inbox implements InboxService {

    /**
     * The ESCI configuration.
     */
    private final ESCIConfig config;

    /**
     * The inbox service.
     */
    private final InboxService service;


    /**
     * Constructs an {@code Inbox}.
     *
     * @param config the ESCI configuration
     * @param service  the inbox service
     */
    public Inbox(ESCIConfig config, InboxService service) {
        this.config = config;
        this.service = service;
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier
     */
    public Party getSupplier() {
        return config.getSupplier();
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location
     */
    public Party getStockLocation() {
        return config.getStockLocation();
    }

    /**
     * Returns the supplier account identifier.
     *
     * @return the supplier account identifier. May be {@code null}
     */
    public String getAccountId() {
        return config.getAccountId();
    }

    /**
     * Returns a list of document references, in the order that they were received.
     * <p/>
     * Each reference uniquely identifies a single document.
     *
     * @return a list of document references
     */
    public List<DocumentReferenceType> getDocuments() {
        return service.getDocuments();
    }

    /**
     * Returns the document with the specified reference.
     *
     * @param reference the document reference
     * @return the corresponding document, or {@code null} if the document is not found
     */
    public Document getDocument(DocumentReferenceType reference) {
        return service.getDocument(reference);
    }

    /**
     * Acknowledges a document.
     * <p/>
     * Once acknowledged, the document will no longer be returned by {@link #getDocuments} nor {@link #getDocument}.
     *
     * @param reference the document reference
     * @throws DocumentNotFoundException if the reference doesn't refer to a valid document
     */
    public void acknowledge(DocumentReferenceType reference) throws DocumentNotFoundException {
        service.acknowledge(reference);
    }

}
