/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.invoice;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.esci.adapter.map.AbstractUBLMapper;
import org.openvpms.esci.adapter.util.ESCIAdapterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * Helper to match invoice lines to their corresponding order items.
 * Where an invoice line doesn't explicitly refer to an order line, attempts will be made to match it to:
 * <ol>
 * <li>an unreferenced order line with the same product and quantity, or if none is found
 * <li>an unreferenced order line with the same product
 * </ol>
 *
 * @author Tim Anderson
 */
class InvoiceOrderMatcher extends AbstractUBLMapper {

    /**
     * The order resolver.
     */
    private final OrderResolver orderResolver;

    /**
     * Constructs an {@link InvoiceOrderMatcher}.
     *
     * @param service       the archetype service
     * @param orderResolver the order resolver
     */
    InvoiceOrderMatcher(IArchetypeService service, OrderResolver orderResolver) {
        super(service);
        this.orderResolver = orderResolver;
    }

    /**
     * Matches invoice lines to their corresponding order lines.
     *
     * @param invoice       the invoice
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @return the mapping context containing the invoice line mapping and associated order items
     */
    public MappingContext match(UBLInvoice invoice, Party supplier, Party stockLocation) {
        FinancialAct order = orderResolver.getOrder(invoice, supplier, stockLocation);
        MappingContext context = new MappingContext(invoice, supplier, stockLocation, order, getService());
        if (order != null) {
            context.addOrderItems(order);
        }

        List<InvoiceLineState> lines = new ArrayList<>();
        List<InvoiceLineState> unlinked = new ArrayList<>();

        // first pass - create state for each invoice line, and determine the list of unlinked lines
        for (UBLInvoiceLine line : invoice.getInvoiceLines()) {
            InvoiceLineState state = getState(line, context);
            lines.add(state);
            if (state.isUnlinked()) {
                unlinked.add(state);
            }
        }

        if (!unlinked.isEmpty()) {
            // second pass - link invoice lines to unlinked order items, where an exact match exists
            UnlinkedTracker tracker = new UnlinkedTracker(context);
            for (InvoiceLineState line : unlinked) {
                exactMatch(line, tracker);
            }

            // third pass - link invoice lines to unlinked order items, where a partial match exists
            Set<InvoiceLineState> partial = new HashSet<>(tracker.getPartialMatches());
            for (InvoiceLineState line : partial) {
                partialMatch(line, tracker);
            }
        }
        context.setInvoiceLines(lines);
        return context;
    }

    /**
     * Returns the invoice line state for an invoice line.
     * <p/>
     * This ensures that any referenced order and product is valid.
     * <p/>
     * If there is:
     * <ul>
     * <li>a document-level order reference, then all order lines must reference this order
     * <li>no document level order reference, order references must be fully qualified i.e must specify both
     * the order line and order
     * <ul>
     * If the OrderLineReference/LineID is {@code -1}, no order item will be populated. This must be handled by
     * matching on product and quantity.
     *
     * @param line    the invoice line
     * @param context the mapping context
     * @return the invoice line state
     * @throws ESCIAdapterException if the order reference or product was incorrectly specified
     */
    protected InvoiceLineState getState(UBLInvoiceLine line, MappingContext context) {
        OrderItem orderItem = orderResolver.getOrderItem(line, context);
        Product product = line.getProduct(context.getSupplier());
        FinancialAct order = orderItem != null ? orderItem.getOrder() : null;
        FinancialAct item = orderItem != null ? orderItem.getItem() : null;
        return new InvoiceLineState(line, product, order, item);
    }

    /**
     * Attempts to match an invoice line to an unreferenced order item.
     * <p/>
     * This performs an exact match on product and quantity. If there is only a match on product, the line is
     * registered as partial match with the item.
     *
     * @param line     the invoice line
     * @param unlinked the set of unreferenced order items
     */
    private void exactMatch(InvoiceLineState line, UnlinkedTracker unlinked) {
        FinancialAct result = null;
        Reference invoiceRef = line.getProduct().getObjectReference();
        for (FinancialAct item : unlinked.getItems(line.getOrder())) {
            IArchetypeService service = getService();
            IMObjectBean bean = service.getBean(item);
            Reference orderRef = bean.getTargetRef("product");
            if (Objects.equals(invoiceRef, orderRef)) {
                BigDecimal orderQuantity = item.getQuantity();
                if (line.getQuantity().compareTo(orderQuantity) == 0) {
                    result = item;
                    break;
                } else {
                    unlinked.addPartialMatch(line, item);
                }
            }
        }
        if (result != null) {
            line.setOrderItem(result);
            unlinked.remove(line);
        }
    }

    /**
     * Attempts to match an invoice line to an unreferenced order item.
     * <p/>
     * This performs an exact match on product and quantity. If there is only a match on product, the line is
     * registered as partial match with the item.
     *
     * @param line     the invoice line
     * @param unlinked the set of unreferenced order items
     */
    private void partialMatch(InvoiceLineState line, UnlinkedTracker unlinked) {
        List<FinancialAct> matches = unlinked.getPartialMatches(line);
        if (!matches.isEmpty()) {
            if (matches.size() > 1) {
                // sort items on ascending quantity.
                matches.sort(Comparator.comparing(FinancialAct::getQuantity));
            }
            line.setOrderItem(matches.get(0));
            unlinked.remove(line);
        }
    }


    /**
     * Tracks unlinked invoice lines and order items.
     */
    private static class UnlinkedTracker {

        /**
         * The unlinked order items, keyed on order.
         */
        private Map<FinancialAct, List<FinancialAct>> unlinked;

        /**
         * The order items that partially match an invoice lines.
         */
        private Map<InvoiceLineState, List<FinancialAct>> partial = new HashMap<>();


        /**
         * Constructs an {@link UnlinkedTracker}.
         *
         * @param context the mapping context
         */
        UnlinkedTracker(MappingContext context) {
            unlinked = new HashMap<>(context.getOrderItems());
        }

        /**
         * Returns the unlinked items for an order.
         *
         * @param order the order
         * @return the unlinked items for the order
         */
        public List<FinancialAct> getItems(FinancialAct order) {
            List<FinancialAct> result = unlinked.get(order);
            return (result != null) ? result : Collections.emptyList();
        }

        /**
         * Removes unlinked information for a line, and it associated order item.
         *
         * @param line the line
         */
        public void remove(InvoiceLineState line) {
            List<FinancialAct> items = unlinked.get(line.getOrder());
            FinancialAct item = line.getOrderItem();
            if (items != null) {
                items.remove(item);
            }
            partial.remove(line);

            for (InvoiceLineState l : getPartialMatches()) {
                List<FinancialAct> matches = getPartialMatches(l);
                if (!matches.isEmpty()) {
                    matches.remove(item);
                }
            }
        }

        /**
         * Add a partial match between a line and an order item.
         *
         * @param line the invoice line
         * @param item the order item
         */
        public void addPartialMatch(InvoiceLineState line, FinancialAct item) {
            List<FinancialAct> items = partial.computeIfAbsent(line, k -> new ArrayList<>());
            items.add(item);
        }

        /**
         * Returns the invoice lines where there is a partial match.
         *
         * @return the invoice lines
         */
        public Set<InvoiceLineState> getPartialMatches() {
            return partial.keySet();
        }

        /**
         * Returns order items that partially match an invoice line.
         *
         * @param line the invoice line
         * @return the order items
         */
        public List<FinancialAct> getPartialMatches(InvoiceLineState line) {
            List<FinancialAct> result = partial.get(line);
            return (result != null) ? result : Collections.emptyList();
        }

    }

}
