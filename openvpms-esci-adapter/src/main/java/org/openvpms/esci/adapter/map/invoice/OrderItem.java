/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.invoice;

import org.openvpms.component.model.act.FinancialAct;

/**
 * Associates an order and order item.
 *
 * @author Tim Anderson
 */
public class OrderItem {

    /**
     * The order.
     */
    private final FinancialAct order;

    /**
     * The order item. May be {@code null}
     */
    private final FinancialAct item;

    /**
     * Constructs an {@link OrderItem}.
     * <p/>
     * Use this where there is a document level order, but invoice line items don't explicitly reference an order item.
     *
     * @param order the order
     */
    public OrderItem(FinancialAct order) {
        this(order, null);
    }

    /**
     * Constructs an {@link OrderItem}.
     *
     * @param order the order
     * @param item  the order item. May be {@code null}
     */
    public OrderItem(FinancialAct order, FinancialAct item) {
        this.order = order;
        this.item = item;
    }

    /**
     * Returns the order.
     *
     * @return the order
     */
    public FinancialAct getOrder() {
        return order;
    }

    /**
     * Returns the order item.
     *
     * @return order item. May be {@code null}
     */
    public FinancialAct getItem() {
        return item;
    }

}
