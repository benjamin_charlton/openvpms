/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.List;

import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.SUPPLIER_ORGANISATION;
import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI;

/**
 * Helper for locating ESCI suppliers, and accessing their configuration.
 *
 * @author Tim Anderson
 */
class ESCISuppliers {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;


    /**
     * Constructs an {@link ESCISuppliers}.
     *
     * @param service the archetype service
     */
    public ESCISuppliers(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns all active ESCI configurations.
     *
     * @return all active ESCI configurations
     */
    public List<ESCIConfig> getESCIConfigs() {
        List<ESCIConfig> result = new ArrayList<>();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Tuple> query = builder.createQuery(Tuple.class)
                .distinct(true);
        Root<Party> from = query.from(Party.class, SUPPLIER_ORGANISATION).alias("supplier");
        Join<Party, IMObject> stockLocations = from.join("stockLocations", SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI)
                .alias("stockLocation");
        Join<IMObject, IMObject> location = stockLocations
                .join("target").alias("location");
        query.multiselect(from, stockLocations, location);
        query.where(builder.equal(from.get("active"), true),
                    builder.equal(location.get("active"), true));
        TypedQuery<Tuple> typedQuery = service.createQuery(query);
        for (Tuple tuple : typedQuery.getResultList()) {
            EntityRelationship relationship = tuple.get("stockLocation", EntityRelationship.class);
            if (relationship.isActive()) {
                Party supplier = tuple.get("supplier", Party.class);
                Party stockLocation = tuple.get("location", Party.class);
                ESCIConfig config = new ESCIConfig(relationship, supplier, stockLocation, service);
                result.add(config);
            }
        }
        return result;
    }

}
