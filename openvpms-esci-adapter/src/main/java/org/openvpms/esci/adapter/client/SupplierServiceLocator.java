/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.client;

import org.openvpms.esci.adapter.dispatcher.ESCIConfig;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.service.InboxService;
import org.openvpms.esci.service.OrderService;


/**
 * Returns references to supplier services.
 *
 * @author Tim Anderson
 */
public interface SupplierServiceLocator {

    /**
     * Returns a proxy for a supplier's {@link OrderService}.
     *
     * @param config the ESCI configuration
     * @return a proxy for the service provided by the supplier
     * @throws ESCIAdapterException if the associated <tt>serviceURL</tt> is invalid or the proxy cannot be created
     */
    OrderService getOrderService(ESCIConfig config);

    /**
     * Returns a proxy for a supplier's {@link OrderService}.
     *
     * @param serviceURL the WSDL document URL of the service
     * @param username   the username to connect to the service with
     * @param password   the password to connect  to the service with
     * @return a proxy for the service provided by the supplier
     * @throws ESCIAdapterException if the associated <tt>serviceURL</tt> is invalid or the proxy cannot be created
     */
    OrderService getOrderService(String serviceURL, String username, String password);

    /**
     * Returns a proxy for the supplier's {@link InboxService}.
     *
     * @param config the ESCI configuration
     * @return a proxy for the service provided by the supplier
     * @throws ESCIAdapterException if the associated <tt>serviceURL</tt> is invalid or the proxy cannot be created
     */
    InboxService getInboxService(ESCIConfig config);
}
