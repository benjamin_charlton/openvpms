/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import org.openvpms.domain.practice.Location;

/**
 * SMS message.
 *
 * @author Tim Anderson
 */
public interface Message {

    /**
     * Returns the OpenVPMS identifier for this message.
     *
     * @return the identifier
     */
    long getId();

    /**
     * Returns the message identifier, issued by the SMS provider.
     *
     * @return the message identifier, or {@code null} if none has been issued
     */
    String getProviderId();

    /**
     * Returns the phone number.
     * <p/>
     * For outbound messages, this is the number of the recipient.<br/>
     * For inbound messages, this is the number of the sender.
     *
     * @return the phone number
     */
    String getPhone();

    /**
     * Returns the message text.
     *
     * @return the message text
     */
    String getMessage();

    /**
     * Returns the practice location that this message relates to.
     * <p/>
     * For outbound messages, it is always set, and indicates the practice location that sent the message.<br/>
     * For inbound messages, it indicates the location that the message is for.
     *
     * @return the practice location. May be {@code null} for inbound messages
     */
    Location getLocation();
}
