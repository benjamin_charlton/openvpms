/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import org.openvpms.component.model.party.Party;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * An SMS message, sent from OpenVPMS.
 *
 * @author Tim Anderson
 */
public interface OutboundMessage extends Message {

    enum Status {
        PENDING,    // message is pending
        SENT,       // message was sent to the provider
        EXPIRED,    // message expired before it could be sent
        DELIVERED,  // message was delivered to the recipient
        ERROR,      // message couldn't be sent
        REVIEWED;   // the message had an error, and the error been reviewed. It cannot be resent

        /**
         * Determines if this status can transition to another.
         *
         * @param to the status to transition to
         * @return {@code true} if this can transition to the specified status
         */
        public boolean canTransition(Status to) {
            return to.compareTo(this) >= 0 || to == ERROR;
        }
    }

    /**
     * Returns the timestamp when the message was created.
     *
     * @return the timestamp
     */
    OffsetDateTime getCreated();

    /**
     * Returns the timestamp when the message was updated.
     *
     * @return the timestamp
     */
    OffsetDateTime getUpdated();

    /**
     * Returns the timestamp when the message expires.
     * <p/>
     * If it has not been sent to the provider by this time, it's status will change to {@link Status#EXPIRED}.
     * <p/>
     * If it has been sent to the provider, but not delivered by this time, the provider may choose not to deliver it.
     *
     * @return the timestamp, or {@code null} if the message never expires
     */
    OffsetDateTime getExpiry();

    /**
     * Returns the timestamp when the message was sent.
     *
     * @return the timestamp when the message was sent, or {@code null} if it hasn't been sent
     */
    OffsetDateTime getSent();

    /**
     * Determines if the message has expired.
     * <p/>
     * This only applies to messages that have {@code PENDING status}.
     *
     * @return {@code true} if the message has expired
     */
    boolean isExpired();

    /**
     * Returns the party that this message is for.
     *
     * @return the party, or {@code null} if the party is not known
     */
    Party getRecipient();

    /**
     * Returns the message status.
     *
     * @return the message status
     */
    Status getStatus();

    /**
     * Sets the message status.
     * <p/>
     * This clears any status message.
     *
     * @param status the message status
     */
    void setStatus(Status status);

    /**
     * Sets the message status, along with any status message from the SMS provider.
     *
     * @param status        the status
     * @param statusMessage the status message. May be {@code null}
     */
    void setStatus(Status status, String statusMessage);

    /**
     * Returns the status message.
     *
     * @return the status message. May be {@code null}
     */
    String getStatusMessage();

    /**
     * Determines if the message can transition to the specified status.
     *
     * @param status the status
     * @return {@code true} if the message can transition to the specified status, otherwise {@code false}
     */
    boolean canTransition(Status status);

    /**
     * Determines if the message has a reply with the specified identifier.
     *
     * @param providerId the provider identifier
     * @return {@code true} if there is a reply with the specified identifier
     */
    boolean hasReply(String providerId);

    /**
     * Returns any replies to the message.
     *
     * @return the replies
     */
    List<InboundMessage> getReplies();

    /**
     * Returns a builder to change the state of the message.
     *
     * @return a builder to change the message
     */
    StateBuilder state();

    /**
     * Returns a builder to add a reply to the message.
     *
     * @return a builder to add a reply
     */
    ReplyBuilder reply();

}