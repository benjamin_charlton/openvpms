/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.OutboundMessage;

/**
 * SMS provider.
 *
 * @author Tim Anderson
 */
public interface SMSProvider {

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     * @throws SMSException for any error
     */
    String getName();

    /**
     * Returns the SMS service archetypes that this supports.
     *
     * @return a list of archetypes prefixed by <em>entity.smsProvider</em>
     * @throws SMSException for any error
     */
    String[] getArchetypes();

    /**
     * Sends an SMS.
     * <p/>
     * On successful send, the message status should be updated to {@link OutboundMessage.Status#SENT}.
     * <p/>
     * If the message cannot be sent due to an unrecoverable error, the message status should be updated to
     * {@link OutboundMessage.Status#ERROR}. It will not be resubmitted.
     * <p/>
     * If the message cannot be sent due to a temporary error, throw an {@link SMSException}; it will be resubmitted.
     *
     * @param message the message to send
     * @param config  the SMS provider configuration
     * @throws SMSException if the send fails and may be retried
     */
    void send(OutboundMessage message, Entity config);
}
