/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import org.openvpms.component.model.party.Party;

import java.time.OffsetDateTime;

/**
 * Builder for {@link OutboundMessage}s.
 *
 * @author Tim Anderson
 */
public interface OutboundMessageBuilder extends MessageBuilder<OutboundMessageBuilder> {

    /**
     * Sets the recipient of the SMS.
     *
     * @param recipient the recipient
     * @return this
     */
    OutboundMessageBuilder recipient(Party recipient);

    /**
     * Sets the timestamp when the message expires.
     * <p/>
     * If it has not been sent to the provider by this time, it's status will change to {@link OutboundMessage.Status#EXPIRED}.
     * <p/>
     * If it has been sent to the provider, but not delivered by this time, the provider may choose not to deliver it.
     *
     * @param expiry the expiry, or {@code null} if the message never expires
     * @return this
     */
    OutboundMessageBuilder expiry(OffsetDateTime expiry);

    /**
     * Sets the customer that the SMS refers to.
     * <p/>
     * Defaults to the {@link #recipient}, if it is a customer.
     *
     * @param customer the customer
     * @return this
     */
    OutboundMessageBuilder customer(Party customer);

    /**
     * Sets the patient that the SMS refers to.
     *
     * @param patient the patient
     * @return this
     */
    OutboundMessageBuilder patient(Party patient);

    /**
     * Sets  the subject of the SMS, for communication logging purposes.
     *
     * @param subject the SMS subject
     * @return this
     */
    OutboundMessageBuilder subject(String subject);

    /**
     * Sets the reason for the SMS, for communication logging purposes.
     *
     * @param reason the reason for the SMS. A code for an <em>lookup.customerCommunicationReason</em>
     * @return this
     */
    OutboundMessageBuilder reason(String reason);

    /**
     * Sets the note about the SMS, for communication logging purposes.
     *
     * @param note the note
     * @return this
     */
    OutboundMessageBuilder note(String note);

    /**
     * Builds the message.
     *
     * @return the message
     */
    OutboundMessage build();

}
