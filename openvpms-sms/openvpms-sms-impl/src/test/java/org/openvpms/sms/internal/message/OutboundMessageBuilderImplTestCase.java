/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.factory.DomainServiceImpl;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.mail.AbstractSMSTest;
import org.openvpms.sms.message.OutboundMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * Tests the {@link OutboundMessageBuilderImpl} class.
 *
 * @author Tim Anderson
 */
public class OutboundMessageBuilderImplTestCase extends AbstractSMSTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;


    /**
     * The builder.
     */
    private OutboundMessageBuilderImpl builder;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        ArchetypeService service = getArchetypeService();
        DomainService domainService = new DomainServiceImpl(service);
        builder = new OutboundMessageBuilderImpl(service, domainService);
    }

    /**
     * Tests the building a message.
     */
    @Test
    public void testBuild() {
        Party recipient = supplierFactory.createVet();
        Party location = practiceFactory.createLocation();
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        String reason = lookupFactory.getLookup("lookup.customerCommunicationReason", "PATIENT_UPDATE").getCode();
        Date expiryDate = DateRules.getTomorrow();
        OffsetDateTime expiryDateTime = DateRules.toOffsetDateTime(expiryDate);
        OutboundMessage message = builder.recipient(recipient)
                .phone("12345678")
                .message("a message")
                .location(location)
                .reason(reason)
                .subject("a subject")
                .patient(patient)
                .customer(customer)
                .expiry(expiryDateTime)
                .note("a note")
                .build();

        assertEquals(recipient, message.getRecipient());
        assertEquals("12345678", message.getPhone());
        assertEquals("a message", message.getMessage());
        assertEquals(OutboundMessage.Status.PENDING, message.getStatus());
        assertEquals(expiryDateTime, message.getExpiry());

        ((OutboundMessageImpl) message).queue();  // required to save it.

        // check the underlying act
        Act act = getAct(message);
        IMObjectBean bean = getBean(act);
        assertEquals(recipient, bean.getTarget("contact"));
        assertEquals("12345678", bean.getString("phone"));
        assertEquals("a message", bean.getString("message"));
        assertEquals(OutboundMessage.Status.PENDING.toString(), act.getStatus());
        assertEquals(reason, act.getReason());
        assertEquals(expiryDate, bean.getDate("expiryTime"));
        assertEquals(OutboundMessageImpl.QueueStatus.QUEUED.toString(), act.getStatus2());
        assertEquals(customer, bean.getTarget("customer"));
        assertEquals(patient, bean.getTarget("patient"));
        assertEquals("a note", bean.getString("note"));
    }

    /**
     * Verifies that when the recipient is a customer, the customer node is automatically populated.
     */
    @Test
    public void testCustomerIsSetWhenRecipientIsACustomer() {
        Party customer = customerFactory.createCustomer();
        OutboundMessage message = builder.recipient(customer)
                .phone("12345678")
                .message("a message")
                .build();

        ((OutboundMessageImpl) message).queue();  // required to save it.

        // check the underlying act
        Act act = getAct(message);
        IMObjectBean bean = getBean(act);
        assertEquals(customer, bean.getTarget("contact"));
        assertEquals(customer, bean.getTarget("customer"));
    }

    /**
     * Verifies a message can be built with no recipient.
     */
    @Test
    public void testNoRecipient() {
        Party customer = customerFactory.createCustomer();
        OutboundMessage message = builder.customer(customer)
                .phone("12345678")
                .message("a message")
                .build();

        ((OutboundMessageImpl) message).queue();  // required to save it.

        // check the underlying act
        Act act = getAct(message);
        IMObjectBean bean = getBean(act);
        assertNull(bean.getTarget("contact"));
        assertEquals(customer, bean.getTarget("customer"));
    }

    /**
     * Verifies a validation exception is thrown if no phone number is present.
     */
    @Test
    public void testNoPhone() {
        Party customer = customerFactory.createCustomer();
        try {
            builder.customer(customer)
                    .phone(null)
                    .message("a message")
                    .build();
            fail("Expected build() to fail");
        } catch (SMSException exception) {
            assertEquals("SMS-0040: No phone number specified", exception.getMessage());
        }
    }
}
