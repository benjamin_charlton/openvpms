/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.service.practice.PracticeService;
import org.openvpms.sms.internal.message.OutboundMessageBuilderImpl;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.OutboundMessageBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.List;

/**
 * Generates SMSes and replies for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSMSGenerator {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The message builder.
     */
    private final OutboundMessageBuilder builder;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The available practice locations.
     */
    private final List<Location> locations;

    /**
     * Constructs a {@link TestSMSGenerator}.
     *
     * @param service         the archetype service
     * @param domainService   the domain service
     * @param practiceService the practice service
     */
    public TestSMSGenerator(ArchetypeService service, DomainService domainService, PracticeService practiceService) {
        this.service = service;
        this.domainService = domainService;
        builder = new OutboundMessageBuilderImpl(service, domainService);
        locations = practiceService.getLocations();
    }

    /**
     * Generates SMS messages and replies for each customer with a mobile.
     */
    public void generate() {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        Root<Party> root = query.from(Party.class, CustomerArchetypes.PERSON);
        query.orderBy(builder.asc(root.get("id")));
        TypedQueryIterator<Party> iterator = new TypedQueryIterator<>(service.createQuery(query), 100);
        int index = 0;
        while (iterator.hasNext()) {
            Customer customer = domainService.create(iterator.next(), Customer.class);
            Phone phone = customer.getMobilePhone();
            if (phone != null) {
                if (index >= locations.size()) {
                    index = 0;
                }
                Location location = index < locations.size() ? locations.get(index++) : null;
                generateMessageWithReplies(customer, phone, location);
            }
        }
    }

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String contextPath = "applicationContext.xml";
        ApplicationContext context;
        if (!new File(contextPath).exists()) {
            context = new ClassPathXmlApplicationContext(contextPath);
        } else {
            context = new FileSystemXmlApplicationContext(contextPath);
        }
        TestSMSGenerator generator = new TestSMSGenerator(context.getBean(IArchetypeRuleService.class),
                                                          context.getBean(DomainService.class),
                                                          context.getBean(PracticeService.class));
        generator.generate();
    }

    private void generateMessageWithReplies(Customer customer, Phone phone, Location location) {
        Patient patient = customer.getPatients().active().getObject();
        OutboundMessage message = builder
                .recipient(customer)
                .message("test message")
                .location(location)
                .customer(customer)
                .patient(patient)
                .phone(phone.getPhoneNumber())
                .build();
        message.setStatus(OutboundMessage.Status.SENT);
        message.state().reply()
                .message("first test reply")
                .phone(phone.getPhoneNumber())
                .build();
        message.state().reply()
                .message("second test\nreply")
                .phone(phone.getPhoneNumber())
                .build();
    }

}