/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.internal.mail.AbstractSMSTest;
import org.openvpms.sms.internal.service.SMSLengthCalculator;
import org.openvpms.sms.internal.service.SMSLengthCalculatorTestCase;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link MessagesImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class MessagesImplTestCase extends AbstractSMSTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The messages service.
     */
    private MessagesImpl messages;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        messages = new MessagesImpl(getArchetypeService(), domainService);
    }

    /**
     * Tests the {@link MessagesImpl#getParts(String)} method.
     * <p/>
     * This delegates to {@link SMSLengthCalculator}. For a more comprehensive test,
     * see {@link SMSLengthCalculatorTestCase}.
     */
    @Test
    public void testGetParts() {
        // check parts for messages that will be encoded as GSM
        assertEquals(1, messages.getParts(""));
        assertEquals(1, messages.getParts(StringUtils.repeat("x", 160)));
        assertEquals(2, messages.getParts(StringUtils.repeat("x", 161)));
        assertEquals(2, messages.getParts(StringUtils.repeat("x", 306)));
        assertEquals(3, messages.getParts(StringUtils.repeat("x", 307)));
        assertEquals(3, messages.getParts(StringUtils.repeat("x", 459)));
        assertEquals(4, messages.getParts(StringUtils.repeat("x", 460)));
        assertEquals(4, messages.getParts(StringUtils.repeat("x", 612)));
        assertEquals(5, messages.getParts(StringUtils.repeat("x", 613)));

        // checks parts for messages that will be encoded as UCS-2
        assertEquals(1, messages.getParts(StringUtils.repeat("\u0080", 70)));
        assertEquals(2, messages.getParts(StringUtils.repeat("\u0080", 71)));
        assertEquals(2, messages.getParts(StringUtils.repeat("\u0080", 134)));
        assertEquals(3, messages.getParts(StringUtils.repeat("\u0080", 135)));
        assertEquals(3, messages.getParts(StringUtils.repeat("\u0080", 201)));
        assertEquals(4, messages.getParts(StringUtils.repeat("\u0080", 202)));
        assertEquals(4, messages.getParts(StringUtils.repeat("\u0080", 268)));
    }

    /**
     * Tests the {@link MessagesImpl#getOutboundMessage(long)} method.
     */
    @Test
    public void testGetOutboundMessage() {
        OutboundMessage message = createOutboundMessage(null, "a message");
        OutboundMessage retrieved = messages.getOutboundMessage(message.getId());
        assertNotNull(retrieved);
        assertEquals("a message", retrieved.getMessage());
    }

    /**
     * Tests the {@link MessagesImpl#getOutboundMessage(long)} method.
     */
    @Test
    public void testGetOutboundMessageForProviderId() {
        String id = UUID.randomUUID().toString();
        createOutboundMessage(id, "another message");
        OutboundMessage retrieved = messages.getOutboundMessage("actIdentity.smsTest", id);
        assertNotNull(retrieved);
        assertEquals("another message", retrieved.getMessage());
    }

    /**
     * Tests the {@link MessagesImpl#getSent(OffsetDateTime, String)} method.
     * <p/>
     * This generates a an <em>actIdentity.sms</em> archetype to ensure messages from earlier tests don't get picked
     * up.
     */
    @Test
    public void testGetSent() {
        OffsetDateTime now = DateRules.toOffsetDateTime(DateUtils.truncate(new Date(), Calendar.SECOND));
        // truncate to seconds as MySQL doesn't store millis

        // generate some SENT messages
        List<OutboundMessage> created = new ArrayList<>();
        String identityArchetype = createIdentityArchetype();
        Party customer = customerFactory.createCustomer();
        for (int i = 0; i < 10; ++i) {
            OutboundMessage message = messages.getOutboundMessageBuilder()
                    .recipient(customer)
                    .message(Integer.toString(i))
                    .phone("123456")
                    .build();
            message.state()
                    .status(OutboundMessage.Status.SENT)
                    .providerId(identityArchetype, UUID.randomUUID().toString())
                    .build();
            created.add(message);
        }

        // retrieve them and verify they match those sent
        Iterable<OutboundMessage> iterable = messages.getSent(now, identityArchetype);
        List<OutboundMessage> loaded = new ArrayList<>();
        CollectionUtils.addAll(loaded, iterable);

        assertEquals(created, loaded);
    }

    /**
     * Tests the {@link MessagesImpl#getInboundMessage(long)} method.
     */
    @Test
    public void testGetInboundMessage() {
        InboundMessage message = createInboundMessage(null, "a reply");
        InboundMessage retrieved = messages.getInboundMessage(message.getId());
        assertNotNull(retrieved);
        assertEquals("a reply", retrieved.getMessage());
    }

    /**
     * Tests the {@link MessagesImpl#getInboundMessage(String, String)} method.
     */
    @Test
    public void testGetInboundMessageForProviderId() {
        String id = UUID.randomUUID().toString();
        createInboundMessage(id, "another reply");
        InboundMessage retrieved = messages.getInboundMessage("actIdentity.smsTest", id);
        assertNotNull(retrieved);
        assertEquals("another reply", retrieved.getMessage());
    }

    /**
     * Creates a new outbound message.
     *
     * @param id   the provider id. May be {@code null}
     * @param text the message text
     * @return a new outbound message
     */
    private OutboundMessage createOutboundMessage(String id, String text) {
        OutboundMessage message = messages.getOutboundMessageBuilder()
                .recipient(customerFactory.createCustomer())
                .message(text)
                .phone("123456")
                .build();
        ((OutboundMessageImpl) message).queue();  // required to save the message
        if (id != null) {
            message.state().providerId("actIdentity.smsTest", id)
                    .build();
        }
        return message;
    }

    /**
     * Creates a new outbound message.
     *
     * @param id   the provider id. May be {@code null}
     * @param text the message text
     * @return a new outbound message
     */
    private InboundMessage createInboundMessage(String id, String text) {
        return messages.getInboundMessageBuilder()
                .providerId("actIdentity.smsTest", id)
                .message(text)
                .phone("123456")
                .build();
    }

    private String createIdentityArchetype() {
        ArchetypeDescriptor descriptor = new ArchetypeDescriptor();
        String archetype = TestHelper.randomName("actIdentity.sms");
        String name = archetype + ".1.0";
        descriptor.setName(name);
        descriptor.setClassName(org.openvpms.component.business.domain.im.act.ActIdentity.class.getName());
        NodeDescriptor id = new NodeDescriptor();
        id.setName("id");
        id.setPath("/id");
        id.setType(Long.class.getName());
        NodeDescriptor identity = new NodeDescriptor();
        identity.setName("identity");
        identity.setPath("/identity");
        identity.setType(String.class.getName());
        descriptor.addNodeDescriptor(identity);
        save(descriptor);
        return archetype;
    }
}