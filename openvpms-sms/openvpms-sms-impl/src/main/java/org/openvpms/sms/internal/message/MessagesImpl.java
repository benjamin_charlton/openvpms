/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.service.SMSLengthCalculator;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.InboundMessageBuilder;
import org.openvpms.sms.message.Messages;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.OutboundMessageBuilder;

import java.time.OffsetDateTime;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Default implementation of {@link Messages}.
 *
 * @author Tim Anderson
 */
public class MessagesImpl implements Messages {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs an {@link MessagesImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain object factory
     */
    public MessagesImpl(ArchetypeService service, DomainService domainService) {
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Returns the number of parts that a message comprises.
     * <p>
     * The number of parts is determined by the length of the message, and how the message will be encoded.
     * <p>
     * A message that can be encoded using 7-bit GSM characters will use:
     * <ul>
     * <li>a single part for up to 160 characters</li>
     * <li>multiples of 153 characters for multi-part messages</li>
     * </ul>
     * A message encoded using UCS-2 will use:
     * <ul>
     * <li>a single part for up to 70 characters</li>
     * <li>multiples of 67 characters for multi-part messages</li>
     * </ul>
     *
     * @param message the message
     * @return the number of parts
     */
    @Override
    public int getParts(String message) {
        return SMSLengthCalculator.getParts(message);
    }

    /**
     * Returns a builder to build outbound messages.
     *
     * @return a message builder
     */
    @Override
    public OutboundMessageBuilder getOutboundMessageBuilder() {
        return new OutboundMessageBuilderImpl(service, domainService);
    }

    /**
     * Returns a message given its OpenVPMS identifier.
     *
     * @param id the message identifier
     * @return the corresponding message, or {@code null} if none is found
     */
    @Override
    public OutboundMessage getOutboundMessage(long id) {
        Act act = getOutbound(id);
        return (act != null) ? new OutboundMessageImpl(act, service, domainService) : null;
    }

    /**
     * Returns a message given its provider identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return the corresponding message, or {@code null} if none is found
     * @throws SMSException for any SMS error
     */
    @Override
    public OutboundMessage getOutboundMessage(String archetype, String id) {
        Act act = getOutbound(archetype, id);
        return act != null ? new OutboundMessageImpl(act, service, domainService) : null;
    }


    /**
     * Returns messages that have been sent from the specified time.
     * <p/>
     * This implementation pages the messages to limit memory use; any message status changes will therefore affect
     * subsequent pages.
     *
     * @param from      the messages
     * @param archetype the provider SMS id archetype
     * @return the sent messages
     */
    @Override
    public Iterable<OutboundMessage> getSent(OffsetDateTime from, String archetype) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, SMSArchetypes.MESSAGE);
        root.join("providerId", archetype);
        query.where(builder.equal(root.get("status"), OutboundMessage.Status.SENT.toString()),
                    builder.greaterThanOrEqualTo(root.get("endTime"), DateRules.toDate(from)));
        query.orderBy(builder.asc(root.get("id")));
        return new MessageIterable(query);
    }

    /**
     * Returns an inbound message given its OpenVPMS identifier.
     *
     * @param id the message identifier
     * @return the corresponding message, or {@code null} if none is found
     */
    @Override
    public InboundMessage getInboundMessage(long id) {
        Act act = service.get(SMSArchetypes.REPLY, id, Act.class);
        return (act != null) ? new InboundMessageImpl(act, service, domainService) : null;
    }

    /**
     * Returns an inbound message given its provider identifier.
     * <p/>
     * If there are multiple inbound messages with the same identifier, this returns that with the lower OpenVPMS id.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return the corresponding inbound message, or {@code null} if none is found
     * @throws SMSException for any SMS error
     */
    @Override
    public InboundMessage getInboundMessage(String archetype, String id) {
        Act act = getMessage(SMSArchetypes.REPLY, archetype, id);
        return act != null ? new InboundMessageImpl(act, service, domainService) : null;
    }

    /**
     * Returns a builder for an inbound message.
     * <p/>
     * NOTE: this should only be used for inbound messages that are not replies.<br/>
     * For replies, use {@link OutboundMessage#reply()}.
     *
     * @return a new builder
     */
    @Override
    public InboundMessageBuilder getInboundMessageBuilder() {
        return new InboundMessageBuilderImpl(service, domainService, this);
    }

    /**
     * Returns an outbound message act, given its OpenVPMS identifier.
     *
     * @param id the message identifer
     * @return the corresponding message act, or {@code null} if none is found
     */
    public Act getOutbound(long id) {
        return service.get(SMSArchetypes.MESSAGE, id, Act.class);
    }

    /**
     * Returns an outbound message act, given its provider identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return the corresponding message act, or {@code null} if none is found
     */
    public Act getOutbound(String archetype, String id) {
        return getMessage(SMSArchetypes.MESSAGE, archetype, id);
    }

    /**
     * Returns a message given its archetype and provider identifier.
     *
     * @param archetype   the message archetype
     * @param idArchetype the identifier archetype
     * @param id          the message identifier
     * @return the corresponding message, or {@code null} if none is found
     */
    private Act getMessage(String archetype, String idArchetype, String id) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, archetype);
        Join<Act, IMObject> providerId = root.join("providerId", idArchetype);
        providerId.on(builder.equal(providerId.get("identity"), id));
        query.orderBy(builder.asc(root.get("id")));
        // shouldn't need to sort by id as providerIds should be unique. Sort by id just in case this isn't the case,
        // to ensure consistent behaviour

        return service.createQuery(query).getFirstResult();
    }

    /**
     * Adapts a query returning <em>act.smsMessage</em> to an Iterable of {@link OutboundMessage}.
     */
    private class MessageIterable implements Iterable<OutboundMessage> {

        private final CriteriaQuery<Act> query;

        public MessageIterable(CriteriaQuery<Act> query) {
            this.query = query;
        }

        /**
         * Returns an iterator over elements.
         *
         * @return an Iterator
         */
        @Override
        public Iterator<OutboundMessage> iterator() {
            TypedQuery<Act> typed = service.createQuery(query);
            return new MessageIterator(new TypedQueryIterator<>(typed, 100));
        }
    }

    /**
     * Adapts an iterator returning acts to {@link OutboundMessage}.
     */
    private class MessageIterator implements Iterator<OutboundMessage> {

        /**
         * The act iterator.
         */
        private final Iterator<Act> iterator;

        /**
         * Constructs a {@link MessageIterator}.
         *
         * @param iterator the act iterator
         */
        public MessageIterator(Iterator<Act> iterator) {
            this.iterator = iterator;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public OutboundMessage next() {
            Act act = iterator.next();
            return new OutboundMessageImpl(act, service, domainService);
        }
    }
}
