/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.mail.template;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;

/**
 * Factory for creating {@link MailTemplate} instances from <em>entity.SMSConfigEmail*</em>.
 *
 * @author Tim Anderson
 */
public class MailTemplateFactory {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Nodes which may not appear as variables.
     */
    private static final String[] RESERVED = {"name", "description", "website", "from", "fromExpression", "to",
                                              "toExpression", "replyTo", "replyToExpression", "subject",
                                              "subjectExpression", "text", "textExpression", "phone", "message",
                                              "active"};

    /**
     * Constructs a {@link MailTemplateFactory}.
     *
     * @param service the service
     */
    public MailTemplateFactory(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Populates a template from an <em>entity.SMSEmail*</em> configuration.
     *
     * @param config the SMS configuration
     * @return the template
     */
    public MailTemplate getTemplate(Entity config) {
        IMObjectBean configBean = service.getBean(config);
        ArchetypeDescriptor archetype = service.getArchetypeDescriptor(config.getArchetype());
        MailTemplate result = new MailTemplate();
        result.setCountryPrefix(getString(configBean, "countryPrefix"));
        result.setAreaPrefix(getString(configBean, "areaPrefix"));
        result.setFrom(getString(configBean, "from"));
        result.setFromExpression(getString(configBean, "fromExpression"));
        result.setTo(getString(configBean, "to"));
        result.setToExpression(getString(configBean, "toExpression"));
        result.setReplyTo(getString(configBean, "replyTo"));
        result.setReplyToExpression(getString(configBean, "replyToExpression"));
        result.setSubject(getString(configBean, "subject"));
        result.setSubjectExpression(getString(configBean, "subjectExpression"));
        result.setText(getString(configBean, "text"));
        result.setTextExpression(getString(configBean, "textExpression"));
        int maxParts = 1;
        if (configBean.hasNode("parts")) {
            maxParts = configBean.getInt("parts", 1);
            if (maxParts < 1) {
                maxParts = 1;
            }
        }
        result.setMaxParts(maxParts);
        for (NodeDescriptor descriptor : archetype.getNodeDescriptors()) {
            String type = descriptor.getType();
            if ((String.class.getName().equals(type) || Boolean.class.getName().equals(type))
                && !ArrayUtils.contains(RESERVED, descriptor.getName())) {
                Object value = ((org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor) descriptor).getValue((IMObject) config);
                result.addVariable(descriptor.getName(), value);
            }
        }
        return result;
    }

    /**
     * Helper to returned the named string from the bean, if it exists.
     *
     * @param bean the bean
     * @param name the node name
     * @return the string value. May be {@code null}
     */
    private String getString(IMObjectBean bean, String name) {
        String result = null;
        if (bean.hasNode(name)) {
            result = StringUtils.trimToNull(bean.getString(name));
        }
        return result;
    }

}
