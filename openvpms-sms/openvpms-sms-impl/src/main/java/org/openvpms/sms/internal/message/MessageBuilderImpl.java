/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.message.MessageBuilder;

/**
 * Default implementation of {@link MessageBuilder}.
 *
 * @author Tim Anderson
 */
public abstract class MessageBuilderImpl<T extends MessageBuilder<T>> implements MessageBuilder<T> {

    /**
     * The practice location the SMS relates to.
     */
    private Party location;

    /**
     * The phone number.
     */
    private String phone;

    /**
     * The message.
     */
    private String message;

    /**
     * Returns the phone number.
     *
     * @return the phone number. May be {@code null}
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone number.
     *
     * @param phone the phone number
     * @return this
     */
    public T phone(String phone) {
        this.phone = phone;
        return getThis();
    }

    /**
     * Sets the message.
     *
     * @param message the message
     * @return this
     */
    public T message(String message) {
        this.message = message;
        return getThis();
    }

    /**
     * Sets the practice location that this message relates to.
     * <p/>
     * For outbound messages this is the practice location that sent the message.<br/>
     * For inbound messages, this is the practice location that the message is for.
     *
     * @param location the location
     * @return this
     */
    @Override
    public T location(Party location) {
        this.location = location;
        return getThis();
    }

    /**
     * Builds the message
     *
     * @param bean the message bean
     */
    protected void build(IMObjectBean bean) {
        bean.setValue("phone", phone);
        bean.setValue("message", message);
        if (location != null) {
            bean.setTarget("location", location);
        }
    }

    /**
     * Returns this.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected T getThis() {
        return (T) this;
    }
}
