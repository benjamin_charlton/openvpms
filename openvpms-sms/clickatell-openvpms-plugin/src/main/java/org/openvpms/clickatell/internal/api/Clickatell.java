/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.api;

import org.openvpms.clickatell.internal.model.SMS;
import org.openvpms.clickatell.internal.model.SMSResponses;
import org.openvpms.clickatell.internal.model.SMSStatus;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Clickatell REST API.
 *
 * @author Tim Anderson
 */
public interface Clickatell {

    /**
     * Sends a message.
     *
     * @param message the SMS message
     * @return the responses
     */
    @POST
    @Path("/messages")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    SMSResponses send(SMS message);

    /**
     * Returns the status of a message.
     *
     * @param messageId the message identifier
     * @return the status of the message
     */
    @GET
    @Path("/public-client/message/status")
    SMSStatus getStatus(@QueryParam("messageId") String messageId);

}
