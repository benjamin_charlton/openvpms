/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.service;

import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.client.ClickatellClient;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.plugin.service.util.ContextClassLoaderHelper;

/**
 * Factory for {@link Clickatell} instances.
 * <p/>
 * This proxies all calls to ensure the thread context class loader is set. This is to avoid problems with the
 * wrong javax.ws.rs.ext.RuntimeDelegate being used.
 *
 * @author Tim Anderson
 */
public class ClickatellFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The password encryptor.
     */
    private final PasswordEncryptor encryptor;

    /**
     * Constructs a {@link ClickatellFactory}.
     *
     * @param service   the archetype service
     * @param encryptor the password encryptor
     */
    public ClickatellFactory(ArchetypeService service, PasswordEncryptor encryptor) {
        this.encryptor = encryptor;
        this.service = service;
    }

    /**
     * Creates a new {@link Clickatell} client.
     *
     * @param config the configuration
     * @return a new client
     */
    public Clickatell create(IMObject config) {
        IMObjectBean bean = service.getBean(config);
        return create(bean);
    }

    /**
     * Creates a new {@link Clickatell} client.
     *
     * @param bean the configuration
     * @return a new client
     */
    public Clickatell create(IMObjectBean bean) {
        String apiKey = encryptor.decrypt(bean.getString("apiKey"));
        return ContextClassLoaderHelper.proxy(() -> create(apiKey));
    }

    /**
     * Returns the Clickatell API client.
     *
     * @param apiKey the API key
     * @return a new client
     */
    protected Clickatell create(String apiKey) {
        return new ClickatellClient(apiKey);
    }
}
