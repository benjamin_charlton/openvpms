/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.service;

import org.apache.commons.lang3.time.StopWatch;
import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.model.SMSStatus;
import org.openvpms.sms.message.Messages;
import org.openvpms.sms.message.OutboundMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Checks to see if any SENT messages have been delivered or expired by Clickatell, updating their statuses accordingly.
 *
 * @author Tim Anderson
 */
public class MessageStatusUpdater {

    /**
     * The Clickatell client.
     */
    private final Clickatell clickatell;

    /**
     * The message service.
     */
    private final Messages messages;

    /**
     * Process all SENT messages sent on/after this time.
     */
    private final OffsetDateTime from;

    /**
     * Flag to indicate if updates should stop.
     */
    private volatile boolean stop;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MessageStatusUpdater.class);

    /**
     * Constructs a {@link MessageStatusUpdater}.
     *
     * @param clickatell the CLickatell client
     * @param messages   the messages service
     * @param from       processes all SENT messages sent on/after this time
     */
    public MessageStatusUpdater(Clickatell clickatell, Messages messages, OffsetDateTime from) {
        this.clickatell = clickatell;
        this.messages = messages;
        this.from = from;
    }

    /**
     * Runs the update.
     *
     * @return the number of updated messages
     */
    public int update() {
        Set<Long> seen = new HashSet<>();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int passes = 0;
        int updated = 0;
        try {
            while (!stop) {
                passes++;
                int changed = update(seen);
                if (changed == 0) {
                    break;
                }
                updated += changed;
            }
        } catch (Throwable exception) {
            log.error("Failed to update SMS message statuses: {}", exception.getMessage(), exception);
        }
        stopWatch.stop();
        log.debug("Processed={}" + ", updated={}, passes={} in {}", seen.size(), updated, passes, stopWatch);
        return updated;
    }

    /**
     * Indicates to stop updating.
     */
    public void stop() {
        stop = true;
    }

    /**
     * Updates the status of messages that have been sent since the {@code from} time.
     * <p/>
     * As any status changes will change the paged iteration over the messages, this needs to be called multiple
     * times to ensure messages aren't missed.
     *
     * @param seen the set of seen message identifiers, to avoid processing the same message twice
     * @return the number of updated messages
     */
    protected int update(Set<Long> seen) {
        int updated = 0;
        Iterable<OutboundMessage> sent = messages.getSent(from, ClickatellArchetypes.ID);
        for (OutboundMessage message : sent) {
            if (stop) {
                break;
            }
            if (seen.add(message.getId())) {
                String messageId = message.getProviderId();
                if (messageId != null) {
                    SMSStatus status = getStatus(messageId);
                    if (status != null) {
                        if ("RECEIVED_BY_RECIPIENT".equals(status.getStatus())) {
                            message.setStatus(OutboundMessage.Status.DELIVERED);
                            updated++;
                        } else if ("EXPIRED".equals(status.getStatus())) {
                            message.setStatus(OutboundMessage.Status.EXPIRED);
                            updated++;
                        } else {
                            log.debug("Cannot update message={} with Clickatell Id={}, Clickatell status={}",
                                      message.getId(), messageId, status.getStatus());
                        }
                    }
                }
            }
        }
        return updated;
    }

    /**
     * Returns the SMS status for a message.
     *
     * @param messageId the message identifier
     * @return the status, or {@code null} if the message cannot be found
     */
    private SMSStatus getStatus(String messageId) {
        try {
            return clickatell.getStatus(messageId);
        } catch (NotFoundException ignore) {
            log.warn("failed to get status for message={}", messageId);
            return null;
        }
    }
}
