/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Test;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.LookupLink;
import org.openvpms.component.model.lookup.LookupRelationship;
import org.openvpms.component.model.object.Reference;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test that ability to create and query lookups.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("archetype-service-appcontext.xml")
public class ArchetypeServiceLookupTestCase extends AbstractArchetypeServiceTest {

    /**
     * Tests lookup creation, retrieval and removal.
     */
    @Test
    public void testCreateRemoveLookup() {
        IArchetypeService service = getArchetypeService();
        Lookup lookup1 = createBreed("DASCHUND");
        Lookup lookup2 = (Lookup) service.get(lookup1.getObjectReference());

        assertEquals(lookup1, lookup2);

        service.remove(lookup1);
        assertNull(service.get(lookup1.getObjectReference()));
    }

    /**
     * Verifies that the create/update information is populated.
     */
    @Test
    public void testCreateUpdate() {
        AuditableTestHelper auditor = new AuditableTestHelper(getArchetypeService());
        User user1 = createUser();
        User user2 = createUser();
        save(user1);
        save(user2);

        Lookup breed = (Lookup) create("lookup.breed");
        breed.setCode("BLUE_HEELER" + "_" + System.nanoTime());
        auditor.checkSave(breed, user1);

        // verify create/update information doesn't change when the object is saved without being modified
        auditor.checkSaveUnchanged(breed, user2);

        // change an attribute and verify the create/update details are propagated back to the lookup
        breed.setName("Blue Heeler");
        save(breed);
        auditor.checkSave(breed, user2);

        // verify the object updates if a relationship is added
        Lookup species = createSpecies("CANINE");
        IArchetypeService service = getArchetypeService();
        IMObjectBean bean = service.getBean(species);
        bean.addTarget("target", breed, "source");
        Lookup saved = auditor.checkSave(breed, user1, user1, () -> save(species, breed));
        assertEquals(1, saved.getTargetLookupRelationships().size());
    }

    /**
     * Verifies that when the source or target of a lookup relationship is deleted, the related lookup is retained.
     */
    @Test
    public void testDeleteLookupWithRelationships() {
        checkDeleteLookupWithRelationships(false);
    }

    /**
     * Verifies that when the source or target of a lookup relationship is deleted by reference, the related lookup is
     * retained.
     */
    @Test
    public void testDeleteLookupWithRelationshipsByReference() {
        checkDeleteLookupWithRelationships(true);
    }

    /**
     * Verifies that the target of an {@link LookupLink} cannot be removed, and that removal of the source
     * of an {@link LookupLink} doesn't cascade to the target.
     */
    @Test
    public void testAddRemoveLookupLinks() {
        checkAddRemoveLookupLinks(false);
    }

    /**
     * Verifies that the target of an {@link LookupLink} cannot be removed by reference, and that removal of the source
     * of an {@link LookupLink} doesn't cascade to the target.
     */
    @Test
    public void testAddRemoveLookupLinksByReference() {
        checkAddRemoveLookupLinks(true);
    }

    /**
     * Verifies that when the source or target of a lookup relationship is deleted, the related lookup is retained.
     *
     * @param byReference if {@code true}, use {@link IArchetypeService#remove(Reference)} else use
     *                    {@link IArchetypeService#remove(org.openvpms.component.model.object.IMObject)}
     */
    private void checkDeleteLookupWithRelationships(boolean byReference) {
        Lookup breed1 = createBreed("DASCHUND");
        Lookup breed2 = createBreed("DALMATION");
        Lookup species = createSpecies("CANINE");

        IMObjectBean bean = getBean(species);
        LookupRelationship relationship1 = (LookupRelationship) bean.addTarget("target", breed1, "source");
        LookupRelationship relationship2 = (LookupRelationship) bean.addTarget("target", breed2, "source");
        save(species, breed1, breed2);

        // reload
        species = get(species);
        breed1 = get(breed1);
        breed2 = get(breed2);

        // verify relationships exists
        assertTrue(species.getSourceLookupRelationships().contains(relationship1));
        assertTrue(species.getSourceLookupRelationships().contains(relationship2));
        assertTrue(breed1.getTargetLookupRelationships().contains(relationship1));
        assertTrue(breed2.getTargetLookupRelationships().contains(relationship2));

        // now remove breed2
        remove(breed2, byReference);
        assertNull(get(breed2));

        // verify species exists and is not related to breed2
        species = get(species);
        assertNotNull(species);
        assertFalse(species.getSourceLookupRelationships().contains(relationship2));

        // now remove species
        remove(species, byReference);
        assertNull(get(species));

        // verify breed1 exists and is not related to species
        breed1 = get(breed1);
        assertNotNull(breed1);
        assertFalse(breed1.getTargetLookupRelationships().contains(relationship1));
    }

    /**
     * Verifies that the target of an {@link LookupLink} cannot be removed, and that removal of the source
     * of an {@link LookupLink} doesn't cascade to the target.
     *
     * @param byReference if {@code true}, use {@link IArchetypeService#remove(Reference)} else use
     *                    {@link IArchetypeService#remove(org.openvpms.component.model.object.IMObject)}
     */
    private void checkAddRemoveLookupLinks(boolean byReference) {
        Lookup species = createSpecies("FELINE");
        Lookup mapping = create("lookup.speciesCustom", "CAT");

        IMObjectBean bean = getArchetypeService().getBean(mapping);
        bean.addTarget("mapping", species);
        save(mapping);

        // verify that the species cannot be removed as it linked from the mapping
        try {
            remove(species, byReference);
            fail("Expected removal of LookupLink target to fail");
        } catch (ArchetypeServiceException expected) {
            if (byReference) {
                assertEquals("Failed to delete object with reference " + species.getObjectReference(),
                             expected.getMessage());
            } else {
                assertEquals("Failed to delete lookup with reference " + species.getObjectReference()
                             + ": Lookup is in use", expected.getMessage());
            }
        }

        // now remove the source
        remove(mapping, byReference);
        assertNull(get(mapping));

        // verify it hasn't cascaded
        assertNotNull(get(species));

        // now remove the target
        remove(species, byReference);
        assertNull(get(species));
    }

    /**
     * Creates a breed lookup.
     *
     * @param code the breed code
     * @return a new lookup
     */
    private Lookup createBreed(String code) {
        return create("lookup.breed", code);
    }

    /**
     * Creates a species lookup.
     *
     * @param code the species code
     * @return a new lookup
     */
    private Lookup createSpecies(String code) {
        return create("lookup.species", code);
    }

    /**
     * Creates a new lookup.
     *
     * @param archetype the archetype
     * @param code      the code prefix
     * @return a new lookup
     */
    private Lookup create(String archetype, String code) {
        Lookup lookup = (Lookup) create(archetype);
        lookup.setCode(code + "_" + System.nanoTime());
        save(lookup);
        return lookup;
    }
}
