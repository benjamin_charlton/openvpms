/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Test;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Tests the {@link CachingReadOnlyArchetypeService}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("archetype-service-appcontext.xml")
public class CachingReadOnlyAchetypeServiceTestCase extends AbstractReadOnlyAchetypeServiceTest {

    /**
     * Verifies that objects returned by the service are cached.
     * <p/>
     * NOTE: saves/deletes etc do not update the cache.
     */
    @Test
    public void testGetCached() {
        ReadOnlyArchetypeService service = createService();
        User user = createUser();
        save(user);

        IMObject object1 = service.get(user.getObjectReference());
        assertEquals(user, object1);

        IMObject object2 = service.get(user.getObjectReference());
        assertSame(object1, object2);

        IMObject object3 = service.get(user.getObjectReference(), true);
        assertSame(object1, object3);

        IMObject object4 = service.get(user.getObjectReference(), true);
        assertSame(object1, object4);

        User object5 = service.get(user.getObjectReference(), User.class);
        assertSame(object1, object5);

        IMObject object6 = service.get(user.getArchetype(), user.getId());
        assertSame(object1, object6);

        User object7 = service.get(user.getArchetype(), user.getId(), User.class);
        assertSame(object1, object7);

        IMObject object8 = service.get(user.getArchetype(), user.getId(), true);
        assertSame(object1, object8);

        IMObject object9 = service.get(user.getArchetype(), user.getId(), false);
        assertSame(object1, object9);
    }

    /**
     * Creates a new {@link ReadOnlyArchetypeService}.
     *
     * @return a new service
     */
    @Override
    protected ReadOnlyArchetypeService createService() {
        return new CachingReadOnlyArchetypeService(10, getArchetypeService());
    }

}
