/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.model.entity.Entity;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link MonitoringIMObjectCache} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("../archetype-service-appcontext.xml")
public class MonitoringIMObjectCacheTestCase extends AbstractArchetypeServiceTest {

    /**
     * Tests caching instances of a single archetype.
     */
    @Test
    public void testCache() {
        String archetype = "entity.testCache" + RandomStringUtils.randomAlphabetic(10);
        createArchetypeDescriptor(archetype);
        MonitoringIMObjectCache<Entity> cache = new MonitoringIMObjectCache<>(getArchetypeService(), archetype,
                                                                              Entity.class, true);

        checkCache(cache);

        Entity object1 = createEntity(archetype);
        Entity object2 = createEntity(archetype);
        checkCache(cache, object1, object2);

        // verify inactive objects aren't cached
        object1.setActive(false);
        save(object1);
        checkCache(cache, object2);

        // verify changing inactive -> active objects are cached
        object1.setActive(true);
        save(object1);
        checkCache(cache, object1, object2);

        // verify removed objects aren't cached
        remove(object2);
        checkCache(cache, object1);

        cache.destroy();
    }

    /**
     * Verifies that caches that monitor a wildcarded archetype receive notifications for archetypes that were
     * created after the cache.
     */
    @Test
    public void testWildcardCache() {
        String prefix = "entity.testCache" + RandomStringUtils.randomAlphabetic(10);
        String wildcard = prefix + "*";
        String archetypeA = prefix + "A";
        String archetypeB = prefix + "B";

        createArchetypeDescriptor(archetypeA);
        MonitoringIMObjectCache<Entity> cache = new MonitoringIMObjectCache<>(getArchetypeService(), wildcard,
                                                                              Entity.class, true);

        Entity object1 = createEntity(archetypeA);
        Entity object2 = createEntity(archetypeA);
        checkCache(cache, object1, object2);

        // create new archetype and verify that instances are cached
        createArchetypeDescriptor(archetypeB);

        Entity object3 = createEntity(archetypeB);
        checkCache(cache, object1, object2, object3);

        cache.destroy();
    }

    /**
     * Creates and saves a new entity.
     *
     * @param archetype the archetype
     * @return a new entity
     */
    private Entity createEntity(String archetype) {
        Entity object = (Entity) create(archetype);
        save(object);
        return object;
    }

    /**
     * Verifies a cache contains the expected objects.
     *
     * @param cache    the cache
     * @param expected the expected objects
     */
    private void checkCache(MonitoringIMObjectCache<Entity> cache, Entity... expected) {
        List<Entity> objects = cache.getObjects();
        assertEquals(expected.length, objects.size());
        for (Entity object : expected) {
            assertTrue(objects.contains(object));
        }
    }

    /**
     * Creates an archetype descriptor.
     *
     * @param archetype the archetype
     */
    private void createArchetypeDescriptor(String archetype) {
        ArchetypeDescriptor descriptor = new ArchetypeDescriptor();
        String name = archetype + ".1.0";
        descriptor.setName(name);
        descriptor.setClassName(org.openvpms.component.business.domain.im.common.Entity.class.getName());
        descriptor.setSingleton(true);
        descriptor.setDisplayName("Test " + archetype);
        NodeDescriptor node = new NodeDescriptor();
        node.setName("id");
        node.setPath("/id");
        node.setType(Long.class.getName());
        descriptor.addNodeDescriptor(node);
        save(descriptor);
    }
}
