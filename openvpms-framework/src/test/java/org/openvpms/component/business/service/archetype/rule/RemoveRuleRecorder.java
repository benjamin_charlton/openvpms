/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.rule;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Helper to track invocation of rules when {@link ArchetypeRuleService#remove(Reference)}
 * and {@link ArchetypeRuleService#remove(IMObject)} are invoked.
 *
 * @author Tim Anderson
 */
public class RemoveRuleRecorder {

    /**
     * Tracks if a 'before' rule was invoked for an object prior to its removal.
     */
    private final Set<Reference> removedBefore = Collections.synchronizedSet(new HashSet<>());

    /**
     * Tracks if an 'after' rule was invoked for an object after to its removal.
     */
    private final Set<Reference> removedAfter = Collections.synchronizedSet(new HashSet<>());

    /**
     * Indicates that a 'before' rule was invoked for an object before it was removed.
     *
     * @param object the object
     */
    public void setRemovedBefore(IMObject object) {
        removedBefore.add(object.getObjectReference());
    }

    /**
     * Determines if a 'before' rule was invoked for an object before it was removed.
     *
     * @param object the object
     * @return {@code true} if the rule was invoked
     */
    public boolean removedBeforeInvoked(IMObject object) {
        return removedBefore.contains(object.getObjectReference());
    }

    /**
     * Indicates that an 'after' rule was invoked for an object after it was removed.
     *
     * @param object the object
     */
    public void setRemovedAfter(IMObject object) {
        removedAfter.add(object.getObjectReference());
    }

    /**
     * Determines if an 'after' rule was invoked for an object after it was removed.
     *
     * @param object the object
     * @return {@code true} if the rule was invoked
     */
    public boolean removedAfterInvoked(IMObject object) {
        return removedAfter.contains(object.getObjectReference());
    }

}
