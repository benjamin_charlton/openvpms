/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.business.service.archetype.descriptor.cache.IArchetypeDescriptorCache;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link IMObjectValidator} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("archetype-service-appcontext.xml")
public class IMObjectValidatorTestCase extends AbstractArchetypeServiceTest {

    /**
     * The archetype descriptor cache.
     */
    @Autowired
    private IArchetypeDescriptorCache cache;

    /**
     * The validator.
     */
    private IMObjectValidator validator;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        validator = new IMObjectValidator(cache);
    }

    /**
     * Tests validation of string nodes.
     */
    @Test
    public void testStringNode() {
        Party animal = (Party) create("party.animalpet");
        animal.setName("Fido");
        checkError(animal, "species", "value is required");
        IMObjectBean bean = getBean(animal);
        bean.setValue("species", "");
        checkError(animal, "species", "value is required");
        bean.setValue("species", "CANINE");

        assertTrue(validator.validate(animal).isEmpty());

        animal.setName("X");
        checkError(animal, "name", "String too short. At least 2 characters are required but only 1 were provided");
        animal.setName(StringUtils.repeat("X", 31));
        checkError(animal, "name", "String too long. Maximum length is 30 characters but 31 were provided");

        animal.setName("Fido\u001F");
        checkError(animal, "name", "contains invalid characters");
    }

    /**
     * Tests cardinality validation.
     */
    @Test
    public void testCardinality() {
        Act act = (Act) create("act.customerEstimation");
        act.setStatus("IN_PROGRESS");
        Act item = (Act) create("act.customerEstimationItem");
        Party customer = (Party) create("party.customerperson");
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", customer);
        checkError(act, "items", "must supply at least 1 SourceActRelationship");
        bean.addTarget("items", item);
        assertTrue(validator.validate(act).isEmpty());

        bean.addTarget("customer", create("party.customerperson"));
        checkError(act, "customer", "cannot supply more than 1 item");
    }

    /**
     * Tests validation of reference nodes.
     */
    @Test
    public void testReference() {
        Act act = (Act) create("act.customerEstimation");
        Act item = (Act) create("act.customerEstimationItem");
        act.setStatus("IN_PROGRESS");
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", create("party.customerperson"));
        Relationship relationship = bean.addTarget("items", item);
        assertTrue(validator.validate(act).isEmpty());

        relationship.setTarget(null);
        checkError(act, relationship, "target", "value is required");

        relationship.setTarget(act.getObjectReference());
        checkError(act, relationship, "target", "Validation failed for assertion archetypeRange");
    }

    /**
     * Verifies validation fails with a single error.
     *
     * @param object  the object to validate
     * @param node    the node validation should fail on
     * @param message the expected message
     */
    private void checkError(IMObject object, String node, String message) {
        checkError(object, object, node, message);
    }

    /**
     * Verifies validation fails with a single error.
     *
     * @param object  the object to validate
     * @param error   the object that validation should fail on
     * @param node    the node validation should fail on
     * @param message the expected message
     */
    private void checkError(IMObject object, IMObject error, String node, String message) {
        List<ValidationError> errors = validator.validate(object);
        assertEquals(1, errors.size());
        checkError(errors.get(0), error, node, message);
    }

    /**
     * Verifies a validation error matches that expected.
     *
     * @param error   the error to check
     * @param object  the expected object
     * @param node    the expected node
     * @param message the expected message
     */
    private void checkError(ValidationError error, IMObject object, String node, String message) {
        assertEquals(object.getArchetype(), error.getArchetype());
        assertEquals(node, error.getNode());
        assertEquals(message, error.getMessage());
        assertEquals(object.getObjectReference(), error.getReference());
    }
}