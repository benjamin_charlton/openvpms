/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link ListenerRegistrations} class.
 *
 * @author Tim Anderson
 */
public class ListenerRegistrationsTestCase {

    /**
     * Tests registration for a single archetype.
     */
    @Test
    public void testSingleArchetypeRegistration() {
        ListenerRegistrations registrations = new ListenerRegistrations();
        IArchetypeServiceListener listener1 = Mockito.mock(IArchetypeServiceListener.class);
        IArchetypeServiceListener listener2 = Mockito.mock(IArchetypeServiceListener.class);

        check(registrations, "act.a");
        assertEquals(0, registrations.getRegistrationCount());
        assertEquals(0, registrations.getListenerCount());

        registrations.add("act.a", Collections.singletonList("act.a"), listener1);
        registrations.add("act.a", Collections.singletonList("act.a"), listener2);
        registrations.add("act.b", Collections.singletonList("act.b"), listener1);
        assertEquals(3, registrations.getRegistrationCount());
        assertEquals(3, registrations.getListenerCount());

        check(registrations, "act.a", listener1, listener2);
        check(registrations, "act.b", listener1);

        registrations.remove("act.a", listener1);
        check(registrations, "act.a", listener2);
        assertEquals(2, registrations.getRegistrationCount());
        assertEquals(2, registrations.getListenerCount());

        registrations.remove("act.b", listener1);
        check(registrations, "act.a", listener2);
        check(registrations, "act.b");
        assertEquals(1, registrations.getRegistrationCount());
        assertEquals(1, registrations.getListenerCount());

        registrations.remove("act.a", listener2);
        assertEquals(0, registrations.getRegistrationCount());
        assertEquals(0, registrations.getListenerCount());
    }

    /**
     * Verifies that wildcard registrations include new archetypes.
     */
    @Test
    public void testWildcardArchetypeRegistration() {
        ListenerRegistrations registrations = new ListenerRegistrations();
        IArchetypeServiceListener listener = Mockito.mock(IArchetypeServiceListener.class);

        check(registrations, "act.a");
        registrations.add("act.*", Arrays.asList("act.a", "act.b"), listener);
        assertEquals(1, registrations.getRegistrationCount());
        assertEquals(2, registrations.getListenerCount());

        check(registrations, "act.a", listener);
        check(registrations, "act.b", listener);
        check(registrations, "act.c");                  // archetype not registered

        registrations.archetypeAdded("act.c");
        check(registrations, "act.c", listener);
        assertEquals(1, registrations.getRegistrationCount());
        assertEquals(3, registrations.getListenerCount());
    }

    /**
     * Verifies listeners for an archetype match that expected.
     *
     * @param registrations the registrations
     * @param archetype     the archetype
     * @param expected      the expected listeners
     */
    private void check(ListenerRegistrations registrations, String archetype, IArchetypeServiceListener... expected) {
        IArchetypeServiceListener[] actual = registrations.get(archetype);
        if (actual == null) {
            assertEquals(0, expected.length);
        } else {
            assertArrayEquals(expected, actual);
        }
    }
}
