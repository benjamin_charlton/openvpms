/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Test;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.business.domain.im.product.ProductPrice;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.business.service.archetype.AuditableTestHelper.AuditableState;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link Product} class in conjunction with the {@link IArchetypeService}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("archetype-service-appcontext.xml")
public class ArchetypeServiceProductTestCase extends AbstractArchetypeServiceTest {

    /**
     * Verifies that the create/update information is populated.
     */
    @Test
    public void testCreateUpdate() {
        AuditableTestHelper auditor = new AuditableTestHelper(getArchetypeService());

        User user1 = createUser();
        User user2 = createUser();
        save(user1);
        save(user2);

        Product product = (Product) create("product.product");
        product.setName("XProduct" + System.currentTimeMillis());
        auditor.checkSave(product, user1);

        // verify create/update information doesn't change when the object is saved without being modified
        auditor.checkSaveUnchanged(product, user2);

        // change an attribute and verify the create/update details are propagated back to the product
        product.setDescription("a description");
        auditor.checkSave(product, user2);

        // verify the object updates if a relationship is added
        ProductPrice price1 = (ProductPrice) create("productPrice.margin");
        auditor.checkNull(price1);
        Date preSave = new Date();
        AuditableState priceState = new AuditableState(price1);

        price1.setPrice(BigDecimal.TEN);
        product.addProductPrice(price1);
        Product saved = auditor.checkSave(product, user1);
        assertEquals(1, saved.getProductPrices().size());

        // verify the price was created.
        priceState = priceState.checkCreate(price1, preSave, user1);

        // modify the price and save the product.
        Date preSave2 = new Date();
        price1.setPrice(BigDecimal.ONE);
        auditor.checkSaveUnchanged(product, user2);  // NOTE: the product shouldn't change, just the price

        // verify the price was updated
        priceState.checkUpdate(price1, preSave2, user2);
    }
}
