/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Test;
import org.openvpms.component.business.domain.im.common.EntityLink;
import org.openvpms.component.business.domain.im.common.EntityRelationship;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.business.service.lookup.LookupUtil;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.springframework.test.context.ContextConfiguration;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


/**
 * Test that ability to create and query parties.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
@ContextConfiguration("archetype-service-appcontext.xml")
public class ArchetypeServicePartyTestCase extends AbstractArchetypeServiceTest {

    /**
     * Test the creation of a simple contact with a contact classification.
     */
    @Test
    public void testSimplePartyWithContactCreation() {
        Lookup classification = createContactPurpose("EMAIL");
        save(classification);
        Lookup classification1 = createContactPurpose("HOME");
        save(classification1);

        Party person = createPerson("MR", "Jim", "Alateras");
        person.addContact(createContact(classification));
        person.addContact(createContact(classification1));
        save(person);

        Party person2 = (Party) get(person.getObjectReference());
        assertNotNull(person2);
        assertEquals(2, person.getContacts().size());
    }

    /**
     * Verifies that the create/update information is populated.
     */
    @Test
    public void testCreateUpdate() {
        AuditableTestHelper auditor = new AuditableTestHelper(getArchetypeService());
        User user1 = createUser();
        User user2 = createUser();
        save(user1);
        save(user2);

        Party party = createPerson("MR", "Foo", "Bar");
        auditor.checkSave(party, user1);

        // verify create/update information doesn't change when the object is saved without being modified
        auditor.checkSaveUnchanged(party, user2);

        // change an attribute and verify the create/update details are propagated back to the party
        party.getDetails().put("title", "DR");
        auditor.checkSave(party, user2);

        // verify the object updates if a relationship is added
        party.addContact(createContact(createContactPurpose("HOME")));
        Party saved = auditor.checkSave(party, user1);
        assertEquals(1, saved.getContacts().size());
    }

    /**
     * Tests party removal.
     */
    @Test
    public void testRemove() {
        checkRemove(false);
    }

    /**
     * Tests party removal by reference.
     */
    @Test
    public void testRemoveByReference() {
        checkRemove(true);
    }

    /**
     * Verifies that classifications can be added and removed from a party,
     * and that removal doesn't delete the classification itself.
     */
    @Test
    public void testAddRemoveClassifications() {
        Lookup staff1 = createStaff("STAFF1");
        Lookup staff2 = createStaff("STAFF2");
        save(staff1);
        save(staff2);

        Party person = createPerson("MR", "Jim", "Alateras");
        assertEquals(0, person.getClassifications().size());

        person.addClassification(staff1);
        assertTrue(person.getClassifications().contains(staff1));
        save(person);

        person.removeClassification(staff1);
        assertFalse(person.getClassifications().contains(staff1));
        person.addClassification(staff2);
        save(person);

        person = (Party) get(person.getObjectReference());
        assertEquals(1, person.getClassifications().size());
        assertTrue(person.getClassifications().contains(staff2));
        assertEquals(staff1, get(staff1.getObjectReference()));
        assertEquals(staff2, get(staff2.getObjectReference()));
    }

    /**
     * Verifies that multiple parties can be saved via the
     * {@link IArchetypeService#save(Collection<IMObject>)} method.
     */
    @Test
    public void testSaveCollection() {
        Lookup classification = createContactPurpose("EMAIL");
        save(classification);
        Lookup classification1 = createContactPurpose("HOME");
        save(classification1);

        Party person1 = createPerson("MR", "Jim", "Alateras");
        person1.addContact(createContact(classification));
        person1.addContact(createContact(classification1));

        Party person2 = createPerson("MR", "Tim", "Anderson");
        person1.addContact(createContact(classification));
        person1.addContact(createContact(classification1));

        // check the initial values of the ids
        assertEquals(-1, person1.getId());
        assertEquals(-1, person2.getId());

        // save the collection
        Collection<Party> col = Arrays.asList(person1, person2);
        save(col);

        // verify the ids have updated
        assertNotEquals(person1.getId(), -1);
        assertNotEquals(person2.getId(), -1);
        assertEquals(0, person1.getVersion());
        assertEquals(0, person2.getVersion());

        // verify the versions don't update until a change is made
        save(col);
        assertEquals(0, person1.getVersion());
        assertEquals(0, person2.getVersion());

        // update person1 and recheck versions after save
        person1.getDetails().put("lastName", "Foo");
        save(col);
        assertEquals(1, person1.getVersion());
        assertEquals(0, person2.getVersion());
    }

    /**
     * Verifies that classifications can be added and removed from a contact,
     * and that removal doesn't delete the classification itself.
     */
    @Test
    public void testAddRemoveContactClassifications() {
        Lookup email = createContactPurpose("EMAIL");
        save(email);
        Lookup home = createContactPurpose("HOME");
        save(home);

        Contact contact = createContact(email);
        save(contact);

        contact.removeClassification(email);
        contact.addClassification(home);
        save(contact);

        contact = (Contact) get(contact.getObjectReference());
        assertEquals(1, contact.getClassifications().size());
        assertTrue(contact.getClassifications().contains(home));
        assertEquals(email, get(email.getObjectReference()));
        assertEquals(home, get(home.getObjectReference()));
    }

    /**
     * Verifies that the target of an {@link EntityRelationship} can be removed, and that removal of the source
     * of an {@link EntityRelationship} doesn't cascade to the target.
     */
    @Test
    public void testAddRemoveEntityRelationships() {
        checkAddRemoveEntityRelationships(false);
    }

    /**
     * Verifies that the target of an {@link EntityRelationship} can be removed by reference, and that removal of the
     * source of an {@link EntityRelationship} doesn't cascade to the target.
     */
    @Test
    public void testAddRemoveEntityRelationshipsByReference() {
        checkAddRemoveEntityRelationships(true);
    }

    /**
     * Verifies that the target of an {@link EntityLink} cannot be removed, and that removal of the source
     * of an {@link EntityLink} doesn't cascade to the target.
     */
    @Test
    public void testAddRemoveEntityLinks() {
        checkAddRemoveEntityLinks(false);
    }

    /**
     * Verifies that the target of an {@link EntityLink} cannot be removed, and that removal of the source
     * of an {@link EntityLink} doesn't cascade to the target.
     */
    @Test
    public void testAddRemoveEntityLinksByReference() {
        checkAddRemoveEntityLinks(true);
    }

    /**
     * Verifies that the target of an {@link EntityRelationship} can be removed, and that removal of the source
     * of an {@link EntityRelationship} doesn't cascade to the target.
     */
    private void checkAddRemoveEntityRelationships(boolean byReference) {
        Party person = createPerson("MR", "Tim", "Anderson");
        Party pet1 = createPet();
        Party pet2 = createPet();
        IMObjectBean bean = getBean(person);
        bean.addTarget("patients", "entityRelationship.patientOwner", pet1, "customers");
        bean.addTarget("patients", "entityRelationship.patientOwner", pet2, "customers");
        save(person, pet1, pet2);

        // pet1 can be removed even though it is linked to person
        remove(pet1, byReference);
        assertNull(get(pet1));

        person = get(person);  // need to reload as EntityDeleteHandler doesn't propagate changes
        assertNotNull(person);

        // now remove the source
        remove(person, byReference);
        assertNull(get(person));

        // verify it hasn't cascaded
        pet2 = get(pet2);
        assertNotNull(pet2);
    }

    /**
     * Verifies that the target of an {@link EntityLink} cannot be removed, and that removal of the source
     * of an {@link EntityLink} doesn't cascade to the target.
     */
    private void checkAddRemoveEntityLinks(boolean byReference) {
        Party person = createPerson("MR", "Tim", "Anderson");
        Party location = createLocation();
        IMObjectBean bean = getBean(person);
        bean.addTarget("location", location);
        save(person, location);

        // verify that the location cannot be removed as it linked from the person
        try {
            remove(location, byReference);
            fail("Expected removal of EntityLink target to fail");
        } catch (ArchetypeServiceException expected) {
            assertEquals("Failed to delete object with reference " + location.getObjectReference(),
                         expected.getMessage());
        }

        // now remove the source
        remove(person, byReference);
        assertNull(get(person));

        // verify it hasn't cascaded
        assertNotNull(get(location));

        // now remove the target
        remove(location, byReference);
        assertNull(get(location));
    }

    /**
     * Tests party removal.
     *
     * @param byReference if {@code true}, use {@link IArchetypeService#remove(Reference)} else use
     *                    {@link IArchetypeService#remove(org.openvpms.component.model.object.IMObject)}
     */
    private void checkRemove(boolean byReference) {
        Lookup classification = createContactPurpose("HOME");
        save(classification);
        Party person = createPerson("MR", "Jim", "Alateras");
        Contact contact = createContact(classification);
        save(contact);
        person.addContact(contact);
        save(person);
        assertNotNull(get(person.getObjectReference()));
        assertNotNull(get(contact.getObjectReference()));

        // invalidate the object. Shouldn't prevent its removal
        person.getDetails().put("lastName", null);

        try {
            validateObject(person);
            fail("Expected the party to be invalid");
        } catch (ValidationException ignore) {
            // expected behaviour
        }

        // now remove it, and verify the associated contact has also been removed
        remove(person, byReference);
        assertNull(get(person.getObjectReference()));
        assertNull(get(contact.getObjectReference()));
    }

    /**
     * Create a person with the specified title, firstName and lastName.
     *
     * @param title     the title
     * @param firstName the first name
     * @param lastName  the last name
     * @return a new person
     */
    private Party createPerson(String title, String firstName, String lastName) {
        Party person = (Party) create("party.customerperson");
        person.getClassifications().clear();
        person.getDetails().put("lastName", lastName);
        person.getDetails().put("firstName", firstName);
        person.getDetails().put("title", title);

        return person;
    }

    /**
     * Creates a new party.organisationLocation.
     *
     * @return a new location
     */
    private Party createLocation() {
        Party location = (Party) create("party.organisationLocation");
        location.setName("ZLocation");
        return location;
    }


    /**
     * Creates a new party.patientpet.
     *
     * @return a new pet
     */
    private Party createPet() {
        Party pet = (Party) create("party.patientpet");
        pet.setName("ZFido");
        IMObjectBean bean = getBean(pet);
        bean.setValue("species", "CANINE");
        bean.setValue("breed", "PUG");
        return pet;
    }


    /**
     * Create a contact with the specified classification.
     *
     * @param classification the classification
     * @return a new contact
     */
    private Contact createContact(Lookup classification) {
        Contact contact = (Contact) create("contact.location");

        contact.getDetails().put("address", "kalulu rd");
        contact.getDetails().put("suburb", "Belgrave");
        contact.getDetails().put("postcode", "3160");
        contact.getDetails().put("state", "VIC");
        contact.getDetails().put("country", "AU");
        contact.addClassification(classification);

        return contact;
    }

    /**
     * Creates a lookup with the specified code.
     *
     * @param code the code of the lookup
     * @return a new lookup
     */
    private Lookup createStaff(String code) {
        Lookup lookup = LookupUtil.createLookup(getArchetypeService(), "lookup.staff", code);
        lookup.setDescription(code);
        return lookup;
    }

    /**
     * Creates a lookup with the specified code.
     *
     * @param code the code of the lookup
     * @return a new lookup
     */
    private Lookup createContactPurpose(String code) {
        return LookupUtil.createLookup(getArchetypeService(), "lookup.contactPurpose", code);
    }
}
