/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.lookup;

import org.apache.commons.collections4.IteratorUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.business.service.cache.BasicEhcacheManager;
import org.openvpms.component.model.lookup.Lookup;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link CachingLookupService}.
 *
 * @author Tim Anderson
 */
public class CachingLookupServiceTestCase extends AbstractLookupServiceTest {

    /**
     * The cache manager.
     */
    private BasicEhcacheManager cacheManager;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        cacheManager = new BasicEhcacheManager(100);
        CachingLookupService service = new CachingLookupService(getArchetypeService(), getDAO(), cacheManager);
        setLookupService(service);
    }

    /**
     * Cleans up after the test
     */
    @After
    public void tearDown() {
        ((CachingLookupService) getLookupService()).destroy();
        cacheManager.destroy();
    }

    /**
     * Verifies that {@link CachingLookupService#getLookups(String)} returns all lookups even if the cache is full.
     * <p/>
     * Every lookup archetype takes one cache key, and each lookup is stored by code and reference.
     */
    @Test
    public void testGetLookupsForFullCache() {
        removeLookups("lookup.diagnosis");
        removeLookups("lookup.diagnosisVeNom");

        BasicEhcacheManager cacheManager = new BasicEhcacheManager(5);
        CachingLookupService service = new CachingLookupService(getArchetypeService(), getDAO(), cacheManager);
        try {
            checkCacheSize(0, service);

            Lookup lookup1 = createLookup("lookup.diagnosis", "FIBROMA");
            save(lookup1);
            checkCacheSize(2, service);  // entry for key and reference

            Lookup lookup2 = createLookup("lookup.diagnosis", "HEART_MURMUR");
            save(lookup2);
            checkCacheSize(4, service);  // entry for key and reference

            Lookup lookup3 = createVeNomLookup();
            save(lookup3);
            checkCacheSize(5, service);  // cache now full - something discarded

            // verify individual lookups can be retrieved
            checkLookup(lookup1, service);
            checkLookup(lookup2, service);
            checkLookup(lookup3, service);

            // ... and by collection
            checkLookups(service, "lookup.diagnosis", lookup1, lookup2);

            checkCacheSize(5, service);

            checkLookups(service, "lookup.diagnosisVeNom", lookup3);
            checkLookups(service, "lookup.diagnosis", lookup1, lookup2);
        } finally {
            service.destroy();
            cacheManager.destroy();
        }
    }

    /**
     * Verifies a lookup can be retrieved by code and by reference.
     *
     * @param lookup  the lookup
     * @param service the lookup service
     */
    private void checkLookup(Lookup lookup, CachingLookupService service) {
        assertEquals(lookup, service.getLookup(lookup.getArchetype(), lookup.getCode()));
        assertEquals(lookup, service.getLookup(lookup.getObjectReference()));
    }

    /**
     * Verifies the expected lookups are present for an archetype.
     *
     * @param service   the lookup service
     * @param archetype the lookup archetype
     * @param lookups   the expected lookups
     */
    private void checkLookups(CachingLookupService service, String archetype, Lookup... lookups) {
        Collection<Lookup> list = service.getLookups(archetype);
        assertEquals(lookups.length, list.size());
        for (Lookup lookup : lookups) {
            assertTrue(list.contains(lookup));
        }
    }

    /**
     * Verifies the expected no. of elements are in the cache.
     *
     * @param count   the expected no. of elements
     * @param service the lookup service
     */
    private void checkCacheSize(int count, CachingLookupService service) {
        assertEquals(count, IteratorUtils.size(service.getCache().iterator()));
    }

}
