package org.openvpms.component.system.common.crypto;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.component.security.crypto.PasswordEncryptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Tests the {@link DefaultPasswordEncryptorFactory}.
 *
 * @author Tim Anderson
 */
public class DefaultPasswordEncryptorFactoryTestCase {


    /**
     * Tests the factory.
     */
    @Test
    public void testFactory() {
        String key1 = KeyGenerator.generate(10, 10); // key password is 10 characters
        check(key1);

        String key2 = KeyGenerator.generate(20, 30); // key password is between 20 and 30 characters
        check(key2);
    }

    /**
     * Tests the {@link DefaultPasswordEncryptorFactory}.
     *
     * @param key the key
     */
    private void check(String key) {
        DefaultPasswordEncryptorFactory factory = new DefaultPasswordEncryptorFactory(key);
        PasswordEncryptor encryptor = factory.create();

        for (int i = 1; i < 16; ++i) {
            String password = StringUtils.repeat("x", i);  // make sure passwords of different lengths are supported

            String encrypted1 = encryptor.encrypt(password);
            assertEquals(password, encryptor.decrypt(encrypted1));

            // verify that encrypting the same password again yields a different encryption
            String encrypted2 = encryptor.encrypt(password);
            assertNotEquals(encrypted1, encrypted2);
            assertEquals(password, encryptor.decrypt(encrypted2));
        }
    }

}
