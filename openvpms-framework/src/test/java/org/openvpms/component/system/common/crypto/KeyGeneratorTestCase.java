package org.openvpms.component.system.common.crypto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link KeyGenerator}.
 *
 * @author Tim Anderson
 */
public class KeyGeneratorTestCase {

    /**
     * Tests key generation.
     */
    @Test
    public void test() {
        String key1 = KeyGenerator.generate(10, 10); // password is 10 characters
        String[] decode1 = KeyGenerator.decode(key1);
        assertEquals(2, decode1.length);

        // verify the salt is hexadecimal
        String salt1 = decode1[0];
        assertTrue(salt1.matches("\\p{XDigit}+"));

        // check password length
        String pass1 = decode1[1];
        assertEquals(10, pass1.length());

        String key2 = KeyGenerator.generate(20, 30); // password between 20 and 30 characters
        String[] decode2 = KeyGenerator.decode(key2);
        assertEquals(2, decode2.length);

        // verify the salt is hexadecimal
        String salt2 = decode2[0];
        assertTrue(salt2.matches("\\p{XDigit}+"));

        // check password length
        String pass2 = decode2[1];
        assertTrue(pass2.length() >= 20 && pass2.length() <= 30);
    }
}
