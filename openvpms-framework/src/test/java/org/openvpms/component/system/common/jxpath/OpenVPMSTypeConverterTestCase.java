/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.jxpath;

import org.junit.Test;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.system.common.util.ClassHelper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link OpenVPMSTypeConverter}.
 *
 * @author Tim Anderson
 */
public class OpenVPMSTypeConverterTestCase {

    /**
     * The converter.
     */
    private final OpenVPMSTypeConverter converter = new OpenVPMSTypeConverter();

    /**
     * Tests conversion to BigDecimal.
     */
    @Test
    public void testConvertToBigDecimal() {
        checkConversion(null, BigDecimal.class, null);
        checkConversion("10", BigDecimal.class, BigDecimal.TEN);
        checkConversion((byte) 1, BigDecimal.class, BigDecimal.ONE);
        checkConversion((short) 10, BigDecimal.class, BigDecimal.TEN);
        checkConversion(1, BigDecimal.class, BigDecimal.ONE);
        checkConversion(10L, BigDecimal.class, BigDecimal.TEN);
        checkConversion(1.0f, BigDecimal.class, BigDecimal.ONE);
        checkConversion(10.0d, BigDecimal.class, BigDecimal.TEN);
        checkConversion(BigInteger.ONE, BigDecimal.class, BigDecimal.ONE);
        checkConversion(BigDecimal.TEN, BigDecimal.class, BigDecimal.TEN);
        checkConversion(Money.ONE, BigDecimal.class, BigDecimal.ONE);
    }

    /**
     * Test conversion from BigDecimal.
     */
    @Test
    public void testConvertFromBigDecimal() {
        checkConversion(BigDecimal.ONE, String.class, "1");
        checkConversion(BigDecimal.TEN, Byte.class, (byte) 10);
        checkConversion(BigDecimal.ONE, Short.class, (short) 1);
        checkConversion(BigDecimal.TEN, Integer.class, 10);
        checkConversion(BigDecimal.ONE, Long.class, 1L);
        checkConversion(BigDecimal.TEN, Float.class, 10.0f);
        checkConversion(BigDecimal.ONE, Double.class, 1.0d);
        checkConversion(BigDecimal.TEN, BigInteger.class, BigInteger.TEN);
        checkConversion(BigDecimal.ONE, BigDecimal.class, BigDecimal.ONE);
        checkConversion(BigDecimal.ONE, Money.class, Money.ONE);
    }

    /**
     * Tests conversion to BigInteger.
     */
    @Test
    public void testConvertToBigInteger() {
        checkConversion(null, BigInteger.class, null);
        checkConversion("10", BigInteger.class, BigInteger.TEN);
        checkConversion((byte) 1, BigInteger.class, BigInteger.ONE);
        checkConversion((short) 10, BigInteger.class, BigInteger.TEN);
        checkConversion(1, BigInteger.class, BigInteger.ONE);
        checkConversion(10L, BigInteger.class, BigInteger.TEN);
        checkConversion(1.0f, BigInteger.class, BigInteger.ONE);
        checkConversion(10.0d, BigInteger.class, BigInteger.TEN);
        checkConversion(BigInteger.ONE, BigInteger.class, BigInteger.ONE);
        checkConversion(BigDecimal.TEN, BigInteger.class, BigInteger.TEN);
        checkConversion(Money.ONE, BigInteger.class, BigInteger.ONE);
    }

    /**
     * Test conversion from BigInteger.
     */
    @Test
    public void testConvertFromBigInteger() {
        checkConversion(BigInteger.ONE, String.class, "1");
        checkConversion(BigInteger.TEN, Byte.class, (byte) 10);
        checkConversion(BigInteger.ONE, Short.class, (short) 1);
        checkConversion(BigInteger.TEN, Integer.class, 10);
        checkConversion(BigInteger.ONE, Long.class, 1L);
        checkConversion(BigInteger.TEN, Float.class, 10.0f);
        checkConversion(BigInteger.ONE, Double.class, 1.0d);
        checkConversion(BigInteger.TEN, BigInteger.class, BigInteger.TEN);
        checkConversion(BigInteger.ONE, BigDecimal.class, BigDecimal.ONE);
        checkConversion(BigInteger.ONE, Money.class, Money.ONE);
    }

    /**
     * Tests conversion to Money.
     */
    @Test
    public void testConvertToMoney() {
        checkConversion(null, Money.class, null);
        checkConversion("10", Money.class, Money.TEN);
        checkConversion((byte) 1, Money.class, Money.ONE);
        checkConversion((short) 10, Money.class, Money.TEN);
        checkConversion(1, Money.class, Money.ONE);
        checkConversion(10L, Money.class, Money.TEN);
        checkConversion(1.0f, Money.class, Money.ONE);
        checkConversion(10.0d, Money.class, Money.TEN);
        checkConversion(BigInteger.ONE, Money.class, Money.ONE);
        checkConversion(BigDecimal.TEN, Money.class, Money.TEN);
        checkConversion(Money.ONE, Money.class, Money.ONE);
    }

    /**
     * Test conversion from Money.
     */
    @Test
    public void testConvertFromMoney() {
        checkConversion(Money.ONE, String.class, "1");
        checkConversion(Money.TEN, Byte.class, (byte) 10);
        checkConversion(Money.ONE, Short.class, (short) 1);
        checkConversion(Money.TEN, Integer.class, 10);
        checkConversion(Money.ONE, Long.class, 1L);
        checkConversion(Money.TEN, Float.class, 10.0f);
        checkConversion(Money.ONE, Double.class, 1.0d);
        checkConversion(Money.TEN, BigInteger.class, BigInteger.TEN);
        checkConversion(Money.ONE, BigDecimal.class, BigDecimal.ONE);
        checkConversion(Money.ONE, Money.class, Money.ONE);
    }

    /**
     * Verifies that conversion to and from Money instances works if the conversion is invoked in from a different
     * classloader. This tests the fix for OBF-282.
     */
    @Test
    public void testConversionFromDifferentContextClassLoader() {
        // invoke with the context class loader set to that of the converter
        ClassLoader classLoader = converter.getClass().getClassLoader();
        ClassHelper.invoke(classLoader, () -> {
            checkConversion(Money.ONE, String.class, "1");
            checkConversion("10", Money.class, Money.TEN);
        });
        // invoke with the context class loader set to that of the parent class loader
        ClassHelper.invoke(classLoader.getParent(), () -> {
            checkConversion(Money.ONE, String.class, "1");
            checkConversion("10", Money.class, Money.TEN);
        });
    }

    /**
     * Tests date conversion.
     * <p/>
     * NOTE: Date -> String conversion uses toString() which is not particularly robust, but isn't used anywhere
     * outside of testing. TODO
     */
    @Test
    public void testDateConversion() {
        Date date1 = new Date();
        Date date2 = (Date) new UtilDateConverter().convert(Date.class, "2021-08-04 12:53:00 GMT+11");
        assertNotNull(date2);
        checkConversion(date1, Date.class, date1);
        checkConversion("2021-08-04 12:53:00 GMT+11", Date.class, date2);
        checkConversion(date2, String.class, date2.toString());
        checkConversion(null, Date.class, null);
    }

    /**
     * Verifies conversion matches that expected.
     *
     * @param from     the value to convert from
     * @param to       the type to convert to
     * @param expected the expected value. May be {@code null}
     */
    @SuppressWarnings("unchecked")
    private <T extends Comparable<?>> void checkConversion(Object from, Class<T> to, Object expected) {
        Object value = converter.convert(from, to);
        if (expected == null) {
            assertNull(value);
        } else {
            assertEquals(0, ((Comparable<T>) expected).compareTo((T) value));
        }
    }
}