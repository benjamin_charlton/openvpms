/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.common.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link Predicates} class.
 *
 * @author Tim Anderson
 */
public class PredicatesTestCase {

    /**
     * Tests the {@link Predicates#activeNow()}, {@link Predicates#active(Date, Date)} and
     * {@link Predicates#activeAt(Date)} method.
     */
    @Test
    public void testActive() {
        Date now = new Date();
        Date yesterday = DateUtils.addDays(now, -1);
        Date tomorrow = DateUtils.addDays(now, 1);
        EntityRelationship relationship1 = createRelationship((Date) null, null);
        EntityRelationship relationship2 = createRelationship(yesterday, null);
        EntityRelationship relationship3 = createRelationship(null, yesterday);
        EntityRelationship relationship4 = createRelationship(yesterday, tomorrow);
        EntityRelationship relationship5 = createRelationship(tomorrow, null);

        assertTrue(Predicates.activeNow().test(relationship1));
        assertTrue(Predicates.activeNow().test(relationship2));
        assertFalse(Predicates.activeNow().test(relationship3));
        assertTrue(Predicates.activeNow().test(relationship4));
        assertFalse(Predicates.activeNow().test(relationship5));

        assertTrue(Predicates.active(yesterday, tomorrow).test(relationship1));
        assertFalse(Predicates.active(null, yesterday).test(relationship2));
        assertTrue(Predicates.active(yesterday, null).test(relationship2));

        assertTrue(Predicates.activeAt(now).test(relationship1));
        assertTrue(Predicates.activeAt(now).test(relationship2));
        assertFalse(Predicates.activeAt(now).test(relationship3));

        // test equality
        assertEquals(Predicates.activeNow(), Predicates.activeNow());

        assertEquals(Predicates.active(yesterday, tomorrow), Predicates.active(yesterday, tomorrow));
        assertNotEquals(Predicates.active(yesterday, tomorrow), Predicates.active(yesterday, now));

        assertEquals(Predicates.activeAt(now), Predicates.activeAt(now));
        assertNotEquals(Predicates.activeAt(now), Predicates.activeAt(tomorrow));
    }

    /**
     * Tests the {@link Predicates#sourceEquals(IMObject)}, {@link Predicates#sourceEquals(Reference)},
     * {@link Predicates#targetEquals(IMObject)} and {@link Predicates#targetEquals(Reference)} methods.
     */
    @Test
    public void testSourceTargetEquals() {
        IMObject customer = createObject("party.customerperson");
        IMObject patient = createObject("party.patientpet");
        EntityRelationship relationship = createRelationship(customer, patient);

        assertTrue(Predicates.sourceEquals(customer).test(relationship));
        assertTrue(Predicates.sourceEquals(customer.getObjectReference()).test(relationship));
        assertFalse(Predicates.sourceEquals(patient).test(relationship));
        assertFalse(Predicates.sourceEquals(patient.getObjectReference()).test(relationship));

        assertTrue(Predicates.targetEquals(patient).test(relationship));
        assertTrue(Predicates.targetEquals(patient.getObjectReference()).test(relationship));
        assertFalse(Predicates.targetEquals(customer).test(relationship));
        assertFalse(Predicates.targetEquals(customer.getObjectReference()).test(relationship));

        // test equality
        assertEquals(Predicates.sourceEquals(customer), Predicates.sourceEquals(customer));
        assertEquals(Predicates.sourceEquals(customer), Predicates.sourceEquals(customer.getObjectReference()));
        assertNotEquals(Predicates.sourceEquals(customer), Predicates.sourceEquals(patient));
        assertNotEquals(Predicates.sourceEquals(customer), Predicates.sourceEquals(patient.getObjectReference()));

        assertEquals(Predicates.targetEquals(customer), Predicates.targetEquals(customer));
        assertEquals(Predicates.targetEquals(customer), Predicates.targetEquals(customer.getObjectReference()));
        assertNotEquals(Predicates.targetEquals(customer), Predicates.targetEquals(patient));
        assertNotEquals(Predicates.targetEquals(customer), Predicates.targetEquals(patient.getObjectReference()));
    }

    /**
     * Tests the {@link Predicates#sourceIsA(String...)} and {@link Predicates#targetIsA(String...)}.
     */
    @Test
    public void testSourceTargetIsA() {
        IMObject customer = createObject("party.customerperson");
        IMObject patient = createObject("party.patientpet");
        EntityRelationship relationship = createRelationship(customer, patient);

        assertTrue(Predicates.sourceIsA("party.customerperson").test(relationship));
        assertTrue(Predicates.sourceIsA("party.patientpet", "party.customerperson").test(relationship));
        assertTrue(Predicates.sourceIsA("party.customer*").test(relationship));
        assertFalse(Predicates.sourceIsA("party.patientpet").test(relationship));
        assertFalse(Predicates.sourceIsA("party.patient*").test(relationship));

        assertTrue(Predicates.targetIsA("party.patientpet").test(relationship));
        assertTrue(Predicates.targetIsA("party.customerperson", "party.patientpet").test(relationship));
        assertTrue(Predicates.targetIsA("party.patient*").test(relationship));
        assertFalse(Predicates.targetIsA("party.customerperson").test(relationship));
        assertFalse(Predicates.targetIsA("party.customer*").test(relationship));

        // test equality
        assertEquals(Predicates.sourceIsA("party.patientpet"), Predicates.sourceIsA("party.patientpet"));
        assertEquals(Predicates.sourceIsA("party.patientpet", "party.customerperson"),
                     Predicates.sourceIsA("party.patientpet", "party.customerperson"));
        assertEquals(Predicates.sourceIsA("party.patient*"), Predicates.sourceIsA("party.patient*"));
        assertNotEquals(Predicates.sourceIsA("party.patientpet"), Predicates.sourceIsA("party.customerperson"));


        assertEquals(Predicates.targetIsA("party.patientpet"), Predicates.targetIsA("party.patientpet"));
        assertEquals(Predicates.targetIsA("party.patientpet", "party.customerperson"),
                     Predicates.targetIsA("party.patientpet", "party.customerperson"));
        assertEquals(Predicates.targetIsA("party.patient*"), Predicates.targetIsA("party.patient*"));
        assertNotEquals(Predicates.targetIsA("party.patientpet"), Predicates.targetIsA("party.customerperson"));

        // NOTE - order is important. For current purposes don't need to be concerned with arbitrary ordering
        assertNotEquals(Predicates.sourceIsA("party.patientpet", "party.customerperson"),
                        Predicates.sourceIsA("party.customerperson", "party.patientpet"));
        assertNotEquals(Predicates.targetIsA("party.patientpet", "party.customerperson"),
                        Predicates.targetIsA("party.customerperson", "party.patientpet"));
    }

    /**
     * Tests the {@link Predicates#isA(String...)} method.
     */
    @Test
    public void testIsA() {
        IMObject object = createObject("party.customerperson");
        assertTrue(Predicates.isA("party.customerperson").test(object));
        assertTrue(Predicates.isA("party.patientpet", "party.customerperson").test(object));
        assertTrue(Predicates.isA("party.customer*").test(object));

        assertFalse(Predicates.isA("party.patientpet").test(object));
        assertFalse(Predicates.isA("party.patient*").test(object));

        // test equality
        assertEquals(Predicates.isA("party.customerperson"), Predicates.isA("party.customerperson"));
        assertEquals(Predicates.isA("party.customerperson", "party.patientpet"),
                     Predicates.isA("party.customerperson", "party.patientpet"));
        assertNotEquals(Predicates.isA("party.customerperson"), Predicates.isA("party.patientpet"));
    }

    /**
     * Creates a new relationship.
     *
     * @param source the source
     * @param target the target
     * @return a new relationship
     */
    private EntityRelationship createRelationship(IMObject source, IMObject target) {
        EntityRelationship relationship = new EntityRelationship();
        relationship.setSource(source.getObjectReference());
        relationship.setTarget(target.getObjectReference());
        return relationship;
    }

    /**
     * Creates a new relationship.
     *
     * @param from the from date. May be {@code null}
     * @param to   the to date. May be {@code null}
     * @return a new relationship
     */
    private EntityRelationship createRelationship(Date from, Date to) {
        EntityRelationship relationship = new EntityRelationship();
        relationship.setActiveStartTime(from);
        relationship.setActiveEndTime(to);
        return relationship;
    }

    /**
     * Creates a new object.
     *
     * @param archetype the archetype
     * @return a new object
     */
    private IMObject createObject(String archetype) {
        return new org.openvpms.component.business.domain.im.common.IMObject(new ArchetypeId(archetype));
    }
}