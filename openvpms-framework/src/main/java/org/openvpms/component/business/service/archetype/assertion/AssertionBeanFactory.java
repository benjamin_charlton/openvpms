/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.assertion;

import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.support.StaticListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.ResolvableType;

import java.util.List;

/**
 * An {@link BeanFactory} for {@link AssertionDescriptor} to allow assertions to use a limited set of beans
 * from an {@link ApplicationContext}.
 * <p/>
 * This specifies the beans to export by name, to avoid circular dependency issues when wiring beans.
 *
 * @author Tim Anderson
 */
public class AssertionBeanFactory implements BeanFactory, ApplicationContextAware {

    /**
     * The beans to delegate to.
     */
    private final StaticListableBeanFactory beans = new StaticListableBeanFactory();

    /**
     * The name of the beans to expose.
     */
    private final List<String> names;

    /**
     * The application context.
     */
    private ApplicationContext context;

    /**
     * Determines if the beans have been initialised.
     */
    private boolean initialised;

    /**
     * Constructs an {@link AssertionBeanFactory}.
     *
     * @param names the names of the beans to expose to {@link AssertionDescriptor}
     */
    public AssertionBeanFactory(List<String> names) {
        this.names = names;
    }

    /**
     * Return an instance, which may be shared or independent, of the specified bean.
     *
     * @param name the name of the bean to retrieve
     * @return an instance of the bean
     * @throws NoSuchBeanDefinitionException if there is no bean with the specified name
     * @throws BeansException                if the bean could not be obtained
     */
    @Override
    public Object getBean(String name) throws BeansException {
        return getBeans().getBean(name);
    }

    /**
     * Return an instance, which may be shared or independent, of the specified bean.
     *
     * @param name         the name of the bean to retrieve
     * @param requiredType type the bean must match; can be an interface or superclass
     * @return an instance of the bean
     * @throws NoSuchBeanDefinitionException  if there is no such bean definition
     * @throws BeanNotOfRequiredTypeException if the bean is not of the required type
     * @throws BeansException                 if the bean could not be created
     */
    @Override
    public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
        return getBeans().getBean(name, requiredType);
    }

    /**
     * Return an instance, which may be shared or independent, of the specified bean.
     *
     * @param name the name of the bean to retrieve
     * @param args arguments to use when creating a bean instance using explicit arguments
     *             (only applied when creating a new instance as opposed to retrieving an existing one)
     * @return an instance of the bean
     * @throws NoSuchBeanDefinitionException if there is no such bean definition
     * @throws BeanDefinitionStoreException  if arguments have been given but
     *                                       the affected bean isn't a prototype
     * @throws BeansException                if the bean could not be created
     * @since 2.5
     */
    @Override
    public Object getBean(String name, Object... args) throws BeansException {
        return getBeans().getBean(name, args);
    }

    /**
     * Return the bean instance that uniquely matches the given object type, if any.
     *
     * @param requiredType type the bean must match; can be an interface or superclass
     * @return an instance of the single bean matching the required type
     * @throws NoSuchBeanDefinitionException   if no bean of the given type was found
     * @throws NoUniqueBeanDefinitionException if more than one bean of the given type was found
     * @throws BeansException                  if the bean could not be created
     */
    @Override
    public <T> T getBean(Class<T> requiredType) throws BeansException {
        return getBeans().getBean(requiredType);
    }

    /**
     * Return an instance, which may be shared or independent, of the specified bean.
     *
     * @param requiredType type the bean must match; can be an interface or superclass
     * @param args         arguments to use when creating a bean instance using explicit arguments
     *                     (only applied when creating a new instance as opposed to retrieving an existing one)
     * @return an instance of the bean
     * @throws NoSuchBeanDefinitionException if there is no such bean definition
     * @throws BeanDefinitionStoreException  if arguments have been given but
     *                                       the affected bean isn't a prototype
     * @throws BeansException                if the bean could not be created
     * @since 4.1
     */
    @Override
    public <T> T getBean(Class<T> requiredType, Object... args) throws BeansException {
        return getBeans().getBean(requiredType, args);
    }

    /**
     * Return an provider for the specified bean, allowing for lazy on-demand retrieval
     * of instances, including availability and uniqueness options.
     *
     * @param requiredType type the bean must match; can be an interface or superclass
     * @return a corresponding provider handle
     */
    @Override
    public <T> ObjectProvider<T> getBeanProvider(Class<T> requiredType) {
        return getBeans().getBeanProvider(requiredType);
    }

    /**
     * Return an provider for the specified bean, allowing for lazy on-demand retrieval
     * of instances, including availability and uniqueness options.
     *
     * @param requiredType type the bean must match; can be a generic type declaration.
     *                     Note that collection types are not supported here, in contrast to reflective
     *                     injection points. For programmatically retrieving a list of beans matching a
     *                     specific type, specify the actual bean type as an argument here and subsequently
     *                     use {@link ObjectProvider#orderedStream()} or its lazy streaming/iteration options.
     * @return a corresponding provider handle
     */
    @Override
    public <T> ObjectProvider<T> getBeanProvider(ResolvableType requiredType) {
        return getBeans().getBeanProvider(requiredType);
    }

    /**
     * Does this bean factory contain a bean definition or externally registered singleton
     * instance with the given name?
     *
     * @param name the name of the bean to query
     * @return whether a bean with the given name is present
     */
    @Override
    public boolean containsBean(String name) {
        return getBeans().containsBean(name);
    }

    /**
     * Is this bean a shared singleton? That is, will {@link #getBean} always
     * return the same instance?
     *
     * @param name the name of the bean to query
     * @return whether this bean corresponds to a singleton instance
     * @throws NoSuchBeanDefinitionException if there is no bean with the given name
     */
    @Override
    public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
        return getBeans().isSingleton(name);
    }

    /**
     * Is this bean a prototype? That is, will {@link #getBean} always return
     * independent instances?
     *
     * @param name the name of the bean to query
     * @return whether this bean will always deliver independent instances
     * @throws NoSuchBeanDefinitionException if there is no bean with the given name
     */
    @Override
    public boolean isPrototype(String name) throws NoSuchBeanDefinitionException {
        return getBeans().isPrototype(name);
    }

    /**
     * Check whether the bean with the given name matches the specified type.
     *
     * @param name        the name of the bean to query
     * @param typeToMatch the type to match against (as a {@code ResolvableType})
     * @return {@code true} if the bean type matches, {@code false} if it doesn't match or cannot be determined yet
     * @throws NoSuchBeanDefinitionException if there is no bean with the given name
     */
    @Override
    public boolean isTypeMatch(String name, ResolvableType typeToMatch) throws NoSuchBeanDefinitionException {
        return isTypeMatch(name, typeToMatch);
    }

    /**
     * Check whether the bean with the given name matches the specified type.
     *
     * @param name        the name of the bean to query
     * @param typeToMatch the type to match against (as a {@code Class})
     * @return {@code true} if the bean type matches, {@code false} if it doesn't match or cannot be determined yet
     * @throws NoSuchBeanDefinitionException if there is no bean with the given name
     */
    @Override
    public boolean isTypeMatch(String name, Class<?> typeToMatch) throws NoSuchBeanDefinitionException {
        return getBeans().isTypeMatch(name, typeToMatch);
    }

    /**
     * Determine the type of the bean with the given name.
     *
     * @param name the name of the bean to query
     * @return the type of the bean, or {@code null} if not determinable
     * @throws NoSuchBeanDefinitionException if there is no bean with the given name
     */
    @Override
    public Class<?> getType(String name) throws NoSuchBeanDefinitionException {
        return getBeans().getType(name);
    }

    /**
     * Determine the type of the bean with the given name.
     *
     * @param name                 the name of the bean to query
     * @param allowFactoryBeanInit whether a {@code FactoryBean} may get initialized
     *                             just for the purpose of determining its object type
     * @return the type of the bean, or {@code null} if not determinable
     * @throws NoSuchBeanDefinitionException if there is no bean with the given name
     */
    @Override
    public Class<?> getType(String name, boolean allowFactoryBeanInit) throws NoSuchBeanDefinitionException {
        return getBeans().getType(name, allowFactoryBeanInit);
    }

    /**
     * Return the aliases for the given bean name, if any.
     * All of those aliases point to the same bean when used in a {@link #getBean} call.
     * <p>If the given name is an alias, the corresponding original bean name
     * and other aliases (if any) will be returned, with the original bean name
     * being the first element in the array.
     * <p>Will ask the parent factory if the bean cannot be found in this factory instance.
     *
     * @param name the bean name to check for aliases
     * @return the aliases, or an empty array if none
     */
    @Override
    public String[] getAliases(String name) {
        return getBeans().getAliases(name);
    }

    /**
     * Set the ApplicationContext that this object runs in.
     *
     * @param applicationContext the ApplicationContext object to be used by this object
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.context = applicationContext;
    }

    /**
     * Lazily initialises the bean factory on first access.
     *
     * @return the bean factory
     */
    private synchronized BeanFactory getBeans() {
        if (!initialised) {
            for (String name : names) {
                if (context.containsBean(name)) {
                    beans.addBean(name, context.getBean(name));
                }
            }
            initialised = true;
        }
        return beans;
    }

}
