/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.plugin;

import org.openvpms.component.business.domain.im.common.AuditableIMObject;

/**
 * Plugin data.
 * <p/>
 * Note that plugins must have a unique key and name.
 *
 * @author Tim Anderson
 */
public class Plugin extends AuditableIMObject {

    /**
     * The plugin key.
     */
    private String key;

    /**
     * Returns the plugin key.
     *
     * @return the plugin key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the plugin key.
     * <p/>
     * This must be unique.
     *
     * @param key the plugin key
     */
    public void setKey(String key) {
        this.key = key;
    }
}