/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.dao.im.common.IMObjectDAO;
import org.openvpms.component.business.dao.im.common.IMObjectDAOException;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.Beanable;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.descriptor.cache.IArchetypeDescriptorCache;
import org.openvpms.component.business.service.ruleengine.IRuleEngine;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionTypeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.NodeSet;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.TypedQueryImpl;
import org.openvpms.component.system.common.query.criteria.CriteriaBuilderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * Default implementation of the {@link IArchetypeService} interface.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
public class ArchetypeService implements IArchetypeService {

    /**
     * The listeners..
     */
    private final ListenerRegistrations listeners = new ListenerRegistrations();

    /**
     * The object factory.
     */
    private final IMObjectFactory factory;

    /**
     * The validator.
     */
    private final IMObjectValidator validator;

    /**
     * A reference to the archetype descriptor cache
     */
    private final IArchetypeDescriptorCache dCache;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The criteria builder.
     */
    private final CriteriaBuilderImpl criteriaBuilder;

    /**
     * The DAO instance it will use.
     */
    private IMObjectDAO dao;

    /**
     * The rule engine to use. If not specified then the service does
     * not support rule engine invocations.
     */
    private IRuleEngine ruleEngine;

    /**
     * Define a logger for this class
     */
    private static final Logger log = LoggerFactory.getLogger(ArchetypeService.class);


    /**
     * Constructs a {@link ArchetypeService}.
     *
     * @param cache              the archetype descriptor cache
     * @param transactionManager the transaction manager
     */
    public ArchetypeService(IArchetypeDescriptorCache cache, PlatformTransactionManager transactionManager) {
        dCache = cache;
        this.transactionManager = transactionManager;
        factory = new ObjectFactory();
        validator = new IMObjectValidator(cache);
        criteriaBuilder = new CriteriaBuilderImpl(dCache);
    }

    /**
     * Returns the DAO.
     *
     * @return the dao.
     */
    public IMObjectDAO getDao() {
        return dao;
    }

    /**
     * Sets the DAO.
     *
     * @param dao the dao to set.
     */
    public void setDao(IMObjectDAO dao) {
        this.dao = dao;
    }

    /**
     * Returns the rule engine.
     *
     * @return the rule engine
     */
    public IRuleEngine getRuleEngine() {
        return ruleEngine;
    }

    /**
     * Sets the rule engine.
     *
     * @param ruleEngine the rule engine
     */
    public void setRuleEngine(IRuleEngine ruleEngine) {
        this.ruleEngine = ruleEngine;
    }

    /**
     * Return all archetype names that match that specified.
     *
     * @param archetype   the archetype. May contain a wildcard character
     * @param primaryOnly return only the primary archetypes
     * @return a list of archetypes
     */
    @Override
    public List<String> getArchetypes(String archetype, boolean primaryOnly) {
        return getArchetypeShortNames(archetype, primaryOnly);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getArchetypeDescriptor(java.lang.String)
     */
    @Override
    public org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor
    getArchetypeDescriptor(String shortName) {
        return dCache.getArchetypeDescriptor(shortName);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getAssertionTypeRecord(java.lang.String)
     */
    @Override
    public AssertionTypeDescriptor getAssertionTypeDescriptor(String name) {
        return dCache.getAssertionTypeDescriptor(name);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getAssertionTypeRecords()
     */
    @Override
    public List<AssertionTypeDescriptor> getAssertionTypeDescriptors() {
        return new ArrayList<>(dCache.getAssertionTypeDescriptors());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#createDefaultObject(org.openvpms.component.business.domain.archetype.ArchetypeId)
     */
    @Override
    public IMObject create(ArchetypeId id) {
        log.debug("ArchetypeService.create: Creating object of type {}", id.getShortName());
        return factory.create(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#createDefaultObject(java.lang.String)
     */
    @Override
    public IMObject create(String name) {
        log.debug("ArchetypeService.create: Creating object of type {}", name);
        return factory.create(name);
    }

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @param type      the expected type of the object
     * @return a new object
     * @throws OpenVPMSException  for any error
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    @Override
    public <T extends org.openvpms.component.model.object.IMObject> T create(String archetype, Class<T> type) {
        IMObject result = create(archetype);
        if (result == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.NoArchetypeDefinition, archetype);
        }
        return type.cast(result);
    }

    /**
     * Validate the specified {@link IMObject}. To validate the object it will retrieve the archetype and iterate
     * through the assertions.
     *
     * @param object the object to validate
     * @return any validation errors
     */
    @Override
    public List<org.openvpms.component.service.archetype.ValidationError> validate(
            org.openvpms.component.model.object.IMObject object) {
        return validator.validate(object);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#validateObject(org.openvpms.component.model.object.IMObject)
     */
    @Override
    public void validateObject(org.openvpms.component.model.object.IMObject object) {
        List<org.openvpms.component.service.archetype.ValidationError> errors = validator.validate(object);
        if (!errors.isEmpty()) {
            throw new ValidationException(
                    errors, ValidationException.ErrorCode.FailedToValidObjectAgainstArchetype,
                    new Object[]{object.getArchetype()});

        }
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#deriveValues(org.openvpms.component.business.domain.im.common.IMObject)
     */
    @Override
    public void deriveValues(org.openvpms.component.model.object.IMObject object) {
        // check for a non-null object
        if (object == null) {
            return;
        }

        log.debug("ArchetypeService.deriveValues: Deriving values for type {} with id {} and version {}",
                  object.getArchetype(), object.getId(), object.getVersion());

        // check that we can retrieve a valid archetype for this object
        ArchetypeDescriptor descriptor = getArchetypeDescriptor(object.getArchetype());
        if (descriptor == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.NoArchetypeDefinition,
                                                object.getArchetype());
        }

        Map<String, NodeDescriptor> descriptors
                = (Map<String, NodeDescriptor>) (Map<String, ?>) descriptor.getNodeDescriptorMap();
        if (!descriptors.isEmpty()) {
            JXPathContext context = JXPathHelper.newContext(object);
            deriveValues((IMObject) object, context, descriptors);
        }
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#deriveValue(org.openvpms.component.business.domain.im.common.IMObject, java.lang.String)
     */
    @Override
    public void deriveValue(org.openvpms.component.model.object.IMObject object, String node) {
        if (object == null) {
            throw new ArchetypeServiceException(
                    ArchetypeServiceException.ErrorCode.NonNullObjectRequired);
        }

        if (StringUtils.isEmpty(node)) {
            throw new ArchetypeServiceException(
                    ArchetypeServiceException.ErrorCode.NonNullNodeNameRequired);
        }

        // check that the node name is valid for the specified object
        ArchetypeDescriptor adesc = getArchetypeDescriptor(object.getArchetype());
        if (adesc == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.InvalidArchetypeDescriptor,
                                                object.getArchetype());
        }

        NodeDescriptor ndesc = (NodeDescriptor) adesc.getNodeDescriptor(node);
        if (ndesc == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.InvalidNodeDescriptor,
                                                node, object.getArchetype());
        }

        // derive the value
        ndesc.deriveValue(object);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getArchetypeRecords()
     */
    @Override
    public List<ArchetypeDescriptor> getArchetypeDescriptors() {
        return new ArrayList<>(dCache.getArchetypeDescriptors());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getArchetypeDescriptors(java.lang.String)
     */
    @Override
    public List<ArchetypeDescriptor> getArchetypeDescriptors(String shortName) {
        return new ArrayList<>(dCache.getArchetypeDescriptors(shortName));
    }

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @return the corresponding object, or {@code null} if none is found
     * @throws ArchetypeServiceException if the query fails
     */
    @Override
    public IMObject get(Reference reference) {
        return dao.get(reference);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    @Override
    public <T extends org.openvpms.component.model.object.IMObject> T get(Reference reference, Class<T> type) {
        return type.cast(get(reference));
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public org.openvpms.component.model.object.IMObject get(String archetype, long id) {
        return get(new IMObjectReference(archetype, id));
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    @Override
    public <T extends org.openvpms.component.model.object.IMObject> T get(String archetype, long id, Class<T> type) {
        return type.cast(get(archetype, id));
    }

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding object, or {@code null} if none is found
     * @throws ArchetypeServiceException if the query fails
     */
    @Override
    public IMObject get(Reference reference, boolean active) {
        try {
            return dao.get(reference, active);
        } catch (Exception exception) {
            String message = "select " + reference;
            throw new ArchetypeServiceException(
                    ArchetypeServiceException.ErrorCode.FailedToExecuteQuery, exception, message);
        }
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public org.openvpms.component.model.object.IMObject get(String archetype, long id, boolean active) {
        return get(new IMObjectReference(archetype, id), active);
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#get(org.openvpms.component.system.common.query.ArchetypeQuery)
     */
    public IPage<IMObject> get(IArchetypeQuery query) {
        log.debug("ArchetypeService.get: query {}", query);
        try {
            return dao.get(query);
        } catch (Exception exception) {
            throw new ArchetypeServiceException(
                    ArchetypeServiceException.ErrorCode.FailedToExecuteQuery, exception, query.toString());
        }
    }

    /**
     * Retrieves partially populated objects that match the specified query
     * criteria.<p/>
     * This may be used to selectively load parts of object graphs to improve
     * performance.
     * <p>
     * All simple properties of the returned objects are populated - the
     * <code>nodes</code> argument is used to specify which collection nodes to
     * populate. If empty, no collections will be loaded, and the behaviour of
     * accessing them is undefined.
     *
     * @param query the archetype query
     * @param nodes the collection node names
     * @return a page of objects that match the query criteria
     */
    @Override
    public IPage<IMObject> get(IArchetypeQuery query, Collection<String> nodes) {
        log.debug("ArchetypeService.get: query={}, nodes={}", query, nodes);
        try {
            return dao.get(query, nodes);
        } catch (Exception exception) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToExecuteQuery, exception,
                                                query.toString());
        }
    }

    /**
     * Retrieves the objects matching the query.
     *
     * @param query the archetype query
     * @return a page of objects that match the query criteria
     * @throws ArchetypeServiceException if the query fails
     */
    @Override
    public IPage<ObjectSet> getObjects(IArchetypeQuery query) {
        log.debug("ArchetypeService.getObjects: query={}", query);
        try {
            return dao.getObjects(query);
        } catch (Exception exception) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToExecuteQuery, exception,
                                                query.toString());
        }
    }

    /**
     * Retrieves the nodes from the objects that match the query criteria.
     *
     * @param query the archetype query
     * @param nodes the node names
     * @return the nodes for each object that matches the query criteria
     * @throws ArchetypeServiceException if the query fails
     */
    @Override
    public IPage<NodeSet> getNodes(IArchetypeQuery query, Collection<String> nodes) {
        log.debug("ArchetypeService.get: query={}, nodes={}", query, nodes);
        try {
            return dao.getNodes(query, nodes);
        } catch (Exception exception) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToExecuteQuery, exception,
                                                query.toString());
        }
    }

    /**
     * Returns a builder to create queries.
     *
     * @return the criteria builder
     */
    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return criteriaBuilder;
    }

    /**
     * Creates a {@link TypedQuery} for executing a criteria query.
     *
     * @param query the criteria query
     * @return the new query instance
     */
    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> query) {
        return new TypedQueryImpl<>(dao.createQuery(query), query.getResultType(), dao);
    }

    /**
     * (non-Javadoc)
     *
     * @see IArchetypeService#remove(org.openvpms.component.model.object.IMObject)
     */
    @Override
    public void remove(org.openvpms.component.model.object.IMObject object) {
        IArchetypeServiceListener[] matches = listeners.get(object.getArchetype());
        remove(object, matches);
    }

    /**
     * Removes an object given its reference.
     *
     * @param reference the object reference
     * @throws OpenVPMSException for any error
     */
    @Override
    public void remove(Reference reference) {
        IArchetypeServiceListener[] matches = listeners.get(reference.getArchetype());
        if (matches == null) {
            log.debug("ArchetypeService.remove(Reference): Removing object of type {} with id {}",
                      reference.getArchetype(), reference.getId());
            try {
                dao.delete(reference);
            } catch (IMObjectDAOException exception) {
                if (IMObjectDAOException.ErrorCode.CannotDeleteLookupInUse.equals(exception.getErrorCode())) {
                    throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.CannotDeleteLookupInUse,
                                                        exception, reference);
                }
                throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToDeleteObject,
                                                    exception, reference);
            }
        } else {
            // need to load the IMObject so that listeners can be notified
            org.openvpms.component.model.object.IMObject object = get(reference);
            if (object == null) {
                throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToDeleteObject,
                                                    reference);
            }
            remove(object, matches);
        }
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#save(org.openvpms.component.business.domain.im.common.IMObject)
     */
    @Override
    public void save(org.openvpms.component.model.object.IMObject entity) {
        save(entity, true);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#save(org.openvpms.component.business.domain.im.common.IMObject)
     */
    @Override
    public void save(org.openvpms.component.model.object.IMObject object, boolean validate) {
        log.debug("ArchetypeService.save: Saving object of type {} with id {} and version {}",
                  object.getArchetype(), object.getId(), object.getVersion());

        if (dao == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.NoDaoConfigured);
        }

        notifySave(object, true);

        IMObject unwrapped = unwrap(object);

        if (validate) {
            validateObject(unwrapped);
        }
        try {
            dao.save(unwrapped);
            if (unwrapped instanceof ArchetypeDescriptor || unwrapped instanceof AssertionTypeDescriptor) {
                updateCache(object);
            }
            notifySave(object, false);
        } catch (IMObjectDAOException exception) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToSaveObject, exception,
                                                object);
        }
    }

    /**
     * Save a collection of {@link IMObject} instances.
     *
     * @param objects the objects to insert or update
     * @throws ArchetypeServiceException if an object can't be saved
     * @throws ValidationException       if an object can't be validated
     */
    @Override
    public void save(Collection<? extends org.openvpms.component.model.object.IMObject> objects) {
        save(objects, true);
    }

    /**
     * Save a collection of {@link IMObject} instances.
     *
     * @param objects  the objects to insert or update
     * @param validate whether to validate or not
     * @throws ArchetypeServiceException if an object can't be saved
     * @throws ValidationException       if an object can't be validated
     */
    @Override
    public void save(Collection<? extends org.openvpms.component.model.object.IMObject> objects, boolean validate) {
        if (dao == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.NoDaoConfigured);
        }

        notifySave(objects, true);

        List<IMObject> unwrapped = unwrap(objects);

        // first validate each object
        if (validate) {
            for (org.openvpms.component.model.object.IMObject object : unwrapped) {
                validateObject(object);
            }
        }

        // now issue a call to save the objects
        try {
            dao.save(unwrapped);
            for (IMObject object : unwrapped) {
                if (object instanceof ArchetypeDescriptor || object instanceof AssertionTypeDescriptor) {
                    updateCache(object);
                }
            }
            notifySave(objects, false);
        } catch (IMObjectDAOException exception) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToSaveCollectionOfObjects,
                                                exception, objects.size());
        }
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getArchetypeShortNames(java.lang.String, boolean)
     */
    public List<String> getArchetypeShortNames(String shortName, boolean primaryOnly) {
        return dCache.getArchetypeShortNames(shortName, primaryOnly);
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#getArchetypeShortNames()
     */
    public List<String> getArchetypeShortNames() {
        return dCache.getArchetypeShortNames();
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.service.archetype.IArchetypeService#executeRule(java.lang.String, java.util.Map, java.util.List)
     */
    public List<Object> executeRule(String ruleUri, Map<String, Object> props, List<Object> facts) {
        if (ruleEngine == null) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.RuleEngineNotSupported);
        }
        try {
            return ruleEngine.executeRules(ruleUri, props, facts);
        } catch (Exception exception) {
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToExecuteRule, exception,
                                                ruleUri);
        }
    }

    /**
     * Adds a listener to receive notification of changes.
     * <p>
     * In a transaction, notifications occur on successful commit.
     *
     * @param shortName the archetype short to receive events for. May contain wildcards.
     * @param listener  the listener to add
     */
    public void addListener(String shortName, IArchetypeServiceListener listener) {
        List<String> matches = getArchetypeShortNames(shortName, false);
        listeners.add(shortName, matches, listener);
    }

    /**
     * Removes a listener.
     *
     * @param shortName the archetype short to remove the listener for. May contain wildcards.
     * @param listener  the listener to remove
     */
    public void removeListener(String shortName, IArchetypeServiceListener listener) {
        listeners.remove(shortName, listener);
    }

    /**
     * Returns a bean for an object.
     * <p>
     * NOTE: this method throws an {@link UnsupportedOperationException} for any save() method on the returned bean
     * as these bypass proxies of the service, thus skipping any authority checks.
     *
     * @param object the object
     * @return the bean
     * @throws UnsupportedOperationException if invoked
     */
    @Override
    public IMObjectBean getBean(org.openvpms.component.model.object.IMObject object) {
        return new org.openvpms.component.business.service.archetype.helper.IMObjectBean(object, this) {
            @Override
            public void save() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void save(org.openvpms.component.model.object.IMObject... objects) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void save(Collection<org.openvpms.component.model.object.IMObject> objects) {
                throw new UnsupportedOperationException();
            }
        };
    }

    /**
     * Returns the listeners for an archetype.
     *
     * @param archetype the archetype
     * @return the listeners, or {@code null} if there aren't any
     */
    protected IArchetypeServiceListener[] getListeners(String archetype) {
        return listeners.get(archetype);
    }

    /**
     * Removes an object.
     *
     * @param object    the object to remove
     * @param listeners the listeners to notify. May be {@code null}
     */
    private void remove(org.openvpms.component.model.object.IMObject object, IArchetypeServiceListener[] listeners) {
        log.debug("ArchetypeService.remove: Removing object of type {} with id {} and version {}",
                  object.getArchetype(), object.getId(), object.getVersion());

        notifyRemove(object, listeners, true);

        try {
            dao.delete((IMObject) object);
            notifyRemove(object, listeners, false);
        } catch (IMObjectDAOException exception) {
            if (IMObjectDAOException.ErrorCode.CannotDeleteLookupInUse.equals(exception.getErrorCode())) {
                throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.CannotDeleteLookupInUse,
                                                    exception, object.getObjectReference());
            }
            throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToDeleteObject,
                                                exception, object.getObjectReference());
        }
    }

    /**
     * Unwraps an object to return the underlying implementation.
     *
     * @param object the object to unwrap
     * @return the implementation
     */
    private IMObject unwrap(org.openvpms.component.model.object.IMObject object) {
        if (object instanceof Beanable) {
            return ((Beanable) object).getObject();
        }
        return (IMObject) object;
    }

    /**
     * Unwraps a collection of objects to return the underlying implementation.
     *
     * @param objects the objects to unwrap
     * @return the implementation
     */
    private List<IMObject> unwrap(Collection<? extends org.openvpms.component.model.object.IMObject> objects) {
        List<IMObject> result = new ArrayList<>();
        for (org.openvpms.component.model.object.IMObject object : objects) {
            result.add(unwrap(object));
        }
        return result;
    }

    /**
     * Iterate through the {@link NodeDescriptor}s and set all derived values.
     * Do it recursively.
     *
     * @param object  the object
     * @param context the context object
     * @param nodes   a list of node descriptors
     * @throws ArchetypeServiceException for any error
     */
    private void deriveValues(IMObject object, JXPathContext context, Map<String, NodeDescriptor> nodes) {
        for (NodeDescriptor node : nodes.values()) {
            if (node.isDerived()) {
                try {
                    Object value = context.getValue(node.getDerivedValue());
                    node.setValue(object, context, value);
                } catch (Exception exception) {
                    throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToDeriveValue,
                                                        exception, node.getName(), node.getPath());
                }
            }

            // if this node contains other nodes then make a recursive call
            if (node.getNodeDescriptors().size() > 0) {
                deriveValues(object, context, node.getNodeDescriptors());
            }
        }
    }

    /**
     * Updates the descriptor cache. If a transaction is in progress, the
     * cache will only be updated on transaction commit. This means that the
     * descriptor will only be available via the <em>get*Descriptor</em> methods
     * on successful commit.
     *
     * @param object the object to add to the cache
     */
    private void updateCache(org.openvpms.component.model.object.IMObject object) {
        if (TransactionSynchronizationManager.isActualTransactionActive()) {
            // update the cache when the transaction commits
            TransactionSynchronizationManager.registerSynchronization(
                    new TransactionSynchronizationAdapter() {
                        @Override
                        public void afterCompletion(int status) {
                            if (status == STATUS_COMMITTED) {
                                addToCache(object);
                            }
                        }
                    });
        } else {
            addToCache(object);
        }
    }

    /**
     * Adds a descriptor to the cache.
     *
     * @param object the object to add to the cache
     */
    private void addToCache(org.openvpms.component.model.object.IMObject object) {
        if (object instanceof org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor) {
            String archetype = ((ArchetypeDescriptor) object).getArchetypeType();
            boolean exists = dCache.getArchetypeDescriptor(archetype) != null;
            org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor descriptor
                    = (org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor) get(
                    object.getObjectReference());
            // retrieve the saved instance - this will have assertion descriptors correctly associated with assertion
            // type descriptors
            if (descriptor != null) {
                dCache.addArchetypeDescriptor(descriptor);
                if (!exists) {
                    listeners.archetypeAdded(archetype);
                }
            }
        } else if (object instanceof org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor) {
            dCache.addAssertionTypeDescriptor(
                    (org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor) object);
        }
    }

    /**
     * Notifies any listeners when an object is saved.
     *
     * @param object  the object
     * @param preSave if {@code true} the object is about to be saved, otherwise it has been saved
     */
    private void notifySave(org.openvpms.component.model.object.IMObject object, boolean preSave) {
        notifySave(object, null, preSave);
    }

    /**
     * Notifies any listeners when a collection of objects is saved.
     *
     * @param objects the objects
     * @param preSave if {@code true} the objects are about to be saved, otherwise they have been saved
     */
    private void notifySave(Collection<? extends org.openvpms.component.model.object.IMObject> objects,
                            boolean preSave) {
        Notifier notifier = null;
        for (org.openvpms.component.model.object.IMObject object : objects) {
            notifier = notifySave(object, notifier, preSave);
        }
    }

    /**
     * Notifies any listeners when an object is saved.
     *
     * @param object   the saved object
     * @param notifier the notifier to use. If {@code null} indicates to create a new notifier
     * @param preSave  if {@code true} the object is about to be saved, otherwise it has been saved
     * @return the notifier
     */
    private Notifier notifySave(org.openvpms.component.model.object.IMObject object, Notifier notifier,
                                boolean preSave) {
        IArchetypeServiceListener[] matches = listeners.get(object.getArchetype());
        if (matches != null) {
            if (notifier == null) {
                notifier = Notifier.getNotifier(this, transactionManager);
            }
            if (preSave) {
                notifier.notifySaving(object, matches);
            } else {
                notifier.notifySaved(object, matches);
            }
        }
        return notifier;
    }

    /**
     * Notifies any listeners when an object is removed.
     *
     * @param object    removed saved object
     * @param listeners the listeners to notify
     * @param preRemove if {@code true} the object is about to be removed, otherwise it has been removed
     */
    private void notifyRemove(org.openvpms.component.model.object.IMObject object,
                              IArchetypeServiceListener[] listeners, boolean preRemove) {
        if (listeners != null) {
            Notifier notifier = Notifier.getNotifier(this, transactionManager);
            if (preRemove) {
                notifier.notifyRemoving(object, listeners);
            } else {
                notifier.notifyRemoved(object, listeners);
            }
        }
    }

    private class ObjectFactory extends AbstractIMObjectFactory {

        @Override
        protected org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor
        getArchetypeDescriptor(String shortName) {
            return dCache.getArchetypeDescriptor(shortName);
        }

        @Override
        protected org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor
        getArchetypeDescriptor(ArchetypeId id) {
            return dCache.getArchetypeDescriptor(id);
        }
    }

}
