/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper.lookup;

import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.datatypes.property.AssertionProperty;
import org.openvpms.component.business.domain.im.datatypes.property.PropertyList;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;

import java.util.ArrayList;
import java.util.List;


/**
 * Local lookup assertion. The lookups are defined inline in the assertion.
 * E.g:
 * <pre>
 *   <node name="sex" path="/details/sex" type="java.lang.String">
 *     <assertion name="lookup.local">
 *       <propertyList name="entries">
 *         <property name="MALE" value="male"/>
 *         <property name="FEMALE" value="female"/>
 *       </propertyList>
 *     </assertion>
 * 	 </node>
 * </pre>
 *
 * @author Tim Anderson
 */
class LocalLookup extends AbstractLookupAssertion {

    /**
     * The lookup type.
     */
    public static final String TYPE = "lookup.local"; // NON-NLS

    /**
     * The lookups.
     */
    private final List<Lookup> lookups = new ArrayList<>();


    /**
     * Constructs a {@link LocalLookup}.
     *
     * @param assertion     the assertion descriptor
     * @param service       the archetype service
     * @param lookupService the lookup service
     */
    public LocalLookup(AssertionDescriptor assertion, ArchetypeService service, LookupService lookupService) {
        super(assertion, TYPE, service, lookupService);
        PropertyList list = (PropertyList) assertion.getPropertyMap().getProperties().get("entries"); 
        for (NamedProperty prop : list.getProperties()) {
            AssertionProperty aprop = (AssertionProperty) prop;
            lookups.add(new org.openvpms.component.business.domain.im.lookup.Lookup(ArchetypeId.LOCAL_LOOKUP_ID,
                                                                                    aprop.getName(), aprop.getValue()));
        }
    }

    /**
     * Returns the lookups for this assertion.
     *
     * @return a list of lookups
     */
    @Override
    public List<Lookup> getLookups() {
        return lookups;
    }

    /**
     * Returns the lookup with the specified code.
     *
     * @return the lookup matching {@code code}, or {@code null} if none is found
     */
    @Override
    public Lookup getLookup(String code) {
        for (Lookup lookup : lookups) {
            if (lookup.getCode().equals(code)) {
                return lookup;
            }
        }
        return null;
    }

}
