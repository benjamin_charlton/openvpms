/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.resources.Messages;
import org.openvpms.component.system.common.exception.OpenVPMSException;


/**
 * Exception class for exceptions raised by {@link PropertyResolver}.
 *
 * @author Tim Anderson
 */
public class PropertyResolverException extends OpenVPMSException {

    /**
     * An enumeration of error codes.
     */
    public enum ErrorCode {
        InvalidProperty,
        InvalidObject,
        InvalidNode
    }

    /**
     * The error code.
     */
    private final ErrorCode errorCode;

    /**
     * The messages.
     */
    private static final Messages messages = Messages.getMessages(
            "org.openvpms.component.business.service.archetype.helper." + OpenVPMSException.ERRMESSAGES_FILE);

    /**
     * Constructs a {@link PropertyResolverException}.
     *
     * @param code the error code
     * @param args the message arguments
     */
    public PropertyResolverException(ErrorCode code, Object... args) {
        super(messages.getMessage(code.toString(), args));
        this.errorCode = code;
    }

    /**
     * Returns the error code.
     *
     * @return the error code
     */
    public ErrorCode getErrorCode() {
        return errorCode;
    }

}
