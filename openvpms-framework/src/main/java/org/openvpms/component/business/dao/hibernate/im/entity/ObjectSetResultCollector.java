/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.entity;

import org.openvpms.component.business.dao.hibernate.cache.ArchetypeIdCache;
import org.openvpms.component.business.dao.hibernate.im.common.Assembler;
import org.openvpms.component.business.dao.hibernate.im.common.Context;
import org.openvpms.component.business.dao.hibernate.im.common.IMObjectDO;
import org.openvpms.component.business.dao.im.common.ResultCollector;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.system.common.query.ObjectSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Implementation of {@link ResultCollector} that collects {@link ObjectSet}s.
 *
 * @author Tim Anderson
 */
public class ObjectSetResultCollector extends HibernateResultCollector<ObjectSet> {

    /**
     * Indicates which columns starts references.
     */
    private final String[] refColumnNames;

    /**
     * The results.
     */
    private final List<ObjectSet> result = new ArrayList<>();

    /**
     * A map of type aliases to their corresponding archetypes. May be {@code null}
     */
    private final Map<String, Set<String>> types;

    /**
     * The archetype Id cache.
     */
    private final ArchetypeIdCache archetypeIds;

    /**
     * The object names.
     */
    private String[] names;


    /**
     * Constructs an {@link ObjectSetResultCollector}.
     *
     * @param names        the object names. May be {@code null}
     * @param refNames     the names of the references being selected. May be {@code null}
     * @param types        a map of type aliases to their corresponding archetype
     * @param archetypeIds the archetype Id cache
     */
    public ObjectSetResultCollector(List<String> names, List<String> refNames, Map<String, Set<String>> types,
                                    ArchetypeIdCache archetypeIds) {
        if (names != null) {
            this.names = names.toArray(new String[0]);
            refColumnNames = new String[this.names.length];

            for (String refName : refNames) {
                int index = names.indexOf(refName + ".archetypeId");
                if (index == -1 || index + 2 >= refColumnNames.length) {
                    throw new IllegalArgumentException(
                            "Argument 'refNames' contains an invalid reference");
                }
                refColumnNames[index] = refName + ".reference";
            }
        } else {
            refColumnNames = null;
        }
        this.types = types;
        this.archetypeIds = archetypeIds;
    }

    /**
     * Collects an object.
     *
     * @param object the object to collect
     */
    public void collect(Object object) {
        ObjectSet set = createObjectSet();
        ObjectLoader loader = getLoader();
        if (names == null) {
            getNames(object);
        }
        if (object instanceof Object[]) {
            collect((Object[]) object, set, loader);
        } else if (names.length != 1) {
            throw new IllegalStateException("Mismatch args");
        } else {
            if (object instanceof IMObjectDO) {
                object = assemble((IMObjectDO) object);
            } else {
                loader.load(object);
            }
            set.set(names[0], object);
        }
        result.add(set);
    }

    /**
     * Returns the results.
     *
     * @return the results
     */
    protected List<ObjectSet> getResults() {
        return result;
    }

    /**
     * Collects values and stores them in the set.
     *
     * @param values the values
     * @param set    the set to populate
     * @param loader the loader
     */
    private void collect(Object[] values, ObjectSet set, ObjectLoader loader) {
        if (values.length != names.length) {
            throw new IllegalStateException("Mismatch args");
        }
        for (int i = 0; i < names.length; ) {
            if (refColumnNames != null && refColumnNames[i] != null) {
                IMObjectReference ref = null;
                ArchetypeId archetypeId = (ArchetypeId) values[i];
                if (archetypeId != null) {
                    archetypeId = archetypeIds.get(archetypeId);
                    Long id = (Long) values[i + 1];
                    if (id == null) {
                        id = -1L; // should never happen
                    }
                    String linkId = (String) values[i + 2];
                    ref = new IMObjectReference(archetypeId, id, linkId);
                }
                set.set(refColumnNames[i], ref);
                i += 3;
            } else {
                Object value = values[i];
                if (value instanceof IMObjectDO) {
                    value = assemble((IMObjectDO) value);
                } else if (value instanceof ArchetypeId) {
                    value = archetypeIds.get((ArchetypeId) value);
                } else if (value != null) {
                    loader.load(value);
                }
                set.set(names[i], value);
                i++;
            }
        }
    }

    /**
     * Assembles an {@link IMObject} from an {@link IMObjectDO}.
     *
     * @param object the source object
     * @return the assembled object
     */
    private IMObject assemble(IMObjectDO object) {
        Context context = getContext();
        Assembler assembler = context.getAssembler();
        return assembler.assemble(object, context);
    }

    /**
     * Creates a new object set.
     *
     * @return a new object set
     */
    private ObjectSet createObjectSet() {
        ObjectSet set = new ObjectSet();
        if (types != null) {
            for (Map.Entry<String, Set<String>> entry : types.entrySet()) {
                set.addType(entry.getKey(), entry.getValue());
            }
        }
        return set;
    }

    /**
     * Creates the names to associate with each object in the set.
     *
     * @param object the object set
     */
    private void getNames(Object object) {
        if (object instanceof Object[]) {
            Object[] values = (Object[]) object;
            names = new String[values.length];
            for (int i = 0; i < names.length; ++i) {
                names[i] = "" + i;
            }

        } else {
            names = new String[1];
            names[0] = "0";
        }
    }
}
