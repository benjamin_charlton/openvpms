/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.openvpms.component.model.user.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Default implementation of {@link AuthenticationContext}.
 * <p/>
 * This uses {@link SecurityContextHolder}.
 *
 * @author Tim Anderson
 */
public class AuthenticationContextImpl implements AuthenticationContext {

    /**
     * Returns the user.
     *
     * @return the user, or {@code null} if no user has been set
     */
    @Override
    public User getUser() {
        User result = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof User) {
                result = (User) principal;
            }
        }
        return result;
    }

    /**
     * Sets the user.
     *
     * @param user the user. May be {@code null}
     */
    @Override
    public void setUser(User user) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        if (user != null) {
            org.openvpms.component.business.domain.im.security.User u
                    = (org.openvpms.component.business.domain.im.security.User) user;
            UsernamePasswordAuthenticationToken authentication
                    = new UsernamePasswordAuthenticationToken(user, u.getPassword(), u.getAuthorities());
            context.setAuthentication(authentication);
        }
        SecurityContextHolder.setContext(context);
    }
}
