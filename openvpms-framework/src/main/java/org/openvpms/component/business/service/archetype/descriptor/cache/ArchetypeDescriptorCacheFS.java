/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.component.business.service.archetype.descriptor.cache;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.mapping.MappingException;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptors;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptors;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;

/**
 * This implementation reads the archetype descriptors from the file system,
 * parses them and caches them in memory.
 *
 * @author Jim Alateras
 */
public class ArchetypeDescriptorCacheFS extends BaseArchetypeDescriptorCache
        implements IArchetypeDescriptorCache {

    /**
     * The beans available to {@link AssertionDescriptor}s when they are evaluated. May be {@code null}
     */
    private final BeanFactory beans;

    /**
     * The logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(ArchetypeDescriptorCacheFS.class);

    /**
     * Archetype mapping file path.
     */
    private static final String ARCHETYPE_MAPPING_PATH
            = "org/openvpms/component/business/domain/im/archetype/descriptor/archetype-mapping-file.xml";

    /**
     * Assertion type mapping file path.
     */
    private static final String ASSERTION_MAPPING_PATH
            = "org/openvpms/component/business/domain/im/archetype/descriptor/assertion-type-mapping-file.xml";


    /**
     * Construct an instance of this cache by loading and parsing all the
     * archetype definitions in the specified file.
     * <p/>
     * The resource specified by archefile must be loadable from the classpath.
     * A similar constraint applies to the assertFile.
     *
     * @param archeFile  the file that holds all the archetype records.
     * @param assertFile the file that holds the assertions
     * @throws ArchetypeDescriptorCacheException thrown if it cannot bootstrap the cache
     */
    public ArchetypeDescriptorCacheFS(String archeFile, String assertFile) {
        this(archeFile, assertFile, null);
    }

    /**
     * Construct an instance of this cache by loading and parsing all the
     * archetype definitions in the specified file.
     * <p/>
     * The resource specified by archefile must be loadable from the classpath.
     * A similar constraint applies to the assertFile.
     *
     * @param archeFile  the file that holds all the archetype records.
     * @param assertFile the file that holds the assertions
     * @param beans      the beans available to {@link AssertionDescriptor}s when they are evaluated. May be {@code null}
     * @throws ArchetypeDescriptorCacheException thrown if it cannot bootstrap the cache
     */
    public ArchetypeDescriptorCacheFS(String archeFile, String assertFile, BeanFactory beans) {
        this.beans = beans;
        loadAssertionTypeDescriptorsFromFile(assertFile);
        loadArchetypeDescriptorsInFile(archeFile);
    }

    /**
     * Construct the archetype cache by loading and parsing all archetype
     * definitions in the specified directory. Only process files with the
     * specified extensions
     *
     * @param archDir    the directory
     * @param extensions only process files with these extensions
     * @param assertFile the file that holds the assertions
     * @throws ArchetypeDescriptorCacheException thrown if it cannot bootstrap the cache
     */
    public ArchetypeDescriptorCacheFS(String archDir, String[] extensions, String assertFile) {
        this(archDir, extensions, assertFile, null);
    }

    /**
     * Construct the archetype cache by loading and parsing all archetype
     * definitions in the specified directory. Only process files with the
     * specified extensions
     *
     * @param archDir    the directory
     * @param extensions only process files with these extensions
     * @param assertFile the file that holds the assertions
     * @throws ArchetypeDescriptorCacheException thrown if it cannot bootstrap the cache
     */
    public ArchetypeDescriptorCacheFS(String archDir, String[] extensions, String assertFile, BeanFactory beans) {
        this.beans = beans;
        loadAssertionTypeDescriptorsFromFile(assertFile);
        loadArchetypeDescriptorsInDir(archDir, extensions);
    }


    /**
     * Processes an assertion.
     * <p/>
     * This links it to its corresponding {@link AssertionTypeDescriptor}.
     *
     * @param assertion the assertion
     */
    @Override
    protected void processAssertion(AssertionDescriptor assertion) {
        super.processAssertion(assertion);
        assertion.setBeans(beans);
    }

    /**
     * Process the archetypes defined in the specified file. Te file must be
     * located in the classpath.
     *
     * @param afile the path to the file containing the archetype records
     */
    private void loadArchetypeDescriptorsInFile(String afile) {

        // check that a non-null file was specified
        if (StringUtils.isEmpty(afile)) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.NoFileSpecified);
        }

        try {
            logger.debug("Attempting to process records in {}", afile);
            processArchetypeDescriptors(loadArchetypeDescriptors(afile));
        } catch (Exception exception) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.InvalidFile,
                    new Object[]{afile}, exception);
        }
    }

    /**
     * Load and parse all the archetypes that are defined in the files of the
     * speciied directory. Only process files with the nominated extensions. If
     * the directory is invalid or if any of the files in the directory are
     * invalid throw an exception.
     * <p/>
     * The archetypes will be cached in memory.
     *
     * @param adir       the directory to search
     * @param extensions the file extensions to check
     */
    private void loadArchetypeDescriptorsInDir(String adir, String[] extensions) {

        // check that a non-null directory was specified
        if (StringUtils.isEmpty(adir)) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.NoDirSpecified);
        }

        // check that a valid directory was specified
        File dir = FileUtils.toFile(Thread.currentThread()
                                            .getContextClassLoader().getResource(adir));
        if (dir == null) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.InvalidDir,
                    new Object[]{adir});
        }

        if (!dir.isDirectory()) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.InvalidDir,
                    new Object[]{adir});
        }

        // process all the files in the directory, that match the filter
        for (File file : FileUtils.listFiles(dir, extensions, true)) {
            try {
                logger.debug("Attempting to process records in {}", file.getName());

                processArchetypeDescriptors(loadArchetypeDescriptors(new FileReader(file)));
            } catch (Exception exception) {
                // do not throw an exception but log a warning
                logger.warn("Failed to load archetype",
                            new ArchetypeDescriptorCacheException(
                                    ArchetypeDescriptorCacheException.ErrorCode.InvalidFile,
                                    new Object[]{file.getName()}, exception));
            }

        }

    }

    /**
     * Iterate over all the descriptors and add them to the existing set of descriptors.
     *
     * @param descriptors the descriptors to process
     */
    @SuppressWarnings("unchecked")
    private void processArchetypeDescriptors(ArchetypeDescriptors descriptors) {
        for (ArchetypeDescriptor descriptor : descriptors.getArchetypeDescriptorsAsArray()) {
            ArchetypeId archId = descriptor.getType();

            logger.debug("Processing archetype record {}", archId.getShortName());

            try {
                // make sure that the underlying type is loadable
                Thread.currentThread().getContextClassLoader().loadClass(
                        descriptor.getClassName());
                addArchetypeDescriptor(descriptor);
            } catch (ClassNotFoundException excpetion) {
                throw new ArchetypeDescriptorCacheException(
                        ArchetypeDescriptorCacheException.ErrorCode.FailedToLoadClass,
                        new Object[]{descriptor.getClassName()}, excpetion);
            }

            // check that the assertions are specified correctly
            Map<String, NodeDescriptor> nodeDescriptors
                    = (Map<String, NodeDescriptor>) (Map<String, ?>) descriptor.getNodeDescriptorMap();
            if (nodeDescriptors.size() > 0) {
                checkAssertionsInNode(nodeDescriptors);
            }
        }
    }

    /**
     * Process the file and load all the {@link AssertionTypeDescriptor} instances
     *
     * @param assertFile the name of the file holding the assertion types
     */
    private void loadAssertionTypeDescriptorsFromFile(String assertFile) {
        // check that a non-null file was specified
        if (StringUtils.isEmpty(assertFile)) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.NoAssertionTypeFileSpecified);
        }

        AssertionTypeDescriptors types;
        try {
            logger.debug("Attempting to process file {}", assertFile);
            types = loadAssertionTypeDescriptors(assertFile);
            for (AssertionTypeDescriptor descriptor : types.getAssertionTypeDescriptors().values()) {
                addAssertionTypeDescriptor(descriptor);

                logger.debug("Loaded assertion type {}", descriptor.getName());
            }
        } catch (Exception exception) {
            throw new ArchetypeDescriptorCacheException(
                    ArchetypeDescriptorCacheException.ErrorCode.InvalidAssertionFile,
                    new Object[]{assertFile}, exception);
        }
    }

    /**
     * Return the {@link ArchetypeDescriptors} declared in the specified file.
     * In this case the file must be a resource in the class path.
     *
     * @param resourceName the name of the resource that the archetypes are dfined
     * @return ArchetypeDescriptors
     * @throws MappingException    if the mapping is invalid
     * @throws MarshalException    if the archetype descriptors cannot be read
     * @throws ValidationException if the archetype descriptors are invalid
     */
    private ArchetypeDescriptors loadArchetypeDescriptors(String resourceName)
            throws MappingException, MarshalException, ValidationException {
        // load the mapping file
        Mapping mapping = new Mapping();

        mapping.loadMapping(getResource(ARCHETYPE_MAPPING_PATH));
        Unmarshaller unmarshaller = new Unmarshaller(mapping);

        InputSource descriptors = getResource(resourceName);
        return (ArchetypeDescriptors) unmarshaller.unmarshal(descriptors);
    }

    /**
     * Return the {@link ArchetypeDescriptors} declared in the specified file.
     * In this case the file must be a resource in the class path.
     *
     * @param reader the reader that declares all the archetypes
     * @return ArchetypeDescriptors
     * @throws Exception propagate exception to caller
     */
    private ArchetypeDescriptors loadArchetypeDescriptors(Reader reader)
            throws Exception {
        // load the mapping file
        Mapping mapping = new Mapping();

        mapping.loadMapping(getResource(ARCHETYPE_MAPPING_PATH));

        return (ArchetypeDescriptors) new Unmarshaller(mapping)
                .unmarshal(reader);
    }

    /**
     * Return the {@link AssertionTypeDescriptors} in the specified file
     *
     * @param assertFile the file that declares the assertions
     * @return AssertionTypeDescriptors
     * @throws MappingException    if the mapping is invalid
     * @throws MarshalException    if the assertion type descriptors cannot be read
     * @throws ValidationException if the assertion type descriptors are invalid
     */
    private AssertionTypeDescriptors loadAssertionTypeDescriptors(String assertFile)
            throws MappingException, MarshalException, ValidationException {
        // load the mapping file
        Mapping mapping = new Mapping();

        mapping.loadMapping(getResource(ASSERTION_MAPPING_PATH));

        return (AssertionTypeDescriptors) new Unmarshaller(mapping)
                .unmarshal(getResource(assertFile));
    }

    /**
     * Helper to return an <tt>InputSource</tt> for a resource.
     *
     * @param resourceName the resource name
     * @return the input source
     * @throws IllegalArgumentException if the resource does not exist
     */
    private InputSource getResource(String resourceName) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream(resourceName);
        if (stream == null) {
            throw new IllegalArgumentException("Resource not found: " + resourceName);
        }
        return new InputSource(new InputStreamReader(stream));
    }
}
