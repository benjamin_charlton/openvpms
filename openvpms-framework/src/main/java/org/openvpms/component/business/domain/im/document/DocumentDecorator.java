/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.document;

import org.openvpms.component.business.domain.im.common.AuditableIMObjectDecorator;
import org.openvpms.component.model.document.Document;

import java.io.IOException;
import java.io.InputStream;

/**
 * Decorator for {@link Document}.
 *
 * @author Tim Anderson
 */
public class DocumentDecorator extends AuditableIMObjectDecorator implements Document {

    /**
     * Constructs a {@link DocumentDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public DocumentDecorator(Document peer) {
        super(peer);
    }

    /**
     * Returns the document content.
     *
     * @return the content
     * @throws IOException for any I/O error
     */
    @Override
    public InputStream getContent() throws IOException {
        return getPeer().getContent();
    }

    /**
     * Sets the document content.
     *
     * @param stream a stream of the content
     * @throws IOException for any I/O error
     */
    @Override
    public void setContent(InputStream stream) throws IOException {
        getPeer().setContent(stream);
    }

    /**
     * Returns the document size.
     * <p>
     * If the document has been compressed, this represents its size prior to compression.
     *
     * @return the document size
     */
    @Override
    public int getSize() {
        return getPeer().getSize();
    }

    /**
     * Sets the document size.
     *
     * @param size the size of the document.
     */
    @Override
    public void setSize(int size) {
        getPeer().setSize(size);
    }

    /**
     * Returns the mime type.
     *
     * @return the mime type.
     */
    @Override
    public String getMimeType() {
        return getPeer().getMimeType();
    }

    /**
     * Sets the mime type.
     *
     * @param mimeType the mime type.
     */
    @Override
    public void setMimeType(String mimeType) {
        getPeer().setMimeType(mimeType);
    }

    /**
     * Returns the document checksum.
     *
     * @return the checksum
     */
    @Override
    public long getChecksum() {
        return getPeer().getChecksum();
    }

    /**
     * Sets the document checksum.
     *
     * @param checksum the checksum
     */
    @Override
    public void setChecksum(long checksum) {
        getPeer().setChecksum(checksum);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected Document getPeer() {
        return (Document) super.getPeer();
    }
}
