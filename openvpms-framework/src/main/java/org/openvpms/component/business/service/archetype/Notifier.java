/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.openvpms.component.model.object.IMObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Notifies {@link IArchetypeServiceListener} of archetype service events.
 *
 * @author Tim Anderson
 */
class Notifier {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Determines if transaction synchronization is active.
     */
    private final boolean syncActive;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The set of saved objects.
     */
    private final Set<IMObject> saved = new LinkedHashSet<>();

    /**
     * The set of removed objects.
     */
    private final Set<IMObject> removed = new LinkedHashSet<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(Notifier.class);


    /**
     * Constructs a {@link Notifier}.
     *
     * @param service            the archetype service
     * @param syncActive         determines if transaction synchronization is active. If so, notification is delayed
     *                           until the transaction commits
     * @param transactionManager the transaction manager
     */
    private Notifier(ArchetypeService service, boolean syncActive, PlatformTransactionManager transactionManager) {
        this.service = service;
        this.syncActive = syncActive;
        this.transactionManager = transactionManager;
    }

    /**
     * Notifies listeners of an object being removed.
     * <p/>
     * If there is a transaction active, notification will be delayed and
     * only those listeners registered at time of transaction commit will be
     * notified.
     * <p/>
     * If there is no transaction active, the supplied listeners will be
     * notified immediately.
     *
     * @param object    the saved object
     * @param listeners the listeners to notify, if no transaction is active
     */
    void notifyRemoved(IMObject object, IArchetypeServiceListener[] listeners) {
        if (syncActive) {
            removed.add(object);
        } else {
            doNotifyRemoved(object, listeners);
        }
    }

    /**
     * Returns the notifier for the given service and current thread.
     * <p/>
     * If one does not exist, it will be created.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @return the context
     */
    static Notifier getNotifier(ArchetypeService service, PlatformTransactionManager transactionManager) {
        Notifier notifier;
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            if (!TransactionSynchronizationManager.hasResource(service)) {
                notifier = new Notifier(service, true, transactionManager);
                TransactionSynchronizationManager.bindResource(service, notifier);
                TransactionSynchronizationManager.registerSynchronization(new NotifierSynchronization(notifier));
            } else {
                notifier = (Notifier) TransactionSynchronizationManager.getResource(service);
            }
        } else {
            notifier = new Notifier(service, false, transactionManager);
        }
        return notifier;
    }

    /**
     * Notifies listeners of an object about to be saved.
     *
     * @param object    the object being saved
     * @param listeners the listeners to notify
     */
    void notifySaving(IMObject object, IArchetypeServiceListener[] listeners) {
        for (IArchetypeServiceListener listener : listeners) {
            if (syncActive) {
                // there is a transaction in progress, so register the object in order to notify listeners on commit or
                // rollback
                saved.add(object);
            }
            try {
                listener.save((org.openvpms.component.business.domain.im.common.IMObject) object);
            } catch (Exception exception) {
                logListenerException(exception);
            }
        }
    }

    /**
     * Notifies listeners of an object about to be removed.
     *
     * @param object    the object being removed
     * @param listeners the listeners to notify
     */
    void notifyRemoving(IMObject object, IArchetypeServiceListener[] listeners) {
        for (IArchetypeServiceListener listener : listeners) {
            if (syncActive) {
                // there is a transaction in progress, so register the object in order to notify listeners on commit or
                // rollback
                removed.add(object);
            }
            try {
                listener.remove((org.openvpms.component.business.domain.im.common.IMObject) object);
            } catch (Exception exception) {
                logListenerException(exception);
            }
        }
    }

    /**
     * Notifies listeners of an object being saved.
     * <p/>
     * If there is a transaction active, notification will be delayed and
     * only those listeners registered at time of transaction commit will be
     * notified.
     * <p/>
     * If there is no transaction active, the supplied listeners will be
     * notified immediately.
     *
     * @param object    the saved object
     * @param listeners the listeners to notify, if no transaction is active
     */
    void notifySaved(IMObject object, IArchetypeServiceListener[] listeners) {
        if (syncActive) {
            saved.add(object);
        } else {
            doNotifySaved(object, listeners);
        }
    }

    /**
     * Notifies listeners of any pending events on commit.
     */
    private void notifyCommit() {
        doInNewTransaction(() -> {
            for (IMObject object : saved) {
                IArchetypeServiceListener[] listeners = service.getListeners(object.getArchetype());
                if (listeners != null) {
                    doNotifySaved(object, listeners);
                }
            }
            for (IMObject object : removed) {
                IArchetypeServiceListener[] listeners = service.getListeners(object.getArchetype());
                if (listeners != null) {
                    doNotifyRemoved(object, listeners);
                }
            }
        });
        destroy();
    }

    /**
     * Notifies listeners of any pending events on rollback.
     */
    private void notifyRollback() {
        doInNewTransaction(() -> {
            for (IMObject object : saved) {
                IArchetypeServiceListener[] listeners = service.getListeners(object.getArchetype());
                if (listeners != null) {
                    doNotifyRollback(object, listeners);
                }
            }
            for (IMObject object : removed) {
                IArchetypeServiceListener[] listeners = service.getListeners(object.getArchetype());
                if (listeners != null) {
                    doNotifyRollback(object, listeners);
                }
            }
        });
        destroy();
    }

    /**
     * Executes a {@code Runnable} in a new transaction context.
     *
     * @param runnable the runnable to execute
     */
    private void doInNewTransaction(Runnable runnable) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                runnable.run();
            }
        });
    }

    /**
     * Destroys any pending events.
     */
    private void destroy() {
        saved.clear();
        removed.clear();
    }

    /**
     * Notifies listeners of an object being saved.
     *
     * @param object    the saved object
     * @param listeners the listener to notify
     */
    private void doNotifySaved(IMObject object, IArchetypeServiceListener[] listeners) {
        for (IArchetypeServiceListener listener : listeners) {
            try {
                listener.saved((org.openvpms.component.business.domain.im.common.IMObject) object);
            } catch (Exception exception) {
                logListenerException(exception);
            }
        }
    }

    /**
     * Notifies listeners of an object being removed.
     *
     * @param object    the removed object
     * @param listeners the listeners to notify
     */
    private void doNotifyRemoved(IMObject object, IArchetypeServiceListener[] listeners) {
        for (IArchetypeServiceListener listener : listeners) {
            try {
                listener.removed((org.openvpms.component.business.domain.im.common.IMObject) object);
            } catch (Exception exception) {
                logListenerException(exception);
            }
        }
    }

    /**
     * Notifies listeners of an object being rolled back.
     *
     * @param object    the removed object
     * @param listeners the listeners to notify
     */
    private void doNotifyRollback(IMObject object, IArchetypeServiceListener[] listeners) {
        for (IArchetypeServiceListener listener : listeners) {
            try {
                listener.rollback((org.openvpms.component.business.domain.im.common.IMObject) object);
            } catch (Exception exception) {
                logListenerException(exception);
            }
        }
    }

    /**
     * Logs an unhandled exception from a listener.
     *
     * @param exception the exception
     */
    private void logListenerException(Exception exception) {
        log.error("Caught unhandled exception from IArchetypeServiceListener", exception);
    }

    /**
     * Helper class to trigger events on transaction commit.
     */
    private static class NotifierSynchronization
            extends TransactionSynchronizationAdapter {

        private final Notifier notifier;

        private boolean suspended;

        NotifierSynchronization(Notifier notifier) {
            this.notifier = notifier;
        }

        @Override
        public void suspend() {
            if (TransactionSynchronizationManager.unbindResourceIfPossible(notifier.service) != null) {
                suspended = true;
            }
        }

        @Override
        public void resume() {
            if (suspended) {
                TransactionSynchronizationManager.bindResource(notifier.service, notifier);
                suspended = false;
            }
        }

        @Override
        public void afterCompletion(int status) {
            TransactionSynchronizationManager.unbindResource(notifier.service);
            if (status == STATUS_COMMITTED) {
                notifier.notifyCommit();
            } else if (status == STATUS_ROLLED_BACK) {
                notifier.notifyRollback();
            }
            notifier.destroy();
        }

    }

}
