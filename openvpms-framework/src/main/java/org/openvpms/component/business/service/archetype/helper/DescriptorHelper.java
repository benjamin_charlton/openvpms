/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * Helper class for working with {@link ArchetypeDescriptor} and {@link NodeDescriptor}s.
 *
 * @author Tim Anderson
 */
public final class DescriptorHelper {

    /**
     * Default constructor.
     */
    private DescriptorHelper() {
        // no-op
    }

    /**
     * Returns an archetype descriptor, given its shortname.
     *
     * @param shortName the shortname
     * @param service   the archetype service
     * @return the descriptor corresponding to {@code shortName}, or
     * {@code null} if none exists
     * @throws ArchetypeServiceException for any error
     */
    public static ArchetypeDescriptor getArchetypeDescriptor(String shortName, ArchetypeService service) {
        return (ArchetypeDescriptor) service.getArchetypeDescriptor(shortName);
    }

    /**
     * Returns an archetype descriptor, given a reference.
     *
     * @param reference the object reference
     * @param service   the archetype service
     * @return the descriptor corresponding to {@code reference}, or
     * {@code null} if none exists
     * @throws ArchetypeServiceException for any error
     */
    public static ArchetypeDescriptor getArchetypeDescriptor(Reference reference, ArchetypeService service) {
        return getArchetypeDescriptor(reference.getArchetype(), service);
    }

    /**
     * Returns the archetype descriptor for the specified object.
     *
     * @param object  the object
     * @param service the archetype service
     * @return the archetype descriptor corresponding to {@code object}
     * @throws ArchetypeServiceException for any error
     */
    public static ArchetypeDescriptor getArchetypeDescriptor(IMObject object, ArchetypeService service) {
        return (ArchetypeDescriptor) service.getArchetypeDescriptor(object.getArchetype());
    }

    /**
     * Returns the archetype descriptors for an archetype short name.
     *
     * @param shortName the archetype short name. May contain wildcards
     * @param service   the archetype service
     * @return a list of archetype descriptors
     * @throws ArchetypeServiceException for any error
     */
    public static List<org.openvpms.component.model.archetype.ArchetypeDescriptor>
    getArchetypeDescriptors(String shortName, ArchetypeService service) {
        return getArchetypeDescriptors(new String[]{shortName}, service);
    }

    /**
     * Returns the archetype descriptors for an archetype range.
     *
     * @param range   the archetype range. May contain wildcards
     * @param service the archetype service
     * @return a list of archetype descriptors
     * @throws ArchetypeServiceException for any error
     */
    public static List<org.openvpms.component.model.archetype.ArchetypeDescriptor> getArchetypeDescriptors(
            String[] range, ArchetypeService service) {
        List<org.openvpms.component.model.archetype.ArchetypeDescriptor> result = new ArrayList<>();
        for (String shortName : range) {
            result.addAll(service.getArchetypeDescriptors(shortName));
        }
        return result;
    }

    /**
     * Returns archetype short names from a descriptor.
     * <p/>
     * This uses short names from {@link NodeDescriptor#getArchetypeRange()}, or
     * if none are present, those from {@link NodeDescriptor#getFilter()}.
     * <p/>
     * Any wildcards are expanded.
     *
     * @param descriptor the node descriptor
     * @param service    the archetype service
     * @return a list of short names
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getShortNames(NodeDescriptor descriptor, ArchetypeService service) {
        String[] names = getShortNames(descriptor.getArchetypeRange(), false, service);
        if (names.length == 0 && !StringUtils.isEmpty(descriptor.getFilter())) {
            names = getShortNames(descriptor.getFilter(), false, service);
        }
        return names;
    }

    /**
     * Returns primary archetype short names matching the specified criteria.
     *
     * @param shortName the short name. May contain wildcards
     * @param service   the archetype service
     * @return a list of short names matching the criteria
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getShortNames(String shortName, ArchetypeService service) {
        return getShortNames(shortName, true, service);
    }

    /**
     * Returns archetype short names matching the specified criteria.
     *
     * @param shortName   the short name. May contain wildcards
     * @param primaryOnly if {@code true} only include primary archetypes
     * @param service     the archetype service
     * @return a list of short names matching the criteria
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getShortNames(String shortName, boolean primaryOnly, ArchetypeService service) {
        return getShortNames(new String[]{shortName}, primaryOnly, service);
    }

    /**
     * Returns primary archetype short names matching the specified criteria.
     *
     * @param shortNames the short names. May contain wildcards
     * @param service    the archetype service
     * @return a list of short names matching the criteria
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getShortNames(String[] shortNames, ArchetypeService service) {
        return getShortNames(shortNames, true, service);
    }

    /**
     * Returns archetype short names matching the specified criteria.
     *
     * @param shortNames  the shortNames. May contain wildcards
     * @param primaryOnly if {@code true} only include primary archetypes
     * @param service     the archetype service
     * @return a list of short names matching the criteria
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getShortNames(String[] shortNames, boolean primaryOnly, ArchetypeService service) {
        Set<String> result = new LinkedHashSet<>();
        for (String shortName : shortNames) {
            List<String> matches = service.getArchetypes(shortName, primaryOnly);
            result.addAll(matches);
        }
        return result.toArray(new String[0]);
    }

    /**
     * Returns archetype short names from a node for each archetype matching {@code shortName} where
     * the specified node is present. This expands any wildcards.
     * <p/>
     * This uses short names from {@link NodeDescriptor#getArchetypeRange()}, or
     * if none are present, those from {@link NodeDescriptor#getFilter()}.
     *
     * @param shortName the archetype short name. May contain wildcards
     * @param node      the node name
     * @param service   the archetype service
     * @return a list of short names
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getNodeShortNames(String shortName, String node, ArchetypeService service) {
        return getNodeShortNames(new String[]{shortName}, node, service);
    }

    /**
     * Returns archetype short names from a node for each archetype where
     * the specified node is present. This expands any wildcards.
     * <p/>
     * This uses short names from {@link NodeDescriptor#getArchetypeRange()}, or
     * if none are present, those from {@link NodeDescriptor#getFilter()}.
     *
     * @param shortNames the archetype short names. May contain wildcards
     * @param node       the node name
     * @param service    the archetype service
     * @return a list of short names
     * @throws ArchetypeServiceException for any error
     */
    public static String[] getNodeShortNames(String[] shortNames, String node, ArchetypeService service) {
        Set<String> matches = new LinkedHashSet<>();
        String[] expanded = getShortNames(shortNames, false, service);
        for (String shortName : expanded) {
            ArchetypeDescriptor archetype = getArchetypeDescriptor(shortName, service);
            if (archetype != null) {
                NodeDescriptor desc = archetype.getNodeDescriptor(node);
                if (desc != null) {
                    matches.addAll(Arrays.asList(getShortNames(desc, service)));
                }
            }
        }
        return matches.toArray(new String[0]);
    }

    /**
     * Returns the display name for an archetype.
     *
     * @param shortName the archetype short name
     * @param service   the archetype service
     * @return the archetype display name, or {@code null} if none exists
     * @throws ArchetypeServiceException for any error
     */
    public static String getDisplayName(String shortName, ArchetypeService service) {
        ArchetypeDescriptor descriptor = getArchetypeDescriptor(shortName, service);
        return (descriptor != null) ? descriptor.getDisplayName() : null;
    }

    /**
     * Returns the display name for an object.
     *
     * @param object  the object
     * @param service the archetype service
     * @return a display name for the object, or {@code null} if none exists
     * @throws ArchetypeServiceException for any error
     */
    public static String getDisplayName(IMObject object, ArchetypeService service) {
        ArchetypeDescriptor descriptor = getArchetypeDescriptor(object, service);
        return (descriptor != null) ? descriptor.getDisplayName() : null;
    }

    /**
     * Returns the display name for a node.
     *
     * @param object  the object
     * @param node    the node
     * @param service the archetype service
     * @return a display name for the node, or {@code null} if none exists
     * @throws ArchetypeServiceException for any error
     */
    public static String getDisplayName(IMObject object, String node, ArchetypeService service) {
        return getDisplayName(object.getArchetype(), node, service);
    }

    /**
     * Returns the display name for a node.
     *
     * @param shortName the archetype short name
     * @param node      the node
     * @param service   the archetype service
     * @return a display name for the node, or {@code null} if none exists
     * @throws ArchetypeServiceException for any error
     */
    public static String getDisplayName(String shortName, String node, ArchetypeService service) {
        String result = null;
        ArchetypeDescriptor archetype = getArchetypeDescriptor(shortName, service);
        if (archetype != null) {
            NodeDescriptor descriptor = archetype.getNodeDescriptor(node);
            if (descriptor != null) {
                result = descriptor.getDisplayName();
            }
        }
        return result;
    }

    /**
     * Returns node names common to a set of archetypes.
     *
     * @param shortNames the archetype short names. May contain wildcards.
     * @param service    the archetype service
     * @return node names common to the archetypes
     */
    public static String[] getCommonNodeNames(String shortNames, ArchetypeService service) {
        return getCommonNodeNames(shortNames, null, service);
    }

    /**
     * Returns node names common to a set of archetypes.
     *
     * @param shortNames the archetype short names. May contain wildcards.
     * @param service    the archetype service
     * @return node names common to the archetypes
     */
    public static String[] getCommonNodeNames(String[] shortNames, ArchetypeService service) {
        return getCommonNodeNames(shortNames, null, service);
    }

    /**
     * Returns node names common to a set of archetypes.
     *
     * @param shortNames the archetype short names. May contain wildcards.
     * @param nodes      node names to check. If {@code null}, all nodes common to the archetypes will be returned.
     * @param service    the archetype service
     * @return node names common to the archetypes
     */
    public static String[] getCommonNodeNames(String shortNames, String[] nodes, ArchetypeService service) {
        return getCommonNodeNames(new String[]{shortNames}, nodes, service);
    }

    /**
     * Returns node names common to a set of archetypes.
     *
     * @param shortNames the archetype short names. May contain wildcards.
     * @param nodes      node names to check. If {@code null}, all nodes common to the archetypes will be returned.
     * @param service    the archetype service
     * @return node names common to the archetypes
     */
    public static String[] getCommonNodeNames(String[] shortNames, String[] nodes, ArchetypeService service) {
        Set<String> result = new LinkedHashSet<>();

        boolean init = false;
        if (nodes != null && nodes.length != 0) {
            result.addAll(Arrays.asList(nodes));
        } else {
            init = true;
        }
        for (org.openvpms.component.model.archetype.ArchetypeDescriptor descriptor :
                getArchetypeDescriptors(shortNames, service)) {
            if (init) {
                for (NodeDescriptor node : ((ArchetypeDescriptor) descriptor).getAllNodeDescriptors()) {
                    result.add(node.getName());
                }
                init = false;
            } else {
                result.removeIf(node -> descriptor.getNodeDescriptor(node) == null);
            }
        }
        return result.toArray(new String[0]);
    }

    /**
     * Returns the node descriptor for the named archetype and node.
     *
     * @param shortName the archetype short name
     * @param node      the node name
     * @param service   the archetype service
     * @return the node, or {@code null} if none is found
     */
    public static NodeDescriptor getNode(String shortName, String node, ArchetypeService service) {
        ArchetypeDescriptor descriptor = getArchetypeDescriptor(shortName, service);
        return (descriptor != null) ? descriptor.getNodeDescriptor(node) : null;
    }

}

