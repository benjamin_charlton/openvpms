/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.act;

import org.openvpms.component.model.act.FinancialAct;

import java.math.BigDecimal;

/**
 * Decorator for {@link FinancialAct}.
 *
 * @author Tim Anderson
 */
public class FinancialActDecorator extends ActDecorator implements FinancialAct {

    /**
     * Constructs a {@link FinancialActDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public FinancialActDecorator(FinancialAct peer) {
        super(peer);
    }

    /**
     * Determines if this is a credit or debit transaction.
     *
     * @return {@code true} if it's a credit, {@code false} if it's a debit
     */
    @Override
    public boolean isCredit() {
        return getPeer().isCredit();
    }

    /**
     * Determines if this is a credit or debit transaction.
     *
     * @param credit if {@code true} it's a credit. If {@code false} it's a debit
     */
    @Override
    public void setCredit(boolean credit) {
        getPeer().setCredit(credit);
    }

    /**
     * Sets the fixed cost.
     *
     * @param fixedCost the fixed cost
     */
    @Override
    public void setFixedCost(BigDecimal fixedCost) {
        getPeer().setFixedCost(fixedCost);
    }

    /**
     * Returns the fixed cost.
     *
     * @return the fixed cost.
     */
    @Override
    public BigDecimal getFixedCost() {
        return getPeer().getFixedCost();
    }

    /**
     * Returns the fixed amount.
     *
     * @return the fixed amount
     */
    @Override
    public BigDecimal getFixedAmount() {
        return getPeer().getFixedAmount();
    }

    /**
     * Sets the fixed amount.
     *
     * @param fixedAmount the fixed amount
     */
    @Override
    public void setFixedAmount(BigDecimal fixedAmount) {
        getPeer().setFixedAmount(fixedAmount);
    }

    /**
     * Sets the unit cost.
     *
     * @param unitCost the unit cost
     */
    @Override
    public void setUnitCost(BigDecimal unitCost) {
        getPeer().setUnitCost(unitCost);
    }

    /**
     * Returns the unit cost.
     *
     * @return the unit cost
     */
    @Override
    public BigDecimal getUnitCost() {
        return getPeer().getUnitCost();
    }

    /**
     * Returns the unit amount.
     *
     * @return the unit amount.
     */
    @Override
    public BigDecimal getUnitAmount() {
        return getPeer().getUnitAmount();
    }

    /**
     * Sets the unit amount.
     *
     * @param unitAmount the unit amount
     */
    @Override
    public void setUnitAmount(BigDecimal unitAmount) {
        getPeer().setUnitAmount(unitAmount);
    }

    /**
     * Returns the quantity.
     *
     * @return the quantity
     */
    @Override
    public BigDecimal getQuantity() {
        return getPeer().getQuantity();
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     */
    @Override
    public void setQuantity(BigDecimal quantity) {
        getPeer().setQuantity(quantity);
    }

    /**
     * Returns the tax amount.
     *
     * @return the tax amount
     */
    @Override
    public BigDecimal getTaxAmount() {
        return getPeer().getTaxAmount();
    }

    /**
     * Sets the tax amount.
     *
     * @param taxAmount the tax amount
     */
    @Override
    public void setTaxAmount(BigDecimal taxAmount) {
        getPeer().setTaxAmount(taxAmount);
    }

    /**
     * Returns the total.
     *
     * @return the total
     */
    @Override
    public BigDecimal getTotal() {
        return getPeer().getTotal();
    }

    /**
     * Sets the total.
     *
     * @param total the total
     */
    @Override
    public void setTotal(BigDecimal total) {
        getPeer().setTotal(total);
    }

    /**
     * Returns the allocated amount.
     * <p>For debits, it is the amount of credits that have been allocated against the total amount.
     * If allocated = total, then the debt is fully paid.
     * </p>
     * <p/>
     * For credits, it is the amount of the credit that has been allocated against a debit.
     * If allocated = total then credit has been fully allocated.
     * </p>
     *
     * @return the allocated amount
     */
    @Override
    public BigDecimal getAllocatedAmount() {
        return getPeer().getAllocatedAmount();
    }

    /**
     * Sets the allocated amount.
     *
     * @param amount the allocated amount
     */
    @Override
    public void setAllocatedAmount(BigDecimal amount) {
        getPeer().setAllocatedAmount(amount);
    }

    /**
     * Determines if the act has been printed.
     *
     * @return {@code true} if the act has been printed
     */
    @Override
    public boolean isPrinted() {
        return getPeer().isPrinted();
    }

    /**
     * Determines if the act has been printed.
     *
     * @param printed if {@code true}, the act has been printed
     */
    @Override
    public void setPrinted(boolean printed) {
        getPeer().setPrinted(printed);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected FinancialAct getPeer() {
        return (FinancialAct) super.getPeer();
    }
}
