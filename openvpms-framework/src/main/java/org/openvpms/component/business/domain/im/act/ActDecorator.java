/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.act;

import org.openvpms.component.business.domain.im.common.AuditableIMObjectDecorator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.Participation;

import java.util.Date;
import java.util.Set;

/**
 * Decorator for {@link Act}.
 *
 * @author Tim Anderson
 */
public class ActDecorator extends AuditableIMObjectDecorator implements Act {

    /**
     * Constructs an {@link ActDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public ActDecorator(Act peer) {
        super(peer);
    }

    /**
     * Returns the activity start time.
     *
     * @return the start time. May be {@code null}.
     */
    @Override
    public Date getActivityStartTime() {
        return getPeer().getActivityStartTime();
    }

    /**
     * Sets the activity start time.
     *
     * @param time the start time. May be {@code null}
     */
    @Override
    public void setActivityStartTime(Date time) {
        getPeer().setActivityStartTime(time);
    }

    /**
     * Returns the activity end time.
     *
     * @return the end time. May be {@code null}.
     */
    @Override
    public Date getActivityEndTime() {
        return getPeer().getActivityEndTime();
    }

    /**
     * Sets the activity end time.
     *
     * @param time the end time. May be {@code null}
     */
    @Override
    public void setActivityEndTime(Date time) {
        getPeer().setActivityEndTime(time);
    }

    /**
     * Returns the reason for the activity.
     *
     * @return the reason. May be {@code null}
     */
    @Override
    public String getReason() {
        return getPeer().getReason();
    }

    /**
     * Sets the reason for the activity.
     *
     * @param reason the reason. May be {@code null}
     */
    @Override
    public void setReason(String reason) {
        getPeer().setReason(reason);
    }

    /**
     * Returns the status of the activity.
     *
     * @return the status. May be {@code null}
     */
    @Override
    public String getStatus() {
        return getPeer().getStatus();
    }

    /**
     * Sets the status of the activity.
     *
     * @param status the status. May be {@code null}
     */
    @Override
    public void setStatus(String status) {
        getPeer().setStatus(status);
    }

    /**
     * Returns the secondary status of the activity.
     *
     * @return the secondary status. May be {@code null}
     */
    @Override
    public String getStatus2() {
        return getPeer().getStatus2();
    }

    /**
     * Sets the secondary status of the activity.
     *
     * @param status2 the secondary status. May be {@code null}
     */
    @Override
    public void setStatus2(String status2) {
        getPeer().setStatus2(status2);
    }

    /**
     * Returns the activity title.
     *
     * @return the title. May be {@code null}
     */
    @Override
    public String getTitle() {
        return getPeer().getTitle();
    }

    /**
     * Sets the activity title.
     *
     * @param title the title. May be {@code null}
     */
    @Override
    public void setTitle(String title) {
        getPeer().setTitle(title);
    }

    /**
     * Returns the identities for this activity.
     *
     * @return the identities
     */
    @Override
    public Set<ActIdentity> getIdentities() {
        return getPeer().getIdentities();
    }

    /**
     * Adds an identity.
     *
     * @param identity the entity identity to add
     */
    @Override
    public void addIdentity(ActIdentity identity) {
        getPeer().addIdentity(identity);
    }

    /**
     * Removes an identity.
     *
     * @param identity the identity to remove
     */
    @Override
    public void removeIdentity(ActIdentity identity) {
        getPeer().removeIdentity(identity);
    }

    /**
     * Returns the relationships where this activity is the source.
     *
     * @return the relationships
     */
    @Override
    public Set<ActRelationship> getSourceActRelationships() {
        return getPeer().getSourceActRelationships();
    }

    /**
     * Add a relationship where this activity is the source.
     *
     * @param relationship the relationship to add
     */
    @Override
    public void addSourceActRelationship(ActRelationship relationship) {
        getPeer().addSourceActRelationship(relationship);
    }

    /**
     * Removes a relationship where this activity is the source.
     *
     * @param relationship the relationship to remove
     */
    @Override
    public void removeSourceActRelationship(ActRelationship relationship) {
        getPeer().removeSourceActRelationship(relationship);
    }

    /**
     * Returns the relationships where this activity is the target.
     *
     * @return the relationships
     */
    @Override
    public Set<ActRelationship> getTargetActRelationships() {
        return getPeer().getTargetActRelationships();
    }

    /**
     * Add a relationship where this activity is the target.
     *
     * @param relationship the relationship to add
     */
    @Override
    public void addTargetActRelationship(ActRelationship relationship) {
        getPeer().addTargetActRelationship(relationship);
    }

    /**
     * Removes a relationship where this activity is the target.
     *
     * @param relationship the relationship to remove
     */
    @Override
    public void removeTargetActRelationship(ActRelationship relationship) {
        getPeer().removeSourceActRelationship(relationship);
    }

    /**
     * Adds a relationship between this activity and another.
     * <p>
     * It will determine if this is a source or target of the relationship and invoke
     * {@link #addSourceActRelationship} or {@link #addTargetActRelationship} accordingly.
     *
     * @param relationship the relationship to add
     */
    @Override
    public void addActRelationship(ActRelationship relationship) {
        getPeer().addActRelationship(relationship);
    }

    /**
     * Remove a relationship between this activity and another.
     * <p>
     * It will determine if this is a source or target of the relationship and invoke
     * {@link #removeSourceActRelationship} or {@link #removeTargetActRelationship} accordingly.
     *
     * @param relationship the act relationship to remove
     */
    @Override
    public void removeActRelationship(ActRelationship relationship) {
        getPeer().removeSourceActRelationship(relationship);
    }

    /**
     * Return all the relationships that the activity has.
     * <p>
     * NOTE: the returned set cannot be used to add or remove relationships.
     *
     * @return the relationships
     */
    @Override
    public Set<ActRelationship> getActRelationships() {
        return getPeer().getActRelationships();
    }

    /**
     * Returns the participation relationships for the activity.
     * <p>
     * These determine the entities participating in the activity.
     *
     * @return the participation relationships
     */
    @Override
    public Set<Participation> getParticipations() {
        return getPeer().getParticipations();
    }

    /**
     * Adds a participation relationship.
     *
     * @param participation the participation to add
     */
    @Override
    public void addParticipation(Participation participation) {
        getPeer().addParticipation(participation);
    }

    /**
     * Removes a participation relationship.
     *
     * @param participation the participation to remove
     */
    @Override
    public void removeParticipation(Participation participation) {
        getPeer().removeParticipation(participation);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected Act getPeer() {
        return (Act) super.getPeer();
    }
}
