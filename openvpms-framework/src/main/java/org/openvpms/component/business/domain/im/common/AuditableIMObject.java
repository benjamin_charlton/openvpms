/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.common;

import org.openvpms.component.model.object.Reference;

import java.util.Date;

/**
 * Default implementation of {@link org.openvpms.component.model.object.AuditableIMObject}.
 *
 * @author Tim Anderson
 */
public class AuditableIMObject extends IMObject implements org.openvpms.component.model.object.AuditableIMObject {

    /**
     * The timestamp when the object was created.
     */
    private Date created;

    /**
     * The user that created the object.
     */
    private Reference createdBy;

    /**
     * The timestamp when the object was updated.
     */
    private Date updated;

    /**
     * The user that updated the object.
     */
    private Reference updatedBy;

    /**
     * Returns the time the object was created.
     *
     * @return the create time. May be {@code null}
     */
    @Override
    public Date getCreated() {
        return created;
    }

    /**
     * Sets the time the object was created.
     *
     * @param created the create time. May be {@code null}
     */
    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * Returns a reference to the user that created the object.
     *
     * @return the user reference. May be {@code null}
     */
    @Override
    public Reference getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the user that created the object.
     *
     * @param createdBy the user reference. May be {@code null}
     */
    @Override
    public void setCreatedBy(Reference createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Returns the time when the object was updated.
     *
     * @return the update time. May be {@code null}
     */
    @Override
    public Date getUpdated() {
        return updated;
    }

    /**
     * Sets the time when the object was updated.
     *
     * @param updated the update time. May be {@code null}
     */
    @Override
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /**
     * Returns the user that updated the object.
     *
     * @return the user reference. May be {@code null}
     */
    @Override
    public Reference getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the user that updated the object.
     *
     * @param updatedBy the user reference. May be {@code null}
     */
    @Override
    public void setUpdatedBy(Reference updatedBy) {
        this.updatedBy = updatedBy;
    }

}
