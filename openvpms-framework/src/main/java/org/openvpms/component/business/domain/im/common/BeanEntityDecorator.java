/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.common;


import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.component.model.entity.Entity;


/**
 * An {@link EntityDecorator} that wraps the entity in an {@link IMObjectBean}.
 *
 * @author Tim Anderson
 */
public class BeanEntityDecorator extends EntityDecorator {

    /**
     * The bean.
     */
    private final IMObjectBean bean;

    /**
     * Constructs a {@link BeanEntityDecorator}.
     *
     * @param peer    the peer to delegate to
     * @param factory the bean factory
     */
    public BeanEntityDecorator(Entity peer, IMObjectBeanFactory factory) {
        this(factory.getBean(peer));
    }

    /**
     * Creates a {@link BeanEntityDecorator}.
     *
     * @param bean the bean wrapping the entity
     */
    public BeanEntityDecorator(IMObjectBean bean) {
        super(bean.getObject(Entity.class));
        this.bean = bean;
    }

    /**
     * Returns the bean.
     *
     * @return the bean
     */
    public IMObjectBean getBean() {
        return bean;
    }
}