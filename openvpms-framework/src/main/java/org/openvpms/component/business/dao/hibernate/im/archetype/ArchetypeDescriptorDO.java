/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.archetype;

import org.openvpms.component.business.dao.hibernate.im.common.AuditableIMObjectDO;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.archetype.descriptor.DescriptorException;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;

import java.util.Map;


/**
 * Data object interface corresponding to the {@link ArchetypeDescriptor} class.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
public interface ArchetypeDescriptorDO extends DescriptorDO, AuditableIMObjectDO {

    /**
     * Returns the archetype type.
     *
     * @return the archetype id
     */
    ArchetypeId getType();

    /**
     * Returns the java class name.
     *
     * @return the class name
     */
    String getClassName();

    /**
     * Sets the java class name.
     *
     * @param className the class name
     */
    void setClassName(String className);

    /**
     * Determines if this is the latest version of the archetype.
     *
     * @return {@code true} if this is the latest version
     */
    boolean isLatest();

    /**
     * Determines if this is the latest version of the archetype.
     *
     * @param latest {@code true} if this is the latest version
     */
    void setLatest(boolean latest);

    /**
     * Determines if this is a primary archetype.
     *
     * @return {@code true} if this is a primary archetype
     */
    boolean isPrimary();

    /**
     * Determines if this is a primary archetype.
     *
     * @param primary {@code true} if this is a primary archetype
     */
    void setPrimary(boolean primary);

    /**
     * Determines if there should only be a single active instance of this archetype.
     *
     * @return {@code true} there should only be a single active instance, {@code false} if there can be multiple
     * instances
     */
    boolean isSingleton();

    /**
     * Determines if there should only be a single active instance of this archetype.
     *
     * @param singleton if {@code true} there should only be a single active instance; if {@code false} there can be
     *                  multiple instances
     */
    void setSingleton(boolean singleton);

    /**
     * Add a node descriptor to this archetype descriptor.
     *
     * @param node the node descriptor to add
     * @throws DescriptorException if a node descriptor exists with the same name
     */
    void addNodeDescriptor(NodeDescriptorDO node);

    /**
     * Removes the specified node descriptor.
     *
     * @param node the node descriptor to remove
     */
    void removeNodeDescriptor(NodeDescriptorDO node);

    /**
     * Return the top-level node descriptors, keyed on name.
     *
     * @return the top-level node descriptors
     */
    Map<String, NodeDescriptorDO> getNodeDescriptors();

    /**
     * Return all the node descriptors for this archetype, keyed on name.
     * <p/>
     * This flattens out the node descriptor heirarchy.
     *
     * @return the node descriptors
     */
    Map<String, NodeDescriptorDO> getAllNodeDescriptors();

    /**
     * Returns the named node descriptor.
     *
     * @param name the node descriptor name
     * @return the corresponding node descriptor, or {@code null} if none is found
     */
    NodeDescriptorDO getNodeDescriptor(String name);

    /**
     * Returns the display name.
     *
     * @return the display name
     */
    String getDisplayName();

    /**
     * Sets the display name.
     *
     * @param displayName the display name
     */
    void setDisplayName(String displayName);
}
