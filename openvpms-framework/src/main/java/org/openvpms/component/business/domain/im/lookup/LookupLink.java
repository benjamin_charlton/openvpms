/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.lookup;

import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.common.IMObjectRelationship;
import org.openvpms.component.model.lookup.Lookup;

/**
 * Describes a link between two lookups.
 * <p>
 * This is used by {@link Lookup} to establish uni-directional links where both the source and targets may be queried.
 *
 * @author Tim Anderson
 */
public class LookupLink extends IMObjectRelationship implements org.openvpms.component.model.lookup.LookupLink {

    /**
     * Default constructor.
     */
    public LookupLink() {
        super();
    }

    /**
     * Constructs a {@link LookupLink}
     *
     * @param archetypeId the archetype identifier
     */
    public LookupLink(ArchetypeId archetypeId) {
        super(archetypeId);
    }
}
