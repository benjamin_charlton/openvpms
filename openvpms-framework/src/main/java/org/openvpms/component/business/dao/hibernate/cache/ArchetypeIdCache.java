/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.cache;

import org.openvpms.component.business.dao.hibernate.interceptor.IMObjectInterceptor;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.archetype.ReadOnlyArchetypeId;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.IMObjectReference;

import java.util.HashMap;
import java.util.Map;

/**
 * Caches {@link ArchetypeId} instances.
 * <p/>
 * The number of {@link ArchetypeId}s should be small due to there being a limited no. of archetypes, however
 * each {@link IMObject} and {@link IMObjectReference} is populated with a new one each time it is loaded from the
 * database.
 * <p/>
 * This class exists to replace {@link ArchetypeId}s loaded by Hibernate with a cached version, before the object
 * is returned. While it doesn't stop the no. of objects being created, it does mean that most of them will be
 * garbage collected.
 *
 * @author Tim Anderson
 * @see IMObjectInterceptor
 */
public class ArchetypeIdCache {

    /**
     * The cache.
     */
    private final Map<String, ArchetypeId> ids = new HashMap<>();

    /**
     * Returns the cached instance of an {@link ArchetypeId}, creating one if necessary.
     *
     * @param archetypeId the uncached instance
     * @return the cached instance. This is read-only
     */
    public ArchetypeId get(ArchetypeId archetypeId) {
        return get(archetypeId.getShortName(), archetypeId.getVersion());
    }

    /**
     * Returns the cached instance of an {@link ArchetypeId}, creating one if necessary.
     *
     * @param archetype the archetype
     * @param version   the archetype version
     * @return the cached instance. This is read-only
     */
    public synchronized ArchetypeId get(String archetype, String version) {
        String qualified = archetype + "." + version;
        return ids.computeIfAbsent(qualified, s -> new ReadOnlyArchetypeId(archetype, version));
    }

}
