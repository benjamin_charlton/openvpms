/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.plugin;

import org.openvpms.component.business.dao.hibernate.im.common.AuditableIMObjectDO;
import org.openvpms.component.business.domain.im.plugin.Plugin;

import java.sql.Blob;

/**
 * Data object interface corresponding to the {@link Plugin} class.
 *
 * @author Tim Anderson
 */
public interface PluginDO extends AuditableIMObjectDO {

    /**
     * Returns the plugin key.
     *
     * @return the plugin key
     */
    String getKey();

    /**
     * Sets the plugin key
     *
     * @param key the plugin key
     */
    void setKey(String key);

    /**
     * Returns the plugin binary data.
     *
     * @return the the plugin binary data
     */
    Blob getData();

    /**
     * Sets the plugin binary data.
     *
     * @param data the binary data
     */
    void setData(Blob data);

}