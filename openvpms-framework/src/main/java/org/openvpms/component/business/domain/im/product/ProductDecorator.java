/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.product;

import org.openvpms.component.business.domain.im.common.EntityDecorator;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;

import java.util.Set;

/**
 * Decorator for {@link Product}.
 *
 * @author Tim Anderson
 */
public class ProductDecorator extends EntityDecorator implements Product {

    /**
     * Constructs a {@link ProductDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public ProductDecorator(Entity peer) {
        super(peer);
    }

    /**
     * Returns the product prices.
     *
     * @return the product prices.
     */
    @Override
    public Set<ProductPrice> getProductPrices() {
        return getPeer().getProductPrices();
    }

    /**
     * Adds a price.
     *
     * @param price the product price to add
     */
    @Override
    public void addProductPrice(ProductPrice price) {
        getPeer().addProductPrice(price);
    }

    /**
     * Removes a price.
     *
     * @param price the product price to add
     */
    @Override
    public void removeProductPrice(ProductPrice price) {
        getPeer().removeProductPrice(price);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected Product getPeer() {
        return (Product) super.getPeer();
    }
}
