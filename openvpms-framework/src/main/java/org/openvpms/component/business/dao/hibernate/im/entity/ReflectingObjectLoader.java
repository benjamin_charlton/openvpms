/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.entity;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * An {@link ObjectLoader} that uses reflection to load collections.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class ReflectingObjectLoader implements ObjectLoader {

    /**
     * Cache of collection methods, keyed on their class.
     */
    private final Map<Class<?>, Method[]> collectionMethods = new HashMap<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ReflectingObjectLoader.class);

    /**
     * Loads an object.
     *
     * @param object the object to load
     * @throws HibernateException for any hibernate error
     */
    public void load(Object object) {
        load(object, new HashSet<>());
    }

    /**
     * Recursively loads an object.
     *
     * @param object the object to load
     * @param loaded the set of objects already loaded
     * @throws HibernateException for any hibernate error
     */
    protected void load(Object object, Set<Object> loaded) {
        Hibernate.initialize(object);
        loaded.add(object);
        Method[] methods = getCollectionMethods(object);
        for (Method method : methods) {
            Collection<?> collection = null;
            try {
                collection = (Collection<?>) method.invoke(object);
            } catch (Exception exception) {
                log.warn(exception.getMessage(), exception);
            }
            if (collection != null) {
                for (Object elt : collection) {
                    if (!loaded.contains(elt)) {
                        load(elt, loaded);
                    }
                }
            }
        }
    }

    /**
     * Returns the collection methods for an object.
     *
     * @param object the object
     * @return the collection methods
     */
    private synchronized Method[] getCollectionMethods(Object object) {
        Class<?> clazz = object.getClass();
        Method[] result = collectionMethods.get(clazz);
        if (result == null) {
            List<Method> list = new ArrayList<>();
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                if (isCollectionGetter(method)) {
                    list.add(method);
                }
            }
            result = list.toArray(new Method[0]);
            collectionMethods.put(clazz, result);
        }
        return result;
    }

    /**
     * Determines if a method is a 'get' method returning a collection.
     *
     * @param method the method to check
     * @return {@code true} if the method returns a collection, otherwise {@code false}
     */
    private boolean isCollectionGetter(Method method) {
        Class<?>[] paramTypes = method.getParameterTypes();
        Class<?> returnType = method.getReturnType();
        return Modifier.isPublic(method.getModifiers())
               && paramTypes.length == 0
               && Collection.class.isAssignableFrom(returnType)
               && method.getName().startsWith("get");
    }
}
