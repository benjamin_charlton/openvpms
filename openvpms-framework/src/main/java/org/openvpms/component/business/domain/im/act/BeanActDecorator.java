/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.act;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.IMObjectBeanFactory;

/**
 * An {@link ActDecorator} that wraps the act in an {@link IMObjectBean}..
 *
 * @author Tim Anderson
 */
public class BeanActDecorator extends ActDecorator {

    /**
     * The bean.
     */
    private final IMObjectBean bean;

    /**
     * Constructs a {@link BeanActDecorator}.
     *
     * @param peer    the peer to delegate to
     * @param factory the bean factory
     */
    public BeanActDecorator(Act peer, IMObjectBeanFactory factory) {
        this(factory.getBean(peer));
    }

    /**
     * Creates a {@link BeanActDecorator}.
     *
     * @param bean the bean wrapping the act
     */
    public BeanActDecorator(IMObjectBean bean) {
        super(bean.getObject(Act.class));
        this.bean = bean;
    }

    /**
     * Returns the bean.
     *
     * @return the bean
     */
    public IMObjectBean getBean() {
        return bean;
    }
}
