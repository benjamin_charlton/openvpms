/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.act;

import org.openvpms.component.business.dao.hibernate.im.common.IMObjectRelationshipDOImpl;


/**
 * Implementation of the {@link ActRelationshipDO} interface.
 *
 * @author Tim Anderson
 */
public class ActRelationshipDOImpl extends IMObjectRelationshipDOImpl
        implements ActRelationshipDO {

    /**
     * The relationship sequence.
     */
    private int sequence;

    /**
     * Indicates whether the relationship is one of parent-child. This means
     * that the parent is the owner of the relationship and is responsible for
     * managing its lifecycle. When the parent is deleted then it will also
     * delete the child.
     */
    private boolean parentChildRelationship;


    /**
     * Default constructor.
     */
    public ActRelationshipDOImpl() {
        super();
    }

    /**
     * Returns the relationship sequence.
     * <p/>
     * This may be used to help order relationships.
     *
     * @return the relationship sequence
     */
    @Override
    public int getSequence() {
        return sequence;
    }

    /**
     * Sets the relationship sequence.
     * <p/>
     * This may be used to help order relationships.
     *
     * @param sequence the relationship sequence
     */
    @Override
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     * Determines if this is a parent/child relationship between two acts.
     *
     * @param parentChildRelationship if {@code true} it is a parent/child relationship
     */
    public void setParentChildRelationship(boolean parentChildRelationship) {
        this.parentChildRelationship = parentChildRelationship;
    }

    /**
     * Determines if this is a parent/child relationship between two acts.
     * If {@code true} it indicates that the parent act is the owner of the
     * relationship and is responsible for managing its lifecycle. When the
     * parent act is deleted, then the child act must also be deleted.
     *
     * @return {@code true} if this is a parent/child relationship
     */
    public boolean isParentChildRelationship() {
        return parentChildRelationship;
    }

}
