package org.openvpms.component.system.common.crypto;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.keygen.KeyGenerators;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Utility to generate a key containing a salt and password for use with {@link Base64EncodingPasswordEncryptor}.
 * <p/>
 * Any generated key needs to be stored securely.
 *
 * @author Tim Anderson
 */
public class KeyGenerator {

    /**
     * Generates a random password and salt, and encodes them in a single key.
     *
     * @param minLength the minimum password length
     * @param maxLength the maximum password length
     * @return the encoded key
     */
    public static String generate(int minLength, int maxLength) {
        SecureRandom random = new SecureRandom();
        int count;
        if (minLength == maxLength) {
            count = maxLength;
        } else {
            count = random.nextInt(maxLength - minLength) + minLength;
        }

        // generate a password of length=count, using printable ASCII characters
        String password = RandomStringUtils.random(count, 33, 126, false, false, null, random);
        byte[] salt = KeyGenerators.secureRandom(16).generateKey();
        String key = encode64(salt) + ":" + password;
        return encode64(key);
    }

    /**
     * Decodes a salt and password encoded in a key.
     *
     * @param key the key
     * @return the salt and password
     */
    public static String[] decode(String key) {
        String decoded = decode64(key);
        int index = decoded.indexOf(':');
        if (index == -1) {
            throw new IllegalStateException("Invalid key");
        }
        String salt = decoded.substring(0, index);
        String hex = new String(Hex.encode(Base64.getDecoder().decode(salt)));
        String password = decoded.substring(index + 1);
        return new String[]{hex, password};
    }

    /**
     * Base64 encodes a string.
     *
     * @param string the string to encode
     * @return the encoded string
     */
    private static String encode64(String string) {
        return encode64(string.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Base64 encodes a byte array.
     *
     * @param bytes the bytes to encode
     * @return the encoded string
     */
    private static String encode64(byte[] bytes) {
        return Base64.getEncoder().withoutPadding().encodeToString(bytes);
    }

    /**
     * Decodes a base64 encoded string.
     *
     * @param string the string to decode
     * @return the decoded string
     */
    private static String decode64(String string) {
        return new String(Base64.getDecoder().decode(string), StandardCharsets.UTF_8);
    }
}
