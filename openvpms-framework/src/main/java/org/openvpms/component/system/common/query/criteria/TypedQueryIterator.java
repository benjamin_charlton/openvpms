/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.query.criteria;

import org.openvpms.component.query.TypedQuery;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * An {@link Iterator} over a {@link TypedQuery}.
 *
 * @author Tim Anderson
 */
public class TypedQueryIterator<T> implements Iterator<T> {

    /**
     * The query.
     */
    private final TypedQuery<T> query;

    /**
     * The page size.
     */
    private final int pageSize;

    /**
     * The current page.
     */
    private List<T> page;

    /**
     * The current position.
     */
    private int position = 0;

    /**
     * The current iterator.
     */
    private Iterator<T> iterator;

    /**
     * Constructs a {@link TypedQueryIterator}.
     *
     * @param query    the query
     * @param pageSize the page size
     */
    public TypedQueryIterator(TypedQuery<T> query, int pageSize) {
        this(query, 0, pageSize);
    }

    /**
     * Constructs a {@link TypedQueryIterator}.
     *
     * @param query    the query
     * @param position the starting position
     * @param pageSize the page size
     */
    public TypedQueryIterator(TypedQuery<T> query, int position, int pageSize) {
        this.position = position;
        this.query = query;
        this.pageSize = pageSize;
    }

    /**
     * Returns {@code true} if the iteration has more elements.
     * (In other words, returns {@code true} if {@link #next} would
     * return an element rather than throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    @Override
    public boolean hasNext() {
        return getIterator().hasNext();
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    @Override
    public T next() {
        return getIterator().next();
    }

    /**
     * Returns the iterator, advancing it to the next page if required.
     *
     * @return the iterator
     */
    protected Iterator<T> getIterator() {
        if (page == null || !iterator.hasNext()) {
            if (page == null || page.size() == pageSize) {
                query.setMaxResults(pageSize);
                query.setFirstResult(position);
                position += pageSize;
                page = query.getResultList();
                iterator = page.iterator();
            }
        }
        return iterator;
    }
}
