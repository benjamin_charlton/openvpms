/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.loader;

import org.openvpms.tools.archetype.comparator.DescriptorChange;

/**
 * Updates a descriptor.
 *
 * @author Tim Anderson
 */
public abstract class DescriptorUpdater<T> {

    /**
     * Updates a descriptor.
     *
     * @param target the descriptor to update
     * @param source the descriptor to update from
     * @return {@code true} if the descriptor was updated, {@code false} if no changes were applied
     */
    public abstract boolean update(T target, T source);

    /**
     * Returns the new value of a change, cast to a String.
     *
     * @param change the change
     * @return the new value. May be {@code null}
     */
    protected String toString(DescriptorChange<?> change) {
        return (String) change.getNewVersion();
    }

    /**
     * Returns the new value of a change, cast to a boolean.
     *
     * @param change the change
     * @return the new value
     */
    protected boolean toBoolean(DescriptorChange<?> change) {
        Boolean result = (Boolean) change.getNewVersion();
        return result != null && result;
    }

    /**
     * Returns the new value of a change, cast to an int.
     *
     * @param change the change
     * @return the new value
     */
    protected int toInt(DescriptorChange<?> change) {
        return toInt(change, 0);
    }

    /**
     * Returns the new value of a change, cast to an int.
     *
     * @param change       the change
     * @param defaultValue the default value, if the new version is {@code null}
     * @return the new value
     */
    protected int toInt(DescriptorChange<?> change, int defaultValue) {
        Integer result = (Integer) change.getNewVersion();
        return result != null ? result : defaultValue;
    }

}