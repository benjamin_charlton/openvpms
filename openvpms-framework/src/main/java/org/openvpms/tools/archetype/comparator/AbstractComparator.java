/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.comparator;

import org.openvpms.component.model.object.IMObject;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Base class for archetype comparator.
 *
 * @author Tim Anderson
 */
public class AbstractComparator {

    /**
     * Determines the keys that have been added between two versions of a map.
     *
     * @param oldVersion the old version
     * @param newVersion the new version
     * @return the added keys
     */
    protected <T> Set<String> getAdded(Map<String, T> oldVersion, Map<String, T> newVersion) {
        Set<String> result = new HashSet<String>(newVersion.keySet());
        result.removeAll(oldVersion.keySet());
        return result;
    }

    /**
     * Determines the keys that have been deleted between two versions of a map.
     *
     * @param oldVersion the old version
     * @param newVersion the new version
     * @return the deleted keys
     */
    protected <T> Set<String> getDeleted(Map<String, T> oldVersion, Map<String, T> newVersion) {
        Set<String> result = new HashSet<>(oldVersion.keySet());
        result.removeAll(newVersion.keySet());
        return result;
    }

    /**
     * Determines the keys that have been retained between two versions of a map.
     *
     * @param oldVersion the old version
     * @param newVersion the new version
     * @return the retained keys
     */
    protected <T> Set<String> getRetained(Map<String, T> oldVersion, Map<String, T> newVersion) {
        Set<String> result = new HashSet<>(oldVersion.keySet());
        result.retainAll(newVersion.keySet());
        return result;
    }

    /**
     * Converts a set of objects to a map keyed on name.
     *
     * @param objects the objects
     * @return the corresponding map
     */
    protected <T extends IMObject> Map<String, T> toMap(Set<T> objects) {
        Map<String, T> result = new LinkedHashMap<>();
        for (T object : objects) {
            result.put(object.getName(), object);
        }
        return result;
    }

}
