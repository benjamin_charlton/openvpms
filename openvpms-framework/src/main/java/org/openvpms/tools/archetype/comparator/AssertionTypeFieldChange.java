/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.comparator;

import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor;

/**
 * Tracks a field change between versions of an {@link AssertionTypeDescriptor}.
 *
 * @author Tim Anderson
 */
public class AssertionTypeFieldChange extends DescriptorChange<Object> {

    public enum Field {
        NAME("name"),
        PROPERTY_ARCHETYPE("propertyArchetype");

        private final String displayName;

        Field(String displayName) {
            this.displayName = displayName;
        }

        @Override
        public String toString() {
            return displayName;
        }
    }

    /**
     * The field.
     */
    private final Field field;

    /**
     * Constructs a {@link AssertionTypeFieldChange}.
     * <p/>
     * Note: at least one of the arguments must be non-null;
     *
     * @param oldVersion the old version. May be {@code null}
     * @param newVersion the new version. May be {@code null}
     */
    public AssertionTypeFieldChange(Field field, Object oldVersion, Object newVersion) {
        super(oldVersion, newVersion);
        this.field = field;
    }

    /**
     * Returns the field.
     *
     * @return the field
     */
    public Field getField() {
        return field;
    }
}