<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="act.patientAlert.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.act.Act" displayName="Patient Alert">
        <node name="id" path="/id" type="java.lang.Long" hidden="true" readOnly="true"/>
        <node name="created" type="java.util.Date" path="/created" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="createdBy" path="/createdBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="updated" type="java.util.Date" path="/updated" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="updatedBy" path="/updatedBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="name" type="java.lang.String" path="/name" hidden="true" minCardinality="1" derived="true"
              derivedValue="'Alert'"/>
        <node name="description" type="java.lang.String" path="/description" hidden="true" derived="true"
              derivedValue="concat(openvpms:get(., 'alertType.entity.name'), expr:concatIf(' - ', /reason), expr:concatIf(', End Date: ', date:formatDate(/activityEndTime)))"/>
        <node displayName="Date" name="startTime" path="/activityStartTime" type="java.util.Date" minCardinality="1"
              defaultValue="java.util.Date.new()"/>
        <node displayName="End Date" name="endTime" path="/activityEndTime" type="java.util.Date" minCardinality="0"/>
        <node name="patient" path="/participations" type="java.util.HashSet" minCardinality="1" maxCardinality="1"
              filter="participation.patient" hidden="true"/>
        <node name="alertType" path="/participations" type="java.util.HashSet" minCardinality="1" maxCardinality="1"
              filter="participation.patientAlertType"/>
        <node name="reason" path="/reason" type="java.lang.String" maxLength="100"/>
        <node name="product" path="/participations" type="java.util.HashSet" minCardinality="0" maxCardinality="1"
              filter="participation.product"/>
        <node name="clinician" path="/participations" type="java.util.HashSet" minCardinality="0" maxCardinality="1"
              filter="participation.clinician"/>
        <node name="status" path="/status" type="java.lang.String" minCardinality="1" defaultValue="'IN_PROGRESS'">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="IN_PROGRESS" value="In Progress"/>
                    <property name="COMPLETED" value="Completed"/>
                </propertyList>
            </assertion>
        </node>
        <node name="notes" displayName="Notes" path="/details/notes" type="java.lang.String" minCardinality="0"
              maxLength="2000"/>
        <node name="invoiceItem" path="/targetActRelationships" type="java.util.HashSet"
              baseName="TargetActRelationship" minCardinality="0" maxCardinality="1" hidden="true"
              filter="actRelationship.invoiceItemAlert" readOnly="true"/>
    </archetype>
</archetypes>
