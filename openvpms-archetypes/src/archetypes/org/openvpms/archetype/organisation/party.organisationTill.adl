<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="party.organisationTill.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.party.Party" displayName="Till">
        <node name="id" path="/id" type="java.lang.Long" readOnly="true"/>
        <node name="name" displayName="Name" type="java.lang.String" path="/name" minCardinality="1" maxLength="100">
            <assertion name="propercase"/>
        </node>
        <node name="description" type="java.lang.String"
              path="/description" hidden="true" derived="true"
              derivedValue="concat('Last Cleared : ', expr:if(boolean(/details/lastCleared), date:formatDateTime(/details/lastCleared, 'medium', 'short'), 'Never'), ', Cash Float : ',/details/tillFloat)"/>
        <node name="tillFloat" path="/details/tillFloat"
              type="org.openvpms.component.business.domain.im.datatypes.quantity.Money"/>
        <node name="lastCleared" path="/details/lastCleared" type="java.util.Date"/>
        <node name="active" path="/active" type="java.lang.Boolean" defaultValue="true()"/>
        <node name="printerName" path="/details/printerName" type="java.lang.String" minCardinality="0"/>
        <node name="drawerCommand" path="/details/drawerCommand" type="java.lang.String" minCardinality="0">
            <assertion name="regularExpression">
                <property name="expression" value="(^$)|(^(\d+)(,\s*\d+)*$)"/>
                <errorMessage>The Drawer Command must contain a comma separated list of values in the range 0..255
                </errorMessage>
            </assertion>
        </node>
        <node name="terminals" displayName="EFTPOS Terminals" path="/entityLinks" type="java.util.HashSet"
              baseName="EntityLink" minCardinality="0" maxCardinality="*" filter="entityLink.tillEFTPOSTerminal"/>
        <node name="locations" path="/targetEntityRelationships"
              type="java.util.HashSet" baseName="EntityRelationship" minCardinality="0" maxCardinality="*"
              filter="entityRelationship.locationTill">
            <assertion name="uniqueEntityRelationship">
                <errorMessage>Duplicate active till location relationship</errorMessage>
            </assertion>
        </node>
    </archetype>
</archetypes>
