<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <!-- =========================================================================================================== -->
    <!-- This archetype exists to manage the state transitions of act.patientInvestigation when ordered via the      -->
    <!-- Laboratory API                                                                                              -->
    <!-- =========================================================================================================== -->

    <archetype name="act.laboratoryOrder.1.0" latest="true" type="org.openvpms.component.business.domain.im.act.Act"
               displayName="Laboratory Order">
        <node name="id" path="/id" type="java.lang.Long" readOnly="true"/>
        <node name="created" type="java.util.Date" path="/created" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="createdBy" path="/createdBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="updated" type="java.util.Date" path="/updated" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="updatedBy" path="/updatedBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="name" type="java.lang.String" path="/name" hidden="true" readOnly="true"/>
        <node name="investigationId" path="/identities" type="java.util.HashSet" baseName="Identity" minCardinality="1"
              maxCardinality="1" filter="actIdentity.patientInvestigation"/>
        <node name="startTime" displayName="Date" path="/activityStartTime" type="java.util.Date"
              minCardinality="1" defaultValue="java.util.Date.new()"/>
        <node name="patient" path="/participations" type="java.util.HashSet" minCardinality="1" maxCardinality="1"
              filter="participation.patient" readOnly="true"/>
        <node name="orderId" displayName="Laboratory Order Id" path="/identities" type="java.util.HashSet"
              baseName="Identity" minCardinality="0" maxCardinality="1" filter="actIdentity.laboratoryOrder*"
              readOnly="true"/>
        <node name="laboratory" path="/participations" type="java.util.HashSet" minCardinality="1"
              maxCardinality="1" filter="participation.laboratory" readOnly="true"/>
        <node name="device" path="/participations" type="java.util.HashSet" minCardinality="0" maxCardinality="1"
              filter="participation.laboratoryDevice" readOnly="true"/>
        <node name="investigationType" path="/participations" type="java.util.HashSet" minCardinality="1"
              maxCardinality="1" filter="participation.investigationType" readOnly="true"/>
        <node name="location" path="/participations" type="java.util.HashSet" minCardinality="1"
              maxCardinality="1" filter="participation.location" readOnly="true"/>
        <node name="description" displayName="Notes" type="java.lang.String" path="/description" readOnly="true"/>
        <node name="clinician" path="/participations" type="java.util.HashSet" minCardinality="0" maxCardinality="1"
              filter="participation.clinician" readOnly="true"/>
        <node name="type" path="/details/type" type="java.lang.String" minCardinality="1">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="NEW" value="New Order"/>
                    <property name="CANCEL" value="Cancel Order"/>
                </propertyList>
            </assertion>
        </node>
        <node name="status" displayName="Order Status" path="/status" type="java.lang.String" minCardinality="1"
              defaultValue="'PENDING'" readOnly="true">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="PENDING" value="Pending"/>
                    <property name="QUEUED" value="Queued"/>
                    <property name="CONFIRM" value="Confirm"/>
                    <property name="SUBMITTING" value="Submitting"/>
                    <property name="SUBMITTED" value="Submitted"/>
                    <property name="ERROR" value="Error"/>
                    <property name="COMPLETED" value="Completed"/>
                    <property name="CANCELLED" value="Cancelled"/>
                </propertyList>
            </assertion>
        </node>
        <node name="status2" displayName="Result Status" path="/status2" type="java.lang.String" minCardinality="1"
              defaultValue="'PENDING'" readOnly="true">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="PENDING" value="Pending"/>
                    <property name="WAITING_FOR_SAMPLE" value="Waiting for Sample"/>
                    <property name="IN_PROGRESS" value="In Progress"/>
                    <property name="PARTIAL_RESULTS" value="Partial Results"/>
                    <property name="COMPLETED" value="Completed"/>
                </propertyList>
            </assertion>
        </node>
        <node name="tests" path="/participations" type="java.util.HashSet" minCardinality="0" maxCardinality="*"
              filter="participation.laboratoryTest" readOnly="true"/>
        <node name="investigation" path="/targetActRelationships" type="java.util.HashSet"
              baseName="TargetActRelationship" minCardinality="0" maxCardinality="1" hidden="true"
              filter="actRelationship.patientInvestigationOrder"/>
    </archetype>
</archetypes>
