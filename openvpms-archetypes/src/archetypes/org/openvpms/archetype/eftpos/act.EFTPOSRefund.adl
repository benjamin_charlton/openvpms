<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <!-- act.EFTPOSRefund is used by EFTPOS terminals to track the status of refunds -->
    <archetype name="act.EFTPOSRefund.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.act.FinancialAct"
               displayName="EFTPOS Refund">
        <node name="id" path="/id" type="java.lang.Long" hidden="true" readOnly="true"/>
        <node name="parentId" displayName="Refund Id" path="/identities" type="java.util.HashSet"
              baseName="Identity" minCardinality="0" maxCardinality="1" filter="actIdentity.EFTPOSParentId"
              readOnly="true"/>
        <node name="transactionId" path="/identities" type="java.util.HashSet"
              baseName="Identity" minCardinality="0" maxCardinality="1" filter="actIdentity.EFTPOSTransaction*"
              readOnly="true"/>
        <node name="name" type="java.lang.String" path="/name" hidden="true" minCardinality="1" derived="true"
              derivedValue="'EFT Refund'"/>
        <node name="description" type="java.lang.String" path="/description" hidden="true" readOnly="true"/>
        <node name="startTime" displayName="Date" path="/activityStartTime"
              type="java.util.Date" hidden="true" minCardinality="0"
              defaultValue="java.util.Date.new()" readOnly="true"/>
        <node name="amount" path="/total" type="org.openvpms.component.business.domain.im.datatypes.quantity.Money"
              defaultValue="0.0" readOnly="true"/>
        <node name="credit" path="/credit" type="java.lang.Boolean" defaultValue="true()" readOnly="true"
              hidden="true"/>
        <node name="total" path="/details/total"
              type="org.openvpms.component.business.domain.im.datatypes.quantity.Money" derived="true"
              derivedValue="/amount"/>
        <node name="status" path="/status" type="java.lang.String"
              minCardinality="1" defaultValue="'PENDING'" readOnly="true">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="PENDING" value="Pending"/>
                    <property name="IN_PROGRESS" value="In Progress"/>
                    <property name="APPROVED" value="Approved"/>
                    <property name="DECLINED" value="Declined"/>
                    <property name="ERROR" value="Error"/>
                    <property name="NO_TERMINAL" value="No Terminal"/>
                </propertyList>
                <errorMessage>Invalid Status</errorMessage>
            </assertion>
        </node>
        <node name="message" path="/details/message" type="java.lang.String" maxLength="5000" minCardinality="0"
              readOnly="true"/>
        <node name="cardType" path="/details/cardType" type="java.lang.String" readOnly="true" minCardinality="0"/>
        <node name="authorisationCode" path="/identities" type="java.util.HashSet"
              baseName="Identity" minCardinality="0" maxCardinality="1" filter="actIdentity.EFTPOSAuthCode"
              readOnly="true"/>
        <node name="responseCode" path="/details/responseCode" type="java.lang.String" readOnly="true"
              minCardinality="0"/>
        <node name="response" path="/details/response" type="java.lang.String" readOnly="true"
              minCardinality="0" maxLength="5000"/>
        <node name="maskedCardNumber" path="/details/maskedCardNumber" type="java.lang.String" readOnly="true"
              minCardinality="0"/>
        <node name="retrievalReferenceNumber" displayName="RRN" path="/identities" type="java.util.HashSet"
              baseName="Identity" minCardinality="0" maxCardinality="1" filter="actIdentity.EFTPOSRRN"
              readOnly="true"/>
        <node name="systemTraceAuditNumber" displayName="STAN" path="/identities" type="java.util.HashSet"
              baseName="Identity" minCardinality="0" maxCardinality="1" filter="actIdentity.EFTPOSSTAN"
              readOnly="true"/>
        <node name="customer" path="/participations" type="java.util.HashSet"
              minCardinality="1" maxCardinality="1" filter="participation.customer" readOnly="true"/>
        <node name="terminal" path="/participations" type="java.util.HashSet"
              minCardinality="0" maxCardinality="1" filter="participation.EFTPOSTerminal" readOnly="true"/>
        <node name="location" path="/participations" type="java.util.HashSet"
              minCardinality="1" maxCardinality="1" filter="participation.location" readOnly="true"/>
        <node name="receipts" path="/sourceActRelationships"
              type="java.util.HashSet" baseName="SourceActRelationship" minCardinality="0" maxCardinality="*"
              readOnly="true" filter="actRelationship.EFTPOSReceipt"/>
        <node name="transaction" displayName="Refund" path="/targetActRelationships"
              type="java.util.HashSet" baseName="TargetActRelationship" minCardinality="0" maxCardinality="1"
              filter="actRelationship.customerAccountRefundEFTPOS" hidden="true"/>
        <node name="identities" path="/identities" type="java.util.HashSet" baseName="Identity" minCardinality="0"
              maxCardinality="*" readOnly="true" hidden="true" filter="actIdentity.EFTPOS*"/>
    </archetype>
</archetypes>
