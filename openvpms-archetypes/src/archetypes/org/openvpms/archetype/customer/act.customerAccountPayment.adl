<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="act.customerAccountPayment.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.act.FinancialAct" displayName="Payment">
        <node name="id" path="/id" type="java.lang.Long" readOnly="true"/>
        <node name="created" type="java.util.Date" path="/created" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="createdBy" path="/createdBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="updated" type="java.util.Date" path="/updated" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="updatedBy" path="/updatedBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="name" type="java.lang.String" path="/name" hidden="true" minCardinality="1" derived="true"
              derivedValue="'Customer Payment'"/>
        <node name="description" type="java.lang.String" path="/description" hidden="true" readOnly="true"
              minCardinality="0"/>
        <node name="startTime" displayName="Date" path="/activityStartTime"
              type="java.util.Date" minCardinality="1" defaultValue="java.util.Date.new()" readOnly="true"/>
        <node name="amount" path="/total"
              type="org.openvpms.component.business.domain.im.datatypes.quantity.Money" minCardinality="1"
              defaultValue="'0.0'" readOnly="true">
            <assertion name="positive">
                <errorMessage>Value must be > 0.0</errorMessage>
            </assertion>
        </node>
        <node name="allocatedAmount" path="/allocatedAmount"
              type="org.openvpms.component.business.domain.im.datatypes.quantity.Money" minCardinality="1"
              defaultValue="'0.0'" hidden="true" readOnly="true"/>
        <node name="credit" path="/credit" type="java.lang.Boolean" defaultValue="true()" readOnly="true"
              hidden="true"/>
        <node name="printed" path="/printed" type="java.lang.Boolean" defaultValue="false()"/>
        <node name="hide" path="/details/hide" type="java.lang.Boolean" defaultValue="false()" hidden="true"
              readOnly="true"/>
        <node name="status" path="/status" type="java.lang.String" minCardinality="1" defaultValue="'POSTED'">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="IN_PROGRESS" value="In Progress"/>
                    <property name="ON_HOLD" value="On Hold"/>
                    <property name="POSTED" value="Finalised"/>
                </propertyList>
                <errorMessage>Invalid Payment Status</errorMessage>
            </assertion>
        </node>
        <node name="notes" path="/details/notes" type="java.lang.String" minCardinality="0" maxLength="300"/>
        <node name="reference" displayName="Reference" path="/details/reference" type="java.lang.String"
              minCardinality="0" maxLength="20"/>
        <node name="till" path="/participations" type="java.util.HashSet"
              minCardinality="1" maxCardinality="1"
              filter="participation.till"/>
        <node name="customer" path="/participations" type="java.util.HashSet"
              minCardinality="1" maxCardinality="1"
              filter="participation.customer" hidden="true"/>
        <node name="accountBalance" path="/participations" type="java.util.HashSet"
              minCardinality="0" maxCardinality="1"
              filter="participation.customerAccountBalance" hidden="true"/>
        <node name="location" path="/participations" type="java.util.HashSet"
              minCardinality="0" maxCardinality="1"
              filter="participation.location" readOnly="true"/>
        <node name="audit" path="/details/audit" type="java.lang.String" minCardinality="0" maxLength="5000"
              readOnly="true"/>
        <node name="items" path="/sourceActRelationships" type="java.util.HashSet" minCardinality="0" maxCardinality="*"
              filter="actRelationship.customerAccountPaymentItem"/>
        <node name="allocation" path="/targetActRelationships"
              type="java.util.HashSet" baseName="TargetActRelationship" minCardinality="0" maxCardinality="*"
              filter="actRelationship.customerAccountAllocation" hidden="true"/>
        <node name="reversal" path="/sourceActRelationships"
              type="java.util.HashSet" baseName="SourceActRelationship" minCardinality="0" maxCardinality="1"
              filter="actRelationship.customerAccountReversal" hidden="true"/>
        <node name="reverses" path="/targetActRelationships"
              type="java.util.HashSet" baseName="TargetActRelationship" minCardinality="0" maxCardinality="1"
              filter="actRelationship.customerAccountReversal" hidden="true"/>
        <node name="tillBalance" path="/targetActRelationships"
              type="java.util.HashSet" baseName="TargetActRelationship" minCardinality="0" maxCardinality="1"
              filter="actRelationship.tillBalanceItem" hidden="true" readOnly="true"/>
    </archetype>
</archetypes>
