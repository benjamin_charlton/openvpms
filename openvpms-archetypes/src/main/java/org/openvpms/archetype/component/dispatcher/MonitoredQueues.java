/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.component.dispatcher;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.MonitoringIMObjectCache;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages a set of {@link Queue}s where the queue owners are archetypes.
 * <p/>
 * The set of queues will change as their owners are added and deleted via the {@link IArchetypeService}.
 *
 * @author Tim Anderson
 */
public abstract class MonitoredQueues<O extends IMObject, Q>
        extends MonitoringIMObjectCache<O> implements Queues<O, Q> {

    /**
     * The queues, keyed on owner reference.
     */
    private final Map<Reference, Q> queues = Collections.synchronizedMap(new HashMap<>());

    /**
     * Constructs a {@link MonitoredQueues}.
     * <p/>
     * The caller must invoked {@link #load()} to load queues.
     *
     * @param service   the archetype service
     * @param archetype the queue owner archetype
     * @param ownerType the queue owner type
     */
    protected MonitoredQueues(IArchetypeService service, String archetype, Class<O> ownerType) {
        super(service, archetype, ownerType, false);
    }

    /**
     * Returns the queues.
     *
     * @return the queues
     */
    @Override
    public List<Q> getQueues() {
        return new ArrayList<>(queues.values());
    }

    /**
     * Returns a queue given its owner.
     *
     * @param owner the queue owner
     * @return the queue
     */
    @Override
    public Q getQueue(O owner) {
        Q queue;
        synchronized (queues) {
            queue = queues.get(owner.getObjectReference());
            if (queue == null) {
                queue = createQueue(owner);
                this.queues.put(owner.getObjectReference(), queue);
            }
        }
        return queue;
    }

    /**
     * Crates a new queue.
     *
     * @param owner the queue owner
     * @return a new queue
     */
    protected abstract Q createQueue(O owner);

    /**
     * Invoked when an object is added to the cache.
     *
     * @param object the added object
     */
    @Override
    protected void added(O object) {
        queues.put(object.getObjectReference(), createQueue(object));
    }

    /**
     * Invoked when an object is removed from the cache.
     *
     * @param object the removed object
     */
    @Override
    protected void removed(O object) {
        queues.remove(object.getObjectReference());
    }
}
