/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.component.dispatcher;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Manages a queue of objects for processing.
 *
 * @author Tim Anderson
 */
public abstract class Queue<T, O> {

    /**
     * The owner of the queue.
     */
    private final O owner;

    /**
     * Used to set a time when dispatching should resume.
     */
    private long waitUntil = -1;

    /**
     * The current object.
     */
    private T current;

    /**
     * The time when an object was last successfully processed.
     */
    private Date lastProcessed;

    /**
     * The error message if processing the last object was unsuccessful.
     */
    private Date lastError;

    /**
     * The error message if processing the last object was unsuccessful.
     */
    private String lastErrorMessage;

    /**
     * Determines if dispatching for this queue is suspended.
     */
    private boolean suspended;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(Queue.class);

    /**
     * Constructs an {@link Queue}.
     *
     * @param owner the owner of the queue
     */
    protected Queue(O owner) {
        this.owner = owner;
    }

    /**
     * Returns the owner of the queue.
     *
     * @return the queue owner
     */
    public O getOwner() {
        return owner;
    }

    /**
     * Retrieves, but does not remove, the first act in the queue.
     *
     * @return the head of this queue, or {@code null} if the queue is empty
     */
    public synchronized T peekFirst() {
        if (current == null) {
            getNext();
        }
        return current;
    }

    /**
     * Invoked when an object is processed.
     */
    public synchronized void processed() {
        if (current == null) {
            throw new IllegalStateException("No current object");
        }
        lastProcessed = new Date();
        processed(current, lastProcessed);
    }

    /**
     * Invoked when an exception occurs processing an object.
     *
     * @param exception the exception
     */
    public synchronized void error(Throwable exception) {
        Date now = new Date();
        String error = exception.getMessage();
        if (error == null) {
            error = ExceptionUtils.getMessage(exception);
        }
        log.error("Error received from {}: {}", toString(owner), error);
        completed(now, error);
    }

    /**
     * Determines if processing should be suspended.
     *
     * @param suspend if {@code true}, suspend processing, otherwise resume it
     */
    public synchronized void setSuspended(boolean suspend) {
        this.suspended = suspend;
    }

    /**
     * Determines if processing should be suspended.
     *
     * @return {@code true} if processing should be suspended
     */
    public synchronized boolean isSuspended() {
        return suspended;
    }

    /**
     * Set the time after which processing should continue.
     *
     * @return the time, in milliseconds, or {@code -1} if there is no delay
     */
    public synchronized long getWaitUntil() {
        return waitUntil;
    }

    /**
     * Set the time after which processing should continue.
     *
     * @param millis the time, in milliseconds, or {@code -1} if there is no delay
     */
    public synchronized void setWaitUntil(long millis) {
        this.waitUntil = millis;
    }

    /**
     * Returns the time of the last processed act.
     *
     * @return the time when an act was last processed, or {@code null} if none have been processed
     */
    public synchronized Date getProcessedTimestamp() {
        return lastProcessed;
    }

    /**
     * Returns the time of the last error.
     *
     * @return the time of the last error, or {@code null} if the last act was successfully processed
     */
    public synchronized Date getErrorTimestamp() {
        return lastError;
    }

    /**
     * Returns the error message of the last error.
     *
     * @return the last error message. May be {@code null}
     */
    public synchronized String getErrorMessage() {
        return lastErrorMessage;
    }

    /**
     * Returns the interval to wait after a failure.
     *
     * @return the interval, in seconds
     */
    public abstract int getRetryInterval();

    /**
     * Returns a string representation of the object.
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return toString(owner);
    }

    /**
     * Invoked when an object is processed.
     * <p/>
     * This implementation simply invokes {@code completed(null, null)}
     *
     * @param object    the processed object
     * @param processed the timestamp when it was processed
     */
    protected void processed(T object, Date processed) {
        completed(null, null);
    }

    /**
     * Retrieves the next object.
     *
     * @param owner the owner of the queue
     * @return the next object, or {@code null} if there is none
     */
    protected abstract T getNext(O owner);

    /**
     * Invoked after processing an object.
     *
     * @param errorDate the error date/time, or {@code null} if there was no error
     * @param error     the error message, or {@code null} if there was no error
     */
    protected void completed(Date errorDate, String error) {
        current = null;
        lastError = errorDate;
        lastErrorMessage = error;
    }

    /**
     * Returns a string representation of the owner.
     *
     * @param owner the queue owner
     * @return a string representation of the owner
     */
    protected String toString(O owner) {
        return owner.toString();
    }

    /**
     * Retrieves the next object
     * .
     */
    private void getNext() {
        current = getNext(owner);
    }
}
