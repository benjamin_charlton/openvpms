/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient.reminder;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.Location;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.IdConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.gte;
import static org.openvpms.component.system.common.query.Constraints.idEq;
import static org.openvpms.component.system.common.query.Constraints.isNull;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.notExists;
import static org.openvpms.component.system.common.query.Constraints.shortName;
import static org.openvpms.component.system.common.query.Constraints.sort;
import static org.openvpms.component.system.common.query.Constraints.subQuery;


/**
 * Queries <em>act.patientReminder</em> acts.
 * <p/>
 * The acts are sorted on customer name, patient name and startTime.
 *
 * @author Tim Anderson
 */
public class ReminderQuery {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The reminder type.
     */
    private Entity reminderType;

    /**
     * The 'from' due date.
     */
    private Date from;

    /**
     * The 'to' due date.
     */
    private Date to;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The location.
     */
    private Location location = Location.ALL;

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * Patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * Reminder type node name.
     */
    private static final String REMINDER_TYPE = "reminderType";

    /**
     * Practice node name.
     */
    private static final String PRACTICE = "practice";

    /**
     * Source node name.
     */
    private static final String SOURCE = "source";

    /**
     * Target node name.
     */
    private static final String TARGET = "target";

    /**
     * Entity node name.
     */
    private static final String ENTITY = "entity";

    /**
     * Name node.
     */
    private static final String NAME = "name";


    /**
     * Constructs a {@link ReminderQuery}.
     *
     * @param service the archetype service
     */
    public ReminderQuery(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Sets the reminder type.
     *
     * @param reminderType an <em>entity.reminderType</em>. If {@code null} indicates to query all reminder types
     */
    public void setReminderType(Entity reminderType) {
        this.reminderType = reminderType;
    }

    /**
     * Sets the 'from' date.
     * <p/>
     * This excludes all reminders with a due date prior to the specified date.
     * <p/>
     * Any time component is ignored.
     *
     * @param from the from date. If {@code null} don't set a lower bound for due dates
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * Sets the 'to' date.
     * <p/>
     * This filters excludes reminders with a due date after the specified date.
     * <p/>
     * Any time component is ignored.
     *
     * @param to the to date. If {@code null} don't set an upper bound for due
     *           dates
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer. May be {@code null}
     */
    public void setCustomer(Party customer) {
        this.customer = customer;
    }

    /**
     * Sets the location to query.
     * <p/>
     * Defaults to {@link Location#ALL}.
     *
     * @param location the location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Returns an iterator over the reminder acts matching the query
     * criteria.
     * Note that each iteration may throw {@link ArchetypeServiceException}.
     * Also note that the behaviour is undefined if any of the attributes
     * are modified while an iteration is in progress.
     *
     * @return an iterator over the reminder acts
     */
    public Iterable<Act> query() {
        return () -> new IMObjectQueryIterator<>(service, createQuery());
    }

    /**
     * Executes the query.
     *
     * @return a list of the reminder acts matching the query criteria
     */
    public List<Act> execute() {
        List<Act> result = new ArrayList<>();
        for (Act act : query()) {
            result.add(act);
        }
        return result;
    }

    /**
     * Returns a new {@link ArchetypeQuery} matching the constraints.
     *
     * @return a new query
     */
    public ArchetypeQuery createQuery() {
        final String actAlias = "a";
        final String patientAlias = "p";
        final String customerAlias = "c";
        final String ownerAlias = "o";
        final String reminderTypeAlias = "r";
        final String participationAlias = "powner";

        ShortNameConstraint act = shortName(actAlias, ReminderArchetypes.REMINDER, true);
        ArchetypeQuery query = new ArchetypeQuery(act);
        query.setMaxResults(1000);

        query.add(eq("status", ReminderStatus.IN_PROGRESS));

        query.add(join(PATIENT, shortName(participationAlias, PatientArchetypes.PATIENT_PARTICIPATION, true)));
        query.add(new IdConstraint(actAlias, participationAlias + ".act"));
        query.add(shortName(ownerAlias, PatientArchetypes.PATIENT_OWNER, false));
        query.add(shortName(patientAlias, PatientArchetypes.PATIENT, true));
        ShortNameConstraint cust = shortName(customerAlias, CustomerArchetypes.PERSON, true);
        if (this.customer != null) {
            cust.add(eq("id", this.customer.getId()));
        }
        if (location.getPracticeLocation() != null) {
            cust.add(join(PRACTICE, "l2").add(eq(TARGET, location.getPracticeLocation())));
        }
        query.add(cust);

        if (location.isNone()) {
            query.add(notExists(subQuery(CustomerArchetypes.PERSON, "c2").add(
                    join(PRACTICE, "l2").add(idEq(customerAlias, "c2")))));
        }

        query.add(new IdConstraint(participationAlias + "." + ENTITY, patientAlias));
        query.add(new IdConstraint(patientAlias, ownerAlias + "." + TARGET));
        query.add(new IdConstraint(customerAlias, ownerAlias + "." + SOURCE));
        query.add(sort(customerAlias, NAME));
        query.add(sort(patientAlias, NAME));
        query.add(sort(actAlias, START_TIME));
        query.add(isNull(ownerAlias + ".activeEndTime")); // only use owner relationships that are open-ended

        ShortNameConstraint reminder = shortName(reminderTypeAlias, ReminderArchetypes.REMINDER_TYPE_PARTICIPATION,
                                                 true);
        if (reminderType != null) {
            query.add(join(REMINDER_TYPE, reminder).add(eq(ENTITY, reminderType)));
        } else {
            query.add(reminder);
            query.add(new IdConstraint(reminderTypeAlias + ".act", actAlias));
        }
        if (from != null) {
            query.add(gte(START_TIME, DateRules.getDate(from)));
        }
        if (to != null) {
            // remove any time component and add 1 day
            Date tempTo = DateRules.getDate(to);
            tempTo = DateRules.getDate(tempTo, 1, DateUnits.DAYS);
            query.add(Constraints.lt(START_TIME, tempTo));
        }
        return query;
    }


}