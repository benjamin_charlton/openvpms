/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.common.EntityRelationship;
import org.openvpms.component.business.domain.im.common.Participation;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.List;


/**
 * Document template helper.
 *
 * @author Tim Anderson
 */
public class TemplateHelper {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;


    /**
     * Constructs a {@link TemplateHelper}.
     *
     * @param service the archetype service
     */
    public TemplateHelper(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Retrieves a document template with matching name.
     *
     * @param name the document name
     * @return the document associated with an <em>act.documentTemplate</em>
     * having the specified name, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document getDocument(String name) {
        Document result = null;
        DocumentAct act = getDocumentAct(name);
        if (act != null) {
            result = (Document) get(act.getDocument());
        }
        return result;
    }

    /**
     * Retrieves a document template act with matching name.
     *
     * @param name the name
     * @return the first <em>act.documentTemplate</em> having the specified name, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentAct getDocumentAct(String name) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<DocumentAct> query = builder.createQuery(DocumentAct.class);
        Root<DocumentAct> root = query.from(DocumentAct.class, DocumentArchetypes.DOCUMENT_TEMPLATE_ACT);
        query.where(builder.equal(root.get("name"), name));
        query.orderBy(builder.asc(root.get("id")));

        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns an <em>entity.documentTemplate</em> with matching archetype
     * short name.
     *
     * @param shortName the archetype short name
     * @return the template corresponding to {@code shortName} or {@code null}
     * if none can be found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Entity getTemplateForArchetype(String shortName) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, DocumentArchetypes.DOCUMENT_TEMPLATE);
        Join<Entity, Lookup> typeJoin = root.join("type");
        typeJoin.on(builder.equal(typeJoin.get("code"), shortName));
        query.where(builder.equal(root.get("active"), true));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns an <em>entity.documentTemplate</em> for the specified archetype, associated with an organisation
     * location or practice.
     * <p/>
     * If there are multiple relationships, the lowest id will be returned.
     *
     * @param shortName    the archetype short name
     * @param organisation the <em>party.organisationLocation</em> or <em>party.organisationPractice</em>
     * @return the template corresponding to {@code shortName} or {@code null} if none can be found
     */
    public Entity getTemplateForArchetype(String shortName, Party organisation) {
        Entity result = null;
        IMObjectBean bean = service.getBean(organisation);
        List<EntityRelationship> list = bean.getValues("templates", EntityRelationship.class, Predicates.activeNow());
        list.sort((o1, o2) -> {
            long id1 = (o1.getSource() != null) ? o1.getSource().getId() : -1;
            long id2 = (o2.getSource() != null) ? o2.getSource().getId() : -1;
            return (Long.compare(id1, id2));
        });
        for (EntityRelationship relationship : list) {
            Entity template = (Entity) get(relationship.getSource());
            if (template != null && template.isActive() && hasArchetype(template, shortName)) {
                result = template;
                break;
            }
        }
        return result;
    }

    /**
     * Returns the document template for the specified archetype.
     *
     * @param shortName the archetype short name
     * @return the template corresponding to {@code shortName} or {@code null} if none can be found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentTemplate getDocumentTemplate(String shortName) {
        Entity result = getTemplateForArchetype(shortName);
        return (result != null) ? new DocumentTemplate(result, service) : null;
    }

    /**
     * Returns the document template for the specified archetype, associated with an organisation location or practice.
     * <p/>
     * If there are multiple relationships, the lowest id will be returned.
     *
     * @param shortName    the archetype short name
     * @param organisation the <em>party.organisationLocation</em> or <em>party.organisationPractice</em>
     * @return the template corresponding to {@code shortName} or {@code null} if none can be found
     */
    public DocumentTemplate getDocumentTemplate(String shortName, Party organisation) {
        Entity result = getTemplateForArchetype(shortName, organisation);
        return (result != null) ? new DocumentTemplate(result, service) : null;
    }

    /**
     * Retrieves a document template with matching archetype short name.
     *
     * @param shortName the archetype short name
     * @return the template corresponding to {@code shortName} or {@code null}
     * if none can be found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document getDocumentForArchetype(String shortName) {
        Document document = null;
        Entity template = getTemplateForArchetype(shortName);
        if (template != null) {
            DocumentAct act = getDocumentAct(template);
            if (act != null) {
                document = (Document) get(act.getDocument());
            }
        }
        return document;
    }

    /**
     * Returns the document associated with an <em>entity.documentTemplate</em>.
     *
     * @param template the template. An <em>entity.documentTemplate</em>
     * @return the corresponding document, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document getDocumentFromTemplate(Entity template) {
        DocumentAct act = getDocumentAct(template);
        if (act != null) {
            return (Document) get(act.getDocument());
        }
        return null;
    }

    /**
     * Returns the document act associated with an <em>entity.documentTemplate</em>.
     *
     * @param template the template. An <em>entity.documentTemplate</em>
     * @return the document act, or {@code null} if none exists
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentAct getDocumentAct(Entity template) {
        DocumentAct result = null;
        Participation participation = getDocumentParticipation(template);
        if (participation != null) {
            result = (DocumentAct) get(participation.getAct());
        }
        return result;
    }

    /**
     * Returns the first <em>participation.document</em> associated with an
     * <em>entity.documentTemplate</em>.
     *
     * @param template the document template entity
     * @return the participation, or {@code null} if none exists
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Participation getDocumentParticipation(Entity template) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Participation> query = builder.createQuery(Participation.class);
        Root<Participation> root = query.from(Participation.class, DocumentArchetypes.DOCUMENT_PARTICIPATION);
        query.where(builder.equal(root.get("entity"), template.getObjectReference()));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns the file name of the content associated with an <em>entity.documentTemplate</em>.
     *
     * @param template the document template entity
     * @return the file name, or {@code null} if none is found
     */
    public String getFileName(Entity template) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<DocumentAct> root = query.from(DocumentAct.class, DocumentArchetypes.DOCUMENT_TEMPLATE_ACT);
        Join<DocumentAct, Entity> join = root.join("template");
        query.select(root.get("fileName"));
        query.where(builder.equal(join.get("entity"), template.getObjectReference()));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Determine if a template is for a particular archetype.
     *
     * @param template  the template
     * @param shortName the archetype short name
     * @return {@code true} if the template has the archetype short name, otherwise {@code false}
     */
    private boolean hasArchetype(Entity template, String shortName) {
        IMObjectBean bean = service.getBean(template);
        Lookup type = bean.getObject("type", Lookup.class);
        return type != null && type.getCode().equals(shortName);
    }

    /**
     * Helper to return an object given its reference.
     *
     * @param ref the object reference. May be {@code null}
     * @return the object corresponding to ref or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    private IMObject get(Reference ref) {
        return (ref != null) ? service.get(ref) : null;
    }
}
