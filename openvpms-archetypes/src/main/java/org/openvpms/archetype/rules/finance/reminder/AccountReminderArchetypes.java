/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.reminder;

/**
 * Account reminder archetypes.
 *
 * @author Tim Anderson
 */
public class AccountReminderArchetypes {

    /**
     * SMS charge reminder archetype.
     */
    public static final String CHARGE_REMINDER_SMS = "act.customerChargeReminderSMS";

    /**
     * The account reminder job archetype.
     */
    public static final String ACCOUNT_REMINDER_JOB = "entity.jobAccountReminder";

    /**
     * The account reminder account archetype.
     */
    public static final String ACCOUNT_REMINDER_COUNT = "entity.accountReminderCount";

    /**
     * Default constructor.
     */
    private AccountReminderArchetypes() {
    }
}