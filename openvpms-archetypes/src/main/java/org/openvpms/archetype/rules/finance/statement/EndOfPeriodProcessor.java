/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.statement;

import org.openvpms.archetype.component.processor.Processor;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.invoice.InvoiceItemStatus;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.openvpms.archetype.rules.finance.statement.StatementProcessorException.ErrorCode.InvalidStatementDate;


/**
 * End-of-period statement processor.
 * <p>
 * This performs end-of-period for a customer. End-of-period processing
 * includes:
 * <ul>
 * <li>the addition of accounting fees, if the customer has overdue balances
 * that incur a fee; and
 * <li>the creation of closing and opening balance acts</li>
 * </ul>
 * <p>
 * End-of-period processing only occurs if the customer has no statement
 * on or after the specified statement date and:
 * <ul>
 * <li>there are COMPLETED invoices to be POSTED; or
 * <li>there is a non-zero balance; or
 * <li>there is a zero balance but there has been account activity since
 * the last opening balance</li>
 * </ul>
 *
 * @author Tim Anderson
 */
class EndOfPeriodProcessor implements Processor<Party> {

    /**
     * The statement date timestamp.
     */
    private final Date timestamp;

    /**
     * If {@code true}, post completed charges.
     */
    private final boolean postCompletedCharges;

    /**
     * The archetype service.
     */
    private final IArchetypeRuleService service;

    /**
     * Statement act query helper.
     */
    private final StatementActHelper acts;

    /**
     * Customer account rules.
     */
    private final CustomerAccountRules account;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules laboratoryRules;

    /**
     * Statement rules.
     */
    private final StatementRules statement;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(EndOfPeriodProcessor.class);


    /**
     * Constructs a {@link EndOfPeriodProcessor}.
     *
     * @param statementDate        the statement date. Must be a date prior today.
     * @param postCompletedCharges if {@code true}, post completed charges
     * @param practice             the practice
     * @param service              the archetype service
     * @param rules                the customer account rules
     * @param laboratoryRules      the laboratory rules
     * @throws StatementProcessorException if the statement date is invalid
     */
    public EndOfPeriodProcessor(Date statementDate, boolean postCompletedCharges, Party practice,
                                IArchetypeRuleService service, CustomerAccountRules rules,
                                LaboratoryRules laboratoryRules) {
        this.service = service;
        statementDate = DateRules.getDate(statementDate); // ignore time component
        if (statementDate.compareTo(DateRules.getYesterday()) > 0) {
            throw new StatementProcessorException(InvalidStatementDate, statementDate);
        }
        acts = new StatementActHelper(service);
        account = rules;
        statement = new StatementRules(practice, service, rules);
        timestamp = acts.getStatementTimestamp(statementDate);
        this.postCompletedCharges = postCompletedCharges;
        this.laboratoryRules = laboratoryRules;
    }

    /**
     * Process a customer.
     *
     * @param customer the customer to process
     * @throws OpenVPMSException for any error
     */
    public void process(Party customer) {
        StatementPeriod period = new StatementPeriod(customer, timestamp, acts);
        if (!period.hasStatement()) {
            boolean needStatement = false;
            Date open = period.getOpeningBalanceTimestamp();
            Date close = period.getClosingBalanceTimestamp();
            if (postCompletedCharges) {
                needStatement = postCompletedCharges(customer, period);
            }
            BigDecimal balance = null;
            if (!needStatement) {
                balance = account.getBalance(customer, open, close, period.getOpeningBalance());
                if (balance.compareTo(BigDecimal.ZERO) == 0) {
                    if (acts.hasAccountActivity(customer, open, close)) {
                        needStatement = true;
                    }
                } else {
                    needStatement = true;
                }
            }
            if (needStatement) {
                if (balance == null) {
                    balance = account.getBalance(customer, open, close, period.getOpeningBalance());
                }
                createPeriodEnd(customer, period, balance);
            }
        }
    }

    /**
     * Generates an <em>act.customerAccountClosingBalance</em> and <em>act.customerAccountOpeningBalance</em> for the
     * specified customer.
     *
     * @param customer the customer
     * @param period   the statement period
     * @param balance  the closing balance
     * @throws ArchetypeServiceException for any error
     */
    public void createPeriodEnd(Party customer, StatementPeriod period, BigDecimal balance) {
        BigDecimal overdue = BigDecimal.ZERO;
        FinancialAct fee = null;
        if (balance.compareTo(BigDecimal.ZERO) != 0) {
            Date overdueDate = account.getOverdueDate(customer, timestamp);
            overdue = account.getOverdueBalance(customer, timestamp, overdueDate);
            if (overdue.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal accountFee = statement.getAccountFee(customer, timestamp);
                if (accountFee.compareTo(BigDecimal.ZERO) != 0) {
                    Date feeStartTime = period.getFeeTimestamp();
                    fee = statement.createAccountingFeeAdjustment(customer, accountFee, feeStartTime);
                    balance = balance.add(accountFee);
                }
            }
        }

        // ensure the acts are ordered correctly, ie. close before open
        Date closeTime = period.getClosingBalanceTimestamp();
        Date openTime = new Date(closeTime.getTime() + 1000);

        FinancialAct close = account.createClosingBalance(customer, closeTime, balance);
        FinancialAct open = account.createOpeningBalance(customer, openTime, balance);

        IMObjectBean bean = service.getBean(close);
        bean.setValue("overdueBalance", overdue);

        if (fee != null) {
            service.save(Arrays.asList((IMObject) fee, close, open));
        } else {
            service.save(Arrays.asList((IMObject) close, open));
        }
    }

    /**
     * Posts completed charges.
     *
     * @param customer the customer
     * @param period   the statement period
     * @return {@code true} if any charges were POSTED, indicating a statement is required, otherwise {@code false}
     */
    private boolean postCompletedCharges(Party customer, StatementPeriod period) {
        boolean needStatement = false;

        for (Act act : acts.getCompletedCharges(customer, timestamp, period.getOpeningBalanceTimestamp(),
                                                period.getClosingBalanceTimestamp())) {
            if (canPost(act)) {
                post(act, period);
                needStatement = true;
            }
        }
        return needStatement;
    }

    /**
     * Determines if a COMPLETED charge can be POSTED.
     * <p/>
     * If the charge is an invoice that has unsubmitted investigations or outstanding pharmacy orders, posting is not
     * permitted.
     *
     * @param act the charge
     * @return {@code true} if the charge can be POSTED, otherwise {@code false}
     */
    private boolean canPost(Act act) {
        boolean result = true;
        if (act.isA(CustomerAccountArchetypes.INVOICE)) {
            IMObjectBean bean = service.getBean(act);
            Map<Reference, Act> investigations = new HashMap<>();
            for (FinancialAct item : bean.getTargets("items", FinancialAct.class)) {
                if (InvoiceItemStatus.ORDERED.equals(item.getStatus())) {
                    log.warn("Cannot POST COMPLETED invoice={}: it has an outstanding order", act.getId());
                    result = false;
                    break;
                } else if (hasUnsubmittedInvestigations(item, investigations)) {
                    log.warn("Cannot POST COMPLETED invoice={}: it has an unsubmitted investigation", act.getId());
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Determines if an invoice item has unsubmitted investigations.
     *
     * @param item           the invoice item
     * @param investigations a cache of investigations, keyed on reference
     * @return {@code true} if the item has at least one unsubmitted investigation
     */
    private boolean hasUnsubmittedInvestigations(FinancialAct item, Map<Reference, Act> investigations) {
        IMObjectBean itemBean = service.getBean(item);
        for (Reference ref : itemBean.getTargetRefs("investigations")) {
            Act investigation = investigations.computeIfAbsent(ref, reference -> service.get(reference, Act.class));
            if (investigation != null && laboratoryRules.isUnsubmittedInvestigation(investigation)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Posts a completed charge act. This sets the status to {@code POSTED},
     * and the startTime to 1 second less than the statement timestamp.
     *
     * @param act    the act to post
     * @param period the statement period
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void post(Act act, StatementPeriod period) {
        act.setActivityStartTime(period.getCompletedChargeTimestamp());
        act.setStatus(ActStatus.POSTED);
        service.save(act);
    }

}
