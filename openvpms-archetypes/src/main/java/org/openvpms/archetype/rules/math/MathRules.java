/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.math;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import static java.math.BigDecimal.ROUND_HALF_UP;
import static java.math.BigDecimal.ZERO;

/**
 * Math rules.
 *
 * @author Tim Anderson
 */
public class MathRules {

    /**
     * One hundred.
     */
    public static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);

    /**
     * One thousand.
     */
    public static final BigDecimal ONE_THOUSAND = BigDecimal.valueOf(1000);

    /**
     * One pound, in kilograms.
     */
    public static final BigDecimal ONE_POUND_IN_KILOS = new BigDecimal("0.45359237");

    /**
     * One pound in grams.
     */
    public static final BigDecimal ONE_POUND_IN_GRAMS = ONE_POUND_IN_KILOS.multiply(ONE_THOUSAND);

    /**
     * One kilo in pounds.
     */
    public static final BigDecimal ONE_KILO_IN_POUNDS = BigDecimal.ONE.divide(ONE_POUND_IN_KILOS, 8, ROUND_HALF_UP);

    /**
     * One gram in pounds.
     */
    public static final BigDecimal ONE_GRAM_IN_POUNDS = BigDecimal.ONE.divide(ONE_POUND_IN_GRAMS, 8, ROUND_HALF_UP);


    /**
     * Default constructor.
     */
    private MathRules() {

    }

    /**
     * Calculates a charge/estimate total.
     * <br/>
     * If the quantity is:
     * <ul>
     *     <li>+ve: {@code fixedPrice + unitPrice * quantity - discount}</li>
     *     <li>-ve: {@code -(fixedPrice + unitPrice * abs(quantity) - discount)}</li>
     *     <li>zero: {@code 0}</li>
     * </ul>
     *
     * @param fixedPrice the fixed price
     * @param unitPrice  the unit price
     * @param quantity   the quantity
     * @param discount   the discount
     * @param scale      the no. of decimal places to round to
     * @return the total
     */
    public static BigDecimal calculateTotal(BigDecimal fixedPrice, BigDecimal unitPrice, BigDecimal quantity,
                                            BigDecimal discount, int scale) {
        BigDecimal total;
        if (!isZero(quantity)) {
            BigDecimal unitTotal = round(quantity.abs().multiply(unitPrice), scale);
            total = fixedPrice.add(unitTotal).subtract(discount);
            if (quantity.signum() < 0) {
                total = total.negate();
            }
            total = round(total, scale);
        } else {
            total = ZERO;
        }
        return total;
    }

    /**
     * Rounds a value to the default no. of decimal places.
     * todo - could be configurable via a properies file
     *
     * @param value the value
     * @return the rounded value
     */
    public static BigDecimal round(BigDecimal value) {
        return round(value, 2);
    }

    /**
     * Rounds a value.
     *
     * @param value the value
     * @param scale the no. of decimal places
     * @return the rounded value
     */
    public static BigDecimal round(BigDecimal value, int scale) {
        return value.setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * Performs a division, rounding the result to the specified no. of places.
     *
     * @param dividend the value to divide
     * @param divisor  the divisor
     * @param scale    the no. of decimal places
     * @return the divided value
     */
    public static BigDecimal divide(BigDecimal dividend, BigDecimal divisor, int scale) {
        return dividend.divide(divisor, scale, RoundingMode.HALF_UP);
    }

    /**
     * Performs a division, rounding the result to the specified no. of places.
     *
     * @param dividend the value to divide
     * @param divisor  the divisor
     * @param scale    the no. of decimal places
     * @return the divided value
     */
    public static BigDecimal divide(BigDecimal dividend, int divisor, int scale) {
        return divide(dividend, BigDecimal.valueOf(divisor), scale);
    }

    /**
     * Helper to determine if two decimals are equal.
     *
     * @param lhs the left-hand side. May be {@code null}
     * @param rhs right left-hand side. May be {@code null}
     * @return {@code true</t> if they are equal, otherwise {@code false}
     */
    public static boolean equals(BigDecimal lhs, BigDecimal rhs) {
        if (lhs != null && rhs != null) {
            return lhs.compareTo(rhs) == 0;
        }
        return Objects.equals(lhs, rhs);
    }

    /**
     * Helper to determine if a decimal is zero.
     *
     * @param value the value to check
     * @return {@code true} if the decimal is zero
     */
    public static boolean isZero(BigDecimal value) {
        return value.signum() == 0;
    }

    /**
     * Determines if a decimal is positive.
     *
     * @param value the value to check
     * @return {@code true} if the decimal is greater than zero
     */
    public static boolean isPositive(BigDecimal value) {
        return value.signum() > 0;
    }

    /**
     * Determines if two numeric ranges intersect.
     *
     * @param from1 the start of the first range. May be {@code null}
     * @param to1   the end of the first range. May be {@code null}
     * @param from2 the start of the date range. May be {@code null}
     * @param to2   the end of the date range. May be {@code null}
     * @return {@code true} if the ranges intersect
     */
    public static boolean intersects(BigDecimal from1, BigDecimal to1, BigDecimal from2, BigDecimal to2) {
        if (from1 == null && to1 == null) {
            return true;
        } else if (from1 == null) {
            return from2 == null || to1.compareTo(from2) > 0;
        } else if (to1 == null) {
            return to2 == null || from1.compareTo(to2) < 0;
        } else if (from2 == null && to2 == null) {
            return true;
        } else if (from2 == null) {
            return from1.compareTo(to2) < 0;
        } else if (to2 == null) {
            return to1.compareTo(from2) > 0;
        }
        return from2.compareTo(to1) < 0 && to2.compareTo(from1) > 0;
    }
}
