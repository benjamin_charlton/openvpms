/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;


/**
 * Act status types for <em>act.customerTask</em> acts.
 *
 * @author Tim Anderson
 */
public class TaskStatus {

    /**
     * Pending status.
     */
    public static final String PENDING = WorkflowStatus.PENDING;

    /**
     * Cancelled status.
     */
    public static final String CANCELLED = WorkflowStatus.CANCELLED;

    /**
     * In progress status.
     */
    public static final String IN_PROGRESS = WorkflowStatus.IN_PROGRESS;

    /**
     * Billed status.
     */
    public static final String BILLED = WorkflowStatus.BILLED;

    /**
     * Completed status.
     */
    public static final String COMPLETED = WorkflowStatus.COMPLETED;

    /**
     * Helper to determine if a status is {@link #COMPLETED} or {@link #CANCELLED}.
     *
     * @param status the status
     * @return {@code true} if the status is in the 'COMPLETE' range, otherwise {@code false}
     */
    public static boolean isComplete(String status) {
        return COMPLETED.equals(status) || CANCELLED.equals(status);
    }

    /**
     * Helper to determine if a status is in the 'INCOMPLETE' range.
     *
     * @param status the status
     * @return {@code true} if the status is not complete, otherwise {@code false}
     */
    public static boolean isIncomplete(String status) {
        return !isComplete(status);
    }

}
