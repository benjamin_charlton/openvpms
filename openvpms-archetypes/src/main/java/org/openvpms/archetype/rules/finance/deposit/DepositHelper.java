/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.deposit;

import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.CollectionNodeConstraint;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.NodeConstraint;
import org.openvpms.component.system.common.query.ObjectRefNodeConstraint;
import org.openvpms.component.system.common.query.RelationalOp;

import java.util.List;


/**
 * Bank deposit helper.
 *
 * @author Tim Anderson
 */
public class DepositHelper {

    /**
     * Default constructor.
     */
    private DepositHelper() {

    }

    /**
     * Returns the undeposited bank deposit for an account, if it exists.
     *
     * @param account the account
     * @param service the archetype service
     * @return an <em>act.bankDeposit</em> or {@code null} if none exists
     */
    public static FinancialAct getUndepositedDeposit(Entity account, IArchetypeService service) {
        ArchetypeQuery query = new ArchetypeQuery(DepositArchetypes.BANK_DEPOSIT, false, true);
        query.setFirstResult(0);
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        query.add(new NodeConstraint("status", RelationalOp.EQ, DepositStatus.UNDEPOSITED));
        CollectionNodeConstraint participations
                = new CollectionNodeConstraint(
                "depositAccount", DepositArchetypes.DEPOSIT_PARTICIPATION,
                false, true);
        participations.add(new ObjectRefNodeConstraint(
                "entity", account.getObjectReference()));
        query.add(participations);
        List<IMObject> matches = service.get(query).getResults();
        return (!matches.isEmpty()) ? (FinancialAct) matches.get(0) : null;
    }

    /**
     * Creates a new bank deposit.
     *
     * @param account the account to deposit to
     * @param service the archetype service
     * @return a new bank deposit
     */
    public static Act createBankDeposit(Entity account, IArchetypeService service) {
        Act act = service.create(DepositArchetypes.BANK_DEPOSIT, Act.class);
        IMObjectBean bean = service.getBean(act);
        bean.setTarget("depositAccount", account);
        return act;
    }
}
