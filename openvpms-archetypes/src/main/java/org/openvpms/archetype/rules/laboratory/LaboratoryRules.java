/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.laboratory;

import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Laboratory rules.
 *
 * @author Tim Anderson
 */
public class LaboratoryRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Laboratory node name.
     */
    private static final String LABORATORY = "laboratory";

    /**
     * Location node name.
     */
    private static final String LOCATION = "location";

    /**
     * Creates a {@link LaboratoryRules}.
     *
     * @param service the archetype service
     */
    public LaboratoryRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the laboratory associated with an investigation type.
     *
     * @param investigationType the investigation type
     * @return the laboratory, or {@code null} if there is none
     */
    public Entity getLaboratory(Entity investigationType) {
        return getLaboratory(service.getBean(investigationType));
    }

    /**
     * Returns the laboratory for the specified investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the laboratory, or {@code null} if the investigation type is not associated with a laboratory at the
     * specified location
     */
    public Entity getLaboratory(Entity investigationType, Party location) {
        IMObjectBean bean = service.getBean(investigationType);
        return getLaboratory(bean, location);
    }

    /**
     * Returns the laboratory for the specified investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the laboratory, or {@code null} if the investigation type is not associated with a laboratory at the
     * specified location
     */
    public Entity getLaboratory(IMObjectBean investigationType, Party location) {
        Entity laboratory = getLaboratory(investigationType);
        Entity result = null;
        if (laboratory != null) {
            if (laboratory.isA(LaboratoryArchetypes.HL7_LABORATORY_GROUP)) {
                result = getGroupLaboratory(laboratory, location);
            } else if (laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES, LaboratoryArchetypes.HL7_LABORATORY)
                       && canUseAtLocation(laboratory, location.getObjectReference())) {
                result = laboratory;
            }
        }
        return result;
    }

    /**
     * Creates an order for an investigation.
     * <p/>
     * The order is unsaved.
     *
     * @param investigation the investigation
     * @return the corresponding order
     */
    public Act createOrder(Act investigation) {
        if (investigation.isNew()) {
            // need to propagate the investigation identifier to the order
            throw new IllegalStateException("Cannot create orders for unsaved investigations");
        }
        IMObjectBean bean = service.getBean(investigation);
        Act order = service.create(LaboratoryArchetypes.ORDER, Act.class);
        order.setActivityStartTime(investigation.getActivityStartTime());
        // NOTE: the investigation id is stored on the order to allow order retrieval after an investigation has been
        // deleted. This may be necessary for laboratories to cancel an order associated with a deleted investigation
        ActIdentity identity = service.create(InvestigationArchetypes.INVESTIGATION_ID, ActIdentity.class);
        identity.setIdentity(Long.toString(investigation.getId()));
        order.addIdentity(identity);
        order.setDescription(investigation.getDescription());
        IMObjectBean orderBean = service.getBean(order);
        orderBean.setValue("type", "NEW");
        orderBean.setTarget("patient", bean.getTargetRef("patient"));
        orderBean.setTarget(LABORATORY, bean.getTargetRef(LABORATORY));
        orderBean.setTarget("investigationType", bean.getTargetRef("investigationType"));
        orderBean.setTarget(LOCATION, bean.getTargetRef(LOCATION));
        orderBean.setTarget("clinician", bean.getTargetRef("clinician"));
        orderBean.setTarget("device", bean.getTargetRef("device"));
        for (Reference test : bean.getTargetRefs("tests")) {
            orderBean.addTarget("tests", test);
        }
        bean.addTarget("order", order, "investigation");
        return order;
    }

    /**
     * Determines if an investigation is associated with a laboratory and needs to be submitted or confirmed.
     * <p/>
     * This only applies to investigations associated with <em>entity.laboratoryService*</em> services, not
     * HL7 services.
     *
     * @param investigation the investigation
     * @return {@code true} if the investigation needs to be submitted or confirmed
     */
    public boolean isUnsubmittedInvestigation(Act investigation) {
        boolean result = false;
        if (!InvestigationActStatus.CANCELLED.equals(investigation.getStatus())) {
            String orderStatus = investigation.getStatus2();
            if (InvestigationActStatus.PENDING.equals(orderStatus)
                || InvestigationActStatus.CONFIRM.equals(orderStatus)
                || InvestigationActStatus.CONFIRM_DEFERRED.equals(orderStatus)) {
                IMObjectBean bean = service.getBean(investigation);
                Reference laboratory = bean.getTargetRef("laboratory");
                result = (laboratory != null && laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES));
            }
        }
        return result;
    }

    /**
     * Determines if a laboratory can be used at a location.
     *
     * @param laboratory the laboratory
     * @param location   the location
     * @return {@code true} if the laboratory can be used at the location
     */
    public boolean canUseLaboratoryAtLocation(Entity laboratory, Party location) {
        boolean result;
        if (laboratory.isA(LaboratoryArchetypes.HL7_LABORATORY_GROUP)) {
            result = getGroupLaboratory(laboratory, location) != null;
        } else {
            result = canUseAtLocation(laboratory, location.getObjectReference());
        }
        return result;
    }

    /**
     * Determines if a laboratory device can be used at a location.
     *
     * @param device   the device
     * @param location the location
     * @return {@code true} if the laboratory can be used at the location
     */
    public boolean canUseDeviceAtLocation(Entity device, Party location) {
        return canUseAtLocation(device, location.getObjectReference());
    }

    /**
     * Returns a laboratory test.
     *
     * @param archetype the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param code      the test code
     * @return the test, or {@code null} if none is found. The returned test may be inactive
     */
    public Entity getTest(String archetype, String code) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, LaboratoryArchetypes.TEST);
        Join<Entity, Identity> identity = root.join("code", archetype);
        identity.on(builder.equal(identity.get("identity"), code));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns the active products associated with a test.
     *
     * @param test the test
     * @return the active products that are associated with the test
     */
    public List<Product> getProducts(Entity test) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Product> query = builder.createQuery(Product.class);
        Root<Product> root = query.from(Product.class, ProductArchetypes.MEDICATION, ProductArchetypes.MERCHANDISE,
                                        ProductArchetypes.SERVICE);
        Join<Product, IMObject> tests = root.join("tests");
        tests.on(builder.equal(tests.get("target"), test.getObjectReference()));
        query.where(builder.equal(root.get("active"), true));
        return service.createQuery(query).getResultList();
    }

    /**
     * Returns the laboratory associated with an investigation type.
     *
     * @param investigationType the investigation type bean
     * @return the laboratory, or {@code null} if there is none
     */
    private Entity getLaboratory(IMObjectBean investigationType) {
        return investigationType.getTarget(LABORATORY, Entity.class);
    }

    /**
     * Returns the laboratory for the specified location from a laboratory group.
     *
     * @param group    the laboratory group
     * @param location the location that the laboratory services
     * @return the laboratory, or {@code null} if none is found
     */
    private Entity getGroupLaboratory(Entity group, Party location) {
        IMObjectBean bean = service.getBean(group);
        return getGroupLaboratory(bean, location);
    }

    /**
     * Returns the laboratory for the specified location from a laboratory group.
     *
     * @param group    the laboratory group
     * @param location the location that the laboratory services
     * @return the laboratory, or {@code null} if none is found
     */
    private Entity getGroupLaboratory(IMObjectBean group, Party location) {
        Entity result = null;
        Reference reference = location.getObjectReference();
        List<Entity> laboratories = group.getTargets("services", Entity.class);
        if (!laboratories.isEmpty()) {
            // force the same lab to be returned if multiple are supported via misconfiguration
            laboratories.sort(Comparator.comparingLong(IMObject::getId));
        }
        for (Entity laboratory : laboratories) {
            if (canUseAtLocation(laboratory, reference)) {
                result = laboratory;
                break;
            }
        }
        return result;
    }

    /**
     * Determines if a laboratory can be used at a location.
     *
     * @param laboratory the laboratory
     * @param location   the practice location reference
     * @return {@code true} if the laboratory has no locations (i.e can be used at all locations), or is linked to the
     * location
     */
    private boolean canUseAtLocation(Entity laboratory, Reference location) {
        boolean result = false;
        IMObjectBean bean = service.getBean(laboratory);
        if (laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES, LaboratoryArchetypes.DEVICES)) {
            List<Reference> locations = bean.getTargetRefs("locations");
            result = (locations.isEmpty() || locations.contains(location));
        } else if (laboratory.isA(LaboratoryArchetypes.HL7_LABORATORY)) {
            result = Objects.equals(location, bean.getTargetRef(LOCATION));
        }
        return result;
    }
}
