/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.laboratory;

/**
 * Laboratory archetypes.
 *
 * @author Tim Anderson
 */
public class LaboratoryArchetypes {

    /**
     * Investigation type short name.
     */
    public static final String INVESTIGATION_TYPE = "entity.investigationType";

    /**
     * Investigation type identity archetypes.
     */
    public static final String INVESTIGATION_TYPE_IDS = "entityIdentity.investigationType*";

    /**
     * Laboratory order identifiers.
     */
    public static final String LAB_ORDER_IDS = "actIdentity.laboratoryOrder*";

    /**
     * The laboratory services archetype.
     */
    public static final String LABORATORY_SERVICES = "entity.laboratoryService*";

    /**
     * Laboratory order archetype.
     */
    public static final String ORDER = "act.laboratoryOrder";

    /**
     * Default laboratory device archetype.
     */
    public static final String DEVICE = "entity.laboratoryDevice";

    /**
     * Laboratory device archetypes.
     */
    public static final String DEVICES = "entity.laboratoryDevice*";

    /**
     * Laboratory device identity archetypes.
     */
    public static final String DEVICE_IDS = "entityIdentity.laboratoryDevice*";

    /**
     * Laboratory test archetype.
     */
    public static final String TEST = "entity.laboratoryTest";

    /**
     * Default laboratory test identity archetype.
     */
    public static final String TEST_CODE = "entityIdentity.laboratoryTest";

    /**
     * Laboratory test code archetypes.
     */
    public static final String TEST_CODES = "entityIdentity.laboratoryTest*";

    /**
     * HL7 Laboratory archetype short name.
     */
    public static final String HL7_LABORATORY = "entity.HL7ServiceLaboratory";

    /**
     * HL7 Laboratory group archetype short name.
     */
    public static final String HL7_LABORATORY_GROUP = "entity.HL7ServiceLaboratoryGroup";

    /**
     * HL7 Laboratory test archetype.
     */
    public static final String HL7_TEST = "entity.laboratoryTestHL7";

    /**
     * Investigation type to test relationship archetype.
     */
    public static final String INVESTIGATION_TYPE_TEST = "entityLink.investigationTypeTest";

    /**
     * Default constructor.
     */
    private LaboratoryArchetypes() {

    }
}
