/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import javax.print.attribute.Size2DSyntax;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.InvalidOrientation;
import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.InvalidPaperSize;
import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.InvalidUnits;
import static org.openvpms.component.model.bean.Policies.active;


/**
 * Wrapper for <em>entity.documentTemplate</em>.
 *
 * @author Tim Anderson
 */
public class DocumentTemplate {

    public enum PrintMode {
        IMMEDIATE,
        MANUAL,
        CHECK_OUT
    }

    public enum Format {
        PDF("application/pdf"),
        ODT("application/vnd.oasis.opendocument.text");

        private final String mimeType;

        Format(String mimeType) {
            this.mimeType = mimeType;
        }

        /**
         * Returns the mime type.
         *
         * @return the mime type
         */
        public String getMimeType() {
            return mimeType;
        }
    }

    /**
     * A4 paper size.
     */
    public static final String A4 = "A4";

    /**
     * A5 paper size.
     */
    public static final String A5 = "A5";

    /**
     * Letter paper size.
     */
    public static final String LETTER = "LETTER";

    /**
     * Custom paper size.
     */
    public static final String CUSTOM = "CUSTOM";

    /**
     * Portrait print orientation.
     */
    public static final String PORTRAIT = "PORTRAIT";

    /**
     * Landscape print orientation.
     */
    public static final String LANDSCAPE = "LANDSCAPE";

    /**
     * Millimetres unit for paper size.
     */
    public static final String MM = "MM";

    /**
     * Inchces unit for paper size.
     */
    public static final String INCH = "INCH";

    /**
     * The bean to access the template's properties.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Cached template.
     */
    private DocumentAct act;

    /**
     * Cached email template.
     */
    private Entity email;

    /**
     * Cached SMS template.
     */
    private Entity sms;

    /**
     * Printers node name.
     */
    private static final String PRINTERS = "printers";

    /**
     * Constructs a {@link DocumentTemplate}.
     *
     * @param template the template
     * @param service  the archetype service
     */
    public DocumentTemplate(Entity template, ArchetypeService service) {
        bean = service.getBean(template);
        this.service = service;
    }

    /**
     * Returns the document template name.
     *
     * @return the document template name. May be {@code null}
     */
    public String getName() {
        return bean.getString("name");
    }

    /**
     * Returns the document template description.
     *
     * @return the document template description. May be {@code null}
     */
    public String getDescription() {
        return bean.getString("description");
    }

    /**
     * Determines if the template is active.
     *
     * @return {@code true} if the template is active; otherwise it is inactive
     */
    public boolean isActive() {
        return bean.getBoolean("active");
    }

    /**
     * Returns the document template type.
     *
     * @return the type. May be {@code null}
     */
    public String getType() {
        Lookup type = bean.getObject("type", Lookup.class);
        return type != null ? type.getCode() : null;
    }

    /**
     * Returns the user level that the template applies to.
     * <p/>
     * TODO - need a better facility for user authorisation
     *
     * @return the user level that the template applies to. May be {@code null}
     */
    public String getUserLevel() {
        return bean.getString("userLevel");
    }

    /**
     * Returns the report type.
     *
     * @return the report type. May be {@code null}
     */
    public String getReportType() {
        return bean.getString("reportType");
    }

    /**
     * Returns the print mode.
     *
     * @return the print mode. May be {@code null}
     */
    public PrintMode getPrintMode() {
        String mode = bean.getString("printMode");
        return (mode != null) ? PrintMode.valueOf(mode) : null;
    }

    /**
     * Returns the paper size.
     * <p/>
     * Current legal values are:
     * <ul>
     * <li>{@link #A4}
     * <li>{@link #A5}
     * <li>{@link #LETTER}
     * <li>{@link #CUSTOM}
     * </ul>
     *
     * @return the paper size. May be {@code null}
     */
    public String getPaperSize() {
        return bean.getString("paperSize");
    }

    /**
     * Sets the paper size.
     *
     * @param size the paper size
     */
    public void setPaperSize(String size) {
        bean.setValue("paperSize", size);
    }

    /**
     * Returns the print orientation.
     * <p/>
     * Current legal values are:
     * <ul>
     * <li>{@link #PORTRAIT}
     * <li>{@link #LANDSCAPE}
     * </ul>
     *
     * @return the print orientation. May be {@code null}
     */
    public String getOrientation() {
        return bean.getString("orientation");
    }

    /**
     * Returns the default number of copies to print.
     *
     * @return the default number of copies
     */
    public int getCopies() {
        return bean.getInt("copies");
    }

    /**
     * Returns the output format.
     *
     * @return the output format, or {@code null} to use the default.
     */
    public Format getOutputFormat() {
        String format = bean.getString("outputFormat");
        return format != null ? Format.valueOf(format) : null;
    }

    /**
     * Returns the paper height.
     * <p/>
     * Note that this is only applicable if {@link #getPaperSize()} is {@link #CUSTOM}.
     *
     * @return the paper height
     */
    public BigDecimal getPaperHeight() {
        return bean.getBigDecimal("paperHeight");
    }

    /**
     * Returns the paper width.
     * <p/>
     * Note that this is only applicable if {@link #getPaperSize()} is {@link #CUSTOM}.
     *
     * @return the paper width
     */
    public BigDecimal getPaperWidth() {
        return bean.getBigDecimal("paperWidth");
    }

    /**
     * Returns the paper units.
     * <p/>
     * Legal values are:
     * <ul>
     * <li>{@link #MM}
     * <li>{@link #INCH}
     * </ul>
     *
     * @return the paper units. May be {@code null}
     */
    public String getPaperUnits() {
        return bean.getString("paperUnits");
    }

    /**
     * Returns the email template.
     *
     * @return the email template. May be {@code null}
     */
    public Entity getEmailTemplate() {
        if (email == null) {
            email = bean.getTarget("email", Entity.class, active());
        }
        return email;
    }

    /**
     * Returns the SMS template.
     *
     * @return the SMS template. May be {@code null}
     */
    public Entity getSMSTemplate() {
        if (sms == null) {
            sms = bean.getTarget("sms", Entity.class, active());
        }
        return sms;
    }

    /**
     * Returns the media size.
     *
     * @return the media size for the template, or {@code null} if none is defined
     */
    public MediaSizeName getMediaSize() {
        String size = getPaperSize();
        if (size != null) {
            BigDecimal width = getPaperWidth();
            BigDecimal height = getPaperHeight();
            String units = getPaperUnits();
            MediaSizeName media;
            if (PaperSize.CUSTOM.name().equals(size)) {
                media = getMedia(width, height, units);
            } else {
                media = PaperSize.getMediaSizeName(size);
            }
            return media;
        }
        return null;
    }

    /**
     * Returns the print orientation.
     *
     * @return the print orientation. May be {@code null}
     */
    public OrientationRequested getOrientationRequested() {
        return getOrientation() != null ? Orientation.getOrientation(getOrientation()) : null;
    }

    /**
     * Returns the printers.
     *
     * @return the printers
     */
    public List<DocumentTemplatePrinter> getPrinters() {
        List<DocumentTemplatePrinter> result = new ArrayList<>();
        List<EntityRelationship> printers = bean.getValues(PRINTERS, EntityRelationship.class);
        for (EntityRelationship printer : printers) {
            result.add(new DocumentTemplatePrinter(printer, service));
        }
        return result;
    }

    /**
     * Returns the printer for a given practice organisation.
     *
     * @param location an <em>party.organisationPractice</em> or <em>party.organisationLocation</em>
     * @return the corresponding printer, or {@code null} if none is defined
     */
    public DocumentTemplatePrinter getPrinter(Entity location) {
        Predicate<EntityRelationship> predicate = Predicates.<EntityRelationship>activeNow()
                .and(Predicates.targetEquals(location));
        EntityRelationship printer = bean.getValue(PRINTERS, EntityRelationship.class, predicate);
        return (printer != null) ? new DocumentTemplatePrinter(printer, service) : null;
    }

    /**
     * Adds a new printer relationship.
     *
     * @param location an <em>party.organisationPractice</em> or <em>party.organisationLocation</em>
     * @return the new printer relationship
     */
    public DocumentTemplatePrinter addPrinter(Entity location) {
        EntityRelationship relationship = (EntityRelationship) bean.addTarget(PRINTERS, location);
        // TODO - printer relationships should be EntityLinks
        location.addEntityRelationship(relationship);
        return new DocumentTemplatePrinter(relationship, service);
    }

    /**
     * Returns the document associated with the template.
     *
     * @return the corresponding document, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Document getDocument() {
        DocumentAct act = getDocumentAct();
        return act != null && act.getDocument() != null ? service.get(act.getDocument(), Document.class) : null;
    }

    /**
     * Returns the name of the document associated with the template.
     *
     * @return the document name, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public String getDocumentName() {
        DocumentAct act = getDocumentAct();
        return (act != null) ? act.getName() : null;
    }

    /**
     * Returns the mime type of the template.
     *
     * @return the mime type, or {@code null} if no document is associated with the template
     */
    public String getMimeType() {
        DocumentAct act = getDocumentAct();
        return act != null ? act.getMimeType() : null;
    }

    /**
     * Returns the file name format expression.
     *
     * @return the file name format expression. May be {@code null}
     */
    public String getFileNameExpression() {
        List<IMObject> fileNameFormat = bean.getValues("fileNameFormat");
        if (!fileNameFormat.isEmpty()) {
            IMObjectBean format = service.getBean(fileNameFormat.get(0));
            return format.getString("expression");
        }
        return null;
    }

    /**
     * Returns the document act associated with the template
     *
     * @return the document act, or {@code null} if none exists
     */
    public DocumentAct getDocumentAct() {
        if (act == null) {
            act = new TemplateHelper(service).getDocumentAct(getEntity());
        }
        return act;
    }

    /**
     * Returns the <em>entity.documentTemplate</em> entity.
     *
     * @return the entity
     */
    public Entity getEntity() {
        return bean.getObject(Entity.class);
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return getEntity().hashCode();
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof DocumentTemplate) && ((DocumentTemplate) obj).getEntity().equals(getEntity());
    }

    /**
     * Helper to convert a custom paper size to a {@link MediaSizeName}.
     *
     * @param width  the page width
     * @param height the page height
     * @param units  the units. One of 'MM' or 'INCH'.
     * @return the corresponding media size name.
     * @throws DocumentException if the paper size is invalid
     */
    private MediaSizeName getMedia(BigDecimal width, BigDecimal height, String units) {
        int unitCode = Units.getUnits(units);
        try {
            return MediaSize.findMedia(width.floatValue(), height.floatValue(), unitCode);
        } catch (IllegalArgumentException exception) {
            String size = width + "x" + height + " " + units;
            throw new DocumentException(InvalidPaperSize, size);
        }
    }

    /**
     * Provides a mapping between supported orientations and values defined in
     * {@link OrientationRequested}.
     */
    private enum Orientation {

        PORTRAIT(OrientationRequested.PORTRAIT),
        LANDSCAPE(OrientationRequested.LANDSCAPE);

        private final OrientationRequested orientationRequested;

        Orientation(OrientationRequested orientationRequested) {
            this.orientationRequested = orientationRequested;
        }

        public OrientationRequested getOrientation() {
            return orientationRequested;
        }

        public static OrientationRequested getOrientation(String orientation) {
            for (Orientation o : Orientation.values()) {
                if (o.name().equals(orientation)) {
                    return o.getOrientation();
                }
            }
            throw new DocumentException(InvalidOrientation, orientation);
        }
    }

    /**
     * Provides a mapping between supported paper sizes and
     * {@link MediaSizeName}.
     */
    private enum PaperSize {

        A4(MediaSizeName.ISO_A4),
        A5(MediaSizeName.ISO_A5),
        LETTER(MediaSizeName.NA_LETTER),
        CUSTOM(null);

        private final MediaSizeName mediaName;

        PaperSize(MediaSizeName name) {
            mediaName = name;
        }

        public MediaSizeName getMediaSizeName() {
            return mediaName;
        }

        public static MediaSizeName getMediaSizeName(String name) {
            for (PaperSize size : values()) {
                if (size.name().equals(name)) {
                    return size.getMediaSizeName();
                }
            }
            throw new DocumentException(InvalidPaperSize, name);
        }
    }

    /**
     * Provides a mapping between paper size units and corresponding
     * values defined in {@link javax.print.attribute.Size2DSyntax}.
     */
    private enum Units {

        MM(Size2DSyntax.MM),
        INCH(Size2DSyntax.INCH);

        private final int value;

        Units(int value) {
            this.value = value;
        }

        public int getUnits() {
            return value;
        }

        public static int getUnits(String units) {
            for (Units u : Units.values()) {
                if (u.name().equals(units)) {
                    return u.getUnits();
                }
            }
            throw new DocumentException(InvalidUnits, units);
        }
    }

}
