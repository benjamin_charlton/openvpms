/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.account;

import org.openvpms.archetype.rules.act.ActCalculator;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.NamedQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectRefSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.ACCOUNT_ACTS;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CLOSING_BALANCE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.DEBITS_CREDITS;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.OPENING_BALANCE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountRuleException.ErrorCode.InvalidBalance;


/**
 * Customer balance calculator.
 *
 * @author Tim Anderson
 */
public class BalanceCalculator {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * The act amount.
     */
    private static final String ACT_AMOUNT = "act.amount";

    /**
     * The act credit flag.
     */
    private static final String ACT_CREDIT = "act.credit";

    /**
     * The act allocated amount.
     */
    private static final String ACT_ALLOCATED_AMOUNT = "act.allocatedAmount";


    /**
     * Constructs a {@link BalanceCalculator}.
     *
     * @param service the archetype service
     */
    public BalanceCalculator(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Calculates the outstanding balance for a customer.
     *
     * @param customer the customer
     * @return the outstanding balance
     * @throws ArchetypeServiceException for any archetype service error
     */
    public BigDecimal getBalance(org.openvpms.component.model.party.Party customer) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createUnallocatedObjectSetQuery(customer, DEBITS_CREDITS);
        Iterator<ObjectSet> iterator = new ObjectSetQueryIterator(service, query);
        return calculateBalance(iterator);
    }

    /**
     * Calculates the outstanding balance for a customer, incorporating acts
     * up to the specified date.
     *
     * @param customer the customer
     * @param date     the date
     * @return the outstanding balance
     * @throws ArchetypeServiceException for any archetype service error
     */
    public BigDecimal getBalance(Party customer, Date date) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createUnallocatedObjectSetQuery(customer, DEBITS_CREDITS);
        Iterator<ObjectSet> iterator = new ObjectSetQueryIterator(service, query);
        query.add(Constraints.lt(START_TIME, date));
        return calculateBalance(iterator);
    }

    /**
     * Calculates the balance for a customer for all POSTED acts between two times, inclusive.
     *
     * @param customer       the customer
     * @param from           the from-time. If {@code null}, indicates that the time is unbounded
     * @param to             the to-time. If {@code null}, indicates that the time is unbounded
     * @param inclusive      if {@code true}, the to-time is inclusive, else it is exclusive
     * @param openingBalance the opening balance
     * @return the balance
     */
    public BigDecimal getBalance(Party customer, Date from, Date to, boolean inclusive, BigDecimal openingBalance) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createObjectSetQuery(customer, DEBITS_CREDITS, true);
        query.add(Constraints.eq("status", ActStatus.POSTED));
        if (from != null) {
            query.add(Constraints.gte(START_TIME, from));
        }
        if (to != null) {
            if (inclusive) {
                query.add(Constraints.lte(START_TIME, to));
            } else {
                query.add(Constraints.lt(START_TIME, to));
            }
        }
        Iterator<ObjectSet> iterator = new ObjectSetQueryIterator(service, query);
        return calculateDefinitiveBalance(iterator, openingBalance);
    }

    /**
     * Calculates a definitive outstanding balance for a customer.
     * This sums total amounts for <em>all</em> POSTED acts associated with the
     * customer, rather than just using unallocated acts, and can be used
     * to detect account balance errors.
     *
     * @param customer the customer
     * @return the outstanding balance
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws CustomerAccountRuleException if an opening or closing balance doesn't match the expected balance
     */
    public BigDecimal getDefinitiveBalance(Party customer) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer, ACCOUNT_ACTS);
        query.add(Constraints.sort(START_TIME));
        query.add(Constraints.sort("id"));
        query.add(new ObjectRefSelectConstraint("act"));
        query.add(new NodeSelectConstraint("amount"));
        query.add(new NodeSelectConstraint("credit"));
        query.add(Constraints.eq("status", ActStatus.POSTED));
        Iterator<ObjectSet> iterator = new ObjectSetQueryIterator(service, query);
        BigDecimal total = BigDecimal.ZERO;
        ActCalculator calculator = new ActCalculator(service);
        while (iterator.hasNext()) {
            ObjectSet set = iterator.next();
            BigDecimal amount = set.getBigDecimal(ACT_AMOUNT, BigDecimal.ZERO);
            boolean credit = set.getBoolean(ACT_CREDIT);
            IMObjectReference act = set.getReference("act.reference");
            if (act.isA(OPENING_BALANCE, CLOSING_BALANCE)) {
                if (act.isA(CLOSING_BALANCE)) {
                    credit = !credit;
                }
                BigDecimal balance = (credit) ? amount.negate() : amount;
                if (balance.compareTo(total) != 0) {
                    throw new CustomerAccountRuleException(InvalidBalance, act.getArchetype(), total, balance);
                }
            } else {
                total = calculator.addAmount(total, amount, credit);
            }
        }
        return total;
    }

    /**
     * Calculates the current overdue balance for a customer.
     * This is the sum of unallocated amounts date less than the specified overdue date, where the amount is greater
     * than zero.
     *
     * @param customer the customer
     * @param date     the overdue date
     * @return the overdue balance
     * @throws ArchetypeServiceException for any archetype service error
     */
    public BigDecimal getOverdueBalance(Party customer, Date date) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createUnallocatedObjectSetQuery(customer, DEBITS_CREDITS);
        // need to query credits as well, as there could be negative amounts
        query.add(Constraints.lt(START_TIME, date));
        Iterator<ObjectSet> iterator = new ObjectSetQueryIterator(service, query);

        BigDecimal amount = calculateBalance(iterator);
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            amount = BigDecimal.ZERO;
        }
        return amount;
    }

    /**
     * Calculates the overdue balance for a customer as of a particular date.
     * <p>
     * This sums any POSTED debits prior to <em>overdueDate</em> that had
     * not been fully allocated by credits as of <em>date</em>.
     *
     * @param customer    the customer
     * @param date        the date
     * @param overdueDate the date when amounts became overdue
     * @return the overdue balance
     * @throws ArchetypeServiceException for any archetype service error
     */
    public BigDecimal getOverdueBalance(Party customer, Date date, Date overdueDate) {

        NamedQuery query = new NamedQuery("getOverdueAmounts", Arrays.asList(
                "id", "total", "allocatedTotal", "allocatedAmount",
                "overdueAllocationTime"));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        query.setParameter("customer", customer.getId());
        query.setParameter("date", date);
        query.setParameter("overdueDate", overdueDate);

        ObjectSetQueryIterator iter = new ObjectSetQueryIterator(service, query);
        long lastId = -1;
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal allocatedTotal = BigDecimal.ZERO;
        BigDecimal result = BigDecimal.ZERO;
        while (iter.hasNext()) {
            ObjectSet set = iter.next();
            long uid = set.getLong("id");
            if (uid != lastId) {
                if (lastId != -1) {
                    result = result.add(total).subtract(allocatedTotal);
                }
                total = set.getBigDecimal("total", BigDecimal.ZERO);
                allocatedTotal = set.getBigDecimal("allocatedTotal", BigDecimal.ZERO);
                lastId = uid;
            }
            BigDecimal allocatedAmount = set.getBigDecimal("allocatedAmount", BigDecimal.ZERO);
            Date overdueAllocationTime = set.getDate("overdueAllocationTime");
            if (overdueAllocationTime != null) {
                // amount was allocated past date, so remove it
                allocatedTotal = allocatedTotal.subtract(allocatedAmount);
            }
        }
        result = result.add(total).subtract(allocatedTotal);
        return result;
    }

    /**
     * Returns the unbilled amount for a customer.
     *
     * @param customer the customer
     * @return the unbilled amount
     * @throws ArchetypeServiceException for any archetype service error
     */
    public BigDecimal getUnbilledAmount(Party customer) {
        String[] shortNames = {INVOICE, COUNTER, CREDIT};
        ArchetypeQuery query = CustomerAccountQueryFactory.createUnbilledObjectSetQuery(customer, shortNames);
        Iterator<ObjectSet> iterator = new ObjectSetQueryIterator(service, query);
        return calculateBalance(iterator);
    }

    /**
     * Returns the amount of an act yet to be allocated.
     *
     * @param act the act
     * @return the amount yet to be allocated
     */
    public BigDecimal getAllocatable(FinancialAct act) {
        return getAllocatable(act.getTotal(), act.getAllocatedAmount());
    }

    /**
     * Helper to return the amount that may be allocated from a total.
     * If either value is {@code null} they are treated as being {@code 0.0}.
     *
     * @param amount    the total amount. May be {@code null}
     * @param allocated the current amount allocated. May be {@code null}
     * @return {@code amount - allocated}
     */
    public BigDecimal getAllocatable(BigDecimal amount, BigDecimal allocated) {
        if (amount == null) {
            amount = BigDecimal.ZERO;
        }
        if (allocated == null) {
            allocated = BigDecimal.ZERO;
        }
        return amount.abs().subtract(allocated);
    }

    /**
     * Determines if the act has been fully allocated.
     *
     * @param act the act
     * @return {@code true} if the act has been full allocated
     */
    public boolean isAllocated(FinancialAct act) {
        return getAllocatable(act).compareTo(BigDecimal.ZERO) == 0;
    }

    /**
     * Calculates the outstanding balance.
     *
     * @param iterator an iterator over the collection
     * @return the outstanding balance
     */
    protected BigDecimal calculateBalance(Iterator<ObjectSet> iterator) {
        BigDecimal total = BigDecimal.ZERO;
        ActCalculator calculator = new ActCalculator(service);
        while (iterator.hasNext()) {
            ObjectSet set = iterator.next();
            BigDecimal amount = set.getBigDecimal(ACT_AMOUNT, BigDecimal.ZERO);
            BigDecimal allocated = set.getBigDecimal(ACT_ALLOCATED_AMOUNT, BigDecimal.ZERO);
            boolean credit = set.getBoolean(ACT_CREDIT);
            if (amount.signum() == -1) {
                credit = !credit;
            }
            BigDecimal unallocated = getAllocatable(amount, allocated);
            total = calculator.addAmount(total, unallocated, credit);
        }
        return total;
    }

    /**
     * Calculates a definitive balance, using act totals.
     *
     * @param iterator       an iterator over the collection
     * @param openingBalance the opening balance
     * @return the balance
     */
    protected BigDecimal calculateDefinitiveBalance(
            Iterator<ObjectSet> iterator, BigDecimal openingBalance) {
        BigDecimal total = openingBalance;
        ActCalculator calculator = new ActCalculator(service);
        while (iterator.hasNext()) {
            ObjectSet set = iterator.next();
            BigDecimal amount = set.getBigDecimal(ACT_AMOUNT, BigDecimal.ZERO);
            boolean credit = set.getBoolean(ACT_CREDIT);
            total = calculator.addAmount(total, amount, credit);
        }
        return total;
    }

}
