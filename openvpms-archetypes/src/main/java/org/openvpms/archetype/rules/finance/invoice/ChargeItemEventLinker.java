/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Helper to links charge item dispensing, investigation and document acts to patient clinical events.
 *
 * @author Tim Anderson
 */
public class ChargeItemEventLinker {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link ChargeItemEventLinker}.
     *
     * @param service the archetype service
     */
    public ChargeItemEventLinker(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Links a charge item's dispensing, investigation and document acts to the associated patient's clinical events.
     *
     * @param item    the charge item
     * @param changes the patient history changes
     */
    public void link(FinancialAct item, PatientHistoryChanges changes) {
        link(Collections.singletonList(item), changes);
    }

    /**
     * Links an item to an event.
     * <p/>
     * The item must be linked to the same patient as the event.
     *
     * @param event   the event to link to
     * @param item    the charge item
     * @param changes the patient history changes
     */
    public void link(Act event, Act item, PatientHistoryChanges changes) {
        link(event, Collections.singletonList(item), changes);
    }

    /**
     * Links items to an event.
     * <p/>
     * The items must be linked to the same patient as the event.
     *
     * @param event   the event to link to
     * @param items   the charge items
     * @param changes the patient history changes
     */
    public void link(Act event, List<Act> items, PatientHistoryChanges changes) {
        prepare(event, items, changes);
        changes.save();
    }

    /**
     * Links items to an event, recording the modifications in the supplied {@code changes}.
     * <p/>
     * Invoke {@link PatientHistoryChanges#save()} to commit the changes.
     *
     * @param event   the event
     * @param items   the charge items
     * @param changes the patient history changes
     */
    public void prepare(Act event, List<Act> items, PatientHistoryChanges changes) {
        for (Act item : items) {
            List<Act> acts = getActs(item, changes);
            changes.addToEvent(event, (List<org.openvpms.component.model.act.Act>) (List<?>) acts);
        }
    }

    /**
     * Links items to an event, recording the modifications in the supplied {@code changes}.
     * <p/>
     * Invoke {@link PatientHistoryChanges#save()} to commit the changes.
     *
     * @param event   the event
     * @param items   the charge items, and their corresponding dispensing, investigation and document acts
     * @param changes the patient history changes
     */
    public void prepare(Act event, Map<FinancialAct, List<Act>> items, PatientHistoryChanges changes) {
        for (Map.Entry<FinancialAct, List<Act>> entry : items.entrySet()) {
            List<Act> acts = new ArrayList<>();
            acts.add(entry.getKey());
            acts.addAll(entry.getValue());
            changes.addToEvent(event, (List<org.openvpms.component.model.act.Act>) (List<?>) acts);
        }
    }

    /**
     * Links multiple charge item's dispensing, investigation and document acts to the associated patient's clinical
     * events.
     *
     * @param items   the charge items
     * @param changes the patient history changes
     */
    public void link(List<FinancialAct> items, PatientHistoryChanges changes) {
        prepare(items, changes);
        changes.save();
    }

    /**
     * Links multiple charge item's dispensing, investigation and document acts to the associated patient's clinical
     * events. The modifications are recorded in the supplied {@code changes}.
     * <p/>
     * Invoke {@link PatientHistoryChanges#save()} to commit the changes.
     *
     * @param items   the charge items
     * @param changes the patient history changes
     */
    @SuppressWarnings("unchecked")
    public void prepare(List<FinancialAct> items, PatientHistoryChanges changes) {
        for (FinancialAct item : items) {
            List<Act> acts = getActs(item, changes);
            Date startTime = item.getActivityStartTime();
            if (startTime == null) {
                startTime = new Date();
            }
            changes.addToEvents((List<org.openvpms.component.model.act.Act>) (List<?>) acts, startTime);
        }
    }

    /**
     * Links multiple charge item's dispensing, investigation and document acts to the associated patient's clinical
     * events. The modifications are recorded in the supplied {@code changes}.
     * <p/>
     * Invoke {@link PatientHistoryChanges#save()} to commit the changes.
     *
     * @param items   the charge items, and their corresponding dispensing, investigation and document acts
     * @param changes the patient history changes
     */
    @SuppressWarnings("unchecked")
    public void prepare(Map<FinancialAct, List<Act>> items, PatientHistoryChanges changes) {
        for (Map.Entry<FinancialAct, List<Act>> entry : items.entrySet()) {
            List<Act> acts = new ArrayList<>();
            FinancialAct item = entry.getKey();
            acts.add(item);
            acts.addAll(entry.getValue());
            changes.addToEvents((List<org.openvpms.component.model.act.Act>) (List<?>) acts,
                                item.getActivityStartTime());
        }
    }

    /**
     * Links multiple clinical notes to the associated patient's clinical events.
     * The modifications are recorded in the supplied {@code changes}.
     * <p/>
     * Invoke {@link PatientHistoryChanges#save()} to commit the changes.
     *
     * @param notes   the notes
     * @param changes the patient history changes
     */
    @SuppressWarnings("unchecked")
    public void prepareNotes(List<Act> notes, PatientHistoryChanges changes) {
        for (Act act : notes) {
            Date startTime = act.getActivityStartTime();
            if (startTime == null) {
                startTime = new Date();
            }
            changes.addToEvents((List<org.openvpms.component.model.act.Act>) (List<?>) notes, startTime);
        }
    }

    /**
     * Returns the dispensing, investigations, and documents acts linked to a charge item.
     *
     * @param item    the charge item
     * @param changes the patient history changes
     * @return the acts
     */
    private List<Act> getActs(Act item, PatientHistoryChanges changes) {
        List<Act> acts = new ArrayList<>();
        IMObjectBean bean = service.getBean(item);
        acts.add(item);
        acts.addAll(getActs(bean, "dispensing", changes));
        acts.addAll(getActs(bean, "investigations", changes));
        acts.addAll(getActs(bean, "documents", changes));
        return acts;
    }

    private List<Act> getActs(IMObjectBean bean, String node, PatientHistoryChanges changes) {
        List<Act> result = new ArrayList<>();
        for (Reference ref : bean.getTargetRefs(node)) {
            Act act = (Act) changes.getObject(ref);
            if (act != null) {
                result.add(act);
            }
        }
        return result;
    }

}
