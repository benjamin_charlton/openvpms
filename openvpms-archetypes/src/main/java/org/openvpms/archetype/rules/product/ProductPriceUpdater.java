/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.math.CurrencyException;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectRefConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.openvpms.archetype.rules.product.ProductPriceUpdaterException.ErrorCode.NoPractice;


/**
 * Updates <em>productPrice.unitPrice</em>s associated with a product.
 *
 * @author Tim Anderson
 */
public class ProductPriceUpdater {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The product price rules.
     */
    private final ProductPriceRules rules;

    /**
     * The practice rules
     */
    private final PracticeRules practiceRules;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The practice currency.
     */
    private Currency currency;

    /**
     * Constructs a {@link ProductPriceUpdater}.
     *
     * @param priceRules    the product price rules
     * @param practiceRules the practice rules
     * @param service       the archetype service
     */
    public ProductPriceUpdater(ProductPriceRules priceRules, PracticeRules practiceRules, IArchetypeService service) {
        this.service = service;
        this.rules = priceRules;
        this.practiceRules = practiceRules;
    }

    /**
     * Updates any <em>productPrice.unitPrice</em> product prices associated with a product.
     *
     * @param product the product
     * @return a list of updated prices
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws ProductPriceUpdaterException if there is no practice
     */
    public List<ProductPrice> update(Product product) {
        List<ProductPrice> result;
        PriceCollector collector = new PriceCollector(service);
        if (needsUpdate(product)) {
            IMObjectBean bean = service.getBean(product);
            List<EntityLink> relationships = bean.getValues("suppliers", EntityLink.class);
            for (EntityLink relationship : relationships) {
                ProductSupplier ps = new ProductSupplier(relationship, service);
                doUpdate(ps, product, false, collector);
            }
            result = collector.save();
        } else {
            result = Collections.emptyList();
        }
        return result;
    }

    /**
     * Updates any <em>productPrice.unitPrice</em> product prices associated with a product.
     * <p/>
     * For prices that have changed:
     * <ul>
     *   <li>the To Date of the existing Unit Price is set to the current date/time</li>
     *   <li>a new Unit Price will be created with a From Date the same as the previous Unit Price's To Date and the
     *       the same Markup and Max Discount as the previous Unit Price</li>
     * </ul>
     * Returns a list of unit prices that have been updated and created. <br/>
     * New prices are only added to the product if {@code save == true}.     *
     *
     * @param product         the product
     * @param productSupplier the product-supplier relationship
     * @param save            if {@code true}, save updated prices
     * @return a list of updated prices
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws ProductPriceUpdaterException if there is no practice
     * @throws IllegalArgumentException     if the product is not that referred to by the product-supplier
     *                                      relationship
     */
    public List<ProductPrice> update(Product product, ProductSupplier productSupplier, boolean save) {
        return update(product, productSupplier, false, save);
    }

    /**
     * Determines if the cost node of any <em>productPrice.unitPrice</em> associated with a product active at the
     * current time, and recalculates its price.
     * <p/>
     * If a unit price needs to be updated, and it is:
     * <ul>
     *     <li>persistent, it will be closed off by setting the to-date to now, and a new price will be created starting
     *     at the same time, with no to-date.<br/>
     *     The new price will have the same markup, max discount, and pricing groups of the previous price.<br/>
     *     This ensures that a price history is maintained.</li>
     *     <li>non-persistent, it will be updated with the new cost and price</li>
     * </ul>
     * New prices are only added to the product if {@code save == true}.
     *
     * @param product            the product
     * @param productSupplier    the product-supplier relationship
     * @param ignoreCostDecrease if {@code true}, don't update any unit price if the new cost price would be less than
     *                           the existing cost price
     * @param save               if {@code true}, save updated prices
     * @return a list of new and updated prices
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws ProductPriceUpdaterException if there is no practice
     * @throws IllegalArgumentException     if the product is not that referred to by the product-supplier
     *                                      relationship
     */
    public List<ProductPrice> update(Product product, ProductSupplier productSupplier, boolean ignoreCostDecrease,
                                     boolean save) {
        PriceCollector collector = new PriceCollector(service);
        update(product, productSupplier, ignoreCostDecrease, collector);
        return (save) ? collector.save() : collector.deriveValues();
    }

    /**
     * Determines if the cost node of any <em>productPrice.unitPrice</em> associated with a product active at the
     * current time, and recalculates its price.
     * <p/>
     * If a unit price needs to be updated, and it is:
     * <ul>
     *     <li>persistent, it will be closed off by setting the to-date to now, and a new price will be created starting
     *     at the same time, with no to-date.<br/>
     *     The new price will have the same markup, max discount, and pricing groups of the previous price.<br/>
     *     This ensures that a price history is maintained.</li>
     *     <li>non-persistent, it will be updated with the new cost and price</li>
     * </ul>
     * New prices are only added to the product if {@code save == true}.
     *
     * @param product            the product
     * @param cost               the cost price
     * @param ignoreCostDecrease if {@code true}, don't update any unit price if the new cost price would be less than
     *                           the existing cost price
     * @param save               if {@code true}, save updated prices
     * @return a list of new and updated prices
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws ProductPriceUpdaterException if there is no practice
     */
    public List<ProductPrice> update(Product product, BigDecimal cost, boolean ignoreCostDecrease, boolean save) {
        PriceCollector collector = new PriceCollector(service);
        doUpdate(product, cost, ignoreCostDecrease, collector);
        return (save) ? collector.save() : collector.deriveValues();
    }

    /**
     * Determines if the cost node of any <em>productPrice.unitPrice</em> associated with a product active at the
     * current time, and recalculates its price.
     * <p/>
     * If a unit price needs to be updated, and it is:
     * <ul>
     *     <li>persistent, it will be closed off by setting the to-date to now, and a new price will be created starting
     *     at the same time, with no to-date.<br/>
     *     The new price will have the same markup, max discount, and pricing groups of the previous price.<br/>
     *     This ensures that a price history is maintained.</li>
     *     <li>non-persistent, it will be updated with the new cost and price</li>
     * </ul>
     *
     * @param product            the product
     * @param productSupplier    the product-supplier relationship
     * @param ignoreCostDecrease if {@code true}, don't update any unit price if the new cost price would be less than
     *                           the existing cost price
     * @param collector          collects the new and updated prices
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws ProductPriceUpdaterException if there is no practice
     * @throws IllegalArgumentException     if the product is not that referred to by the product-supplier
     *                                      relationship
     */
    public void update(Product product, ProductSupplier productSupplier, boolean ignoreCostDecrease,
                       PriceCollector collector) {
        doUpdate(productSupplier, product, ignoreCostDecrease, collector);
    }

    /**
     * Returns the practice.
     *
     * @return the practice
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws ProductPriceUpdaterException if there is no practice
     */
    public Party getPractice() {
        if (practice == null) {
            practice = practiceRules.getPractice();
            if (practice == null) {
                throw new ProductPriceUpdaterException(NoPractice);
            }
        }
        return practice;
    }

    /**
     * Determines if the prices associated with a product should be updated.
     *
     * @param product the product
     * @return {@code true} if prices should be updated
     */
    private boolean needsUpdate(Product product) {
        boolean update = true;
        if (!product.isNew()) {
            Product prior = (Product) service.get(product.getObjectReference());
            if (prior != null) {
                Set<EntityLink> oldSuppliers = getProductSuppliers(prior);
                Set<EntityLink> newSuppliers = getProductSuppliers(product);
                if (oldSuppliers.equals(newSuppliers)) {
                    update = !checkEquals(oldSuppliers, newSuppliers);
                }
            }
        }
        return update;
    }

    /**
     * Determines if the supplier relationships are equal based on list price and package size.
     *
     * @param oldSuppliers the old supplier relationships
     * @param newSuppliers the new supplier relationships
     * @return {@code true} if they are equal
     */
    private boolean checkEquals(Set<EntityLink> oldSuppliers, Set<EntityLink> newSuppliers) {
        Map<Reference, ProductSupplier> oldMap = getProductSuppliers(oldSuppliers);
        Map<Reference, ProductSupplier> newMap = getProductSuppliers(newSuppliers);
        for (Map.Entry<Reference, ProductSupplier> entry : newMap.entrySet()) {
            ProductSupplier supplier = entry.getValue();
            ProductSupplier old = oldMap.get(entry.getKey());
            if (old == null || !MathRules.equals(supplier.getListPrice(), old.getListPrice())
                || supplier.getPackageSize() != old.getPackageSize()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the product suppliers for a set of <em>entityLink.productSupplier</em> relationships.
     *
     * @param suppliers the product supplier relationships
     * @return the product suppliers, keyed on supplier reference.
     */
    private Map<Reference, ProductSupplier> getProductSuppliers(Set<EntityLink> suppliers) {
        Map<Reference, ProductSupplier> result = new HashMap<>();
        for (EntityLink supplier : suppliers) {
            result.put(supplier.getObjectReference(), new ProductSupplier(supplier, service));
        }
        return result;
    }

    /**
     * Determines if prices can be updated.
     * <p/>
     * Prices can be updated if:
     * <ul>
     * <li>autoPriceUpdate is {@code true}; and</li>
     * <li>costPrice &lt;&gt; 0; and</li>
     * <li>the supplier is active</li>
     * </ul>
     *
     * @param productSupplier the product-supplier relationship
     * @return {@code true} if prices can be updated
     */
    private boolean canUpdate(ProductSupplier productSupplier) {
        return productSupplier.isAutoPriceUpdate()
               && !MathRules.isZero(productSupplier.getCostPrice())
               && isActive(productSupplier.getSupplierRef());
    }

    /**
     * Updates prices.
     *
     * @param productSupplier    the product-supplier relationship
     * @param product            the product
     * @param ignoreCostDecrease if {@code true}, don't update any unit price if the new cost price would be less than
     *                           the existing cost price
     * @param collector          collects the new and updated prices
     */
    private void doUpdate(ProductSupplier productSupplier, Product product, boolean ignoreCostDecrease,
                          PriceCollector collector) {
        if (!product.getObjectReference().equals(productSupplier.getRelationship().getSource())) {
            throw new IllegalArgumentException("Argument 'product' is not that referred to by 'productSupplier'");
        }
        if (canUpdate(productSupplier)) {
            BigDecimal cost = productSupplier.getCostPrice();
            doUpdate(product, cost, ignoreCostDecrease, collector);
        }
    }

    /**
     * Updates prices.
     *
     * @param product            the product
     * @param cost               the cost price
     * @param ignoreCostDecrease if {@code true}, don't update any unit price if the new cost price would be less than
     *                           the existing cost price
     * @param collector          collects the new and updated prices
     */
    private void doUpdate(Product product, BigDecimal cost, boolean ignoreCostDecrease, PriceCollector collector) {
        List<ProductPrice> prices = rules.updateUnitPrices(product, cost, ignoreCostDecrease, getCurrency());
        if (!prices.isEmpty()) {
            collector.add(product, prices);
        }
    }

    /**
     * Returns the currency associated with the organisation practice.
     *
     * @return the currency
     * @throws ProductPriceUpdaterException if there is no practice
     * @throws CurrencyException            if the currency is invalid
     */
    private Currency getCurrency() {
        if (currency == null) {
            currency = practiceRules.getCurrency(getPractice());
        }
        return currency;
    }

    /**
     * Helper to determine if an object is active.
     * <p/>
     * This assumes that references to new objects that cannot be retrieved (i.e aren't yet in the transaction
     * context) are always active.
     * <p/>
     * It queries the database for persistent references to avoid pulling in large objects.
     *
     * @param reference the object's reference
     * @return {@code true} if the object is active
     */
    private boolean isActive(IMObjectReference reference) {
        boolean result = false;
        if (reference != null) {
            if (reference.isNew()) {
                IMObject object = service.get(reference);
                result = (object == null) || object.isActive();
            } else {
                ObjectRefConstraint constraint = new ObjectRefConstraint("o", reference);
                ArchetypeQuery query = new ArchetypeQuery(constraint);
                query.add(new NodeSelectConstraint("o.active"));
                query.setMaxResults(1);
                Iterator<ObjectSet> iter = new ObjectSetQueryIterator(service, query);
                if (iter.hasNext()) {
                    ObjectSet set = iter.next();
                    result = set.getBoolean("o.active");
                }
            }
        }

        return result;
    }

    private Set<EntityLink> getProductSuppliers(Product product) {
        IMObjectBean bean = service.getBean(product);
        return new HashSet<>(bean.getValues("suppliers", EntityLink.class));
    }
}
