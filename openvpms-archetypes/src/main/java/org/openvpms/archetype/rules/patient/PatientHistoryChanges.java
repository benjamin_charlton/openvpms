/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.BoardingHelper;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.openvpms.archetype.rules.patient.ClinicalEventHelper.distance;


/**
 * Tracks changes to patient histories.
 * <p/>
 * This can be used during invoicing to manage relationships between charge items and patient history items.
 *
 * @author Tim Anderson
 */
public class PatientHistoryChanges {

    /**
     * The location for new clinical events. May be {@code null}
     */
    private final Entity location;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The events, keyed on reference.
     */
    private final Map<Reference, Act> events = new HashMap<>();

    /**
     * Determines if events are linked to boarding appointments.
     */
    private final Map<Reference, Boolean> boarding = new HashMap<>();

    /**
     * The events for a patient, keyed on reference.
     */
    private final Map<Reference, List<Act>> eventsByPatient = new HashMap<>();

    /**
     * The acts to save.
     */
    private final Map<Reference, Act> toSave = new HashMap<>();

    /**
     * Used to determine if an event was new, prior to {@link #save()} being invoked.
     */
    private final Set<Reference> newEvents = new HashSet<>();

    /**
     * The objects to remove.
     */
    private final Set<IMObject> toRemove = new HashSet<>();

    /**
     * Event node name.
     */
    private static final String EVENT = "event";

    /**
     * Constructs a {@link PatientHistoryChanges}.
     *
     * @param location the location for new events. May be {@code null}
     * @param service  the archetype service
     */
    public PatientHistoryChanges(Entity location, ArchetypeService service) {
        this.location = location;
        this.service = service;
    }

    /**
     * Returns an event given its reference.
     *
     * @param reference the event reference. May be {@code null}
     * @return the corresponding event, or {@code null} if none is found
     */
    public Act getEvent(Reference reference) {
        Act event = null;
        if (reference != null) {
            event = events.get(reference);
            if (event == null) {
                event = service.get(reference, Act.class);
                if (event != null) {
                    events.put(reference, event);
                    Reference patient = getPatient(event);
                    if (patient != null) {
                        List<Act> list = eventsByPatient.computeIfAbsent(patient, k -> new ArrayList<>());
                        list.add(event);
                        sortEvents(list);
                    }
                }
            }
        }
        return event;
    }

    /**
     * Returns the patient reference associated with an event.
     *
     * @param event the event
     * @return the event's patient reference, or {@code null} if none is found
     */
    public Reference getPatient(Act event) {
        IMObjectBean bean = service.getBean(event);
        return getPatient(bean);
    }

    /**
     * Returns the events associated with a patient.
     *
     * @param patient the patient
     * @return the events, or {@code null} if none are found
     */
    public List<Act> getEvents(Reference patient) {
        return eventsByPatient.get(patient);
    }

    /**
     * Returns all events.
     *
     * @return all events
     */
    public List<Act> getEvents() {
        return eventsByPatient.values().stream().flatMap(List::stream).collect(Collectors.toList());
    }

    /**
     * Adds an event.
     * <p/>
     * If the event is new, it will be committed by {@link #save}.
     *
     * @param event the event to add
     */
    public void addEvent(Act event) {
        Reference ref = event.getObjectReference();
        if (events.putIfAbsent(ref, event) == null) {
            if (event.isNew()) {
                newEvents.add(ref);
            }
            IMObjectBean bean = service.getBean(event);
            if (event.isNew()) {
                // add location to new events
                if (location != null && bean.getTargetRef("location") == null) {
                    bean.setTarget("location", location);
                }
                toSave.put(ref, event);
            }

            Reference patient = getPatient(bean);
            if (patient != null) {
                List<Act> acts = eventsByPatient.computeIfAbsent(patient, k -> new ArrayList<>());
                acts.add(event);
                sortEvents(acts);
            }
        }
    }

    /**
     * Returns an event linked to an act.
     *
     * @param act the act
     * @return the linked event, or {@code null} if an event isn't linked to the act
     */
    public Act getLinkedEvent(Act act) {
        Reference ref = getLinkedEventRef(act);
        return getEvent(ref);
    }

    /**
     * Returns the reference of an event linked to an act.
     *
     * @param act the act
     * @return the linked event reference, or {@code null} if an event isn't linked to the act
     */
    public Reference getLinkedEventRef(Act act) {
        IMObjectBean bean = service.getBean(act);
        return bean.hasNode(EVENT) ? bean.getSourceRef(EVENT) : null;
    }

    /**
     * Adds acts to an event, where no relationship exists.
     *
     * @param event the event
     * @param acts  the acts to add
     */
    public void addToEvent(Act event, List<Act> acts) {
        addEvent(event);
        for (Act act : acts) {
            if (!hasRelationship(act)) {
                addRelationship(event, act);
            }
        }
    }

    /**
     * Adds a list of <em>act.patientMedication</em>, <em>act.patientInvestigation*</em>, <em>act.patientDocument*</em>,
     * and <em>act.customerAccountInvoiceItem</em> acts to an <em>act.patientClinicalEvent</em> associated with each
     * act's patient.
     * <p/>
     * If an act already has a relationship, but it belongs to a different patient, the relationship will be removed
     * and a relationship to the patient's own event added.
     *
     * @param acts      the acts to add
     * @param startTime the startTime used to select the event
     */
    public void addToEvents(List<Act> acts, Date startTime) {
        Map<Reference, List<Act>> map = getByPatient(acts, service);
        for (Map.Entry<Reference, List<Act>> entry : map.entrySet()) {
            Reference patient = entry.getKey();
            List<Act> unlinked = new ArrayList<>(); // the acts to link to events
            for (Act act : entry.getValue()) {
                Act existingEvent = getLinkedEvent(act);
                if (existingEvent != null) {
                    // the act is already linked to an event
                    if (!Objects.equals(getPatient(existingEvent), patient)) {
                        // the existing event is for a different patient. Need to unlink this.
                        removeRelationship(existingEvent, act);
                        unlinked.add(act);
                    }
                } else {
                    unlinked.add(act);
                }
            }
            if (!unlinked.isEmpty()) {
                Act event = getEventForAddition(patient, startTime, getClinician(unlinked));
                addToEvent(event, acts);
            }
        }
    }


    /**
     * Helper to add a relationship between an event and an act.
     *
     * @param event the event
     * @param act   the act
     */
    public void addRelationship(Act event, Act act) {
        IMObjectBean bean = service.getBean(event);
        String node = getRelationshipNode(act);
        ActRelationship relationship = (ActRelationship) bean.addTarget(node, act);
        act.addActRelationship(relationship);
        changed(event);
        changed(act);
    }

    /**
     * Helper to remove a relationship between an event and an act.
     *
     * @param event the event
     * @param act   the act
     */
    public void removeRelationship(Act event, Act act) {
        IMObjectBean bean = service.getBean(event);
        String node = getRelationshipNode(act);
        ActRelationship relationship = bean.getValue(node, ActRelationship.class, Predicates.targetEquals(act));
        if (relationship != null) {
            event.removeSourceActRelationship(relationship);
            act.removeTargetActRelationship(relationship);
            changed(event);
            changed(act);
        }
    }

    /**
     * Removes a relationship between an act and the its <em>act.patientClinicalEvent</em>
     * <p/>
     * If a relationship is removed, both the act and event will be queued for save.
     *
     * @param act the act
     */
    public void removeRelationship(Act act) {
        IMObjectBean bean = service.getBean(act);
        if (bean.hasNode(EVENT)) {
            ActRelationship relationship = bean.getValue(EVENT, ActRelationship.class, Predicates.targetEquals(act));
            if (relationship != null) {
                Act event = getEvent(relationship.getSource());
                if (event != null) {
                    removeRelationship(event, act);
                }
            }
        }
    }

    /**
     * Adds an invoice item document.
     * <p/>
     * This will add a relationship between them, and schedule them for commit.
     *
     * @param item     the invoice item
     * @param document the document
     */
    public void addItemDocument(FinancialAct item, Act document) {
        IMObjectBean bean = service.getBean(item);
        ActRelationship relationship = (ActRelationship) bean.addTarget("documents", document);
        document.addActRelationship(relationship);
        changed(item);
        changed(document);
    }

    /**
     * Removes an invoice item document.
     *
     * @param item     the invoice item
     * @param document the document
     */
    public void removeItemDocument(FinancialAct item, Act document) {
        changed(document);  // need to save the act with relationships removed, prior to removing it
        changed(item);
        toRemove.add(document);

        IMObjectBean itemBean = service.getBean(item);
        ActRelationship r = itemBean.getValue("documents", ActRelationship.class, Predicates.targetEquals(document));
        item.removeActRelationship(r);
        document.removeActRelationship(r);

        removeRelationship(document); // remove the event relationship
    }

    /**
     * Determines if an act has a relationship to an event.
     *
     * @param act the act
     * @return {@code true} if the act has a relationship, otherwise {@code false}
     */
    public boolean hasRelationship(Act act) {
        return getLinkedEventRef(act) != null;
    }

    /**
     * Determines if an event added via {@link #addEvent(Act)} was new prior to {@link #save()}  being invoked.
     *
     * @param event the event
     * @return {@code true} if the event was new prior to save
     */
    public boolean isNew(Act event) {
        return newEvents.contains(event.getObjectReference());
    }

    /**
     * Marks events as being completed, setting their end time.
     *
     * @param endTime the end time
     */
    public void complete(Date endTime) {
        for (Act event : events.values()) {
            if (!toRemove.contains(event)) {
                event.setStatus(ActStatus.COMPLETED);
                event.setActivityEndTime(endTime);
                changed(event);
            }
        }
    }

    /**
     * Saves any changes.
     */
    public void save() {
        if (!toSave.isEmpty()) {
            service.save(toSave.values());
        }
        if (!toRemove.isEmpty()) {
            service.save(toRemove);
            // need to save before removal in order to avoid ObjectDeletedException for old relationships
            for (IMObject object : toRemove) {
                service.remove(object);
            }
        }
    }

    /**
     * Retrieve an object given its reference.
     *
     * @param ref the reference
     * @return the object corresponding to the reference, or {@code null} if it can't be retrieved
     * @throws ArchetypeServiceException for any error
     */
    public IMObject getObject(Reference ref) {
        IMObject result = null;
        if (ref != null) {
            if (ref.isA(PatientArchetypes.CLINICAL_EVENT)) {
                result = getEvent(ref);
            } else {
                result = toSave.get(ref);
            }
            if (result == null) {
                result = service.get(ref);
            }
        }
        return result;
    }

    /**
     * Determines if an event is for boarding.
     *
     * @param act the event
     * @return {@code true} if the event is used for boarding
     */
    public boolean isBoarding(Act act) {
        Reference ref = act.getObjectReference();
        return boarding.computeIfAbsent(ref, reference -> {
            IMObjectBean bean = service.getBean(act);
            Act appointment = bean.getSource("appointment", Act.class);
            return appointment != null && BoardingHelper.isBoardingAppointment(appointment, service);
        });
    }

    /**
     * Returns an <em>act.patientClinicalEvent</em> that may have acts added.
     * <p/>
     * The <em>act.patientClinicalEvent</em> is selected as follows:
     * <ol>
     * <li>find the event intersecting the start time for the patient
     * <li>if it is an <em>IN_PROGRESS</em> and
     * <pre>event.startTime &gt;= (startTime - 1 week)</pre>
     * use it
     * <li>if it is <em>COMPLETED</em> and
     * <pre>startTime &gt;= event.startTime && startTime <= event.endTime</pre>
     * use it; otherwise
     * <li>create a new event, with <em>IN_PROGRESS</em> status and startTime
     * </ol>
     *
     * @param patient   the patient to use
     * @param timestamp the time to select the event
     * @param clinician the clinician to use when creating new events. May be {@code null}
     * @return an event
     */
    public Act getEventForAddition(Reference patient, Date timestamp, Reference clinician) {
        Act result = null;
        List<Act> patientEvents = getEvents(patient);
        if (patientEvents != null) {
            result = getClosestEvent(timestamp, patientEvents);
        }
        if (result == null) {
            result = ClinicalEventHelper.getEvent(patient, timestamp, service); // hit the database
            if (result != null) {
                addEvent(result);
            }
        }
        if (result != null && !canAddToEvent(result, timestamp)) {
            result = null;
        }
        if (result == null) {
            result = ClinicalEventHelper.createEvent(patient, timestamp, clinician, service);
            addEvent(result);
        }
        return result;
    }

    /**
     * Returns the closest event to a timestamp.
     *
     * @param timestamp the timestamp
     * @param events    the available clinical events for the patient
     * @return the closest event, or {@code null} if none is found
     */
    public Act getClosestEvent(Date timestamp, List<Act> events) {
        Act result = null;
        Date lowerBound = DateRules.getDate(timestamp);
        Date upperBound = DateRules.getNextDate(lowerBound);
        long resultDistance = 0;

        for (Act event : events) {
            if (DateRules.intersects(event.getActivityStartTime(), event.getActivityEndTime(), lowerBound,
                                     upperBound)) {
                if (result == null) {
                    resultDistance = distance(timestamp, event);
                    result = event;
                } else {
                    long distance = distance(timestamp, event);
                    if (distance < resultDistance) {
                        resultDistance = distance;
                        result = event;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Determines if an act can be added to an event.
     *
     * @param event     the event
     * @param startTime the act start time
     * @return {@code true} if the event can be added to, otherwise {@code false}
     */
    protected boolean canAddToEvent(Act event, Date startTime) {
        boolean result = true;
        // get date component of start time so not comparing time components
        Date startDate = DateRules.getDate(startTime);
        // get date component of event start and end datetimes

        Date eventStart = DateRules.getDate(event.getActivityStartTime());
        Date eventEnd = DateRules.getDate(event.getActivityEndTime());
        if (ActStatus.IN_PROGRESS.equals(event.getStatus())) {
            if (!isBoarding(event)) {
                Date date = DateRules.getDate(startDate, -1, DateUnits.WEEKS);
                if (eventStart.before(date)) {
                    result = false; // need to create a new event
                }
            }
        } else {  // COMPLETED
            if (startDate.before(eventStart)
                || (eventEnd != null && startDate.after(eventEnd))
                || (eventEnd == null && startDate.after(eventStart))) {
                result = false; // need to create a new event
            }
        }
        return result;
    }

    /**
     * Returns a map of acts keyed on their associated patient reference.
     *
     * @param acts the acts
     * @return the acts keyed on patient reference
     */
    protected static Map<Reference, List<Act>> getByPatient(List<Act> acts, ArchetypeService service) {
        Map<Reference, List<Act>> result = new HashMap<>();
        for (Act act : acts) {
            IMObjectBean bean = service.getBean(act);
            Reference patient = bean.getTargetRef("patient");
            if (patient != null) {
                List<Act> list = result.computeIfAbsent(patient, s -> new ArrayList<>());
                list.add(act);
                result.put(patient, list);
            }
        }
        return result;
    }

    /**
     * Returns the first clinician found in a Collection of Acts.
     *
     * @param acts a collection of Acts
     * @return the clinician, or {@code null} if none is found
     * @throws ArchetypeServiceException for any error
     */
    private Reference getClinician(Collection<Act> acts) {
        for (Act act : acts) {
            IMObjectBean bean = service.getBean(act);
            Reference clinician = bean.getTargetRef("clinician");
            if (clinician != null) {
                return clinician;
            }
        }
        return null;
    }

    /**
     * Returns the event relationship node name for a target act.
     *
     * @param act the act
     * @return the event node name
     */
    private String getRelationshipNode(Act act) {
        return act.isA(CustomerAccountArchetypes.INVOICE_ITEM) ? "chargeItems" : "items";
    }

    /**
     * Sorts events on ascending start time.
     *
     * @param events the events to sort
     */
    private void sortEvents(List<Act> events) {
        if (events.size() > 1) {
            events.sort((o1, o2) -> DateRules.compareTo(o1.getActivityStartTime(), o2.getActivityStartTime()));
        }
    }

    /**
     * Returns the patient associated with an act.
     *
     * @param bean the act bean
     * @return the patient reference, or {@code null} if none is found
     */
    private Reference getPatient(IMObjectBean bean) {
        return bean.getTargetRef("patient");
    }

    /**
     * Flags an act as being changed.
     *
     * @param act the changed act
     */
    private void changed(Act act) {
        toSave.put(act.getObjectReference(), act);
    }
}
