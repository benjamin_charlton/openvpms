/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Message archetype short names.
 *
 * @author Tim Anderson
 */
public class MessageArchetypes {

    /**
     * The audit message archetype short name.
     */
    public static final String AUDIT = "act.auditMessage";

    /**
     * The user message archetype short name.
     */
    public static final String USER = "act.userMessage";

    /**
     * The system message archetype short name.
     */
    public static final String SYSTEM = "act.systemMessage";

    /**
     * All system message archetypes.
     */
    public static String SYSTEM_MESSAGES = "act.systemMessage*";

    /**
     * All message archetypes.
     */
    public static final List<String> MESSAGES
            = Collections.unmodifiableList(Arrays.asList(USER, SYSTEM_MESSAGES, AUDIT));

    /**
     * Default constructor.
     */
    private MessageArchetypes() {
        // no-op
    }

}