/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.openvpms.archetype.rules.act.ActStatus;


/**
 * Act status types for <em>act.customerAppointment</em> and <em>act.customerTask</em> acts.
 *
 * @author Tim Anderson
 */
public class WorkflowStatus {

    /**
     * Pending status.
     */
    public static final String PENDING = "PENDING";

    /**
     * Cancelled status.
     */
    public static final String CANCELLED = ActStatus.CANCELLED;

    /**
     * In progress status.
     */
    public static final String IN_PROGRESS = ActStatus.IN_PROGRESS;

    /**
     * Billed status.
     */
    public static final String BILLED = "BILLED";

    /**
     * Completed status.
     */
    public static final String COMPLETED = ActStatus.COMPLETED;

    /**
     * Default constructor.
     */
    private WorkflowStatus() {
        // no-op
    }
}
