/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.openvpms.component.model.object.Reference;

import java.util.Date;

/**
 * A {@link Times} associated with a schedule.
 *
 * @author Tim Anderson
 */
public class ScheduleTimes extends Times {

    /**
     * The schedule.
     */
    private final Reference schedule;

    /**
     * Constructs an {@link Times} not associated with an existing appointment.
     *
     * @param reference the event reference, or {@code null} if the event hasn't been saved
     * @param schedule  the schedule reference
     * @param startTime the start time
     * @param endTime   the end time
     */
    public ScheduleTimes(Reference reference, Reference schedule, Date startTime, Date endTime) {
        super(reference, startTime, endTime);
        this.schedule = schedule;
    }

    /**
     * Returns the schedule.
     *
     * @return the schedule. May be {@code null}
     */
    public Reference getSchedule() {
        return schedule;
    }

    /**
     * Returns the schedule identifier.
     *
     * @return the schedule identifier, or {@code -1} if its not set
     */
    public long getScheduleId() {
        return schedule != null ? schedule.getId() : -1;
    }
}
