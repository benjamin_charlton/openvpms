/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;

/**
 * Archetype messages.
 *
 * @author Tim Anderson
 */
public class ArchetypeMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("ARCH", ArchetypeMessages.class.getName());

    /**
     * Default constructor.
     */
    private ArchetypeMessages() {

    }

    /**
     * Returns a message to indicate that a unit price was created by a supplier delivery.
     *
     * @param delivery the delivery act
     * @param supplier the supplier
     * @return a new message
     */
    public static Message priceCreatedByDelivery(Act delivery, Party supplier) {
        return messages.create(1501, delivery.getId(), supplier.getName(), delivery.getActivityStartTime());
    }
}
