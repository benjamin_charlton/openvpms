/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.patient;

import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.Function;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.supplier.SupplierRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.AbstractObjectFunctions;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * JXPath extension functions for patients.
 * <p>
 * JXPath cannot determine which methods to invoke when nulls are involved and will throw
 * {@code JXPathFunctionNotFoundException}  as result.<p/>
 * To avoid this methods accept Objects instead.
 *
 * @author Tim Anderson
 */
public class PatientFunctions extends AbstractObjectFunctions {

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The supplier rules.
     */
    private final SupplierRules supplierRules;

    /**
     * The appointment rules.
     */
    private final AppointmentRules appointmentRules;

    /**
     * The medical record rules.
     */
    private final MedicalRecordRules recordRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Policy to return the first patient participation from an act.
     */
    private static final Policy<Participation> PATIENT_PARTICIPATION
            = Policies.any(Participation.class, Predicates.isA(PatientArchetypes.PATIENT_PARTICIPATION));

    /**
     * Constructs a {@link PatientFunctions}.
     *
     * @param patientRules     the patient rules
     * @param supplierRules    the supplier rules
     * @param appointmentRules the appointment rules
     * @param recordRules      the medical record rules
     * @param service          the archetype service
     */
    public PatientFunctions(PatientRules patientRules, SupplierRules supplierRules, AppointmentRules appointmentRules,
                            MedicalRecordRules recordRules, IArchetypeService service) {
        super("patient");
        setObject(this);
        this.patientRules = patientRules;
        this.appointmentRules = appointmentRules;
        this.supplierRules = supplierRules;
        this.recordRules = recordRules;
        this.service = service;
    }

    /**
     * Returns the age of the patient as of the current date.
     * <p>
     * If the patient is deceased, the age of the patient when they died will be returned.
     * <p>
     * Invoked using: {@code patient:age()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the stringified form of the patient's age or {@code null} if the patient was not specified
     */
    public String age(ExpressionContext context) {
        return age(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the age of the patient as of the current date.
     * <p>
     * If the patient is deceased, the age of the patient when they died will be returned.
     * <p>
     * Invoked using: {@code patient:age(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the stringified form of the patient's age or {@code null} if the patient was not specified
     */
    public String age(Object patient) {
        Party p = getPatient(patient);
        return p != null ? patientRules.getPatientAge(p) : null;
    }

    /**
     * Returns the age of the patient as of the specified date.
     * <p>
     * Invoked using: {@code patient:age(patient, date)}
     *
     * @param patient the patient. May be {@code null}
     * @param date    the date. May be {@code null}
     * @return the stringified form of the patient's age or {@code null} if the patient was not specified
     */
    public String age(Object patient, Date date) {
        Party p = getPatient(patient);
        if (p != null) {
            return (date == null) ? patientRules.getPatientAge(p) : patientRules.getPatientAge(p, date);
        }
        return null;
    }

    /**
     * Returns the active alerts for a patient.
     * <p>
     * Invoked using: {@code patient:alerts()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the active alerts for the patient, or an empty list if there are none or the patient was {@code null}
     */
    public Iterable<Act> alerts(ExpressionContext context) {
        return alerts(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the active alerts for a patient.
     * <p>
     * Invoked using: {@code patient:alerts(.)}
     *
     * @param patient the patient. May be {@code null}
     * @return the active alerts for the patient, or an empty list if there are none or the patient was {@code null}
     */
    public Iterable<Act> alerts(Object patient) {
        Party p = getPatient(patient);
        return (p != null) ? patientRules.getAlerts(p) : Collections.emptyList();
    }

    /**
     * Returns pending appointments for a patient.
     * <p>
     * Invoked using: {@code patient:appointments(patient, interval, units)}
     *
     * @param patient  the patient. May be {@code null}
     * @param interval the interval, relative to the current date/time
     * @param units    the interval units
     * @return the pending appointments for the patient
     */
    public Iterable<Act> appointments(Object patient, int interval, String units) {
        Party p = getPatient(patient);
        if (p != null && interval > 0 && units != null) {
            return appointmentRules.getPendingPatientAppointments(p, interval, DateUnits.valueOf(units));
        }
        return Collections.emptyList();
    }

    /**
     * Returns the patient associated with the supplied context, if any.
     * <p>
     * If the supplied context is an act, the first participation.patient will be used to return the patient.
     * <p>
     * Invoked using: {@code patient:get()}
     *
     * @param context the expression context. Expected to refer to a patient or act
     * @return the patient, or {@code null} if the context doesn't refer to a patient
     */
    public Party get(ExpressionContext context) {
        return get(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the patient associated with the supplied object, if any.
     * <p>
     * If the supplied object is an act, the first participation.patient will be used to return the patient.
     * <p>
     * Invoked using: {@code patient:get(object)}
     *
     * @param object the object. May be a patient, act, or {@code null}
     * @return the patient, or {@code null} if the object doesn't refer to a patient
     */
    public Party get(Object object) {
        Party result = null;
        object = unwrap(object);
        if (object instanceof Party) {
            result = getPatient(object);
        } else if (object instanceof Act) {
            Act act = (Act) object;
            IMObjectBean bean = service.getBean(act);
            result = bean.getTarget(act.getParticipations(), Party.class, PATIENT_PARTICIPATION);
        }
        return result;
    }

    /**
     * Returns the most recent active identity for a patient, for the specified archetype.
     * <p>
     * Invoked using: {@code patient:identity('somearchetype')}
     *
     * @param context   the expression context. Expected to refer to a patient
     * @param archetype the identity archetype
     * @return the active identity, or {@code null} if none is found
     */
    public Identity identity(ExpressionContext context, String archetype) {
        return identity(context.getContextNodePointer().getValue(), archetype);
    }

    /**
     * Returns the most recent active identity for a patient, for the specified archetype.
     * <p>
     * Invoked using: {@code patient:identity(patient, 'somearchetype')}
     *
     * @param patient the patient. May be {@code null}
     * @return the active identity, or {@code null} if none is found
     */
    public Identity identity(Object patient, String archetype) {
        Party p = getPatient(patient);
        return (p != null) ? patientRules.getEntityIdentity(p, archetype) : null;
    }

    /**
     * Returns the active identities for a patient, of the specified archetype.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:identities(patient, 'somearchetype')}
     *
     * @param context   the expression context. Expected to refer to a patient
     * @param archetype the archetype
     * @return the identities, or an empty collection if no patient is specified or the patient has no identities
     */
    public Iterable<Identity> identities(ExpressionContext context, String archetype) {
        return identities(context.getContextNodePointer().getValue(), archetype);
    }

    /**
     * Returns the active identities for a patient, of the specified archetype.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:identities(patient, 'somearchetype')}
     *
     * @param patient   the patient. May be {@code null}
     * @param archetype the archetype
     * @return the identities, or an empty collection if no patient is specified or the patient has no identities
     */
    public Iterable<Identity> identities(Object patient, String archetype) {
        Party p = getPatient(patient);
        return (p != null) ? patientRules.getIdentities(p, archetype) : Collections.emptyList();
    }

    /**
     * Returns the most recent active microchip identity for a patient.
     * <p>
     * Invoked using: {@code patient:microchip()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the active microchip object, or {@code null} if none is found
     */
    public Identity microchip(ExpressionContext context) {
        return microchip(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the most recent active microchip identity for a patient.
     * <p>
     * Invoked using: {@code patient:microchip(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the active microchip object, or {@code null} if none is found
     */
    public Identity microchip(Object patient) {
        Party p = getPatient(patient);
        return (p != null) ? patientRules.getMicrochip(p) : null;
    }

    /**
     * Returns the active microchips for a patient.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:microchips()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the microchips, or an empty collection if no patient is specified or the patient has no microchips
     */
    public Iterable<Identity> microchips(ExpressionContext context) {
        return microchips(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the active microchips for a patient.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:microchips(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the microchips, or an empty collection if no patient is specified or the patient has no microchips
     */
    public Iterable<Identity> microchips(Object patient) {
        return identities(patient, PatientArchetypes.MICROCHIP);
    }

    /**
     * Returns the owner of the patient as of the current date.
     * <p>
     * Invoked using: {@code patient:owner()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the patient's owner, or {@code null} if none can be found
     */
    public Party owner(ExpressionContext context) {
        return owner(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the owner of the patient as of the current date.
     * <p>
     * Invoked using: {@code patient:owner(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the patient's owner, or {@code null} if none can be found
     */
    public Party owner(Object patient) {
        Party p = getPatient(patient);
        return (p != null) ? patientRules.getOwner(p) : null;
    }

    /**
     * Returns the owner of the patient as of the specified date.
     * <p>
     * Invoked using: {@code patient:owner(patient, date)}
     *
     * @param patient the patient. May be {@code null}
     * @param date    the date. May be {@code null}
     * @return the patient's owner, or {@code null} if none can be found
     */
    public Party owner(Object patient, Date date) {
        Party p = getPatient(patient);
        if (p != null) {
            return (date != null) ? patientRules.getOwner(p, date, false) : owner(p);
        }
        return null;
    }


    /**
     * Returns the most recent active pet tag for a patient.
     * <p>
     * Invoked using: {@code patient:petTag()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the active tag, or {@code null} if none is found
     */
    public Identity petTag(ExpressionContext context) {
        return petTag(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the most recent active pet tag for a patient.
     * <p>
     * Invoked using: {@code patient:petTag(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the active pet tag, or {@code null} if none is found
     */
    public Identity petTag(Object patient) {
        return identity(patient, PatientArchetypes.PET_TAG);
    }

    /**
     * Returns the active pet tags for a patient.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:petTags()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the pet tags, or an empty collection if no patient is specified or the patient has no tags
     */
    public Iterable<Identity> petTags(ExpressionContext context) {
        return petTags(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the active pet tags for a patient.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:petTags(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the pet tags, or an empty collection if no patient is specified or the patient has no tags
     */
    public Iterable<Identity> petTags(Object patient) {
        return identities(patient, PatientArchetypes.PET_TAG);
    }

    /**
     * Returns the most recent active rabies tag for a patient.
     * <p>
     * Invoked using: {@code patient:rabiesTag()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the active rabies tag, or {@code null} if none is found
     */
    public Identity rabiesTag(ExpressionContext context) {
        return rabiesTag(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the most recent active rabies tag for a patient.
     * <p>
     * Invoked using: {@code patient:rabiesTag(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the active rabies tag, or {@code null} if none is found
     */
    public Identity rabiesTag(Object patient) {
        return identity(patient, PatientArchetypes.RABIES_TAG);
    }

    /**
     * Returns the active rabies tags for a patient.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:rabiesTags()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the rabies tags, or an empty collection if no patient is specified or the patient has no rabies tags
     */
    public Iterable<Identity> rabiesTags(ExpressionContext context) {
        return rabiesTags(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the active rabies tags for a patient.
     * <p>
     * If there are multiple identities, these will be ordered with the highest id first.
     * <p>
     * Invoked using: {@code patient:rabiesTags(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the rabies tags, or an empty collection if no patient is specified or the patient has no rabies tags
     */
    public Iterable<Identity> rabiesTags(Object patient) {
        return identities(patient, PatientArchetypes.RABIES_TAG);
    }

    /**
     * Returns the referral vet for a patient as of the current date.
     * <p>
     * This is determined by the patient's associated party from the first matching
     * <em>entityRelationship.referredFrom</em> or <em>entityRelationship.referredTo</em> overlapping the current time.
     * <p>
     * Invoked using: {@code patient:referral(context)}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the referral vet, or {@code null} if there is no patient or the patient isn't being referred
     */
    public Party referral(ExpressionContext context) {
        return referral(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the referral vet for a patient as of the current date.
     * <p>
     * This is determined by the patient's associated party from the first matching
     * <em>entityRelationship.referredFrom</em> or <em>entityRelationship.referredTo</em> overlapping the current time.
     * <p>
     * Invoked using: {@code patient:referral(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the referral vet, or {@code null} if there is no patient or the patient isn't being referred
     */
    public Party referral(Object patient) {
        return referral(patient, false);
    }

    /**
     * Returns the referral vet for a patient as of the specified date.
     * <p>
     * This is determined by the patient's associated party from the first matching
     * <em>entityRelationship.referredFrom</em> or <em>entityRelationship.referredTo</em> overlapping the current time.
     * <p>
     * Invoked using: {@code patient:referral(patient, date)}
     *
     * @param patient the patient. May be {@code null}
     * @param date    the date. If {@code null} the current date is used
     * @return the referral vet, or {@code null} if there is no patient or the patient isn't being referred
     */
    public Party referralByDate(Object patient, Date date) {
        return referral(patient, false, date);
    }

    /**
     * Returns the referral practice or vet for a patient as of the current date.
     * <p>
     * This is determined by the patient's associated party from the first matching
     * <em>entityRelationship.referredFrom</em> or <em>entityRelationship.referredTo</em> overlapping the current time.
     * <p>
     * Invoked using: {@code patient:referral(patient, boolean)}
     *
     * @param patient  the patient. May be {@code null}
     * @param practice if  {@code true} return the referring vet's practice, otherwise return the referring vet
     * @return the referral practice or vet, or {@code null} if there is no patient or the patient isn't being referred
     */
    public Party referral(Object patient, boolean practice) {
        return referral(patient, practice, null);
    }

    /**
     * Returns the referral practice or vet for a patient as of the specied date.
     * <p>
     * If a patient is supplied, this is the patient's associated party from the first matching
     * <em>entityRelationship.referredFrom</em> or <em>entityRelationship.referredTo</em> overlapping the current time.
     * <br/>
     * If an act is supplied, this is the patient's associated party from the first matching
     * <em>entityRelationship.referredFrom</em> or <em>entityRelationship.referredTo</em> overlapping the act's start
     * time.
     * <p>
     * Invoked using: {@code patient:referral(patient, boolean, date)}
     *
     * @param object   the patient. May be {@code null}
     * @param date     the date. If {@code null} the current date is used
     * @param practice if  {@code true} return the referring vet's practice, otherwise return the referring vet
     * @return the referral practice or vet, or {@code null} if there is no patient or the patient isn't being referred
     */
    public Party referral(Object object, boolean practice, Date date) {
        Party result = null;
        Party patient = getPatient(object);
        if (patient != null) {
            if (date == null) {
                date = new Date();
            }
            Party vet = patientRules.getReferralVet(patient, date);
            if (practice) {
                if (vet != null) {
                    result = supplierRules.getReferralVetPractice(vet, date);
                }
            } else {
                result = vet;
            }
        }
        return result;
    }

    /**
     * Returns the most recent <em>act.patientClinicalEvent</em> (i.e. Visit) for a patient.
     * <p>
     * Invoked using: {@code patient:visit()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the most recent visit for {@code patient}, or {@code null} if none is found
     */
    public Act visit(ExpressionContext context) {
        return visit(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the most recent <em>act.patientClinicalEvent</em> (i.e. Visit) for a patient.
     * <p>
     * Invoked using: {@code patient:visit(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the most recent visit for {@code patient}, or {@code null} if none is found
     */
    public Act visit(Object patient) {
        Party p = getPatient(patient);
        return (p != null) ? recordRules.getEvent(p) : null;
    }

    /**
     * Returns the patient weight, in kilos.
     * <p>
     * This uses the most recent recorded weight for the patient.
     * <p>
     * Invoked using: {@code patient:weight()}
     *
     * @param context the expression context. Expected to refer to a patient
     * @return the patient weight, in kilos, or {@link BigDecimal#ZERO} if none exists or no patient was supplied
     */
    public BigDecimal weight(ExpressionContext context) {
        return weight(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the patient weight, in kilos.
     * <p>
     * This uses the most recent recorded weight for the patient.
     * <p>
     * Invoked using: {@code patient:weight(patient)}
     *
     * @param patient the patient. May be {@code null}
     * @return the patient weight, in kilos, or {@link BigDecimal#ZERO} if none exists or no patient was supplied
     */
    public BigDecimal weight(Object patient) {
        Party p = getPatient(patient);
        return (p != null) ? patientRules.getWeight(p).toKilograms() : BigDecimal.ZERO;
    }

    /**
     * Returns the patient weight, in the specified units.
     * <p>
     * This uses the most recent recorded weight for the patient.
     *
     * @param patient the patient. May be {@code null}
     * @param units   the units. One of {@code KILOGRAMS}, {@code GRAMS}, or {@code POUNDS}
     * @return the patient weight, in the specified units, or {@link BigDecimal#ZERO} if none exists or no patient was
     * supplied
     */
    public BigDecimal weight(Object patient, String units) {
        BigDecimal result = BigDecimal.ZERO;
        Party p = getPatient(patient);
        if (p != null) {
            Weight weight = patientRules.getWeight(p);
            result = weight.convert(WeightUnits.valueOf(units));
        }
        return result;
    }

    /**
     * Returns a Function, if any, for the specified namespace name and parameter types.
     * <p>
     * This implementation changes:
     * <ul>
     * <li>referral to {@link #referralByDate(Object, Date)} if the second argument is a date.</li>
     * </ul>
     * This is required as JXPath can't resolve which method to call if arguments are null or methods are overloaded
     * with the same number of arguments.
     *
     * @param namespace if it is not the namespace specified in the constructor, the method returns null
     * @param name      is a function name.
     * @return a MethodFunction, or {@code null} if there is no such function.
     */
    @Override
    public Function getFunction(String namespace, String name, Object[] parameters) {
        if ("referral".equals(name) && parameters != null && parameters.length == 2
            && parameters[1] instanceof Date) {
            name = "referralByDate";
        }
        return super.getFunction(namespace, name, parameters);
    }

    /**
     * Helper to get access to the actual patient supplied by JXPath.
     * <p>
     * This is a workaround to allow functions to be supplied null arguments, which JXPath handles by wrapping in a
     * list.
     *
     * @param object the object to unwrap
     * @return the unwrapped object. May be {@code null}
     */
    private Party getPatient(Object object) {
        object = unwrap(object);
        return (object instanceof Party && ((Party) object).isA(PatientArchetypes.PATIENT)) ? (Party) object : null;
    }

    /**
     * Helper to get access to the actual object supplied by JXPath.
     * <p>
     * This is a workaround to allow functions to be supplied null arguments, which JXPath handles by wrapping in a
     * list.
     *
     * @param object the object to unwrap
     * @return the unwrapped object. May be {@code null}
     */
    private Object unwrap(Object object) {
        if (object instanceof List) {
            List<?> values = (List<?>) object;
            object = !values.isEmpty() ? values.get(0) : null;
        }
        return object;
    }

}
