/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.investigation;

import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.openvpms.archetype.function.laboratory.InvestigationFunctions;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.jxpath.ObjectFunctions;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link InvestigationFunctions} class.
 *
 * @author Tim Anderson
 */
public class InvestigationFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;


    /**
     * Tests the {@link InvestigationFunctions#accountId(Object)}
     * and {@link InvestigationFunctions#accountId(ExpressionContext)} methods.
     * These are invoked using:
     * <ul>
     *     <li>investigation:accountId() where the context is an investigation; and</li>
     *     <li>investigation:accountId(investigation), when it isn't</li>
     * </ul>
     */
    @Test
    public void testAccountId() {
        Party patient = patientFactory.createPatient();
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Entity investigationTypeA = laboratoryFactory.newInvestigationType()
                .accountId("X", location1)
                .accountId("Y", location2)
                .build();
        Entity investigationTypeB = laboratoryFactory.createInvestigationType();
        DocumentAct investigation1 = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationTypeA)
                .build();
        DocumentAct investigation2 = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationTypeA)
                .location(location1)
                .build();
        DocumentAct investigation3 = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationTypeA)
                .location(location2)
                .build();
        DocumentAct investigation4 = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationTypeB)
                .location(location1)
                .build();

        // investigation with no location
        JXPathContext context1 = createContext(investigation1);
        assertNull(context1.getValue("investigation:accountId()"));
        assertNull(context1.getValue("investigation:accountId(.)"));

        // investigation with location and associated account id
        JXPathContext context2 = createContext(investigation2);
        assertEquals("X", context2.getValue("investigation:accountId()"));
        assertEquals("X", context2.getValue("investigation:accountId(.)"));

        // investigation with location and associated account id
        JXPathContext context3 = createContext(investigation3);
        assertEquals("Y", context3.getValue("investigation:accountId()"));
        assertEquals("Y", context3.getValue("investigation:accountId(.)"));

        // investigation with investigation type with no account ids
        JXPathContext context4 = createContext(investigation4);
        assertNull(context4.getValue("investigation:accountId()"));
        assertNull(context4.getValue("investigation:accountId(.)"));

        // test null handing
        JXPathContext context5 = createContext(investigation4);
        assertNull(context5.getValue("investigation:accountId(null)"));
    }

    /**
     * Tests the {@link InvestigationFunctions#accountIdForInvestigationType(Object, Object)} method.
     * This is invoked using:<br/>
     * {@code investigation:accountId(investigationType, location)}
     */
    @Test
    public void testAccountIdForInvestigationTypeAndLocation() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Entity investigationTypeA = laboratoryFactory.newInvestigationType()
                .accountId("X", location1)
                .accountId("Y", location2)
                .build();

        JXPathContext context1 = createContext(new Object());
        context1.getVariables().declareVariable("investigationType", investigationTypeA);
        context1.getVariables().declareVariable("location", location1);
        assertEquals("X", context1.getValue("investigation:accountId($investigationType, $location)"));

        // test null handling
        JXPathContext context2 = createContext(new Object());
        context2.getVariables().declareVariable("investigationType", null);
        context2.getVariables().declareVariable("location", null);
        assertNull(context2.getValue("investigation:accountId($investigationType, $location)"));
    }


    /**
     * Creates a new {@link JXPathContext} with the investigation functions registered.
     *
     * @param object the context object
     * @return a new JXPath context
     */
    private JXPathContext createContext(Object object) {
        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(new ObjectFunctions(new InvestigationFunctions(getArchetypeService()), "investigation"));
        return JXPathHelper.newContext(object, library);
    }


}
