/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import static org.openvpms.archetype.test.TestHelper.save;

/**
 * Factory for creating products.
 *
 * @author Tim Anderson
 */
public class TestProductFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestProductFactory}.
     *
     * @param service the service
     */
    public TestProductFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates and saves a medication product.
     *
     * @return the medication product
     */
    public Product createMedication() {
        return newMedication().build();
    }

    /**
     * Returns a builder for a new medication.
     *
     * @return a medication builder
     */
    public TestMedicationProductBuilder newMedication() {
        return new TestMedicationProductBuilder(service);
    }

    /**
     * Returns a builder to update a product.
     *
     * @param product the product
     * @return a medication builder
     */
    public TestMedicationProductBuilder updateMedication(Product product) {
        return new TestMedicationProductBuilder(product, service);
    }

    /**
     * Creates and saves a merchandise product.
     *
     * @return the merchandise product
     */
    public Product createMerchandise() {
        return newMerchandise().build();
    }

    /**
     * Returns a builder for a new merchandise product.
     *
     * @return a merchandise builder
     */
    public TestMerchandiseProductBuilder newMerchandise() {
        return new TestMerchandiseProductBuilder(service);
    }

    /**
     * Creates and saves a service product.
     *
     * @return the service product
     */
    public Product createService() {
        return newService().build();
    }

    /**
     * Returns a builder for a new service.
     *
     * @return a service builder
     */
    public TestServiceProductBuilder newService() {
        return new TestServiceProductBuilder(service);
    }

    /**
     * Creates and saves a template.
     *
     * @return the template product
     */
    public Product createTemplate() {
        return newTemplate().build();
    }

    /**
     * Returns a builder for a new template.
     *
     * @return a new template builder
     */
    public TestTemplateProductBuilder newTemplate() {
        return new TestTemplateProductBuilder(service);
    }

    /**
     * Returns a builder for a new fixed price.
     *
     * @return a new fixed price builder
     */
    public TestFixedPriceBuilder<?> newFixedPrice() {
        return new TestFixedPriceBuilder<>(service);
    }

    /**
     * Returns a builder for a new unit price.
     *
     * @return a new fixed price builder
     */
    public TestUnitPriceBuilder<?> newUnitPrice() {
        return new TestUnitPriceBuilder<>(service);
    }

    /**
     * Creates a new product type.
     *
     * @return the product type
     */
    public Entity createProductType() {
        Entity entity = service.create(ProductArchetypes.PRODUCT_TYPE, Entity.class);
        entity.setName(TestHelper.randomName("zproducttype"));
        save(entity);
        return entity;
    }

    /**
     * Returns a builder for a new product batch.
     *
     * @return a new product batch builder
     */
    public TestBatchBuilder newBatch() {
        return new TestBatchBuilder(service);
    }

    /**
     * Returns a builder for a new discount.
     *
     * @return a new discount builder
     */
    public TestDiscountBuilder newDiscount() {
        return new TestDiscountBuilder(service);
    }
}

