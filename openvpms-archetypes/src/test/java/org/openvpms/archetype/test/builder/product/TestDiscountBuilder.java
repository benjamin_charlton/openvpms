/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.finance.discount.DiscountArchetypes;
import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for <entity.discountType</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDiscountBuilder extends AbstractTestEntityBuilder<Entity, TestDiscountBuilder> {

    /**
     * The discount rate.
     */
    private ValueStrategy rate = ValueStrategy.defaultValue();

    /**
     * The discount type.
     */
    private ValueStrategy type = ValueStrategy.defaultValue();

    /**
     * Determines if the fixed price is discounted.
     */
    private ValueStrategy discountFixedPrice = ValueStrategy.defaultValue();

    /**
     * Constructs an {@link AbstractTestEntityBuilder}.
     *
     * @param service the archetype service
     */
    public TestDiscountBuilder(ArchetypeService service) {
        super(DiscountArchetypes.DISCOUNT_TYE, Entity.class, service);
        name(ValueStrategy.random("zdiscount"));
    }

    /**
     * Makes the discount a percentage discount, specifying the rate.
     *
     * @param rate the discount rate
     * @return this
     */
    public TestDiscountBuilder percentage(BigDecimal rate) {
        return rate(rate, DiscountRules.PERCENTAGE);
    }

    /**
     * Makes the discount a fixed discount, specifying the rate.
     *
     * @param rate the discount rate
     * @return this
     */
    public TestDiscountBuilder fixed(BigDecimal rate) {
        return rate(rate, DiscountRules.FIXED);
    }

    /**
     * Makes the discount an at-cost with rate discount.
     *
     * @param rate the discount rate
     * @return this
     */
    public TestDiscountBuilder costRate(BigDecimal rate) {
        return rate(rate, DiscountRules.COST_RATE);
    }

    /**
     * Determines if the fixed price is discounted.
     *
     * @param discountFixedPrice if {@code true}, the fixed price is discounted, otherwise only the unit price is
     *                           discounted
     * @return this
     */
    public TestDiscountBuilder discountFixedPrice(boolean discountFixedPrice) {
        this.discountFixedPrice = ValueStrategy.value(discountFixedPrice);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        rate.setValue(bean, "rate");
        type.setValue(bean, "type");
        discountFixedPrice.setValue(bean, "discountFixed");
    }

    /**
     * Sets the discount rate and type.
     *
     * @param rate the rate
     * @param type the discount type
     * @return this
     */
    private TestDiscountBuilder rate(BigDecimal rate, String type) {
        this.rate = ValueStrategy.value(rate);
        this.type = ValueStrategy.value(type);
        return this;
    }
}