/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.reminderCount</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderCountBuilder extends AbstractTestEntityBuilder<Entity, TestReminderTypeBuilder> {

    /**
     * The parent builder.
     */
    private final TestReminderTypeBuilder parent;

    /**
     * The count.
     */
    private ValueStrategy count = ValueStrategy.defaultValue();

    /**
     * The interval.
     */
    private ValueStrategy interval = ValueStrategy.defaultValue();

    /**
     * The interval units.
     */
    private ValueStrategy units = ValueStrategy.defaultValue();

    /**
     * Constructs a {@link TestReminderCountBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestReminderCountBuilder(TestReminderTypeBuilder parent, ArchetypeService service) {
        super(ReminderArchetypes.REMINDER_COUNT, Entity.class, service);
        this.parent = parent;
    }

    /**
     * Sets the count.
     *
     * @param count the count
     * @return this
     */
    public TestReminderCountBuilder count(int count) {
        this.count = ValueStrategy.value(count);
        return this;
    }

    /**
     * Sets the interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestReminderCountBuilder interval(int interval, DateUnits units) {
        this.interval = ValueStrategy.value(interval);
        this.units = ValueStrategy.value(units);
        return this;
    }

    /**
     * Adds the count to the reminder type.
     *
     * @return the reminder type builder
     */
    public TestReminderTypeBuilder add() {
        Entity count = build(false);
        parent.addReminderCount(count);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        count.setValue(bean, "count");
        interval.setValue(bean, "interval");
        units.setValue(bean, "units");
    }
}