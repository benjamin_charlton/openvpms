/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>product.template</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestTemplateProductBuilder extends AbstractTestProductBuilder<TestTemplateProductBuilder> {

    /**
     * The links to the products that the template includes.
     */
    private final List<EntityLink> includes = new ArrayList<>();

    /**
     * The note to include on invoices.
     */
    private String invoiceNote;

    /**
     * The note to include on visits.
     */
    private String visitNote;


    /**
     * Constructs a {@link TestTemplateProductBuilder}.
     *
     * @param service the archetype service
     */
    public TestTemplateProductBuilder(ArchetypeService service) {
        super(ProductArchetypes.TEMPLATE, service);
    }

    /**
     * Sets the invoice note.
     * <p/>
     * This is included on invoices when the template expands.
     *
     * @param invoiceNote the invoice note
     * @return this
     */
    public TestTemplateProductBuilder invoiceNote(String invoiceNote) {
        this.invoiceNote = invoiceNote;
        return this;
    }

    /**
     * Sets the visit note.
     * <p/>
     * This is included on visits when the template expands.
     *
     * @param visitNote the visit note
     * @return this
     */
    public TestTemplateProductBuilder visitNote(String visitNote) {
        this.visitNote = visitNote;
        return this;
    }

    /**
     * Returns a builder to add a product that that the template includes.
     *
     * @return a new builder
     */
    public TestTemplateIncludeBuilder include() {
        return new TestTemplateIncludeBuilder(this, getService());
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Product object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (invoiceNote != null) {
            bean.setValue("invoiceNote", invoiceNote);
        }
        if (visitNote != null) {
            bean.setValue("visitNote", visitNote);
        }
        if (!includes.isEmpty()) {
            int sequence = 0;
            Reference reference = object.getObjectReference();
            for (EntityLink include : includes) {
                include.setSource(reference);
                IMObjectBean includeBean = getService().getBean(include);
                includeBean.setValue("sequence", ++sequence);
                object.addEntityLink(include);
            }
            includes.clear(); // can't re-use
        }
    }

    /**
     * Adds a product inclusion link.
     *
     * @param link the link. An <em>entityLink.productIncludes</em>
     */
    void include(EntityLink link) {
        includes.add(link);
    }
}
