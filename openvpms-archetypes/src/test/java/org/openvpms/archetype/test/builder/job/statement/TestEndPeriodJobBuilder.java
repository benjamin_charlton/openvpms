/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.job.statement;

import org.openvpms.archetype.test.builder.job.AbstractTestJobBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.jobEndPeriod</em>.
 *
 * @author Tim Anderson
 */
public class TestEndPeriodJobBuilder extends AbstractTestJobBuilder<TestEndPeriodJobBuilder> {

    /**
     * Determines if completed charges should be posted.
     */
    private ValueStrategy postCompletedCharges = ValueStrategy.defaultValue();

    /**
     * Constructs a {@link TestEndPeriodJobBuilder}.
     *
     * @param service the archetype service
     */
    public TestEndPeriodJobBuilder(ArchetypeService service) {
        super("entity.jobEndPeriod", service);
    }

    /**
     * Determines if charges with COMPLETED status should be changed to POSTED.
     *
     * @param post if {@code true}, post completed charges
     * @return this
     */
    public TestEndPeriodJobBuilder postCompletedCharges(boolean post) {
        postCompletedCharges = ValueStrategy.value(post);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        postCompletedCharges.setValue(bean, "postCompletedCharges");
    }
}