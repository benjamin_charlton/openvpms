/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.verifier.customer.account;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Calendar.SECOND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Verifies an invoice item matches that expected.
 *
 * @author Tim Anderson
 */
public class TestInvoiceItemVerifier {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The expected reminder types references and corresponding reminder details.
     */
    private final Map<Reference, Reminder> expectedReminders = new HashMap<>();

    /**
     * The expected alert types and corresponding due dates.
     */
    private final Map<Reference, Alert> expectedAlerts = new HashMap<>();

    /**
     * The expected patient.
     */
    private Reference patient;

    /**
     * The expected product.
     */
    private Reference product;

    /**
     * The expected batch.
     */
    private Reference batch;

    /**
     * The expected quantity.
     */
    private BigDecimal quantity = BigDecimal.ONE;

    /**
     * The expected minimum quantity.
     */
    private BigDecimal minQuantity = BigDecimal.ZERO;

    /**
     * The expected fixed cost.
     */
    private BigDecimal fixedCost = BigDecimal.ZERO;

    /**
     * The expected fixed price.
     */
    private BigDecimal fixedPrice = BigDecimal.ZERO;

    /**
     * The expected unit cost.
     */
    private BigDecimal unitCost = BigDecimal.ZERO;

    /**
     * The expected unit price.
     */
    private BigDecimal unitPrice = BigDecimal.ZERO;

    /**
     * The expected discount.
     */
    private BigDecimal discount = BigDecimal.ZERO;

    /**
     * The expected tax.
     */
    private BigDecimal tax = BigDecimal.ZERO;

    /**
     * The expected total.
     */
    private BigDecimal total = BigDecimal.ZERO;

    /**
     * The expected product template.
     */
    private Reference template;

    /**
     * The expected product template expansion group.
     */
    private Integer group;

    /**
     * The expected clinician;
     */
    private Reference clinician;

    /**
     * The expected user that created the item.
     */
    private Reference createdBy;

    /**
     * The expected print flag.
     */
    private boolean print = true;

    /**
     * The expected service ratio.
     */
    private BigDecimal serviceRatio;

    /**
     * Medication label, if a medication is present.
     */
    private String label;

    /**
     * The medication expiry date, if a medication is present.
     */
    private Date expiryDate;

    /**
     * The expected visit.
     */
    private Reference visit;

    /**
     * Constructs a {@link TestInvoiceItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestInvoiceItemVerifier(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Sets the expected patient.
     *
     * @param patient the expected patient
     * @return this
     */
    public TestInvoiceItemVerifier patient(Party patient) {
        this.patient = (patient != null) ? patient.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected product.
     *
     * @param product the expected product
     * @return this
     */
    public TestInvoiceItemVerifier product(Product product) {
        this.product = (product != null) ? product.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected batch.
     *
     * @param batch the expected batch
     */
    public TestInvoiceItemVerifier batch(Entity batch) {
        this.batch = (batch != null) ? batch.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected  quantity.
     *
     * @param quantity the expected quantity
     * @return this
     */
    public TestInvoiceItemVerifier quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the expected quantity.
     *
     * @param quantity the expected quantity
     * @return this
     */
    public TestInvoiceItemVerifier quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * Sets the expected minimum quantity.
     *
     * @param minQuantity the expected minimum quantity
     * @return this
     */
    public TestInvoiceItemVerifier minQuantity(int minQuantity) {
        return minQuantity(BigDecimal.valueOf(minQuantity));
    }

    /**
     * Sets the expected minimum quantity.
     *
     * @param minQuantity the expected minimum quantity
     * @return this
     */
    public TestInvoiceItemVerifier minQuantity(BigDecimal minQuantity) {
        this.minQuantity = minQuantity;
        return this;
    }

    /**
     * Sets the expected fixed cost.
     *
     * @param fixedCost the expected fixed cost
     * @return this
     */
    public TestInvoiceItemVerifier fixedCost(int fixedCost) {
        return fixedCost(BigDecimal.valueOf(fixedCost));
    }

    /**
     * Sets the expected fixed cost.
     *
     * @param fixedCost the expected fixed cost
     * @return this
     */
    public TestInvoiceItemVerifier fixedCost(String fixedCost) {
        return fixedCost(new BigDecimal(fixedCost));
    }

    /**
     * Sets the expected fixed cost.
     *
     * @param fixedCost the expected fixed cost
     * @return this
     */
    public TestInvoiceItemVerifier fixedCost(BigDecimal fixedCost) {
        this.fixedCost = fixedCost;
        return this;
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestInvoiceItemVerifier fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestInvoiceItemVerifier fixedPrice(String fixedPrice) {
        return fixedPrice(new BigDecimal(fixedPrice));
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestInvoiceItemVerifier fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
        return this;
    }

    /**
     * Sets the expected unit cost.
     *
     * @param unitCost the expected unit cost
     */
    public TestInvoiceItemVerifier unitCost(int unitCost) {
        return unitCost(BigDecimal.valueOf(unitCost));
    }

    /**
     * Sets the expected unit cost.
     *
     * @param unitCost the expected unit cost
     */
    public TestInvoiceItemVerifier unitCost(String unitCost) {
        return unitCost(new BigDecimal(unitCost));
    }

    /**
     * Sets the expected unit cost.
     *
     * @param unitCost the expected unit cost
     */
    public TestInvoiceItemVerifier unitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
        return this;
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public TestInvoiceItemVerifier unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public TestInvoiceItemVerifier unitPrice(String unitPrice) {
        return unitPrice(new BigDecimal(unitPrice));
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public TestInvoiceItemVerifier unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    /**
     * Sets the expected service ratio.
     *
     * @param serviceRatio the expected service ratio
     * @return this
     */
    public TestInvoiceItemVerifier serviceRatio(int serviceRatio) {
        return serviceRatio(BigDecimal.valueOf(serviceRatio));
    }

    /**
     * Sets the expected service ratio.
     *
     * @param serviceRatio the expected service ratio
     * @return this
     */
    public TestInvoiceItemVerifier serviceRatio(BigDecimal serviceRatio) {
        this.serviceRatio = serviceRatio;
        return this;
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public TestInvoiceItemVerifier discount(int discount) {
        return discount(BigDecimal.valueOf(discount));
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public TestInvoiceItemVerifier discount(String discount) {
        return discount(new BigDecimal(discount));
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public TestInvoiceItemVerifier discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    /**
     * Sets the expected tax.
     *
     * @param tax the expected tax
     */
    public TestInvoiceItemVerifier tax(int tax) {
        return tax(BigDecimal.valueOf(tax));
    }

    /**
     * Sets the expected tax.
     *
     * @param tax the expected tax
     */
    public TestInvoiceItemVerifier tax(String tax) {
        return tax(new BigDecimal(tax));
    }

    /**
     * Sets the expected tax.
     *
     * @param tax the expected tax
     */
    public TestInvoiceItemVerifier tax(BigDecimal tax) {
        this.tax = tax;
        return this;
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public TestInvoiceItemVerifier total(int total) {
        return total(BigDecimal.valueOf(total));
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public TestInvoiceItemVerifier total(String total) {
        return total(new BigDecimal(total));
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public TestInvoiceItemVerifier total(BigDecimal total) {
        this.total = total;
        return this;
    }

    /**
     * Sets the expected product template.
     *
     * @param template the template. May be {@code null}
     * @return this
     */
    public TestInvoiceItemVerifier template(Product template) {
        this.template = (template != null) ? template.getObjectReference() : null;
        if (template == null) {
            group = null;
        }
        return this;
    }

    /**
     * Sets the expected template expansion group.
     *
     * @param group the expected template expansion group
     * @return this
     */
    public TestInvoiceItemVerifier group(int group) {
        this.group = group;
        return this;
    }

    /**
     * Sets the expected clinician.
     *
     * @param clinician the expected clinician
     */
    public TestInvoiceItemVerifier clinician(User clinician) {
        this.clinician = clinician != null ? clinician.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected created-by user.
     *
     * @param createdBy the expected created-by user
     * @return this
     */
    public TestInvoiceItemVerifier createdBy(User createdBy) {
        this.createdBy = (createdBy != null) ? createdBy.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected print flag.
     *
     * @param print the expected print flag
     * @return this
     */
    public TestInvoiceItemVerifier print(boolean print) {
        this.print = print;
        return this;
    }

    /**
     * Sets the medication label and expiry date, for medication products.
     *
     * @param label      the medication label
     * @param expiryDate the medication expiry date
     */
    public TestInvoiceItemVerifier medication(String label, Date expiryDate) {
        this.label = label;
        this.expiryDate = expiryDate;
        return this;
    }

    /**
     * Sets the visit.
     *
     * @param visit the visit
     * @return this
     */
    public TestInvoiceItemVerifier visit(Act visit) {
        this.visit = (visit != null) ? visit.getObjectReference() : null;
        return this;
    }

    /**
     * Adds a reminder.
     *
     * @param reminderType the expected reminder type
     * @param dueDate      the expected due date
     * @param nextDueDate  the expected next due date
     * @param status       the expected status
     */
    public TestInvoiceItemVerifier addReminder(Entity reminderType, Date dueDate, Date nextDueDate, String status) {
        expectedReminders.put(reminderType.getObjectReference(), new Reminder(dueDate, nextDueDate, status));
        return this;
    }

    /**
     * Removes reminders.
     *
     * @return this
     */
    public TestInvoiceItemVerifier resetReminders() {
        expectedReminders.clear();
        return this;
    }

    /**
     * Adds an alert.
     *
     * @param alertType the expected alert type
     * @param endDate   the expected end date
     * @param status    the expected status
     */
    public TestInvoiceItemVerifier addAlert(Entity alertType, Date endDate, String status) {
        expectedAlerts.put(alertType.getObjectReference(), new Alert(alertType, endDate, status));
        return this;
    }

    /**
     * Removes alerts.
     *
     * @return this
     */
    public TestInvoiceItemVerifier resetAlerts() {
        expectedAlerts.clear();
        return this;
    }

    /**
     * Verifies that the estimate item matches that expected.
     *
     * @param item the estimate item
     */
    public void verify(Act item) {
        IMObjectBean bean = service.getBean(item);
        assertEquals(patient, bean.getTargetRef("patient"));
        assertEquals(product, bean.getTargetRef("product"));
        assertEquals(batch, bean.getTargetRef("batch"));
        assertEquals(template, bean.getTargetRef("template"));
        checkEquals(quantity, bean.getBigDecimal("quantity"));
        checkEquals(minQuantity, bean.getBigDecimal("minQuantity"));
        checkEquals(fixedCost, bean.getBigDecimal("fixedCost"));
        checkEquals(fixedPrice, bean.getBigDecimal("fixedPrice"));
        checkEquals(unitCost, bean.getBigDecimal("unitCost"));
        checkEquals(unitPrice, bean.getBigDecimal("unitPrice"));
        checkEquals(discount, bean.getBigDecimal("discount"));
        checkEquals(tax, bean.getBigDecimal("tax"));
        checkEquals(total, bean.getBigDecimal("total"));
        Participation participation = bean.getObject("template", Participation.class);
        if (participation == null) {
            assertNull(group);
        } else {
            IMObjectBean participationBean = service.getBean(participation);
            assertEquals(group, participationBean.getValue("group"));
        }
        assertEquals(clinician, bean.getTargetRef("clinician"));
        assertEquals(createdBy, item.getCreatedBy());
        checkEquals(serviceRatio, bean.getBigDecimal("serviceRatio"));
        assertEquals(print, bean.getBoolean("print"));

        assertEquals(visit, bean.getSourceRef("event"));

        if (product.isA(ProductArchetypes.MEDICATION)) {
            verifyMedication(bean.getTarget("dispensing", Act.class));
        } else {
            assertNull(bean.getTarget("dispensing", Act.class));
            assertNull(label);
            assertNull(expiryDate);
        }

        verifyReminders(item, bean.getTargets("reminders", Act.class));
        verifyAlerts(item, bean.getTargets("alerts", Act.class));
    }

    /**
     * Verifies there is an invoice item that matches the criteria.
     *
     * @param items invoice items
     * @return the matching item
     */
    public Act verify(List<Act> items) {
        Act item = FinancialTestHelper.find(items, patient, product, null);
        if (item == null) {
            fail("Failed to find invoice item for patient=" + patient + ", product=" + product);
        }
        verify(item);
        return item;
    }

    /**
     * Verifies there is an invoice item that matches the criteria.
     *
     * @param itemRelationships invoice item relationships
     * @param sequence          the expected relationship sequence
     * @return the matching item
     */
    public Act verify(RelatedIMObjects<Act, ActRelationship> itemRelationships, int sequence) {
        List<Act> items = IterableUtils.toList(itemRelationships.getObjects());
        Act act = verify(items);
        ActRelationship relationship = itemRelationships.getRelationships().stream()
                .filter(r -> r.getTarget().equals(act.getObjectReference()))
                .findFirst()
                .orElse(null);
        assertNotNull(relationship);
        assertEquals(sequence, relationship.getSequence());
        return act;
    }

    /**
     * Verifies a medication act is present with the expected details.
     *
     * @param medication the medication
     */
    private void verifyMedication(Act medication) {
        assertNotNull(medication);
        IMObjectBean bean = service.getBean(medication);
        assertEquals(patient, bean.getTargetRef("patient"));
        assertEquals(product, bean.getTargetRef("product"));
        assertEquals(batch, bean.getTargetRef("batch"));
        checkEquals(quantity, bean.getBigDecimal("quantity"));
        assertEquals(label, bean.getString("label"));
        assertEquals(expiryDate, medication.getActivityEndTime());
        assertEquals(clinician, bean.getTargetRef("clinician"));
        assertEquals(visit, bean.getSourceRef("event"));
    }

    /**
     * Verifies the expected reminders are present.
     *
     * @param item      the invoice item
     * @param reminders the actual reminders
     */
    private void verifyReminders(Act item, List<Act> reminders) {
        assertEquals(expectedReminders.size(), reminders.size());
        for (Act reminder : reminders) {
            IMObjectBean bean = service.getBean(reminder);
            Reminder expected = expectedReminders.get(bean.getTargetRef("reminderType"));
            assertNotNull(expected);
            assertEquals(patient, bean.getTargetRef("patient"));
            assertEquals(product, bean.getTargetRef("product"));
            Date initialTime = DateUtils.truncate(bean.getDate("initialTime"), SECOND); // details nodes store ms
            assertEquals(0, DateRules.compareTo(item.getActivityStartTime(), initialTime));
            assertEquals(0, DateRules.compareTo(expected.getDueDate(), reminder.getActivityEndTime()));
            assertEquals(0, DateRules.compareTo(expected.getNextDueDate(), reminder.getActivityStartTime()));
            assertEquals(expected.getStatus(), reminder.getStatus());
        }
    }

    /**
     * Verifies the expected alerts are present.
     *
     * @param item   the invoice item
     * @param alerts the actual alerts
     */
    private void verifyAlerts(Act item, List<Act> alerts) {
        assertEquals(expectedAlerts.size(), alerts.size());
        for (Act alert : alerts) {
            IMObjectBean bean = service.getBean(alert);
            Alert expected = expectedAlerts.get(bean.getTargetRef("alertType"));
            assertNotNull(expected);
            assertEquals(item.getActivityStartTime(), alert.getActivityStartTime());
            assertEquals(expected.getEndDate(), alert.getActivityEndTime());

            assertEquals(patient, bean.getTargetRef("patient"));
            assertEquals(product, bean.getTargetRef("product"));
            assertEquals(clinician, bean.getTargetRef("clinician"));
            assertEquals(expected.getReason(), alert.getReason());
            assertEquals(expected.getStatus(), alert.getStatus());
        }
    }

    private class Alert {

        private final Entity alertType;

        private final Date endDate;

        private final String status;

        private final String reason;

        public Alert(Entity alertType, Date endDate, String status) {
            this.alertType = alertType;
            this.endDate = endDate;
            this.status = status;
            reason = service.getBean(alertType).getString("reason");
        }

        public Entity getAlertType() {
            return alertType;
        }

        public Date getEndDate() {
            return endDate;
        }

        public String getReason() {
            return reason;
        }

        public String getStatus() {
            return status;
        }
    }

    /**
     * Expected reminder details.
     */
    private static class Reminder {

        /**
         * The expected due date.
         */
        private final Date dueDate;

        /**
         * Expected next due date.
         */
        private final Date nextDueDate;

        /**
         * Expected status.
         */
        private final String status;

        /**
         * Constructs a {@link Reminder}.
         *
         * @param dueDate     the expected due date
         * @param nextDueDate the expected next due date
         * @param status      the expected status
         */
        public Reminder(Date dueDate, Date nextDueDate, String status) {
            this.dueDate = dueDate;
            this.nextDueDate = nextDueDate;
            this.status = status;
        }

        /**
         * Returns the expected due date.
         *
         * @return the expected due date
         */
        public Date getDueDate() {
            return dueDate;
        }

        /**
         * Returns the expected next due date.
         *
         * @return the expected next due date
         */
        public Date getNextDueDate() {
            return nextDueDate;
        }

        /**
         * Returns the expected status.
         *
         * @return the expected status
         */
        public String getStatus() {
            return status;
        }
    }
}
