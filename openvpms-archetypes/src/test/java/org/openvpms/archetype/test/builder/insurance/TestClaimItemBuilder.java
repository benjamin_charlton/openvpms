/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.insurance;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Test insurance claim item builder.
 *
 * @author Tim Anderson
 */
public class TestClaimItemBuilder extends AbstractTestIMObjectBuilder<FinancialAct, TestClaimItemBuilder> {

    /**
     * The parent claim builder.
     */
    private final TestClaimBuilder parent;

    /**
     * The treatment start time.
     */
    private Date startTime = new Date();

    /**
     * The treatment end time.
     */
    private Date endTime = new Date();

    /**
     * The VeNom diagnosis lookup code.
     */
    private String diagnosis;

    /**
     * The invoice items being claimed.
     */
    private FinancialAct[] invoiceItems;

    /**
     * Constructs a {@link TestClaimItemBuilder}.
     *
     * @param parent  the parent claim builder
     * @param service the archetype service
     */
    public TestClaimItemBuilder(TestClaimBuilder parent, ArchetypeService service) {
        super(InsuranceArchetypes.CLAIM_ITEM, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Sets the treatment dates.
     *
     * @param startTime the treatment start time
     * @param endTime   the treatment end time
     * @return this
     */
    public TestClaimItemBuilder treatmentDates(Date startTime, Date endTime) {
        treatmentStart(startTime);
        return treatmentEnd(endTime);
    }

    /**
     * Sets the treatment start time.
     *
     * @param startTime the start time
     * @return this
     */
    public TestClaimItemBuilder treatmentStart(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    /**
     * Sets the treatment end time.
     *
     * @param endTime the end time
     * @return this
     */
    public TestClaimItemBuilder treatmentEnd(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    /**
     * Sets the VeNom diagnosis code.
     *
     * @param diagnosis the lookup code
     * @return this
     */
    public TestClaimItemBuilder diagnosis(String diagnosis) {
        return diagnosis(diagnosis, diagnosis, diagnosis);
    }

    /**
     * Sets the VeNom diagnosis code.
     *
     * @param code         the lookup code
     * @param name         the lookup name
     * @param dictionaryId the VeNom dictionary identifier for the code
     * @return this
     */
    public TestClaimItemBuilder diagnosis(String code, String name, String dictionaryId) {
        Lookup lookup = new TestLookupBuilder("lookup.diagnosisVeNom", getService())
                .code(code)
                .name(name)
                .build(false);
        IMObjectBean bean = getBean(lookup);
        bean.setValue("dataDictionaryId", dictionaryId);
        bean.save();
        this.diagnosis = lookup.getCode();
        return this;
    }

    /**
     * Sets the invoice items to claim.
     *
     * @param invoiceItems the invoice items
     * @return this
     */
    public TestClaimItemBuilder invoiceItems(FinancialAct... invoiceItems) {
        this.invoiceItems = invoiceItems;
        return this;
    }

    /**
     * Adds the item to the parent claim.
     *
     * @return the parent claim builder
     */
    public TestClaimBuilder add() {
        FinancialAct item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        object.setActivityStartTime(startTime);
        object.setActivityEndTime(endTime);
        if (diagnosis != null) {
            object.setReason(diagnosis);
        }

        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        if (invoiceItems != null) {
            for (FinancialAct invoiceItem : invoiceItems) {
                amount = amount.add(invoiceItem.getTotal());
                tax = tax.add(invoiceItem.getTaxAmount());
                bean.addTarget("items", invoiceItem, "claims");
                toSave.add(invoiceItem);
            }
        }
        bean.setValue("amount", amount);
        bean.setValue("tax", tax);
    }
}
