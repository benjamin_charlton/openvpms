/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * A builder <em>act.calendarEvent</em>, <em>act.calendarBlock</em> and <em>act.rosterEvent</em> instances, for testing
 * purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestCalendarEventBuilder<B extends AbstractTestCalendarEventBuilder<B>>
        extends AbstractTestActBuilder<Act, B> {

    /**
     * The schedule.
     */
    private Entity schedule;

    /**
     * Constructs an {@link AbstractTestCalendarEventBuilder}.
     *
     * @param archetype the event archetype
     * @param service   the archetype service
     */
    public AbstractTestCalendarEventBuilder(String archetype, ArchetypeService service) {
        super(archetype, Act.class, service);
    }

    /**
     * Sets the schedule.
     *
     * @param schedule the schedule
     * @return this
     */
    public B schedule(Entity schedule) {
        this.schedule = schedule;
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (schedule != null) {
            bean.setTarget("schedule", schedule);
        }
    }
}