/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.test.builder.patient.AbstractTestPatientActBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.Set;

/**
 * Builder for <em>act.patientReminder</em> for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderBuilder extends AbstractTestPatientActBuilder<Act, TestReminderBuilder> {

    /**
     * The reminder rules.¶
     */
    private final ReminderRules rules;

    /**
     * The date to base due date calculations on.
     */
    private Date date;

    /**
     * The due date.
     */
    private Date dueDate;

    /**
     * The next due date.
     */
    private Date nextDueDate;

    /**
     * The reminder type.
     */
    private Entity reminderType;

    /**
     * The reminder count.
     */
    private int count;

    /**
     * Constructs an {@link AbstractTestPatientActBuilder}.
     *
     * @param reminderRules the reminder rules
     * @param service       the archetype service
     */
    public TestReminderBuilder(ReminderRules reminderRules, ArchetypeService service) {
        super(ReminderArchetypes.REMINDER, Act.class, service);
        this.rules = reminderRules;
    }

    /**
     * Sets the reminder date.
     * <p/>
     * This is the date from which the due date is calculated.
     * <p/>
     * If not set, defaults to the time when the reminder is built.
     *
     * @param date the date
     * @return this
     */
    public TestReminderBuilder date(Date date) {
        this.date = date;
        return this;
    }

    /**
     * Sets the reminder due date.
     * <p/>
     * If not set, it will be calculated using the {@link #date(Date)} and the {@link #reminderType(Entity)}.
     *
     * @param dueDate the due date
     * @return this
     */
    public TestReminderBuilder dueDate(Date dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    /**
     * Sets the reminder next due date.
     * <p/>
     * If not set, it will be calculated using the {@link #dueDate(Date) due date},
     * the {@link #reminderType(Entity) reminder type}, and the {@link #count(int) count}.
     *
     * @param nextDueDate the next due date
     * @return this
     */
    public TestReminderBuilder nextDueDate(Date nextDueDate) {
        this.nextDueDate = nextDueDate;
        return this;
    }

    /**
     * Sets the reminder type.
     *
     * @param reminderType the reminder type
     * @return this
     */
    public TestReminderBuilder reminderType(Entity reminderType) {
        this.reminderType = reminderType;
        return this;
    }

    /**
     * Sets the reminder count.
     *
     * @param count the reminder count
     * @return this
     */
    public TestReminderBuilder count(int count) {
        this.count = count;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave) {
        Date initial = (date != null) ? date : new Date();
        Date due = (dueDate != null) ? dueDate : rules.calculateReminderDueDate(initial, reminderType);
        endTime(due);

        Date nextDue = (nextDueDate != null) ? nextDueDate : rules.getNextReminderDate(due, reminderType, count);
        if (nextDue == null) {
            nextDue = due;
        }
        startTime(nextDue);
        super.build(object, bean, toSave);
        bean.setValue("initialTime", initial);
        bean.setTarget("reminderType", reminderType);
    }
}