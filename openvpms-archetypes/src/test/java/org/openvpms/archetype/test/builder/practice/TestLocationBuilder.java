/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>party.organisationLocation</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLocationBuilder extends AbstractTestPartyBuilder<Party, TestLocationBuilder> {

    /**
     * Determines if online booking is enabled.
     */
    private ValueStrategy onlineBooking = ValueStrategy.defaultValue();

    /**
     * The tills.
     */
    private Entity[] tills;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * Determines if stock control  is enabled.
     */
    private ValueStrategy stockControl = ValueStrategy.defaultValue();

    /**
     * The schedule views.
     */
    private Entity[] scheduleViews;

    /**
     * The over-the-counter party.
     */
    private Party otc;

    /**
     * Product type service ratios.
     */
    private Map<Entity, BigDecimal> serviceRatios = new HashMap<>();

    /**
     * Constructs a {@link TestLocationBuilder}.
     *
     * @param service the archetype service
     */
    public TestLocationBuilder(ArchetypeService service) {
        super(PracticeArchetypes.LOCATION, Party.class, service);
        name(ValueStrategy.random("zlocation"));
    }

    /**
     * Constructs a {@link TestLocationBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestLocationBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Make the location available for online booking.
     *
     * @return this
     */
    public TestLocationBuilder onlineBooking() {
        return onlineBooking(true);
    }

    /**
     * Determines if the location is available for online booking.
     *
     * @param onlineBooking if {@code true}, the location is available for online booking
     * @return this
     */
    public TestLocationBuilder onlineBooking(boolean onlineBooking) {
        this.onlineBooking = ValueStrategy.value(onlineBooking);
        return this;
    }

    /**
     * Sets the tills.
     *
     * @param tills the tills
     * @return this
     */
    public TestLocationBuilder tills(Entity... tills) {
        this.tills = tills;
        return this;
    }

    /**
     * Sets the stock location for the location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public TestLocationBuilder stockLocation(Party stockLocation) {
        this.stockLocation = stockLocation;
        return this;
    }

    /**
     * Determines if stock control is enabled.
     *
     * @param stockControl if {@code true}, stock control is enabled, else it is disabled
     * @return this
     */
    public TestLocationBuilder stockControl(boolean stockControl) {
        this.stockControl = ValueStrategy.value(stockControl);
        return this;
    }

    /**
     * Sets the Over-the-Counter party for the location.
     *
     * @param otc the OTC party
     * @return this
     */
    public TestLocationBuilder otc(Party otc) {
        this.otc = otc;
        return this;
    }

    /**
     * Sets the schedule views.
     *
     * @param scheduleViews the schedule views
     * @return this
     */
    public TestLocationBuilder scheduleViews(Entity... scheduleViews) {
        this.scheduleViews = scheduleViews;
        return this;
    }

    /**
     * Sets the service ratio for a product type at the location.
     *
     * @param productType the product type
     * @param ratio       the service ratio
     * @return this
     */
    public TestLocationBuilder addServiceRatio(Entity productType, int ratio) {
        return addServiceRatio(productType, BigDecimal.valueOf(ratio));
    }

    /**
     * Sets the service ratio for a product type at the location.
     *
     * @param productType the product type
     * @param ratio       the service ratio
     * @return this
     */
    public TestLocationBuilder addServiceRatio(Entity productType, BigDecimal ratio) {
        serviceRatios.put(productType, ratio);
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        onlineBooking.setValue(bean, "onlineBooking");
        if (tills != null) {
            for (Entity till : tills) {
                bean.addTarget("tills", till, "locations");
                toSave.add(till);
            }
        }
        if (stockLocation != null) {
            bean.addTarget("stockLocations", stockLocation, "locations");
            toSave.add(stockLocation);
        }
        stockControl.setValue(bean, "stockControl");
        if (otc != null) {
            bean.addTarget("OTC", otc, "location");
            toSave.add(otc);
            otc = null; // can't reuse as only 1 relationship allowed
        }
        if (scheduleViews != null) {
            for (Entity scheduleView : scheduleViews) {
                bean.addTarget("scheduleViews", scheduleView);
            }
        }

        if (!serviceRatios.isEmpty()) {
            for (Map.Entry<Entity, BigDecimal> entry : serviceRatios.entrySet()) {
                Relationship relationship = bean.addTarget("serviceRatios", entry.getKey());
                getBean(relationship).setValue("ratio", entry.getValue());
            }
            serviceRatios.clear();
        }
    }
}
