/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builds <em>act.customerAccountChargesCounter</em> instances, for testing purposes
 *
 * @author Tim Anderson
 */
public class TestCounterSaleBuilder
        extends AbstractTestCustomerChargeBuilder<TestCounterSaleBuilder, TestCounterSaleItemBuilder> {

    /**
     * Determines if reminders should be sent.
     */
    private ValueStrategy sendReminder = ValueStrategy.defaultValue();


    /**
     * Constructs a {@link TestCounterSaleBuilder}.
     *
     * @param service the archetype service
     */
    public TestCounterSaleBuilder(ArchetypeService service) {
        super(CustomerAccountArchetypes.COUNTER, service);
    }

    /**
     * Determines if reminders should be sent for unpaid counter sales.
     *
     * @param sendReminder if {@code true}, send reminders
     * @return this
     */
    public TestCounterSaleBuilder sendReminder(boolean sendReminder) {
        this.sendReminder = ValueStrategy.value(sendReminder);
        return this;
    }

    /**
     * Returns a builder to add a counter sale item.
     *
     * @return a counter sale item builder
     */
    @Override
    public TestCounterSaleItemBuilder item() {
        return new TestCounterSaleItemBuilder(this, getService());
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        sendReminder.setValue(bean, "sendReminder");
    }
}