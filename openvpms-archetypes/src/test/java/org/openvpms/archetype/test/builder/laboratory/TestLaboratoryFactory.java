/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating laboratories and laboratory tests, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLaboratoryFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestLaboratoryFactory}.
     *
     * @param service the service
     */
    public TestLaboratoryFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates and saves a new investigation type.
     *
     * @return the investigation type
     */
    public Entity createInvestigationType() {
        return newInvestigationType().build();
    }

    /**
     * Creates and saves a new investigation type.
     *
     * @param laboratory the laboratory
     * @return the investigation type
     */
    public Entity createInvestigationType(Entity laboratory) {
        return newInvestigationType().laboratory(laboratory).build();
    }

    /**
     * Returns a builder to create an investigation type.
     *
     * @return the investigation type builder
     */
    public TestInvestigationTypeBuilder newInvestigationType() {
        return new TestInvestigationTypeBuilder(service);
    }

    /**
     * Creates and saves a new test laboratory.
     *
     * @return an <em>entity.laboratoryServiceTest</em>
     */
    public Entity createLaboratory() {
        return newLaboratory().build();
    }

    /**
     * Creates and saves a new test laboratory, linked to practice locations.
     *
     * @param locations the practice locations
     * @return an <em>entity.laboratoryServiceTest</em>
     */
    public Entity createLaboratory(Party... locations) {
        return newLaboratory().locations(locations).build();
    }

    /**
     * Returns a builder for a new test laboratory service.
     *
     * @return a laboratory service builder
     */
    public TestLaboratoryBuilder newLaboratory() {
        return new TestLaboratoryBuilder(service);
    }

    /**
     * Returns a builder for the specified laboratory service.
     *
     * @param archetype the laboratory service archetype. Must be an <em>entity.laboratoryService*</em>
     * @return a laboratory service builder
     */
    public TestLaboratoryBuilder newLaboratory(String archetype) {
        return new TestLaboratoryBuilder(archetype, service);
    }

    /**
     * Creates and saves a new laboratory device.
     *
     * @param laboratory the laboratory
     * @return the device
     */
    public Entity createDevice(Entity laboratory) {
        return newDevice().laboratory(laboratory).build();
    }

    /**
     * Creates and saves a new laboratory device, linked to practice locations.
     *
     * @param laboratory the laboratory
     * @param locations  the locations
     * @return the device
     */
    public Entity createDevice(Entity laboratory, Party... locations) {
        return newDevice().laboratory(laboratory).locations(locations).build();
    }

    /**
     * Returns a builder for a laboratory device.
     *
     * @return a laboratory device builder
     */
    public TestDeviceBuilder newDevice() {
        return new TestDeviceBuilder(service);
    }

    /**
     * Returns a builder for the specified laboratory device.
     *
     * @param archetype the laboratory device archetype. Must be an <em>entity.laboratoryDevice*</em>
     * @return a laboratory device builder
     */
    public TestDeviceBuilder newDevice(String archetype) {
        return new TestDeviceBuilder(archetype, service);
    }

    /**
     * Returns a builder for a new laboratory test.
     *
     * @param investigationType the investigation type
     * @return a test builder
     */
    public Entity createTest(Entity investigationType) {
        return newTest().investigationType(investigationType).build();
    }

    /**
     * Returns a builder for a new laboratory test.
     *
     * @return a test builder
     */
    public TestLaboratoryTestBuilder newTest() {
        return new TestLaboratoryTestBuilder(service);
    }

    /**
     * Creates and saves an HL7 laboratory.
     *
     * @param location the practice location where the laboratory is used
     * @param user     the service user
     * @return the HL7 laboratory
     */
    public Entity createHL7Laboratory(Party location, User user) {
        return new TestHL7LaboratoryBuilder(service).location(location).user(user).build();
    }

    /**
     * Creates and saves an HL7 laboratory group.
     *
     * @param laboratories the laboratories in the group
     * @return the HL7 laboratory group
     */
    public Entity createHL7LaboratoryGroup(Entity... laboratories) {
        return new TestHL7LaboratoryGroupBuilder(service).laboratories(laboratories).build();
    }
}
