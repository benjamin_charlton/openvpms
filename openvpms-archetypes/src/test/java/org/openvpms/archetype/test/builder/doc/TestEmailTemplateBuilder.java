/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.documentTemplateEmail*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEmailTemplateBuilder extends AbstractTestDocumentTemplateBuilder<TestEmailTemplateBuilder> {

    /**
     * The parent builder.
     */
    private final TestDocumentTemplateBuilder parent;

    /**
     * The email subject.
     */
    private String subject;

    /**
     * The subject type.
     */
    private String subjectType;

    /**
     * The content type.
     */
    private String contentType;

    /**
     * The content.
     */
    private String content;

    /**
     * Constructs a {@link TestEmailTemplateBuilder}.
     *
     * @param archetype the email template archetype
     * @param service   the archetype service
     * @param handlers  the document handlers
     */
    public TestEmailTemplateBuilder(String archetype, ArchetypeService service, DocumentHandlers handlers) {
        this(null, archetype, service, handlers);
    }

    /**
     * Constructs a {@link TestEmailTemplateBuilder}.
     *
     * @param template the template to update
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    TestEmailTemplateBuilder(Entity template, ArchetypeService service, DocumentHandlers handlers) {
        super(template, service, handlers);
        this.parent = null;
    }

    /**
     * Constructs a {@link TestEmailTemplateBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the email template archetype
     * @param service   the archetype service
     * @param handlers  the document handlers
     */
    TestEmailTemplateBuilder(TestDocumentTemplateBuilder parent, String archetype, ArchetypeService service,
                             DocumentHandlers handlers) {
        super(archetype, service, handlers);
        this.parent = parent;
        name(ValueStrategy.random("zemailtemplate"));
    }

    /**
     * Sets the email subject.
     *
     * @param subject the subject
     * @return this
     */
    public TestEmailTemplateBuilder subject(String subject) {
        this.subject = subject;
        return this;
    }

    /**
     * Sets the email subject type.
     *
     * @param subjectType the subject type
     * @return this
     */
    public TestEmailTemplateBuilder subjectType(String subjectType) {
        this.subjectType = subjectType;
        return this;
    }

    /**
     * Sets the content type.
     *
     * @param contentType the content type
     * @return this
     */
    public TestEmailTemplateBuilder contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    /**
     * Sets the content.
     * <p/>
     * If the {@link #contentType} is unset, defaults it to TEXT.
     *
     * @param content the content
     * @return this
     */
    public TestEmailTemplateBuilder content(String content) {
        this.content = content;
        if (contentType != null) {
            contentType("TEXT");
        }
        return this;
    }

    /**
     * Attaches a document.
     * <p/>
     * If the {@link #contentType} is unset, defaults it to DOCUMENT.
     *
     * @param document the document
     * @return this
     */
    @Override
    public TestEmailTemplateBuilder document(Document document) {
        if (contentType == null) {
            contentType("DOCUMENT");
        }
        return super.document(document);
    }

    /**
     * Adds the template to the parent builder.
     *
     * @return the parent builder
     */
    public TestDocumentTemplateBuilder add() {
        return parent.emailTemplate(build(false));
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(org.openvpms.component.model.entity.Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (subject != null) {
            bean.setValue("subject", subject);
        }
        if (subjectType != null) {
            bean.setValue("subjectType", subjectType);
        }
        if (contentType != null) {
            bean.setValue("contentType", contentType);
        }
        if (content != null) {
            bean.setValue("content", content);
        }
    }
}
