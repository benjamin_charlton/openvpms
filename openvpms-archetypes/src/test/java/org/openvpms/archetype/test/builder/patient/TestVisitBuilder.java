/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.patientClinicalEvent</em> (aka visit), instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestVisitBuilder extends AbstractTestPatientActBuilder<Act, TestVisitBuilder> {

    /**
     * The items.
     */
    private List<Act> items = new ArrayList<>();

    /**
     * Constructs a {@link TestVisitBuilder}.
     *
     * @param service the archetype service
     */
    public TestVisitBuilder(ArchetypeService service) {
        super(PatientArchetypes.CLINICAL_EVENT, Act.class, service);
    }

    /**
     * Adds an item.
     *
     * @param item the item
     * @return this
     */
    public TestVisitBuilder addItem(Act item) {
        items.add(item);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        for (Act item : items) {
            bean.addTarget("items", item, "event");
        }
    }
}
