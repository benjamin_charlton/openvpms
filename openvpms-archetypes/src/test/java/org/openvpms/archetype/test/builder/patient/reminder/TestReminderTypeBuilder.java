/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.lookup.TestSpeciesBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>entity.reminderType</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderTypeBuilder extends AbstractTestEntityBuilder<Entity, TestReminderTypeBuilder> {

    /**
     * The default interval.
     */
    private ValueStrategy defaultInterval = ValueStrategy.defaultValue();

    /**
     * The default interval units.
     */
    private ValueStrategy defaultUnits = ValueStrategy.defaultValue();

    /**
     * The cancel interval.
     */
    private ValueStrategy cancelInterval = ValueStrategy.defaultValue();

    /**
     * The cancel interval units.
     */
    private ValueStrategy cancelUnits = ValueStrategy.defaultValue();

    /**
     * The reminder counts to add.
     */
    private List<Entity> reminderCounts = new ArrayList<>();

    /**
     * The species to add.
     */
    private String[] species;

    /**
     * The reminder groups to add.
     */
    private String[] groups;

    /**
     * Constructs a {@link TestReminderTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestReminderTypeBuilder(ArchetypeService service) {
        super(ReminderArchetypes.REMINDER_TYPE, Entity.class, service);
        name(ValueStrategy.random("zremindertype"));
    }

    /**
     * Sets the default interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestReminderTypeBuilder defaultInterval(int interval, DateUnits units) {
        defaultInterval = ValueStrategy.value(interval);
        defaultUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Sets the cancel interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestReminderTypeBuilder cancelInterval(int interval, DateUnits units) {
        cancelInterval = ValueStrategy.value(interval);
        cancelUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Returns a builder for a new reminder count.
     *
     * @return a reminder count builder
     */
    public TestReminderCountBuilder newCount() {
        return new TestReminderCountBuilder(this, getService());
    }

    /**
     * Adds a reminder count.
     *
     * @param count the reminder count
     */
    public TestReminderTypeBuilder addReminderCount(Entity count) {
        reminderCounts.add(count);
        return this;
    }

    /**
     * Adds species.
     *
     * @param species the species codes
     * @return this
     */
    public TestReminderTypeBuilder addSpecies(String... species) {
        this.species = species;
        return this;
    }

    /**
     * Adds reminder groups.
     *
     * @param groups the reminder groups
     * @return this
     */
    public TestReminderTypeBuilder addGroups(String... groups) {
        this.groups = groups;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        defaultInterval.setValue(bean, "defaultInterval");
        defaultUnits.setValue(bean, "defaultUnits");
        cancelInterval.setValue(bean, "cancelInterval");
        cancelUnits.setValue(bean, "cancelUnits");
        for (Entity count : reminderCounts) {
            bean.addTarget("counts", count);
            toSave.add(count);
        }
        reminderCounts.clear();
        if (species != null) {
            TestSpeciesBuilder builder = new TestSpeciesBuilder(getService());
            for (String code : species) {
                Lookup lookup = builder.code(code).build();
                object.addClassification(lookup);
            }
        }
        if (groups != null) {
            TestLookupBuilder builder = new TestLookupBuilder(ReminderArchetypes.GROUP, getService());
            for (String group : groups) {
                Lookup lookup = builder.code(group).build();
                object.addClassification(lookup);
            }
        }
    }
}