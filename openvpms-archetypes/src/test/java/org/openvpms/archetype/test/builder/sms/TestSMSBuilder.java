/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.sms;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.smsMessage</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSMSBuilder extends AbstractTestSMSBuilder<TestSMSBuilder> {

    /**
     * The replies.
     */
    private final List<Reply> replies = new ArrayList<>();

    /**
     * Constructs a {@link TestSMSBuilder}.
     *
     * @param service the archetype service
     */
    public TestSMSBuilder(ArchetypeService service) {
        super("act.smsMessage", service);
    }

    /**
     * Constructs a {@link TestSMSBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestSMSBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the recipient.
     *
     * @param recipient the recipient
     * @return this
     */
    public TestSMSBuilder recipient(Party recipient) {
        return contact(recipient);
    }

    /**
     * Sets the time when the SMS was updated.
     * <p/>
     * This uses the startTime node for indexing purposes.
     *
     * @param updated the updated time
     * @return this
     */
    public TestSMSBuilder updated(Date updated) {
        return startTime(updated);
    }

    /**
     * Sets the time when the SMS was sent.
     * <p/>
     * This uses the endTime node.
     *
     * @param sent the sent time
     * @return this
     */
    public TestSMSBuilder sent(Date sent) {
        return endTime(sent);
    }

    /**
     * Adds a reply.
     *
     * @param message  the message
     * @return this
     */
    public TestSMSBuilder addReply(String message) {
        return addReply(new Date(), message);
    }

    /**
     * Adds a reply.
     *
     * @param received the timestamp when the reply was received
     * @param message  the message
     * @return this
     */
    public TestSMSBuilder addReply(Date received, String message) {
        replies.add(new Reply(received, message));
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (!replies.isEmpty()) {
            TestSMSReplyBuilder builder = new TestSMSReplyBuilder(getService());
            for (Reply reply : replies) {
                Act act = builder.sender(getContact())
                        .location(getLocation())
                        .phone(getPhone())
                        .message(reply.message)
                        .received(reply.received)
                        .build();
                bean.addTarget("replies", act, "sms");
                toSave.add(act);
            }
            replies.clear();
        }
    }

    private static class Reply {
        private final Date received;

        private final String message;

        public Reply(Date received, String message) {
            this.received = received;
            this.message = message;
        }
    }
}