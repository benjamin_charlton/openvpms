/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builds <em>party.supplierVeterinaryPractice</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestVetPracticeBuilder extends AbstractTestPartyBuilder<Party, TestVetPracticeBuilder> {

    /**
     * The vets.
     */
    private Party[] vets;

    /**
     * Constructs a {@link TestVetPracticeBuilder}.
     *
     * @param service the archetype service
     */
    public TestVetPracticeBuilder(ArchetypeService service) {
        super(SupplierArchetypes.SUPPLIER_VET_PRACTICE, Party.class, service);
        name(ValueStrategy.random("zvetpractice"));
    }

    /**
     * Sets the vets in the practice.
     *
     * @param vets the vets
     * @return this
     */
    public TestVetPracticeBuilder vets(Party... vets) {
        this.vets = vets;
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (vets != null) {
            for (Party vet  : vets) {
                bean.addTarget("veterinarians", vet, "practices");
                toSave.add(vet);
            }
        }
    }
}