/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.lookup;

import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Factory for creating lookups.
 *
 * @author Tim Anderson
 */
public class TestLookupFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestLookupFactory}.
     *
     * @param service the service
     */
    public TestLookupFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates and saves a lookup.
     *
     * @param archetype the lookup archetype
     * @param code      the lookup code
     * @return the lookup
     */
    public Lookup getLookup(String archetype, String code) {
        return newLookup(archetype).code(code).build();
    }

    /**
     * Creates and saves a breed lookup.
     *
     * @param breed the breed code
     */
    public Lookup getBreed(String breed) {
        return newBreed().code(breed).build();
    }

    /**
     * Creates and saves a breed lookup.
     *
     * @param breed   the breed code
     * @param species the species code
     */
    public Lookup getBreed(String breed, String species) {
        return newBreed().code(breed).species(getSpecies(species)).build();
    }

    /**
     * Returns a builder for a breed.
     *
     * @return a breed builder
     */
    public TestBreedBuilder newBreed() {
        return new TestBreedBuilder(service);
    }

    /**
     * Creates and saves a country lookup.
     *
     * @param code the country code
     */
    public Lookup getCountry(String code) {
        return newCountry().code(code).build();
    }

    /**
     * Returns a builder for a country.
     *
     * @return a country builder
     */
    public TestCountryBuilder newCountry() {
        return new TestCountryBuilder(service);
    }

    /**
     * Creates and saves a currency lookup.
     *
     * @param code the currency code
     * @return the currency lookup
     */
    public Lookup getCurrency(String code) {
        return newCurrency().code(code).build();
    }

    /**
     * Returns a builder for a currency.
     *
     * @return a currency builder
     */
    public TestCurrencyBuilder newCurrency() {
        return new TestCurrencyBuilder(service);
    }

    /**
     * Creates and saves a species.
     *
     * @return the species lookup
     */
    public Lookup getSpecies(String code) {
        return newSpecies().code(code).build();
    }

    /**
     * Returns a builder for a species.
     *
     * @return a species builder
     */
    public TestSpeciesBuilder newSpecies() {
        return new TestSpeciesBuilder(service);
    }

    /**
     * Returns a builder for a macro.
     *
     * @return a macro builder
     */
    public TestMacroBuilder newMacro() {
        return new TestMacroBuilder(service);
    }

    /**
     * Creates and saves a new tax type.
     *
     * @return the tax type lookup
     */
    public Lookup createTaxType(BigDecimal rate) {
        return newTaxType().rate(rate).build();
    }

    /**
     * Returns a builder for a tax type.
     *
     * @return a tax type builder
     */
    public TestTaxTypeBuilder newTaxType() {
        return new TestTaxTypeBuilder(service);
    }

    /**
     * Creates a lookup.
     *
     * @param archetype the lookup archetype
     * @param code      the lookup code
     * @return the lookup
     */
    public Lookup createLookup(String archetype, String code) {
        return createLookup(archetype, code, false);
    }

    /**
     * Creates a lookup.
     *
     * @param archetype the lookup archetype
     * @param code      the lookup code
     * @param isDefault determines if the lookup is the default or not
     * @return the lookup
     */
    public Lookup createLookup(String archetype, String code, boolean isDefault) {
        return new TestLookupBuilder(archetype, service).code(code).isDefault(isDefault).build();
    }

    /**
     * Returns a builder for a new lookup.
     *
     * @param archetype the lookup archetype
     * @return a lookup builder
     */
    public TestLookupBuilder newLookup(String archetype) {
        return new TestLookupBuilder(archetype, service);
    }

}
