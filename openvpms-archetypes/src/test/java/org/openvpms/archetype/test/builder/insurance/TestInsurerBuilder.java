/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.insurance;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Test insurer builder.
 *
 * @author Tim Anderson
 */
public class TestInsurerBuilder extends AbstractTestPartyBuilder<Party, TestInsurerBuilder> {

    /**
     * The insurer id archetype.
     */
    private String insurerIdArchetype;

    /**
     * The insurer id.
     */
    private ValueStrategy insurerId;

    /**
     * Constructs a {@link TestInsurerBuilder}.
     *
     * @param service the archetype service
     */
    public TestInsurerBuilder(ArchetypeService service) {
        super(SupplierArchetypes.INSURER, Party.class, service);
        name(ValueStrategy.random("zinsurer"));
    }

    /**
     * Constructs a {@link TestInsurerBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestInsurerBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the insurer identifier.
     *
     * @param archetype the insurer identifier archetype
     * @param insurerId the insurer identifier
     * @return this
     */
    public TestInsurerBuilder insurerId(String archetype, String insurerId) {
        return insurerId(archetype, ValueStrategy.value(insurerId));
    }

    /**
     * Sets the insurer identifier.
     *
     * @param archetype the insurer identifier archetype
     * @param insurerId the insurer identifier
     * @return this
     */
    public TestInsurerBuilder insurerId(String archetype, ValueStrategy insurerId) {
        this.insurerIdArchetype = archetype;
        this.insurerId = insurerId;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (insurerIdArchetype != null && insurerId != null) {
            object.addIdentity(createEntityIdentity(insurerIdArchetype, insurerId));
        }
    }
}