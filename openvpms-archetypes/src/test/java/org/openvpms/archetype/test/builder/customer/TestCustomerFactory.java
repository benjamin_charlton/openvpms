/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer;

import org.openvpms.archetype.test.builder.party.TestEmailContactBuilder;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating test customers.
 *
 * @author Tim Anderson
 */
public class TestCustomerFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestCustomerFactory}.
     *
     * @param service the service
     */
    public TestCustomerFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates and saves a customer.
     *
     * @return the customer
     */
    public Party createCustomer() {
        return newCustomer().build();
    }

    /**
     * Creates and saves a customer.
     *
     * @param firstName the customer's first name
     * @param lastName  the customer's last name
     * @return the customer
     */
    public Party createCustomer(String firstName, String lastName) {
        return newCustomer().firstName(firstName).lastName(lastName).build();
    }

    /**
     * Creates and saves a customer.
     *
     * @param title     the customer title code
     * @param firstName the customer's first name
     * @param lastName  the customer's last name
     * @return the customer
     */
    public Party createCustomer(String title, String firstName, String lastName) {
        return newCustomer().title(title).firstName(firstName).lastName(lastName).build();
    }

    /**
     * Returns a builder for a new customer.
     *
     * @return a customer builder
     */
    public TestCustomerBuilder newCustomer() {
        return new TestCustomerBuilder(service);
    }

    /**
     * Returns a builder to update a customer.
     *
     * @param customer the customer
     * @return a customer builder
     */
    public TestCustomerBuilder updateCustomer(Party customer) {
        return new TestCustomerBuilder(customer, service);
    }

    /**
     * Creates an email contact.
     *
     * @param email    the email address
     * @param purposes the contact purposes
     * @return a new contact
     */
    public Contact createEmail(String email, String... purposes) {
        return createEmail(email, false, purposes);
    }

    /**
     * Creates an email contact.
     *
     * @param email     the email address
     * @param preferred if {@code true}, the contact is the preferred contact
     * @param purposes  the contact purposes
     * @return a new contact
     */
    public Contact createEmail(String email, boolean preferred, String... purposes) {
        return newEmail().email(email).preferred(preferred).purposes(purposes).build();
    }

    /**
     * Returns a builder for a new email contact.
     *
     * @return a new email contact builder
     */
    public TestEmailContactBuilder<?, ?> newEmail() {
        return new TestEmailContactBuilder<>(service);
    }
}
