/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.sql.Time;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>party.organisationSchedule</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestScheduleBuilder extends AbstractTestEntityBuilder<Entity, TestScheduleBuilder> {

    /**
     * The appointment types, with associated no. of slots and default flag.
     */
    private final Map<Entity, Pair<Integer, Boolean>> appointmentTypes = new HashMap<>();

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The schedule start time.
     */
    private LocalTime start;

    /**
     * The schedule end time.
     */
    private LocalTime end;

    /**
     * The slot size.
     */
    private ValueStrategy slotSize = ValueStrategy.defaultValue();

    /**
     * The slot units.
     */
    private ValueStrategy slotUnits = ValueStrategy.defaultValue();

    /**
     * Determines if online booking is enabled.
     */
    private ValueStrategy onlineBooking = ValueStrategy.defaultValue();

    /**
     * Determines if reminders should be sent.
     */
    private ValueStrategy sendReminders = ValueStrategy.defaultValue();

    /**
     * The cage type.
     */
    private Entity cageType;

    /**
     * Constructs an {@link TestScheduleBuilder}.
     *
     * @param service the archetype service
     */
    public TestScheduleBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.ORGANISATION_SCHEDULE, Entity.class, service);
        name(ValueStrategy.random("zschedule"));
    }

    /**
     * Constructs a {@link TestScheduleBuilder}.
     *
     * @param schedule the schedule to update
     * @param service  the archetype service
     */
    public TestScheduleBuilder(Entity schedule, ArchetypeService service) {
        super(schedule, service);
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public TestScheduleBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Sets the slot size.
     *
     * @param slotSize the slot size, in minutes
     * @return this
     */
    public TestScheduleBuilder slotSize(int slotSize) {
        return slotSize(slotSize, DateUnits.MINUTES);
    }

    /**
     * Sets the slot size.
     *
     * @param size  the slot size
     * @param units the slot units ({@link DateUnits#MINUTES} or {@link DateUnits#HOURS})
     * @return this
     */
    public TestScheduleBuilder slotSize(int size, DateUnits units) {
        this.slotSize = ValueStrategy.value(size);
        this.slotUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Sets the schedule start and end times.
     *
     * @param startHour the starting hour (0..23)
     * @param endHour   the ending hour (0..23). For midnight, set to 0.
     * @return this
     */
    public TestScheduleBuilder times(int startHour, int endHour) {
        start = LocalTime.of(startHour, 0);
        end = LocalTime.of(endHour, 0);
        return this;
    }

    /**
     * Sets the schedule start and end times.
     *
     * @param startTime the start time, as HH:MM
     * @param endTime   the ending time, as HH:MM.
     * @return this
     */
    public TestScheduleBuilder times(String startTime, String endTime) {
        start = (startTime != null) ? LocalTime.parse(startTime) : null;
        end = (endTime != null) ? LocalTime.parse(endTime) : null;
        return this;
    }

    /**
     * Sets the cage type.
     * <p/>
     * This is used when the schedule represents a physical cage/kennel.
     *
     * @param cageType the cage type
     * @return this
     */
    public TestScheduleBuilder cageType(Entity cageType) {
        this.cageType = cageType;
        return this;
    }

    /**
     * Sets the available appointment types.
     * <p/>
     * Each appointment type takes up one slot.
     *
     * @param appointmentTypes the appointment types
     * @return this
     */
    public TestScheduleBuilder appointmentTypes(Entity... appointmentTypes) {
        for (Entity appointmentType : appointmentTypes) {
            addAppointmentType(appointmentType, 1, false);
        }
        return this;
    }

    /**
     * Adds an appointment type.
     *
     * @param appointmentType the appointment type
     * @param noSlots         the no. of slots the appointment type takes up
     * @param isDefault       if {@code true}, indicates this is the default appointment type
     * @return this
     */
    public TestScheduleBuilder addAppointmentType(Entity appointmentType, int noSlots, boolean isDefault) {
        appointmentTypes.put(appointmentType, new ImmutablePair<>(noSlots, isDefault));
        return this;
    }

    /**
     * Make the schedule available for online booking.
     *
     * @return this
     */
    public TestScheduleBuilder onlineBooking() {
        return onlineBooking(true);
    }

    /**
     * Determines if the schedule is available for online booking.
     *
     * @param onlineBooking if {@code true}, the schedule is available for online booking
     * @return this
     */
    public TestScheduleBuilder onlineBooking(boolean onlineBooking) {
        this.onlineBooking = ValueStrategy.value(onlineBooking);
        return this;
    }

    /**
     * Determines if reminders should be sent.
     *
     * @param sendReminders if {@code true}, send appointment reminders
     * @return this
     */
    public TestScheduleBuilder sendReminders(boolean sendReminders) {
        this.sendReminders = ValueStrategy.value(sendReminders);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (location != null) {
            bean.setTarget("location", location);
        }
        slotSize.setValue(bean, "slotSize");
        slotUnits.setValue(bean, "slotUnits");
        if (start != null) {
            bean.setValue("startTime", Time.valueOf(start));
        }
        if (end != null) {
            bean.setValue("endTime", Time.valueOf(end));
        }
        onlineBooking.setValue(bean, "onlineBooking");
        sendReminders.setValue(bean, "sendReminders");

        if (cageType != null) {
            bean.setTarget("cageType", cageType);
        }

        for (Map.Entry<Entity, Pair<Integer, Boolean>> appointmentType : appointmentTypes.entrySet()) {
            Relationship relationship = bean.addTarget("appointmentTypes", appointmentType.getKey());
            IMObjectBean relBean = getBean(relationship);
            relBean.setValue("noSlots", appointmentType.getValue().getLeft());
            relBean.setValue("default", appointmentType.getValue().getRight());
        }
    }
}
