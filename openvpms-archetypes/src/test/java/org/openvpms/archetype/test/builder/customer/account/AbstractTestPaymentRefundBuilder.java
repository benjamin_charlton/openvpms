/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.customerAccountPayment</em> and <em>act.customerAccountRefund</em>instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestPaymentRefundBuilder<B extends AbstractTestPaymentRefundBuilder<B>>
        extends AbstractTestCustomerAccountActBuilder<B> {

    /**
     * The items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The built items. Available after building.
     */
    private List<FinancialAct> builtItems = new ArrayList<>();

    /**
     * The till.
     */
    private Entity till;

    /**
     * Constructs an {@link AbstractTestPaymentRefundBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    protected AbstractTestPaymentRefundBuilder(String archetype, ArchetypeService service) {
        super(archetype, service);
    }

    /**
     * Sets the till.
     *
     * @param till the till
     * @return this
     */
    public B till(Entity till) {
        this.till = till;
        return getThis();
    }

    /**
     * Adds items.
     *
     * @param items the item to add
     * @return this
     */
    public B add(FinancialAct ... items) {
        Collections.addAll(this.items, items);
        return getThis();
    }

    /**
     * Returns the built items.
     *
     * @return the built items
     */
    public List<FinancialAct> getItems() {
        return builtItems;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (till != null) {
            bean.setTarget("till", till);
        }
        BigDecimal amount = BigDecimal.ZERO;
        for (FinancialAct item : items) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            item.addActRelationship(relationship);
            toSave.add(item);
            IMObjectBean itemBean = bean.getBean(item);
            amount = amount.add(itemBean.getBigDecimal("amount", BigDecimal.ZERO));
        }
        bean.setValue("amount", amount);
        builtItems = new ArrayList<>(items);
        items.clear(); // can't reuse
    }
}
