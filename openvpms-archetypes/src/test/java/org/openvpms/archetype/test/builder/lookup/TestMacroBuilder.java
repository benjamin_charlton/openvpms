/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.lookup;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for macro lookups, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestMacroBuilder extends AbstractTestLookupBuilder<TestMacroBuilder> {

    /**
     * The macro expression.
     */
    private String expression;

    /**
     * Constructs a {@link TestMacroBuilder}.
     *
     * @param service the archetype service
     */
    public TestMacroBuilder(ArchetypeService service) {
        super("lookup.macro", service);
    }

    /**
     * Sets the expression.
     *
     * @param expression the expression
     * @return this
     */
    public TestMacroBuilder expression(String expression) {
        this.expression = expression;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Lookup object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (expression != null) {
            bean.setValue("expression", expression);
        }
    }
}
