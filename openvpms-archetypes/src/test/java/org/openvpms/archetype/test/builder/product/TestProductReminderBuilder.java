/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entityLink.productReminder</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestProductReminderBuilder<P extends AbstractTestProductBuilder<P>>
        extends AbstractTestIMObjectBuilder<EntityLink, TestProductReminderBuilder<P>> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The reminder type.
     */
    private Entity reminderType;

    /**
     * The interactive flag.
     */
    private ValueStrategy interactive = ValueStrategy.defaultValue();

    /**
     * The period.
     */
    private ValueStrategy period = ValueStrategy.defaultValue();

    /**
     * The period units.
     */
    private ValueStrategy units = ValueStrategy.defaultValue();

    /**
     * Constructs a {@link TestProductReminderBuilder}.
     *
     * @param service the archetype service
     */
    public TestProductReminderBuilder(P parent, ArchetypeService service) {
        super(ProductArchetypes.PRODUCT_REMINDER_RELATIONSHIP, EntityLink.class, service);
        this.parent = parent;
    }

    /**
     * Sets the reminder type.
     *
     * @param reminderType the reminder type
     * @return this
     */
    public TestProductReminderBuilder<P> reminderType(Entity reminderType) {
        this.reminderType = reminderType;
        return this;
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public EntityLink build() {
        return build(false);
    }

    /**
     * Adds the relationship to the parent product.
     *
     * @return the parent product builder
     */
    public P add() {
        parent.addProductReminder(build());
        return parent;
    }

    /**
     * Sets the interactive flag.
     *
     * @param interactive the interactive flag
     * @return this
     */
    public TestProductReminderBuilder<P> interactive(boolean interactive) {
        this.interactive = ValueStrategy.value(interactive);
        return this;
    }

    /**
     * Sets the period.
     *
     * @param period the period
     * @param units  the period units
     * @return this
     */
    public TestProductReminderBuilder<P> period(int period, DateUnits units) {
        this.period = ValueStrategy.value(period);
        this.units = ValueStrategy.value(units != null ? units.toString() : null);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(EntityLink object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (reminderType != null) {
            object.setTarget(reminderType.getObjectReference());
        }
        interactive.setValue(bean, "interactive");
        period.setValue(bean, "period");
        units.setValue(bean, "periodUom");
    }
}