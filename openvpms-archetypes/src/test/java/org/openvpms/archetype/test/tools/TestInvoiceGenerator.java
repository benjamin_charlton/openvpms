/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.tools;

import org.apache.commons.lang.RandomStringUtils;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.Date;

/**
 * Generates invoices.
 *
 * @author Tim Anderson
 */
public class TestInvoiceGenerator {

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String contextPath = "openvpms-archetypes-test-context.xml";
        ApplicationContext context;
        if (!new File(contextPath).exists()) {
            context = new ClassPathXmlApplicationContext(contextPath);
        } else {
            context = new FileSystemXmlApplicationContext(contextPath);
        }
        TestPracticeFactory practiceFactory = context.getBean(TestPracticeFactory.class);
        TestCustomerFactory customerFactory = context.getBean(TestCustomerFactory.class);
        TestPatientFactory patientFactory = context.getBean(TestPatientFactory.class);
        TestCustomerAccountFactory accountFactory = context.getBean(TestCustomerAccountFactory.class);
        TestProductFactory productFactory = context.getBean(TestProductFactory.class);
        Product product1 = productFactory.createService();
        Product product2 = productFactory.createService();
        Party location = practiceFactory.createLocation();
        Date today = new Date();
        for (int i = 0; i < 100; ++i) {
            Date startTime = DateRules.getDate(today, -i, DateUnits.DAYS);

            Party customer = customerFactory.newCustomer().addMobilePhone(RandomStringUtils.randomNumeric(10)).build();
            Party patient = patientFactory.createPatient(customer);
            accountFactory.newInvoice()
                    .customer(customer)
                    .item().patient(patient).product(product1).quantity(1).fixedPrice(10)
                    .add()
                    .item().patient(patient).product(product2).quantity(2).unitPrice(3)
                    .add()
                    .status(FinancialActStatus.POSTED)
                    .startTime(startTime)
                    .endTime(startTime)
                    .location(location)
                    .sendReminder(true)
                    .build();
        }
    }
}