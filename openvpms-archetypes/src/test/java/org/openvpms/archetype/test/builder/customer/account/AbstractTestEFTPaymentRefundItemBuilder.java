/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openvpms.archetype.test.builder.eft.AbstractTestEFTPOSTransactionBuilder;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.customerAccountPaymentEFT</em> and <em>act.customerAccountRefundEFT</em>instances, for testing
 * purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestEFTPaymentRefundItemBuilder<P extends AbstractTestPaymentRefundBuilder<P>,
        B extends AbstractTestPaymentRefundItemBuilder<P, B>>
        extends AbstractTestPaymentRefundItemBuilder<P, B> {

    /**
     * The cash-out amount.
     */
    private BigDecimal cashout;

    /**
     * The EFTPOS transactions to add.
     */
    private final List<FinancialAct> eft = new ArrayList<>();

    /**
     * The EFTPOS transactions to create. Each element is a transaction status and terminal
     */
    private final List<Pair<String, Entity>> eftToCreate = new ArrayList<>();

    /**
     * Constructs an {@link AbstractTestEFTPaymentRefundItemBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the archetype to create
     * @param service   the archetype service
     */
    protected AbstractTestEFTPaymentRefundItemBuilder(P parent, String archetype, ArchetypeService service) {
        super(parent, archetype, service);
    }

    /**
     * Sets the cash-out amount.
     *
     * @param cashout the cash-out amount
     * @return this
     */
    public B cashout(int cashout) {
        return cashout(BigDecimal.valueOf(cashout));
    }

    /**
     * Sets the cash-out amount.
     *
     * @param cashout the cash-out amount
     * @return this
     */
    public B cashout(BigDecimal cashout) {
        this.cashout = cashout;
        return getThis();
    }

    /**
     * Adds an EFT transaction with the specified status.
     *
     * @param status   the status
     * @param terminal the EFTPOS terminal
     */
    public B addTransaction(String status, Entity terminal) {
        eftToCreate.add(new ImmutablePair<>(status, terminal));
        return getThis();
    }

    /**
     * Adds an EFT transaction.
     *
     * @param transaction the transaction
     */
    public B addTransaction(FinancialAct transaction) {
        eft.add(transaction);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (cashout != null) {
            bean.setValue("cashout", cashout);
        }
        if (!eftToCreate.isEmpty()) {
            P parent = getParent();
            for (Pair<String, Entity> pair : eftToCreate) {
                AbstractTestEFTPOSTransactionBuilder<?> builder = createEFTPOSTransactionBuilder();
                Party customer = parent.getCustomer();
                if (customer == null) {
                    throw new IllegalStateException("Parent customer has not been set");
                }
                Party location = parent.getLocation();
                if (location == null) {
                    throw new IllegalStateException("Parent location has not been set");
                }
                FinancialAct act = builder.customer(customer)
                        .amount(object.getTotal())
                        .status(pair.getLeft())
                        .terminal(pair.getRight())
                        .location(location)
                        .build(false);
                eft.add(act);
            }
        }
        if (!eft.isEmpty()) {
            int sequence = 0;
            for (FinancialAct act : eft) {
                ActRelationship relationship = (ActRelationship) bean.addTarget("eft", act);
                act.addActRelationship(relationship);
                relationship.setSequence(sequence++);
                toSave.add(act);
            }
            eft.clear();
        }
    }

    /**
     * Returns a builder to create an EFTPOS transaction.
     *
     * @return a new builder
     */
    protected abstract AbstractTestEFTPOSTransactionBuilder<?> createEFTPOSTransactionBuilder();
}
