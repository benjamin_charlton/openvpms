/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>entity.investigationType</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestInvestigationTypeBuilder extends AbstractTestEntityBuilder<Entity, TestInvestigationTypeBuilder> {

    /**
     * The laboratory that provides this investigation type.
     */
    private Entity laboratory;

    /**
     * The devices that may perform the investigation type.
     */
    private Entity[] devices;

    /**
     * Account identifiers keyed on their locations.
     */
    private Map<Party, String> accountIds = new HashMap<>();

    /**
     * Constructs a {@link TestInvestigationTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestInvestigationTypeBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.INVESTIGATION_TYPE, Entity.class, service);
        name(ValueStrategy.random("zinvestigationtype"));
    }

    /**
     * Sets the laboratory that provides the investigation type.
     *
     * @param laboratory the laboratory
     * @return this
     */
    public TestInvestigationTypeBuilder laboratory(Entity laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Sets the devices that may perform this investigation type.
     *
     * @param devices the devices
     * @return the devices
     */
    public TestInvestigationTypeBuilder devices(Entity... devices) {
        this.devices = devices;
        return this;
    }

    /**
     * Adds an account identifier.
     *
     * @param accountId the account identifier
     * @param location  the practice location the account identifier applies to
     * @return this
     */
    public TestInvestigationTypeBuilder accountId(String accountId, Party location) {
        accountIds.put(location, accountId);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (laboratory != null) {
            bean.setTarget("laboratory", laboratory);
        }
        if (devices != null) {
            for (Entity device : devices) {
                bean.addTarget("devices", device);
            }
        }
        for (Map.Entry<Party, String> entry : accountIds.entrySet()) {
            Relationship relationship = bean.addTarget("locations", entry.getKey());
            IMObjectBean relationshipBean = getBean(relationship);
            relationshipBean.setValue("accountId", entry.getValue());
        }
    }
}
