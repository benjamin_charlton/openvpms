/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for <em>entityLink.productIncludes</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestTemplateIncludeBuilder extends AbstractTestIMObjectBuilder<EntityLink, TestTemplateIncludeBuilder> {

    /**
     * The parent builder.
     */
    private final TestTemplateProductBuilder parent;

    /**
     * The product to include.
     */
    private Product product;

    /**
     * The low quantity.
     */
    private BigDecimal lowQuantity;

    /**
     * The high quantity.
     */
    private BigDecimal highQuantity;

    /**
     * The minimum weight.
     */
    private BigDecimal minWeight;

    /**
     * The maximum weight.
     */
    private BigDecimal maxWeight;

    /**
     * Determines if the line item should be printed.
     */
    private Boolean print;

    /**
     * Determines if the product has a zero price.
     */
    private Boolean zeroPrice;

    /**
     * Determines if the product should be skipped if it is missing.
     */
    private Boolean skipIfMissing;

    /**
     * Constructs a {@link TestTemplateIncludeBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestTemplateIncludeBuilder(TestTemplateProductBuilder parent, ArchetypeService service) {
        super(ProductArchetypes.PRODUCT_INCLUDES, EntityLink.class, service);
        this.parent = parent;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public TestTemplateIncludeBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Sets the quantity.
     * <p/>
     * This makes the low and high quantities the same.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestTemplateIncludeBuilder quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     * <p/>
     * This makes the low and high quantities the same.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestTemplateIncludeBuilder quantity(BigDecimal quantity) {
        return lowQuantity(quantity).highQuantity(quantity);
    }

    /**
     * Sets the low quantity.
     *
     * @param lowQuantity the low quantity
     * @return this
     */
    public TestTemplateIncludeBuilder lowQuantity(int lowQuantity) {
        return lowQuantity(BigDecimal.valueOf(lowQuantity));
    }

    /**
     * Sets the low quantity.
     *
     * @param lowQuantity the low quantity
     * @return this
     */
    public TestTemplateIncludeBuilder lowQuantity(BigDecimal lowQuantity) {
        this.lowQuantity = lowQuantity;
        return this;
    }

    /**
     * Sets the high quantity.
     *
     * @param highQuantity the high quantity
     * @return this
     */
    public TestTemplateIncludeBuilder highQuantity(int highQuantity) {
        return highQuantity(BigDecimal.valueOf(highQuantity));
    }

    /**
     * Sets the high quantity.
     *
     * @param highQuantity the high quantity
     * @return this
     */
    public TestTemplateIncludeBuilder highQuantity(BigDecimal highQuantity) {
        this.highQuantity = highQuantity;
        return this;
    }

    /**
     * Sets the minimum weight.
     *
     * @param minWeight the minimum weight, inclusive
     * @return this
     */
    public TestTemplateIncludeBuilder minWeight(int minWeight) {
        return minWeight(BigDecimal.valueOf(minWeight));
    }

    /**
     * Sets the minimum weight.
     *
     * @param minWeight the minimum weight, inclusive
     * @return this
     */
    public TestTemplateIncludeBuilder minWeight(BigDecimal minWeight) {
        this.minWeight = minWeight;
        return this;
    }

    /**
     * Sets the maximum weight.
     *
     * @param maxWeight the maximum weight, exclusive
     * @return this
     */
    public TestTemplateIncludeBuilder maxWeight(int maxWeight) {
        return maxWeight(BigDecimal.valueOf(maxWeight));
    }

    /**
     * Sets the maximum weight.
     *
     * @param maxWeight the maximum weight, exclusive
     * @return this
     */
    public TestTemplateIncludeBuilder maxWeight(BigDecimal maxWeight) {
        this.maxWeight = maxWeight;
        return this;
    }

    /**
     * Determines if the line item is printed when the product is charged.
     *
     * @param print if {@code true}, print the line item, else suppress it
     * @return this
     */
    public TestTemplateIncludeBuilder print(boolean print) {
        this.print = print;
        return this;
    }

    /**
     * Zeroes the price when the product is charged.
     *
     * @return this
     */
    public TestTemplateIncludeBuilder zeroPrice() {
        return zeroPrice(true);
    }

    /**
     * Sets the zero price indicator.
     * <p/>
     * This determines if the product has a price when charged via the template.
     *
     * @param zeroPrice if {@code true}, the product has a zero price, otherwise it attracts the normal price
     * @return this
     */
    public TestTemplateIncludeBuilder zeroPrice(boolean zeroPrice) {
        this.zeroPrice = zeroPrice;
        return this;
    }

    /**
     * Sets the skip-if-missing indicator.
     * <p/>
     * This determines if the product is skipped if it is not available at the practice location where the
     * template is used, or if it prevents the template from expanding.
     *
     * @param skipIfMissing if {@code true} skip the product if it is missing, otherwise prevent template expansion
     * @return this
     */
    public TestTemplateIncludeBuilder skipIfMissing(boolean skipIfMissing) {
        this.skipIfMissing = skipIfMissing;
        return this;
    }

    /**
     * Builds the include and adds it to the parent builder.
     *
     * @return the parent builder
     */
    public TestTemplateProductBuilder add() {
        EntityLink include = build(false);
        parent.include(include);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(EntityLink object, org.openvpms.component.model.bean.IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (product != null) {
            object.setTarget(product.getObjectReference());
        }
        if (lowQuantity != null) {
            bean.setValue("lowQuantity", lowQuantity);
        }
        if (highQuantity != null) {
            bean.setValue("highQuantity", highQuantity);
        }
        if (minWeight != null) {
            bean.setValue("minWeight", minWeight);
        }
        if (maxWeight != null) {
            bean.setValue("maxWeight", maxWeight);
        }
        if (print != null) {
            bean.setValue("print", print);
        }
        if (zeroPrice != null) {
            bean.setValue("zeroPrice", zeroPrice);
        }
        if (skipIfMissing != null) {
            bean.setValue("skipIfMissing", skipIfMissing);
        }
    }
}
