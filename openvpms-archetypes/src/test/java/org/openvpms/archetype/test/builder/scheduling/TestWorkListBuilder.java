/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>party.organisationWorkList</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestWorkListBuilder extends AbstractTestEntityBuilder<Entity, TestWorkListBuilder> {

    /**
     * The task types, with associated no. of slots and default flag.
     */
    private final Map<Entity, Pair<Integer, Boolean>> taskTypes = new HashMap<>();

    /**
     * The max no. of slots.
     */
    private ValueStrategy maxSlots = ValueStrategy.defaultValue();

    /**
     * Constructs an {@link TestWorkListBuilder}.
     *
     * @param service the archetype service
     */
    public TestWorkListBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.ORGANISATION_WORKLIST, Entity.class, service);
        name(ValueStrategy.random("zworklist"));
    }

    /**
     * Constructs a {@link TestWorkListBuilder}.
     *
     * @param workList the work list to update
     * @param service  the archetype service
     */
    public TestWorkListBuilder(Entity workList, ArchetypeService service) {
        super(workList, service);
    }

    /**
     * Sets the maximum no. of slots.
     *
     * @param maxSlots the maximum no. of slots
     * @return this
     */
    public TestWorkListBuilder maxSlots(int maxSlots) {
        this.maxSlots = ValueStrategy.value(maxSlots);
        return this;
    }

    /**
     * Sets the available task types.
     * <p/>
     * Each task type takes up one slot.
     *
     * @param taskTypes the task types
     * @return this
     */
    public TestWorkListBuilder taskTypes(Entity... taskTypes) {
        for (Entity taskType : taskTypes) {
            addTaskType(taskType, 1, false);
        }
        return this;
    }

    /**
     * Adds a task type.
     *
     * @param taskType  the task type
     * @param noSlots   the no. of slots the task type takes up
     * @param isDefault if {@code true}, indicates this is the default task type
     * @return this
     */
    public TestWorkListBuilder addTaskType(Entity taskType, int noSlots, boolean isDefault) {
        taskTypes.put(taskType, new ImmutablePair<>(noSlots, isDefault));
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        maxSlots.setValue(bean, "maxSlots");
        for (Map.Entry<Entity, Pair<Integer, Boolean>> taskType : taskTypes.entrySet()) {
            Relationship relationship = bean.addTarget("taskTypes", taskType.getKey());
            IMObjectBean relBean = getBean(relationship);
            relBean.setValue("noSlots", taskType.getValue().getLeft());
            relBean.setValue("default", taskType.getValue().getRight());
        }
    }
}