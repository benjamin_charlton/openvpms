/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.SmartContextLoader;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.support.GenericGroovyXmlContextLoader;
import org.springframework.test.context.support.GenericXmlContextLoader;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertNotNull;


/**
 * Abstract base class for tests using the archetype service.
 * <p/>
 * Note that {@link GenericXmlContextLoader} is specified in the {@link ContextConfiguration} to suppress
 * use of {@link GenericGroovyXmlContextLoader} when groovy it is on the classpath by {@link SmartContextLoader}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration(value = "/applicationContext.xml", loader = GenericXmlContextLoader.class)
public abstract class ArchetypeServiceTest extends AbstractJUnit4SpringContextTests {

    /**
     * The archetype service bean name.
     */
    private final String archetypeServiceBeanName;

    /**
     * The archetype service.
     */
    private IArchetypeService service;

    /**
     * The lookup service.
     */
    @Autowired
    private ILookupService lookups;

    /**
     * Constructs an {@link ArchetypeServiceTest}.
     */
    public ArchetypeServiceTest() {
        this("archetypeRuleService");
    }

    /**
     * Constructs an {@link ArchetypeServiceTest}.
     *
     * @param archetypeServiceBeanName the name of the archetype service bean
     */
    public ArchetypeServiceTest(String archetypeServiceBeanName) {
        this.archetypeServiceBeanName = archetypeServiceBeanName;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getArchetypeService() {
        if (service == null) {
            service = applicationContext.getBean(archetypeServiceBeanName, IArchetypeService.class);
        }
        return service;
    }

    /**
     * Returns the lookup service.
     *
     * @return the lookup service
     */
    protected ILookupService getLookupService() {
        return lookups;
    }

    /**
     * Helper to create a new object.
     *
     * @param archetype the archetype
     * @return the new object
     */
    protected IMObject create(String archetype) {
        IMObject object = getArchetypeService().create(archetype);
        assertNotNull(object);
        return object;
    }

    /**
     * Helper to create a new object.
     *
     * @param archetype the archetype
     * @param type      the expected type
     * @return the new object
     */
    protected <T extends org.openvpms.component.model.object.IMObject> T create(String archetype, Class<T> type) {
        return getArchetypeService().create(archetype, type);
    }

    /**
     * Helper to save an object.
     *
     * @param object the object to save
     * @throws ArchetypeServiceException if the service cannot save the object
     * @throws ValidationException       if the object cannot be validated
     */
    protected void save(org.openvpms.component.model.object.IMObject object) {
        getArchetypeService().save(object);
    }

    /**
     * Helper to save a collection of objects.
     *
     * @param objects the object to save
     * @throws ArchetypeServiceException if the service cannot save the objects
     * @throws ValidationException       if the object cannot be validated
     */
    @SafeVarargs
    protected final <T extends org.openvpms.component.model.object.IMObject> void save(T... objects) {
        save(Arrays.asList(objects));
    }

    /**
     * Helper to save a collection of objects.
     *
     * @param objects the object to save
     * @throws ArchetypeServiceException if the service cannot save the objects
     * @throws ValidationException       if the object cannot be validated
     */
    protected <T extends org.openvpms.component.model.object.IMObject> void save(Collection<T> objects) {
        getArchetypeService().save(objects);
    }

    /**
     * Helper to reload an object from the archetype service.
     *
     * @param object the object to reload
     * @return the corresponding object or {@code null} if no object is found
     */
    @SuppressWarnings("unchecked")
    protected <T extends org.openvpms.component.model.object.IMObject> T get(T object) {
        return (T) get(object.getObjectReference());
    }

    /**
     * Helper to retrieve an object from the archetype service.
     *
     * @param ref the object reference. May be {@code null}
     * @return the corresponding object or {@code null} if no object is found
     */
    protected IMObject get(Reference ref) {
        return ref != null ? getArchetypeService().get(ref) : null;
    }

    /**
     * Helper to retrieve an object from the archetype service.
     *
     * @param ref the object reference. May be {@code null}
     * @return the corresponding object or {@code null} if no object is found
     */
    protected <T extends org.openvpms.component.model.object.IMObject> T get(Reference ref, Class<T> type) {
        return ref != null ? getArchetypeService().get(ref, type) : null;
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(org.openvpms.component.model.object.IMObject object) {
        return getArchetypeService().getBean(object);
    }

    /**
     * Helper to reload an object.
     *
     * @param bean the bean
     * @return the reloaded object, or {@code null} if none is found
     */
    protected IMObjectBean get(IMObjectBean bean) {
        org.openvpms.component.model.object.IMObject object = get(bean.getObject());
        return (object != null) ? getBean(object) : null;
    }

    /**
     * Helper to remove an object.
     *
     * @param object the object to remove
     * @throws ArchetypeServiceException if the service cannot remove the object
     */
    protected void remove(org.openvpms.component.model.object.IMObject object) {
        getArchetypeService().remove(object);
    }

    /**
     * Verifies two {@code BigDecimal} instances are equal.
     *
     * @param expected the expected value. May be {@code null}
     * @param actual   the actual value. May be {@code null}
     */
    protected void checkEquals(BigDecimal expected, BigDecimal actual) {
        TestHelper.checkEquals(expected, actual);
    }

    /**
     * Verifies two {@code BigDecimal} instances are equal.
     *
     * @param expected the expected value
     * @param actual   the actual value. May be {@code null}
     */
    protected void checkEquals(String expected, BigDecimal actual) {
        checkEquals(new BigDecimal(expected), actual);
    }

    /**
     * Verifies two {@code BigDecimal} instances are equal.
     *
     * @param expected the expected value
     * @param actual   the actual value. May be {@code null}
     */
    protected void checkEquals(int expected, BigDecimal actual) {
        checkEquals(BigDecimal.valueOf(expected), actual);
    }

    /**
     * Returns the display name for an object.
     *
     * @param object the object
     * @return the display name for the object
     */
    protected String getDisplayName(org.openvpms.component.model.object.IMObject object) {
        return DescriptorHelper.getDisplayName(object, service);
    }

}
