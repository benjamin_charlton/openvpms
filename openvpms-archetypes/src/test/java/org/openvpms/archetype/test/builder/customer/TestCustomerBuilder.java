/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>party.customerperson</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCustomerBuilder extends AbstractTestPartyBuilder<Party, TestCustomerBuilder> {

    /**
     * The customer title code.
     */
    private String titleCode;

    /**
     * The first name.
     */
    private ValueStrategy firstName = ValueStrategy.value("J");

    /**
     * The last name.
     */
    private ValueStrategy lastName = ValueStrategy.random("Smith");

    /**
     * The company name.
     */
    private String companyName;

    /**
     * The customer discounts.
     */
    private Entity[] discounts;

    /**
     * The <em>lookup.taxType</em> codes that the customer is exempt from.
     */
    private String[] taxExemptionCodes;

    /**
     * Constructs a {@link TestCustomerBuilder}.
     *
     * @param service the archetype service
     */
    public TestCustomerBuilder(ArchetypeService service) {
        super(CustomerArchetypes.PERSON, Party.class, service);
    }

    /**
     * Constructs a {@link TestCustomerBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestCustomerBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the title.
     *
     * @param titleCode the <em>lookup.title</em> code
     * @return this
     */
    public TestCustomerBuilder title(String titleCode) {
        this.titleCode = titleCode;
        return this;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name
     * @return this
     */
    public TestCustomerBuilder firstName(String firstName) {
        return firstName(ValueStrategy.value(firstName));
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name
     * @return this
     */
    public TestCustomerBuilder firstName(ValueStrategy firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name
     * @return this
     */
    public TestCustomerBuilder lastName(String lastName) {
        return lastName(ValueStrategy.value(lastName));
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name
     */
    public TestCustomerBuilder lastName(ValueStrategy lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Sets the company name.
     *
     * @param companyName the company name
     */
    public TestCustomerBuilder companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    /**
     * Adds a home phone.
     *
     * @param phone the phone number
     * @return this
     */
    public TestCustomerBuilder addHomePhone(String phone) {
        addPhone(phone, ContactArchetypes.HOME_PURPOSE);
        return this;
    }

    /**
     * Adds a work phone.
     *
     * @param phone the phone number
     * @return this
     */
    public TestCustomerBuilder addWorkPhone(String phone) {
        addPhone(phone, ContactArchetypes.WORK_PURPOSE);
        return this;
    }

    /**
     * Sets the customer discounts.
     *
     * @param discounts the discounts
     */
    public TestCustomerBuilder discounts(Entity... discounts) {
        this.discounts = discounts;
        return this;
    }

    /**
     * Adds tax exemptions.
     *
     * @param taxCodes the <em>lookup.taxType</em> codes
     * @return this
     */
    public TestCustomerBuilder addTaxExemptions(String... taxCodes) {
        this.taxExemptionCodes = taxCodes;
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (titleCode != null) {
            TestLookupBuilder builder = new TestLookupBuilder(UserArchetypes.TITLE, getService());
            bean.setValue("title", builder.code(titleCode).build().getCode());
        }
        firstName.setValue(bean, "firstName");
        lastName.setValue(bean, "lastName");
        bean.setValue("companyName", companyName);

        if (discounts != null) {
            for (Entity discount : discounts) {
                bean.addTarget("discounts", discount);
            }
        }
        if (taxExemptionCodes != null) {
            for (String code :taxExemptionCodes) {
                Lookup taxType = new TestLookupBuilder("lookup.taxType", getService()).code(code).build();
                object.addClassification(taxType);
            }
        }
    }
}
