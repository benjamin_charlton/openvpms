/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestCurrencyBuilder;
import org.openvpms.archetype.test.builder.lookup.TestTaxTypeBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.archetype.test.builder.user.TestUserBuilder;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.organisationPractice</em> instances, for testing purposes.
 * <p/>
 * Note that the <em>party.organisationPractice</em> is a singleton, so any saved instance will be returned.
 *
 * @author Tim Anderson
 */
public class TestPracticeBuilder extends AbstractTestPartyBuilder<Party, TestPracticeBuilder> {

    /**
     * Determines if an existing practice is being updated.
     */
    private final boolean update;

    /**
     * The tax rates.
     */
    private final List<Lookup> taxes = new ArrayList<>();

    /**
     * The currency.
     */
    private String currencyCode = "AUD";

    /**
     * The currency lookup.
     */
    private Lookup currencyLookup;

    /**
     * The service user.
     */
    private User serviceUser;

    /**
     * The practice locations.
     */
    private Party[] locations;

    /**
     * The default weight units.
     */
    private ValueStrategy defaultWeightUnits = ValueStrategy.defaultValue();

    /**
     * Determines if charges/estimates have minimum quantities.
     */
    private ValueStrategy minimumQuantities = ValueStrategy.defaultValue();

    /**
     * Determines the user classification code that can override minimum quantities.
     */
    private ValueStrategy minimumQuantitiesOverride = ValueStrategy.defaultValue();


    /**
     * Constructs a {@link TestPracticeBuilder}.
     *
     * @param service the archetype service
     */
    public TestPracticeBuilder(ArchetypeService service) {
        super(PracticeArchetypes.PRACTICE, Party.class, service);
        name("zpractice");
        update = false;
    }

    /**
     * Constructs a {@link TestPracticeBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestPracticeBuilder(Party object, ArchetypeService service) {
        super(object, service);
        update = true;
    }

    /**
     * Sets the practice locations.
     *
     * @param locations the locations
     * @return this
     */
    public TestPracticeBuilder locations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Sets the currency.
     * <p/>
     * If the currency lookup exists, it will be reset.
     *
     * @param currency the currency code
     * @return this
     */
    public TestPracticeBuilder currency(String currency) {
        this.currencyCode = currency;
        return this;
    }

    /**
     * Sets the currency.
     *
     * @param currency the currency lookup
     * @return this
     */
    public TestPracticeBuilder currency(Lookup currency) {
        currencyLookup = currency;
        return currency((currency != null) ? currency.getCode() : null);
    }

    /**
     * Adds a tax type.
     *
     * @param rate the tax rate
     * @return this
     */
    public TestPracticeBuilder addTaxType(BigDecimal rate) {
        return addTaxType(new TestTaxTypeBuilder(getService()).rate(rate).build(false));
    }

    /**
     * Adds a tax type.
     *
     * @param taxType the tax type
     * @return this
     */
    public TestPracticeBuilder addTaxType(Lookup taxType) {
        taxes.add(taxType);
        return this;
    }

    /**
     * Sets the service user.
     * <p/>
     * This creates a random user.
     *
     * @return this
     */
    public TestPracticeBuilder serviceUser() {
        return serviceUser(new TestUserBuilder(getService()).build());
    }

    /**
     * Sets the service user.
     *
     * @param serviceUser the service user
     * @return this
     */
    public TestPracticeBuilder serviceUser(User serviceUser) {
        this.serviceUser = serviceUser;
        return this;
    }

    /**
     * Sets the default weight units.
     *
     * @param units the units
     * @return this
     */
    public TestPracticeBuilder defaultWeightUnits(WeightUnits units) {
        defaultWeightUnits = ValueStrategy.value(units != null ? units.toString() : null);
        return this;
    }

    /**
     * Determines if minimum quantities are enabled for charge and estimate items.
     *
     * @param minimumQuantities if {@code true}, enable minimum quantities for charge and estimate items else disable
     *                          them
     * @return this
     */
    public TestPracticeBuilder minimumQuantities(boolean minimumQuantities) {
        this.minimumQuantities = ValueStrategy.value(minimumQuantities);
        return this;
    }

    /**
     * Sets the user classification code for users that can override minimum quantities, when minimum quantities
     * are enabled.
     *
     * @param minimumQuantitiesOverride the \classification code of users that can override minimum quantities
     * @return this
     */
    public TestPracticeBuilder minimumQuantitiesOverride(String minimumQuantitiesOverride) {
        this.minimumQuantitiesOverride = ValueStrategy.value(minimumQuantitiesOverride);
        return this;
    }

    /**
     * Returns the object to build.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Party getObject(String archetype) {
        Party practice;
        if (update || !PracticeArchetypes.PRACTICE.equals(archetype)) {
            practice = super.getObject(archetype);
        } else {
            practice = getExisingPractice();
            if (practice != null) {
                resetValues(practice);
            } else {
                practice = super.getObject(archetype);
            }
        }
        return practice;
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location, "practice");
                toSave.add(location);
            }
            locations = null; // can't reuse
        }
        if (currencyLookup == null) {
            currencyLookup = new TestCurrencyBuilder(getService()).code(currencyCode).build();
        }
        bean.setValue("currency", currencyLookup.getCode());

        if (serviceUser != null) {
            bean.setTarget("serviceUser", serviceUser);
        }
        for (Lookup tax : taxes) {
            object.addClassification(tax);
        }
        taxes.clear();

        defaultWeightUnits.setValue(bean, "defaultWeightUnits");

        minimumQuantities.setValue(bean, "minimumQuantities");
        minimumQuantitiesOverride.setValue(bean, "minimumQuantitiesOverride");
    }

    /**
     * Resets a practice to its defaults.
     *
     * @param practice the practice
     */
    private void resetValues(Party practice) {
        IMObjectBean bean = getBean(practice);
        // remove contacts
        for (Contact contact : bean.getValues("contacts", Contact.class)) {
            practice.removeContact(contact);
        }

        // remove locations
        for (Party location : bean.getTargets("locations", Party.class)) {
            bean.removeTarget("locations", location);
        }

        // remove taxes
        for (Lookup tax : bean.getValues("taxes", Lookup.class)) {
            bean.removeValue("taxes", tax);
        }

        bean.setValue("useLocationProducts", false);
        bean.setValue("useLoggedInClinician", true);
        bean.setValue("defaultWeightUnits", null);
        bean.removeValues("serviceUser");
        bean.setValue("minimumQuantities", false);
        bean.setValue("minimumQuantitiesOverride", null);
    }

    /**
     * Returns the existing active practice.
     *
     * @return the existing active practice, or {@code null} if none exists
     */
    private Party getExisingPractice() {
        ArchetypeService service = getService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        Root<Party> root = query.from(Party.class, PracticeArchetypes.PRACTICE);
        query.where(builder.equal(root.get("active"), true));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }
}
