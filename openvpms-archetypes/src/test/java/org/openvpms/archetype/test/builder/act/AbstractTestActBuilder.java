/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.act;

import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * A builder of {@link Act} instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestActBuilder<T extends Act, B extends AbstractTestActBuilder<T, B>>
        extends AbstractTestIMObjectBuilder<T, B> {

    /**
     * Act identities.
     */
    private final List<ActIdentity> identities = new ArrayList<>();

    /**
     * The start time.
     */
    private ValueStrategy startTime = ValueStrategy.defaultValue();

    /**
     * The end time.
     */
    private ValueStrategy endTime = ValueStrategy.defaultValue();

    /**
     * The status.
     */
    private ValueStrategy status = ValueStrategy.defaultValue();

    /**
     * The secondary status.
     */
    private ValueStrategy status2 = ValueStrategy.defaultValue();

    /**
     * Constructs an {@link AbstractTestActBuilder}.
     *
     * @param type    the type
     * @param service the archetype service
     */
    public AbstractTestActBuilder(Class<T> type, ArchetypeService service) {
        super(type, service);
    }

    /**
     * Constructs an {@link AbstractTestActBuilder}.
     *
     * @param archetype the archetype to build
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestActBuilder(String archetype, Class<T> type, ArchetypeService service) {
        super(archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestActBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestActBuilder(T object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the start time.
     *
     * @param startTime the start time
     * @return this
     */
    public B startTime(String startTime) {
        return startTime(parseDate(startTime));
    }

    /**
     * Sets the start time.
     *
     * @param startTime the start time
     * @return this
     */
    public B startTime(Date startTime) {
        return startTime(ValueStrategy.value(startTime));
    }

    /**
     * Sets the start time.
     *
     * @param startTime the start time
     * @return this
     */
    public B startTime(ValueStrategy startTime) {
        this.startTime = startTime;
        return getThis();
    }

    /**
     * Sets the end time.
     *
     * @param endTime the end time
     * @return this
     */
    public B endTime(String endTime) {
        return endTime(parseDate(endTime));
    }

    /**
     * Sets the end time.
     *
     * @param endTime the end time
     * @return this
     */
    public B endTime(Date endTime) {
        return endTime(ValueStrategy.value(endTime));
    }

    /**
     * Sets the end time.
     *
     * @param endTime the end time
     * @return this
     */
    public B endTime(ValueStrategy endTime) {
        this.endTime = endTime;
        return getThis();
    }

    /**
     * Sets the status.
     *
     * @param status the status
     * @return this
     */
    public B status(String status) {
        this.status = ValueStrategy.value(status);
        return getThis();
    }

    /**
     * Sets the secondary status.
     *
     * @param status2 the secondary status
     * @return this
     */
    public B status2(String status2) {
        this.status2 = ValueStrategy.value(status2);
        return getThis();
    }

    /**
     * Adds an identity.
     *
     * @param archetype the archetype
     * @param identity  the identity
     * @return this
     */
    public B addIdentity(String archetype, String identity) {
        return addIdentities(createActIdentity(archetype, ValueStrategy.value(identity)));
    }

    /**
     * Adds identities.
     *
     * @param identities the identities
     * @return this
     */
    public B addIdentities(ActIdentity... identities) {
        this.identities.addAll(Arrays.asList(identities));
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        startTime.setValue(bean, "startTime");
        endTime.setValue(bean, "endTime");
        status.setValue(bean, "status");
        status2.setValue(bean, "status2");

        if (!identities.isEmpty()) {
            for (ActIdentity identity : identities) {
                object.addIdentity(identity);
            }
            identities.clear();  // don't re-use
        }
    }

    /**
     * Parses a date or date time.
     *
     * @param value the value. May be {@code null}
     * @return the corresponding date. May be {@code null}
     */
    protected Date parseDate(String value) {
        if (value != null) {
            return value.contains(":") ? TestHelper.getDatetime(value) : TestHelper.getDate(value);
        }
        return null;
    }
}
