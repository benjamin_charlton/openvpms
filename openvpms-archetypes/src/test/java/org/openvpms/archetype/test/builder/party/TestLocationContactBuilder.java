/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>contact.location</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLocationContactBuilder<T extends Party, P extends AbstractTestPartyBuilder<T, P>>
        extends AbstractTestContactBuilder<T, P, TestLocationContactBuilder<T, P>> {

    /**
     * The street address.
     */
    private String address;

    /**
     * The suburb code.
     */
    private String suburbCode;

    /**
     * The suburb name.
     */
    private String suburbName;

    /**
     * The state code.
     */
    private String stateCode;

    /**
     * The state name.
     */
    private String stateName;

    /**
     * The post code.
     */
    private String postCode;

    /**
     * Constructs a {@link TestLocationContactBuilder}.
     *
     * @param service the archetype service
     */
    public TestLocationContactBuilder(ArchetypeService service) {
        super(ContactArchetypes.LOCATION, service);
    }

    /**
     * Constructs a {@link TestLocationContactBuilder}.
     *
     * @param parent    the parent builder
     * @param service   the archetype service
     */
    public TestLocationContactBuilder(P parent, ArchetypeService service) {
        super(parent, ContactArchetypes.LOCATION, service);
    }

    /**
     * Sets the street address.
     *
     * @param address the street address
     * @return this
     */
    public TestLocationContactBuilder<T, P> address(String address) {
        this.address = address;
        return this;
    }

    /**
     * Sets the suburb code.
     *
     * @param suburbCode the suburb code
     * @return this
     */
    public TestLocationContactBuilder<T, P> suburbCode(String suburbCode) {
        this.suburbCode = suburbCode;
        return this;
    }

    /**
     * Sets the suburb name.
     *
     * @param suburbName the suburb name
     * @return this
     */
    public TestLocationContactBuilder<T, P> suburbName(String suburbName) {
        this.suburbName = suburbName;
        return this;
    }

    /**
     * Sets the state code.
     *
     * @param stateCode the state code
     * @return this
     */
    public TestLocationContactBuilder<T, P> stateCode(String stateCode) {
        this.stateCode = stateCode;
        return this;
    }

    /**
     * Sets the state name.
     *
     * @param stateName the state name
     * @return this
     */
    public TestLocationContactBuilder<T, P> stateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    /**
     * Sets the postcode.
     *
     * @param postCode the postcode
     * @return this
     */
    public TestLocationContactBuilder<T, P> postCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Contact object, IMObjectBean bean, Set<IMObject> toSave) {
        Lookup state = null;
        if (stateCode != null) {
            state = new TestLookupBuilder(ContactArchetypes.STATE, getService())
                    .code(stateCode)
                    .name(stateName).build();
        }
        if (suburbCode != null) {
            TestLookupBuilder builder = new TestLookupBuilder(ContactArchetypes.SUBURB, getService())
                    .code(suburbCode)
                    .name(suburbName);
            if (state != null) {
                builder.source(state);
            }
            builder.build();
        }
        if (address != null) {
            bean.setValue("address", address);
        }
        if (suburbCode != null) {
            bean.setValue("suburb", suburbCode);
        }
        if (stateCode != null) {
            bean.setValue("state", stateCode);
        }
        if (postCode != null) {
            bean.setValue("postcode", postCode);
        }
        super.build(object, bean, toSave);
    }
}
