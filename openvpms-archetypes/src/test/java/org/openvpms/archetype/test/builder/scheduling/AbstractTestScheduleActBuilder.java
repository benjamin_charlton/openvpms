/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * .
 *
 * @author Tim Anderson
 */
public class AbstractTestScheduleActBuilder<B extends AbstractTestScheduleActBuilder<B>>
        extends AbstractTestActBuilder<Act, B> {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The notes.
     */
    private ValueStrategy notes = ValueStrategy.defaultValue();

    /**
     * Constructs an {@link AbstractTestScheduleActBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestScheduleActBuilder(String archetype, ArchetypeService service) {
        super(archetype, Act.class, service);
    }

    /**
     * Constructs an {@link AbstractTestScheduleActBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestScheduleActBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    public B customer(Party customer) {
        this.customer = customer;
        return getThis();
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public B patient(Party patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public B clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Sets the notes.
     *
     * @param notes the notes
     * @return this
     */
    public B notes(String notes) {
        this.notes = ValueStrategy.value(notes);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        notes.setValue(bean, "notes");
    }
}
