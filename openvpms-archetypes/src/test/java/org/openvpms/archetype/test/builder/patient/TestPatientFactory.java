/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Factory for creating patients and patient records.
 *
 * @author Tim Anderson
 */
public class TestPatientFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules laboratoryRules;

    /**
     * Constructs a {@link TestPatientFactory}.
     *
     * @param service         the archetype service
     * @param laboratoryRules the laboratory rules
     */
    public TestPatientFactory(ArchetypeService service, LaboratoryRules laboratoryRules) {
        this.service = service;
        this.laboratoryRules = laboratoryRules;
    }

    /**
     * Creates a new patient alert.
     *
     * @return a patient alert
     */
    public Act createAlert(Party patient, Entity alertType) {
        return newAlert().patient(patient).alertType(alertType).build();
    }

    /**
     * Returns a builder for a new patient alert.
     *
     * @return an alert builder
     */
    public TestPatientAlertBuilder newAlert() {
        return new TestPatientAlertBuilder(service);
    }

    /**
     * Creates a patient alert type.
     *
     * @return a new patient alert type
     */
    public Entity createAlertType() {
        return newAlertType().build();
    }

    /**
     * Returns a builder for a new patient alert type.
     *
     * @return an alert builder
     */
    public TestPatientAlertTypeBuilder newAlertType() {
        return new TestPatientAlertTypeBuilder(service);
    }

    /**
     * Creates and saves a new patient.
     *
     * @return the patient
     */
    public Party createPatient() {
        return newPatient().build();
    }

    /**
     * Creates and saves a new patient linked to a customer.
     *
     * @param owner the customer
     * @return the patient
     */
    public Party createPatient(Party owner) {
        return newPatient().owner(owner).build();
    }

    /**
     * Returns a builder for a new patient.
     *
     * @return a patient builder
     */
    public TestPatientBuilder newPatient() {
        return new TestPatientBuilder(service);
    }

    /**
     * Returns a builder to update a patient.
     *
     * @return a patient builder
     */
    public TestPatientBuilder updatePatient(Party patient) {
        return new TestPatientBuilder(patient, service);
    }

    /**
     * Returns a builder for a new attachment.
     *
     * @return an attachment builder
     */
    public TestAttachmentBuilder newAttachment() {
        return new TestAttachmentBuilder(service);
    }

    /**
     * Returns a builder for a new investigation.
     *
     * @return an investigation builder
     */
    public TestInvestigationBuilder newInvestigation() {
        return new TestInvestigationBuilder(service, laboratoryRules);
    }

    /**
     * Returns a builder for a new visit.
     *
     * @return a visit builder
     */
    public TestVisitBuilder newVisit() {
        return new TestVisitBuilder(service);
    }

    /**
     * Creates and saves a new patient weight.
     *
     * @param patient the patient
     * @param weight  the weight, in kilograms
     * @return the weight act
     */
    public Act createWeight(Party patient, BigDecimal weight) {
        return new TestWeightBuilder(service).patient(patient).weight(weight).build();
    }

    /**
     * Returns a builder for a new weight.
     *
     * @return a weight builder
     */
    public TestWeightBuilder newWeight() {
        return new TestWeightBuilder(service);
    }

}
