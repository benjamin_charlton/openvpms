/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestPartyBuilder<T extends Party, B extends AbstractTestPartyBuilder<T, B>>
        extends AbstractTestEntityBuilder<T, B> {

    /**
     * The contacts.
     */
    private final List<Contact> contacts = new ArrayList<>();

    /**
     * Constructs an {@link AbstractTestPartyBuilder}.
     *
     * @param archetype the archetype
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestPartyBuilder(String archetype, Class<T> type, ArchetypeService service) {
        super(archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestPartyBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestPartyBuilder(T object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds a contact.
     *
     * @param contact the contact to add
     * @return this
     */
    public B addContact(Contact contact) {
        contacts.add(contact);
        return getThis();
    }

    /**
     * Adds contacts.
     *
     * @param contacts the contacts to add
     * @return this
     */
    public B addContacts(Contact... contacts) {
        for (Contact contact : contacts) {
            addContact(contact);
        }
        return getThis();
    }

    /**
     * Adds a phone contact.
     *
     * @param phone    the phone number
     * @param purposes the purposes
     * @return this
     */
    public B addPhone(String phone, String... purposes) {
        return newPhone().phone(phone).purposes(purposes).add();
    }

    /**
     * Adds a mobile phone contact.
     * <p/>
     * This sets <em>sms</em> to {@code true}.
     *
     * @param phone the phone number
     * @return this
     */
    public B addMobilePhone(String phone) {
        return newPhone().phone(phone).purposes(ContactArchetypes.MOBILE_PURPOSE).sms().add();
    }

    /**
     * Returns a builder for a phone contact.
     *
     * @return a new builder
     */
    public TestPhoneContactBuilder<T, B> newPhone() {
        return new TestPhoneContactBuilder<>(getThis(), getService());
    }

    /**
     * Adds an email contact.
     *
     * @param email the email address
     * @return this
     */
    public B addEmail(String email, String... purposes) {
        return newEmail().email(email).purposes(purposes).add();
    }

    /**
     * Returns a builder for an email contact.
     *
     * @return a new builder
     */
    public TestEmailContactBuilder<T, B> newEmail() {
        return new TestEmailContactBuilder<>(getThis(), getService());
    }

    /**
     * Adds an address.
     *
     * @param address    the street address
     * @param suburbCode the suburb code
     * @param stateCode  the state code
     * @param postCode   the post code
     * @param purposes   the contact purposes
     * @return this
     */
    public B addAddress(String address, String suburbCode, String stateCode, String postCode, String... purposes) {
        return newLocation().address(address).suburbCode(suburbCode).stateCode(stateCode).postCode(postCode)
                .purposes(purposes)
                .add();
    }

    /**
     * Returns a builder for a location contact.
     *
     * @return a new builder
     */
    public TestLocationContactBuilder<T, B> newLocation() {
        return new TestLocationContactBuilder<>(getThis(), getService());
    }

    /**
     * Adds a website contact.
     *
     * @param url the website url
     * @return this
     */
    public B addWebsite(String url) {
        TestWebsiteContactBuilder builder = new TestWebsiteContactBuilder(getService());
        Contact contact = builder.url(url).build();
        contacts.add(contact);
        return getThis();
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        for (Contact contact : contacts) {
            object.addContact(contact);
        }
        contacts.clear(); // can't be reused
    }
}
