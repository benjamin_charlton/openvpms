/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Builds <em>act.customerAccountPayment</em> instances, for testing purposes
 *
 * @author Tim Anderson
 */
public class TestPaymentBuilder extends AbstractTestPaymentRefundBuilder<TestPaymentBuilder> {

    /**
     * Constructs a {@link TestPaymentBuilder}.
     *
     * @param service the archetype service
     */
    public TestPaymentBuilder(ArchetypeService service) {
        super(CustomerAccountArchetypes.PAYMENT, service);
    }

    /**
     * Returns a builder to add a cash item.
     *
     * @return a cash item builder
     */
    public TestCashPaymentItemBuilder cash() {
        return new TestCashPaymentItemBuilder(this, getService());
    }

    /**
     * Adds a cash item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder cash(int amount) {
        return cash().amount(amount).add();
    }

    /**
     * Adds a cash item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder cash(BigDecimal amount) {
        return cash().amount(amount).add();
    }

    /**
     * Returns a builder to add an EFT item.
     *
     * @return an EFT item builder
     */
    public TestEFTPaymentItemBuilder eft() {
        return new TestEFTPaymentItemBuilder(this, getService());
    }

}
