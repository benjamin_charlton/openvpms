/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.builder.entity.TestEntityIdentityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.patientpet</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPatientBuilder extends AbstractTestPartyBuilder<Party, TestPatientBuilder> {

    /**
     * Male patient.
     */
    public static final String SEX_MALE = PatientRules.SEX_MALE;

    /**
     * Female patient.
     */
    public static final String SEX_FEMALE = PatientRules.SEX_FEMALE;

    /**
     * Unspecified sex patient.
     */
    public static final String SEX_UNSPECIFIED = PatientRules.SEX_UNSPECIFIED;

    /**
     * The microchips.
     */
    private final List<String> microchips = new ArrayList<>();

    /**
     * The patient species.
     */
    private String species = "CANINE";

    /**
     * The patient breed.
     */
    private String breed;

    /**
     * The patient date of birth.
     */
    private Date dateOfBirth;

    /**
     * The patient sex.
     */
    private String sex;

    /**
     * Determines if the patient has been desexed.
     */
    private Boolean desexed;

    /**
     * Determines if the patient is deceased.
     */
    private Boolean deceased;

    /**
     * The patient date of death.
     */
    private Date dateOfDeath;

    /**
     * The patient owner.
     */
    private Party owner;

    /**
     * The patient colour.
     */
    private String colour;

    /**
     * The referrals.
     */
    private List<Referral> referrals = new ArrayList<>();

    /**
     * Constructs a {@link TestPatientBuilder}.
     *
     * @param service the archetype service
     */
    public TestPatientBuilder(ArchetypeService service) {
        super(PatientArchetypes.PATIENT, Party.class, service);
        name(ValueStrategy.random("Spot-"));
    }

    /**
     * Constructs a {@link TestPatientBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestPatientBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the patient species.
     * <p/>
     * This is the code for an <em>lookup.species</em>.
     * <p/>
     * If no species is specified, "CANINE" will be used.
     * <p/>
     * If the species does not exist, it will be created.
     *
     * @param species the patient species
     * @return this
     */
    public TestPatientBuilder species(String species) {
        this.species = species;
        return this;
    }

    /**
     * Sets the patient breed.
     * <p/>
     * This is the code for an <em>lookup.breed</em>.
     *
     * @param breed the patient breed
     * @return this
     */
    public TestPatientBuilder breed(String breed) {
        this.breed = breed;
        return this;
    }

    /**
     * Sets the patient sex to {@link #SEX_MALE}.
     *
     * @return this
     */
    public TestPatientBuilder male() {
        return sex(SEX_MALE);
    }

    /**
     * Sets the patient sex to {@link #SEX_FEMALE}.
     *
     * @return this
     */
    public TestPatientBuilder female() {
        return sex(SEX_FEMALE);
    }

    /**
     * Sets the patient sex to {@link #SEX_UNSPECIFIED}.
     *
     * @return this
     */
    public TestPatientBuilder unspecifiedSex() {
        return sex(SEX_UNSPECIFIED);
    }

    /**
     * Sets the patient sex.
     *
     * @param sex the patient sex
     * @return this
     */
    public TestPatientBuilder sex(String sex) {
        this.sex = sex;
        return this;
    }

    /**
     * Determines if the patient has been desexed or not.
     *
     * @param desexed if {@code true}, the patient has been desexed
     * @return this
     */
    public TestPatientBuilder desexed(boolean desexed) {
        this.desexed = desexed;
        return this;
    }

    /**
     * Sets the patient date-of-birth.
     *
     * @param dateOfBirth the date of birth
     * @return this
     */
    public TestPatientBuilder dateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    /**
     * Determines if the patient is deceased.
     *
     * @param deceased if {@code true}, the patient is deceased
     * @return this
     */
    public TestPatientBuilder deceased(boolean deceased) {
        this.deceased = deceased;
        return this;
    }

    /**
     * Sets the patient date of death.
     *
     * @param dateOfDeath the date of death
     * @return this
     */
    public TestPatientBuilder dateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
        return this;
    }

    /**
     * Sets the patient colour.
     *
     * @param colour the patient colour
     * @return this
     */
    public TestPatientBuilder colour(String colour) {
        this.colour = colour;
        return this;
    }

    /**
     * Sets the patient owner.
     *
     * @param owner the owner
     * @return this
     */
    public TestPatientBuilder owner(Party owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Adds a microchip.
     *
     * @param microchip the microchip
     * @return this
     */
    public TestPatientBuilder addMicrochip(String microchip) {
        microchips.add(microchip);
        return this;
    }

    /**
     * Adds a referred-from vet relationship.
     *
     * @param vet the vet
     * @return this
     */
    public TestPatientBuilder addReferredFrom(Party vet) {
        referrals.add(new Referral(vet, true));
        return this;
    }

    /**
     * Adds a referred-to vet relationship.
     *
     * @param vet the vet
     * @return this
     */
    public TestPatientBuilder addReferredTo(Party vet) {
        referrals.add(new Referral(vet, false));
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        ArchetypeService service = getService();
        TestLookupBuilder speciesBuilder = new TestLookupBuilder(PatientArchetypes.SPECIES, service);
        Lookup speciesLookup = speciesBuilder.code(species).build();
        bean.setValue("species", speciesLookup.getCode());

        if (breed != null) {
            TestLookupBuilder breedBuilder = new TestLookupBuilder(PatientArchetypes.BREED, service);
            Lookup breedLookup = breedBuilder.code(breed).source(speciesLookup).build();
            bean.setValue("breed", breedLookup.getCode());
        }
        if (sex != null) {
            bean.setValue("sex", sex);
        }
        if (desexed != null) {
            bean.setValue("desexed", desexed);
        }
        if (dateOfBirth != null) {
            bean.setValue("dateOfBirth", dateOfBirth);
        }
        if (deceased != null) {
            bean.setValue("deceased", deceased);
        }
        if (dateOfDeath != null) {
            bean.setValue("deceasedDate", dateOfDeath);
        }
        if (colour != null) {
            bean.setValue("colour", colour);
        }
        if (owner != null) {
            IMObjectBean ownerBean = service.getBean(owner);
            ownerBean.addTarget("patients", PatientArchetypes.PATIENT_OWNER, object, "customers");
            toSave.add(owner);
        }

        for (String microchip : microchips) {
            EntityIdentity identity = new TestEntityIdentityBuilder(PatientArchetypes.MICROCHIP, getService())
                    .identity(microchip).build();
            object.addIdentity(identity);
        }

        for (Referral referral : referrals) {
            String archetype = referral.from ? PatientArchetypes.REFERRED_FROM : PatientArchetypes.REFERRED_TO;
            Party vet = referral.vet;
            EntityRelationship relationship = (EntityRelationship) bean.addTarget("referrals", archetype, vet);
            vet.addEntityRelationship(relationship);
            toSave.add(vet);
        }
    }

    private static class Referral {

        private final Party vet;

        private final boolean from;

        public Referral(Party vet, boolean from) {
            this.vet = vet;
            this.from = from;
        }
    }
}
