/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>productPrice.fixedPrice</em> archetypes, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestFixedPriceBuilder<P extends AbstractTestProductBuilder<P>>
        extends AbstractTestPriceBuilder<P, TestFixedPriceBuilder<P>> {

    /**
     * The default price flag.
     */
    private ValueStrategy defaultPrice = ValueStrategy.defaultValue();

    /**
     * Constructs a {@link TestFixedPriceBuilder}.
     *
     * @param service the archetype service
     */
    public TestFixedPriceBuilder(ArchetypeService service) {
        this(null, service);
    }

    /**
     * Constructs a {@link TestFixedPriceBuilder}.
     *
     * @param parent    the parent builder
     * @param service   the archetype service
     */
    public TestFixedPriceBuilder(P parent, ArchetypeService service) {
        super(parent, ProductArchetypes.FIXED_PRICE, service);
    }

    /**
     * Sets the default price flag.
     *
     * @param defaultPrice the default price. If {@code true}, this is the default fixed price for the product
     * @return this
     */
    public TestFixedPriceBuilder<P> defaultPrice(boolean defaultPrice) {
        this.defaultPrice = ValueStrategy.value(defaultPrice);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(ProductPrice object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        defaultPrice.setValue(bean, "defaultPrice");
    }
}