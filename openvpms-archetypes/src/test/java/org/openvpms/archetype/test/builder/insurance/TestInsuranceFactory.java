/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.insurance;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;


/**
 * Factory for creating test insurance objects.
 *
 * @author Tim Anderson
 */
public class TestInsuranceFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestInsuranceFactory}.
     *
     * @param service the service
     */
    public TestInsuranceFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates and saves an insurer.
     *
     * @return the insurer
     */
    public Party createInsurer() {
        return newInsurer().build();
    }

    /**
     * Creates and saves an insurer.
     *
     * @param name the insurer's name
     * @return the insurer
     */
    public Party createInsurer(String name) {
        return newInsurer().name(name).build();
    }

    /**
     * Returns a builder for a new insurer.
     *
     * @return an insurer builder
     */
    public TestInsurerBuilder newInsurer() {
        return new TestInsurerBuilder(service);
    }

    /**
     * Returns a builder to update an insurer.
     *
     * @return an insurer builder
     */
    public TestInsurerBuilder updateInsurer(Party insurer) {
        return new TestInsurerBuilder(insurer, service);
    }

    /**
     * Creates a new policy.
     *
     * @return a new policy
     */
    public Act createPolicy(Party customer, Party patient, Party insurer, String policyNumber) {
        return newPolicy()
                .customer(customer)
                .patient(patient)
                .insurer(insurer)
                .policyNumber(policyNumber)
                .build();
    }

    /**
     * Returns a builder for a new policy.
     *
     * @return a policy builder
     */
    public TestPolicyBuilder newPolicy() {
        return new TestPolicyBuilder(service);
    }

    /**
     * Returns a builder for a new claim.
     *
     * @return a new claim builder
     */
    public TestClaimBuilder newClaim() {
        return new TestClaimBuilder(service);
    }

}
