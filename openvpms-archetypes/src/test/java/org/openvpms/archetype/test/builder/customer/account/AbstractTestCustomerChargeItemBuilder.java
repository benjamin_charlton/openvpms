/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import static java.math.BigDecimal.ZERO;

/**
 * Builds <em>act.customerAccount*Item</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestCustomerChargeItemBuilder<P extends AbstractTestCustomerChargeBuilder<P, B>,
        B extends AbstractTestCustomerChargeItemBuilder<P, B>>
        extends AbstractTestIMObjectBuilder<FinancialAct, B> {

    /**
     * The act start time.
     */
    private Date startTime;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The product.
     */
    private Product product;

    /**
     * The template the product was sourced from.
     */
    private Product template;

    /**
     * The template expansion group.
     */
    private Integer group;

    /**
     * The quantity.
     */
    private BigDecimal quantity;

    /**
     * The fixed price.
     */
    private BigDecimal fixedPrice;

    /**
     * The unit price.
     */
    private BigDecimal unitPrice;

    /**
     * The discount.
     */
    private BigDecimal discount;

    /**
     * The tax amount.
     */
    private BigDecimal tax;

    /**
     * The parent builder.
     */
    private P parent;

    /**
     * Constructs a {@link AbstractTestCustomerChargeItemBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestCustomerChargeItemBuilder(P parent, String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Sets the start time.
     *
     * @param startTime the start time
     * @return this
     */
    public B startTime(Date startTime) {
        this.startTime = startTime;
        return getThis();
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public B patient(Party patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public B clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Sets a medication product.
     *
     * @return this
     */
    public B medicationProduct() {
        return product(new TestProductFactory(getService()).createMedication());
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public B product(Product product) {
        this.product = product;
        return getThis();
    }

    /**
     * Sets the template.
     *
     * @param template the template
     * @return this
     */
    public B template(Product template) {
        this.template = template;
        return getThis();
    }

    /**
     * Sets the template expansion group.
     *
     * @param group the template expansion group
     * @return this
     */
    public B group(int group) {
        this.group = group;
        return getThis();
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return getThis();
    }

    /**
     * Sets the fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public B fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public B fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
        return getThis();
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return getThis();
    }

    /**
     * Sets the discount.
     *
     * @param discount the discount
     * @return this
     */
    public B discount(int discount) {
        return discount(BigDecimal.valueOf(discount));
    }

    /**
     * Sets the discount.
     *
     * @param discount the discount
     * @return this
     */
    public B discount(BigDecimal discount) {
        this.discount = discount;
        return getThis();
    }

    /**
     * Sets the tax amount.
     *
     * @param tax the tax amount
     * @return this
     */
    public B tax(int tax) {
        return tax(BigDecimal.valueOf(tax));
    }

    /**
     * Sets the tax amount.
     *
     * @param tax the tax amount
     * @return this
     */
    public B tax(BigDecimal tax) {
        this.tax = tax;
        return getThis();
    }

    /**
     * Adds the item to the parent charge.
     *
     * @return the parent charge builder
     */
    public P add() {
        FinancialAct item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (startTime != null) {
            bean.setValue("startTime", startTime);
        }
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        if (product != null) {
            bean.setTarget("product", product);
        }
        if (template != null) {
            bean.setTarget("template", template);
            Participation participation = bean.getObject("template", Participation.class);
            IMObjectBean participationBean = bean.getBean(participation);
            participationBean.setValue("group", group);
        }
        if (quantity != null) {
            bean.setValue("quantity", quantity);
        }
        if (fixedPrice != null) {
            bean.setValue("fixedPrice", fixedPrice);
        }
        if (unitPrice != null) {
            bean.setValue("unitPrice", unitPrice);
        }
        if (discount != null) {
            bean.setValue("discount", discount);
        }
        if (tax != null) {
            bean.setValue("tax", tax);
        }
        BigDecimal total = MathRules.calculateTotal(bean.getBigDecimal("fixedPrice", ZERO),
                                                    bean.getBigDecimal("unitPrice", ZERO),
                                                    bean.getBigDecimal("quantity", ZERO),
                                                    bean.getBigDecimal("discount", ZERO), 2);
        bean.setValue("total", total);
    }
}
