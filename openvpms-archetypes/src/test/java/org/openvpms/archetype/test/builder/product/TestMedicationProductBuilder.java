/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>product.medication</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestMedicationProductBuilder extends AbstractTestProductBuilder<TestMedicationProductBuilder> {

    /**
     * The doses to add.
     */
    private final List<Entity> doses = new ArrayList<>();

    /**
     * The dispensing label instructions.
     */
    private ValueStrategy labelInstructions = ValueStrategy.defaultValue();

    /**
     * The concentration, used for doses.
     */
    private ValueStrategy concentration = ValueStrategy.defaultValue();

    /**
     * Constructs a {@link TestMedicationProductBuilder}.
     *
     * @param service the archetype service
     */
    public TestMedicationProductBuilder(ArchetypeService service) {
        super(ProductArchetypes.MEDICATION, service);
    }

    /**
     * Constructs a {@link TestMedicationProductBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestMedicationProductBuilder(Product object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the dispensing label instructions.
     *
     * @param instructions the dispensing label instructions
     * @return this
     */
    public TestMedicationProductBuilder labelInstructions(String instructions) {
        this.labelInstructions = ValueStrategy.value(instructions);
        return this;
    }

    /**
     * Sets the concentration.
     *
     * @param concentration the concentration
     * @return this
     */
    public TestMedicationProductBuilder concentration(int concentration) {
        return concentration(BigDecimal.valueOf(concentration));
    }

    /**
     * Sets the concentration.
     *
     * @param concentration the concentration
     * @return this
     */
    public TestMedicationProductBuilder concentration(BigDecimal concentration) {
        this.concentration = ValueStrategy.value(concentration);
        return this;
    }

    /**
     * Adds a dose.
     *
     * @param dose the dose
     * @return this
     */
    public TestMedicationProductBuilder dose(Entity dose) {
        doses.add(dose);
        return this;
    }

    /**
     * Returns a dose builder.
     *
     * @return a new dose builder
     */
    public TestDoseBuilder dose() {
        return new TestDoseBuilder(this, getService());
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Product object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        labelInstructions.setValue(bean, "dispInstructions");
        concentration.setValue(bean, "concentration");
        for (Entity dose : doses) {
            bean.addTarget("doses", dose);
            toSave.add(dose);
        }
        doses.clear(); // can't be reused
    }
}
