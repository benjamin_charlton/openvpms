/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * A builder of <em>act.customerAccount*</em> acts.
 *
 * @author Tim Anderson
 */
public class AbstractTestCustomerAccountActBuilder<B extends AbstractTestCustomerAccountActBuilder<B>>
        extends AbstractTestActBuilder<FinancialAct, B> {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * Constructs a {@link AbstractTestCustomerAccountActBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestCustomerAccountActBuilder(String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    public B customer(Party customer) {
        this.customer = customer;
        return getThis();
    }

    /**
     * Returns the customer.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        return customer;
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public B location(Party location) {
        this.location = location;
        return getThis();
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location. May be {@code null}
     */
    public Party getLocation() {
        return location;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
    }
}
