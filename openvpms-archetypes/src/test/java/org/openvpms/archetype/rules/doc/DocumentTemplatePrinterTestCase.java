/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.archetype.rules.doc;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.springframework.beans.factory.annotation.Autowired;

import javax.print.attribute.standard.MediaTray;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link DocumentTemplatePrinter} class.
 *
 * @author Tim Anderson
 */
public class DocumentTemplatePrinterTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests the default values of a new document template printer.
     */
    @Test
    public void testDefaults() {
        EntityRelationship relationship = create(DocumentArchetypes.DOCUMENT_TEMPLATE_PRINTER,
                                                 EntityRelationship.class);
        DocumentTemplatePrinter printer = new DocumentTemplatePrinter(relationship, getArchetypeService());

        assertNull(printer.getPrinter());
        assertNull(printer.getPaperTray());
        assertTrue(printer.getInteractive());
        assertNull(printer.getMediaTray());
        assertNull(printer.getTemplate());
        assertNull(printer.getTemplateRef());
        assertNull(printer.getLocation());
        assertNull(printer.getLocationRef());
    }

    /**
     * Tests the {@link DocumentTemplatePrinter} setters and getters.
     */
    @Test
    public void testAccessors() {
        Party location = TestHelper.createLocation();
        TestDocumentTemplateBuilder builder = documentFactory.newTemplate();
        Entity template = builder.printer()
                .location(location)
                .printer(":printer")
                .paperTray("TOP")
                .interactive(false)
                .add()
                .build();
        IMObjectBean bean = getBean(template);
        EntityRelationship relationship = bean.getObject("printers", EntityRelationship.class);

        DocumentTemplatePrinter printer = new DocumentTemplatePrinter(relationship, getArchetypeService());

        // reload the relationship
        printer = new DocumentTemplatePrinter(get(printer.getRelationship()), getArchetypeService());

        assertEquals(new PrinterReference(null, "printer"), printer.getPrinter());
        assertEquals("TOP", printer.getPaperTray());
        assertFalse(printer.getInteractive());
        assertEquals(MediaTray.TOP, printer.getMediaTray());
        assertEquals(template, printer.getTemplate());
        assertEquals(template.getObjectReference(), printer.getTemplateRef());
        assertEquals(location, printer.getLocation());
        assertEquals(location.getObjectReference(), printer.getLocationRef());
    }

}