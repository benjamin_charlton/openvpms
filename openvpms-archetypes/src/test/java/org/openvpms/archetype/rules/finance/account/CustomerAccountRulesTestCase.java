/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.account;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.rules.finance.estimate.EstimateTestHelper;
import org.openvpms.archetype.rules.finance.invoice.ChargeItemEventLinker;
import org.openvpms.archetype.rules.finance.till.TillBalanceRules;
import org.openvpms.archetype.rules.finance.till.TillBalanceStatus;
import org.openvpms.archetype.rules.insurance.ClaimStatus;
import org.openvpms.archetype.rules.insurance.InsuranceTestHelper;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.patient.reminder.ReminderTestHelper;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.eft.TestEFTPOSPaymentBuilder;
import org.openvpms.archetype.test.builder.eft.TestEFTPOSRefundBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.IMObjectRelationship;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.business.domain.im.product.Product;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.archetype.helper.ActBean;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.ruleengine.RuleEngineException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ValidationError;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.act.ActStatus.COMPLETED;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.archetype.rules.act.FinancialActStatus.ON_HOLD;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CLOSING_BALANCE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER_ITEM;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT_ADJUST;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT_ITEM;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.DEBIT_ADJUST;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE_ITEM;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.OPENING_BALANCE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_CASH;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_CHEQUE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_DISCOUNT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_EFT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_OTHER;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_CASH;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_CHEQUE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_DISCOUNT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_EFT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_OTHER;
import static org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper.createInvestigationType;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link CustomerAccountRules} class when triggered by the
 * <em>archetypeService.save.act.customerAccountBadDebt.before</em>,
 * <em>archetypeService.save.act.customerAccountBadDebt.after</em>,
 * <em>archetypeService.save.act.customerAccountChargesCounter.before</em>,
 * <em>archetypeService.save.act.customerAccountChargesCounter.after</em>,
 * <em>archetypeService.save.act.customerAccountChargesCredit.before</em>,
 * <em>archetypeService.save.act.customerAccountChargesCredit.after</em>,
 * <em>archetypeService.save.act.customerAccountChargesInvoice.before</em>,
 * <em>archetypeService.save.act.customerAccountChargesInvoice.after</em>,
 * <em>archetypeService.save.act.customerAccountCreditAdjust.before</em>,
 * <em>archetypeService.save.act.customerAccountCreditAdjust.after</em>,
 * <em>archetypeService.save.act.customerAccountDebitAdjust.before</em>,
 * <em>archetypeService.save.act.customerAccountDebitAdjust.after</em>,
 * <em>archetypeService.save.act.customerAccountInitialBalance.before</em>,
 * <em>archetypeService.save.act.customerAccountInitialBalance.after</em>,
 * <em>archetypeService.save.act.customerAccountPayment.before</em>,
 * <em>archetypeService.save.act.customerAccountPayment.after</em>,
 * <em>archetypeService.save.act.customerAccountRefund.before</em> and.
 * <em>archetypeService.save.act.customerAccountRefund.after</em> rules.
 * In order for these tests to be successful, the archetype service
 * must be configured to trigger the above rules.
 *
 * @author Tim Anderson
 */
public class CustomerAccountRulesTestCase extends AbstractCustomerAccountTest {

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The account act factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The till balance rules.
     */
    private TillBalanceRules tillBalanceRules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        tillBalanceRules = new TillBalanceRules(getArchetypeService());
    }

    /**
     * Tests the {@link CustomerAccountRules#calculateTotal(BigDecimal, BigDecimal, BigDecimal, BigDecimal)} method.
     */
    @Test
    public void testCalculateTotal() {
        CustomerAccountRules rules = getRules();
        BigDecimal fixedPrice = BigDecimal.valueOf(10);
        BigDecimal discount = BigDecimal.valueOf(5);
        BigDecimal unitPrice1 = new BigDecimal("0.50");
        BigDecimal quantity = BigDecimal.valueOf(2);

        checkEquals(BigDecimal.valueOf(6), rules.calculateTotal(fixedPrice, unitPrice1, quantity, discount));
        checkEquals(BigDecimal.valueOf(-6), rules.calculateTotal(fixedPrice, unitPrice1, quantity.negate(), discount));

        // now ensure it is rounded to 2 decimal places
        BigDecimal unitPrice2 = new BigDecimal("0.514");
        checkEquals(new BigDecimal("6.03"), rules.calculateTotal(fixedPrice, unitPrice2, quantity, discount));
        checkEquals(new BigDecimal("-6.03"), rules.calculateTotal(fixedPrice, unitPrice2, quantity.negate(),
                                                                  discount));

        // simulate 100% discount
        BigDecimal fixedPrice3 = new BigDecimal("12.00");
        BigDecimal discount3 = new BigDecimal("106.08");
        BigDecimal unitPrice3 = new BigDecimal("37.63");
        BigDecimal quantity3 = new BigDecimal("2.50");
        checkEquals(ZERO, rules.calculateTotal(fixedPrice3, unitPrice3, quantity3, discount3));
        checkEquals(ZERO, rules.calculateTotal(fixedPrice3, unitPrice3, quantity3.negate(), discount3));

        // zero quantity
        checkEquals(ZERO, rules.calculateTotal(fixedPrice, unitPrice1, ZERO, discount));
        checkEquals(ZERO, rules.calculateTotal(fixedPrice, unitPrice2, ZERO, discount));
        checkEquals(ZERO, rules.calculateTotal(fixedPrice3, unitPrice3, ZERO, discount3));
        checkEquals(ZERO, rules.calculateTotal(fixedPrice, unitPrice1, ZERO.negate(), discount));
    }

    /**
     * Tests the {@link CustomerAccountRules#getOpeningBalanceBefore(Party, Date)}
     * and {@link CustomerAccountRules#getOpeningBalanceAfter(Party, Date)} methods.
     */
    @Test
    public void testGetOpeningBalanceBeforeAndAfter() {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();
        assertNull(rules.getOpeningBalanceBefore(customer, new Date()));
        assertNull(rules.getOpeningBalanceAfter(customer, new Date()));

        Date date1 = getDate("2018-01-01");
        Date date2 = getDate("2018-02-01");
        Date date3 = getDate("2018-03-01");

        FinancialAct open1 = rules.createOpeningBalance(customer, date1, TEN);
        FinancialAct open2 = rules.createOpeningBalance(customer, date2, TEN.negate());
        FinancialAct open3 = rules.createOpeningBalance(customer, date3, ONE);
        save(open1, open2, open3);

        assertNull(rules.getOpeningBalanceBefore(customer, date1));
        assertEquals(open1, rules.getOpeningBalanceBefore(customer, date2));
        assertEquals(open2, rules.getOpeningBalanceBefore(customer, date3));

        assertEquals(open2, rules.getOpeningBalanceAfter(customer, date1));
        assertEquals(open3, rules.getOpeningBalanceAfter(customer, date2));
        assertNull(rules.getOpeningBalanceAfter(customer, date3));
    }

    /**
     * Tests the {@link CustomerAccountRules#createOpeningBalance(Party, Date, BigDecimal)} method.
     */
    @Test
    public void testCreateOpeningBalance() {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();

        Date date = new Date();
        FinancialAct open1 = rules.createOpeningBalance(customer, date, BigDecimal.ZERO);
        FinancialAct open2 = rules.createOpeningBalance(customer, date, TEN);
        FinancialAct open3 = rules.createOpeningBalance(customer, date, TEN.negate());

        checkOpeningBalance(open1, customer, date, ZERO, false);
        checkOpeningBalance(open2, customer, date, TEN, false);
        checkOpeningBalance(open3, customer, date, TEN, true);
    }

    /**
     * Tests the {@link CustomerAccountRules#createOpeningBalance(Party, Date, BigDecimal)} method.
     */
    @Test
    public void testCreateClosingBalance() {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();

        Date date = new Date();
        FinancialAct close1 = rules.createClosingBalance(customer, date, BigDecimal.ZERO);
        FinancialAct close2 = rules.createClosingBalance(customer, date, TEN);
        FinancialAct close3 = rules.createClosingBalance(customer, date, TEN.negate());

        checkClosingBalance(close1, customer, date, ZERO, true);
        checkClosingBalance(close2, customer, date, TEN, true);
        checkClosingBalance(close3, customer, date, TEN, false);
    }

    /**
     * Verifies that when a posted <em>act.customerAccountChargesInvoice</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddChargesInvoiceToBalance() {
        checkAddToBalance(createChargesInvoice(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerAccountChargesCounter</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddChargesCounterToBalance() {
        checkAddToBalance(createChargesCounter(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerAccountChargesCredit</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddChargesCreditToBalance() {
        checkAddToBalance(createChargesCredit(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerPayment</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddPaymentToBalance() {
        checkAddToBalance(createPayment(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerRefund</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddRefundToBalance() {
        checkAddToBalance(createRefund(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerAccountCreditAdjust</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddCreditAdjustToBalance() {
        checkAddToBalance(createCreditAdjust(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerAccountDebitAdjust</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddDebitAdjustToBalance() {
        checkAddToBalance(createDebitAdjust(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerAccountInitialBalance</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddInitialBalanceToBalance() {
        checkAddToBalance(createInitialBalance(new BigDecimal(100)));
    }

    /**
     * Verifies that when a posted <em>act.customerAccountBadDebt</em>
     * is saved, an <em>participation.customerAccountBalance</em> is
     * associated with it.
     */
    @Test
    public void testAddBadDebtToBalance() {
        checkAddToBalance(createBadDebt(new BigDecimal(100)));
    }

    /**
     * Tests the {@link CustomerAccountRules#getBalance(Party)} for an invoice.
     */
    @Test
    public void testGetBalanceForInvoice() {
        Party customer = getCustomer();
        BigDecimal amount = new BigDecimal(100);
        save(createChargesInvoice(amount));

        // check the balance
        checkEquals(amount, getRules().getBalance(customer));
    }

    /**
     * Tests the {@link CustomerAccountRules#getBalance(Party)} for a credit.
     */
    @Test
    public void testGetBalanceForCredit() {
        Party customer = getCustomer();
        BigDecimal amount = new BigDecimal(100);
        save(createChargesCredit(amount));

        // check the balance
        checkEquals(amount.negate(), getRules().getBalance(customer));
    }

    /**
     * Verifies that a negative invoice is treated as a credit by {@link CustomerAccountRules#getBalance(Party)}.
     */
    @Test
    public void testGetBalanceForNegativeInvoice() {
        Party customer = getCustomer();
        BigDecimal amount = new BigDecimal(-100);
        save(createChargesInvoice(amount));

        // check the balance
        checkEquals(amount, getRules().getBalance(customer));
    }

    /**
     * Verifies that a negative credit is treated as a debit by {@link CustomerAccountRules#getBalance(Party)}.
     */
    @Test
    public void testGetBalanceForNegativeCredit() {
        Party customer = getCustomer();
        BigDecimal amount = new BigDecimal(-100);
        List<FinancialAct> invoiceActs = createChargesCredit(amount);
        save(invoiceActs);

        // check the balance
        checkEquals(amount.negate(), getRules().getBalance(customer));
    }

    /**
     * Verifies that an <em>act.customerAccountChargesInvoice</em> is
     * offset by an <em>act.customerAccountPayment</em> for the same amount.
     */
    @Test
    public void testGetBalanceForChargesInvoiceAndPayment() {
        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        List<FinancialAct> payment = Collections.singletonList(createPayment(amount));
        checkCalculateBalanceForSameAmount(invoice, payment);
    }

    /**
     * Verifies that an <em>act.customerAccountChargesCounter</em> is
     * offset by an <em>act.customerAccountPayment</em> for the same amount.
     */
    @Test
    public void testGetBalanceForChargesCounterAndPayment() {
        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> counter = createChargesCounter(amount);
        List<FinancialAct> payment = Collections.singletonList(createPayment(amount));
        checkCalculateBalanceForSameAmount(counter, payment);
    }

    /**
     * Verifies that an <em>act.customerAccountChargesInvoice</em> is
     * offset by an <em>act.customerAccountChargesCredit</em> for the same
     * amount.
     */
    @Test
    public void testGetBalanceForChargesInvoiceAndCredit() {
        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        List<FinancialAct> credit = createChargesCredit(amount);
        checkCalculateBalanceForSameAmount(invoice, credit);
    }

    /**
     * Verifies that an <em>act.customerAccountRefund</em> is offset by an
     * <em>act.customerAccountPayment</em> for the same amount.
     */
    @Test
    public void testGetBalanceForRefundAndPayment() {
        BigDecimal amount = new BigDecimal(100);
        FinancialAct refund = createRefund(amount);
        FinancialAct payment = createPayment(amount);
        checkCalculateBalanceForSameAmount(refund, payment);
    }

    /**
     * Verifies that an <em>act.customerAccountDebitAdjust</em> is offset by an
     * <em>act.customerAccountCreditAdjust</em> for the same amount.
     */
    @Test
    public void testGetBalanceForDebitAndCreditAdjust() {
        BigDecimal amount = new BigDecimal(100);
        FinancialAct debit = createDebitAdjust(amount);
        FinancialAct credit = createCreditAdjust(amount);
        checkCalculateBalanceForSameAmount(debit, credit);
    }

    /**
     * Verifies that an <em>act.customerAccountInitialBalance</em> is offset by
     * an <em>act.customerAccountBadDebt</em> for the same amount.
     */
    @Test
    public void testGetBalanceForInitialBalanceAndBadDebt() {
        BigDecimal amount = new BigDecimal(100);
        FinancialAct debit = createInitialBalance(amount);
        FinancialAct credit = createBadDebt(amount);
        checkCalculateBalanceForSameAmount(debit, credit);
    }

    /**
     * Tests the {@link CustomerAccountRules#getBalance} method.
     */
    @Test
    public void testGetBalance() {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();
        BigDecimal hundred = new BigDecimal(100);
        BigDecimal sixty = new BigDecimal(60);
        BigDecimal forty = new BigDecimal(40);
        List<FinancialAct> invoiceActs = createChargesInvoice(hundred);
        save(invoiceActs);

        // check the balance
        checkEquals(hundred, rules.getBalance(customer));

        // make sure the invoice has an account balance participation
        FinancialAct invoice = invoiceActs.get(0);
        IMObjectBean bean = getBean(invoice);
        assertEquals(customer, bean.getTarget("accountBalance"));

        // reload and verify act has not changed
        invoice = get(invoice);
        checkEquals(hundred, invoice.getTotal());
        checkEquals(ZERO, invoice.getAllocatedAmount());
        checkAllocation(invoice);

        // pay 60 of the debt
        FinancialAct payment1 = createPayment(sixty);
        save(payment1);

        // check the balance
        checkEquals(forty, rules.getBalance(customer));

        // reload and verify the acts have changed
        invoice = get(invoice);
        payment1 = get(payment1);

        checkEquals(hundred, invoice.getTotal());
        checkEquals(sixty, invoice.getAllocatedAmount());
        checkAllocation(invoice, payment1);

        checkEquals(sixty, payment1.getTotal());
        checkEquals(sixty, payment1.getAllocatedAmount());
        checkAllocation(payment1, invoice);

        // pay the remainder of the debt
        FinancialAct payment2 = createPayment(forty);
        save(payment2);

        // check the balance
        checkEquals(ZERO, rules.getBalance(customer));

        // reload and verify the acts have changed
        invoice = get(invoice);
        payment2 = get(payment2);

        checkEquals(hundred, invoice.getTotal());
        checkEquals(hundred, invoice.getAllocatedAmount());
        checkAllocation(invoice, payment1, payment2);

        checkEquals(forty, payment2.getTotal());
        checkEquals(forty, payment2.getAllocatedAmount());
        checkAllocation(payment2, invoice);
    }

    /**
     * Tests the {@link CustomerAccountRules#getBalance(Party, BigDecimal,
     * boolean)} method.
     */
    @Test
    public void testGetRunningBalance() {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();
        BigDecimal hundred = new BigDecimal(100);
        BigDecimal sixty = new BigDecimal(60);
        BigDecimal forty = new BigDecimal(40);
        BigDecimal ten = new BigDecimal(10);
        BigDecimal five = new BigDecimal(5);
        List<FinancialAct> invoice = createChargesInvoice(hundred);
        save(invoice);

        // check the balance for a payment
        checkEquals(hundred, rules.getBalance(customer, ZERO, true));

        // check the balance for a refund
        checkEquals(ZERO, rules.getBalance(customer, ZERO, false));

        // simulate payment of 60. Running balance should be 40
        checkEquals(forty, rules.getBalance(customer, sixty, true));

        // overpay by 10
        FinancialAct payment1 = createPayment(new BigDecimal("110.0"));
        save(payment1);

        // check the balance for a payment
        checkEquals(ZERO, rules.getBalance(customer, ZERO, true));

        // check the balance for a refund
        checkEquals(ten, rules.getBalance(customer, ZERO, false));

        // check the balance after refunding 5
        checkEquals(five, rules.getBalance(customer, five, false));
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} method.
     */
    @Test
    public void testGetCurrentOverdueBalance() {
        CustomerAccountRules rules = getRules();

        // add a 30 day payment term for accounts to the customer
        Party customer = getCustomer();
        customer.addClassification(createAccountType(30, DateUnits.DAYS));
        save(customer);

        // create and save a new invoice
        BigDecimal amount = new BigDecimal(100);
        Date startTime = getDate("2007-01-01");
        List<FinancialAct> invoice = createChargesInvoice(amount, startTime);
        save(invoice);

        // check the invoice is not overdue on the day it is saved
        BigDecimal overdue = rules.getOverdueBalance(customer, startTime);
        checkEquals(ZERO, overdue);

        // 30 days from saved, amount shouldn't be overdue
        Date now = DateRules.getDate(startTime, 30, DateUnits.DAYS);
        overdue = rules.getOverdueBalance(customer, now);
        checkEquals(ZERO, overdue);

        // 31 days from saved, invoice should be overdue.
        now = DateRules.getDate(now, 1, DateUnits.DAYS);
        overdue = rules.getOverdueBalance(customer, now);
        checkEquals(amount, overdue);

        // now save a credit for the same date as the invoice with total
        // > invoice total. The balance should be negative, but the overdue
        // balance should be zero as it only sums debits.
        List<FinancialAct> creditActs = createChargesCredit(new BigDecimal(150));
        FinancialAct credit = creditActs.get(0);
        credit.setActivityStartTime(startTime);
        save(creditActs);
        checkBalance(new BigDecimal(-50));
        overdue = rules.getOverdueBalance(customer, now);
        checkEquals(ZERO, overdue);
    }

    /**
     * Verifies that a negative invoice isn't included in an overdue balance by
     * {@link CustomerAccountRules#getOverdueBalance(Party, Date)}.
     */
    @Test
    public void testGetCurrentOverdueBalanceForNegativeInvoice() {
        CustomerAccountRules rules = getRules();

        // add a 30 day payment term for accounts to the customer
        Party customer = getCustomer();
        customer.addClassification(createAccountType(30, DateUnits.DAYS));
        save(customer);

        // create and save a new invoice
        BigDecimal amount = new BigDecimal(-100);
        Date startTime = getDate("2007-01-01");
        List<FinancialAct> invoice = createChargesInvoice(amount, startTime);
        save(invoice);

        checkEquals(amount, rules.getBalance(customer));
        checkEquals(ZERO, rules.getOverdueBalance(customer, startTime));

        // verify that 31 days after a positive invoice would be considered overdue, the overdue balance is still 0
        Date now = DateRules.getDate(startTime, 31, DateUnits.DAYS);
        checkEquals(ZERO, rules.getOverdueBalance(customer, now));
    }

    /**
     * Verifies that a negative credit is included in an overdue balance by
     * {@link CustomerAccountRules#getOverdueBalance(Party, Date)}.
     */
    @Test
    public void testGetCurrentOverdueBalanceForNegativeCredit() {
        CustomerAccountRules rules = getRules();

        // add a 30 day payment term for accounts to the customer
        Party customer = getCustomer();
        customer.addClassification(createAccountType(30, DateUnits.DAYS));
        save(customer);

        // create and save a new credit
        BigDecimal amount = new BigDecimal(-100);
        Date startTime = getDate("2007-01-01");
        List<FinancialAct> credit = createChargesCredit(amount);
        credit.get(0).setActivityStartTime(startTime);
        save(credit);

        checkEquals(amount.negate(), rules.getBalance(customer));
        checkEquals(ZERO, rules.getOverdueBalance(customer, startTime));

        // verify that the credit is overdue 31 days after
        Date now = DateRules.getDate(startTime, 31, DateUnits.DAYS);
        checkEquals(amount.negate(), rules.getOverdueBalance(customer, now));
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance(Party, Date, Date)} method.
     */
    @Test
    public void testGetOverdueBalance() {
        CustomerAccountRules rules = getRules();

        // add a 30 day payment term for accounts to the customer
        Party customer = getCustomer();
        customer.addClassification(createAccountType(30, DateUnits.DAYS));
        save(customer);

        // create and save 2 invoices
        BigDecimal amount = new BigDecimal(100);
        Date startTime = getDate("2007-01-01");
        createInvoice(startTime, new BigDecimal(50));
        createInvoice(startTime, new BigDecimal(50));

        // check the invoices are not overdue on the day they are saved
        Date overdueDate = rules.getOverdueDate(customer, startTime);
        BigDecimal overdue = rules.getOverdueBalance(customer, startTime, overdueDate);
        checkEquals(ZERO, overdue);

        // 30 days from saved, amount shouldn't be overdue
        Date now = DateRules.getDate(startTime, 30, DateUnits.DAYS);
        overdueDate = rules.getOverdueDate(customer, now);
        overdue = rules.getOverdueBalance(customer, now, overdueDate);
        checkEquals(ZERO, overdue);

        // 31 days from saved, invoices should be overdue.
        Date statementDate = DateRules.getDate(now, 1, DateUnits.DAYS);
        overdueDate = rules.getOverdueDate(customer, statementDate);
        overdue = rules.getOverdueBalance(customer, statementDate, overdueDate);
        checkEquals(amount, overdue);

        // now save 3 credits dated 32 days from saved.
        // The current balance should = -50, but the overdue balance as
        // of 31 days after saved should still be 100
        now = DateRules.getDate(statementDate, 1, DateUnits.DAYS);
        createCredit(new BigDecimal(40), now);
        createCredit(new BigDecimal(35), now);
        createCredit(new BigDecimal(75), now);

        checkBalance(new BigDecimal(-50));
        overdue = rules.getOverdueBalance(customer, statementDate, overdueDate);
        checkEquals(amount, overdue);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for an initial balance.
     * This should be included in overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForInitialBalance() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(Collections.singletonList(createInitialBalance(plus100)), plus100, plus100);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a counter sale.
     * This should be included in overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForCounterSale() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(createChargesCounter(plus100), plus100, plus100);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a debit adjustment.
     * This should be included in overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForDebitAdjust() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(Collections.singletonList(createDebitAdjust(plus100)), plus100, plus100);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a positive invoice.
     * The invoice should be included in overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForPositiveInvoice() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(createChargesInvoice(plus100), plus100, plus100);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a negative invoice.
     * The invoice should be excluded from overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForNegativeInvoice() {
        BigDecimal minus100 = BigDecimal.valueOf(-100);
        checkOverdueBalance(createChargesInvoice(minus100), minus100, ZERO);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a positive credit.
     * The credit should be excluded from overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForPositiveCredit() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(createChargesCredit(plus100), plus100.negate(), ZERO);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a negative credit.
     * The credit should be included in overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForNegativeCredit() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        BigDecimal minus100 = BigDecimal.valueOf(-100);
        checkOverdueBalance(createChargesCredit(minus100), plus100, plus100);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a refund.
     * This should be included in overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForRefund() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(Collections.singletonList(createRefund(plus100)), plus100, plus100);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a credit adjustment.
     * This should be excluded from overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForCreditAdjustment() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(Collections.singletonList(createCreditAdjust(plus100)), plus100.negate(), ZERO);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a credit adjustment.
     * This should be excluded from overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForBadDebt() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(Collections.singletonList(createBadDebt(plus100)), plus100.negate(), ZERO);
    }

    /**
     * Tests the {@link CustomerAccountRules#getOverdueBalance} methods for a payment.
     * This should be excluded from overdue balances.
     */
    @Test
    public void testGetOverdueBalanceForPayment() {
        BigDecimal plus100 = BigDecimal.valueOf(100);
        checkOverdueBalance(Collections.singletonList(createPayment(plus100)), plus100.negate(), ZERO);
    }

    /**
     * Tests the {@link CustomerAccountRules#getUnbilledAmount(Party)}  method.
     */
    @Test
    public void testGetUnbilledAmount() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoices = createChargesInvoice(amount);
        List<FinancialAct> counters = createChargesCounter(amount);
        List<FinancialAct> credits = createChargesCredit(amount);

        FinancialAct invoice = invoices.get(0);
        invoice.setStatus(IN_PROGRESS);

        FinancialAct counter = counters.get(0);
        counter.setStatus(IN_PROGRESS);

        FinancialAct credit = credits.get(0);
        credit.setStatus(IN_PROGRESS);

        checkEquals(ZERO, rules.getUnbilledAmount(getCustomer()));

        save(invoices);
        checkEquals(amount, rules.getUnbilledAmount(getCustomer()));

        save(counters);
        checkEquals(amount.multiply(new BigDecimal(2)), rules.getUnbilledAmount(getCustomer()));

        save(credits);
        checkEquals(amount, rules.getUnbilledAmount(getCustomer()));

        credit.setStatus(POSTED);
        save(credit);
        checkEquals(amount.multiply(new BigDecimal(2)), rules.getUnbilledAmount(getCustomer()));

        counter.setStatus(POSTED);
        save(counter);
        checkEquals(amount, rules.getUnbilledAmount(getCustomer()));

        invoice.setStatus(POSTED);
        save(invoice);
        checkEquals(ZERO, rules.getUnbilledAmount(getCustomer()));
    }

    /**
     * Tests the reversal of customer account acts by {@link CustomerAccountRules#reverse}.
     */
    @Test
    public void testReverse() {
        checkReverse(createInitialBalance(new BigDecimal(25)), CREDIT_ADJUST);

        checkReverseInvoice();

        checkReverseCharge(createChargesCredit(new BigDecimal(50)), INVOICE, INVOICE_ITEM);

        checkReverseCharge(createChargesCounter(new BigDecimal(40)), CREDIT, CREDIT_ITEM);

        checkReverse(createPaymentCash(new BigDecimal(75)), REFUND, REFUND_CASH, false, null, new BigDecimal(75));

        checkReverse(createPaymentCheque(new BigDecimal(23)), REFUND, REFUND_CHEQUE, false, null, null);

        checkReverse(createPaymentCredit(new BigDecimal(24)), REFUND, REFUND_CREDIT, false, null, null);

        checkReverse(createPaymentDiscount(new BigDecimal(25)), REFUND, REFUND_DISCOUNT, false, null, null);

        checkReverse(createPaymentEFT(new BigDecimal(26)), REFUND, REFUND_EFT, false, null, null);

        checkReverse(createPaymentOther(new BigDecimal(26)), REFUND, REFUND_OTHER, false, null, null);

        checkReverse(createRefundCash(TEN), PAYMENT, PAYMENT_CASH, false, null, TEN);

        checkReverse(createRefundCheque(new BigDecimal(11)), PAYMENT, PAYMENT_CHEQUE, false, null, null);

        checkReverse(createRefundCredit(new BigDecimal(12)), PAYMENT, PAYMENT_CREDIT, false, null, null);

        checkReverse(createRefundDiscount(new BigDecimal(13)), PAYMENT, PAYMENT_DISCOUNT, false, null, null);

        checkReverse(createRefundEFT(new BigDecimal(15)), PAYMENT, PAYMENT_EFT, false, null, null);

        checkReverse(createRefundOther(new BigDecimal(15)), PAYMENT, PAYMENT_OTHER, false, null, null);

        checkReverse(createDebitAdjust(new BigDecimal(5)), CREDIT_ADJUST);

        checkReverse(createCreditAdjust(new BigDecimal(15)), DEBIT_ADJUST);

        checkReverse(createBadDebt(new BigDecimal(20)), DEBIT_ADJUST);
    }

    /**
     * Tests reversal of a negative invoice.
     */
    @Test
    public void testReverseNegativeInvoice() {
        checkReversalOfNegativeCharge(INVOICE, INVOICE_ITEM, CREDIT, CREDIT_ITEM);
    }

    /**
     * Tests reversal of a positive invoice with a negative line item.
     */
    @Test
    public void testReverseInvoiceWithNegativeLineItem() {
        checkReversalOfChargeWithNegativeQuantity(INVOICE, INVOICE_ITEM, CREDIT, CREDIT_ITEM);
    }

    /**
     * Tests reversal of a negative credit.
     */
    @Test
    public void testReverseNegativeCredit() {
        checkReversalOfNegativeCharge(CREDIT, CREDIT_ITEM, INVOICE, INVOICE_ITEM);
    }

    /**
     * Tests reversal of a positive credit with a negative line item.
     */
    @Test
    public void testReverseCreditWithNegativeLineItem() {
        checkReversalOfChargeWithNegativeQuantity(CREDIT, CREDIT_ITEM, INVOICE, INVOICE_ITEM);
    }

    /**
     * Tests reversal of a negative counter sale.
     */
    @Test
    public void testReverseNegativeCounterSale() {
        checkReversalOfNegativeCharge(COUNTER, COUNTER_ITEM, CREDIT, CREDIT_ITEM);
    }

    /**
     * Tests reversal of a positive credit with a negative line item.
     */
    @Test
    public void testReverseCounterSaleWithNegativeLineItem() {
        checkReversalOfChargeWithNegativeQuantity(COUNTER, COUNTER_ITEM, CREDIT, CREDIT_ITEM);
    }

    /**
     * Tests reversal of an allocated act.
     */
    @Test
    public void testReverseAllocated() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        save(invoice);

        FinancialAct payment = createPayment(amount);
        save(payment);
        checkAllocation(get(invoice.get(0)), payment);

        checkBalance(ZERO);

        FinancialAct reversal = rules.reverse(payment, new Date(), "Test reversal", null, false);
        checkEquals(ZERO, reversal.getAllocatedAmount());

        checkBalance(amount);
    }

    /**
     * Tests reversal of an unallocated act.
     */
    @Test
    public void testReverseUnallocated() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        save(invoice);

        checkBalance(amount);

        FinancialAct reversal = rules.reverse(invoice.get(0), new Date(), "Test reversal", null, false);
        checkBalance(ZERO);
        checkAllocation(invoice.get(0), reversal);
    }

    /**
     * Tests reversal of a partially allocated act.
     */
    @Test
    public void testReversePartiallyAllocated() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        BigDecimal sixty = new BigDecimal(60);
        BigDecimal forty = new BigDecimal(40);

        // create a new invoice for $100
        List<FinancialAct> invoices = createChargesInvoice(amount);
        save(invoices);
        FinancialAct invoice = invoices.get(0);
        checkEquals(ZERO, invoice.getAllocatedAmount());

        // pay $60. Payment is fully allocated, $60 of invoice is allocated.
        FinancialAct payment = createPayment(sixty);
        save(payment);
        checkEquals(sixty, payment.getAllocatedAmount());

        invoice = get(invoice);
        checkEquals(sixty, invoice.getAllocatedAmount());

        // $40 outstanding balance
        checkBalance(forty);

        // reverse the payment.
        FinancialAct reversal = rules.reverse(payment, new Date(), "Test reversal", null, false);
        checkEquals(ZERO, reversal.getAllocatedAmount());

        // invoice and payment retain their allocations
        invoice = get(invoice);
        checkEquals(sixty, invoice.getAllocatedAmount());

        payment = get(payment);
        checkEquals(sixty, payment.getAllocatedAmount());

        checkBalance(amount);
    }

    /**
     * Verifies that a reversal doesn't affect an invoice in a gap claim.
     */
    @Test
    public void testReversalDoesntImpactGapClaim() {
        List<FinancialAct> invoice1Acts = createChargesInvoice(TEN, DateRules.getYesterday());
        save(invoice1Acts);
        FinancialAct invoice1 = invoice1Acts.get(0);
        FinancialAct item1 = invoice1Acts.get(1);

        checkBalance(TEN);

        // create a claim for invoice1
        Act policy = (Act) InsuranceTestHelper.createPolicy(getCustomer(), getPatient(),
                                                            InsuranceTestHelper.createInsurer(), "12345");
        FinancialAct claimItem = (FinancialAct) InsuranceTestHelper.createClaimItem(item1);
        User clinician = TestHelper.createClinician();
        FinancialAct claim = (FinancialAct) InsuranceTestHelper.createClaim(policy, TestHelper.createLocation(),
                                                                            clinician, clinician, true, claimItem);
        save(policy, claim, claimItem);

        List<FinancialAct> invoice2Acts = createChargesInvoice(TEN);
        save(invoice2Acts);
        FinancialAct invoice2 = invoice2Acts.get(0);

        checkBalance(BigDecimal.valueOf(20));

        FinancialAct payment = createPayment(TEN);
        save(payment);

        checkBalance(TEN);

        // check allocations - invoice1 shouldn't be allocated, as it is in a gap claim
        invoice1 = get(invoice1);
        invoice2 = get(invoice2);
        payment = get(payment);
        checkEquals(ZERO, invoice1.getAllocatedAmount());
        checkEquals(TEN, invoice2.getAllocatedAmount());
        checkEquals(TEN, payment.getAllocatedAmount());

        // verify that the reversal isn't allocated, but that the balance is zero
        FinancialAct reversal = getRules().reverse(invoice2, new Date());
        checkEquals(ZERO, reversal.getAllocatedAmount());

        checkBalance(ZERO);

        // verify the other allocations
        invoice1 = get(invoice1);
        invoice2 = get(invoice2);
        payment = get(payment);
        checkEquals(ZERO, invoice1.getAllocatedAmount());
        checkEquals(TEN, invoice2.getAllocatedAmount());
        checkEquals(TEN, payment.getAllocatedAmount());
    }

    /**
     * Verifies that a reversal first allocates to the act it is reversing.
     */
    @Test
    public void testReversalAllocationOrder() {
        FinancialAct invoice1 = createInvoice(DateRules.getYesterday(), TEN, POSTED);
        FinancialAct invoice2 = createInvoice(DateRules.getToday(), TEN, POSTED);

        FinancialAct reverse = getRules().reverse(invoice2, new Date());

        invoice1 = get(invoice1);
        invoice2 = get(invoice2);

        checkEquals(ZERO, invoice1.getAllocatedAmount());
        checkEquals(TEN, invoice2.getAllocatedAmount());
        checkEquals(TEN, reverse.getAllocatedAmount());
    }

    /**
     * Verifies that a reversal first allocates to the act it is reversing, then the next oldest act
     */
    @Test
    public void testReversalAllocationOrder2() {
        FinancialAct invoice1 = createInvoice(DateRules.getYesterday(), TEN, POSTED);
        BigDecimal five = BigDecimal.valueOf(5);
        FinancialAct invoice2 = createInvoice(DateRules.getToday(), TEN, POSTED);
        FinancialAct invoice3 = createInvoice(new Date(), TEN, POSTED);

        FinancialAct payment = createPayment(five);
        save(payment);

        // verify payment allocated to oldest invoice
        invoice1 = get(invoice1);
        checkEquals(five, invoice1.getAllocatedAmount());

        // invoice2, invoice3 unchanged
        invoice2 = get(invoice2);
        invoice3 = get(invoice3);
        checkEquals(ZERO, invoice2.getAllocatedAmount());
        checkEquals(ZERO, invoice3.getAllocatedAmount());

        // now reverse it. Should be allocated to first, followed by invoice2
        FinancialAct reverse = getRules().reverse(invoice1, new Date());

        invoice1 = get(invoice1);
        invoice2 = get(invoice2);
        invoice3 = get(invoice3);

        checkEquals(TEN, invoice1.getAllocatedAmount());
        checkEquals(five, invoice2.getAllocatedAmount());
        checkEquals(ZERO, invoice3.getAllocatedAmount());
        checkEquals(TEN, reverse.getAllocatedAmount());
    }

    /**
     * Verifies an act can't be reversed twice.
     */
    @Test
    public void testReverseTwice() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        save(invoice);

        checkBalance(amount);

        rules.reverse(invoice.get(0), new Date(), "Test reversal", null, false);
        checkBalance(ZERO);

        try {
            rules.reverse(invoice.get(0), new Date(), "Test reversal 2", null, false);
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // do nothing
        }
    }

    /**
     * Checks the behaviour of the {@code hide} parameter, when reversing transactions.
     */
    @Test
    public void testReverseHide() {
        List<FinancialAct> invoice1 = createChargesInvoice(new BigDecimal(100));
        save(invoice1);
        checkReverse(invoice1.get(0), "act.customerAccountChargesCredit", "act.customerAccountCreditItem", true, null, null);

        List<FinancialAct> invoice2 = createChargesInvoice(new BigDecimal(100));
        save(invoice2);

        checkReverse(invoice2.get(0), "act.customerAccountChargesCredit", "act.customerAccountCreditItem", false, null, null);
    }

    /**
     * Tests the {@link CustomerAccountRules#isReversed(FinancialAct)} method and
     * {@link CustomerAccountRules#isReversal(FinancialAct)} methods.
     */
    @Test
    public void testIsReversedIsReversal() {
        List<FinancialAct> invoice = createChargesInvoice(new BigDecimal(100));
        save(invoice);
        CustomerAccountRules rules = getRules();
        FinancialAct act = invoice.get(0);
        FinancialAct reverse = rules.reverse(act, new Date());

        assertTrue(rules.isReversed(act));
        assertFalse(rules.isReversed(reverse));

        assertFalse(rules.isReversal(act));
        assertTrue(rules.isReversal(reverse));

        FinancialAct reverse2 = rules.reverse(reverse, new Date());

        assertTrue(rules.isReversed(reverse));
        assertTrue(rules.isReversal(reverse));

        assertFalse(rules.isReversed(reverse2));
        assertTrue(rules.isReversal(reverse2));
    }

    /**
     * Verifies that act.EFTPOSPayments aren't included when an EFT payment is reversed.
     */
    @Test
    public void testReversePaymentEFTWithEFPOSTransaction() {
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSPayment()
                .customer(getCustomer())
                .terminal(practiceFactory.createEFTPOSTerminal())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct item = (FinancialAct) accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct payment = (FinancialAct) accountFactory.newPayment()
                .customer(getCustomer())
                .till(getTill())
                .add(item)
                .build();

        FinancialAct reversal = checkReverse(payment, REFUND, REFUND_EFT, false, null, null);
        List<FinancialAct> items = getBean(reversal).getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());
        IMObjectBean bean = getBean(items.get(0));
        List<FinancialAct> eft = bean.getTargets("eft", FinancialAct.class);
        assertEquals(0, eft.size());
    }

    /**
     * Verifies that act.EFTPOSRefunds aren't included when an EFT refund is reversed.
     */
    @Test
    public void testReverseRefundEFTWithEFPOSTransaction() {
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSRefund()
                .customer(getCustomer())
                .terminal(practiceFactory.createEFTPOSTerminal())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct item = (FinancialAct) accountFactory.newEFTRefundItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct refund = (FinancialAct) accountFactory.newRefund()
                .customer(getCustomer())
                .till(getTill())
                .add(item)
                .build();

        FinancialAct reversal = checkReverse(refund, PAYMENT, PAYMENT_EFT, false, null, null);
        List<FinancialAct> items = getBean(reversal).getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());
        IMObjectBean bean = getBean(items.get(0));
        List<FinancialAct> eft = bean.getTargets("eft", FinancialAct.class);
        assertEquals(0, eft.size());
    }

    /**
     * Verifies only {@code POSTED} acts can be reversed.
     */
    @Test
    public void testReverseNonPosted() {
        for (String status : new String[]{IN_PROGRESS, COMPLETED, ON_HOLD}) {
            List<FinancialAct> acts = createChargesInvoice(new BigDecimal(100));
            FinancialAct invoice = acts.get(0);
            invoice.setStatus(status);
            save(acts);
            try {
                getRules().reverse(invoice, new Date());
                fail("Expected reverse to fail");
            } catch (IllegalStateException expected) {
                assertEquals("Cannot reverse act with status " + status, expected.getMessage());
            }
        }
    }


    /**
     * Tests the {@link CustomerAccountRules#setHidden(FinancialAct, boolean)} method.
     */
    @Test
    public void testSetHidden() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        save(invoice);

        checkBalance(amount);

        FinancialAct act = invoice.get(0);
        assertFalse(rules.isHidden(act));

        FinancialAct reversal = rules.reverse(act, new Date(), "Test reversal", null, false);
        assertFalse(rules.isHidden(reversal));

        checkBalance(ZERO);

        rules.setHidden(act, true);
        checkBalance(ZERO);

        rules.setHidden(reversal, true);
        checkBalance(ZERO);

        assertTrue(rules.isHidden(act));
        assertTrue(rules.isHidden(reversal));

        rules.setHidden(act, false);
        assertFalse(rules.isHidden(act));
        checkBalance(ZERO);

        rules.setHidden(reversal, false);
        assertFalse(rules.isHidden(reversal));
        checkBalance(ZERO);
    }

    /**
     * Verifies that the reversal of a reversal cannot be hidden by
     * {@link CustomerAccountRules#reverse(FinancialAct, Date, String, String, boolean)}.
     * <p>
     * If allowed by default, the customer statement would not add up.
     */
    @Test
    public void testHideIgnoredForReverseOfReverse() {
        CustomerAccountRules rules = getRules();

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice = createChargesInvoice(amount);
        save(invoice);

        checkBalance(amount);

        FinancialAct act = invoice.get(0);
        FinancialAct reversal = rules.reverse(act, new Date(), "Test reversal", null, true);
        checkBalance(ZERO);

        assertTrue(rules.isHidden(act));
        assertTrue(rules.isHidden(reversal));

        FinancialAct reversal2 = rules.reverse(reversal, new Date(), "Test reversal 2", null, true);

        act = get(act);
        reversal = get(reversal);

        assertTrue(rules.isHidden(act));
        assertTrue(rules.isHidden(reversal));
        assertFalse(rules.isHidden(reversal2));

        checkBalance(amount);
    }

    /**
     * Tests the {@link CustomerAccountRules#getReversalArchetype(String)} method.
     */
    @Test
    public void getReversalArchetype() {
        CustomerAccountRules rules = getRules();
        assertEquals(CREDIT, rules.getReversalArchetype(INVOICE));
        assertEquals(INVOICE, rules.getReversalArchetype(CREDIT));
        assertEquals(REFUND, rules.getReversalArchetype(PAYMENT));
        assertEquals(PAYMENT, rules.getReversalArchetype(REFUND));
        assertEquals(REFUND_CASH, rules.getReversalArchetype(PAYMENT_CASH));
        assertEquals(PAYMENT_CASH, rules.getReversalArchetype(REFUND_CASH));
        assertEquals(REFUND_EFT, rules.getReversalArchetype(PAYMENT_EFT));
        assertEquals(PAYMENT_EFT, rules.getReversalArchetype(REFUND_EFT));

        assertNull(rules.getReversalArchetype(SupplierArchetypes.INVOICE));
    }

    /**
     * Verifies that <em>participation.customerAccountBalance</em>
     * participations are not present when an act has a zero total.
     */
    @Test
    public void testZeroAct() {
        // save an IN_PROGRESS invoice with a zero total, and verify it
        // has no balance participation
        List<FinancialAct> invoices = createChargesInvoice(ZERO);
        FinancialAct invoice = invoices.get(0);
        invoice.setStatus(IN_PROGRESS);
        save(invoices);
        IMObjectBean bean = getBean(invoice);
        assertTrue(bean.getValues("accountBalance").isEmpty());

        // change to a non-zero total and save. Should now have a participation
        invoice.setTotal(BigDecimal.TEN);
        save(invoice);
        assertFalse(bean.getValues("accountBalance").isEmpty());

        // revert to a zero total and save. The participation should be removed
        invoice.setTotal(Money.ZERO);
        save(invoice);
        assertTrue(bean.getValues("accountBalance").isEmpty());

        // Post the invoice and verify no participation added
        invoice.setStatus(POSTED);
        save(invoice);
        assertTrue(bean.getValues("accountBalance").isEmpty());
    }

    /**
     * Verifies that older unallocated balances are allocated prior to more
     * recent ones for OVPMS-795.
     */
    @Test
    public void testAllocationOrder() {
        BigDecimal sixty = new BigDecimal(60);
        BigDecimal forty = new BigDecimal(40);
        BigDecimal twenty = new BigDecimal(20);
        Date chargeTime1 = getDate("2007-01-01");
        Date chargeTime2 = getDate("2007-03-30");

        List<FinancialAct> invoice1 = createChargesInvoice(sixty, chargeTime1);
        save(invoice1);
        List<FinancialAct> invoice2 = createChargesInvoice(forty, chargeTime2);
        save(invoice2);

        Date payTime1 = getDate("2007-04-01");
        FinancialAct payment1 = createPayment(forty, payTime1);
        save(payment1);

        FinancialAct reload1 = get(invoice1.get(0));
        FinancialAct reload2 = get(invoice2.get(0));
        checkEquals(forty, reload1.getAllocatedAmount());
        checkEquals(ZERO, reload2.getAllocatedAmount());

        Date payTime2 = getDate("2007-04-02");
        FinancialAct payment2 = createPayment(twenty, payTime2);
        save(payment2);

        reload1 = get(invoice1.get(0));
        reload2 = get(invoice2.get(0));
        checkEquals(sixty, reload1.getAllocatedAmount());
        checkEquals(ZERO, reload2.getAllocatedAmount());

        Date payTime3 = getDate("2007-04-03");
        FinancialAct payment3 = createPayment(forty, payTime3);
        save(payment3);

        reload1 = get(invoice1.get(0));
        reload2 = get(invoice2.get(0));
        checkEquals(sixty, reload1.getAllocatedAmount());
        checkEquals(forty, reload2.getAllocatedAmount());
    }

    /**
     * Tests the {@link CustomerAccountRules#getInvoice(Party)} method.
     */
    @Test
    public void testGetInvoice() {
        CustomerAccountRules rules = getRules();

        Party customer = getCustomer();
        assertNull(rules.getInvoice(customer));

        // verify posted, on hold invoices not returned
        createInvoice(getDate("2013-05-02"), TEN, POSTED);
        createInvoice(getDate("2013-05-02"), TEN, ON_HOLD);
        assertNull(rules.getInvoice(customer));

        FinancialAct invoice2 = createInvoice(getDate("2013-05-02"), TEN, FinancialActStatus.COMPLETED);
        assertEquals(invoice2, rules.getInvoice(customer));

        // verify back-dated IN_PROGRESS invoice returned in preference to COMPLETED invoice
        FinancialAct invoice3 = createInvoice(getDate("2013-05-01"), TEN, IN_PROGRESS);
        assertEquals(invoice3, rules.getInvoice(customer));

        // verify more recent IN_PROGRESS returned
        FinancialAct invoice4 = createInvoice(getDate("2013-05-05"), TEN, IN_PROGRESS);
        assertEquals(invoice4, rules.getInvoice(customer));
    }

    /**
     * Tests the {@link CustomerAccountRules#getCredit(Party)} method.
     */
    @Test
    public void testGetCredit() {
        CustomerAccountRules rules = getRules();

        Party customer = getCustomer();
        assertNull(rules.getCredit(customer));

        // verify posted credits not returned
        createCredit(getDate("2013-05-02"), POSTED);
        assertNull(rules.getInvoice(customer));

        FinancialAct credit2 = createCredit(getDate("2013-05-02"), FinancialActStatus.COMPLETED);
        assertEquals(credit2, rules.getCredit(customer));

        // verify back-dated IN_PROGRESS invoice returned in preference to COMPLETED credit
        FinancialAct credit3 = createCredit(getDate("2013-05-01"), IN_PROGRESS);
        assertEquals(credit3, rules.getCredit(customer));

        // verify more recent IN_PROGRESS returned
        FinancialAct credit4 = createCredit(getDate("2013-05-05"), IN_PROGRESS);
        assertEquals(credit4, rules.getCredit(customer));
    }

    /**
     * Verifies that when an invoice is reversed, any invoice items, medication, investigation and document acts are
     * unlinked from the patient history.
     * <p>
     * Also ensures that:
     * <ul>
     * <li>demographic updates aren't triggered in the process of performing the reversal, which requires
     * the invoice items to be saved.</li>
     * <li>stock quantities match that expected</li>
     * </ul>
     */
    @Test
    public void testReverseInvoiceRemovesEntriesFromHistory() {
        CustomerAccountRules rules = getRules();

        Party patient = getPatient();
        Party location = TestHelper.createLocation();

        // add a demographics update to the product that sets the patient as desexed when the invoice is POSTED
        Product product = getProduct();
        ProductTestHelper.addDemographicUpdate(product, "patient.entity", "party:setPatientDesexed(.)");

        IMObjectRelationship stock = initStockQuantity(new BigDecimal("10"));

        // create the invoice
        List<FinancialAct> invoice1 = createChargesInvoice(new BigDecimal(100));
        FinancialAct invoice = invoice1.get(0);
        invoice.setStatus(IN_PROGRESS);
        Act item1 = invoice1.get(1);
        Act medication = PatientTestHelper.createMedication(patient, getProduct());
        Act investigation = PatientTestHelper.createInvestigation(patient, createInvestigationType());
        Act document = PatientTestHelper.createDocumentForm(patient);
        IMObjectBean itemBean = getBean(item1);
        itemBean.addTarget("dispensing", medication, "invoiceItem");
        itemBean.addTarget("investigations", investigation, "invoiceItems");
        ActRelationship documentRelationship = (ActRelationship) itemBean.addTarget("documents", document);
        document.addActRelationship(documentRelationship);
        assertEquals(3, item1.getSourceActRelationships().size());

        // create another invoice
        List<FinancialAct> invoice2 = createChargesInvoice(new BigDecimal(10), getCustomer(),
                                                           TestHelper.createProduct());
        Act item2 = invoice2.get(1);

        List<IMObject> toSave = new ArrayList<>(invoice1);
        toSave.add(medication);
        toSave.add(investigation);
        toSave.add(document);
        toSave.addAll(invoice2);
        save(toSave);

        checkStock(stock, new BigDecimal("9"));

        assertFalse(patientRules.isDesexed(get(patient)));

        // now create the event
        Act event = PatientTestHelper.createEvent(patient);
        IMObjectBean eventBean = getBean(event);

        // link the charge items, medication, investigation and document to the event
        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        linker.link(event, item1, new PatientHistoryChanges(location, getArchetypeService()));
        linker.link(event, item2, new PatientHistoryChanges(location, getArchetypeService()));

        // verify they are linked
        List<Act> charges = eventBean.getTargets("chargeItems", Act.class);
        assertEquals(2, charges.size());
        assertTrue(charges.contains(item1));
        assertTrue(charges.contains(item2));

        List<Act> items = eventBean.getTargets("items", Act.class);
        assertEquals(3, items.size());
        assertTrue(items.contains(medication));
        assertTrue(items.contains(investigation));
        assertTrue(items.contains(document));

        // now post the invoice. The demographic update should be executed
        invoice.setStatus(POSTED);
        save(invoice);

        // stock should remain the same
        checkStock(stock, new BigDecimal("9"));

        patient = get(patient);
        assertTrue(patientRules.isDesexed(patient));

        // now set the patient as not desexed. When the invoice is reversed, the demographic update shouldn't fire
        // again.
        IMObjectBean bean = getBean(patient);
        bean.setValue("desexed", false);
        bean.save();

        // reverse the invoice.
        FinancialAct credit = rules.reverse(invoice, new Date());
        assertTrue(TypeHelper.isA(credit, CREDIT));
        IMObjectBean creditBean = getBean(credit);
        List<Act> creditItems = creditBean.getTargets("items", Act.class);
        assertEquals(1, creditItems.size());
        Act creditItem = creditItems.get(0);
        assertEquals(0, creditItem.getSourceActRelationships().size()); // should not have relationships

        event = get(event);  // reload the event
        eventBean = getBean(event);

        // ensure the item, medication and investigation still exist
        item1 = get(item1);
        assertNotNull(item1);
        assertNotNull(get(medication));
        assertNotNull(get(investigation));
        assertNotNull(get(document));

        // verify that item1, medication, investigation and document are no longer linked to the event
        charges = eventBean.getTargets("chargeItems", Act.class);
        assertEquals(1, charges.size());
        assertEquals(item2, charges.get(0));

        items = eventBean.getTargets("items", Act.class);
        assertEquals(0, items.size());

        // verify the item, medication, investigation and document are still linked to the invoice item
        assertEquals(3, item1.getSourceActRelationships().size());

        // verify the demographic update didn't get run again
        assertFalse(patientRules.isDesexed(get(patient)));

        // verify stock has reverted to its initial value
        checkStock(stock, new BigDecimal("10"));
    }

    /**
     * Verifies that when an invoice with reminders is reversed, the reminder is retained.
     * TODO - it probably should be deleted or cancelled.
     */
    @Test
    public void testReverseInvoiceWithReminder() {
        CustomerAccountRules rules = getRules();

        Party patient = getPatient();

        // create the invoice
        List<FinancialAct> invoiceActs = createChargesInvoice(new BigDecimal(100));
        FinancialAct invoice = invoiceActs.get(0);
        Act item = invoiceActs.get(1);
        Act reminder = ReminderTestHelper.createReminder(patient, ReminderTestHelper.createReminderType());
        IMObjectBean itemBean = getBean(item);
        ActRelationship relationship = (ActRelationship) itemBean.addTarget("reminders", reminder);
        reminder.addActRelationship(relationship);
        assertEquals(1, item.getSourceActRelationships().size());
        List<IMObject> toSave = new ArrayList<>(invoiceActs);
        toSave.add(reminder);
        save(toSave);

        FinancialAct credit = rules.reverse(invoice, new Date());
        assertTrue(TypeHelper.isA(credit, CREDIT));
        ActBean creditBean = new ActBean(credit);
        List<Act> creditItems = creditBean.getNodeActs("items");
        assertEquals(1, creditItems.size());
        Act creditItem = creditItems.get(0);
        assertEquals(0, creditItem.getSourceActRelationships().size()); // should not have relationships

        item = get(item);
        itemBean = new ActBean(item);
        reminder = get(reminder);
        assertNotNull(reminder);
        assertTrue(itemBean.getTargets("reminders").contains(reminder));
    }

    /**
     * Verifies that a reversal of a payment or refund can be added to the specified till balance.
     */
    @Test
    public void testReverseToSpecifiedTillBalance() {
        Party till = getTill();
        assertNull(tillBalanceRules.getUnclearedBalance(till));
        BigDecimal amount = new BigDecimal(75);
        FinancialAct payment1 = createPaymentCash(amount);
        payment1.setStatus(POSTED);
        save(payment1);
        FinancialAct balance1 = tillBalanceRules.getUnclearedBalance(till);
        assertNotNull(balance1);
        checkEquals(amount, balance1.getTotal());
        balance1.setStatus(IN_PROGRESS);  // i.e. clear in progress
        save(balance1);

        // create a new payment. This should go into a different till balance
        FinancialAct payment2 = createPaymentCash(TEN);
        payment2.setStatus(POSTED);
        save(payment2);
        FinancialAct balance2 = tillBalanceRules.getUnclearedBalance(till);
        assertNotNull(balance2);
        assertNotEquals(balance1.getId(), balance2.getId());
        checkEquals(TEN, balance2.getTotal());

        // now reverse payment1 into balance1. The new total should be zero.
        FinancialAct refund = checkReverse(payment1, REFUND, REFUND_CASH, false, balance1, amount);

        balance1 = get(balance1);
        ActBean bean = new ActBean(balance1);
        assertTrue(bean.hasNodeTarget("items", payment1));
        assertTrue(bean.hasNodeTarget("items", refund));
        checkEquals(ZERO, balance1.getTotal());
    }

    /**
     * Tests the {@link CustomerAccountRules#hasAccountActs(Party)} method.
     */
    @Test
    public void testHasAccountActs() {
        Party customer = getCustomer();
        CustomerAccountRules rules = getRules();
        assertFalse(rules.hasAccountActs(customer));
        save(createChargesInvoice(TEN));
        assertTrue(rules.hasAccountActs(customer));
    }

    /**
     * Verifies a cash payments can be reversed where the {@code rounded amount <> amount}.
     */
    @Test
    public void testReverseCashWithRoundedAmountDifferentToAmount() {
        BigDecimal amount = new BigDecimal("51.54");
        BigDecimal rounded = new BigDecimal("51.55");
        List<FinancialAct> acts = FinancialTestHelper.createPaymentCash(amount, getCustomer(), getTill(), POSTED);
        ActBean item = new ActBean(acts.get(1));
        item.setValue("roundedAmount", rounded);
        item.setValue("tendered", rounded);
        save(acts);
        FinancialAct payment1 = acts.get(0);
        FinancialAct refund = checkReverse(payment1, REFUND, REFUND_CASH, false, null, rounded);
        checkReverse(refund, PAYMENT, PAYMENT_CASH, false, null, rounded);
    }

    /**
     * Verifies that when a refund of a cash payments is reversed, the new tendered amount reflects the rounded amount
     * of the refund, not the original tendered amount.
     * This is because the tendered amount is not preserved in the refund.
     */
    @Test
    public void testReverseCashWithTenderedDifferentToAmount() {
        BigDecimal amount = new BigDecimal("51.54");
        BigDecimal rounded = new BigDecimal("51.55");
        BigDecimal tendered = new BigDecimal("60.00");
        List<FinancialAct> acts = FinancialTestHelper.createPaymentCash(amount, getCustomer(), getTill(), POSTED);
        ActBean item = new ActBean(acts.get(1));
        item.setValue("roundedAmount", rounded);
        item.setValue("tendered", tendered);
        save(acts);
        checkEquals(new BigDecimal("8.45"), item.getBigDecimal("change"));
        FinancialAct payment1 = acts.get(0);
        FinancialAct refund = checkReverse(payment1, REFUND, REFUND_CASH, false, null, rounded);
        FinancialAct payment2 = checkReverse(refund, PAYMENT, PAYMENT_CASH, false, null, rounded);
        ActBean bean = new ActBean(payment2);
        ActBean cash = new ActBean(bean.getNodeActs("items").get(0));

        // verify the tendered amount in the new payment is not that of the original, as the refund doesn't store
        // the information.
        checkEquals(rounded, cash.getBigDecimal("tendered"));
        checkEquals(ZERO, cash.getBigDecimal("change"));
    }

    /**
     * Verifies that customer account acts that are not invoices and credits cannot be created with negative amounts.
     */
    @Test
    public void testNegativeAmountsUnsupported() {
        BigDecimal minusOne = new BigDecimal(-1);
        checkNegativeAmounts(createPayment(minusOne));
        checkNegativeAmounts(createRefund(minusOne));
        checkNegativeAmounts(createCreditAdjust(minusOne));
        checkNegativeAmounts(createDebitAdjust(minusOne));
        checkNegativeAmounts(createInitialBalance(minusOne));
        checkNegativeAmounts(createBadDebt(minusOne));
    }

    /**
     * Verifies that when an invoice is in a gap claim that is not SETTLED, DECLINED, nor CANCELLED, it isn't allocated.
     */
    @Test
    public void testInvoiceInGapClaimWithPayment() {
        checkInvoiceInGapClaim(ClaimStatus.PENDING, createPayment(TEN), false);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is not SETTLED, DECLINED, nor CANCELLED, it isn't allocated.
     */
    @Test
    public void testInvoiceInGapClaimWithCredit() {
        checkInvoiceInGapClaim(ClaimStatus.POSTED, createChargesCredit(TEN), false);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is not SETTLED, DECLINED, nor CANCELLED, it isn't allocated.
     */
    @Test
    public void testInvoiceInGapClaimWithCreditAdjust() {
        checkInvoiceInGapClaim(ClaimStatus.SUBMITTED, createCreditAdjust(TEN), false);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is not SETTLED, DECLINED, nor CANCELLED, it isn't allocated.
     */
    @Test
    public void testInvoiceInGapClaimWithBadDebt() {
        checkInvoiceInGapClaim(ClaimStatus.ACCEPTED, createBadDebt(TEN), false);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is CANCELLED, it is allocated.
     */
    @Test
    public void testInvoiceInCancelledGapClaim() {
        checkInvoiceInGapClaim(ClaimStatus.CANCELLED, createPayment(TEN), true);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is SETTLED, it is allocated.
     */
    @Test
    public void testInvoiceInSettledGapClaim() {
        checkInvoiceInGapClaim(ClaimStatus.SETTLED, createChargesCredit(TEN), true);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is DECLINED, it is allocated.
     */
    @Test
    public void testInvoiceDeclinedGapClaim() {
        checkInvoiceInGapClaim(ClaimStatus.DECLINED, createCreditAdjust(TEN), true);
    }

    /**
     * Tests the {@link CustomerAccountRules#createPaymentOther} method.
     */
    @Test
    public void testCreatePaymentOther() {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();
        Party location = TestHelper.createLocation();
        Party till = getTill();
        TestHelper.getLookup("lookup.customPaymentType", "GAP_BENEFIT_PAYMENT");
        List<FinancialAct> acts = rules.createPaymentOther(customer, TEN, till, location,
                                                           "GAP_BENEFIT_PAYMENT", "some notes");
        assertEquals(2, acts.size());
        FinancialAct payment = acts.get(0);
        FinancialAct item = acts.get(1);
        assertTrue(payment.isA(PAYMENT));
        assertTrue(item.isA(PAYMENT_OTHER));
        assertTrue(payment.isNew());
        assertTrue(item.isNew());
        assertEquals(POSTED, payment.getStatus());
        checkEquals(TEN, payment.getTotal());
        checkEquals(TEN, item.getTotal());

        IMObjectBean paymentBean = getBean(payment);
        assertEquals(customer.getObjectReference(), paymentBean.getTargetRef("customer"));
        assertEquals(till.getObjectReference(), paymentBean.getTargetRef("till"));
        assertEquals("some notes", paymentBean.getString("notes"));

        IMObjectBean itemBean = getBean(item);
        assertEquals("GAP_BENEFIT_PAYMENT", itemBean.getString("paymentType"));

        checkAddToBalance(acts);
    }

    /**
     * Tests the {@link CustomerAccountRules#hasClearedTillBalance(FinancialAct)} method.
     */
    @Test
    public void testHasClearedTillBalance() {
        CustomerAccountRules rules = getRules();
        FinancialAct payment = createPayment(TEN);
        save(payment);
        assertFalse(rules.hasClearedTillBalance(payment));

        IMObjectBean bean = getBean(payment);
        FinancialAct balance = bean.getSource("tillBalance", FinancialAct.class);
        assertNotNull(balance);
        assertEquals(TillBalanceStatus.UNCLEARED, balance.getStatus());

        balance.setStatus(TillBalanceStatus.IN_PROGRESS);
        save(balance);
        assertFalse(rules.hasClearedTillBalance(payment));

        balance.setStatus(TillBalanceStatus.CLEARED);
        save(balance);
        assertTrue(rules.hasClearedTillBalance(payment));
    }

    /**
     * Tests the {@link CustomerAccountRules#post(FinancialAct)} method.
     */
    @Test
    public void testPost() {
        checkBalance(ZERO);
        FinancialAct payment = createPayment(TEN);
        save(payment);
        checkBalance(TEN.negate());

        FinancialAct invoice = createInvoice(DateRules.getYesterday(), TEN, IN_PROGRESS);
        Date now = new Date();
        assertTrue(now.compareTo(invoice.getActivityStartTime()) >= 0);
        getRules().post(invoice);
        invoice = get(invoice);
        assertEquals(POSTED, invoice.getStatus());

        checkEquals(TEN, invoice.getAllocatedAmount());
        payment = get(payment);
        checkEquals(TEN, payment.getAllocatedAmount());
        checkBalance(ZERO);

        // verify the start time has changed. Ignore milliseconds, as they aren't stored.
        assertTrue(DateRules.compareTo(now, invoice.getActivityStartTime(), true) <= 0);

    }

    /**
     * Verifies that when a payment is POSTED via {@link CustomerAccountRules#post(FinancialAct)}, a till balance
     * relationship is created.
     */
    @Test
    public void testPostForPayment() {
        checkPostForPaymentRefund(true);
    }

    /**
     * Verifies that when a refund is POSTED via {@link CustomerAccountRules#post(FinancialAct)}, a till balance
     * relationship is created.
     */
    @Test
    public void testPostForRefund() {
        checkPostForPaymentRefund(false);
    }

    /**
     * Verifies that when a payment or refund is POSTED via {@link CustomerAccountRules#post(FinancialAct)},
     * a till balance relationship is created.
     */
    private void checkPostForPaymentRefund(boolean payment) {
        Party till = getTill();
        assertNull(tillBalanceRules.getUnclearedBalance(till));

        checkBalance(ZERO);
        BigDecimal amount = TEN;
        createInvoice(new Date(), amount);
        checkBalance(amount);
        FinancialAct act;
        if (payment) {
            act = (FinancialAct) accountFactory.newPayment()
                    .customer(getCustomer())
                    .till(getTill())
                    .cash(amount)
                    .status(IN_PROGRESS)
                    .build();
        } else {
            act = (FinancialAct) accountFactory.newRefund()
                    .customer(getCustomer())
                    .till(getTill())
                    .cash(amount)
                    .status(IN_PROGRESS)
                    .build();
        }
        checkBalance(amount);

        assertNull(tillBalanceRules.getUnclearedBalance(till));

        getRules().post(act);

        // verify there is now a till balance with the POSTED act
        FinancialAct balance = tillBalanceRules.getUnclearedBalance(till);
        assertNotNull(balance);
        IMObjectBean bean = getBean(balance);
        assertTrue(bean.hasTarget("items", act));
        checkEquals(payment ? amount : amount.negate(), balance.getTotal());

        // verify the customer balance has updated
        if (payment) {
            checkBalance(ZERO);
        } else {
            checkBalance(BigDecimal.valueOf(20));
        }
    }

    /**
     * Verifies that {@link CustomerAccountRules#post(FinancialAct)} throws an exception for payments with
     * an outstanding EFT transaction, and that it succeeds if the transaction is APPROVED.
     */
    @Test
    public void testPostPaymentForOutstandingEFT() {
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        TestEFTPOSPaymentBuilder eftposPaymentBuilder = new TestEFTPOSPaymentBuilder(getArchetypeService());
        FinancialAct transaction = (FinancialAct) eftposPaymentBuilder.customer(getCustomer())
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct payment = (FinancialAct) accountFactory.newPayment()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .addTransaction(transaction)
                .add()
                .build();
        checkPostForEFTPOS(payment, transaction, EFTPOSTransactionStatus.PENDING, true);
        checkPostForEFTPOS(payment, transaction, EFTPOSTransactionStatus.IN_PROGRESS, true);
        checkPostForEFTPOS(payment, transaction, EFTPOSTransactionStatus.DECLINED, true);
        checkPostForEFTPOS(payment, transaction, EFTPOSTransactionStatus.ERROR, true);
        checkPostForEFTPOS(payment, transaction, EFTPOSTransactionStatus.APPROVED, false);
    }

    /**
     * Verifies that {@link CustomerAccountRules#post(FinancialAct)} throws an exception for refunds with
     * an outstanding EFT transaction, and that it succeeds if the transaction is APPROVED.
     */
    @Test
    public void testPostRefundForOutstandingEFT() {
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        TestEFTPOSRefundBuilder eftposRefundBuilder = new TestEFTPOSRefundBuilder(getArchetypeService());
        FinancialAct transaction = (FinancialAct) eftposRefundBuilder.customer(getCustomer())
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct refund = (FinancialAct) accountFactory.newRefund()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .addTransaction(transaction)
                .add()
                .build();
        checkPostForEFTPOS(refund, transaction, EFTPOSTransactionStatus.PENDING, true);
        checkPostForEFTPOS(refund, transaction, EFTPOSTransactionStatus.IN_PROGRESS, true);
        checkPostForEFTPOS(refund, transaction, EFTPOSTransactionStatus.DECLINED, true);
        checkPostForEFTPOS(refund, transaction, EFTPOSTransactionStatus.ERROR, true);
        checkPostForEFTPOS(refund, transaction, EFTPOSTransactionStatus.APPROVED, false);
    }

    /**
     * Verifies the {@link CustomerAccountRules#post(FinancialAct)} method throws an CustomerAccountRuleException
     * if a transaction is already posted.
     */
    @Test
    public void testPostForPostedTransaction() {
        FinancialAct invoice = createInvoice(new Date(), TEN, POSTED);
        try {
            getRules().post(invoice);
            fail("Expected CustomerAccountRuleException");
        } catch (CustomerAccountRuleException expected) {
            assertEquals(CustomerAccountRuleException.ErrorCode.AlreadyPosted, expected.getErrorCode());
        }
    }

    /**
     * Tests the {@link CustomerAccountRules#hasOutstandingEFTPOSTransaction} method for a payment.
     */
    @Test
    public void testHasOutstandingEFTPOSTransactionForPayment() {
        CustomerAccountRules rules = getRules();

        // create a payment with an EFT transaction
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSPayment().customer(getCustomer())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .terminal(terminal)
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct payment1 = (FinancialAct) accountFactory.newPayment()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .addTransaction(transaction)
                .add()
                .build();
        checkHasOutstandingEFTPOSTransaction(transaction, payment1);

        // verify that a payment with an EFT item but no EFTPOS transaction doesn't have an outstanding EFTPOS
        // transaction
        FinancialAct payment2 = (FinancialAct) accountFactory.newPayment()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .add()
                .build();
        assertFalse(rules.hasOutstandingEFTPOSTransaction(payment2, false));
    }

    /**
     * Tests the {@link CustomerAccountRules#hasOutstandingEFTPOSTransaction} method for a refund.
     */
    @Test
    public void testHasOutstandingEFTPOSTransactionForRefund() {
        CustomerAccountRules rules = getRules();

        // create a refund with an EFT transaction
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSRefund()
                .customer(getCustomer())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .terminal(terminal)
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct refund1 = (FinancialAct) accountFactory.newRefund()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .addTransaction(transaction)
                .add()
                .build();
        checkHasOutstandingEFTPOSTransaction(transaction, refund1);

        // verify that a refund with an EFT item but no EFTPOS transaction doesn't have an outstanding EFFTPOS
        // transaction
        FinancialAct refund2 = (FinancialAct) accountFactory.newRefund()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .add()
                .build();
        assertFalse(rules.hasOutstandingEFTPOSTransaction(refund2, false));
    }

    /**
     * Tests the {@link CustomerAccountRules#hasApprovedEFTPOSTransaction} method for a payment.
     */
    @Test
    public void testHasApprovedEFTPOSTransactionForPayment() {
        // create a payment with an EFT transaction
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSPayment()
                .customer(getCustomer())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .terminal(terminal)
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct payment = (FinancialAct) accountFactory.newPayment()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .addTransaction(transaction)
                .add()
                .build();

        checkHasSuccessfulEFTPOSTransaction(transaction, payment);
    }

    /**
     * Tests the {@link CustomerAccountRules#hasApprovedEFTPOSTransaction} method for a refund.
     */
    @Test
    public void testHasApprovedEFTPOSTransactionForRefund() {
        // create a payment with an EFT transaction
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = (FinancialAct) accountFactory.newEFTPOSRefund()
                .customer(getCustomer())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .terminal(terminal)
                .status(EFTPOSTransactionStatus.PENDING)
                .build();
        FinancialAct refund = (FinancialAct) accountFactory.newRefund()
                .customer(getCustomer())
                .till(getTill())
                .status(IN_PROGRESS)
                .eft()
                .amount(TEN)
                .addTransaction(transaction)
                .add()
                .build();

        checkHasSuccessfulEFTPOSTransaction(transaction, refund);
    }

    /**
     * Tests the {@link CustomerAccountRules#hasApprovedEFTPOSTransaction} method.
     *
     * @param eft the EFTPOS transaction
     * @param act the parent payment/refund
     */
    private void checkHasSuccessfulEFTPOSTransaction(FinancialAct eft, FinancialAct act) {
        CustomerAccountRules rules = getRules();
        assertFalse(rules.hasApprovedEFTPOSTransaction(act));

        eft.setStatus(EFTPOSTransactionStatus.IN_PROGRESS);
        save(eft);
        assertFalse(rules.hasApprovedEFTPOSTransaction(act));

        eft.setStatus(EFTPOSTransactionStatus.DECLINED);
        save(eft);
        assertFalse(rules.hasApprovedEFTPOSTransaction(act));

        eft.setStatus(EFTPOSTransactionStatus.ERROR);
        save(eft);
        assertFalse(rules.hasApprovedEFTPOSTransaction(act));

        eft.setStatus(EFTPOSTransactionStatus.NO_TERMINAL);
        save(eft);
        assertFalse(rules.hasApprovedEFTPOSTransaction(act));

        eft.setStatus(EFTPOSTransactionStatus.APPROVED);
        save(eft);
        assertTrue(rules.hasApprovedEFTPOSTransaction(act));
    }

    /**
     * Tests the {@link CustomerAccountRules#hasOutstandingEFTPOSTransaction} method.
     *
     * @param eft the EFTPOS transaction
     * @param act the parent payment/refund
     */
    private void checkHasOutstandingEFTPOSTransaction(FinancialAct eft, FinancialAct act) {
        CustomerAccountRules rules = getRules();

        // verify that it has an outstanding EFTPOS transaction, until the status is APPROVED
        assertTrue(rules.hasOutstandingEFTPOSTransaction(act, false));
        assertTrue(rules.hasOutstandingEFTPOSTransaction(act, true));

        eft.setStatus(EFTPOSTransactionStatus.IN_PROGRESS);
        save(eft);
        assertTrue(rules.hasOutstandingEFTPOSTransaction(act, false));
        assertTrue(rules.hasOutstandingEFTPOSTransaction(act, true));

        eft.setStatus(EFTPOSTransactionStatus.DECLINED);
        save(eft);
        assertTrue(rules.hasOutstandingEFTPOSTransaction(act, false));
        assertFalse(rules.hasOutstandingEFTPOSTransaction(act, true));

        eft.setStatus(EFTPOSTransactionStatus.ERROR);
        save(eft);
        assertTrue(rules.hasOutstandingEFTPOSTransaction(act, false));
        assertFalse(rules.hasOutstandingEFTPOSTransaction(act, true));

        eft.setStatus(EFTPOSTransactionStatus.APPROVED);
        save(eft);
        assertFalse(rules.hasOutstandingEFTPOSTransaction(act, false));
        assertFalse(rules.hasOutstandingEFTPOSTransaction(act, true));
    }

    /**
     * Verifies that {@link CustomerAccountRules#post(FinancialAct)} throws an exception for payments and refunds with
     * an outstanding EFT transaction, and that it succeeds if the transaction is APPROVED.
     *
     * @param act         the payment/refund
     * @param transaction the EFTPOS transaction
     * @param status      the EFTPOS transaction status to set
     * @param fail        if {@code true}, expect post() to fail, otherwise expect it to succeed
     */
    private void checkPostForEFTPOS(FinancialAct act, FinancialAct transaction, String status, boolean fail) {
        transaction.setStatus(status);
        save(transaction);
        try {
            getRules().post(act);
            if (fail) {
                fail("Expected post() to fail");
            }
            act = get(act);
            assertEquals(POSTED, act.getStatus());
        } catch (CustomerAccountRuleException exception) {
            if (!fail) {
                fail("Expected post() to succeed");
            }
            assertEquals(CustomerAccountRuleException.ErrorCode.CannotPostWithOutstandingEFT, exception.getErrorCode());
            act = get(act);
            assertEquals(IN_PROGRESS, act.getStatus());
        }
    }

    /**
     * Checks the {@link CustomerAccountRules#getOverdueBalance} methods for customer account acts.
     *
     * @param acts     the customer account acts
     * @param balance  the expected balance after the acts are saved
     * @param expected the expected overdue balance
     */
    private void checkOverdueBalance(List<FinancialAct> acts, BigDecimal balance, BigDecimal expected) {
        CustomerAccountRules rules = getRules();

        // add a 30 day payment term for accounts to the customer
        Party customer = getCustomer();
        customer.addClassification(createAccountType(30, DateUnits.DAYS));
        save(customer);

        Date startTime = getDate("2007-01-01");
        acts.get(0).setActivityStartTime(startTime);
        save(acts);

        checkEquals(balance, rules.getBalance(customer));

        // verify it is not overdue on the same date
        checkEquals(ZERO, rules.getOverdueBalance(customer, startTime));

        Date statementDate = DateRules.getDate(startTime, 32, DateUnits.DAYS);

        // 30 days from saved, amount shouldn't be overdue
        Date days30 = DateRules.getDate(startTime, 30, DateUnits.DAYS);
        checkEquals(ZERO, rules.getOverdueBalance(customer, days30));
        checkEquals(ZERO, rules.getOverdueBalance(customer, statementDate,
                                                  rules.getOverdueDate(customer, days30)));

        // verify that is is overdue 31 days after
        Date overdueDate = DateRules.getDate(startTime, 31, DateUnits.DAYS);
        checkEquals(expected, rules.getOverdueBalance(customer, overdueDate));

        checkEquals(expected, rules.getOverdueBalance(customer, statementDate,
                                                      rules.getOverdueDate(customer, overdueDate)));
    }

    /**
     * Verifies a charge with a negative total can be reversed.
     *
     * @param archetype             the charge archetype
     * @param itemArchetype         the charge item archetype
     * @param reversalArchetype     the reversal archetype
     * @param reversalItemArchetype the reversal item archetype
     */
    private void checkReversalOfNegativeCharge(String archetype, String itemArchetype,
                                               String reversalArchetype, String reversalItemArchetype) {
        Product product1 = getProduct();
        Party patient = !itemArchetype.equals(COUNTER_ITEM) ? getPatient() : null;

        BigDecimal minusOne = ONE.negate();
        FinancialAct item1 = FinancialTestHelper.createChargeItem(itemArchetype, patient, null, product1,
                                                                  minusOne, TEN, ONE, ZERO, ZERO);

        List<FinancialAct> charge = FinancialTestHelper.createCharges(archetype, getCustomer(), null, POSTED, item1);
        save(charge);
        FinancialAct reversal = checkReverse(charge.get(0), reversalArchetype, false, null);
        IMObjectBean bean = getBean(reversal);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());

        FinancialTestHelper.checkItem(items.get(0), reversalItemArchetype, patient, product1, null, -1, null, null,
                                      ZERO, minusOne, ZERO, ONE, ZERO, TEN, ZERO, ZERO, BigDecimal.valueOf(-11), true);
    }

    /**
     * Verifies a charge with a line item with a negative quantity can be reversed.
     *
     * @param archetype             the charge archetype
     * @param itemArchetype         the charge item archetype
     * @param reversalArchetype     the reversal archetype
     * @param reversalItemArchetype the reversal item archetype
     */
    private void checkReversalOfChargeWithNegativeQuantity(String archetype, String itemArchetype,
                                                           String reversalArchetype, String reversalItemArchetype) {
        Product product1 = getProduct();
        Product product2 = TestHelper.createProduct();
        Party patient = !itemArchetype.equals(COUNTER_ITEM) ? getPatient() : null;

        FinancialAct item1 = FinancialTestHelper.createChargeItem(itemArchetype, patient, null, product1,
                                                                  ONE, TEN, ONE, ZERO, ZERO);
        BigDecimal minusOne = ONE.negate();
        FinancialAct item2 = FinancialTestHelper.createChargeItem(itemArchetype, patient, null, product2,
                                                                  minusOne, ONE, ONE, ZERO, ZERO);

        List<FinancialAct> charge = FinancialTestHelper.createCharges(archetype, getCustomer(), null, POSTED, item1,
                                                                      item2);
        save(charge);
        FinancialAct reversal = checkReverse(charge.get(0), reversalArchetype, false, null);
        IMObjectBean bean = getBean(reversal);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        FinancialAct item1Reversal = FinancialTestHelper.find(items, patient, product1);
        FinancialAct item2Reversal = FinancialTestHelper.find(items, patient, product2);

        FinancialTestHelper.checkItem(item1Reversal, reversalItemArchetype, patient, product1, null, -1, null, null,
                                      ZERO, ONE, ZERO, ONE, ZERO, TEN, ZERO, ZERO, BigDecimal.valueOf(11), true);
        FinancialTestHelper.checkItem(item2Reversal, reversalItemArchetype, patient, product2, null, -1, null, null,
                                      ZERO, minusOne, ZERO, ONE, ZERO, ONE, ZERO, ZERO, BigDecimal.valueOf(-2), true);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is not SETTLED, DECLINED, nor CANCELLED, it isn't allocated.
     *
     * @param status    the claim status
     * @param credit    the credit act
     * @param allocated if {@code true} expect the invoice to be allocated, otherwise expect its allocation to be zero
     */
    private void checkInvoiceInGapClaim(String status, FinancialAct credit, boolean allocated) {
        checkInvoiceInGapClaim(status, Collections.singletonList(credit), allocated);
    }

    /**
     * Verifies that when an invoice is in a gap claim that is not SETTLED, DECLINED, nor CANCELLED, it isn't allocated.
     *
     * @param status    the claim status
     * @param credit    the credit acts
     * @param allocated if {@code true} expect the invoice to be allocated, otherwise expect its allocation to be zero
     */
    private void checkInvoiceInGapClaim(String status, List<FinancialAct> credit, boolean allocated) {
        List<FinancialAct> acts = createChargesInvoice(MathRules.ONE_HUNDRED);
        save(acts);
        FinancialAct invoiceItem = acts.get(1);

        Party location = TestHelper.createLocation();
        Party customer = getCustomer();
        Act policy = (Act) InsuranceTestHelper.createPolicy(customer, getPatient(),
                                                            InsuranceTestHelper.createInsurer(), "12345");
        save(policy);

        FinancialAct claimItem = (FinancialAct) InsuranceTestHelper.createClaimItem(invoiceItem);
        save(claimItem);
        FinancialAct claim = (FinancialAct) InsuranceTestHelper.createClaim(
                policy, location, TestHelper.createClinician(), TestHelper.createUser(), true, claimItem);
        claim.setStatus(status);
        save(claim);

        save(credit);

        FinancialAct invoice = get(acts.get(0));

        if (allocated) {
            checkEquals(credit.get(0).getAllocatedAmount(), invoice.getAllocatedAmount());
        } else {
            checkEquals(ZERO, invoice.getAllocatedAmount());
        }
    }

    /**
     * Verifies an opening balance act matches that expected.
     *
     * @param act      the act to check
     * @param customer the expected customer
     * @param date     the expected start time
     * @param total    the expected total
     * @param credit   the expected credit flag
     */
    private void checkOpeningBalance(FinancialAct act, Party customer, Date date, BigDecimal total, boolean credit) {
        checkBalanceAct(act, OPENING_BALANCE, customer, date, total, credit);
    }

    /**
     * Verifies a closing balance act matches that expected.
     *
     * @param act      the act to check
     * @param customer the expected customer
     * @param date     the expected start time
     * @param total    the expected total
     * @param credit   the expected credit flag
     */
    private void checkClosingBalance(FinancialAct act, Party customer, Date date, BigDecimal total, boolean credit) {
        checkBalanceAct(act, CLOSING_BALANCE, customer, date, total, credit);
    }

    /**
     * Verifies an opening/closing balance act matches that expected.
     *
     * @param act       the act to check
     * @param archetype the expected archetype
     * @param customer  the expected customer
     * @param date      the expected start time
     * @param total     the expected total
     * @param credit    the expected credit flag
     */
    private void checkBalanceAct(FinancialAct act, String archetype, Party customer, Date date, BigDecimal total,
                                 boolean credit) {
        assertTrue(act.isA(archetype));
        IMObjectBean bean = getBean(act);
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(date, act.getActivityStartTime());
        assertNull(act.getActivityEndTime());
        checkEquals(total, act.getTotal());
        assertEquals(credit, act.isCredit());
    }

    /**
     * Verifies that a customer account act cannot be created with a negative amount.
     *
     * @param act the act to check
     */
    private void checkNegativeAmounts(FinancialAct act) {
        checkNegativeAmounts(Collections.singletonList(act));
    }

    /**
     * Verifies that a customer account act cannot be created with a negative amount.
     *
     * @param acts the acts to check
     */
    private void checkNegativeAmounts(List<FinancialAct> acts) {
        try {
            save(acts);
            fail("Expected save of negative " + acts.get(0).getArchetypeId().getShortName() + " to fail");
        } catch (ValidationException expected) {
            List<ValidationError> errors = expected.getErrors();
            assertEquals(1, errors.size());
            String message = errors.get(0).getMessage();
            assertTrue("Value must be >= 0.0".equals(message) || "Value must be > 0.0".equals(message));
        }
    }

    /**
     * Verifies that when an act is saved, a <em>participation.customerAccountBalance</em> is associated with it.
     * <p>
     * This ensures that the:
     * <ul>
     * <li>customer has no prior account acts before saving</li>
     * <li>has account acts after saving</li>
     * <li>{@link CustomerBalanceUpdater#checkInitialBalance(FinancialAct)} is invoked to reject initial
     * balances being saved when other account acts are present</li>
     * </ul>
     *
     * @param acts the acts. The first act is the parent charge/credit
     */
    private void checkAddToBalance(List<FinancialAct> acts) {
        CustomerAccountRules rules = getRules();
        Party customer = getCustomer();
        IMObjectBean bean = getBean(acts.get(0));
        assertTrue(bean.getValues("accountBalance").isEmpty());
        save(acts);
        Act act = get(acts.get(0));
        bean = new ActBean(act);
        assertEquals(getCustomer(), bean.getTarget("accountBalance"));
        assertTrue(rules.hasAccountActs(customer));

        try {
            save(createInitialBalance(TEN));
            fail("Expected save of Initial Balance to fail");
        } catch (RuleEngineException expected) {
            Throwable rootCause = ExceptionUtils.getRootCause(expected);
            assertTrue(rootCause instanceof CustomerAccountRuleException);
            CustomerAccountRuleException cause = (CustomerAccountRuleException) rootCause;
            assertEquals(CustomerAccountRuleException.ErrorCode.CannotCreateInitialBalance, cause.getErrorCode());
        }
    }

    /**
     * Verifies that when an act is saved, a <em>participation.customerAccountBalance</em> is associated with it.
     *
     * @param act the act
     */
    private void checkAddToBalance(FinancialAct act) {
        checkAddToBalance(Collections.singletonList(act));
    }

    /**
     * Verifies that a debit is offset by a credit of the same amount.
     *
     * @param debit  the debit act
     * @param credit the credit act
     */
    private void checkCalculateBalanceForSameAmount(FinancialAct debit, FinancialAct credit) {
        checkCalculateBalanceForSameAmount(Collections.singletonList(debit), Collections.singletonList(credit));
    }

    /**
     * Verifies that a debit is offset by a credit of the same amount.
     *
     * @param debits  the debit acts
     * @param credits the credit act
     */
    private void checkCalculateBalanceForSameAmount(List<FinancialAct> debits, List<FinancialAct> credits) {
        FinancialAct debit = debits.get(0);
        FinancialAct credit = credits.get(0);

        BigDecimal amount = credit.getTotal();
        checkEquals(amount, debit.getTotal());

        assertTrue(credit.isCredit());
        assertFalse(debit.isCredit());

        // save and reload the debit. The allocated amount should be unchanged
        save(debits);
        debit = get(debit);
        checkEquals(amount, debit.getTotal());
        checkEquals(ZERO, debit.getAllocatedAmount());
        checkAllocation(debit);

        // check the outstanding balance
        checkBalance(amount);

        // save the credit. The allocated amount for the debit and credit should
        // be the same as the total
        save(credits);

        credit = get(credit);
        debit = get(debit);

        checkEquals(amount, credit.getTotal());
        checkEquals(amount, credit.getAllocatedAmount());
        checkAllocation(credit, debit);

        checkEquals(amount, debit.getTotal());
        checkEquals(amount, debit.getAllocatedAmount());
        checkAllocation(debit, credit);

        // check the outstanding balance
        checkBalance(ZERO);
    }

    /**
     * Verifies that the customer account balance matches that expected.
     *
     * @param amount the expected amount
     */
    private void checkBalance(BigDecimal amount) {
        CustomerAccountRules rules = getRules();

        Party customer = getCustomer();
        checkEquals(amount, rules.getBalance(customer));
        checkEquals(amount, rules.getDefinitiveBalance(customer));
    }

    /**
     * Verifies the total amount allocated to an act matches that of the
     * amounts from the associated
     * <em>actRelationship.customerAccountAllocation</em> relationships.
     *
     * @param act  the act
     * @param acts the acts contributing to the allocated amount
     */
    private void checkAllocation(FinancialAct act, FinancialAct... acts) {
        IMObjectBean bean = getBean(act);
        BigDecimal total = ZERO;
        List<ActRelationship> allocations = bean.getValues("allocation", ActRelationship.class);
        List<FinancialAct> matches = new ArrayList<>();
        for (ActRelationship relationship : allocations) {
            if (act.isCredit()) {
                assertEquals(act.getObjectReference(),
                             relationship.getTarget());
                for (FinancialAct source : acts) {
                    if (source.getObjectReference().equals(
                            relationship.getSource())) {
                        matches.add(source);
                        break;
                    }
                }
            } else {
                assertEquals(act.getObjectReference(),
                             relationship.getSource());
                for (FinancialAct target : acts) {
                    if (target.getObjectReference().equals(
                            relationship.getTarget())) {
                        matches.add(target);
                        break;
                    }
                }
            }
            IMObjectBean relBean = getBean(relationship);
            BigDecimal allocation = relBean.getBigDecimal("allocatedAmount");
            total = total.add(allocation);
        }
        checkEquals(act.getAllocatedAmount(), total);

        assertEquals(acts.length, matches.size());
    }

    /**
     * Verifies that an invoice can be reversed by {@link CustomerAccountRules#reverse}.
     */
    private void checkReverseInvoice() {
        List<FinancialAct> invoice = createChargesInvoice(new BigDecimal(100));

        // link an estimate to simulate an estimate being invoiced
        Act estimateItem = EstimateTestHelper.createEstimateItem(getPatient(), getProduct(), new BigDecimal(100));
        Act estimate = EstimateTestHelper.createEstimate(getCustomer(), estimateItem);
        IMObjectBean bean = getBean(estimate);
        bean.addTarget("invoice", invoice.get(0), "estimates");
        List<Act> toSave = new ArrayList<>(invoice);
        toSave.add(estimate);
        toSave.add(estimateItem);

        // check reversal
        checkReverseCharge(invoice, toSave, CREDIT, CREDIT_ITEM);
    }

    /**
     * Verifies that an act can be reversed by
     * {@link CustomerAccountRules#reverse}.
     *
     * @param act       the act to reverse
     * @param shortName the reversal act short name
     */
    private void checkReverse(FinancialAct act, String shortName) {
        checkReverse(act, shortName, null, false, null, null);
    }

    /**
     * Verifies that a charge can be reversed by {@link CustomerAccountRules#reverse}.
     * <p>
     * This ensures that stock is updated appropriately.
     *
     * @param acts          the acts to reverse
     * @param shortName     the reversal act short name
     * @param itemShortName the reversal act child short name
     */
    private void checkReverseCharge(List<FinancialAct> acts, String shortName, String itemShortName) {
        checkReverseCharge(acts, new ArrayList<>(acts), shortName, itemShortName);
    }

    /**
     * Verifies that a charge can be reversed by {@link CustomerAccountRules#reverse}.
     * <p>
     * This ensures that stock is updated appropriately.
     *
     * @param acts          the acts to reverse
     * @param toSave        the charges and related acts to save
     * @param shortName     the reversal act short name
     * @param itemShortName the reversal act child short name
     */
    private void checkReverseCharge(List<FinancialAct> acts, List<Act> toSave, String shortName, String itemShortName) {
        BigDecimal quantity = new BigDecimal("10");
        IMObjectRelationship relationship = initStockQuantity(quantity);

        // save the charge and related acts
        save(toSave);

        // verify the stock quantity has updated
        if (!acts.get(0).isCredit()) {
            checkStock(relationship, quantity.subtract(ONE));
        } else {
            checkStock(relationship, quantity.add(ONE));
        }

        // reverse the charge
        checkReverse(acts.get(0), shortName, itemShortName, false, null, null);

        // ensure the stock has gone back to its initial value
        checkStock(relationship, quantity);
    }

    /**
     * Initialises the quantity for the product and stock location.
     *
     * @param quantity the quantity
     * @return the <em>entityLink.productStockLocation</em> relationship
     */
    private IMObjectRelationship initStockQuantity(BigDecimal quantity) {
        Product product = get(getProduct());
        Party stockLocation = get(getStockLocation());
        return ProductTestHelper.setStockQuantity(product, stockLocation, quantity);
    }

    /**
     * Verifies the quantity of stock matches that expected.
     *
     * @param relationship the <em>entityLink.productStockLocation</em>
     * @param expected     the expected quantity
     */
    private void checkStock(IMObjectRelationship relationship, BigDecimal expected) {
        relationship = get(relationship);
        assertNotNull(relationship);
        IMObjectBean bean = getBean(relationship);
        checkEquals(expected, bean.getBigDecimal("quantity"));
    }

    /**
     * Verifies that an act can be reversed by {@link CustomerAccountRules#reverse}, and has the correct child act,
     * if any, and that the {@code hide} flag of the original and reversed transactions match that expected.
     *
     * @param act           the act to reverse
     * @param shortName     the reversal act short name
     * @param itemShortName the reversal act child short name.
     * @param hide          if {@code true}, set the hide flag on both the original and reversed transactions
     * @param tillBalance   the till balance to add the reversal to. Only applies to payments and refunds.
     *                      May be {@code null}
     * @param roundedAmount the expected rounded amount, for cash payments/refunds, otherwise {@code null}
     * @return the reversal
     */
    private FinancialAct checkReverse(FinancialAct act, String shortName, String itemShortName, boolean hide,
                                      FinancialAct tillBalance, BigDecimal roundedAmount) {
        FinancialAct reversal = checkReverse(act, shortName, hide, tillBalance);
        IMObjectBean bean = getBean(reversal);
        if (itemShortName != null) {
            List<Act> items = bean.getTargets("items", Act.class);
            assertEquals(1, items.size());
            Act item = items.get(0);
            assertTrue(item.isA(itemShortName));
            if (item.isA(PAYMENT_CASH, REFUND_CASH)) {
                ActBean itemBean = new ActBean(item);
                checkEquals(roundedAmount, itemBean.getBigDecimal("roundedAmount"));
            }
            checkEquals(act.getTotal(), ((FinancialAct) item).getTotal());
        } else {
            if (bean.hasNode("items")) {
                List<Act> items = bean.getTargets("items", Act.class);
                assertEquals(0, items.size());
            }
        }

        return reversal;
    }

    /**
     * Verifies that an act can be reversed by {@link CustomerAccountRules#reverse}.
     *
     * @param act         the act to reverse
     * @param archetype   the reversal act archetype
     * @param hide        if {@code true}, set the hide flag on both the original and reversed transactions
     * @param tillBalance the till balance to add the reversal to. Only applies to payments and refunds.
     *                    May be {@code null}
     * @return the reversal
     */
    private FinancialAct checkReverse(FinancialAct act, String archetype, boolean hide, FinancialAct tillBalance) {
        CustomerAccountRules rules = getRules();
        BigDecimal currentBalance = rules.getBalance(getCustomer());

        BigDecimal amount = act.getTotal();
        BigDecimal preBalance;
        BigDecimal reverseBalance;
        BigDecimal change = act.isCredit() ? amount.negate() : amount;

        if (!act.isNew()) {
            // already factored into the balance
            preBalance = currentBalance;
            reverseBalance = currentBalance.subtract(change);
        } else {
            preBalance = currentBalance.add(change);
            reverseBalance = currentBalance;
            act.setStatus(POSTED);
            save(act);
        }

        // check the balance
        checkBalance(preBalance);

        Date now = new Date();
        FinancialAct reversal = rules.reverse(act, now, "Test reversal", null, hide, tillBalance);
        assertTrue(reversal.isA(archetype));
        IMObjectBean bean = getBean(reversal);

        assertTrue(rules.isReversed(act));
        assertFalse(rules.isReversed(reversal));

        // verify the two acts have a relationship
        IMObjectBean original = getBean(act);
        assertTrue(original.hasTarget("reversal", reversal));

        // check the notes and reference
        assertEquals("Test reversal", bean.getString("notes"));
        assertEquals(Long.toString(act.getId()), bean.getString("reference"));

        // check the hide flags
        assertEquals(hide, original.getBoolean("hide"));
        assertEquals(hide, bean.getBoolean("hide"));

        // check the balance
        checkBalance(reverseBalance);
        return reversal;
    }

    /**
     * Creates and saves a POSTED invoice with the specified amount and start time.
     *
     * @param startTime the start time
     * @param amount    the amount
     */
    private void createInvoice(Date startTime, BigDecimal amount) {
        createInvoice(startTime, amount, POSTED);
    }

    /**
     * Helper to create an invoice with the specified start time, amount and status.
     *
     * @param startTime the start time
     * @param amount    the amount
     * @param status    the status
     * @return the invoice
     */
    private FinancialAct createInvoice(Date startTime, BigDecimal amount, String status) {
        List<FinancialAct> invoices = createChargesInvoice(amount);
        FinancialAct invoice = invoices.get(0);
        invoice.setActivityStartTime(startTime);
        invoice.setStatus(status);
        save(invoices);
        return invoice;
    }

    /**
     * Helper to create a credit with the specified start time and status.
     *
     * @param startTime the start time
     * @param status    the status
     * @return the invoice
     */
    private FinancialAct createCredit(Date startTime, String status) {
        List<FinancialAct> acts = createChargesCredit(TEN);
        FinancialAct invoice = acts.get(0);
        invoice.setActivityStartTime(startTime);
        invoice.setStatus(status);
        save(acts);
        return invoice;
    }

    /**
     * Creates and saves a POSTED credit with the specified amount and start time.
     *
     * @param amount    the amount
     * @param startTime the start time
     */
    private void createCredit(BigDecimal amount, Date startTime) {
        List<FinancialAct> credits = createChargesCredit(amount);
        FinancialAct credit = credits.get(0);
        credit.setActivityStartTime(startTime);
        save(credits);
    }
}
