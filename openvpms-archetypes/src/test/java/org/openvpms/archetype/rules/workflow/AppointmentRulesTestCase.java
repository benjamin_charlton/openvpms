/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestAppointmentBuilder;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.util.DateRules.getDate;
import static org.openvpms.archetype.rules.util.DateUnits.HOURS;
import static org.openvpms.archetype.rules.util.DateUnits.MINUTES;
import static org.openvpms.archetype.rules.util.DateUnits.MONTHS;
import static org.openvpms.archetype.rules.util.DateUnits.YEARS;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link AppointmentRules} class.
 *
 * @author Tim Anderson
 */
public class AppointmentRulesTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The appointment rules.
     */
    private AppointmentRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new AppointmentRules(getArchetypeService());
    }

    /**
     * Tests the {@link AppointmentRules#getSlotSize(org.openvpms.component.model.entity.Entity)} method.
     */
    @Test
    public void testGetSlotSize() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        assertEquals(15, rules.getSlotSize(schedule));
    }

    /**
     * Tests the {@link AppointmentRules#getDefaultAppointmentType} method.
     */
    @Test
    public void testGetDefaultAppointmentType() {
        Entity appointmentType1 = createAppointmentType();
        Entity appointmentType2 = createAppointmentType();
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .slotSize(15, MINUTES)
                .build();
        assertNull(rules.getDefaultAppointmentType(schedule));

        schedulingFactory.updateSchedule(schedule)
                .addAppointmentType(appointmentType1, 2, false)
                .build();

        // no default appointment type
        assertNull(rules.getDefaultAppointmentType(schedule));

        schedulingFactory.updateSchedule(schedule)
                .addAppointmentType(appointmentType2, 2, true)
                .build();
        assertEquals(rules.getDefaultAppointmentType(schedule), appointmentType2);
    }

    /**
     * Tests the behaviour of {@link AppointmentRules#calculateEndTime} when
     * the schedule units are in minutes .
     */
    @Test
    public void testCalculateEndTimeForMinsUnits() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        Date start = getDatetime("2006-08-22 09:00");
        Date end = rules.calculateEndTime(start, schedule, appointmentType);
        Date expected = getDatetime("2006-08-22 09:30");
        assertEquals(expected, end);
    }

    /**
     * Tests the behaviour of {@link AppointmentRules#calculateEndTime} when
     * the schedule units are in hours.
     */
    @Test
    public void testCalculateEndTimeForHoursUnits() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(1, HOURS, 3, appointmentType);
        Date start = getDatetime("2006-08-22 09:00");
        Date end = rules.calculateEndTime(start, schedule, appointmentType);
        Date expected = getDatetime("2006-08-22 12:00");
        assertEquals(expected, end);
    }

    /**
     * Verifies that the status of a task associated with an appointment is updated when the appointment is saved.
     * <p>
     * Note that this requires the <em>archetypeService.save.act.customerAppointment.after</em> rule in order
     * to invoke the {@link AppointmentRules#updateTask(org.openvpms.component.business.domain.im.act.Act)}
     * method.
     */
    @Test
    public void testUpdateTaskStatus() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        Act appointment = createAppointment("2006-08-22 09:00", schedule, appointmentType);

        Act task = createTask();
        schedulingFactory.updateAppointment(appointment)
                .status(AppointmentStatus.IN_PROGRESS)
                .task(task)
                .build();

        task = get(task);
        assertEquals(ActStatus.IN_PROGRESS, task.getStatus());

        checkStatus(appointment, AppointmentStatus.PENDING, task, TaskStatus.IN_PROGRESS); // no change
        checkStatus(appointment, AppointmentStatus.CONFIRMED, task, TaskStatus.IN_PROGRESS); // no change
        checkStatus(appointment, AppointmentStatus.CHECKED_IN, task, TaskStatus.IN_PROGRESS);
        checkStatus(appointment, AppointmentStatus.IN_PROGRESS, task, TaskStatus.IN_PROGRESS);
        checkStatus(appointment, AppointmentStatus.BILLED, task, TaskStatus.BILLED);
        checkStatus(appointment, AppointmentStatus.COMPLETED, task, TaskStatus.COMPLETED);
        checkStatus(appointment, AppointmentStatus.CANCELLED, task, TaskStatus.CANCELLED);
        checkStatus(appointment, AppointmentStatus.NO_SHOW, task, TaskStatus.CANCELLED);
    }

    /**
     * Verifies that the status of an appointment associated with a task is updated when the task is saved.
     * <p>
     * Note that this requires the <em>archetypeService.save.act.customerTask.after</em> rule, in order to invoke
     * {@link AppointmentRules#updateAppointment(org.openvpms.component.business.domain.im.act.Act)} method.
     */
    @Test
    public void testUpdateAppointmentStatus() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        Act appointment = createAppointment("2006-08-22 09:00", schedule, appointmentType);
        assertEquals(AppointmentStatus.PENDING, appointment.getStatus());

        Act task = createTask();
        schedulingFactory.updateAppointment(appointment)
                .status(AppointmentStatus.IN_PROGRESS)
                .task(task)
                .build();

        checkStatus(task, TaskStatus.PENDING, appointment, AppointmentStatus.IN_PROGRESS);
        checkStatus(task, TaskStatus.IN_PROGRESS, appointment, AppointmentStatus.IN_PROGRESS);
        checkStatus(task, TaskStatus.BILLED, appointment, AppointmentStatus.BILLED);
        checkStatus(task, TaskStatus.COMPLETED, appointment, AppointmentStatus.COMPLETED);
        checkStatus(task, TaskStatus.CANCELLED, appointment, AppointmentStatus.CANCELLED);
    }

    /**
     * Tests the {@link AppointmentRules#getActivePatientAppointment(Party)} method.
     */
    @Test
    public void testGetActivePatientAppointment() {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);

        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);

        assertNull(rules.getActivePatientAppointment(patient));

        TestAppointmentBuilder builder = schedulingFactory.newAppointment()
                .startTime("2022-07-14 09:00")
                .endTime("2022-07-14 09:15")
                .customer(customer)
                .schedule(schedule)
                .appointmentType(appointmentType);
        Act appointment1 = builder.patient(patient).build();

        assertNull(rules.getActivePatientAppointment(patient));
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.CONFIRMED, null);
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.CHECKED_IN, appointment1);
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.IN_PROGRESS, appointment1);
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.BILLED, appointment1);
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.CANCELLED, null);
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.NO_SHOW, null);
        checkActivePatientAppointment(patient, appointment1, AppointmentStatus.COMPLETED, appointment1);

        Act appointment2 = builder.startTime("2023-07-14 11:00")
                .endTime("2023-07-14 11:15")
                .build();
        assertEquals(appointment1, rules.getActivePatientAppointment(patient));
        checkActivePatientAppointment(patient, appointment2, AppointmentStatus.CONFIRMED, appointment1);
        checkActivePatientAppointment(patient, appointment2, AppointmentStatus.CHECKED_IN, appointment2);
    }

    /**
     * Tests the {@link AppointmentRules#getActiveCustomerAppointments(Party, Party, Act)} method.
     */
    @Test
    public void testGetActiveCustomerAppointments() {
        Party customer = customerFactory.createCustomer();
        Party patientA = patientFactory.createPatient(customer);
        Party patientB = patientFactory.createPatient(customer);
        Party patientC = patientFactory.createPatient(customer);

        Entity appointmentType = createAppointmentType();
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Entity schedule1 = createSchedule(15, MINUTES, 2, appointmentType, location1);
        Entity schedule2 = createSchedule(15, MINUTES, 2, appointmentType, location2);

        TestAppointmentBuilder builder = schedulingFactory.newAppointment()
                .startTime("2022-07-14 09:00")
                .customer(customer)
                .appointmentType(appointmentType);
        Act appointmentA = builder.schedule(schedule1).patient(patientA).build();
        Act appointmentB = builder.schedule(schedule1).patient(patientB).build();
        Act appointmentC = builder.schedule(schedule2).patient(patientC).build();
        assertEquals(AppointmentStatus.PENDING, appointmentA.getStatus());
        assertEquals(AppointmentStatus.PENDING, appointmentA.getStatus());

        // all appointments are inactive, so should be excluded
        checkActiveCustomerAppointments(customer, location1, null);
        checkActiveCustomerAppointments(customer, location2, null);

        setStatus(appointmentA, AppointmentStatus.CONFIRMED);
        setStatus(appointmentC, AppointmentStatus.CONFIRMED);

        checkActiveCustomerAppointments(customer, location1, null);
        checkActiveCustomerAppointments(customer, location2, null);

        // check active statuses
        setStatus(appointmentA, AppointmentStatus.CHECKED_IN);
        setStatus(appointmentB, AppointmentStatus.IN_PROGRESS);
        setStatus(appointmentC, AppointmentStatus.IN_PROGRESS);
        checkActiveCustomerAppointments(customer, location1, null, appointmentA, appointmentB);
        checkActiveCustomerAppointments(customer, location2, null, appointmentC);

        setStatus(appointmentA, AppointmentStatus.ADMITTED);
        setStatus(appointmentB, AppointmentStatus.BILLED);
        checkActiveCustomerAppointments(customer, location1, null, appointmentA, appointmentB);
        checkActiveCustomerAppointments(customer, location2, null, appointmentC);

        // check exclusion
        checkActiveCustomerAppointments(customer, location1, appointmentB, appointmentA);

        // verify when all location1 appointments are inactive, they are not returned
        setStatus(appointmentA, AppointmentStatus.COMPLETED);
        setStatus(appointmentB, AppointmentStatus.CANCELLED);
        setStatus(appointmentC, AppointmentStatus.NO_SHOW);
        checkActiveCustomerAppointments(customer, location1, null);
        checkActiveCustomerAppointments(customer, location2, null);
    }

    /**
     * Tests the {@link AppointmentRules#copy(Act)} method.
     */
    @Test
    public void testCopy() {
        Date arrival = getDatetime("2006-08-22 08:55");
        Date start = getDatetime("2006-08-22 09:00");
        Date end = getDatetime("2006-08-22 09:15");
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        User clinician = userFactory.createClinician();

        Act task = createTask();

        Act appointment = schedulingFactory.newAppointment()
                .startTime(start)
                .endTime(end)
                .schedule(schedule)
                .appointmentType(appointmentType)
                .customer(customer)
                .patient(patient)
                .clinician(clinician)
                .status(AppointmentStatus.IN_PROGRESS)
                .arrivalTime(arrival)
                .notes("some notes")
                .task(task)
                .build();

        // now copy it, and verify the task relationship isn't copied
        Act copy = rules.copy(appointment);
        assertTrue(copy.isA(ScheduleArchetypes.APPOINTMENT));
        assertTrue(copy.getActRelationships().isEmpty());

        assertTrue(copy.isNew());        // shouldn't be saved
        IMObjectBean copyBean = getBean(copy);
        assertEquals(0, DateRules.compareTo(start, copy.getActivityStartTime()));
        assertEquals(0, DateRules.compareTo(end, copy.getActivityEndTime()));
        assertEquals(schedule, copyBean.getTarget("schedule"));
        assertEquals(customer, copyBean.getTarget("customer"));
        assertEquals(patient, copyBean.getTarget("patient"));
        assertEquals(clinician, copyBean.getTarget("clinician"));
        assertEquals(appointmentType, copyBean.getTarget("appointmentType"));
        assertEquals(AppointmentStatus.IN_PROGRESS, copy.getStatus());
        assertEquals(0, DateRules.compareTo(arrival, copyBean.getDate("arrivalTime")));
        assertEquals("some notes", copyBean.getString("notes"));
        assertEquals(appointment.getReason(), copyBean.getString("reason"));
    }

    /**
     * Tests the {@link AppointmentRules#getScheduleView} method.
     */
    @Test
    public void testGetScheduleView() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();

        Entity scheduleA = createSchedule(location1);
        Entity scheduleB = createSchedule(location1);
        Entity scheduleC = createSchedule(location2);
        Entity scheduleD = createSchedule(location2);

        Entity view1 = schedulingFactory.createScheduleView(scheduleA, scheduleB);
        Entity view2 = schedulingFactory.createScheduleView(scheduleC);

        practiceFactory.updateLocation(location1)
                .scheduleViews(view1)
                .build();
        practiceFactory.updateLocation(location2)
                .scheduleViews(view2)
                .build();

        assertEquals(view1, rules.getScheduleView(location1, scheduleA));
        assertEquals(view1, rules.getScheduleView(location1, scheduleB));
        assertEquals(view2, rules.getScheduleView(location2, scheduleC));

        assertNull(rules.getScheduleView(location2, scheduleA));
        assertNull(rules.getScheduleView(location2, scheduleB));
        assertNull(rules.getScheduleView(location2, scheduleA));
        assertNull(rules.getScheduleView(location1, scheduleD));
        assertNull(rules.getScheduleView(location2, scheduleD));
    }

    /**
     * Tests the {@link AppointmentRules#getLocation} method.
     */
    @Test
    public void testGetLocation() {
        Party location1 = practiceFactory.createLocation();
        Entity scheduleA = createSchedule(location1);
        assertEquals(location1, rules.getLocation(scheduleA));
    }

    /**
     * Tests the {@link AppointmentRules#isRemindersEnabled} method.
     */
    @Test
    public void testIsRemindersEnabled() {
        // check schedule support
        Entity schedule = createSchedule(practiceFactory.createLocation());
        assertFalse(rules.isRemindersEnabled(schedule));

        schedulingFactory.updateSchedule(schedule)
                .sendReminders(true)
                .build();

        assertTrue(rules.isRemindersEnabled(schedule));

        // check appointment type support
        Entity appointmentType = schedulingFactory.createAppointmentType();
        assertFalse(rules.isRemindersEnabled(appointmentType));

        schedulingFactory.updateAppointmentType(appointmentType)
                .sendReminders(true)
                .build();
        assertTrue(rules.isRemindersEnabled(appointmentType));

        // check null
        assertFalse(rules.isRemindersEnabled(null));
    }

    /**
     * Tests the {@link AppointmentRules#getNoReminderPeriod()}.
     */
    @Test
    public void testGetNoReminderPeriod() {
        Entity job = create(AppointmentRules.APPOINTMENT_REMINDER_JOB, Entity.class);
        AppointmentRules rules = new AppointmentRules(getArchetypeService()) {
            @Override
            protected IMObject getAppointmentReminderJob() {
                return (IMObject) job;
            }
        };
        assertEquals(Period.days(2), rules.getNoReminderPeriod());
    }

    /**
     * Tests the  {@link AppointmentRules#isBoardingAppointment} method.
     */
    @Test
    public void testIsBoardingAppointment() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        Act appointment = createAppointment("2006-08-22 09:00", schedule, appointmentType);

        assertFalse(rules.isBoardingAppointment(appointment));

        // now add a cage type
        schedulingFactory.updateSchedule(schedule)
                .cageType(schedulingFactory.createCageType())
                .build();

        assertTrue(rules.isBoardingAppointment(appointment));
    }

    /**
     * Tests the {@link AppointmentRules#getBoardingDays} methods.
     */
    @Test
    public void testGetBoardingDays() {
        checkGetDays(1, "2016-03-23 10:00:00", "2016-03-23 17:00:00"); // same day
        checkGetDays(1, "2016-03-23 10:00:00", "2016-03-24 00:00:00"); // ends at midnight, so considered the same day
        checkGetDays(2, "2016-03-23 10:00:00", "2016-03-24 09:00:00"); // less than 24 hours
        checkGetDays(2, "2016-03-23 10:00:00", "2016-03-24 10:00:00"); // 24 hours
        checkGetDays(2, "2016-03-23 10:00:00", "2016-03-24 17:00:00"); // more than 24 hours
        checkGetDays(3, "2016-03-23 10:00:00", "2016-03-25 09:00:00"); // less than 48 hours
        checkGetDays(3, "2016-03-23 10:00:00", "2016-03-25 10:00:00"); // 48 hours
        checkGetDays(3, "2016-03-23 10:00:00", "2016-03-25 17:00:00"); // more than 48 hours

        checkGetDays(0, "2016-03-23 10:00:00", "2016-03-20 17:00:00"); // future dated being checked out now?
    }

    /**
     * Tests the {@link AppointmentRules#getBoardingNights(Date, Date)} method.
     */
    @Test
    public void testGetBoardingNights() {
        checkGetNights(1, "2016-03-23 10:00:00", "2016-03-23 17:00:00"); // same day
        checkGetNights(1, "2016-03-23 10:00:00", "2016-03-24 00:00:00"); // ends at midnight, so considered the same day
        checkGetNights(1, "2016-03-23 10:00:00", "2016-03-24 09:00:00"); // less than 24 hours
        checkGetNights(1, "2016-03-23 10:00:00", "2016-03-24 10:00:00"); // 24 hours
        checkGetNights(1, "2016-03-23 10:00:00", "2016-03-24 17:00:00"); // more than 24 hours
        checkGetNights(2, "2016-03-23 10:00:00", "2016-03-25 09:00:00"); // less than 48 hours
        checkGetNights(2, "2016-03-23 10:00:00", "2016-03-25 10:00:00"); // 48 hours
        checkGetNights(2, "2016-03-23 10:00:00", "2016-03-25 17:00:00"); // more than 48 hours

        checkGetNights(0, "2016-03-23 10:00:00", "2016-03-20 17:00:00"); // future dated being checked out now?
    }

    /**
     * Test the {@link AppointmentRules#getEvent(Act)} method.
     */
    @Test
    public void testGetEvent() {
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(15, MINUTES, 2, appointmentType);
        Act appointment = createAppointment("2006-08-22 09:00", schedule, appointmentType);

        assertNull(rules.getEvent(appointment));
        IMObjectBean bean = getBean(appointment);

        Act event = PatientTestHelper.createEvent(bean.getTarget("patient", Party.class));
        schedulingFactory.updateAppointment(appointment)
                .event(event)
                .build();

        assertEquals(event, rules.getEvent(appointment));
    }

    /**
     * Tests the {@link AppointmentRules#getPendingCustomerAppointments} and
     * {@link AppointmentRules#getPendingPatientAppointments} methods.
     */
    @Test
    public void testGetPendingAppointments() {
        Party customer1 = customerFactory.createCustomer();
        Party patient1a = patientFactory.createPatient(customer1);
        Party patient1b = patientFactory.createPatient(customer1);
        Party customer2 = customerFactory.createCustomer();
        Party patient2 = patientFactory.createPatient();
        Party location = practiceFactory.createLocation();
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(location, appointmentType);
        Date now = new Date();
        Act act1a = createAppointment(getDate(now, -1, HOURS), schedule, appointmentType, customer1, patient1a);
        Act act1b = createAppointment(getDate(now, 6, MONTHS), schedule, appointmentType, customer1, patient1b);
        Act act1c = createAppointment(getDate(now, 9, MONTHS), schedule, appointmentType, customer1, null);
        Act act1d = createAppointment(getDate(now, 2, YEARS), schedule, appointmentType, customer1, patient1a);
        Act act2a = createAppointment(getDate(now, -1, YEARS), schedule, appointmentType, customer2, patient2);
        Act act2b = createAppointment(getDate(now, 1, MONTHS), schedule, appointmentType, customer2, patient2);
        Act act2c = createAppointment(getDate(now, 6, MONTHS), schedule, appointmentType, customer2, patient2);
        Act act2d = createAppointment(getDate(now, 9, MONTHS), schedule, appointmentType, customer2, patient2);

        act1b.setStatus(AppointmentStatus.CONFIRMED);
        act2b.setStatus(AppointmentStatus.CANCELLED);
        act2d.setStatus(AppointmentStatus.NO_SHOW);
        save(act1b, act2b, act2d);

        // act1a and act2a should never be returned as they are dated before now
        assertTrue(act1a.getActivityStartTime().compareTo(now) < 0);
        assertTrue(act2a.getActivityStartTime().compareTo(now) < 0);

        checkAppointments(rules.getPendingCustomerAppointments(customer1, 1, YEARS), act1b, act1c);
        // statuses are CONFIRMED and PENDING respectively

        checkAppointments(rules.getPendingPatientAppointments(patient1a, 3, YEARS), act1d); // PENDING
        checkAppointments(rules.getPendingPatientAppointments(patient1b, 1, YEARS), act1b); // CONFIRMED
        checkAppointments(rules.getPendingCustomerAppointments(customer2, 1, YEARS), act2c); // PENDING
    }

    /**
     * Tests the {@link AppointmentRules#getNextPatientAppointment} method.
     */
    @Test
    public void testGetNextPatientAppointment() {
        Party customer1 = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer1);
        Party patient2 = patientFactory.createPatient(customer1);
        Party location = practiceFactory.createLocation();
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(location, appointmentType);
        Date start1 = DateRules.getYesterday();
        Date start2 = DateRules.getToday();
        Date start3 = DateRules.getTomorrow();
        Date before1 = getDate(start1, -1, MINUTES);
        Date after1 = getDate(start1, 1, MINUTES);
        Date after2 = getDate(start2, 1, MINUTES);
        Date after3 = getDate(start3, 1, MINUTES);

        Act act1 = createAppointment(start1, schedule, appointmentType, customer1, patient1);
        setStatus(act1, AppointmentStatus.CONFIRMED);
        Act act2 = createAppointment(start2, schedule, appointmentType, customer1, patient1);
        Act act3 = createAppointment(start3, schedule, appointmentType, customer1, patient1);

        assertEquals(act1, rules.getNextPatientAppointment(patient1, before1));
        assertEquals(act1, rules.getNextPatientAppointment(patient1, start1));
        assertEquals(act2, rules.getNextPatientAppointment(patient1, after1));
        assertEquals(act2, rules.getNextPatientAppointment(patient1, start2));
        assertEquals(act3, rules.getNextPatientAppointment(patient1, after2));
        assertEquals(act3, rules.getNextPatientAppointment(patient1, start3));
        assertNull(rules.getNextPatientAppointment(patient1, after3));

        // verify that only PENDING/CONFIRMED appointments are considered
        String[] statuses = {AppointmentStatus.CANCELLED, AppointmentStatus.CHECKED_IN, AppointmentStatus.ADMITTED,
                             AppointmentStatus.IN_PROGRESS, AppointmentStatus.ADMITTED, AppointmentStatus.COMPLETED,
                             AppointmentStatus.NO_SHOW};
        for (String status : statuses) {
            setStatus(act1, status);
            assertEquals(act2, rules.getNextPatientAppointment(patient1, before1));
            assertEquals(act2, rules.getNextPatientAppointment(patient1, start1));
        }

        // verify that if the patient has no appointments, nothing is returned
        assertNull(rules.getNextPatientAppointment(patient2, before1));
    }

    /**
     * Tests the {@link AppointmentRules#getSlotTime} method.
     */
    @Test
    public void testGetSlotTime() {
        Date date1 = getDatetime("2015-03-05 09:00:00");
        assertEquals(date1, rules.getSlotTime(date1, 15, false));
        assertEquals(date1, rules.getSlotTime(date1, 15, true));

        Date date2 = getDatetime("2015-03-05 09:05:00");
        assertEquals(getDatetime("2015-03-05 09:00:00"), rules.getSlotTime(date2, 15, false));
        assertEquals(getDatetime("2015-03-05 09:15:00"), rules.getSlotTime(date2, 15, true));

        Date date3 = getDatetime("2015-03-05 12:15:00");
        assertEquals(getDatetime("2015-03-05 12:00:00"), rules.getSlotTime(date3, 30, false));
        assertEquals(getDatetime("2015-03-05 12:30:00"), rules.getSlotTime(date3, 30, true));

        // check daylight saving (at least in AEST). See OVPMS-2210
        Date date4 = getDatetime("2019-10-06 01:00:00");
        assertEquals(getDatetime("2019-10-06 00:00:00"), rules.getSlotTime(date4, 24 * 60, false));
        assertEquals(getDatetime("2019-10-07 00:00:00"), rules.getSlotTime(date4, 24 * 60, true));

        Date date5 = getDatetime("2020-04-05 01:00:00");
        assertEquals(getDatetime("2020-04-05 00:00:00"), rules.getSlotTime(date5, 24 * 60, false));
        assertEquals(getDatetime("2020-04-06 00:00:00"), rules.getSlotTime(date5, 24 * 60, true));
    }

    /**
     * Tests the {@link AppointmentRules#getOverlap(Date, Date, org.openvpms.component.model.entity.Entity)} method.
     */
    @Test
    public void testGetOverlap() {
        Party location = practiceFactory.createLocation();
        Entity appointmentType = createAppointmentType();
        Entity schedule = createSchedule(location, appointmentType);
        Date start = getDatetime("2020-05-16 09:00");  // appointment start
        Date end = getDatetime("2020-05-16 09:30");    // appointment end
        Date before = getDatetime("2020-05-16 08:30"); // before appointment start
        Date during = getDatetime("2020-05-16 09:15"); // during appointment
        Date after = getDatetime("2020-05-16 09:45");  // after appointment ebd

        checkOverlap(start, end, schedule, null);
        Act appointment = createAppointment("2020-05-16 09:00", schedule, appointmentType);
        save(appointment);

        checkOverlap(before, start, schedule, null);         // no overlap
        checkOverlap(end, after, schedule, null);            // no overlap
        checkOverlap(start, end, schedule, appointment);     // exact overlap
        checkOverlap(start, during, schedule, appointment);  // overlap from start
        checkOverlap(during, end, schedule, appointment);    // overlap to end
        checkOverlap(before, during, schedule, appointment); // overlaps start
        checkOverlap(during, after, schedule, appointment);  // overlaps end

        // verify there is no overlap when the appointment is cancelled
        setStatus(appointment, AppointmentStatus.CANCELLED);
        checkOverlap(before, start, schedule, null);
        checkOverlap(end, after, schedule, null);
        checkOverlap(start, end, schedule, null);
        checkOverlap(start, during, schedule, null);
        checkOverlap(during, end, schedule, null);
        checkOverlap(before, during, schedule, null);
        checkOverlap(during, after, schedule, null);
    }

    /**
     * Sets the status of an appointment and verifies the {@link AppointmentRules#getActivePatientAppointment(Party)}
     * method returns the expected appointment.
     *
     * @param patient     the patient
     * @param appointment the appointment to update
     * @param newStatus   the new appointment status
     * @param expected    the expected appointment
     */
    private void checkActivePatientAppointment(Party patient, Act appointment, String newStatus, Act expected) {
        setStatus(appointment, newStatus);
        assertEquals(expected, rules.getActivePatientAppointment(patient));
    }

    /**
     * Verifies active appointments for a customer.
     *
     * @param customer the customer
     * @param location the practice location
     * @param exclude  the appointment to exclude. May be {@code null}
     * @param expected the expected active appointments
     */
    private void checkActiveCustomerAppointments(Party customer, Party location, Act exclude, Act... expected) {
        List<org.openvpms.component.business.domain.im.act.Act> matches
                = IterableUtils.toList(rules.getActiveCustomerAppointments(customer, location, exclude));
        assertEquals(matches, Arrays.asList(expected));
    }

    /**
     * Sets an act status and saves it.
     *
     * @param act    the act
     * @param status the act status
     */
    private void setStatus(Act act, String status) {
        act.setStatus(status);
        save(act);
    }

    /**
     * Verifies that the correct overlap times are returned.
     *
     * @param startTime the start of the date range
     * @param endTime   the end of the date range
     * @param schedule  the schedule
     * @param overlap   the overlapping appointment, or {@code null} if there is none
     */
    private void checkOverlap(Date startTime, Date endTime, Entity schedule, Act overlap) {
        Times times = rules.getOverlap(startTime, endTime, schedule);
        if (overlap == null) {
            assertNull(times);
        } else {
            assertEquals(overlap.getActivityStartTime(), times.getStartTime());
            assertEquals(overlap.getActivityEndTime(), times.getEndTime());
            assertEquals(overlap.getObjectReference(), times.getReference());
        }
    }

    /**
     * Verifies that {@link AppointmentRules#getBoardingDays(Date, Date)} and
     * {@link AppointmentRules#getBoardingDays(Act)} return the expected no. of days.
     *
     * @param expected  the expected no. of days
     * @param startTime the boarding start time
     * @param endTime   the boarding end time
     */
    private void checkGetDays(int expected, String startTime, String endTime) {
        Date start = getDatetime(startTime);
        Date end = getDatetime(endTime);
        assertEquals(expected, rules.getBoardingDays(start, end));

        Act appointment = schedulingFactory.newAppointment().startTime(start).endTime(end).build(false);
        assertEquals(expected, rules.getBoardingDays(appointment));
    }

    /**
     * Verifies that {@link AppointmentRules#getBoardingNights(Date, Date)} method returns the expected no. of nights.
     *
     * @param expected  the expected no. of days
     * @param startTime the boarding start time
     * @param endTime   the boarding end time
     */
    private void checkGetNights(int expected, String startTime, String endTime) {
        Date start = getDatetime(startTime);
        Date end = getDatetime(endTime);
        assertEquals(expected, rules.getBoardingNights(start, end));
    }

    /**
     * Verifies appointments match those expected.
     *
     * @param iterable an iterable over the appointments
     * @param expected the expected appointments
     */
    private void checkAppointments(Iterable<org.openvpms.component.business.domain.im.act.Act> iterable,
                                   Act... expected) {
        List<Act> result = new ArrayList<>();
        CollectionUtils.addAll(result, iterable);
        assertEquals(expected.length, result.size());
        for (int i = 0; i < expected.length; ++i) {
            assertEquals(expected[i], result.get(i));
        }
    }

    /**
     * Helper to create a {@code PENDING} <em>act.customerAppointment</em>.
     *
     * @param startTime       the appointment start time
     * @param schedule        the schedule
     * @param appointmentType the appointment type
     * @return a new act
     */
    private Act createAppointment(String startTime, Entity schedule, Entity appointmentType) {
        return createAppointment(getDatetime(startTime), schedule, appointmentType);
    }

    /**
     * Helper to create a {@code PENDING} <em>act.customerAppointment</em>.
     *
     * @param startTime       the appointment start time
     * @param schedule        the schedule
     * @param appointmentType the appointment type
     * @return a new act
     */
    private Act createAppointment(Date startTime, Entity schedule, Entity appointmentType) {
        return schedulingFactory.newAppointment()
                .startTime(startTime)
                .schedule(schedule)
                .appointmentType(appointmentType)
                .customer(customerFactory.createCustomer())
                .patient(patientFactory.createPatient())
                .build();
    }

    /**
     * Helper to create a pending 15 minute appointment.
     *
     * @param startTime       the appointment start time
     * @param schedule        the schedule
     * @param appointmentType the appointment type
     * @param customer        the customer
     * @param patient         the patient
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Entity schedule, Entity appointmentType, Party customer,
                                  Party patient) {
        return schedulingFactory.newAppointment()
                .startTime(startTime)
                .schedule(schedule)
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customer)
                .patient(patient)
                .build();
    }

    /**
     * Helper to create a <em>act.customerTask</em>.
     *
     * @return a new act
     */
    private Act createTask() {
        Entity taskType = createTaskType();
        Party customer = customerFactory.createCustomer();
        Entity workList = schedulingFactory.createWorkList();
        return schedulingFactory.newTask()
                .status(TaskStatus.PENDING)
                .startTime(new Date())
                .endTime(new Date())
                .taskType(taskType)
                .customer(customer)
                .workList(workList)
                .build();
    }

    /**
     * Helper to create a new <em>entity.appointmentType</em>.
     *
     * @return a new appointment type
     */
    private Entity createAppointmentType() {
        return schedulingFactory.createAppointmentType();
    }

    /**
     * Helper to create a new <em>entity.taskType</em>.
     *
     * @return a new task type
     */
    private Entity createTaskType() {
        return schedulingFactory.createTaskType();
    }

    /**
     * Helper to create a <code>party.organisationSchedule</em>.
     *
     * @param slotSize        the schedule slot size
     * @param slotUnits       the schedule slot units
     * @param noSlots         the appointment no. of slots
     * @param appointmentType the appointment type
     * @return a new schedule
     */
    private Entity createSchedule(int slotSize, DateUnits slotUnits, int noSlots, Entity appointmentType) {
        return createSchedule(slotSize, slotUnits, noSlots, appointmentType, practiceFactory.createLocation());
    }

    /**
     * Helper to create a <code>party.organisationSchedule</em>.
     *
     * @param slotSize        the schedule slot size
     * @param slotUnits       the schedule slot units
     * @param noSlots         the appointment no. of slots
     * @param appointmentType the appointment type
     * @param location        the practice location
     * @return a new schedule
     */
    private Entity createSchedule(int slotSize, DateUnits slotUnits, int noSlots, Entity appointmentType,
                                  Party location) {
        return schedulingFactory.newSchedule()
                .location(location)
                .slotSize(slotSize, slotUnits)
                .addAppointmentType(appointmentType, noSlots, true)
                .build();
    }

    /**
     * Creates a schedule for a practice location.
     *
     * @param location the practice location
     * @return a new schedule
     */
    private Entity createSchedule(Party location) {
        return createSchedule(location, createAppointmentType());
    }

    /**
     * Creates a schedule with a single appointment type for a practice location.
     *
     * @param location        the practice location
     * @param appointmentType the appointment type
     * @return a new schedule
     */
    private Entity createSchedule(Party location, Entity appointmentType) {
        return createSchedule(15, MINUTES, 2, appointmentType, location);
    }

    /**
     * Checks the status of a linked act (an <em>act.customerAppointment</em>
     * or <em>act.customerTask</em> when its linker is saved.
     *
     * @param source         the source act
     * @param status         the status to set
     * @param linked         the linked act
     * @param expectedStatus the expected linked act status
     */
    private void checkStatus(Act source, String status, Act linked,
                             String expectedStatus) {
        source.setStatus(status);
        linked = get(linked);    // ensure using the latest version
        Date endTime = linked.getActivityEndTime();
        try {
            // force a sleep to ensure end times are different to check
            // correct updates
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {
            // do nothing.
        }
        save(source);

        // reload the linked act to get any new status
        linked = get(linked);
        assertNotNull(linked);
        assertEquals(expectedStatus, linked.getStatus());

        // for completed acts where the linked act is a task, expect the
        // endTimes to be different
        if (TypeHelper.isA(linked, ScheduleArchetypes.TASK)) {
            if (WorkflowStatus.COMPLETED.equals(expectedStatus)) {
                // end time should be > than before
                assertTrue(linked.getActivityEndTime().compareTo(endTime) > 0);
            } else {
                assertEquals(endTime, linked.getActivityEndTime());
            }
        }
    }
}
