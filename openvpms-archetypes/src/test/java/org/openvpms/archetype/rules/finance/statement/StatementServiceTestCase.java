/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.statement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;

/**
 * Tests the {@link StatementService}.
 * <p/>
 * NOTE: most functionality is covered by {@link EndOfPeriodProcessorTestCase}.
 *
 * @author Tim Anderson
 */
public class StatementServiceTestCase extends AbstractStatementTest {

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules accountRules;

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The statement rules.
     */
    private StatementRules rules;

    /**
     * The statement service.
     */
    private StatementService statementService;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        Party practice = getPractice();
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getPractice()).thenReturn(practice);
        statementService = new StatementService((IArchetypeRuleService) getArchetypeService(), accountRules,
                                                laboratoryRules, practiceService);
        rules = new StatementRules(practice, getArchetypeService(), accountRules);
    }

    /**
     * Verifies that the {@link StatementService#endPeriod(Party, Date, boolean)} must be yesterday or before.
     */
    @Test
    public void testEndPeriodStatementDate() {
        Party customer = (Party) customerFactory.createCustomer();
        try {
            statementService.endPeriod(customer, DateRules.getToday(), true);
            fail("Expected StatementProcessorException to be thrown");
        } catch (StatementProcessorException expected) {
            assertEquals(StatementProcessorException.ErrorCode.InvalidStatementDate, expected.getErrorCode());
        }

        try {
            statementService.endPeriod(customer, DateRules.getTomorrow(), true);
            fail("Expected StatementProcessorException to be thrown");
        } catch (StatementProcessorException expected) {
            assertEquals(StatementProcessorException.ErrorCode.InvalidStatementDate, expected.getErrorCode());
        }

        Date yesterday = DateRules.getYesterday();
        statementService.endPeriod(customer, DateRules.getPreviousDate(yesterday), true);

        statementService.endPeriod(customer, yesterday, true);
    }

    /**
     * Tests end of period.
     * <p/>
     * On completion, the following acts should be present:
     * <ul>
     * <li>1/1/2007 invoice 100.00
     * <li>1/1/2007 closing balance -100.00
     * <li>1/1/2007 opening balance 100.00
     * <li>2/1/2007 invoice 100.00
     * </ul>
     */
    @Test
    public void testEndOfPeriod() {
        // 30 days account fee days i.e. 30 days before overdue fees are generated
        Lookup accountType = FinancialTestHelper.createAccountType(30, DateUnits.DAYS, new BigDecimal("10.00"), 30);
        Party customer = (Party) customerFactory.newCustomer()
                .addClassifications(accountType)
                .build();

        Date statementDate = getDate("2007-01-01");

        assertFalse(rules.hasStatement(customer, statementDate));
        List<Act> acts = getActs(customer, statementDate);
        assertEquals(0, acts.size());

        BigDecimal amount = new BigDecimal(100);
        List<FinancialAct> invoice1 = createChargesInvoice(amount, customer, getDatetime("2007-01-01 10:00:00"));
        save(invoice1);

        acts = getActs(customer, statementDate);
        assertEquals(1, acts.size());
        checkAct(acts.get(0), invoice1.get(0), POSTED);

        statementService.endPeriod(customer, statementDate, true);

        assertTrue(rules.hasStatement(customer, statementDate));
        acts = getActs(customer, statementDate);
        assertEquals(2, acts.size());
        checkAct(acts.get(0), invoice1.get(0), POSTED);
        checkClosingBalance(acts.get(1), amount, BigDecimal.ZERO);

        // verify more acts aren't generated for the same statement date
        statementService.endPeriod(customer, statementDate, true);
        acts = getActs(customer, statementDate);
        assertEquals(2, acts.size());

        // save a new invoice after the statement date
        List<FinancialAct> invoice2 = createChargesInvoice(amount, customer, getDatetime("2007-01-02 10:00:00"));
        save(invoice2);

        // verify it doesn't appear in the statement
        acts = getActs(customer, statementDate);
        assertEquals(2, acts.size());

        statementDate = getDate("2007-01-02");
        assertFalse(rules.hasStatement(customer, statementDate));

        acts = getActs(customer, statementDate);
        assertEquals(2, acts.size());
        checkOpeningBalance(acts.get(0), new BigDecimal("100"));
        checkAct(acts.get(1), invoice2.get(0), POSTED);
    }
}