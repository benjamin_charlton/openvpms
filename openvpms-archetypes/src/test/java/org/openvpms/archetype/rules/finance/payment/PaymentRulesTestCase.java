/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.payment;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.AbstractCustomerAccountTest;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.service.ruleengine.RuleEngineException;
import org.openvpms.component.model.act.FinancialAct;
import org.springframework.beans.factory.annotation.Autowired;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * Tests the {@link PaymentRules} class when invoked by
 * <em>archetypeService.remove.act.customerAccountPayment.before.drl</em>,
 * <em>archetypeService.remove.act.customerAccountPaymentEFT.before.drl</em>,
 * <em>archetypeService.remove.act.customerAccountRefund.before.drl</em>, and
 * <em>archetypeService.remove.act.customerAccountRefundEFT.before.drl</em> rules.
 * <br/>
 * In order for these tests to be successful, the archetype service must be configured to trigger the above rules.
 *
 * @author Tim Anderson
 */
public class PaymentRulesTestCase extends AbstractCustomerAccountTest {

    /**
     * The test account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The test practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies that {@link PaymentRules#removePaymentRefund(FinancialAct)} throws an {@code IllegalStateException}
     * if the payment is POSTED.
     */
    @Test
    public void testRemovePaymentRefundForPostedPayment() {
        FinancialAct payment = createPayment(accountFactory.newCashPaymentItem()
                                                     .amount(TEN)
                                                     .build(false));
        checkRemovePaymentRefundForPostedAct(payment);
    }

    /**
     * Verifies that {@link PaymentRules#removePaymentRefund(FinancialAct)} throws an {@code IllegalStateException}
     * if the refund is POSTED.
     */
    @Test
    public void testRemovePaymentRefundForPostedRefund() {
        FinancialAct refund = createRefund(accountFactory.newCashRefundItem()
                                                   .amount(TEN)
                                                   .build(false));
        checkRemovePaymentRefundForPostedAct(refund);
    }

    /**
     * Verifies that when a payment is removed, its items are removed also.
     * <p/>
     * This occurs due to the the parenChild flag being set {@code true} in each of the
     * <em>actRelationship.customerAccountPaymentItem</em> relationships.
     */
    @Test
    public void testRemovePayment() {
        FinancialAct item = accountFactory.newCashPaymentItem()
                .amount(TEN)
                .build(false);
        FinancialAct payment = createPayment(item);
        assertNotNull(get(payment));
        assertNotNull(get(item));
        remove(payment);
        assertNull(get(payment));
        assertNull(get(item));
    }

    /**
     * Verifies that when a refund is removed, its items are removed also.
     * <p/>
     * This occurs due to the the parenChild flag being set {@code true} in each of the
     * <em>actRelationship.customerAccountRefundItem</em> relationships.
     */
    @Test
    public void testRemoveRefund() {
        FinancialAct item = accountFactory.newCashRefundItem()
                .amount(TEN)
                .build(false);
        FinancialAct refund = createRefund(item);
        assertNotNull(get(refund));
        assertNotNull(get(item));
        remove(refund);
        assertNull(get(refund));
        assertNull(get(item));
    }

    /**
     * Verifies that when a payment is removed with an associated EFT item, only those EFT transactions with
     * NO_TERMINAL status are removed.
     */
    @Test
    public void testRemovePaymentWithEFTTransactions() {
        checkRemovePaymentWithEFT(EFTPOSTransactionStatus.PENDING, false);
        checkRemovePaymentWithEFT(EFTPOSTransactionStatus.IN_PROGRESS, false);
        checkRemovePaymentWithEFT(EFTPOSTransactionStatus.APPROVED, false);
        checkRemovePaymentWithEFT(EFTPOSTransactionStatus.DECLINED, false);
        checkRemovePaymentWithEFT(EFTPOSTransactionStatus.ERROR, false);
        checkRemovePaymentWithEFT(EFTPOSTransactionStatus.NO_TERMINAL, true);
    }

    /**
     * Verifies that when an EFT payment item is removed, only those EFT transactions with NO_TERMINAL status are
     * removed.
     */
    @Test
    public void testRemoveEFTPaymentItem() {
        checkRemoveEFTPaymentItem(EFTPOSTransactionStatus.PENDING, false);
        checkRemoveEFTPaymentItem(EFTPOSTransactionStatus.IN_PROGRESS, false);
        checkRemoveEFTPaymentItem(EFTPOSTransactionStatus.APPROVED, false);
        checkRemoveEFTPaymentItem(EFTPOSTransactionStatus.DECLINED, false);
        checkRemoveEFTPaymentItem(EFTPOSTransactionStatus.ERROR, false);
        checkRemoveEFTPaymentItem(EFTPOSTransactionStatus.NO_TERMINAL, true);
    }

    /**
     * Verifies that when a refund is removed with an associated EFT item, only those EFT transactions with
     * NO_TERMINAL status are removed.
     */
    @Test
    public void testRemoveRefundWithEFTTransactions() {
        checkRemoveRefundWithEFT(EFTPOSTransactionStatus.PENDING, false);
        checkRemoveRefundWithEFT(EFTPOSTransactionStatus.IN_PROGRESS, false);
        checkRemoveRefundWithEFT(EFTPOSTransactionStatus.APPROVED, false);
        checkRemoveRefundWithEFT(EFTPOSTransactionStatus.DECLINED, false);
        checkRemoveRefundWithEFT(EFTPOSTransactionStatus.ERROR, false);
        checkRemoveRefundWithEFT(EFTPOSTransactionStatus.NO_TERMINAL, true);
    }

    /**
     * Verifies that when an EFT refund item is removed, only those EFT transactions with NO_TERMINAL status are
     * removed.
     */
    @Test
    public void testRemoveEFTRefundItem() {
        checkRemoveEFTRefundItem(EFTPOSTransactionStatus.PENDING, false);
        checkRemoveEFTRefundItem(EFTPOSTransactionStatus.IN_PROGRESS, false);
        checkRemoveEFTRefundItem(EFTPOSTransactionStatus.APPROVED, false);
        checkRemoveEFTRefundItem(EFTPOSTransactionStatus.DECLINED, false);
        checkRemoveEFTRefundItem(EFTPOSTransactionStatus.ERROR, false);
        checkRemoveEFTRefundItem(EFTPOSTransactionStatus.NO_TERMINAL, true);
    }

    /**
     * Verifies that {@link PaymentRules#removePaymentRefund(FinancialAct)} throws an {@code IllegalStateException}
     * if the act is POSTED.
     *
     * @param act the act
     */
    private void checkRemovePaymentRefundForPostedAct(FinancialAct act) {
        act.setStatus(FinancialActStatus.POSTED);
        save(act);
        try {
            remove(act);
            fail("Expected remove to fail");
        } catch (RuleEngineException expected) {
            Throwable cause = ExceptionUtils.getRootCause(expected.getCause());
            assertEquals("Act is POSTED", cause.getMessage());
        }
    }

    /**
     * Verifies that when a payment is removed with an associated EFT item, only those EFT transactions with
     * NO_TERMINAL status are removed.
     *
     * @param status        the EFT transaction status
     * @param expectDeleted if {@code true}, expect the EFT transaction to be deleted, otherwise expect it to be kept
     */
    private void checkRemovePaymentWithEFT(String status, boolean expectDeleted) {
        FinancialAct transaction = createEFTPOSPayment(status);
        FinancialAct item = accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct payment = createPayment(item);

        remove(payment);
        assertNull(get(payment)); // verify the payment is removed
        assertNull(get(item));    // verify the payment item is removed
        if (expectDeleted) {
            assertNull(get(transaction));
        } else {
            assertNotNull(get(transaction));
        }
    }

    /**
     * Verifies that when an EFT payment item is removed, only those EFT transactions with NO_TERMINAL status are
     * removed.
     *
     * @param status        the EFT transaction status
     * @param expectDeleted if {@code true}, expect the EFT transaction to be deleted, otherwise expect it to be kept
     */
    private void checkRemoveEFTPaymentItem(String status, boolean expectDeleted) {
        FinancialAct transaction = createEFTPOSPayment(status);
        FinancialAct item = accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct payment = createPayment(item);

        remove(item);
        assertNotNull(get(payment)); // verify the payment is not removed
        assertNull(get(item));    // verify the payment item is removed
        if (expectDeleted) {
            assertNull(get(transaction));
        } else {
            assertNotNull(get(transaction));
        }
    }

    /**
     * Creates a new payment.
     *
     * @param item the payment item
     * @return a new payment
     */
    private FinancialAct createPayment(FinancialAct item) {
        return accountFactory.newPayment()
                .customer(getCustomer())
                .till(getTill())
                .add(item)
                .status(FinancialActStatus.IN_PROGRESS)
                .build();
    }

    /**
     * Creates an EFTPOS payment transaction.
     *
     * @param status the transaction status
     * @return a new transaction
     */
    private FinancialAct createEFTPOSPayment(String status) {
        return accountFactory.newEFTPOSPayment()
                .customer(getCustomer())
                .terminal(practiceFactory.createEFTPOSTerminal())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .status(status)
                .build();
    }

    /**
     * Verifies that when a refund is removed with an associated EFT item, only those EFT transactions with
     * NO_TERMINAL status are removed.
     *
     * @param status        the EFT transaction status
     * @param expectDeleted if {@code true}, expect the EFT transaction to be deleted, otherwise expect it to be kept
     */
    private void checkRemoveRefundWithEFT(String status, boolean expectDeleted) {
        FinancialAct transaction = createEFTPOSRefund(status);
        FinancialAct item = accountFactory.newEFTRefundItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct refund = createRefund(item);

        remove(refund);
        assertNull(get(refund));   // verify the refund is removed
        assertNull(get(item));     // verify the refund item is removed
        if (expectDeleted) {
            assertNull(get(transaction));
        } else {
            assertNotNull(get(transaction));
        }
    }

    /**
     * Verifies that when an EFT payment item is removed, only those EFT transactions with NO_TERMINAL status are
     * removed.
     *
     * @param status        the EFT transaction status
     * @param expectDeleted if {@code true}, expect the EFT transaction to be deleted, otherwise expect it to be kept
     */
    private void checkRemoveEFTRefundItem(String status, boolean expectDeleted) {
        FinancialAct transaction = createEFTPOSRefund(status);
        FinancialAct item = accountFactory.newEFTRefundItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct refund = createRefund(item);

        remove(item);
        assertNotNull(get(refund)); // verify the refund is not removed
        assertNull(get(item));    // verify the refund item is removed
        if (expectDeleted) {
            assertNull(get(transaction));
        } else {
            assertNotNull(get(transaction));
        }
    }

    /**
     * Creates a new refund.
     *
     * @param item the refund item
     * @return a new refund
     */
    private FinancialAct createRefund(FinancialAct item) {
        return accountFactory.newRefund()
                .customer(getCustomer())
                .till(getTill())
                .add(item)
                .status(FinancialActStatus.IN_PROGRESS)
                .build();
    }

    /**
     * Creates an EFTPOS refund transaction.
     *
     * @param status the transaction status
     * @return a new transaction
     */
    private FinancialAct createEFTPOSRefund(String status) {
        return accountFactory.newEFTPOSRefund()
                .customer(getCustomer())
                .terminal(practiceFactory.createEFTPOSTerminal())
                .amount(TEN)
                .location(practiceFactory.createLocation())
                .status(status)
                .build();
    }

}
