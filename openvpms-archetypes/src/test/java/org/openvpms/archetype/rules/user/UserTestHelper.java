/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.business.domain.im.security.User;

/**
 * User test helper methods.
 *
 * @author Tim Anderson
 */
public class UserTestHelper {

    /**
     * Creates a new archetype service authority.
     *
     * @param method    the method
     * @param archetype the archetype
     * @return a new authority
     */
    public static ArchetypeAwareGrantedAuthority createAuthority(String method, String archetype) {
        ArchetypeAwareGrantedAuthority authority = TestHelper.create(UserArchetypes.AUTHORITY,
                                                                     ArchetypeAwareGrantedAuthority.class);
        authority.setServiceName("archetypeService");
        authority.setMethod(method);
        authority.setShortName(archetype);
        authority.setName(TestHelper.randomName("zauthority"));
        return authority;
    }

    /**
     * Creates a role.
     *
     * @param authorities the authorities
     * @return a new role
     */
    public static SecurityRole createRole(ArchetypeAwareGrantedAuthority... authorities) {
        SecurityRole role = TestHelper.create(UserArchetypes.ROLE, SecurityRole.class);
        String name = TestHelper.randomName("zrole");
        role.setName(name);
        for (ArchetypeAwareGrantedAuthority authority : authorities) {
            role.addAuthority(authority);
        }
        return role;
    }

    /**
     * Create a user with the specified use type and authorities.
     *
     * @param userType    the user type. May be {@code null}
     * @param authorities the authorities
     * @return a new user
     */
    public static User createUser(String userType, ArchetypeAwareGrantedAuthority... authorities) {
        User user = createUser(authorities);
        if (userType != null) {
            user.addClassification(TestHelper.getLookup(UserArchetypes.USER_TYPE, userType));
        }
        return user;
    }

    /**
     * Create a user with the specified authorities.
     *
     * @param authorities the authorities
     * @return a new user
     */
    public static User createUser(ArchetypeAwareGrantedAuthority... authorities) {
        User user = TestHelper.createUser(TestHelper.randomName("zuser"), false);
        if (authorities.length != 0) {
            SecurityRole role = UserTestHelper.createRole(authorities);
            user.addRole(role);
        }
        return user;
    }

}