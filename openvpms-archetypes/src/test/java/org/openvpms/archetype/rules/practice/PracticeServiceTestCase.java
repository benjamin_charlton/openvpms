/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;

import org.junit.Test;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.job.account.TestAccountReminderJobBuilder;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PracticeService}.
 *
 * @author Tim Anderson
 */
public class PracticeServiceTestCase extends ArchetypeServiceTest {

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests the {@link PracticeService#accountRemindersEnabled()} method.
     */
    @Test
    public void testAccountRemindersEnabled() {
        IArchetypeService service = getArchetypeService();

        // disable existing jobs
        disableJobs();

        PracticeService practiceService = new PracticeService(service, practiceRules, null);
        assertFalse(practiceService.accountRemindersEnabled());

        // create a job and verify account reminders are enabled
        Entity job = new TestAccountReminderJobBuilder(service)
                .count()
                .interval(2, DateUnits.DAYS)
                .template("'dummy'")
                .add()
                .runAs(userFactory.createUser())
                .build();
        assertTrue(practiceService.accountRemindersEnabled());

        // disable the job and verify account reminders are no longer enabled
        job.setActive(false);
        save(job);
        assertFalse(practiceService.accountRemindersEnabled());

        practiceService.destroy();
    }

    /**
     * Disable any existing account reminder jobs.
     */
    private void disableJobs() {
        IArchetypeService service = getArchetypeService();

        // disable any existing entity.jobAccountReminder
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, AccountReminderArchetypes.ACCOUNT_REMINDER_JOB);
        query.where(builder.equal(root.get("active"), true));
        for (Entity job : service.createQuery(query).getResultList()) {
            job.setActive(false);
            save(job);
        }
    }
}
