/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.statement;

import org.apache.commons.collections4.IteratorUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.AccountType;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link StatementRules}.
 *
 * @author Tim Anderson
 */
public class StatementRulesTestCase extends AbstractStatementTest {

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The statement rules.
     */
    private StatementRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new StatementRules(getPractice(), getArchetypeService(), getRules());
    }

    /**
     * Tests the {@link StatementRules#getStatement(Party, Date)} and
     * {@link StatementRules#getStatementPreview(Party, Date, Date, boolean, boolean)} methods.
     */
    @Test
    public void testGetStatementPreview() {
        Party customer = getCustomer();
        BigDecimal feeAmount = new BigDecimal("25.00");
        Lookup accountType = FinancialTestHelper.createAccountType(
                30, DateUnits.DAYS, feeAmount, AccountType.FeeType.FIXED, 30, ZERO, "Test Accounting Fee");
        customer.addClassification(accountType);
        save(customer);

        List<FinancialAct> invoices1 = createChargesInvoice(BigDecimal.TEN, getDatetime("2017-12-31 10:00:00"));
        save(invoices1);

        FinancialAct openingBalance = getRules().createOpeningBalance(customer, getDate("2018-01-01"), BigDecimal.TEN);
        save(openingBalance);

        List<FinancialAct> invoices2 = createChargesInvoice(MathRules.ONE_HUNDRED, getDatetime("2018-01-01 10:00:00"));
        FinancialAct invoice2 = invoices2.get(0);
        save(invoices2);

        List<FinancialAct> invoices3 = createChargesInvoice(MathRules.ONE_HUNDRED, getDatetime("2018-01-01 10:30:00"));
        FinancialAct invoice3 = invoices3.get(0);
        invoice3.setStatus(ActStatus.COMPLETED);
        save(invoices3);

        List<FinancialAct> invoices4 = createChargesInvoice(BigDecimal.TEN, getDatetime("2018-01-01 11:00:00"));
        FinancialAct invoice4 = invoices4.get(0);
        invoice4.setStatus(ActStatus.IN_PROGRESS);
        save(invoices4);

        List<FinancialAct> invoices5 = createChargesInvoice(BigDecimal.TEN, getDatetime("2018-01-03 11:00:00"));
        FinancialAct invoice5 = invoices5.get(0);
        invoice5.setStatus(ActStatus.POSTED);
        save(invoices5);

        // process the customer's statement. Should just return the POSTED and COMPLETED invoice acts.
        // The invoice1 and invoice5 invoices won't be included as they are outside the range.
        checkStatementPreview(customer, getDate("2018-01-01"), getDate("2018-01-02"), openingBalance, invoice2,
                              invoice3);
        // this time, just pass the to date. Should work back to the opening balance
        checkStatementPreview(customer, getDate("2018-01-02"), openingBalance, invoice2, invoice3);

        // process the customer's statement for 5/2. Amounts should be overdue and a fee generated.
        List<FinancialAct> acts1 = getStatementPreview(customer, getDate("2018-01-01"), getDate("2018-02-05"));
        assertEquals(5, acts1.size());
        checkActs(acts1.subList(0, 4), openingBalance, invoice2, invoice3, invoice5);
        checkFee(acts1.get(4), feeAmount);

        // now just pass the to date. Should work back to the opening balance
        List<FinancialAct> acts2 = getStatementPreview(customer, getDate("2018-02-05"), true, true);
        assertEquals(5, acts2.size());
        checkActs(acts2.subList(0, 4), openingBalance, invoice2, invoice3, invoice5);
        checkFee(acts2.get(4), feeAmount);

        // now exclude completed charges and the fee
        List<FinancialAct> acts3 = getStatementPreview(customer, getDate("2018-02-05"), false, false);
        checkActs(acts3, openingBalance, invoice2, invoice5);
    }

    /**
     * Tests the {@link StatementRules#getStatement(Party, Date)} and
     * {@link StatementRules#getStatement(Party, Date, Date)} and
     * {@link StatementRules#getStatementRange(Party, Date, Date)} methods.
     */
    @Test
    public void testGetStatement() {
        Party customer = getCustomer();

        // create an invoice
        BigDecimal amount = new BigDecimal(950);
        List<FinancialAct> invoices1 = createChargesInvoice(amount, getDatetime("2017-12-29 10:00:00"));
        FinancialAct invoice1 = invoices1.get(0);
        save(invoices1);

        // run EOP for 31/12/2017.
        Date statementDate1 = getDate("2017-12-31");
        runEndOfPeriod(customer, statementDate1);

        checkEquals(amount, accountRules.getBalance(customer));
        checkStatement(customer, statementDate1, invoice1);
        checkStatementRange(customer, getDate("2017-12-01"), getDate("2017-12-31"), ZERO, invoice1);
        checkStatementRange(customer, getDate("2018-01-01"), getDate("2018-01-31"), amount);

        // create a payment for 14/1/2018
        FinancialAct payment1 = createPayment(amount, getDatetime("2018-01-14 14:52:00"));
        save(payment1);

        FinancialAct opening1 = accountRules.getOpeningBalanceAfter(customer, statementDate1);
        assertNotNull(opening1);
        checkEquals(amount, opening1.getTotal());
        assertFalse(opening1.isCredit());

        // backdate the statement to 1/1/2018. Should only include the opening balance
        Date statementDate3 = getDate("2018-01-01");
        checkStatement(customer, opening1.getActivityStartTime(), statementDate3, opening1);

        // now just pass the statement date. Should give the same results
        checkStatement(customer, statementDate3, opening1);

        // now forward to the payment date. Statement should include opening balance and payment
        Date statementDate4 = getDate("2018-01-14");
        checkStatement(customer, opening1.getActivityStartTime(), statementDate4, opening1, payment1);

        // again, just passing the statement date
        checkStatement(customer, statementDate4, opening1, payment1);

        // run EOP for the 31/1
        Date statementDate5 = getDate("2018-01-31");
        runEndOfPeriod(customer, statementDate5);

        FinancialAct opening2 = accountRules.getOpeningBalanceAfter(customer, statementDate5);
        assertNotNull(opening2);
        checkEquals(ZERO, opening2.getTotal());

        // check statement for 1/2.
        Date statementDate6 = getDate("2018-02-01");
        checkStatement(customer, statementDate6, opening2);

        // backdate the statement to 14/1/2018. Should only include the opening balance and payment
        checkStatement(customer, statementDate4, opening1, payment1);
        checkStatement(customer, opening1.getActivityStartTime(), statementDate4, opening1, payment1);

        // now check ranges
        checkStatementRange(customer, null, null, invoice1, payment1);
        checkStatementRange(customer, getDate("2017-12-31"), null, amount, payment1);  // dummy opening balance inserted
        checkStatementRange(customer, getDate("2017-12-31"), getDate("2018-01-05"), amount);          // "" ""
        checkStatementRange(customer, getDate("2017-12-31"), getDate("2018-01-15"), amount, payment1); // "" ""
    }

    /**
     * Tests the {@link StatementRules#getStatementRange(Party, Date, Date)} method.
     */
    @Test
    public void testGetStatementRange() {
        Party customer = getCustomer();

        // pay a deposit
        BigDecimal amount = BigDecimal.TEN;
        FinancialAct payment1 = createPayment(amount, getDate("2021-10-30"));
        save(payment1);

        // run EOP for 2021-10-31
        Date statementDate1 = getDate("2021-10-31");
        runEndOfPeriod(customer, statementDate1);

        // verify there is a credit balance
        checkEquals(amount.negate(), accountRules.getBalance(customer));
        FinancialAct opening1 = accountRules.getOpeningBalanceAfter(customer, statementDate1);
        assertNotNull(opening1);
        checkEquals(amount, opening1.getTotal());
        assertTrue(opening1.isCredit());

        // now invoice
        List<FinancialAct> invoice1 = createChargesInvoice(new BigDecimal("160"), getDate("2021-11-01"));
        save(invoice1);

        // run EOP for 2021-11-30
        Date statementDate2 = getDate("2021-11-30");
        runEndOfPeriod(customer, statementDate2);

        // now pay balance
        FinancialAct payment2 = createPayment(new BigDecimal("150"), getDate("2021-12-01"));
        save(payment2);

        checkStatementRange(customer, getDate("2021-11-30"), getDate("2021-12-31"), new BigDecimal("150"), payment2);
        checkStatementRange(customer, getDate("2021-12-01"), getDate("2021-12-31"), new BigDecimal("150"), payment2);
        checkStatementRange(customer, getDate("2021-12-02"), getDate("2021-12-31"), ZERO);
    }

    /**
     * Runs end-of-period for a customer.
     *
     * @param customer      the customer
     * @param statementDate the statement date
     */
    private void runEndOfPeriod(Party customer, Date statementDate) {
        EndOfPeriodProcessor eop = new EndOfPeriodProcessor(statementDate, true, getPractice(),
                                                            (IArchetypeRuleService) getArchetypeService(),
                                                            accountRules, laboratoryRules);
        eop.process(customer);
    }

    /**
     * Verifies a fee matches that expected.
     *
     * @param fee       the fee act
     * @param feeAmount the expected fee amount
     */
    private void checkFee(FinancialAct fee, BigDecimal feeAmount) {
        // check the fee. This should not have been saved
        checkDebitAdjust(fee, feeAmount, "Test Accounting Fee");
        assertTrue(fee.isNew());
    }

    /**
     * Helper to call {@link StatementRules#getStatementPreview(Party, Date, Date, boolean, boolean)} and verify the
     * results match those expected.
     *
     * @param customer the customer
     * @param from     the from-date. If non-null, should corresponding to an opening balance timestamp
     * @param to       the to-date. This corresponds to the statement date
     * @param expected the expected acts
     */
    private void checkStatementPreview(Party customer, Date from, Date to, FinancialAct... expected) {
        List<FinancialAct> actual = getStatementPreview(customer, from, to);
        checkActs(actual, expected);
    }

    /**
     * Returns the acts from {@link StatementRules#getStatementPreview(Party, Date, Date, boolean, boolean)} as a
     * list.
     *
     * @param customer the custom
     * @param from     the from-date. If non-null, should corresponding to an opening balance timestamp
     * @param to       the to-date. This corresponds to the statement date
     * @return the acts
     */
    private List<FinancialAct> getStatementPreview(Party customer, Date from, Date to) {
        return IteratorUtils.toList(rules.getStatementPreview(customer, from, to, true, true).iterator());
    }

    /**
     * Helper to call {@link StatementRules#getStatementPreview(Party, Date, Date, boolean, boolean)} and verify the
     * results match those expected.
     *
     * @param customer the customer
     * @param date     the statement date, used to calculate the account fee
     * @param expected the expected acts
     */
    private void checkStatementPreview(Party customer, Date date, FinancialAct... expected) {
        List<FinancialAct> actual = getStatementPreview(customer, date, true, true);
        checkActs(actual, expected);
    }

    /**
     * Preview acts that will be in a customer's next statement.
     *
     * @param customer                the customer
     * @param date                    the statement date, used to calculate the account fee
     * @param includeCompletedCharges if {@code true} include COMPLETED charges
     * @param includeFee              if {@code true}, include an accounting fee if one is required
     * @return the statement acts
     */
    private List<FinancialAct> getStatementPreview(Party customer, Date date, boolean includeCompletedCharges,
                                                   boolean includeFee) {
        Iterable<FinancialAct> preview = rules.getStatementPreview(customer, date, includeCompletedCharges, includeFee);
        return IteratorUtils.toList(preview.iterator());
    }

    private List<FinancialAct> getStatement(Party customer, Date date) {
        return IteratorUtils.toList(rules.getStatement(customer, date).iterator());
    }

    private void checkStatement(Party customer, Date date, FinancialAct... expected) {
        List<FinancialAct> actual = getStatement(customer, date);
        checkActs(actual, expected);
    }

    private void checkStatement(Party customer, Date from, Date to, FinancialAct... expected) {
        List<FinancialAct> actual = IteratorUtils.toList(rules.getStatement(customer, from, to).iterator());
        checkActs(actual, expected);
    }

    private void checkStatementRange(Party customer, Date from, Date to, FinancialAct... expected) {
        List<FinancialAct> actual = IteratorUtils.toList(rules.getStatementRange(customer, from, to).iterator());
        checkActs(actual, expected);
    }

    private void checkStatementRange(Party customer, Date from, Date to, BigDecimal balance, FinancialAct... expected) {
        List<FinancialAct> actual = IteratorUtils.toList(rules.getStatementRange(customer, from, to).iterator());
        assertEquals(expected.length + 1, actual.size());
        FinancialAct opening = actual.remove(0);
        assertEquals(from, opening.getActivityStartTime());
        checkEquals(balance, opening.getTotal());
        checkActs(actual, expected);
    }

    /**
     * Verifies acts matches those expected.
     *
     * @param actual   the acts
     * @param expected the expected acts
     */
    private void checkActs(List<FinancialAct> actual, FinancialAct... expected) {
        assertEquals(expected.length, actual.size());
        for (int i = 0; i < expected.length; ++i) {
            assertEquals(expected[i], actual.get(i));
        }
    }

}
