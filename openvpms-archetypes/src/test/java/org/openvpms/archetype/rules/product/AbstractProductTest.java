/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;

import java.math.BigDecimal;


/**
 * Base class for product tests.
 *
 * @author Tim Anderson
 */
public class AbstractProductTest extends ArchetypeServiceTest {

    /**
     * Helper to add an <em>productPrice.unitPrice</em> to a product.
     *
     * @param product the product
     * @param price   the price
     * @return the unit price
     */
    protected ProductPrice addUnitPrice(Product product, BigDecimal price) {
        return addUnitPrice(product, price, BigDecimal.ZERO);
    }

    /**
     * Helper to add an <em>productPrice.unitPrice</em> to a product, and save the product.
     *
     * @param product the product
     * @param price   the price
     * @param cost    the cost
     * @return the unit price
     */
    protected ProductPrice addUnitPrice(Product product, BigDecimal price, BigDecimal cost) {
        return addUnitPrice(product, price, cost, true);
    }

    /**
     * Helper to add an <em>productPrice.unitPrice</em> to a product.
     *
     * @param product the product
     * @param price   the price
     * @param cost    the cost
     * @param save    if {@code true} save the product
     * @return the unit price
     */
    protected ProductPrice addUnitPrice(Product product, BigDecimal price, BigDecimal cost, boolean save) {
        ProductPrice unitPrice = create(ProductArchetypes.UNIT_PRICE, ProductPrice.class);
        IMObjectBean priceBean = getBean(unitPrice);
        priceBean.setValue("cost", cost);
        priceBean.setValue("markup", BigDecimal.valueOf(100));
        priceBean.setValue("price", price);
        product.addProductPrice(unitPrice);
        if (save) {
            save(product);
        }
        return unitPrice;
    }

}
