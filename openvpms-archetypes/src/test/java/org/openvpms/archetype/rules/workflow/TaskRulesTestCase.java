/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.act.Act;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link TaskRules} class.
 *
 * @author Tim Anderson
 */
public class TaskRulesTestCase extends ArchetypeServiceTest {

    /**
     * The rules.
     */
    private TaskRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new TaskRules(getArchetypeService());
    }

    /**
     * Tests the {@link TaskRules#getWorkList(Act)} method.
     */
    @Test
    public void testGetWorkList() {
        Entity list = ScheduleTestHelper.createWorkList();
        Act task = ScheduleTestHelper.createTask(DateRules.getToday(), DateRules.getTomorrow(), list);
        assertEquals(list, rules.getWorkList(task));
    }

    /**
     * Tests the {@link TaskRules#getWorkListView(Party, org.openvpms.component.model.entity.Entity)} method.
     */
    @Test
    public void testGetWorkListView() {
        Party location1 = TestHelper.createLocation();
        Party location2 = TestHelper.createLocation();

        Entity listA = ScheduleTestHelper.createWorkList();
        Entity listB = ScheduleTestHelper.createWorkList();
        Entity listC = ScheduleTestHelper.createWorkList();
        Entity listD = ScheduleTestHelper.createWorkList();

        Entity view1 = ScheduleTestHelper.createWorkListView(listA, listB);
        Entity view2 = ScheduleTestHelper.createWorkListView(listC);

        ScheduleTestHelper.addWorkListView(location1, view1, true);
        ScheduleTestHelper.addWorkListView(location2, view2, true);

        assertEquals(view1, rules.getWorkListView(location1, listA));
        assertEquals(view1, rules.getWorkListView(location1, listB));
        assertEquals(view2, rules.getWorkListView(location2, listC));

        assertNull(rules.getWorkListView(location2, listA));
        assertNull(rules.getWorkListView(location2, listB));
        assertNull(rules.getWorkListView(location2, listA));
        assertNull(rules.getWorkListView(location1, listD));
        assertNull(rules.getWorkListView(location2, listD));
    }
}
