/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.till;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.deposit.DepositArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.ruleengine.RuleEngineException;
import org.openvpms.component.model.bean.IMObjectBean;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.CantAddActToTill;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.InvalidTillArchetype;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.MissingTill;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.UnclearedTillExists;
import static org.openvpms.archetype.test.TestHelper.createTill;


/**
 * Tests the {@link TillBalanceRules} class when invoked by the
 * <em>archetypeService.save.act.tillBalance.before</em>,
 * <em>archetypeService.save.act.customerAccountPayment.after</em> and
 * <em>archetypeService.save.act.customerAccountRefund.after</em> rules.
 * In order for these tests to be successful, the archetype service
 * must be configured to trigger the above rules.
 *
 * @author Tim Anderson
 */
public class TillBalanceRulesTestCase extends AbstractTillRulesTest {

    /**
     * The till.
     */
    private Entity till;

    /**
     * The rules.
     */
    private TillBalanceRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        till = createTill();
        save(till);
        rules = new TillBalanceRules(getArchetypeService());
    }

    /**
     * Verifies that an <em>act.tillBalance</em> with 'Uncleared' status
     * can only be saved if there are no other uncleared till balances for
     * the same till.<br/>
     * Requires the rule <em>archetypeService.save.act.tillBalance.before</em>.
     */
    @Test
    public void testSaveUnclearedTillBalance() {
        FinancialAct balance1 = createBalance(till, TillBalanceStatus.UNCLEARED);
        save(balance1);

        // can save the same balance multiple times
        save(balance1);

        FinancialAct balance2 = createBalance(till, TillBalanceStatus.UNCLEARED);
        try {
            save(balance2);
            fail("Expected save of second uncleared till balance to fail");
        } catch (RuleEngineException expected) {
            Throwable cause = expected.getCause();
            while (cause != null && !(cause instanceof TillRuleException)) {
                cause = cause.getCause();
            }
            assertNotNull(cause);
            TillRuleException exception = (TillRuleException) cause;
            assertEquals(UnclearedTillExists, exception.getErrorCode());
        }
    }

    /**
     * Verifies that multiple <em>act.tillBalance</em> with 'Cleared' status can
     * be saved for the same till.<br/>
     * Requires the rule <em>archetypeService.save.act.tillBalance.before</em>.
     */
    @Test
    public void testSaveClearedTillBalance() {
        for (int i = 0; i < 3; ++i) {
            FinancialAct balance = createBalance(till, TillBalanceStatus.CLEARED);
            save(balance);
        }
    }

    /**
     * Verifies that {@link TillBalanceRules#checkCanSaveTillBalance} throws
     * TillRuleException if invoked for an invalid act.<br/>
     */
    @Test
    public void testCheckCanSaveTillBalanceWithInvalidAct() {
        FinancialAct act = create(DepositArchetypes.BANK_DEPOSIT, FinancialAct.class);
        try {
            rules.checkCanSaveTillBalance(act);
        } catch (TillRuleException expected) {
            assertEquals(InvalidTillArchetype, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillBalanceRules#addToTill} adds posted
     * <em>act.customerAccountPayment</em> and
     * <em>act.customerAccountRefund</em> to the associated till balance
     * when they are saved.
     * Requires the rules <em>archetypeServicfe.save.act.customerAccountPayment.after</em> and
     * <em>archetypeServicfe.save.act.customerAccountRefund.after</em>
     */
    @Test
    public void testAddToTillBalance() {
        List<FinancialAct> payment = createPayment(till);
        List<FinancialAct> refund = createRefund(till);

        checkAddToTillBalance(till, payment, false, BigDecimal.ZERO);
        checkAddToTillBalance(till, refund, false, BigDecimal.ZERO);

        payment.get(0).setStatus(POSTED);
        checkAddToTillBalance(till, payment, false, BigDecimal.ONE);
        // payment now updates balance
        checkAddToTillBalance(till, refund, true, BigDecimal.ONE);
        // refund not added

        refund.get(0).setStatus(POSTED);
        checkAddToTillBalance(till, refund, true, BigDecimal.ZERO);
        // refund now updates balance

        // verify that subsequent saves only get don't get added to the balance
        // again
        checkAddToTillBalance(till, payment, true, BigDecimal.ZERO);
        checkAddToTillBalance(till, refund, true, BigDecimal.ZERO);

        List<FinancialAct> payment2 = createPayment(till);
        payment2.get(0).setStatus(POSTED);
        checkAddToTillBalance(till, payment2, true, BigDecimal.ONE);
    }

    /**
     * Verifies that {@link TillBalanceRules#addToTill} throws
     * TillRuleException if invoked for an invalid act.
     */
    @Test
    public void testAddToTillBalanceWithInvalidAct() {
        FinancialAct act = create(DepositArchetypes.BANK_DEPOSIT, FinancialAct.class);
        try {
            rules.addToTill(act);
        } catch (TillRuleException expected) {
            assertEquals(CantAddActToTill, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillBalanceRules#addToTill} throws
     * TillRuleException if invoked for an act with no till.
     */
    @Test
    public void testAddToTillBalanceWithNoTill() {
        FinancialAct act = create(CustomerAccountArchetypes.PAYMENT, FinancialAct.class);
        act.setStatus(IN_PROGRESS);
        Party party = TestHelper.createCustomer();
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", party);
        try {
            rules.addToTill(act);
        } catch (TillRuleException expected) {
            assertEquals(MissingTill, expected.getErrorCode());
        }
    }

    /**
     * Verifies that if a till balance adjustment is changed, this is reflected in the till balance.
     */
    @Test
    public void testChangeTillBalanceAdjustment() {
        FinancialAct act = create(TillArchetypes.TILL_BALANCE_ADJUSTMENT, FinancialAct.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("credit", true);
        bean.setTarget("till", till);
        bean.setValue("amount", BigDecimal.ONE);
        bean.save();
        FinancialAct balance = rules.getUnclearedBalance(till);
        assertNotNull(balance);
        checkEquals(BigDecimal.ONE, balance.getTotal());

        bean.setValue("amount", BigDecimal.TEN);
        bean.save();
        balance = get(balance);
        checkEquals(BigDecimal.TEN, balance.getTotal());
    }

}
