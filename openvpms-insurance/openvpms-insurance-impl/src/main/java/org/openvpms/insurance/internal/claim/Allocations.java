/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.insurance.claim.Condition;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.openvpms.archetype.rules.insurance.ClaimStatus.ACCEPTED;
import static org.openvpms.archetype.rules.insurance.ClaimStatus.GAP_CLAIM_NOTIFIED;
import static org.openvpms.archetype.rules.insurance.ClaimStatus.GAP_CLAIM_PAID;
import static org.openvpms.archetype.rules.insurance.ClaimStatus.POSTED;
import static org.openvpms.archetype.rules.insurance.ClaimStatus.SETTLED;
import static org.openvpms.archetype.rules.insurance.ClaimStatus.SUBMITTED;

/**
 * Determines allocations of invoices to {@link Condition}s.
 *
 * @author Tim Anderson
 */
class Allocations {

    /**
     * The claim.
     */
    private final ClaimImpl claim;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The condition allocation map.
     */
    private Map<ConditionImpl, BigDecimal> allocationMap;

    /**
     * The total allocation to invoices claimed in this claim.
     */
    private BigDecimal invoiceAllocation = BigDecimal.ZERO;

    /**
     * Constructs an {@link Allocations}.
     *
     * @param claim   the claim
     * @param service the archetype service
     */
    public Allocations(ClaimImpl claim, ArchetypeService service) {
        this.claim = claim;
        this.service = service;
    }

    /**
     * Returns the allocation for a condition.
     *
     * @param condition the condition
     * @return the allocation
     */
    public BigDecimal getAllocation(ConditionImpl condition) {
        lazyInit();
        BigDecimal result = allocationMap.get(condition);
        return result != null ? result : BigDecimal.ZERO;
    }

    /**
     * Returns the total allocation for all conditions.
     *
     * @return the total allocation for all conditions
     */
    public BigDecimal getConditionTotal() {
        BigDecimal result = BigDecimal.ZERO;
        lazyInit();
        for (Map.Entry<ConditionImpl, BigDecimal> entry : allocationMap.entrySet()) {
            result = result.add(entry.getValue());
        }
        return result;
    }

    /**
     * Returns the total amount that has been allocated toward invoices in this claim, excluding amounts that
     * have been allocated by the payment of other claims.
     * <p/>
     * This can be used to determine deposits made on invoices prior to making a gap claim in order to refund
     * overpayments.
     * <p>
     * Specifically, this is the sum of payments of the invoices being claimed, minus any payments which can be
     * attributed to items in the invoices that have been claimed by other claims. <br/>
     * The other claims must be pre-paid or have a gap claim status of PAID or NOTIFIED, as this indicates that payments
     * have been allocated.
     * <p/>
     * NOTE: this may be different to {@link #getConditionTotal()} which excludes payments greater than that being
     * claimed.
     *
     * @return the allocated amount
     */
    public BigDecimal getInvoiceTotal() {
        lazyInit();
        return invoiceAllocation;
    }

    /**
     * Clears the allocations.
     * <p/>
     * They will be recalculated when next accessed.
     */
    public void clear() {
        allocationMap = null;
    }

    /**
     * Lazily calculates the allocations.
     */
    private void lazyInit() {
        if (allocationMap == null) {
            init();
        }
    }

    /**
     * Calculates the allocations.
     */
    private void init() {
        allocationMap = new HashMap<>();
        invoiceAllocation = BigDecimal.ZERO;
        List<ConditionImpl> conditions = getConditions();

        IMObjectCache cache = claim.getCache();
        Map<Reference, FinancialAct> allInvoices = new HashMap<>();
        Map<ConditionImpl, List<InvoiceImpl>> conditionMap = new LinkedHashMap<>();
        for (ConditionImpl condition : conditions) {
            List<InvoiceImpl> invoices = condition.getInvoiceImpls();
            conditionMap.put(condition, invoices);
            for (InvoiceImpl invoice : invoices) {
                allInvoices.computeIfAbsent(invoice.getObjectReference(), k -> (FinancialAct) cache.get(k));
            }
        }

        for (FinancialAct invoice : allInvoices.values()) {
            BigDecimal allocation = invoice.getAllocatedAmount();
            if (!MathRules.isZero(allocation)) {
                BigDecimal allocated = allocateInvoiceToConditions(invoice, conditionMap);
                invoiceAllocation = invoiceAllocation.add(allocated);
            }
        }
    }

    /**
     * Allocates an invoice to the conditions that claim items from it.
     *
     * @param invoice      the invoice
     * @param conditionMap the map of conditions to their corresponding invoices
     * @return the maximum allocation for the invoice, negated if the invoice total is negative
     */
    private BigDecimal allocateInvoiceToConditions(FinancialAct invoice, Map<ConditionImpl,
            List<InvoiceImpl>> conditionMap) {
        BigDecimal maxAllocation = getMaximumAllocation(invoice);
        BigDecimal amount = maxAllocation;
        for (Map.Entry<ConditionImpl, List<InvoiceImpl>> entry : conditionMap.entrySet()) {
            if (amount.compareTo(BigDecimal.ZERO) <= 0) {
                break;
            }
            ConditionImpl condition = entry.getKey();
            List<InvoiceImpl> invoices = entry.getValue();
            amount = allocateInvoiceToCondition(invoice, condition, amount, invoices);
        }
        if (invoice.getTotal().signum() == -1) {
            // negative invoice
            maxAllocation = maxAllocation.negate();
        }
        return maxAllocation;
    }

    /**
     * Allocates an invoice to a condition, if it claims all or part of the invoice.
     *
     * @param invoice   the invoice
     * @param condition the condition
     * @param amount    the amount available for allocation
     * @param invoices  the invoices claimed by the condition
     * @return the remaining amount for allocation
     */
    private BigDecimal allocateInvoiceToCondition(FinancialAct invoice, ConditionImpl condition, BigDecimal amount,
                                                  List<InvoiceImpl> invoices) {
        BigDecimal conditionAllocation = allocationMap.get(condition);
        if (conditionAllocation == null) {
            conditionAllocation = BigDecimal.ZERO;
        }
        for (InvoiceImpl match : invoices) {
            if (match.getId() == invoice.getId()) {
                BigDecimal total = match.getTotal();
                if (amount.compareTo(total) >= 0) {
                    amount = amount.subtract(total);
                } else {
                    total = amount;
                    amount = BigDecimal.ZERO;
                }
                conditionAllocation = conditionAllocation.add(total);
            }
        }
        allocationMap.put(condition, conditionAllocation);
        return amount;
    }

    /**
     * Returns the conditions.
     * <p/>
     * The conditions are sorted on id so that allocations are performed consistently.
     *
     * @return the conditions
     */
    private List<ConditionImpl> getConditions() {
        List<ConditionImpl> result = new ArrayList<>();
        for (Condition condition : claim.getConditions()) {
            result.add((ConditionImpl) condition);
        }
        result.sort(Comparator.comparingLong(ConditionImpl::getId));
        return result;
    }

    /**
     * Determines the maximum available allocation of an invoice to this claim.
     * <p/>
     * This is the amount paid towards the invoice minus any items that have been claimed by other claims
     *
     * @param invoice the invoice
     * @return the maximum allocation of the invoice to the claim
     */
    private BigDecimal getMaximumAllocation(FinancialAct invoice) {
        BigDecimal result = invoice.getAllocatedAmount();
        long id = claim.getId();
        IMObjectCache cache = claim.getCache();
        IMObjectBean bean = service.getBean(invoice);
        for (Reference itemRef : bean.getTargetRefs("items")) {
            FinancialAct item = (FinancialAct) cache.get(itemRef);
            if (item == null) {
                throw new IllegalStateException("Invoice item=" + itemRef + " referenced by invoice="
                                                + invoice.getObjectReference() + " not found");
            }
            IMObjectBean itemBean = service.getBean(item);
            for (Reference reference : itemBean.getSourceRefs("claims")) {
                Act claimItem = (Act) cache.get(reference);
                // determine the claims the invoice item is in ...
                if (claimItem != null) {
                    IMObjectBean claimItemBean = service.getBean(claimItem);
                    Reference claimRef = claimItemBean.getSourceRef("claim");
                    if (claimRef != null && claimRef.getId() != id && isPaid(claimRef, cache)) {
                        // if the invoice item is not claimed by this claim, remove its total
                        result = result.subtract(item.getTotal());
                        if (result.compareTo(BigDecimal.ZERO) <= 0) {
                            result = BigDecimal.ZERO;
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Determine if a claim has been paid.
     * <p/>
     * A claim has been paid if:
     * <ul>
     *     <li>it is a finalised/non-cancelled standard claim - here all invoices must be paid prior to making
     *     the claim</li>
     *     <li>it is a gap claim, and the gap status is set to {@code PAID} or {@code NOTIFIED}.</li>
     * </ul>
     *
     * @param claimRef the claim reference
     * @param cache    the cache of claims
     * @return {@code true} if the claim has been paid
     */
    private boolean isPaid(Reference claimRef, IMObjectCache cache) {
        boolean result = false;
        Act claim = (Act) cache.get(claimRef);
        if (claim != null) {
            result = (GAP_CLAIM_PAID.equals(claim.getStatus2()) || GAP_CLAIM_NOTIFIED.equals(claim.getStatus2()));
            if (!result && (POSTED.equals(claim.getStatus()) || SUBMITTED.equals(claim.getStatus())
                            || ACCEPTED.equals(claim.getStatus()) || SETTLED.equals(claim.getStatus()))) {
                IMObjectBean bean = service.getBean(claim);
                result = !bean.getBoolean("gapClaim");
            }
        }
        return result;
    }

}