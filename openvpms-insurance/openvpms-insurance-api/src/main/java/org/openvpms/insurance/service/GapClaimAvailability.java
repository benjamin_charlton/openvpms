/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.service;

/**
 * Determines if an insurer is accepting gap claims.
 * <p/>
 * Note that gap claim availability may be time related; an insurer may only accept them during business hours
 * for example.
 *
 * @author Tim Anderson
 */
public class GapClaimAvailability {

    /**
     * Determines if gap claims are supported.
     */
    private final boolean available;

    /**
     * The message.
     */
    private final String message;

    /**
     * Constructs a {@link GapClaimAvailability}.
     *
     * @param available determines if gap claims are available
     * @param message   the message
     */
    private GapClaimAvailability(boolean available, String message) {
        this.available = available;
        this.message = message;
    }

    /**
     * Determines if gap claims are supported.
     *
     * @return {@code true} if gap claims are supported
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns an instance indicating that gap claims are supported, but doesn't provide a message.
     *
     * @return an instance that indicates gap claims are supported
     */
    public static GapClaimAvailability available() {
        return available(null);
    }

    /**
     * Returns an instance indicating that gap claims are supported, with a user message.
     * <p/>
     * A message should be provided if there are any constraints about when a gap claim can be submitted e.g. if
     * there are time restrictions.
     *
     * @param message a message for users submitting claims
     * @return an instance that indicates gap claims are supported
     */
    public static GapClaimAvailability available(String message) {
        return new GapClaimAvailability(true, message);
    }

    /**
     * Returns an instance indicating that gap claims are not supported, but doesn't provide a message.
     *
     * @return an instance that indicates gap claims are not supported
     */
    public static GapClaimAvailability notAvailable() {
        return notAvailable(null);
    }

    /**
     * Returns an instance indicating that gap claims are supported, with a message indicating why.
     *
     * @param message a message indicating why gap claims aren't supported
     * @return an instance that indicates gap claims are not supported
     */
    public static GapClaimAvailability notAvailable(String message) {
        return new GapClaimAvailability(false, message);
    }

}
