/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.claim;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * Represents a deposit when an insurer pays the vet for a claim.
 *
 * @author Tim Anderson
 */
public interface Deposit {

    /**
     * Returns the deposit identifier, issued by the insurer.
     *
     * @return the deposit identifier
     */
    String getDepositId();

    /**
     * The deposit date.
     *
     * @return the deposit date
     */
    OffsetDateTime getDate();

    /**
     * The deposit amount.
     *
     * @return the deposit amount
     */
    BigDecimal getAmount();

}
