/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.service;

import org.openvpms.component.model.party.Party;
import org.openvpms.domain.practice.Location;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.exception.InsuranceException;

/**
 * An insurance service that supports {@link GapClaim gap claims}.
 *
 * @author Tim Anderson
 */
public interface GapInsuranceService extends InsuranceService {

    /**
     * Determines if an insurer supports gap claims for a particular insurer, policy and practice location.
     * <p/>
     * If gap claims aren't supported, or have a time restriction as to when they can be submitted, this should
     * be conveyed in the {@link GapClaimAvailability#getMessage() message}.
     *
     * @param insurer      the insurer
     * @param policyNumber the policy number, or {@code null} if it is not known
     * @param location     the practice location
     * @return the gap claim support for the supplied parameters
     * @throws InsuranceException for any error
     */
    GapClaimAvailability getGapClaimAvailability(Party insurer, String policyNumber, Location location);

    /**
     * Invoked to notify the insurer that a gap claim has been part or fully paid by the customer.
     * <p>
     * Part payment occurs if the insurer updated the claim with a non-zero benefit amount, and the customer has
     * accepted it. They have paid the gap amount, i.e. the difference between the claim total and the benefit amount.
     * <br/>
     * The insurer is responsible for paying the practice the benefit amount.
     * <p>
     * Full payment occurs if the insurer did not provide a benefit amount, or the benefit amount was rejected by
     * the customer. In this case, the insurer is responsible for settling the claim with the customer.
     * <p/>
     * For full payment, this can be invoked after the claim has been submitted, but not yet accepted by the insurer.
     * <p/>
     * On success, the {@link GapClaim#paymentNotified()} method should be invoked.
     *
     * @param claim the gap claim
     * @throws InsuranceException for any error
     */
    void notifyPayment(GapClaim claim);

}
