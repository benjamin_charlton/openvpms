/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.archetype;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.DelegatingArchetypeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.business.service.security.RunAs;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionTypeDescriptor;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.archetype.ValidationError;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

/**
 * Implementation of the {@link ArchetypeService} for plugins.
 *
 * @author Tim Anderson
 */
public class PluginArchetypeService implements ArchetypeService {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The lookup service.
     */
    private final ILookupService lookups;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * Archetype service that handles setting up a user context for save operations.
     */
    private final IArchetypeService writable;

    /**
     * Constructs a {@link PluginArchetypeService}.
     *
     * @param service         the archetype service
     * @param lookups         the lookup service
     * @param practiceService the practice service
     */
    public PluginArchetypeService(IArchetypeRuleService service, ILookupService lookups,
                                  PracticeService practiceService) {
        this.service = service;
        this.lookups = lookups;
        this.practiceService = practiceService;
        writable = new DelegatingArchetypeService(service) {
            @Override
            public void save(IMObject object) {
                PluginArchetypeService.this.save(object);
            }

            @Override
            public void save(Collection<? extends IMObject> objects) {
                PluginArchetypeService.this.save(objects);
            }

            @Override
            public org.openvpms.component.business.domain.im.common.IMObject create(String shortName) {
                return (org.openvpms.component.business.domain.im.common.IMObject)
                        PluginArchetypeService.this.create(shortName);
            }

            @Override
            public <T extends IMObject> T create(String archetype, Class<T> type) {
                return PluginArchetypeService.this.create(archetype, type);
            }
        };
    }

    /**
     * Return all archetypes that match that specified.
     *
     * @param archetype   the archetype. May contain a wildcard character
     * @param primaryOnly return only the primary archetypes
     * @return a list of archetypes
     */
    @Override
    public List<String> getArchetypes(String archetype, boolean primaryOnly) {
        return service.getArchetypes(archetype, primaryOnly);
    }

    /**
     * Returns the {@link ArchetypeDescriptor} for the given archetype.
     *
     * @param archetype the archetype
     * @return the descriptor corresponding to the archetype, or {@code null} if none is found
     */
    @Override
    public ArchetypeDescriptor getArchetypeDescriptor(String archetype) {
        return service.getArchetypeDescriptor(archetype);
    }

    /**
     * Return all the {@link ArchetypeDescriptor} instances that match the specified archetype.
     *
     * @param archetype the archetype. May contain wildcards
     * @return a list of matching archetype descriptors
     */
    @Override
    public List<org.openvpms.component.model.archetype.ArchetypeDescriptor> getArchetypeDescriptors(String archetype) {
        return service.getArchetypeDescriptors(archetype);
    }

    /**
     * Returns all the {@link ArchetypeDescriptor} managed by this service.
     *
     * @return the archetype descriptors
     */
    @Override
    public List<ArchetypeDescriptor> getArchetypeDescriptors() {
        return service.getArchetypeDescriptors();
    }

    /**
     * Return the {@link AssertionTypeDescriptor} with the specified name.
     *
     * @param name the name of the assertion type
     * @return the assertion type descriptor corresponding to the name or {@code null} if none is found
     */
    @Override
    public AssertionTypeDescriptor getAssertionTypeDescriptor(String name) {
        return service.getAssertionTypeDescriptor(name);
    }

    /**
     * Return all the {@link AssertionTypeDescriptor} instances supported by this service.
     *
     * @return the assertion type descriptors
     */
    @Override
    public List<AssertionTypeDescriptor> getAssertionTypeDescriptors() {
        return service.getAssertionTypeDescriptors();
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    @Override
    public IMObjectBean getBean(org.openvpms.component.model.object.IMObject object) {
        return new IMObjectBean(object, writable, lookups);
    }

    /**
     * Create a domain object given its archetype.
     *
     * @param archetype the archetype name
     * @return a new object
     * @throws OpenVPMSException for any error
     */
    @Override
    public IMObject create(String archetype) {
        return create(archetype, IMObject.class);
    }

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @param type      the expected type of the object
     * @return a new object
     * @throws OpenVPMSException  for any error
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    @Override
    public <T extends IMObject> T create(String archetype, Class<T> type) {
        return run(() -> service.create(archetype, type));
    }

    /**
     * Derived values for the specified {@link IMObject}, based on its corresponding {@link ArchetypeDescriptor}.
     *
     * @param object the object to derived values for
     * @throws OpenVPMSException for any error
     */
    @Override
    public void deriveValues(IMObject object) {
        run(() -> service.deriveValues(object));
    }

    /**
     * Derive the value for the {@link NodeDescriptor} with the specified name.
     *
     * @param object the object to operate on.
     * @param node   the name of the {@link NodeDescriptor}, which will be used to derive the value
     * @throws OpenVPMSException for any error
     */
    @Override
    public void deriveValue(IMObject object, String node) {
        run(() -> service.deriveValue(object, node));
    }

    /**
     * Saves an object, executing any <em>save</em> rules associated with its archetype.
     *
     * @param object the object to save
     */
    @Override
    public void save(IMObject object) {
        run(() -> service.save(object));
    }

    /**
     * Save a collection of {@link IMObject} instances.
     *
     * @param objects the objects to insert or update
     */
    @Override
    public void save(final Collection<? extends IMObject> objects) {
        run(() -> service.save(objects));
    }

    /**
     * Remove the specified object.
     *
     * @param object the object to remove
     */
    @Override
    public void remove(IMObject object) {
        run(() -> service.remove(object));
    }

    /**
     * Removes an object given its reference.
     *
     * @param reference the object reference
     * @throws OpenVPMSException for any error
     */
    @Override
    public void remove(Reference reference) {
        run(() -> service.remove(reference));
    }

    /**
     * Validates an object.
     *
     * @param object the object to validate
     * @return a list of validation errors, if any
     */
    @Override
    public List<ValidationError> validate(IMObject object) {
        return service.validate(object);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @return the object, or {@code null} if none is found
     */
    @Override
    public IMObject get(Reference reference) {
        return service.get(reference);
    }

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public IMObject get(Reference reference, boolean active) {
        return service.get(reference, active);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    @Override
    public <T extends IMObject> T get(Reference reference, Class<T> type) {
        return service.get(reference, type);
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public IMObject get(String archetype, long id) {
        return service.get(archetype, id);
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public IMObject get(String archetype, long id, boolean active) {
        return service.get(archetype, id, active);
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    @Override
    public <T extends IMObject> T get(String archetype, long id, Class<T> type) {
        return service.get(archetype, id, type);
    }

    /**
     * Returns a builder to create queries.
     *
     * @return the criteria builder
     */
    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return service.getCriteriaBuilder();
    }

    /**
     * Creates a {@link TypedQuery} for executing a criteria query.
     *
     * @param query the criteria query
     * @return the new query instance
     */
    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> query) {
        return service.createQuery(query);
    }

    /**
     * Execute a {@code Runnable} with the security context set.
     *
     * @param runnable the runnable to execute
     */
    private void run(Runnable runnable) {
        User user = practiceService.getServiceUser();
        if (user == null) {
            throw new IllegalStateException(
                    "Cannot invoke ArchetypeService operation as no Service User has been configured");
        }
        RunAs.run(user, runnable);
    }

    /**
     * Execute a {@code Supplier} with the security context set.
     *
     * @param supplier the supplier to execute
     */
    private <T> T run(Supplier<T> supplier) {
        User user = practiceService.getServiceUser();
        if (user == null) {
            throw new IllegalStateException(
                    "Cannot invoke ArchetypeService operation as no Service User has been configured");
        }
        return RunAs.call(user, supplier);
    }

}
