/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.archetype;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.security.RunAs;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Helper to proxy {@link IArchetypeRuleService} for plugins.
 * <br/>
 * TODO - this is a workaround as the PluginArchetypeService cannot be used until IArchetypeService extends
 * org.openvpms.component.service.archetype.ArchetypeService.
 *
 * @author Tim Anderson
 */
public class ArchetypeServiceProxyHelper {

    /**
     * Proxies the archetype service so that calls are made in the security context of the practice service user.
     *
     * @param service         the archetype service
     * @param practiceService the practice service
     * @return the archetype service proxy
     */
    public static IArchetypeRuleService proxy(IArchetypeRuleService service, PracticeService practiceService) {
        InvocationHandler handler = (proxy, method, args) -> {
            User user = practiceService.getServiceUser();
            if (user == null) {
                throw new IllegalStateException(
                        "Cannot invoke ArchetypeService operation as no Service User has been configured");
            }

            if (method.getName().equals("getBean")) {
                return new IMObjectBean((IMObject) args[0], (IArchetypeRuleService) proxy);
            }
            return RunAs.run(user, () -> method.invoke(service, args));
        };
        Class<IArchetypeRuleService> type = IArchetypeRuleService.class;
        return (IArchetypeRuleService) Proxy.newProxyInstance(type.getClassLoader(), new Class[]{type}, handler);
    }
}
