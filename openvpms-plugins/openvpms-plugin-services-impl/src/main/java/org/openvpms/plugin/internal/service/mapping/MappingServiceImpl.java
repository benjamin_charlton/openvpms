/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.mapping.exception.MappingException;
import org.openvpms.mapping.model.Cardinality;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Targets;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.service.i18n.MappingMessages;

import java.util.Comparator;
import java.util.List;

/**
 * Default implementation of {@link MappingServiceImpl}.
 *
 * @author Tim Anderson
 */
public class MappingServiceImpl implements MappingService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link MappingServiceImpl}.
     *
     * @param service the archetype service
     */
    public MappingServiceImpl(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Create a configuration to record mappings between the specified archetype and a target.
     *
     * @param type the type being mapped
     * @return a new configuration to record mappings
     */
    @Override
    public <T extends IMObject> IMObject createMappingConfiguration(Class<T> type) {
        IMObject result;
        if (Entity.class.isAssignableFrom(type)) {
            result = service.create(MappingArchetypes.ENTITY_MAPPINGS, Entity.class);
        } else if (Lookup.class.isAssignableFrom(type)) {
            result = service.create(MappingArchetypes.LOOKUP_MAPPINGS, Lookup.class);
            Lookup lookup = (Lookup) result;
            lookup.setCode(result.getLinkId().toUpperCase());
            // default the code to the linkId. This allows the lookup to be saved immediately as the linkId is unique.
            // API users can set their own code if required.
        } else {
            throw new MappingException(MappingMessages.unsupportedType(type));
        }
        return result;
    }

    /**
     * Create a configuration to record mappings between the specified archetype and a target.
     *
     * @param type        the type being mapped
     * @param cardinality the mapping cardinality
     * @return a new configuration to record mappings
     */
    @Override
    public <T extends IMObject> IMObject createMappingConfiguration(Class<T> type, Cardinality cardinality) {
        IMObject object = createMappingConfiguration(type);
        setCardinality(object, cardinality);
        return object;
    }

    /**
     * Returns a mapping configuration for mapping lookups, optionally creating it if it doesn't exist.
     * <p/>
     * The specified lookup code must be unique. A simple way of achieving this is to encode the domain name with
     * an internal identifier. e.g. <em>com.mydomain.myplugin.id</em>
     *
     * @param code        the lookup code
     * @param create      if {@code true}, create and save the configuration if it does not exist
     * @param cardinality the cardinality to use if creating the configuration, use {@code null} to use the default
     * @return the configuration, or {@code null} if it does not exist, and {@code create} is {@code false}
     */
    @Override
    public Lookup getMappingConfiguration(String code, boolean create, Cardinality cardinality) {
        Lookup result;
        String archetype = MappingArchetypes.LOOKUP_MAPPINGS;
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Lookup> query = builder.createQuery(Lookup.class);
        Root<Lookup> from = query.from(Lookup.class, archetype);
        query.where(builder.equal(from.get("code"), code));
        query.orderBy(builder.asc(from.get("id")));
        result = service.createQuery(query).getFirstResult();
        if (result == null) {
            // TODO - should be done in a transaction.
            result = service.create(archetype, Lookup.class);
            result.setCode(code);
            if (cardinality != null) {
                setCardinality(result, cardinality);
            }
            service.save(result);
        }
        return result;
    }

    /**
     * Creates mappings.
     *
     * @param config      the mapping configuration
     * @param type        the type being mapped
     * @param archetype   the archetype being mapped
     * @param displayName the mapping display name
     * @param targets     the targets
     * @return the mappings
     */
    @Override
    public <T extends IMObject> Mappings<T> createMappings(IMObject config, Class<T> type, String archetype,
                                                           String displayName, Targets targets) {
        List<ArchetypeDescriptor> descriptors = checkArchetype(archetype, type);
        String sourceDisplayName;
        if (descriptors.size() > 1) {
            descriptors.sort(Comparator.comparing(ArchetypeDescriptor::getDisplayName));
            sourceDisplayName = descriptors.get(0).getDisplayName();
            sourceDisplayName += ",...";
        } else {
            sourceDisplayName = descriptors.get(0).getDisplayName();
        }
        return new MappingsImpl<T>(config, type, archetype, displayName, sourceDisplayName, targets, service);
    }

    /**
     * Creates mappings.
     *
     * @param config            the mapping configuration
     * @param type              the type being mapped
     * @param archetype         the archetype being mapped
     * @param displayName       the mapping display name
     * @param sourceDisplayName the source object type display name
     * @param targets           the targets
     * @return the mappings
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends IMObject> Mappings<T> createMappings(IMObject config, Class<T> type, String archetype,
                                                           String displayName, String sourceDisplayName,
                                                           Targets targets) {
        checkArchetype(archetype, type);
        return new MappingsImpl(config, type, archetype, displayName, sourceDisplayName, targets, service);
    }

    /**
     * Sets the mapping cardinality.
     *
     * @param config      the mapping configuration
     * @param cardinality the cardinality
     */
    private void setCardinality(IMObject config, Cardinality cardinality) {
        IMObjectBean bean = service.getBean(config);
        switch (cardinality) {
            case ONE_TO_ONE:
                bean.setValue("cardinality", "1:1");
                break;
            case MANY_TO_ONE:
                bean.setValue("cardinality", "N:1");
                break;
            default:
                throw new IllegalArgumentException("Unsupported cardinality: " + cardinality);
        }
    }

    /**
     * Checks that an archetype is valid.
     *
     * @param archetype the archetype. May contain wildcards
     * @param type      the expected implementation class
     * @return the matching archetype descriptors
     */
    private <T extends IMObject> List<ArchetypeDescriptor> checkArchetype(String archetype, Class<T> type) {
        if (!Entity.class.isAssignableFrom(type) && !Lookup.class.isAssignableFrom(type)) {
            throw new MappingException(MappingMessages.unsupportedType(type));
        }
        List<ArchetypeDescriptor> descriptors = service.getArchetypeDescriptors(archetype);
        if (descriptors.isEmpty()) {
            throw new MappingException(MappingMessages.archetypeNotFound(archetype));
        }
        for (ArchetypeDescriptor descriptor : descriptors) {
            if (!type.isAssignableFrom(descriptor.getClassType())) {
                throw new MappingException(MappingMessages.archetypeNotType(descriptor.getArchetypeType(), type));
            }
        }
        return descriptors;
    }
}
