/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.service.util;

import org.apache.commons.lang3.ClassUtils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

/**
 * Helper to execute calls with the context class loader set, restoring it on completion.
 * <p/>
 * This is important for plugins that rely on third-party libraries that fall back to the system class loader
 * if no context class loader is set (e.g. JAX-RS).
 *
 * @author Tim Anderson
 */
public class ContextClassLoaderHelper {

    /**
     * Proxies an object so that all calls are made with the thread context class loader set to that of the object's
     * class loader.
     *
     * @param object the client
     * @return the proxied object
     */
    @SuppressWarnings("unchecked")
    public static <T> T proxy(T object) {
        Class<?>[] interfaces = ClassUtils.getAllInterfaces(object.getClass()).toArray(new Class<?>[0]);
        InvocationHandler handler = (proxy, method, args) -> invoke(method, object, args);
        return (T) java.lang.reflect.Proxy.newProxyInstance(object.getClass().getClassLoader(), interfaces, handler);
    }

    /**
     * Proxies the result returned by a {@link Supplier}, using its class loader as the thread context class loader.
     * <p/>
     * The call is made with the thread context class loader set to the calls's class loader.
     * <p/>
     * This can be used to ensure that object construction and all if its subsequent calls are done with the thread
     * context class loader set. E.g.:
     * <br/><br/>
     * {@code MyAPI proxied = proxy(() -> new MyAPIImpl());}
     *
     * @param call the call
     * @return the proxied object
     */
    public static <T> T proxy(Supplier<T> call) {
        ClassLoader classLoader = call.getClass().getClassLoader();
        T object = invoke(classLoader, call);
        if (object == null) {
            throw new NullPointerException("Cannot proxy a null object");
        }
        return proxy(object);
    }

    /**
     * Invokes a {@link Runnable} with the context class loader set to that supplied.
     *
     * @param loader   the class loader
     * @param runnable the runnable to execute
     */
    public static void invoke(ClassLoader loader, Runnable runnable) {
        ClassLoader current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(loader);
            runnable.run();
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }
    }

    /**
     * Invokes a {@link Supplier} with the context class loader set to that supplied.
     *
     * @param loader the class loader
     * @param call   the call to execute
     * @return the result of the invocation
     */
    public static <T> T invoke(ClassLoader loader, Supplier<T> call) {
        ClassLoader current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(loader);
            return call.get();
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }
    }

    /**
     * Invokes a method in the context of the plugin class loader.
     *
     * @param method the method to invoke
     * @param object the object to invoke the method on
     * @param args   the method arguments
     * @return the result of the callable
     * @throws Throwable if the method throws an exception
     */
    private static Object invoke(Method method, Object object, Object[] args) throws Throwable {
        try {
            return invoke(object.getClass().getClassLoader(), (Callable<Object>) () -> method.invoke(object, args));
        } catch (InvocationTargetException exception) {
            throw exception.getCause();
        }
    }

    /**
     * Invokes a {@link Callable} with the context class loader set to that supplied.
     *
     * @param loader the class loader
     * @param call   the call to execute
     * @return the result of the invocation
     * @throws Exception any exception thrown by {@code call}
     */
    private static <T> T invoke(ClassLoader loader, Callable<T> call) throws Exception {
        ClassLoader current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(loader);
            return call.call();
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }
    }
}
