/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.auth.internal;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;

/**
 * Implementation of the Seraph {@link Authenticator},  to authenticate users, log them in, log them out and check
 * their roles.
 *
 * @author Tim Anderson
 */
public class AuthenticatorImpl extends DefaultAuthenticator {

    /**
     * The user manager.
     */
    private final UserManager userManager;

    /**
     * Constructs an {@link AuthenticatorImpl}.
     *
     * @param userManager the user manager
     */
    public AuthenticatorImpl(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * Retrieve a Principal for the given username. Returns null if no such user exists.
     *
     * @param username the name of the user to find
     * @return a Principal for the given username.
     */
    @Override
    protected Principal getUser(String username) {
        Principal result = null;
        UserProfile profile = userManager.getUserProfile(username);
        if (profile instanceof UserDetails) {
            UserDetails details = (UserDetails) profile;
            result = new UsernamePasswordAuthenticationToken(details, null, details.getAuthorities());
            // NOTE: in org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider.
            // setDetails(authentication.getDetails()) is invoked, which associates the WebAuthenticationDetails with
            // the token
        }
        return result;
    }

    /**
     * Authenticates the given user and password. Returns {@code true} if the authentication succeeds,
     * and {@code false} if the authentication details are invalid or if the user is not found.
     * Implementations of this method must not attempt to downcast the user to an implementation class.
     *
     * @param user     the user to authenticate. This object only stores the username of the user.
     * @param password the password of the user
     * @return true if the user was successfully authenticated and false otherwise.
     * @throws AuthenticatorException if an error occurs that stops the user from being authenticated (eg remote communication failure).
     */
    @Override
    protected boolean authenticate(Principal user, String password) throws AuthenticatorException {
        return userManager.authenticate(user.getName(), password);
    }
}
