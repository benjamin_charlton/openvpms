/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.mapping;

import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.service.QueryHelper;
import org.openvpms.mapping.model.DefaultTarget;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Maps OpenVPMS roster areas to Deputy OperationalUnits.
 *
 * @author Tim Anderson
 */
public class OperationalUnits extends DeputyTargets {

    /**
     * The Deputy service.
     */
    private final Deputy deputyService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OperationalUnits.class);

    /**
     * Constructs an {@link OperationalUnits}.
     *
     * @param displayName   the display name
     * @param deputyService the Deputy service
     */
    public OperationalUnits(String displayName, Deputy deputyService) {
        super(displayName);
        this.deputyService = deputyService;
    }

    /**
     * Returns a target given its identifier.
     *
     * @param identity the target object identifier
     * @return the target, or {@code null} if none exists
     */
    @Override
    public Target getTarget(String identity) {
        Target result = null;
        try {
            OperationalUnit unit = deputyService.getOperationalUnit(Long.valueOf(identity));
            result = new DefaultTarget(identity, unit.getName(), unit.getActive());
        } catch (Throwable exception) {
            log.error("Failed to get OperationalUnit=" + identity+ ": " + exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Returns the available targets.
     *
     * @param mappings    the mappings
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve, or {@code -1} to retrieve all results
     * @return the available targets
     */
    @Override
    public List<Target> getTargets(Mappings mappings, String name, boolean unmapped, int firstResult, int maxResults) {
        Query query = new Query();
        query.orderBy("OperationalUnitName", true);
        query.orderBy("Id", true);
        // TODO - filtering should be done by Deputy
        List<OperationalUnit> units = QueryHelper.query(query, deputyService::getOperationalUnits);
        return getTargets(mappings, name, unmapped, firstResult, maxResults, units);
    }
}
