/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Deputy response error.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseError {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Error {

        /**
         * The error code.
         */
        private String code;

        /**
         * The error message.
         */
        private String message;

        /**
         * Returns the error code.
         *
         * @return the error code
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the error code.
         *
         * @param code the error code
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * Returns the error message.
         *
         * @return the error message
         */
        public String getMessage() {
            return message;
        }

        /**
         * Sets the error message.
         *
         * @param message the error message
         */
        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * The error.
     */
    private Error error;

    /**
     * Returns the error.
     *
     * @return the error
     */
    public Error getError() {
        return error;
    }

    /**
     * Sets the error.
     *
     * @param error the error
     */
    public void setError(Error error) {
        this.error = error;
    }

    /**
     * Returns a string representation of the error.
     *
     * @return a string representation of the error
     */
    public String toString() {
        return error != null ? error.getCode() + " - " + error.getMessage() : null;
    }

}

