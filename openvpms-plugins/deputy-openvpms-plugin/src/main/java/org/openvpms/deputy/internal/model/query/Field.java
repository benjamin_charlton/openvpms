/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.query;

/**
 * Search field.
 *
 * @author Tim Anderson
 */
public class Field {

    /**
     * The field.
     */
    private String field;

    /**
     * The search type.
     */
    private String type;

    /**
     * The search data.
     */
    private Object data;

    /**
     * Default constructor.
     */
    public Field() {
        super();
    }

    /**
     * Constructs a {@link Field}.
     *
     * @param field the search field
     * @param type  the field type
     * @param data  the search data
     */
    public Field(String field, String type, Object data) {
        this.field = field;
        this.type = type;
        this.data = data;
    }

    /**
     * Returns the search field.
     *
     * @return the search field
     */
    public String getField() {
        return field;
    }

    /**
     * Sets the search field.
     *
     * @param field the search field
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * Returns the field type.
     *
     * @return the field type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the search type.
     *
     * @param type the search type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the search data.
     *
     * @return the search data
     */
    public Object getData() {
        return data;
    }

    /**
     * Sets the search data.
     *
     * @param data the search data
     */
    public void setData(Object data) {
        this.data = data;
    }
}
