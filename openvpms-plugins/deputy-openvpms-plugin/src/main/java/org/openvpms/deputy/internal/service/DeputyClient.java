/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.api.ResponseError;
import org.openvpms.deputy.internal.model.organisation.Company;
import org.openvpms.deputy.internal.model.organisation.Employee;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;
import org.openvpms.ws.util.ErrorResponseFilter;
import org.openvpms.ws.util.ObjectMapperContextResolver;
import org.openvpms.ws.util.SLF4JLoggingFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

/**
 * Default implementation of {@link Deputy}.
 *
 * @author Tim Anderson
 */
class DeputyClient implements Deputy {

    /**
     * The client.
     */
    private Deputy client;

    /**
     * Empty form.
     */
    private static final Form EMPTY_FORM = new Form();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DeputyClient.class);

    /**
     * Connection & read timeout.
     */
    private static final int TIMEOUT = 30000;  // 30 seconds

    /**
     * Authorization header.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * Constructs a {@link DeputyClient}.
     *
     * @param url         the API url
     * @param accessToken the OAuth access token
     */
    DeputyClient(String url, String accessToken) {
        url = StringUtils.removeEnd(url, "/");
        ObjectMapperContextResolver resolver = new ObjectMapperContextResolver(
                TimeZone.getDefault(), ObjectMapperContextResolver.ISO_8601_SECOND_TIMEZONE);
        ClientConfig config = new ClientConfig()
                .register(resolver)
                .register(JacksonFeature.class)
                .register(new ErrorResponseFilter(resolver.getContext(Object.class), ResponseError.class))
                .register(new SLF4JLoggingFeature(log, AUTHORIZATION));

        Client client = ClientBuilder.newClient(config);
        client.property(ClientProperties.CONNECT_TIMEOUT, TIMEOUT);
        client.property(ClientProperties.READ_TIMEOUT, TIMEOUT);

        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.add(AUTHORIZATION, "OAuth " + accessToken);
        headers.add("dp-meta-option", "none");  // disable meta-data return
        WebTarget target = client.target(url);
        this.client = WebResourceFactory.newResource(Deputy.class, target, false, headers, Collections.emptyList(),
                                                     EMPTY_FORM);
    }

    /**
     * Returns the companies.
     *
     * @return the companies
     */
    @Override
    public List<Company> getCompanies() {
        return client.getCompanies();
    }

    /**
     * Queries operational units.
     *
     * @param query the query
     * @return the matching operational units
     */
    @Override
    public List<OperationalUnit> getOperationalUnits(Query query) {
        return client.getOperationalUnits(query);
    }

    /**
     * Returns an operational unit given its identifier.
     *
     * @param id the operational unit identifier
     * @return the corresponding operational unit
     * @throws NotFoundException if the operational unit does not exist
     */
    @Override
    public OperationalUnit getOperationalUnit(long id) {
        return client.getOperationalUnit(id);
    }

    /**
     * Queries employees.
     *
     * @param query the query
     * @return the matching rosters
     */
    @Override
    public List<Employee> getEmployees(Query query) {
        return client.getEmployees(query);
    }

    /**
     * Returns an employee given its identifier.
     *
     * @param id the employee identifier
     * @return the corresponding employee,
     * @throws NotFoundException if the employee does not exist
     */
    @Override
    public Employee getEmployee(long id) {
        return client.getEmployee(id);
    }

    /**
     * Queries rosters.
     *
     * @param query the query
     * @return the matching rosters
     */
    @Override
    public List<Roster> getRosters(Query query) {
        return client.getRosters(query);
    }

    /**
     * Returns a roster given its identifier.
     *
     * @param id the roster identifier
     * @return the roster
     * @throws NotFoundException if the roster is not found
     */
    @Override
    public Roster getRoster(long id) {
        return client.getRoster(id);
    }

    /**
     * Creates a new roster, or updates an existing roster.
     * <p/>
     * To update an existing roster, the {@link RosterData#getId()} must be set.
     *
     * @param data the roster data
     * @return the new roster
     * @throws BadRequestException if an overlap is detected
     */
    @Override
    public Roster roster(RosterData data) {
        return client.roster(data);
    }

    /**
     * Deletes a roster.
     *
     * @param id the event identifier
     * @return the deletion confirmation message
     * @throws NotFoundException if the roster does not exist
     */
    @Override
    public String removeRoster(long id) {
        return client.removeRoster(id);
    }

}
