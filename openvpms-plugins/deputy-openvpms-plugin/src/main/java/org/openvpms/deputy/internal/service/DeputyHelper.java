/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.object.Identity;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.mapping.model.Target;

import java.time.Instant;
import java.util.Date;

/**
 * Deputy helper methods.
 *
 * @author Tim Anderson
 */
class DeputyHelper {

    /**
     * Returns a roster identifier from an identity.
     *
     * @param identity the identity. May be {@code null}
     * @return the corresponding identifier, or {@code -1} if it cannot be determined
     */
    static long getId(Identity identity) {
        long result = -1;
        String id = (identity != null) ? identity.getIdentity() : null;
        if (id != null) {
            try {
                result = Long.valueOf(id);
            } catch (NumberFormatException ignore) {
            }
        }
        return result;
    }

    /**
     * Returns the synchronisation identity for an event.
     *
     * @param event the event
     * @return the identity, or {@code null} if none is found
     */
    static ActIdentity getSynchronisationId(IMObjectBean event) {
        return event.getValue("synchronisation", ActIdentity.class, Predicates.isA(Archetypes.EVENT_ID));
    }

    /**
     * Returns a roster identifier from an event.
     *
     * @param event the event
     * @return the corresponding identifier, or {@code -1} if it cannot be determined
     */
    static long getRosterId(IMObjectBean event) {
        Identity identity = getSynchronisationId(event);
        return (identity != null) ? getId(identity) : -1;
    }

    /**
     * Helper to convert a unix timestamp to a date.
     *
     * @param timestamp the timestamp
     * @return the corresponding date
     */
    static Date fromUnixtimestamp(long timestamp) {
        return Date.from(Instant.ofEpochSecond(timestamp));
    }

    /**
     * Helper to convert a date to a unix timestamp.
     *
     * @param date the date
     * @return the unix timestamp
     */
    static long toUnixtimestamp(Date date) {
        return date.toInstant().getEpochSecond();
    }

    /**
     * Returns the mapping target identifier as a long.
     *
     * @param target the mapping target. May be {@code null}
     * @return the mapping target identifier, or {@code 0} if there is no mapping
     */
    static long getTargetId(Target target) {
        String id = (target != null) ? target.getId() : null;
        return id == null ? 0 : Long.valueOf(id);
    }
}
