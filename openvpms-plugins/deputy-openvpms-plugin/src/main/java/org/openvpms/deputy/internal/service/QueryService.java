/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Path;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.Predicate;
import java.util.Date;
import java.util.List;

/**
 * Query service.
 *
 * @author Tim Anderson
 */
class QueryService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(QueryService.class);

    /**
     * Constructs a {@link QueryService}.
     *
     * @param service the archetype service
     */
    QueryService(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns all events for an area between two dates.
     *
     * @param areaRef the area reference
     * @param from    the start of the range, inclusive
     * @param to      the end of the range, exclusive
     */
    List<Act> getEvents(Reference areaRef, Date from, Date to) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, Archetypes.ROSTER_EVENT);
        Join<Act, Entity> schedule = root.join("schedule");
        schedule.on(builder.equal(schedule.get("entity"), areaRef));
        Path<Date> startTime = root.get("startTime");
        query.where(builder.greaterThanOrEqualTo(startTime, from), builder.lessThan(startTime, to));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getResultList();
    }

    /**
     * Returns the event associated with a roster.
     *
     * @param roster the roster
     * @return the corresponding event, or {@code null} if none exists
     */
    Act getEvent(Roster roster) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, Archetypes.ROSTER_EVENT);
        Join<Act, IMObject> identity = root.join("synchronisation").alias("identity");
        identity.on(builder.equal(identity.get("identity"), Long.toString(roster.getId())));
        query.orderBy(builder.asc(root.get("id")));
        TypedQuery<Act> typedQuery = service.createQuery(query).setMaxResults(1);
        List<Act> acts = typedQuery.getResultList();
        return (!acts.isEmpty()) ? acts.get(0) : null;
    }

    /**
     * Returns the first event for an area that overlaps a date range.
     *
     * @param from    the start of the range
     * @param to      the end of the range
     * @param areaRef the area reference
     * @param userRef the user reference
     * @return the matching events
     */
    Act getOverlappingEvent(Date from, Date to, Reference areaRef, Reference userRef) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, Archetypes.ROSTER_EVENT);
        Join<Act, Entity> schedule = root.join("schedule");
        schedule.on(builder.equal(schedule.get("entity"), areaRef));
        Join<Act, Entity> user;
        if (userRef != null) {
            user = root.join("user");
            user.on(builder.equal(user.get("entity"), userRef));
        } else {
            user = root.leftJoin("user");
        }

        Path<Date> startTime = root.get("startTime");
        Path<Date> endTime = root.get("endTime");

        Predicate expression
                = builder.or(builder.and(builder.lessThan(startTime, from), builder.greaterThan(endTime, from)),
                             builder.and(builder.lessThan(startTime, to), builder.greaterThan(endTime, to)),
                             builder.and(builder.greaterThanOrEqualTo(startTime, from),
                                         builder.lessThanOrEqualTo(endTime, to)));
        if (userRef == null) {
            expression = builder.and(expression, user.isNull());
        }
        query.where(expression);
        query.orderBy(builder.asc(root.get("id")));
        List<Act> list = service.createQuery(query).setMaxResults(1).getResultList();
        return !list.isEmpty() ? list.get(0) : null;
    }

    /**
     * Returns all events for an area not associated with any user that have the same date range.
     *
     * @param from    the start of the range
     * @param to      the end of the range
     * @param areaRef the area reference
     * @return the matching events
     */
    List<Act> getMatchingOpenEvents(Date from, Date to, Reference areaRef) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, Archetypes.ROSTER_EVENT);
        Join<Act, Entity> schedule = root.join("schedule");
        schedule.on(builder.equal(schedule.get("entity"), areaRef));
        Join<Act, Entity> user = root.leftJoin("user");

        query.where(builder.equal(root.get("startTime"), from), builder.equal(root.get("endTime"), to), user.isNull());
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getResultList();
    }

    /**
     * Returns the name for an object.
     *
     * @param reference the object reference
     * @return the object's name, or its id if it cannot be retrieved
     */
    String getName(Reference reference) {
        String name = null;
        try {
            IMObject object = service.get(reference);
            if (object != null) {
                name = object.getName();
            }
        } catch (Throwable exception) {
            log.error("Failed to retrieve object=" + reference, exception);
        }
        if (name == null) {
            name = Long.toString(reference.getId());
        }
        return name;
    }

    /**
     * Determines if an object is active.
     *
     * @param reference the object reference. May be {@code null}
     * @return {@code true} if the corresponding object is active
     */
    boolean isActive(Reference reference) {
        if (reference != null) {
            IMObject object = service.get(reference);
            return (object != null) && object.isActive();
        }
        return false;
    }


}
