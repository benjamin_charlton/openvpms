/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.mapping;

import org.openvpms.deputy.internal.model.organisation.Resource;
import org.openvpms.mapping.model.DefaultTarget;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.openvpms.mapping.model.Targets;

import java.util.ArrayList;
import java.util.List;

/**
 * Deputy mapping targets.
 *
 * @author Tim Anderson
 */
public abstract class DeputyTargets implements Targets {

    /**
     * The display name for the target object type.
     */
    private final String displayName;

    /**
     * Constructs a {@link DeputyTargets}.
     *
     * @param displayName the display name for the target object type
     */
    DeputyTargets(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Returns a display name for the target object type.
     *
     * @return the display name
     */
    @Override
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns a count of the targets matching the criteria.
     *
     * @param mappings the mappings
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of targets matching the criteria
     */
    @Override
    public int count(Mappings mappings, String name, boolean unmapped) {
        int first = 0;
        int max = 500;
        int result = 0;
        for (boolean done = false; !done; first += max) {
            List<Target> targets = getTargets(mappings, name, unmapped, first, max);
            int size = targets.size();
            result += size;
            if (size < max) {
                done = true;
            }
        }
        return result;
    }

    /**
     * Creates a target.
     *
     * @param identity the target object identifier
     * @param name     the target object display name
     * @param active   determines if the target object is active
     * @return a new target
     */
    @Override
    public Target create(String identity, String name, boolean active) {
        return new DefaultTarget(identity, name, active);
    }

    /**
     * Returns the available targets.
     *
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve, or {@code -1} to retrieve all results
     * @param objects     the available objects
     * @return the targets
     */
    List<Target> getTargets(Mappings mappings, String name, boolean unmapped, int firstResult, int maxResults,
                            List<? extends Resource> objects) {
        List<Target> result = new ArrayList<>();
        name = name != null ? name.toLowerCase() : null;
        int matches = 0;
        for (int i = 0; i < objects.size() && (maxResults == -1 || result.size() < maxResults); ++i) {
            Resource object = objects.get(i);
            if (!unmapped || isUnmapped(mappings, object.getId())) {
                String objectName = object.getName();
                if (name == null || (objectName != null && objectName.toLowerCase().contains(name))) {
                    if (matches >= firstResult) {
                        Target target = new DefaultTarget(object.getId(), objectName, object.getActive());
                        result.add(target);
                    }
                    matches++;
                }
            }
        }
        return result;
    }

    /**
     * Determines if a target is unmapped.
     *
     * @param mappings the mappings
     * @param target   the target identifier
     * @return {@code true} if it is unmapped
     */
    private boolean isUnmapped(Mappings mappings, long target) {
        return mappings.getMapping(Long.toString(target)) == null;
    }
}
