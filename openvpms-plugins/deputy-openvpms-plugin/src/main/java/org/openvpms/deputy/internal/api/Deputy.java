/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.api;

import org.openvpms.deputy.internal.model.organisation.Company;
import org.openvpms.deputy.internal.model.organisation.Employee;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Deputy API.
 *
 * @author Tim Anderson
 */
@Path("/api/v1")
public interface Deputy {

    /**
     * Returns the companies.
     *
     * @return the companies
     */
    @GET
    @Path("/resource/Company")
    @Produces(MediaType.APPLICATION_JSON)
    List<Company> getCompanies();

    /**
     * Queries operational units.
     *
     * @param query the query
     * @return the matching operational units
     */
    @POST
    @Path("/resource/OperationalUnit/QUERY")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    List<OperationalUnit> getOperationalUnits(Query query);

    /**
     * Returns an operational unit given its identifier.
     *
     * @param id the operational unit identifier
     * @return the corresponding operational unit
     * @throws NotFoundException if the operational unit does not exist
     */
    @GET
    @Path("/resource/OperationalUnit/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    OperationalUnit getOperationalUnit(@PathParam("id") long id);

    /**
     * Queries employees.
     *
     * @param query the query
     * @return the matching employees
     */
    @POST
    @Path("/resource/Employee/QUERY")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    List<Employee> getEmployees(Query query);

    /**
     * Returns an employee given its identifier.
     *
     * @param id the employee identifier
     * @return the corresponding employee
     * @throws NotFoundException if the employee does not exist
     */
    @GET
    @Path("/resource/Employee/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    Employee getEmployee(@PathParam("id") long id);

    /**
     * Queries rosters.
     *
     * @param query the query
     * @return the matching rosters
     */
    @POST
    @Path("/resource/Roster/QUERY")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    List<Roster> getRosters(Query query);

    /**
     * Returns a roster given its identifier.
     *
     * @param id the roster identifier
     * @return the roster
     * @throws NotFoundException if the roster is not found
     */
    @GET
    @Path("/resource/Roster/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    Roster getRoster(@PathParam("id") long id);

    /**
     * Creates a new roster, or updates an existing roster.
     * <p/>
     * To update an existing roster, the {@link RosterData#getId()} must be set.
     *
     * @param data the roster data
     * @return the new roster
     * @throws BadRequestException if an overlap is detected
     */
    @POST
    @Path("/supervise/roster")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Roster roster(RosterData data);

    /**
     * Deletes a roster.
     *
     * @param id the roster identifier
     * @return the deletion confirmation message
     * @throws NotFoundException if the roster does not exist
     */
    @DELETE
    @Path("/resource/Roster/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    String removeRoster(@PathParam("id") long id);

}
