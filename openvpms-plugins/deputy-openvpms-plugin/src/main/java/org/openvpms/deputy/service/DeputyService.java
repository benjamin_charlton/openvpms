/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.service;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.service.DeputyServiceImpl;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.service.MappingProvider;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.service.archetype.ArchetypeInstaller;
import org.openvpms.plugin.service.archetype.IMObjectListener;
import org.openvpms.plugin.service.config.ConfigurableService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Deputy Service.
 *
 * @author Tim Anderson
 */
@Component(service = {MappingProvider.class, ConfigurableService.class, IMObjectListener.class},
        immediate = true)
public class DeputyService implements MappingProvider, ConfigurableService, IMObjectListener {

    /**
     * The rostering service implementation.
     */
    private DeputyServiceImpl deputyService;

    /**
     * The archetype service.
     */
    private ArchetypeService service;

    /**
     * The archetype installer.
     */
    private ArchetypeInstaller installer;

    /**
     * The mapping service.
     */
    private MappingService mappingService;

    /**
     * The encryptor, used for decrypting passwords.
     */
    private PasswordEncryptor encryptor;

    /**
     * The configuration.
     */
    private IMObject config;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DeputyService.class);

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return DeputyServiceImpl.NAME;
    }

    /**
     * Returns the archetype that this service is configured with.
     *
     * @return the archetype short name
     */
    @Override
    public String getArchetype() {
        return Archetypes.PLUGIN;
    }

    /**
     * Returns the mappings required to support synchronisation.
     *
     * @return the mappings
     */
    @Override
    public List<Mappings<?>> getMappings() {
        return getDeputyService().getMappings();
    }

    /**
     * Invoked when the service is registered, and each time the configuration is updated.
     *
     * @param config may be {@code null}, if no configuration exists, or the configuration is deactivated or removed
     */
    @Override
    public synchronized void setConfiguration(IMObject config) {
        this.config = config;
        if (deputyService != null) {
            deputyService.setConfiguration(config);
        }
    }

    /**
     * Returns the configuration.
     *
     * @return the configuration. May be {@code null}
     */
    @Override
    public synchronized IMObject getConfiguration() {
        return config;
    }

    /**
     * Registers the archetype service.
     *
     * @param service the archetype service
     */
    @Reference
    public synchronized void setArchetypeService(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Registers the archetype installer.
     *
     * @param installer the installer
     */
    @Reference
    public synchronized void setArchetypeInstaller(ArchetypeInstaller installer) {
        this.installer = installer;
    }

    /**
     * Registers the mapping service.
     *
     * @param mappingService the mapping service
     */
    @Reference
    public synchronized void setMappingService(MappingService mappingService) {
        this.mappingService = mappingService;
    }

    /**
     * Registers the password encryptor, used for decrypting passwords.
     *
     * @param encryptor the encryptor
     */
    @Reference
    public synchronized void setEncryptor(PasswordEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    /**
     * The archetypes to receive notifications for.
     *
     * @return the archetypes
     */
    @Override
    public String[] getArchetypes() {
        return new String[]{Archetypes.ROSTER_EVENT};
    }

    /**
     * Invoked when an object is updated.
     *
     * @param object the object
     */
    @Override
    public void updated(IMObject object) {
        if (object instanceof Act) {
            DeputyServiceImpl service;
            synchronized (this) {
                service = deputyService;
            }
            if (service != null) {
                service.eventUpdated((Act) object);
            }
        }
    }

    /**
     * invoked when an object is removed.
     *
     * @param object the object
     */
    @Override
    public void removed(IMObject object) {
        if (object instanceof Act) {
            DeputyServiceImpl service;
            synchronized (this) {
                service = deputyService;
            }
            if (service != null) {
                service.eventRemoved((Act) object);
            }
        }
    }

    /**
     * Activates the service.
     */
    @Activate
    public synchronized void activate() {
        try {
            if (deputyService != null) {
                deputyService.dispose();
            }
            deputyService = new DeputyServiceImpl(service, installer, mappingService, encryptor);
            deputyService.setConfiguration(config);
        } catch (Throwable exception) {
            log.error("Failed to activate DeputyService", exception);
            throw exception;
        }
    }

    /**
     * Deactivates the service.
     */
    @Deactivate
    public synchronized void deactivate() {
        if (deputyService != null) {
            try {
                deputyService.dispose();
            } catch (Throwable exception) {
                log.error("Failed to destroy the DeputyService", exception);
            }
            deputyService = null;
        }
    }

    /**
     * Returns the service implementation.
     *
     * @return the service implementation
     */
    private DeputyServiceImpl getDeputyService() {
        DeputyServiceImpl result;
        synchronized (this) {
            result = deputyService;
        }
        if (result == null) {
            throw new IllegalStateException("Deputy service not configured");
        }
        return result;
    }

}
