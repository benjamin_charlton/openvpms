/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.mapping.model.DefaultTarget;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Targets;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.openvpms.plugin.internal.service.mapping.MappingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.NotFoundException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.deputy.internal.service.DeputyTestHelper.createRoster;

/**
 * Tests the {@link SynchronisationManager}.
 *
 * @author Tim Anderson
 */
public class SynchronisationManagerTestCase extends AbstractDeputyTest {

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The archetype service.
     */
    private ArchetypeService service;

    /**
     * The query service.
     */
    private QueryService queryService;

    /**
     * The mapped areas.
     */
    private Mappings<Entity> areas;

    /**
     * The mapped users.
     */
    private Mappings<User> users;

    /**
     * Test location.
     */
    private Party location;

    /**
     * Test area 1.
     */
    private Entity area1;

    /**
     * Test area 2.
     */
    private Entity area2;

    /**
     * Test area 3.
     */
    private Entity area3;


    /**
     * Test user 1.
     */
    private User user1;

    /**
     * Test user 2.
     */
    private User user2;

    /**
     * Test user 3.
     */
    private User user3;

    /**
     * Test user 4.
     */
    private User user4;

    /**
     * Start id for rosters.
     */
    private long startId;

    /**
     * Area 1 id.
     */
    private static final int AREA1 = 1;

    /**
     * Area 2 id.
     */
    private static final int AREA2 = 2;

    /**
     * Area 3 id.
     */
    private static final int AREA3 = 3;

    /**
     * Deputy employee 1 id.
     */
    private static final int EMPLOYEE1 = 1;

    /**
     * Deputy employee 2 id.
     */
    private static final int EMPLOYEE2 = 2;

    /**
     * Deputy employee 3 id.
     */
    private static final int EMPLOYEE3 = 3;

    /**
     * Deputy employee 4 id.
     */
    private static final int EMPLOYEE4 = 4;

    /**
     * Deputy employee 5 id.
     */
    private static final int EMPLOYEE5 = 5;


    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        Party practice = practiceService.getPractice();
        if (practiceService.getServiceUser() == null) {
            IMObjectBean bean = getBean(practice);
            bean.setTarget("serviceUser", TestHelper.createUser());
        }

        service = new PluginArchetypeService((IArchetypeRuleService) getArchetypeService(), getLookupService(),
                                             practiceService);
        queryService = new QueryService(service);

        MappingService mappingService = new MappingServiceImpl(service);
        IMObject configuration = mappingService.createMappingConfiguration(Entity.class);
        areas = mappingService.createMappings(configuration, Entity.class, Archetypes.ROSTER_AREA,
                                              "Areas", Mockito.mock(Targets.class));
        users = mappingService.createMappings(configuration, User.class, Archetypes.USER,
                                              "Users", Mockito.mock(Targets.class));

        location = TestHelper.createLocation();
        area1 = ScheduleTestHelper.createRosterArea(location);
        area2 = ScheduleTestHelper.createRosterArea(location);
        area3 = ScheduleTestHelper.createRosterArea(location);

        user1 = TestHelper.createUser("User", "1");
        user2 = TestHelper.createUser("User", "2");
        user3 = TestHelper.createUser("User", "3");
        user4 = TestHelper.createUser("User", "4");

        areas.add(area1, new DefaultTarget(Long.toString(AREA1), "Area 1", true));
        users.add(user1, new DefaultTarget(Long.toString(EMPLOYEE1), "Employee 1", true));

        startId = RandomUtils.nextLong();
    }

    /**
     * Tests synchronisation where all changes are made in Deputy.
     */
    @Test
    public void testSyncFromDeputy() {
        areas.add(area2, new DefaultTarget(Long.toString(AREA2), "Area 2", true));
        users.add(user2, new DefaultTarget(Long.toString(EMPLOYEE2), "Employee 2", true));

        List<Roster> rosters = new ArrayList<>();
        List<Roster> rosters1 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE1, AREA1);
        List<Roster> rosters2 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE2, AREA2);
        List<Roster> rosters3 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE3, AREA1); // not mapped
        List<Roster> rosters4 = createRosters(5, "2019-08-12", "09:00", "17:30", 0, AREA1);         // open shifts
        List<Roster> rosters5 = createRosters(2, "2019-08-17", "09:00", "13:30", EMPLOYEE4, AREA2);

        rosters.addAll(rosters1);
        rosters.addAll(rosters2);
        rosters.addAll(rosters3);
        rosters.addAll(rosters4);
        rosters.addAll(rosters5);

        TestDeputy deputy = new TestDeputy(rosters);

        LocalDate date = LocalDate.of(2019, 8, 12);

        QueryService queryService = new QueryService(service);
        SynchronisationManager manager = new SynchronisationManager(5, areas, users, deputy, queryService, service);
        manager.run(date);

        checkEvents(rosters1, user1, area1);
        checkEvents(rosters2, user2, area2);
        checkNoEvents(rosters3);
        checkEvents(rosters4, null, area1);
        checkNoEvents(rosters5);

        // now add a mapping for EMPLOYEE3 and resync
        users.add(user3, new DefaultTarget(Long.toString(EMPLOYEE3), "Employee 3", true));
        manager.run(date);

        checkEvents(rosters1, user1, area1);
        checkEvents(rosters2, user2, area2);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, null, area1);
        checkNoEvents(rosters5);

        // change employees and resync
        changeEmployee(rosters1, 0);          // make open
        changeEmployee(rosters4, EMPLOYEE4);
        manager.run(date);
        checkEvents(rosters1, null, area1);
        checkEvents(rosters2, user2, area2);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, null, area1, SyncStatus.ERROR, "DEPUTY-0051: Cannot synchronise this shift as the " +
                                                             "Deputy Employee 'Employee 4' is not mapped to an " +
                                                             "OpenVPMS User");
        checkNoEvents(rosters5);

        // now add a mapping for EMPLOYEE4 and resync
        users.add(user4, new DefaultTarget(Long.toString(EMPLOYEE4), "Employee 4", true));
        manager.run(date);

        checkEvents(rosters1, null, area1);
        checkEvents(rosters2, user2, area2);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, user4, area1);
        checkNoEvents(rosters5);

        // change rosters1 to point to area3
        changeArea(rosters1, AREA3);
        manager.run(date);
        checkEvents(rosters1, null, area1, SyncStatus.ERROR, "DEPUTY-0052: Cannot synchronise this shift as the " +
                                                             "Deputy Area 'Area 3' is not mapped to an " +
                                                             "OpenVPMS Roster Area");
        checkEvents(rosters2, user2, area2);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, user4, area1);
        checkNoEvents(rosters5);

        // add mapping for AREA3
        areas.add(area3, new DefaultTarget(Long.toString(AREA3), "Area 3", true));
        manager.run(date);
        checkEvents(rosters1, null, area3);
        checkEvents(rosters2, user2, area2);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, user4, area1);
        checkNoEvents(rosters5);

        // now advance the data and verify the roster5 rosters are processed
        manager.run(LocalDate.of(2019, 8, 17));
        checkEvents(rosters1, null, area3);
        checkEvents(rosters2, user2, area2);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, user4, area1);
        checkEvents(rosters5, user4, area2);
    }

    /**
     * Tests synchronisation where all changes are made in OpenVPMS.
     */
    @Test
    public void testSyncFromOpenVPMS() {
        areas.add(area2, new DefaultTarget(Long.toString(AREA2), "Area 2", true));
        users.add(user2, new DefaultTarget(Long.toString(EMPLOYEE2), "Employee 2", true));

        List<Act> events1 = createEvents(5, "2019-08-12", "09:00", "17:30", user1, area1);
        List<Act> events2 = createEvents(5, "2019-08-12", "09:00", "17:30", user2, area2);
        List<Act> events3 = createEvents(5, "2019-08-12", "09:00", "17:30", user3, area1);   // not mapped
        List<Act> events4 = createEvents(5, "2019-08-12", "09:00", "17:30", null, area1);    // open shifts
        List<Act> events5 = createEvents(2, "2019-08-17", "09:00", "13:30", user4, area2);   // not mapped

        TestDeputy deputy = new TestDeputy();

        LocalDate date = LocalDate.of(2019, 8, 12);

        QueryService queryService = new QueryService(service);
        SynchronisationManager manager = new SynchronisationManager(5, areas, users, deputy, queryService, service);
        manager.run(date);

        checkEvents(events1, user1, area1, EMPLOYEE1, AREA1, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkNoRosters(events3, deputy);
        checkEvents(events4, null, area1, 0, AREA1, deputy);
        checkNoRosters(events5, deputy);

        // now add a mapping for EMPLOYEE3 and resync
        users.add(user3, new DefaultTarget(Long.toString(EMPLOYEE3), "Employee 3", true));
        manager.run(date);

        checkEvents(events1, user1, area1, EMPLOYEE1, AREA1, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkEvents(events3, user3, area1, EMPLOYEE3, AREA1, deputy);
        checkEvents(events4, null, area1, 0, AREA1, deputy);
        checkNoRosters(events5, deputy);

        // change employees and resync
        changeUser(events1, null);          // make open
        changeUser(events4, user4);
        manager.run(date);
        checkEvents(events1, null, area1, 0, AREA1, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkEvents(events3, user3, area1, EMPLOYEE3, AREA1, deputy);
        checkEvents(events4, user4, area1, 0, AREA1, SyncStatus.ERROR,
                    "DEPUTY-0050: Cannot synchronise this shift with Deputy as User 4 is not mapped to an Employee " +
                    "in Deputy", deputy);
        checkNoRosters(events5, deputy);

        // now add a mapping for EMPLOYEE4 and resync
        users.add(user4, new DefaultTarget(Long.toString(EMPLOYEE4), "Employee 4", true));
        manager.run(date);

        checkEvents(events1, null, area1, 0, AREA1, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkEvents(events3, user3, area1, EMPLOYEE3, AREA1, deputy);
        checkEvents(events4, user4, area1, EMPLOYEE4, AREA1, deputy);
        checkNoRosters(events5, deputy);

        // change events1 to point to area3. As area3 is not mapped, Deputy won't be updated.
        changeArea(events1, area3);
        manager.run(date);

        checkEvents(events1, null, area3, 0, AREA1, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkEvents(events3, user3, area1, EMPLOYEE3, AREA1, deputy);
        checkEvents(events4, user4, area1, EMPLOYEE4, AREA1, deputy);
        checkNoRosters(events5, deputy);

        // add mapping for AREA3
        areas.add(area3, new DefaultTarget(Long.toString(AREA3), "Area 3", true));
        manager.run(date);

        checkEvents(events1, null, area3, 0, AREA3, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkEvents(events3, user3, area1, EMPLOYEE3, AREA1, deputy);
        checkEvents(events4, user4, area1, EMPLOYEE4, AREA1, deputy);
        checkNoRosters(events5, deputy);

        // now advance the data and verify the roster5 rosters are processed
        manager.run(LocalDate.of(2019, 8, 17));
        checkEvents(events1, null, area3, 0, AREA3, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
        checkEvents(events3, user3, area1, EMPLOYEE3, AREA1, deputy);
        checkEvents(events4, user4, area1, EMPLOYEE4, AREA1, deputy);
        checkEvents(events5, user4, area2, EMPLOYEE4, AREA2, deputy);
    }

    /**
     * Verifies that rosters can be retrieved in pages. This is required as Deputy will return at most 500 records at a
     * time.
     */
    @Test
    public void testRosterPaging() {
        User user5 = TestHelper.createUser();
        users.add(user2, new DefaultTarget(Long.toString(EMPLOYEE2), "Employee 2", true));
        users.add(user3, new DefaultTarget(Long.toString(EMPLOYEE3), "Employee 3", true));
        users.add(user4, new DefaultTarget(Long.toString(EMPLOYEE4), "Employee 4", true));
        users.add(user5, new DefaultTarget(Long.toString(EMPLOYEE5), "Employee 5", true));

        List<Roster> rosters1 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE1, AREA1);
        List<Roster> rosters2 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE2, AREA1);
        List<Roster> rosters3 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE3, AREA1);
        List<Roster> rosters4 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE4, AREA1);
        List<Roster> rosters5 = createRosters(5, "2019-08-12", "09:00", "17:30", EMPLOYEE5, AREA1);

        List<Roster> rosters = new ArrayList<>();
        rosters.addAll(rosters1);
        rosters.addAll(rosters2);
        rosters.addAll(rosters3);
        rosters.addAll(rosters4);
        rosters.addAll(rosters5);

        TestDeputy deputy = new TestDeputy(rosters);

        int maxResults = 2; // pull in 2 rosters per area query. As there is a roster per user in area 1 per day,
        // 5 queries should be issued for each day

        SynchronisationManager manager = new SynchronisationManager(5, areas, users, deputy, queryService, service,
                                                                    maxResults);

        LocalDate date = LocalDate.of(2019, 8, 12);
        manager.run(date);

        checkEvents(rosters1, user1, area1);
        checkEvents(rosters2, user2, area1);
        checkEvents(rosters3, user3, area1);
        checkEvents(rosters4, user4, area1);
        checkEvents(rosters5, user5, area1);
    }

    /**
     * Verifies that inactive roster areas aren't synced.
     */
    @Test
    public void testInactiveArea() {
        areas.add(area2, new DefaultTarget(Long.toString(AREA2), "Area 2", true));
        users.add(user2, new DefaultTarget(Long.toString(EMPLOYEE2), "Employee 2", true));

        List<Act> events1 = createEvents(5, "2019-08-12", "09:00", "17:30", user1, area1);
        List<Act> events2 = createEvents(5, "2019-08-12", "09:00", "17:30", user2, area2);

        // make area2 inactive
        area2.setActive(false);
        save(area2);

        TestDeputy deputy = new TestDeputy();

        LocalDate date = LocalDate.of(2019, 8, 12);

        QueryService queryService = new QueryService(service);
        SynchronisationManager manager = new SynchronisationManager(5, areas, users, deputy, queryService, service);
        manager.run(date);

        checkEvents(events1, user1, area1, EMPLOYEE1, AREA1, deputy);
        checkNoRosters(events2, deputy);

        // make area2 active again
        area2.setActive(true);
        save(area2);

        manager.run(date);

        checkEvents(events1, user1, area1, EMPLOYEE1, AREA1, deputy);
        checkEvents(events2, user2, area2, EMPLOYEE2, AREA2, deputy);
    }

    /**
     * Verifies that inactive users are synced. This is required as their shifts will still need to be fulfilled by
     * someone, or the shifts removed.
     * <p/>
     * NOTE: synchronisation is done at the area level, so if both the area and user are inactive, no synchronisation
     * will occur.
     */
    @Test
    public void testInactiveUser() {
        List<Act> events1 = createEvents(5, "2019-08-12", "09:00", "17:30", user1, area1);

        // make user1 inactive
        user1.setActive(false);
        save(user1);

        TestDeputy deputy = new TestDeputy();

        LocalDate date = LocalDate.of(2019, 8, 12);

        QueryService queryService = new QueryService(service);
        SynchronisationManager manager = new SynchronisationManager(5, areas, users, deputy, queryService, service);
        manager.run(date);

        // verify the shifts synced despite user1 being inactive
        checkEvents(events1, user1, area1, EMPLOYEE1, AREA1, deputy);
    }

    /**
     * Changes the user for a set of events.
     *
     * @param events the events
     * @param user   the user
     */
    private void changeUser(List<Act> events, User user) {
        for (Act event : events) {
            IMObjectBean bean = getBean(get(event));
            if (user == null) {
                bean.removeValues("user");
            } else {
                bean.setTarget("user", user);
            }
            bean.save();
        }
    }

    /**
     * Changes the area for a set of events.
     *
     * @param events the events
     * @param area   the area
     */
    private void changeArea(List<Act> events, Entity area) {
        for (Act event : events) {
            IMObjectBean bean = getBean(get(event));
            bean.setTarget("schedule", area);
            bean.save();
        }
    }

    /**
     * Changes the employee on a set of rosters.
     *
     * @param rosters  the rosters
     * @param employee the employee or {@code 0} to indicate an open roster
     */
    private void changeEmployee(List<Roster> rosters, long employee) {
        for (Roster roster : rosters) {
            roster.setEmployee(employee);
            roster.setModified(new Date());
        }
    }

    /**
     * Changes the area for a set of rosters.
     *
     * @param rosters the rosters
     * @param area    the area
     */
    private void changeArea(List<Roster> rosters, long area) {
        for (Roster roster : rosters) {
            roster.setOperationalUnit(area);
            roster.setModified(new Date());
        }
    }

    /**
     * Verifies that there are no events for a set of rosters.
     *
     * @param rosters the rosters
     */
    private void checkNoEvents(List<Roster> rosters) {
        for (Roster roster : rosters) {
            assertNull(queryService.getEvent(roster));
        }
    }

    /**
     * Checks the events for a set of rosters.
     *
     * @param rosters the rosters
     * @param user    the expected user. May be {@code null}
     * @param area    the expected area
     */
    private List<Act> checkEvents(List<Roster> rosters, User user, Entity area) {
        return checkEvents(rosters, user, area, SyncStatus.SYNC, null);
    }

    /**
     * Checks the events for a set of rosters.
     *
     * @param rosters the rosters
     * @param user    the expected user. May be {@code null}
     * @param area    the expected area
     * @param status  the expected status
     * @param error   the expected error. May be {@code null}
     */
    private List<Act> checkEvents(List<Roster> rosters, User user, Entity area, String status, String error) {
        List<Act> result = new ArrayList<>();
        for (Roster roster : rosters) {
            Act event = queryService.getEvent(roster);
            assertNotNull(event);
            Date startTime = DeputyHelper.fromUnixtimestamp(roster.getStartTime());
            Date endTime = DeputyHelper.fromUnixtimestamp(roster.getEndTime());
            checkEvent(event, startTime, endTime, user, area, location, roster.getId(), status, error);
            result.add(event);
        }
        return result;
    }

    /**
     * Checks events and their associated rosters.
     *
     * @param events          the events
     * @param user            the expected user. May be {@code null}
     * @param area            the expected area
     * @param employee        the expected employee or zero for an open roster
     * @param operationalUnit the expected operational unit
     * @param deputy          the Deputy client
     */
    private void checkEvents(List<Act> events, User user, Entity area, long employee, long operationalUnit,
                             Deputy deputy) {
        checkEvents(events, user, area, employee, operationalUnit, SyncStatus.SYNC, null, deputy);
    }

    /**
     * Checks events and their associated rosters.
     *
     * @param events          the events
     * @param user            the expected user. May be {@code null}
     * @param area            the expected area
     * @param employee        the expected employee or zero for an open roster
     * @param operationalUnit the expected operational unit
     * @param status          the expected status
     * @param error           the expected error. May be {@code null}
     * @param deputy          the Deputy client
     */
    private void checkEvents(List<Act> events, User user, Entity area, long employee, long operationalUnit,
                             String status, String error, Deputy deputy) {
        for (Act event : events) {
            event = get(event);
            assertNotNull(event);
            IMObjectBean bean = getBean(event);
            assertEquals(user, bean.getTarget("user"));
            assertEquals(area, bean.getTarget("schedule"));
            long id = checkSynchronisationId(event, status, error);

            assertTrue(id > 0);
            Roster roster = deputy.getRoster(id);
            // need to strip off timezone.
            Date startTime = new Timestamp(DeputyHelper.fromUnixtimestamp(roster.getStartTime()).getTime());
            Date endTime = new Timestamp(DeputyHelper.fromUnixtimestamp(roster.getEndTime()).getTime());
            assertEquals(event.getActivityStartTime(), startTime);
            assertEquals(event.getActivityEndTime(), endTime);
            assertEquals(employee, roster.getEmployee());
            assertEquals(operationalUnit, roster.getOperationalUnit());
        }
    }

    /**
     * Verifies that there are no rosters for a set of events.
     *
     * @param events the events
     * @param deputy the Deputy client
     */
    private void checkNoRosters(List<Act> events, Deputy deputy) {
        for (Act event : events) {
            event = get(event);
            assertNotNull(event);
            try {
                deputy.getRoster(DeputyHelper.getRosterId(getBean(event)));
                fail("Expected NotFoundException");
            } catch (NotFoundException exception) {
                // expected
            }
        }
    }

    /**
     * Creates a number of rosters on subsequent days.
     *
     * @param count           the number to create
     * @param fromDate        the starting date
     * @param startTime       the start time for each roster
     * @param endTime         the end time for each roster
     * @param employee        the Deputy employee id
     * @param operationalUnit the Deputy operational unit id
     * @return the rosters
     */
    private List<Roster> createRosters(int count, String fromDate, String startTime, String endTime, long employee,
                                       long operationalUnit) {
        List<Roster> result = new ArrayList<>();
        Date start = DeputyTestHelper.getISODatetime(fromDate + "T" + startTime + ":00+10:00");
        Date end = DeputyTestHelper.getISODatetime(fromDate + "T" + endTime + ":00+10:00");
        for (int i = 0; i < count; ++i) {
            Roster roster = createRoster(startId++, start, end, employee, operationalUnit);
            start = DateRules.getDate(start, 1, DateUnits.DAYS);
            end = DateRules.getDate(end, 1, DateUnits.DAYS);
            result.add(roster);
        }
        return result;
    }

    /**
     * Creates a number of events on subsequent days.
     *
     * @param count     the number to create
     * @param fromDate  the starting date
     * @param startTime the start time for each roster
     * @param endTime   the end time for each roster
     * @param user      the user. May be {@code null}
     * @param area      the roster area
     * @return the rosters
     */
    private List<Act> createEvents(int count, String fromDate, String startTime, String endTime, User user,
                                   Entity area) {
        List<Act> result = new ArrayList<>();
        Date start = TestHelper.getDatetime(fromDate + " " + startTime + ":00");
        Date end = TestHelper.getDatetime(fromDate + " " + endTime + ":00");
        for (int i = 0; i < count; ++i) {
            Act event = createEvent(start, end, user, area, location, -1);
            start = DateRules.getDate(start, 1, DateUnits.DAYS);
            end = DateRules.getDate(end, 1, DateUnits.DAYS);
            result.add(event);
        }
        return result;
    }

}
