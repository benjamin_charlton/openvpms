/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import org.apache.felix.framework.Logger;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ExportPackages} class.
 *
 * @author Tim Anderson
 */
public class ExportPackagesTestCase {

    /**
     * Tests export packages.
     */
    @Test
    public void testExportPackages() {
        ExportPackages packages = new ExportPackages(new Logger());
        Map<String, String> map = packages.getPackages();

        // verify OpenVPMS frameworks packages are included
        assertTrue(map.containsKey("org.openvpms.component.model.act"));

        // verify the most recent commons-io version is exported. The commons-io jar exports both a 1.4.9999 and 2.6.0
        // version
        assertEquals("2.6.0", map.get("org.apache.commons.io"));

        // verify that a hardcoded version no. for javax.annotation is used. This should correspond to that
        // of jakarta.annotation-api
        assertEquals("1.3.5", map.get("javax.annotation"));
        assertEquals("1.3.5", map.get("javax.annotation.security"));
        assertEquals("1.3.5", map.get("javax.annotation.sql"));

        // verify that there is com.google.code.findbugs:jsr305 has not been included if it's on the classpath
        // See OVPMS-2622
        assertNull(map.get("javax.annotation.meta"));
    }
}
