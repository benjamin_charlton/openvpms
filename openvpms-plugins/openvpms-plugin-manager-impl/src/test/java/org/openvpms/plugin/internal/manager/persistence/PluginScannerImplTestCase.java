/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.persistence;

import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.plugin.internal.manager.PluginHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link PluginScannerImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PluginScannerImplTestCase extends ArchetypeServiceTest {

    /**
     * Test deployment directory.
     */
    @Rule
    public TemporaryFolder dir = new TemporaryFolder();

    /**
     * The plugin DAO.
     */
    @Autowired
    private PluginDAO pluginDAO;


    /**
     * Sets up the test case.
     *
     * @throws IOException for any I/O error
     */
    @Before
    public void setUp() throws IOException {
        PluginHelper.removeAll(pluginDAO);
    }

    /**
     * Tests the plugin scanner.
     *
     * @throws Exception for any error
     */
    @Test
    public void testPluginScanner() throws Exception {
        PluginScannerImpl scanner = new PluginScannerImpl(dir.getRoot(), pluginDAO);
        Collection<DeploymentUnit> scan1 = scanner.scan();
        assertEquals(0, scan1.size());

        String key1 = PluginHelper.deploy(pluginDAO, "openvpms-test-plugin");

        Collection<DeploymentUnit> scan2 = scanner.scan();
        assertEquals(1, scan2.size());
        checkDeploymentUnit(scan2.iterator().next(), key1);

        Collection<DeploymentUnit> scan3 = scanner.scan();
        assertEquals(0, scan3.size());

        String key2 = PluginHelper.deploy(pluginDAO, "openvpms-test-servlet-plugin");

        Collection<DeploymentUnit> scan4 = scanner.scan();
        assertEquals(1, scan4.size());
        DeploymentUnit servletUnit = scan4.iterator().next();
        checkDeploymentUnit(servletUnit, key2);

        // now remove the servlet deployment. Note that this doesn't remove the plugin.
        // This is because the plugin will be installed before the deployment unit is removed
        scanner.remove(servletUnit);

        // verify the servlet is returned in the next scan
        Collection<DeploymentUnit> scan5 = scanner.scan();
        assertEquals(1, scan5.size());
        checkDeploymentUnit(scan5.iterator().next(), key2);

        // now reset the scanner. Both deployments should be returned
        scanner.reset();

        Collection<DeploymentUnit> scan6 = scanner.scan();
        assertEquals(2, scan6.size());
    }

    /**
     * Verifies a deployment unit matches that expected.
     *
     * @param unit the deployment unit
     * @param key  the plugin key
     */
    private void checkDeploymentUnit(DeploymentUnit unit, String key) {
        Plugin plugin = pluginDAO.getPlugin(key);
        assertNotNull(plugin);
        File path = unit.getPath();
        assertEquals(plugin.getVersion() + "-" + plugin.getName(), path.getName());
        Date created = plugin.getCreated();
        assertNotNull(created);

        // verify the created time is used to populate the file modification timestamp
        assertEquals(0, DateUtils.truncatedCompareTo(created, new Date(path.lastModified()), Calendar.SECOND));
    }
}
