/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Plugin test helper methods.
 *
 * @author Tim Anderson
 */
public class PluginHelper {

    /**
     * Returns the path to the plugin install.
     *
     * @return the path
     */
    public static String getPluginDir() {
        String relPath = PluginHelper.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        File dir = new File(relPath + "../../target/test/WEB-INF/plugins");

        assertTrue("Plugin installation at " + dir.getPath() + " not found ", dir.exists());
        return dir.getAbsolutePath();
    }

    /**
     * Returns the servlet root for testing purposes.
     *
     * @return the servlet root path, as a URI
     */
    public static String getServletRoot() {
        String relPath = PluginHelper.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        File dir = new File(relPath + "../../target/test");
        return dir.toURI().toString();
    }

    /**
     * Removes all plugins.
     *
     * @param dao the plugin DAO
     */
    public static void removeAll(PluginDAO dao) {
        Iterator<Plugin> plugins = dao.getPlugins();
        while (plugins.hasNext()) {
            dao.remove(plugins.next().getKey());
        }
    }

    /**
     * Deploy all plugins in the plugin-staging directory.
     *
     * @param dao the DAO to save plugins to
     * @throws IOException for any I/O error
     */
    public static void deployAll(PluginDAO dao) throws IOException {
        File dir = getStagingDir();
        File[] files = dir.listFiles((dir1, name) -> name.endsWith(".jar"));
        deploy(dao, files);
    }

    /**
     * Deploys a plugin.
     *
     * @param dao the plugin DAO
     * @param pathPrefix the plugin file name prefix
     * @return the plugin key
     * @throws IOException for any I/O error
     */
    public static String deploy(PluginDAO dao, String pathPrefix) throws IOException {
        File dir = getStagingDir();
        File[] files = dir.listFiles((dir1, name) -> name.startsWith(pathPrefix));
        if (files == null || files.length != 1) {
            throw new IOException("No single file matching " + pathPrefix);
        }
        return deploy(dao, files[0]);
    }

    /**
     * Deploys a plugin.
     *
     * @param dao  the plugin DAO
     * @param file the plugin file
     * @return the plugin key
     * @throws FileNotFoundException if the file doesn't exit
     */
    public static String deploy(PluginDAO dao, File file) throws FileNotFoundException {
        String key = OsgiHeaderUtil.getPluginKey(file);
        dao.save(key, file.getName(), new FileInputStream(file));
        return key;
    }

    /**
     * Deploys plugins from files.
     *
     * @param dao   the plugin DAO
     * @param files the files
     * @throws FileNotFoundException if there is nothing to deploy
     */
    private static void deploy(PluginDAO dao, File[] files) throws FileNotFoundException {
        int count = 0;
        if (files != null) {
            for (File file : files) {
                count++;
                deploy(dao, file);
            }
        }
        if (count == 0) {
            throw new FileNotFoundException();
        }
    }

    /**
     * Returns the plugin staging directory.
     *
     * @return the staging directory
     */
    private static File getStagingDir() {
        String relPath = PluginHelper.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        return new File(relPath + "../../target/plugin-staging");
    }

}
