/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.openvpms.plugin.internal.manager.ServletContainerContext;
import org.openvpms.version.Version;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Implementation of {@link WebResourceIntegration}, lifted from the Atlassian Reference Application.
 *
 * @author Tim Anderson
 */
public class WebResourceIntegrationImpl implements WebResourceIntegration {

    /**
     * Calculates hash of all the plugins with i18n resources.
     */
    public static class I18nHasher {
        private final ResettableLazyReference<String> hashLazyReference;

        I18nHasher(PluginAccessor pluginAccessor, PluginEventManager eventManager) {
            hashLazyReference = new ResettableLazyReference<String>() {
                @Override
                protected String create() throws Exception {
                    // Tree Set produces consistent ordering and hashing.
                    Set<String> versions = new TreeSet<>();
                    for (Plugin plugin : pluginAccessor.getEnabledPlugins()) {
                        // It would be better to count only plugins that actually contain i18n resources, but
                        // for simplicity counting all the plugins.
                        versions.add(plugin.getKey() + " v. " + plugin.getPluginInformation().getVersion());
                    }
                    return "" + versions.hashCode();
                }
            };
            eventManager.register(this);
        }

        public String get() {
            return hashLazyReference.get();
        }

        @PluginEventListener
        public void onPluginDisabled(final PluginDisabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginEnabled(final PluginEnabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginModuleEnabled(final PluginModuleEnabledEvent event) {
            hashLazyReference.reset();
        }

        @PluginEventListener
        public void onPluginModuleDisabled(final PluginModuleDisabledEvent event) {
            hashLazyReference.reset();
        }
    }

    private final PluginAccessor pluginAccessor;

    private final LocaleResolver localeResolver;

    private final PluginEventManager pluginEventManager;

    private final I18nResolver i18nResolver;

    private final ServletContainerContext servletContainerContext;

    private final I18nHasher i18nHasher;

    public WebResourceIntegrationImpl(PluginAccessor pluginAccessor, LocaleResolver localeResolver,
                                      I18nResolver i18nResolver, PluginEventManager pluginEventManager,
                                      ServletContainerContext servletContainerContext) {
        this.pluginAccessor = pluginAccessor;
        this.localeResolver = localeResolver;
        this.i18nResolver = i18nResolver;
        this.pluginEventManager = pluginEventManager;
        this.servletContainerContext = servletContainerContext;
        this.i18nHasher = new I18nHasher(pluginAccessor, pluginEventManager);
    }

    /**
     * Applications must implement this method to get access to the application's PluginAccessor
     */
    @Override
    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    /**
     * Applications must implement this method to get access to the application's PluginEventManager
     *
     * @since 3.1.1
     * @deprecated since v3.3.2
     */
    @Override
    public PluginEventManager getPluginEventManager() {
        return pluginEventManager;
    }

    /**
     * This must be a thread-local cache that will be accessible from both the page, and the decorator
     */
    @Override
    public Map<String, Object> getRequestCache() {
        return servletContainerContext.getRequestCache();
    }

    /**
     * Represents the unique number for this system, which when updated will flush the cache. This should be a number
     * and is generally stored in the global application-properties.
     *
     * @return A string representing the count
     */
    @Override
    public String getSystemCounter() {
        return "1";
    }

    /**
     * Represents the last time the system was updated.  This is generally obtained from BuildUtils or similar.
     */
    @Override
    public String getSystemBuildNumber() {
        return Version.REVISION;
    }

    /**
     * The version number of the host application, for example "7.0.0-OD-07" or "5.6". It is intended to identify
     * if two instances of an application are the same version (the versions of plugins are checked separately).
     * This differs from {@link #getSystemBuildNumber()}, which does not necessarily change when the application changes
     * (at least in JIRA this represents the database build number, not the application version number).
     *
     * @since 3.4.7
     */
    @Override
    public String getHostApplicationVersion() {
        return Version.VERSION;
    }

    /**
     * Returns the base URL for this application.  This method may return either an absolute or a relative URL.
     * Implementations are free to determine which mode to use based on any criteria of their choosing. For example, an
     * implementation may choose to return a relative URL if it detects that it is running in the context of an HTTP
     * request, and an absolute URL if it detects that it is not.  Or it may choose to always return an absolute URL, or
     * always return a relative URL.  Callers should only use this method when they are sure that either an absolute or
     * a relative URL will be appropriate, and should not rely on any particular observed behavior regarding how this
     * value is interpreted, which may vary across different implementations.
     * <p>
     * In general, the behavior of this method should be equivalent to calling {@link
     * #getBaseUrl(UrlMode)} with a {@code urlMode} value of {@link
     * UrlMode#AUTO}.
     *
     * @return the string value of the base URL of this application
     */
    @Override
    public String getBaseUrl() {
        return getBaseUrl(UrlMode.AUTO);
    }

    /**
     * Returns the base URL for this application in either relative or absolute format, depending on the value of {@code
     * urlMode}.
     * <p>
     * If {@code urlMode == {@link UrlMode#ABSOLUTE}}, this method returns an absolute URL, with URL
     * scheme, hostname, port (if non-standard for the scheme), and context path.
     * <p>
     * If {@code urlMode == {@link UrlMode#RELATIVE}}, this method returns a relative URL containing
     * just the context path.
     * <p>
     * If {@code urlMode == {@link UrlMode#AUTO}}, this method may return either an absolute or a
     * relative URL.  Implementations are free to determine which mode to use based on any criteria of their choosing.
     * For example, an implementation may choose to return a relative URL if it detects that it is running in the
     * context of an HTTP request, and an absolute URL if it detects that it is not.  Or it may choose to always return
     * an absolute URL, or always return a relative URL.  Callers should only use {@code
     * WebResourceManager.UrlMode#AUTO} when they are sure that either an absolute or a relative URL will be
     * appropriate, and should not rely on any particular observed behavior regarding how this value is interpreted,
     * which may vary across different implementations.
     *
     * @param urlMode specifies whether to use absolute URLs, relative URLs, or allow the concrete implementation to
     *                decide
     * @return the string value of the base URL of this application
     * @since 2.3.0
     */
    @Override
    public String getBaseUrl(UrlMode urlMode) {
        String result;
        HttpServletRequest request = servletContainerContext.getRequest();
        if (urlMode == UrlMode.ABSOLUTE && request != null) {
            StringBuffer url = request.getRequestURL();
            String uri = request.getRequestURI();
            String contextPath = request.getContextPath();
            result = url.substring(0, url.length() - uri.length() + contextPath.length());
        } else {
            result = servletContainerContext.getServletContext().getContextPath();
        }
        return result;
    }

    /**
     * This version number is used for caching URL generation, and needs to be incremented every time the contents
     * of the superbatch may have changed. Practically this means updating every time the plugin system state changes
     *
     * @return a version number
     */
    @Override
    public String getSuperBatchVersion() {
        return "1";
    }

    /**
     * The locale identifier that should be inserted into static resource urls for the current request, if appropriate.
     *
     * @return null if the url should not have a locale component
     * @deprecated since 3.5.22 - This method includes current locale in URL hash which is unnecessary since it's
     * already in a query parameter. The current behaviour causes unnecessary cache misses of pre-baked resources
     * since it'll only work for instances with the same locale in which the pre-baker ran.
     * Use {@link #getI18nStateHash()} instead.
     */
    @Override
    public String getStaticResourceLocale() {
        return getLocale().toString();
    }

    /**
     * The locale hash that should be inserted into static resource urls for the current request, if appropriate.
     * This method should return the hash for i18n-contributing plugins without the current locale attached to it.
     *
     * @return null if the url should not have a locale component.
     * @since 3.5.22
     */
    @Override
    public String getI18nStateHash() {
        return i18nHasher.get();
    }

    /**
     * A reference to the temporary directory the application want the plugin system to use. The temporary directory can
     * be cleared at any time by the application and can be used by the plugin system to cache things like batches or
     * other things that can easily be re-generated. It is recommended that this directory be /apphome/tmp/webresources.
     * The plugin system can delete any or all files it sees fit that exists under this directory at any time.
     * The directory does not need to exist.
     *
     * @return a File reference to the temporary directory. This can not return null.
     * @since 2.9.0
     */
    @Override
    public File getTemporaryDirectory() {
        String tempDir = System.getProperty("java.io.tmpdir");
        return new File(tempDir);
    }

    /**
     * Returns the CDNStrategy for serving resources via CDN. This may return null, in which case no resources should
     * be served via CDN.
     *
     * @return CDN strategy
     */
    @Override
    public CDNStrategy getCDNStrategy() {
        return null;
    }

    /**
     * @return the current user's locale. Must return a default locale if none is found.
     */
    @Override
    public Locale getLocale() {
        return servletContainerContext.getLocale();
    }

    /**
     * @return the set of locales supported by this application. Used to pre-bake resources.
     * @since 3.4.7
     */
    @Override
    public Iterable<Locale> getSupportedLocales() {
        return Collections.singletonList(Locale.getDefault());
    }

    /**
     * Retrieve the unformatted message text associated with this key.
     *
     * @param locale locale in which to look up keys
     * @param key    key for the i18ned message
     * @return the unformatted message text if key is found for the given locale, otherwise returns key itself.
     */
    @Override
    public String getI18nRawText(Locale locale, String key) {
        return i18nResolver.getRawText(locale, key);
    }

    /**
     * Retrieve the message text associated with this key.
     *
     * @param locale locale in which to look up keys
     * @param key    key for the i18ned message
     * @return the message text if key is found for the given locale, otherwise returns key itself.
     */
    @Override
    public String getI18nText(Locale locale, String key) {
        return i18nResolver.getText(locale, key);
    }

    /**
     * Works together with `forbidCondition1AndTransformer1` and temporarily allows for given set of plugins to have
     * legacy stuff. Eventually when all plugins will be updated it will ratchet down to an empty list and will be
     * removed.
     *
     * @since 3.3.0
     */
    @Override
    public Set<String> allowedCondition1Keys() {
        return Collections.emptySet();
    }

    /**
     * Works together with `forbidCondition1AndTransformer1` and temporarily allows for given set of plugins to have
     * legacy stuff. Eventually when all plugins will be updated it will ratchet down to an empty list and will be
     * removed.
     *
     * @since 3.3.0
     */
    @Override
    public Set<String> allowedTransform1Keys() {
        return Collections.emptySet();
    }
}
