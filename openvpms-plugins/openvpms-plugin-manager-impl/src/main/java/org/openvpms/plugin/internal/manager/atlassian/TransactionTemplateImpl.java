/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.osgi.framework.Constants;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Enables the Spring {@link PlatformTransactionManager} available to be used via  an Atlassian
 * {@link TransactionTemplate}.<p/>
 * Spring beans can't directly be exposed to plugins as the spring classes are not exported on the
 * * OSGi {@link Constants#FRAMEWORK_SYSTEMPACKAGES_EXTRA}.<br/>
 * This is due to issues with plugins referencing spring.handlers and spring.schemas within the META-INF in the various
 * spring jars.
 */
public class TransactionTemplateImpl implements TransactionTemplate {

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs a {@link TransactionTemplateImpl}.
     *
     * @param transactionManager te transaction manager
     */
    public TransactionTemplateImpl(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * Executes a callback within a transaction.
     *
     * @param callback the callback
     * @return the result of the callback. May be {@code null}
     */
    @Override
    public <T> T execute(TransactionCallback<T> callback) {
        return new org.springframework.transaction.support.TransactionTemplate(transactionManager).execute(
                transactionStatus -> callback.doInTransaction());
    }
}
