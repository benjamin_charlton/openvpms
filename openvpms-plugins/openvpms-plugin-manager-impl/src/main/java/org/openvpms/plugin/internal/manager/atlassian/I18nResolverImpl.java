/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.message.MessageCollection;
import com.atlassian.sal.core.message.AbstractI18nResolver;
import com.atlassian.sal.core.message.DefaultMessage;
import com.atlassian.sal.core.message.DefaultMessageCollection;
import io.atlassian.util.concurrent.ManagedLock;
import io.atlassian.util.concurrent.ManagedLocks;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Implementation of {@link I18nResolver} lifted from the Atlassian Reference Application.
 *
 * @author Tim Anderson
 */
public class I18nResolverImpl extends AbstractI18nResolver {

    private final Map<Plugin, Iterable<String>> pluginResourceBundleNames = new ConcurrentHashMap<>();

    private ManagedLock.ReadWrite locks = ManagedLocks.manageReadWrite(new ReentrantReadWriteLock());

    private static final Serializable[] EMPTY_SERIALIZABLE = new Serializable[0];


    public I18nResolverImpl(final PluginAccessor pluginAccessor, PluginEventManager pluginEventManager) {
        pluginEventManager.register(this);

        locks.write().withLock(() -> addPluginResourceBundles(pluginAccessor.getPlugins()));
    }


    @Override
    public String resolveText(String key, Serializable[] arguments) {
        String pattern = getPattern(Locale.getDefault(), key);
        if (pattern == null) {
            return key;
        }
        return MessageFormat.format(pattern, (Object[]) arguments);
    }

    @Override
    public String resolveText(final Locale locale, final String key, final Serializable[] arguments) {
        String pattern = StringUtils.defaultString(getPattern(locale, key), getPattern(Locale.getDefault(), key));
        if (pattern == null) {
            return key;
        }
        return MessageFormat.format(pattern, (Object[]) arguments);
    }

    @Override
    public String getRawText(String key) {
        return StringUtils.defaultString(getPattern(Locale.getDefault(), key), key);
    }

    @Override
    public String getRawText(Locale locale, String key) {
        return StringUtils.defaultString(getPattern(locale, key), key);
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(final String prefix) {
        requireNonNull(prefix, "prefix");

        return locks.read().withLock((Supplier<Map<String, String>>) () -> {
            Map<String, String> translationsWithPrefix = new HashMap<>();
            for (Map.Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {

                addMatchingTranslationsToMap(
                        prefix, Locale.getDefault(), pluginBundleNames.getKey(), pluginBundleNames.getValue(), translationsWithPrefix);
            }
            return translationsWithPrefix;
        });
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(final String prefix, final Locale locale) {
        requireNonNull(prefix, "prefix");
        requireNonNull(locale, "locale");

        return locks.read().withLock((Supplier<Map<String, String>>) () -> {
            Map<String, String> translationsWithPrefix = new HashMap<>();
            for (Map.Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {
                addMatchingTranslationsToMap(
                        prefix, locale, pluginBundleNames.getKey(), pluginBundleNames.getValue(), translationsWithPrefix);
            }
            return translationsWithPrefix;
        });
    }


    public String getText(String key, Serializable... arguments) {
        Serializable[] resolvedArguments = new Serializable[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            Serializable argument = arguments[i];
            if (argument instanceof Message) {
                resolvedArguments[i] = getText((Message) argument);
            } else {
                resolvedArguments[i] = arguments[i];
            }
        }
        return resolveText(key, resolvedArguments);
    }

    public String getText(Locale locale, String key, Serializable... arguments) {
        requireNonNull(locale, "locale");
        Serializable[] resolvedArguments = new Serializable[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            Serializable argument = arguments[i];
            if (argument instanceof Message) {
                resolvedArguments[i] = getText(locale, (Message) argument);
            } else {
                resolvedArguments[i] = arguments[i];
            }
        }
        return resolveText(locale, key, resolvedArguments);
    }

    public String getText(String key) {
        return resolveText(key, EMPTY_SERIALIZABLE);
    }

    public String getText(Locale locale, String key) {
        requireNonNull(locale, "locale");
        return resolveText(locale, key, EMPTY_SERIALIZABLE);
    }

    public String getText(Message message) {
        return getText(message.getKey(), message.getArguments());
    }

    public String getText(Locale locale, Message message) {
        return getText(locale, message.getKey(), message.getArguments());
    }

    public Message createMessage(String key, Serializable... arguments) {
        return new DefaultMessage(key, arguments);
    }

    public MessageCollection createMessageCollection() {
        return new DefaultMessageCollection();
    }

    @PluginEventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        locks.write().withLock(() -> addPluginResourceBundles(event.getPlugin()));
    }

    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        locks.write().withLock(() -> removePluginResourceBundles(event.getPlugin()));
    }

    private String getPattern(final Locale locale, final String key) {
        return locks.read().withLock((Supplier<String>) () -> {
            String bundleString = null;
            for (Map.Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {
                for (String bundleName : pluginBundleNames.getValue()) {
                    try {
                        ResourceBundle bundle = getBundle(bundleName, locale, pluginBundleNames.getKey());
                        bundleString = bundle.getString(key);
                    } catch (MissingResourceException e) {
                        // ignore, try next bundle
                    }
                }
            }
            return bundleString;
        });
    }

    private void addMatchingTranslationsToMap(String prefix, Locale locale, Plugin plugin,
                                              Iterable<String> bundleNames,
                                              Map<String, String> translationsWithPrefix) {
        for (String bundleName : bundleNames) {
            try {
                ResourceBundle bundle = getBundle(bundleName, locale, plugin);
                if (bundle != null) {
                    addMatchingTranslationsToMap(prefix, bundle, translationsWithPrefix);
                }
            } catch (MissingResourceException e) {
                // OK, just ignore
            }
        }
    }

    private void addMatchingTranslationsToMap(String prefix, ResourceBundle bundle,
                                              Map<String, String> translationsWithPrefix) {
        Enumeration enumeration = bundle.getKeys();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            if (key.startsWith(prefix)) {
                translationsWithPrefix.put(key, bundle.getString(key));
            }
        }
    }

    private void addPluginResourceBundles(Iterable<Plugin> plugins) {
        for (Plugin plugin : plugins) {
            addPluginResourceBundles(plugin);
        }
    }

    private void addPluginResourceBundles(Plugin plugin) {
        List<String> bundleNames = plugin.getResourceDescriptors().stream()
                .filter(descriptor -> "i18n".equals(descriptor.getType()))
                .map(ResourceDescriptor::getLocation)
                .collect(Collectors.toList());
        addPluginResourceBundles(plugin, bundleNames);
    }

    private void addPluginResourceBundles(Plugin plugin, List<String> bundleNames) {
        pluginResourceBundleNames.put(plugin, bundleNames);
    }

    private void removePluginResourceBundles(Plugin plugin) {
        pluginResourceBundleNames.remove(plugin);
    }

    private ResourceBundle getBundle(String bundleName, Locale locale, Plugin plugin) {
        return ResourceBundle.getBundle(bundleName, locale, plugin.getClassLoader());
    }

}
