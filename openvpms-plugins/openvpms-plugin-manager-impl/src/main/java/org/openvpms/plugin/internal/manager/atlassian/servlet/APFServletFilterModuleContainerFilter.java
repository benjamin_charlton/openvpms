/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian.servlet;

import com.atlassian.plugin.servlet.filter.ServletFilterModuleContainerFilter;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Helper to integrate the Atlassian Plugin Framework {@link ServletFilterModuleContainerFilter} with Spring Security.
 * <p/>
 * This enables servlet plugins to define their own filters.
 *
 * @author Tim Anderson
 */
public class APFServletFilterModuleContainerFilter extends GenericFilterBean {

    /**
     * The filter to delegate to.
     */
    private ServletFilterModuleContainerFilter filter;

    /**
     * The location.
     */
    private String location;

    /**
     * The dispatcher.
     */
    private String dispatcher;

    /**
     * Constructs a {@link APFServletFilterModuleContainerFilter}.
     *
     * @param location   the location
     * @param dispatcher the dispatcher
     */
    APFServletFilterModuleContainerFilter(String location, String dispatcher) {
        this();
        this.location = location;
        this.dispatcher = dispatcher;
    }

    public APFServletFilterModuleContainerFilter() {
        addRequiredProperty("location");
        addRequiredProperty("dispatcher");
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setDispatcher(String dispatcher) {
        this.dispatcher = dispatcher;
    }

    public String getDispatcher() {
        return dispatcher;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        filter.doFilter(servletRequest, servletResponse, filterChain);
    }


    /**
     * Subclasses may override this to perform custom initialization.
     * All bean properties of this filter will have been set before this
     * method is invoked.
     * <p>Note: This method will be called from standard filter initialization
     * as well as filter bean initialization in a Spring application context.
     * Filter name and ServletContext will be available in both cases.
     * <p>This default implementation is empty.
     *
     * @throws ServletException if subclass initialization fails
     * @see #getFilterName()
     * @see #getServletContext()
     */
    @Override
    protected void initFilterBean() throws ServletException {
        filter = new ServletFilterModuleContainerFilter();
        FilterConfig config = getFilterConfig();
        if (config == null) {
            config = new FilterConfig() {
                @Override
                public String getFilterName() {
                    return APFServletFilterModuleContainerFilter.this.getFilterName();
                }

                @Override
                public ServletContext getServletContext() {
                    return APFServletFilterModuleContainerFilter.this.getServletContext();
                }

                @Override
                public String getInitParameter(String name) {
                    if ("location".equals(name)) {
                        return APFServletFilterModuleContainerFilter.this.location;
                    } else if ("dispatcher".equals(name)) {
                        return APFServletFilterModuleContainerFilter.this.dispatcher;
                    }
                    return null;
                }

                @Override
                public Enumeration<String> getInitParameterNames() {
                    return Collections.enumeration(Arrays.asList("location", "dispatcher"));
                }
            };
        }
        filter.init(config);
    }
}
