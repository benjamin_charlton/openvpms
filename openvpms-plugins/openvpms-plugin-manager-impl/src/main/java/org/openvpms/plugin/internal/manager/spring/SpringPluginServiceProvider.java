/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.spring;


import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.hostcomponents.InstanceBuilder;
import org.apache.commons.lang3.ClassUtils;
import org.openvpms.component.system.common.util.ClassHelper;
import org.springframework.beans.factory.BeanFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Provides host services to plugins.
 *
 * @author Tim Anderson
 */
public class SpringPluginServiceProvider implements HostComponentProvider {

    /**
     * The bean factory to retrieve beans from.
     */
    private final BeanFactory factory;

    /**
     * The names of beans to provide to plugins.
     */
    private final Map<String, List<String>> services;

    /**
     * Constructs a {@link SpringPluginServiceProvider}.
     *
     * @param factory  the bean factory to retrieve beans from
     * @param services the names of the services to provide to plugins
     */
    SpringPluginServiceProvider(BeanFactory factory, Map<String, List<String>> services) {
        this.factory = factory;
        this.services = services;
    }

    /**
     * Gives the object a chance to register its host components with the registrar
     *
     * @param registrar The host component registrar
     */
    @Override
    public void provide(ComponentRegistrar registrar) {
        for (Map.Entry<String, List<String>> entry : services.entrySet()) {
            String beanName = entry.getKey();
            if (factory.isSingleton(beanName)) {
                Object bean = factory.getBean(beanName);
                List<String> interfaceNames = entry.getValue();
                List<Class<?>> interfaces;
                if (!interfaceNames.isEmpty()) {
                    interfaces = new ArrayList<>();
                    for (String name : interfaceNames) {
                        Class<?> type;
                        try {
                            type = ClassHelper.getClass(name);
                        } catch (ClassNotFoundException exception) {
                            throw new IllegalStateException("Class not found for plugin service=" + beanName
                                                            + ": " + name, exception);
                        }
                        if (!type.isAssignableFrom(bean.getClass())) {
                            throw new IllegalStateException("Plugin service=" + beanName + " does not implement "
                                                            + type);
                        }
                        interfaces.add(type);
                    }
                } else {
                    interfaces = ClassUtils.getAllInterfaces(bean.getClass());
                }
                if (!interfaces.isEmpty()) {
                    InstanceBuilder builder = registrar.register(interfaces.toArray(new Class[0]));
                    builder.forInstance(bean);
                }
            }
        }
    }

}