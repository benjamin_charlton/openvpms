/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.seraph.auth.AuthenticationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;

/**
 * Implementation of the Seraph {@link AuthenticationContext} that delegates to Spring Security.
 *
 * @author Tim Anderson
 */
public class AuthenticationContextImpl implements AuthenticationContext {

    /**
     * Default constructor.
     */
    public AuthenticationContextImpl() {
    }

    /**
     * Returns the current user.
     *
     * @return the current user. May be {@code null}
     */
    public Principal getUser() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * Sets the current user.
     *
     * @param user the current user. May be {@code null}
     */
    public void setUser(Principal user) {
        SecurityContextHolder.getContext().setAuthentication((Authentication) user);
    }

    /**
     * Clears the current user.
     */
    public void clearUser() {
        setUser(null);
    }
}
