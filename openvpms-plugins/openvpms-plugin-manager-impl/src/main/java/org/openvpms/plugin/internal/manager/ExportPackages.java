/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.framework.Logger;
import org.apache.felix.framework.util.StringMap;
import org.apache.felix.framework.util.manifestparser.ManifestParser;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;
import org.osgi.framework.namespace.PackageNamespace;
import org.osgi.framework.wiring.BundleCapability;
import org.osgi.framework.wiring.BundleRequirement;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.resource.Capability;
import org.osgi.resource.Requirement;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Parses OSGi manifest entries to determine the packages that need to be added to the
 * <em>org.osgi.framework.system.packages.extra</em> property to expose them to plugins.
 * <p/>
 * To avoid clashes with bundles, the following packages are not exported:
 * <ul>
 * <li>org.osg.*</li>
 * <li>java.*</li>
 * <li>javax.ws.* - conflicts with the atlassian-rest-module</li>
 * <li>org.springframework.*</li>
 * </ul>
 * In addition, any instance of jsr305 on the classpath is ignored. This is because it exports javax.annotation
 * which conflicts with that provided by the servlet container.
 * <p/>
 * Tomcat doesn't export the following packages, so these will be exported if they aren't found:
 * <ul>
 *     <li>javax.annotation - 1.3.5</li>
 *     <li>javax.el - 3.0.0</li>
 *     <li>javax.servlet - 3.1.0</li>
 * </ul>
 * <br>
 * Note that Atlassian jars typically don't contain OSGi metadata in their manifests, and so the Atlassian plugin
 * bootstrapping exports all Atlassian packages. It also derives version numbers from the jar names, which causes
 * problems where a jar exports multiple versions (e.g. commons-io).
 *
 * @author Tim Anderson
 */
class ExportPackages {

    /**
     * The packages to export.
     */
    private final Map<String, String> packages;

    /**
     * Packages to the jars that export them.
     */
    private final Map<String, String> packageToJar = new HashMap<>();

    /**
     * The jars to include.
     */
    private final List<String> includes = new ArrayList<>();

    /**
     * The jars to exclude.
     */
    private final List<String> excludes = new ArrayList<>();

    /**
     * The logger.
     */
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ExportPackages.class);

    /**
     * Helper to supply to {@code ManifestParser}.
     */
    private static final BundleRevision BUNDLE_REVISION = new MockBundleRevision();

    /**
     * The annotation API version.
     */
    private static final String ANNOTATION_API = "javax.annotation";

    /**
     * The annotation API version.
     */
    private static final String ANNOTATION_API_VERSION = "1.3.5";

    /**
     * The base servlet API package.
     */
    private static final String SERVLET_API = "javax.servlet";

    /**
     * The required servlet API version.
     */
    private static final String SERVLET_API_VERSION = "3.1.0";

    /**
     * The EL API package.
     */
    private static final String EL_API = "javax.el";

    /**
     * The required EL API version.
     */
    private static final String EL_API_VERSION = "3.0.0";


    /**
     * Constructs an {@link ExportPackages}.
     *
     * @param logger the logger
     */
    ExportPackages(Logger logger) {
        packages = new TreeMap<>();

        try {
            Enumeration<URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
            Set<String> seen = new HashSet<>(); // used to exclude jars included on the classpath more than once
            while (resources.hasMoreElements()) {
                URL manifestURL = resources.nextElement();
                String lowercaseURL = manifestURL.toString().toLowerCase(); // needed for Windows case changes
                if (seen.add(lowercaseURL)) {
                    processManifest(manifestURL, logger);
                }
            }
        } catch (IOException exception) {
            log.debug(exception.getMessage(), exception);
        }
        // add any missing packages. This is required when running under Tomcat
        addAnnotationPackages();
        addELPackages();
        addServletPackages();

        if (log.isDebugEnabled()) {
            log.debug("Included jars: {}", StringUtils.join(includes, ","));
            log.debug("Excluded jars: {}", StringUtils.join(excludes, ","));
        }
    }

    /**
     * Returns the packages to export, and their corresponding version.
     *
     * @return the packages
     */
    Map<String, String> getPackages() {
        return packages;
    }

    /**
     * Returns the packages to export.
     *
     * @return the packages
     */
    List<String> getPackageIncludes() {
        return new ArrayList<>(packages.keySet());
    }

    /**
     * Returns the jars to include.
     *
     * @return the jars to include
     */
    List<String> getJarIncludes() {
        return includes;
    }

    /**
     * Returns the jars to exclude.
     *
     * @return the jars to exclude
     */
    List<String> getJarExcludes() {
        return excludes;
    }

    /**
     * Processes a manifest to determine if a jar should be included or excluded.
     *
     * @param manifestURL the jar manifest URL
     * @param logger      the logger
     */
    private void processManifest(URL manifestURL, Logger logger) {
        Manifest manifest = getManifest(manifestURL);
        boolean include = false;
        String jarURL = getJarURL(manifestURL);
        String jar = getJar(manifestURL);
        if (!excludeJar(jar)) {
            if (manifest != null) {
                Map<String, Object> attributes = getAttributes(manifest);
                if (attributes != null) {
                    if (addPackages(attributes, jarURL, logger)) {
                        include = true;
                    }
                } else {
                    // Not an OSGi bundle. See if it should be included anyway
                    include = includeNonOSGIJar(manifest);
                }
            }
            if (jar != null) {
                if (include || isAtlassianJar(jar)) {
                    includes.add(jar);
                } else if (!includes.contains(jar)) {
                    // when deploying with web overlays, the same jar name can be included from multiple sources
                    excludes.add(jar);
                }
            }
        }
    }

    /**
     * Determines if a jar needs to be excluded.
     * <p/>
     * At present, this only applies to com.google.code.findbugs:jsr305 which erroneously exports javax.annotation.
     *
     * @param jar the jar. May be {@code null}
     * @return {@code true} if the jar should be excluded, otherwise {@code false}
     */
    private boolean excludeJar(String jar) {
        return jar != null && jar.startsWith("jsr305");
    }

    /**
     * Determines if a non-OSGI jar should be included.
     * <p/>
     * It should be included if it provides javax.annotation, javax.el, or javax.servlet.
     * This is required when running under Tomcat, as Tomcat doesn't provide OSGi headers for these.
     * The jar needs to be included if its packages are going to be considered by org.twdata.pkgscanner.PackageScanner
     *
     * @param manifest the jar manifest
     * @return {@code true} if it should be included, otherwise {@code false}
     */
    private boolean includeNonOSGIJar(Manifest manifest) {
        boolean include = false;
        Map<String, Attributes> entries = manifest.getEntries();
        if (entries.get("javax/annotation/") != null || entries.get("javax/el/") != null
            || entries.get("javax/servlet/") != null) {
            include = true;
        }
        return include;
    }

    /**
     * Adds exports for javax.annotation packages if they aren't present.
     */
    private void addAnnotationPackages() {
        String version = packages.get(ANNOTATION_API);
        if (version == null) {
            log.info(ANNOTATION_API + " not exported, defaulting version to " + ANNOTATION_API_VERSION);
            packages.put(ANNOTATION_API, ANNOTATION_API_VERSION);
            packages.put(ANNOTATION_API + ".security", ANNOTATION_API_VERSION);
            packages.put(ANNOTATION_API + ".sql", ANNOTATION_API_VERSION);
        }
    }

    /**
     * Adds exports for javax.servlet packages if they aren't present.
     */
    private void addServletPackages() {
        String version = packages.get(SERVLET_API);
        if (version == null) {
            log.info(SERVLET_API + " not exported, defaulting version to " + SERVLET_API_VERSION);
            packages.put(SERVLET_API, SERVLET_API_VERSION);
            packages.put(SERVLET_API + ".annotation", SERVLET_API_VERSION);
            packages.put(SERVLET_API + ".descriptor", SERVLET_API_VERSION);
            packages.put(SERVLET_API + ".http", SERVLET_API_VERSION);
        }
    }

    /**
     * Adds exports for javax.el packages if they aren't present.
     */
    private void addELPackages() {
        String version = packages.get(EL_API);
        if (version == null) {
            log.info(EL_API + " not exported, defaulting version to " + EL_API_VERSION);
            packages.put(EL_API, EL_API_VERSION);
        }
    }

    /**
     * Hack to avoid excluding Atlassian jars from being exported on the system class path.
     * <p/>
     * These don't have OSGi meta-data, but are picked up by the APF packaging scanning.
     *
     * @param jar the jar
     * @return {@code true} if the jar is an atlassian jar
     */
    private boolean isAtlassianJar(String jar) {
        return jar.startsWith("atlassian-") || jar.startsWith("velocity-htmlsafe");
    }

    /**
     * Determines if a package is excluded.
     *
     * @param pkg the package
     * @return {@code true} if the package is excluded
     */
    private boolean exclude(String pkg) {
        return pkg.startsWith("org.osgi.") || pkg.startsWith("java.")
               || pkg.startsWith("javax.ws")
               || pkg.startsWith("com.fasterxml")
               || pkg.startsWith("org.openvpms.ws.util"); // because it imports com.fasterxml
    }

    /**
     * Tries to determine the JAR from a MANIFEST.MF url.
     *
     * @param url the MANIFEST.MF url
     * @return the corresponding jar, or {@code null} if one cannot be determined
     */
    private String getJar(URL url) {
        String result = null;
        String path = url.getPath();
        int index = path.lastIndexOf('!');
        if (index > 0) {
            path = path.substring(0, index);
            index = path.lastIndexOf('/');
            if (index >= 0) {
                path = path.substring(index + 1);
                result = path.length() > 0 ? path : null;
            }
        }
        return result;
    }

    /**
     * Strips the manifest path from a URL. This should be everything after the !.
     *
     * @param manifestURL the manifest URL
     * @return the
     */
    private String getJarURL(URL manifestURL) {
        String result = manifestURL.toString();
        int index = result.lastIndexOf('!');
        if (index > 0) {
            result = result.substring(0, index);
        }
        return result;
    }

    /**
     * Adds the packages that a manifest exports, if any.
     *
     * @param manifest the manifest
     * @param url      the jar URL
     * @param logger   the Felix logger
     * @return {@code true} if packages were added
     */
    private boolean addPackages(Map<String, Object> manifest, String url, Logger logger) {
        Map<String, String> found = new HashMap<>();
        boolean exclude = false;
        try {
            Map<String, Object> configMap = Collections.emptyMap();
            ManifestParser parser = new ManifestParser(logger, configMap, BUNDLE_REVISION, manifest);
            for (BundleCapability capability : parser.getCapabilities()) {
                if (BundleRevision.PACKAGE_NAMESPACE.equals(capability.getNamespace())) {
                    Map<String, Object> attributes = capability.getAttributes();
                    String pkg = getString(PackageNamespace.PACKAGE_NAMESPACE, attributes);
                    if (pkg != null) {
                        if (exclude(pkg)) {
                            exclude = true;
                            break;
                        } else {
                            String version = getString(PackageNamespace.CAPABILITY_VERSION_ATTRIBUTE, attributes);
                            if (version != null) {
                                String existing = packageToJar.get(pkg);
                                if (existing != null) {
                                    String existingVersion = packages.get(pkg);
                                    if (!version.equals(existingVersion)) {
                                        if (existingVersion == null || isGreater(version, existingVersion)) {
                                            found.put(pkg, version);
                                        } else if (!url.equalsIgnoreCase(existing)) {
                                            // package is exported multiple times under different versions.
                                            // Use the newest version.
                                            // Do a case insensitive comparison for Windows.
                                            log.warn("Package " + pkg + " occurs in both '" + existing
                                                     + "' (" + existingVersion + ") and '" + url
                                                     + "' (" + version + ")");
                                            exclude = true;
                                        }
                                    } else if (!url.equalsIgnoreCase(existing)) {
                                        // same package, but from different Jars. Do a case insensitive comparison for
                                        // Windows.
                                        log.info("Package " + pkg + " occurs in both '" + existing
                                                 + "' (" + existingVersion + ") and '" + url
                                                 + "' (" + version + ")");
                                    }
                                } else {
                                    found.put(pkg, version);
                                    packageToJar.put(pkg, url);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Throwable exception) {
            log.info("Failed to process " + url + ":" + exception.getMessage(), exception);
        }
        if (!exclude) {
            packages.putAll(found);
        }
        return !exclude && !found.isEmpty();
    }

    /**
     * Returns the value of an attribute.
     *
     * @param name       the attribute name
     * @param attributes the attributes
     * @return the corresponding value, or {@code null} if none is found
     */
    private String getString(String name, Map<String, Object> attributes) {
        Object result = attributes.get(name);
        return (result != null) ? result.toString() : null;
    }

    /**
     * Returns the manifest for the specified URL.
     *
     * @param url the URL
     * @return the manifest, or {@code null} if none can be read
     */
    private Manifest getManifest(URL url) {
        Manifest manifest = null;
        try (InputStream stream = url.openStream()) {
            manifest = new Manifest(stream);
        } catch (Throwable exception) {
            log.warn("Failed to read manifest for " + url + ": " + exception, exception);
        }
        return manifest;
    }

    /**
     * Parses the manifest, returning the main attributes if it exports packages.
     *
     * @param manifest the manifest
     * @return the manifest's main attributes, or {@code null} if it doesn't export packages
     */
    private Map<String, Object> getAttributes(Manifest manifest) {
        Map<String, Object> result = null;
        if (manifest.getMainAttributes().getValue(Constants.EXPORT_PACKAGE) != null) {
            result = new StringMap(manifest.getMainAttributes());
        }
        return result;
    }

    /**
     * Determines if a version is greater than another
     *
     * @param version      the version
     * @param otherVersion the version to compare
     * @return {@code true} if {@code version} is greater than {@code otherVersion}
     */
    private boolean isGreater(String version, String otherVersion) {
        try {
            Version a = new Version(version);
            Version b = new Version(otherVersion);
            return a.compareTo(b) > 0;
        } catch (Throwable exception) {
            return false;
        }
    }

    /**
     * Helper to enable the use of {@code ManifestParser}.
     */
    private static class MockBundleRevision implements BundleRevision {
        public String getSymbolicName() {
            return null;
        }

        public Version getVersion() {
            return null;
        }

        public List<BundleCapability> getDeclaredCapabilities(String namespace) {
            return null;
        }

        public List<BundleRequirement> getDeclaredRequirements(String namespace) {
            return null;
        }

        public int getTypes() {
            return 0;
        }

        public BundleWiring getWiring() {
            return null;
        }

        public List<Capability> getCapabilities(String namespace) {
            return null;
        }

        public List<Requirement> getRequirements(String namespace) {
            return null;
        }

        public Bundle getBundle() {
            return null;
        }
    }
}
