/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.atlassian.seraph.auth.AuthenticationContext;

import java.security.Principal;

/**
 * Manages the principal for the current thread, used by the Atlassian HTTP plugin.
 * <p/>
 * This simply delegates to the supplied {@link AuthenticationContext}.
 *
 * @author Tim Anderson
 */
public class ThreadLocalContextManagerImpl implements ThreadLocalContextManager<Principal> {

    /**
     * The authentication context.
     */
    private final AuthenticationContext authenticationContext;

    /**
     * Constructs a {@link ThreadLocalContextManagerImpl}.
     *
     * @param authenticationContext the authentication context
     */
    public ThreadLocalContextManagerImpl(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    /**
     * Get the thread local context of the current thread.
     *
     * @return The thread local context
     */
    public Principal getThreadLocalContext() {
        return authenticationContext.getUser();
    }

    /**
     * Set the thread local context on the current thread
     *
     * @param context The context to set
     */
    public void setThreadLocalContext(Principal context) {
        authenticationContext.setUser(context);
    }

    /**
     * Clear the thread local context on the current thread
     */
    public void clearThreadLocalContext() {
        authenticationContext.setUser(null);
    }
}
