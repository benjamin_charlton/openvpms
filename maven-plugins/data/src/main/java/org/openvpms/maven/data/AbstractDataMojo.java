/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.data;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.maven.archetype.AbstractHibernateMojo;
import org.openvpms.tools.data.loader.StaxArchetypeDataLoader;
import org.springframework.context.ApplicationContext;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;


/**
 * Base class for the OpenVPMS data plugin.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDataMojo extends AbstractHibernateMojo {

    /**
     * The directory to process files from.
     */
    @Parameter(required = true)
    private File dir;

    /**
     * If {@code true}, skips execution.
     */
    @Parameter
    private boolean skip;

    /**
     * Determines if verbose logging is enabled.
     */
    @Parameter
    private boolean verbose;

    /**
     * Returns the director5y to process files from.
     *
     * @return the directory
     */
    public File getDir() {
        return dir;
    }

    /**
     * Determines if verbose logging is enabled.
     *
     * @param verbose if <tt>true</tt> log verbosely
     */
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * Determines if verbose logging is enabled.
     *
     * @return {@code true} if logging verbosely
     */
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * Executes the plugin unless execution is skipped.
     *
     * @throws MojoExecutionException if an unexpected problem occurs
     * @throws MojoFailureException   if an expected problem (such as a compilation failure) occurs
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (!skip) {
            super.execute();
        } else {
            getLog().info("Plugin is skipped");
        }
    }

    /**
     * Load data from the specified directory.
     *
     * @throws MojoExecutionException if an unexpected problem occurs
     */
    protected void doExecute() throws MojoExecutionException {
        if (dir == null || !dir.exists()) {
            throw new MojoExecutionException("Directory not found: " + dir);
        }
        if (!dir.isDirectory()) {
            throw new MojoExecutionException("Not a directory: " + dir);
        }
        try {
            ApplicationContext context = getContext();
            IArchetypeService service = (IArchetypeService) context.getBean("archetypeService");
            StaxArchetypeDataLoader loader = new StaxArchetypeDataLoader(service);
            doExecute(loader);
        } catch (Exception exception) {
            throw new MojoExecutionException("Failed to load data", exception);
        }
    }

    /**
     * Executes the data plugin goal.
     *
     * @param loader the archetype data loader to use
     * @throws IOException        for any I/O error
     * @throws XMLStreamException if a file cannot be read
     */
    protected abstract void doExecute(StaxArchetypeDataLoader loader) throws IOException, XMLStreamException;

    /**
     * Returns the application context paths used to create the Spring application context.
     *
     * @return the context paths
     */
    @Override
    protected String[] getContextPaths() {
        return new String[]{APPLICATION_CONTEXT, "dataloadContext.xml"};
    }
}
