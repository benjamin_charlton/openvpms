/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.maven.data;

import org.apache.maven.plugin.Mojo;
import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;


/**
 * Tests the {@link DataLoadMojo} class.
 *
 * @author Tim Anderson
 */
public class DataLoadMojoTestCase {

    /**
     * The mojo rule.
     */
    @Rule
    public MojoRule rule = new MojoRule();

    /**
     * Verifies the mojo can be executed.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMojoExecution() throws Exception {
        File pomDir = new File("target/test-classes/data");

        Mojo mojo = rule.lookupConfiguredMojo(pomDir, "load");
        mojo.execute();
    }

}
