/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.db;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Mojo to automatically update the OpenVPMS database.
 * <p/>
 * This can be skipped.
 * <p/>
 * NOTE: this loads archetypes, but not plugins.
 *
 * @author Tim Anderson
 */
@Mojo(name = "auto-update", requiresDependencyResolution = ResolutionScope.TEST)
public class AutoDatabaseUpdateMojo extends AbstractDatabaseUpdateMojo {

    /**
     * If {@code true}, skips execution.
     */
    @Parameter
    private boolean skip;

    /**
     * Perform whatever build-process behavior this <code>Mojo</code> implements.
     * <br/>
     * This implementation sets the context class loader to be that of the project's test class path,
     * before invoking {@link #doExecute()}.
     *
     * @throws MojoExecutionException if an unexpected problem occurs.
     * @throws MojoFailureException   an expected problem (such as a compilation failure) occurs.
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (!skip) {
            super.execute();
        } else {
            getLog().info("Plugin is skipped");
        }
    }

}
