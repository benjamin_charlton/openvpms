/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.query;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.query.Filter;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Domain query state.
 *
 * @author Tim Anderson
 */
public class DomainQueryState<D, T> {

    public static class Order {

        public enum Node {ID, NAME}

        private final Node node;

        private final boolean ascending;

        public Order(Node node, boolean ascending) {
            this.node = node;
            this.ascending = ascending;
        }

        public Node getNode() {
            return node;
        }

        public boolean isAscending() {
            return ascending;
        }

        public boolean equals(Object other) {
            return other instanceof Order && node == ((Order) other).node;
        }

        @Override
        public int hashCode() {
            return node.hashCode();
        }
    }

    /**
     * The domain type.
     */
    private final Class<D> domainType;

    /**
     * The model type.
     */
    private final Class<T> type;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The sort order.
     */
    private Set<Order> order = new LinkedHashSet<>();

    /**
     * The archetype.
     */
    private List<String> archetypes;

    /**
     * The id filter.
     */
    private Filter<Long> id;

    /**
     * Determines if active/inactive objects are being queried.
     */
    private Filter<Boolean> active;

    /**
     * The name to filter on.
     */
    private Filter<String> name;

    /**
     * The first result.
     */
    private int firstResult;

    /**
     * The page size.
     */
    private Integer pageSize;

    /**
     * The maximum no. of results to return.
     */
    private Integer maxResults;

    /**
     * Constructs a {@link DomainQueryState}.
     *
     * @param domainType    the domain type
     * @param type          the model type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public DomainQueryState(Class<D> domainType, Class<T> type, ArchetypeService service, DomainService domainService) {
        this.domainType = domainType;
        this.type = type;
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Creates a new state object, with details copied from this.
     *
     * @param domainType the domain type
     * @return a new state object
     */
    public <D2 extends IMObject> DomainQueryState<D2, T> newState(Class<D2> domainType) {
        DomainQueryState<D2, T> result = new DomainQueryState<>(domainType, type, service, domainService);
        populate(result);
        return result;
    }

    /**
     * Returns the domain type.
     *
     * @return the domain type
     */
    public Class<D> getDomainType() {
        return domainType;
    }

    /**
     * Returns the model type
     *
     * @return the model type
     */
    public Class<T> getType() {
        return type;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    public ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the domain object service.
     *
     * @return the domain object service
     */
    public DomainService getDomainService() {
        return domainService;
    }

    /**
     * Returns the archetypes.
     *
     * @return the archetypes. May be {@code null}
     */
    public List<String> getArchetypes() {
        return archetypes;
    }

    /**
     * Sets the archetypes.
     *
     * @param archetypes the archetypes
     */
    public void setArchetypes(String... archetypes) {
        setArchetypes(Arrays.asList(archetypes));
    }

    /**
     * Sets the archetypes.
     *
     * @param archetypes the archetypes
     */
    public void setArchetypes(List<String> archetypes) {
        this.archetypes = archetypes;
    }

    /**
     * Returns the identifier filter.
     *
     * @return the identifier filter. May be {@code null}
     */
    public Filter<Long> getId() {
        return id;
    }

    /**
     * Sets the identifier filter.
     *
     * @param id the identifier filter
     */
    public void setId(Filter<Long> id) {
        this.id = id;
    }

    /**
     * Returns the active filter.
     *
     * @return the active filter. May be {@code null}
     */
    public Filter<Boolean> getActive() {
        return active;
    }

    /**
     * Sets the active filter.
     *
     * @param active the active filter
     */
    public void setActive(Filter<Boolean> active) {
        this.active = active;
    }

    /**
     * Returns the name filter.
     *
     * @return the name filter. May be {@code null}
     */
    public Filter<String> getName() {
        return name;
    }

    /**
     * Sets the name filter.
     *
     * @param name the name filter
     */
    public void setName(Filter<String> name) {
        this.name = name;
    }

    /**
     * Determines if objects are being ordered on ascending or descending id.
     *
     * @param ascending if {@code true}, order on ascending id, else order by descending id
     */
    public void setOrderById(boolean ascending) {
        order.add(new Order(Order.Node.ID, ascending));
    }

    /**
     * Determines if objects are being ordered on ascending or descending name.
     *
     * @param ascending if {@code true}, order on ascending name, else order by descending name
     */
    public void setOrderByName(boolean ascending) {
        order.add(new Order(Order.Node.NAME, ascending));
    }

    /**
     * Returns the sort order.
     *
     * @return the sort order
     */
    public Collection<Order> getOrder() {
        return order;
    }

    /**
     * Returns the position of the first result to retrieve.
     *
     * @return the first result position
     */
    public int getFirstResult() {
        return firstResult;
    }

    /**
     * Sets the position of the first result to retrieve.
     *
     * @param firstResult the first result position
     */
    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }

    /**
     * Returns the page size.
     *
     * @return the page size. May be {@code null}
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Sets the page size.
     *
     * @param pageSize the page size
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Returns the maximum no. of results to retrieve.
     *
     * @return the maximum no. of results to retrieve. May be {@code null}
     */
    public Integer getMaxResults() {
        return maxResults;
    }

    /**
     * Sets the maximum no. of results to retrieve.
     *
     * @param maxResults the maximum no. of results to retrieve
     */
    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * Populates the supplied state.
     *
     * @param state the state to populate
     */
    protected void populate(DomainQueryState<?, ?> state) {
        state.setArchetypes(archetypes);
        state.setId(id);
        state.setActive(active);
        state.setName(name);
        state.order = new LinkedHashSet<>(order);
        state.setFirstResult(firstResult);
        state.setPageSize(pageSize);
        state.setMaxResults(maxResults);
    }

}
