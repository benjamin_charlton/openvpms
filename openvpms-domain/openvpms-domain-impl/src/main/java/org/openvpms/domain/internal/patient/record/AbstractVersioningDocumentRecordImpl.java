/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.DocumentBuilder;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.record.DocumentVersions;
import org.openvpms.domain.patient.record.VersioningDocumentRecord;

import java.util.List;

import static org.openvpms.domain.internal.patient.record.DocumentRecordDocumentBuilderImpl.VERSIONS;

/**
 * Default implementation of {@link VersioningDocumentRecord}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractVersioningDocumentRecordImpl extends AbstractDocumentActRecordImpl
        implements VersioningDocumentRecord {

    /**
     * Constructs a {@link AbstractVersioningDocumentRecordImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    protected AbstractVersioningDocumentRecordImpl(DocumentAct peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns a builder to build the document.
     *
     * @return a document builder
     */
    @Override
    public DocumentBuilder getDocumentBuilder() {
        return getService().createBuilder(getPeer(), DocumentRecordDocumentBuilderImpl.class);
    }

    /**
     * Returns the previous versions of this record.
     *
     * @return the previous versions
     */
    @Override
    public DocumentVersions getVersions() {
        List<ActRelationship> versions = getBean().getValues(VERSIONS, ActRelationship.class);
        return getService().createRelated(versions, DocumentVersionsImpl.class);
    }

}
