/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.user;

import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.user.EmployeeImpl;
import org.openvpms.domain.service.user.EmployeeQuery;
import org.openvpms.domain.user.Employee;

/**
 * Default implementation of {@link EmployeeQuery}.
 *
 * @author Tim Anderson
 */
class EmployeeQueryImpl extends AbstractUserQueryImpl<Employee, EmployeeQuery> implements EmployeeQuery {

    /**
     * Constructs an {@link EmployeeQueryImpl}.
     *
     * @param state the query state
     */
    public EmployeeQueryImpl(UserQueryState<Employee> state) {
        super(state);
        state.setEmployees(true);
    }

    /**
     * Returns a domain object for an object.
     *
     * @param object the object
     * @return the corresponding domain object
     */
    @Override
    protected Employee getDomainObject(User object) {
        // always have to create an Employee, even if the user is no-longer considered an employee, to avoid
        // ClassCastExceptions
        return getState().getDomainService().create(object, EmployeeImpl.class);
    }
}
