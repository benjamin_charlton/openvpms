/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.patient.record.builder.VisitBuilder;
import org.openvpms.domain.patient.record.builder.VisitNoteBuilder;
import org.openvpms.domain.patient.record.builder.VisitWeightBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Default implementation of {@link VisitBuilder}.
 *
 * @author Tim Anderson
 */
public class VisitBuilderImpl extends ParentRecordBuilderImpl<Visit, VisitBuilder>
        implements VisitBuilder {

    /**
     * The visit to update. May be {@code null}
     */
    private final Visit existing;

    /**
     * The identifier of the patient associated with the existing visit.
     */
    private final long existingPatientId;

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * Constructs a {@link VisitBuilderImpl}.
     *
     * @param services the builder services
     */
    public VisitBuilderImpl(PatientServices services) {
        this(null, services);
    }

    /**
     * Constructs a {@link VisitBuilderImpl}.
     *
     * @param visit    the visit to update
     * @param services the builder services
     */
    public VisitBuilderImpl(Visit visit, PatientServices services) {
        super(Visit.ARCHETYPE, services);
        this.existing = visit;
        if (visit != null) {
            Patient patient = visit.getPatient();
            existingPatientId = (patient != null) ? patient.getId() : 0;
            patient(patient);
        } else {
            existingPatientId = 0;
        }
    }

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    @Override
    public Patient getPatient() {
        return patient;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    @Override
    public VisitBuilder patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Returns a builder to add a note.
     *
     * @return a new note builder
     */
    @Override
    public VisitNoteBuilder newNote() {
        return new VisitNoteBuilderImpl(this, getServices());
    }

    /**
     * Returns a builder to add a weight.
     *
     * @return a new weight builder
     */
    @Override
    public VisitWeightBuilder newWeight() {
        return new VisitWeightBuilderImpl(this, getServices());
    }

    /**
     * Builds the record.
     *
     * @return the record
     */
    @Override
    public Visit build() {
        if (patient == null) {
            throw new IllegalStateException("Patient not specified");
        }
        if (existingPatientId != 0 && patient.getId() != existingPatientId) {
            throw new IllegalStateException("Cannot change patient on existing visit");
        }
        BuildContext context = new BuildContext(getArchetypeService());
        Act visit = build(context);
        TransactionTemplate template = new TransactionTemplate(getServices().getTransactionManager());
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                context.save();
            }
        });
        return (existing != null) ? existing : getServices().getDomainService().create(visit, Visit.class);
    }

    /**
     * Returns the record to update.
     *
     * @return the record
     */
    @Override
    protected Act getObject() {
        Act result;
        Patient patient = getPatient();
        if (existing != null) {
            IMObjectBean bean = getBean(existing);
            Reference patientRef = bean.getTargetRef("patient");
            if (patient != null) {
                if (!Objects.equals(patient.getObjectReference(), patientRef)) {
                    throw new IllegalStateException("Mismatched patients");
                }
            } else {
                patient = getServices().getDomainService().get(patientRef, Patient.class);
                if (patient == null) {
                    throw new IllegalStateException("Patient not found: " + patientRef);
                }
                patient(patient);
            }
            result = existing;
        } else {
            OffsetDateTime date = getDate();
            Date startTime = (date != null) ? DateRules.toDate(date) : new Date();
            MedicalRecordRules rules = getServices().getMedicalRecordRules();
            result = rules.getEventForAddition(patient.getObjectReference(), startTime, null);
        }
        return result;
    }

    /**
     * Builds the record.
     *
     * @param object  the record
     * @param bean    a bean wrapping the record
     * @param context the build context
     */
    @Override
    protected void build(Act object, IMObjectBean bean, BuildContext context) {
        super.build(object, bean, context);
        List<RecordBuilderImpl<?, ?>> builders = getBuilders();
        if (!builders.isEmpty()) {
            context.addChange(object);
            for (RecordBuilderImpl<?, ?> builder : builders) {
                Act child = builder.build(context);
                bean.addTarget("items", child, "event");
                context.addChange(child);
            }
        }
    }
}
