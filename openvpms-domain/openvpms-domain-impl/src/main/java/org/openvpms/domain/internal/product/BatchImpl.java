/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.product.Batch;

import java.time.OffsetDateTime;

/**
 * Default implementation of {@link Batch}.
 *
 * @author Tim Anderson
 */
public class BatchImpl extends BeanEntityDecorator implements Batch {

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link BatchImpl}.
     *
     * @param peer          the peer to delegate to
     * @param domainService the domain object service
     */
    public BatchImpl(Entity peer, DomainService domainService) {
        super(peer, domainService);
        this.domainService = domainService;
    }

    /**
     * Creates a {@link BatchImpl}.
     *
     * @param bean          the bean wrapping the entity
     * @param domainService the domain object service
     */
    public BatchImpl(IMObjectBean bean, DomainService domainService) {
        super(bean);
        this.domainService = domainService;
    }

    /**
     * Returns the batch number.
     *
     * @return the batch number
     */
    @Override
    public String getBatchNumber() {
        return getName();
    }

    /**
     * Returns the expiry date.
     *
     * @return the expiry date. May be {@code null}
     */
    @Override
    public OffsetDateTime getExpiry() {
        OffsetDateTime result = null;
        EntityLink product = getBean().getObject("product", EntityLink.class);
        if (product != null && product.getActiveEndTime() != null) {
            result = DateRules.toOffsetDateTime(product.getActiveEndTime());
        }
        return result;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    @Override
    public Product getProduct() {
        IMObject product = getBean().getTarget("product", Product.class);
        return product != null ? domainService.create(getBean(), Product.class) : null;
    }
}
