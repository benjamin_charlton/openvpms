/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.laboratory;

import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link InvestigationType}.
 *
 * @author Tim Anderson
 */
public class InvestigationTypeImpl extends BeanEntityDecorator implements InvestigationType {

    /**
     * The domain object service.
     */
    private final DomainService service;

    /**
     * Constructs an {@link InvestigationTypeImpl}.
     *
     * @param entity  the investigation type
     * @param service the domain object service
     */
    public InvestigationTypeImpl(Entity entity, DomainService service) {
        super(entity, service);
        this.service = service;
    }

    /**
     * Constructs an {@link InvestigationTypeImpl}.
     *
     * @param bean    the investigation type
     * @param service the domain object factory
     */
    public InvestigationTypeImpl(IMObjectBean bean, DomainService service) {
        super(bean);
        this.service = service;
    }

    /**
     * Returns the laboratory assigned investigation type id.
     * <p/>
     * This is short for: {@code getTypeIdentity().getIdentity()}
     *
     * @return the type id, or {@code null} if the investigation type is not managed by a laboratory
     */
    @Override
    public String getTypeId() {
        Identity identity = getTypeIdentity();
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * Returns the laboratory assigned identifier for this type.
     *
     * @return the type identity, or {@code null} if the investigation type is not managed by a laboratory
     */
    @Override
    public Identity getTypeIdentity() {
        return getBean().getObject("typeId", Identity.class);
    }

    /**
     * Returns the laboratory to submit orders to.
     *
     * @return the laboratory, or {@code null} if ordering is not supported
     */
    @Override
    public Laboratory getLaboratory() {
        Laboratory result = null;
        Entity laboratory = getBean().getTarget("laboratory", Entity.class);
        if (laboratory != null) {
            result = service.create(laboratory, Laboratory.class);
        }
        return result;
    }

    /**
     * Returns the devices that can perform tests for this investigation type.
     *
     * @return the devices
     */
    @Override
    public List<Device> getDevices() {
        List<Device> result = new ArrayList<>();
        IMObjectBean bean = getBean();
        for (Entity entity : bean.getTargets("devices", Entity.class)) {
            result.add(service.create(entity, Device.class));
        }
        return result;
    }

}
