/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.joda.time.Period;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.builder.RecordBuilder;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link RecordBuilder}.
 *
 * @author Tim Anderson
 */
public abstract class RecordBuilderImpl<R extends Record, RB extends RecordBuilder<R, RB>>
        implements RecordBuilder<R, RB> {

    /**
     * The archetype being built.
     */
    private final String archetype;

    /**
     * The builder services.
     */
    private final PatientServices services;

    /**
     * The record date.
     */
    private OffsetDateTime date;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The identities.
     */
    private final List<ActIdentity> identities = new ArrayList<>();

    /**
     * Constructs a {@link RecordBuilderImpl}.
     *
     * @param archetype the archetype being built
     * @param services  the builder services
     */
    public RecordBuilderImpl(String archetype, PatientServices services) {
        this.archetype = archetype;
        this.services = services;
    }

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    public abstract Patient getPatient();

    /**
     * Returns the record date.
     *
     * @return the record date. May be {@code null}
     */
    @Override
    public OffsetDateTime getDate() {
        return date;
    }

    /**
     * Sets the record date.
     *
     * @param date the date
     * @return this
     */
    @Override
    public RB date(OffsetDateTime date) {
        this.date = date;
        return getThis();
    }

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public User getClinician() {
        return clinician;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    @Override
    public RB clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Adds an identity.
     * <p/>
     * The identity must be unique for the record type.
     *
     * @param archetype the identity archetype
     * @param identity  the identity
     * @return this
     */
    @Override
    public RB addIdentity(String archetype, String identity) {
        ActIdentity id = create(archetype, ActIdentity.class);
        id.setIdentity(identity);
        identities.add(id);
        return getThis();
    }

    /**
     * Builds the record.
     *
     * @param context the build context
     * @return the record
     */
    public Act build(BuildContext context) {
        Act object = getObject();
        build(object, getBean(object), context);
        return object;
    }

    /**
     * Builds the record.
     *
     * @param object  the record
     * @param bean    a bean wrapping the record
     * @param context the build context
     */
    protected void build(Act object, IMObjectBean bean, BuildContext context) {
        if (object.isNew()) {
            Patient patient = getPatient();
            bean.setTarget("patient", patient);
            context.addChange(object);
        } else {
            // prevent updating the record if it is locked. NOTE: will need to  change this later as some records
            // support partial updates
            checkLock(object);
        }
        if (date != null) {
            bean.setValue("startTime", DateRules.toDate(date));
            context.addChange(object);
        }
        if (!identities.isEmpty()) {
            for (ActIdentity identity : identities) {
                checkIdentity(identity, object);
                object.addIdentity(identity);
            }
            context.addChange(object);
            identities.clear();  // can't reuse
        }
    }

    /**
     * Returns the record to update.
     * <p/>
     * This implementation returns a new instance.
     *
     * @return the record
     */
    protected Act getObject() {
        return getArchetypeService().create(archetype, Act.class);
    }

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @param type      the expected type of the object
     * @return a new object
     */
    protected <T extends IMObject> T create(String archetype, Class<T> type) {
        return getArchetypeService().create(archetype, type);
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return getArchetypeService().getBean(object);
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected RB getThis() {
        return (RB) this;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getArchetypeService() {
        return services.getArchetypeService();
    }

    /**
     * Returns the builder services.
     *
     * @return the service
     */
    protected PatientServices getServices() {
        return services;
    }

    /**
     * Checks if the record is locked.
     *
     * @param object the record
     * @throws IllegalStateException if the record is locked
     */
    protected void checkLock(Act object) {
        if (isLocked(object)) {
            throw new IllegalStateException("Cannot modify locked object: " + object.getObjectReference());
        }
    }

    /**
     * Determines if a record is locked.
     *
     * @param record the record
     * @return {@code true} if the record is locked
     */
    protected boolean isLocked(Act record) {
        boolean locked = ActStatus.POSTED.equals(record.getStatus());
        if (!locked) {
            // not locked. See if record locking hasn't caught up yet
            Period period = services.getPracticeService().getRecordLockPeriod();
            if (period != null) {
                MedicalRecordRules recordRules = services.getMedicalRecordRules();
                if (recordRules.needsLock(record, period)) {
                    locked = true;
                }
            }
        }
        return locked;
    }

    /**
     * Verifies an identity is unique to this record.
     * <p/>
     * NOTE: the same identity can be applied to record of different archetypes.
     *
     * @param identity the identity
     * @param record   the record
     */
    private void checkIdentity(ActIdentity identity, Act record) {
        for (ActIdentity existing : record.getIdentities()) {
            if (existing.getIdentity().equals(identity.getIdentity())
                && existing.getArchetype().equals(identity.getArchetype())) {
                throw new IllegalStateException("Duplicate identity: " + identity.getArchetype()
                                                + ":" + identity.getIdentity());
            }
        }
        ArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Act> from = query.from(Act.class, record.getArchetype());
        query.select(from.get("id"));
        Join<Act, Identity> identities = from.join("identities", identity.getArchetype());
        identities.on(builder.equal(identities.get("identity"), identity.getIdentity()));
        Long id = service.createQuery(query).getFirstResult();
        if (id != null) {
            throw new IllegalStateException("Duplicate identity: " + identity.getArchetype()
                                            + ":" + identity.getIdentity() + " found on record " + id);
        }
    }
}
