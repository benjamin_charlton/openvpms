/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.openvpms.component.math.Weight;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.product.Template;
import org.openvpms.domain.product.TemplateItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link TemplateImpl}.
 *
 * @author Tim Anderson
 */
public class TemplateImpl extends BaseProductImpl implements Template {

    /**
     * The template items.
     */
    private List<TemplateItem> items;


    /**
     * Constructs a {@link TemplateImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the archetype service
     */
    public TemplateImpl(Product peer, ArchetypeService service) {
        super(peer, service);
    }

    /**
     * Constructs a {@link TemplateImpl}.
     *
     * @param bean the bean wrapping the product
     */
    public TemplateImpl(IMObjectBean bean) {
        super(bean);
    }

    /**
     * Returns the note to be included as a note in patient history, when this template is expanded.
     *
     * @return the visit note. May be {@code null}
     */
    @Override
    public String getVisitNote() {
        return getBean().getString("visitNote");
    }

    /**
     * Returns the items included by this template.
     * <p/>
     * Inactive products are excluded.     *
     *
     * @return the items
     */
    @Override
    public List<TemplateItem> getItems() {
        if (items == null) {
            List<TemplateItem> list = new ArrayList<>();
            IMObjectBean bean = getBean();
            RelatedIMObjects<Product, SequencedRelationship> includes
                    = bean.getRelated("includes", Product.class, SequencedRelationship.class)
                    .policy(Policies.newPolicy(SequencedRelationship.class)
                                    .orderBySequence()
                                    .active()
                                    .build());
            for (ObjectRelationship<Product, SequencedRelationship> included : includes.getObjectRelationships()) {
                list.add(new TemplateItemImpl(included.getObject(), bean.getBean(included.getRelationship())));
            }
            items = Collections.unmodifiableList(list);
        }
        return items;
    }

    /**
     * Returns the items included by this template, for a patient weight.
     *
     * @param weight the patient weight
     * @return the items
     */
    @Override
    public List<TemplateItem> getItems(Weight weight) {
        return getItems().stream()
                .filter(item -> item.inWeightRange(weight))
                .collect(Collectors.toList());
    }
}
