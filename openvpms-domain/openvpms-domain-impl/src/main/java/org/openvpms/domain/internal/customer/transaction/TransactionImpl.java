/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Transaction;
import org.openvpms.domain.customer.transaction.TransactionItem;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.practice.Location;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Default implementation of {@link Transaction}.
 *
 * @author Tim Anderson
 */
public class TransactionImpl<T extends TransactionItem> implements Transaction<T> {

    /**
     * The transaction.
     */
    private final FinancialAct transaction;

    /**
     * The bean wrapping the transaction.
     */
    private final IMObjectBean bean;

    /**
     * THe item type, or {@code null} if the transaction doesn't have items.
     */
    private final Class<T> itemType;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The customer.
     */
    private Customer customer;

    /**
     * The transaction items.
     */
    private List<T> items;

    /**
     * The location.
     */
    private Location location;

    /**
     * Items node name.
     */
    private static final String ITEMS = "items";


    /**
     * Constructs a {@link TransactionImpl}.
     *
     * @param transaction   the transaction
     * @param itemType      the line item type, or {@code null} if the transaction has no line items
     * @param domainService the domain service
     */
    public TransactionImpl(FinancialAct transaction, Class<T> itemType, DomainService domainService) {
        this.transaction = transaction;
        this.bean = domainService.getBean(transaction);
        this.itemType = itemType;
        this.domainService = domainService;
    }

    /**
     * Constructs a {@link TransactionImpl}.
     *
     * @param bean          a bean wrapping the transaction
     * @param itemType      the line item type, or {@code null} if the transaction has no line items
     * @param domainService the domain service
     */
    protected TransactionImpl(IMObjectBean bean, Class<T> itemType, DomainService domainService) {
        this.transaction = bean.getObject(FinancialAct.class);
        this.bean = bean;
        this.itemType = itemType;
        this.domainService = domainService;
    }

    /**
     * Returns the OpenVPMS identifier for this charge.
     *
     * @return the identifier
     */
    @Override
    public long getId() {
        return transaction.getId();
    }

    /**
     * Returns the transaction date.
     *
     * @return the transaction date
     */
    @Override
    public OffsetDateTime getDate() {
        return DateRules.toOffsetDateTime(transaction.getActivityStartTime());
    }

    /**
     * Returns the customer the charge is for.
     *
     * @return the customer. May be {@code null} for over-the-counter charges
     */
    @Override
    public Customer getCustomer() {
        if (customer == null) {
            Party party = getBean().getTarget("customer", Party.class);
            if (party != null) {
                customer = domainService.create(party, Customer.class);
            }
        }
        return customer;
    }

    /**
     * Returns the line items.
     *
     * @return the line items
     */
    @Override
    public List<T> getItems() {
        if (items == null) {
            if (bean.hasNode(ITEMS) && itemType != null) {
                List<T> matches = new ArrayList<>();
                List<FinancialAct> acts = bean.getTargets(ITEMS, FinancialAct.class);
                for (FinancialAct act : acts) {
                    matches.add(domainService.create(act, itemType));
                }
                items = Collections.unmodifiableList(matches);
            } else {
                items = Collections.emptyList();
            }
        }
        return items;
    }

    /**
     * Returns the transaction total.
     *
     * @return the transaction total
     */
    @Override
    public BigDecimal getTotal() {
        return transaction.getTotal();
    }

    /**
     * Determines if this transaction has been finalised.
     * <p/>
     * Finalised transactions cannot be modified.
     *
     * @return {@code true} if this transaction has been finalised, otherwise {@code false}
     */
    @Override
    public boolean isFinalised() {
        return FinancialActStatus.POSTED.equals(transaction.getStatus());
    }

    /**
     * Returns the location where this transaction was performed.
     *
     * @return the location. May be {@code null}
     */
    @Override
    public Location getLocation() {
        if (location == null) {
            Party party = getBean().getTarget("location", Party.class);
            if (party != null) {
                location = domainService.create(party, Location.class);
            }
        }
        return location;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TransactionImpl) {
            return transaction.equals(((TransactionImpl<?>) obj).transaction);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return transaction.hashCode();
    }

    /**
     * Returns the underlying act.
     *
     * @return the act
     */
    protected FinancialAct getTransaction() {
        return transaction;
    }

    /**
     * Returns the bean wrapping the transaction.
     *
     * @return the bean
     */
    protected IMObjectBean getBean() {
        return bean;
    }
}
