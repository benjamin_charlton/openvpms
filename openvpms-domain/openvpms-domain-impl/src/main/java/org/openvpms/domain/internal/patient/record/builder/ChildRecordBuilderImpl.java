/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.builder.RecordBuilder;

/**
 * Record builder for children of other records.
 *
 * @author Tim Anderson
 */
public class ChildRecordBuilderImpl<T extends Record, B extends RecordBuilder<T, B>,
        P extends Record, PB extends RecordBuilder<P, PB>>
        extends RecordBuilderImpl<T, B> {

    /**
     * The parent builder.
     */
    private final PB parent;

    /**
     * Constructs a {@link ChildRecordBuilderImpl}.
     *
     * @param archetype the archetype being built
     * @param parent    the parent builder
     * @param services  the builder services
     */
    public ChildRecordBuilderImpl(String archetype, PB parent, PatientServices services) {
        super(archetype, services);
        this.parent = parent;
    }

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    @Override
    public Patient getPatient() {
        return parent.getPatient();
    }

    /**
     * Adds the record.
     *
     * @return the parent builder
     */
    public PB add() {
        ((ParentRecordBuilderImpl) parent).add(this);
        return parent;
    }

}
