/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.transaction.ChargeItem;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;

import java.math.BigDecimal;

/**
 * Default implementation of {@link ChargeItem}.
 *
 * @author Tim Anderson
 */
public abstract class ChargeItemImpl extends TransactionItemImpl implements ChargeItem {

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * The product.
     */
    private Product product;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Constructs a {@link ChargeItemImpl}.
     *
     * @param item          the item
     * @param domainService the domain service
     */
    public ChargeItemImpl(FinancialAct item, DomainService domainService) {
        super(item, domainService);
    }

    /**
     * Constructs a {@link ChargeItemImpl}.
     *
     * @param bean          a bean wrapping the item
     * @param domainService the domain service
     */
    public ChargeItemImpl(IMObjectBean bean, DomainService domainService) {
        super(bean, domainService);
    }

    /**
     * Returns the patient the item was for.
     *
     * @return the patient the item was for
     */
    @Override
    public Patient getPatient() {
        if (patient == null) {
            Party party = getBean().getTarget("patient", Party.class);
            if (party != null) {
                patient = getDomainService().create(party, Patient.class);
            }
        }
        return patient;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    @Override
    public Product getProduct() {
        if (product == null) {
            product = getBean().getTarget("product", Product.class);
        }
        return product;
    }

    /**
     * Returns the quantity.
     *
     * @return the quantity
     */
    @Override
    public BigDecimal getQuantity() {
        return getAct().getQuantity();
    }

    /**
     * Returns the clinician responsible for this charge item.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public User getClinician() {
        if (clinician == null) {
            clinician = getBean().getTarget("clinician", User.class);
        }
        return clinician;
    }

    /**
     * Returns the discount amount, including tax.
     *
     * @return the discount amount
     */
    @Override
    public BigDecimal getDiscount() {
        return getBean().getBigDecimal("discount", BigDecimal.ZERO);
    }

    /**
     * Returns the discount tax amount.
     *
     * @return the discount tax amount
     */
    @Override
    public BigDecimal getDiscountTax() {
        BigDecimal total = getTotal();
        BigDecimal tax = getTotalTax();
        BigDecimal discount = getDiscount();
        return !MathRules.isZero(total) ? MathRules.divide(tax.multiply(discount), total, 2) : BigDecimal.ZERO;
    }

    /**
     * Returns the total tax amount.
     *
     * @return the tax amount
     */
    @Override
    public BigDecimal getTotalTax() {
        return getAct().getTaxAmount();
    }
}
