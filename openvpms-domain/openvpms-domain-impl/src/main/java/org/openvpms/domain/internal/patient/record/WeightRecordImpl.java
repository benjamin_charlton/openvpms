/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.math.Weight;
import org.openvpms.component.model.act.Act;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.record.WeightRecord;

/**
 * Default implementation of {@link WeightRecord}.
 *
 * @author Tim Anderson
 */
public class WeightRecordImpl extends AbstractRecordImpl implements WeightRecord {

    /**
     * The patient rules.
     */
    private final PatientRules rules;

    /**
     * Constructs a {@link WeightRecordImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     * @param rules   the patient rules
     */
    public WeightRecordImpl(Act peer, DomainService service, PatientRules rules) {
        super(peer, service);
        this.rules = rules;
    }

    /**
     * Returns the patient weight.
     *
     * @return the patient weight
     */
    @Override
    public Weight getWeight() {
        return rules.getWeight(getPeer());
    }
}
