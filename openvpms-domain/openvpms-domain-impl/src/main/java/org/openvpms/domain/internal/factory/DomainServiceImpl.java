/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.factory;

import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandlers;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.RelatedObjects;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.object.RelatedDomainObjects;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.AbstractRefreshableApplicationContext;

import java.util.List;

/**
 * Default implementation of {@link DomainService}.
 *
 * @author Tim Anderson
 */
public class DomainServiceImpl implements ApplicationContextAware, DomainService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Domain object implementation handlers.
     */
    private final ArchetypeHandlers<?> handlers;

    /**
     * The parent bean factory.
     */
    private BeanFactory beanFactory;

    /**
     * The default resource name.
     */
    private static final String NAME = "DomainObjectFactory.properties";

    /**
     * The default fallback resource name.
     */
    private static final String FALLBACK_NAME = "DefaultDomainObjectFactory.properties";

    /**
     * Constructs a {@link DomainServiceImpl}.
     *
     * @param service the archetype service
     */
    public DomainServiceImpl(ArchetypeService service) {
        this(NAME, FALLBACK_NAME, service);
    }

    /**
     * Constructs a {@link DomainServiceImpl}.
     *
     * @param name         the resource name
     * @param fallbackName the fallback resource name. May be {@code null}
     * @param service      the archetype service
     */
    public DomainServiceImpl(String name, String fallbackName, ArchetypeService service) {
        this.service = service;
        handlers = new ArchetypeHandlers<>(name, fallbackName, Object.class, "domain", service);
    }

    /**
     * Set the ApplicationContext that this object runs in.
     *
     * @param applicationContext the ApplicationContext object to be used by this object
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (applicationContext instanceof AbstractRefreshableApplicationContext) {
            // need to get the contained bean factory else autowire-candidate=false is ignored
            beanFactory = ((AbstractRefreshableApplicationContext) applicationContext).getBeanFactory();
        } else {
            beanFactory = applicationContext;
        }
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    @Override
    public IMObjectBean getBean(IMObject object) {
        return service.getBean(object);
    }

    /**
     * Creates a domain object from a model object.
     *
     * @param object the model object
     * @param type   the domain object type
     * @return a new domain object
     */
    @Override
    public <R, T extends IMObject> R create(T object, Class<R> type) {
        return create(object, type, null);
    }

    /**
     * Creates a domain object from a model object.
     *
     * @param object       the model object
     * @param type         the domain object type
     * @param fallbackType the fallback type, if there is no specific domain object type for the object
     * @return a new domain object
     */
    @Override
    public <R, F extends R, T extends IMObject> R create(T object, Class<R> type, Class<F> fallbackType) {
        return doCreate(object, object.getArchetype(), type, fallbackType);
    }

    /**
     * Creates a domain object from a model object.
     *
     * @param bean the model object wrapped in a bean
     * @param type the domain object type
     * @return a new domain object
     */
    @Override
    public <R> R create(IMObjectBean bean, Class<R> type) {
        return doCreate(bean, bean.getObject().getArchetype(), type, null);
    }

    /**
     * Creates a builder of the specified type, that takes the parent object in the constructor.
     *
     * @param parent the parent object
     * @param type   the builder type
     * @return a new builder
     */
    @Override
    public <R, T extends IMObject> R createBuilder(T parent, Class<R> type) {
        return doCreate(parent, type);
    }

    /**
     * Creates a {@link RelatedDomainObjects} instance that returns domain objects of the specified type for the targets
     * of the relationships.
     *
     * @param relationships the relationships
     * @param type          the domain object type
     * @return a new related objects instance
     */
    @Override
    public <T, R extends Relationship> RelatedDomainObjects<T, R> createRelatedObjects(List<R> relationships,
                                                                                       Class<T> type) {
        return new RelatedDomainObjects<>(relationships, type, false, this, service);
    }

    /**
     * Creates a {@link RelatedDomainObjects} instance that returns domain objects of the specified type for the targets
     * of the relationships.
     *
     * @param relationships the relationships
     * @param type          the domain object type
     * @param fallbackType  the fallback type, if there is no specific domain object type for an IMObject
     * @return a new related objects instance
     */
    @Override
    public <T, F extends T, R extends Relationship>
    RelatedDomainObjects<T, R> createRelatedObjects(List<R> relationships, Class<T> type, Class<F> fallbackType) {
        return new RelatedDomainObjects<>(relationships, type, fallbackType, false, this, service);
    }

    /**
     * Creates a particular {@link RelatedObjects} implementation.
     * <p/>
     * This must provide a constructor taking a list of relationships.
     *
     * @param relationships the relationships
     * @param type          the domain object type
     * @return a new related objects instance
     */
    @Override
    public <T, R extends Relationship, P extends RelatedObjects<T, R, P>> P createRelated(List<R> relationships,
                                                                                          Class<? extends P> type) {
        return doCreate(relationships, type);
    }

    /**
     * Retrieves a domain object given its reference.
     *
     * @param reference the object reference
     * @param type      the domain object type
     * @return the corresponding domain object, or {@code null} if the object is not found
     */
    @Override
    public <R> R get(Reference reference, Class<R> type) {
        IMObject object = service.get(reference);
        return (object != null) ? create(object, type) : null;
    }

    /**
     * Retrieves a domain object given its reference.
     *
     * @param reference the object reference
     * @param type      the domain object type
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding object, or {@code null} if none is found
     */
    @Override
    public <R> R get(Reference reference, Class<R> type, boolean active) {
        IMObject object = service.get(reference, active);
        return (object != null) ? create(object, type) : null;
    }

    /**
     * Retrieves a domain object given its archetype and identifier.
     *
     * @param archetype the object archetype
     * @param id        the object identifier
     * @param type      the domain object type
     * @return the corresponding domain object, or {@code null} if the object is not found
     */
    @Override
    public <R> R get(String archetype, long id, Class<R> type) {
        IMObject object = service.get(archetype, id);
        return (object != null) ? create(object, type) : null;
    }

    /**
     * Retrieves a domain object given its archetype and identifier.
     *
     * @param archetype the object archetype
     * @param id        the object identifier
     * @param type      the domain object type
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding domain object, or {@code null} if the object is not found
     */
    @Override
    public <R> R get(String archetype, long id, Class<R> type, boolean active) {
        IMObject object = service.get(archetype, id, active);
        return (object != null) ? create(object, type) : null;
    }

    /**
     * Creates a domain object for an object.
     *
     * @param object       the object. An {@link IMObject} or {@link IMObjectBean}
     * @param archetype    the object archetype
     * @param type         the domain type
     * @param fallbackType the fallback type, if there is no specific domain object type for the object.
     *                     May be {@code null}
     * @return the domain object
     */
    @SuppressWarnings("unchecked")
    private <R, F extends R> R doCreate(Object object, String archetype, Class<R> type, Class<F> fallbackType) {
        Object result = object;
        if (!object.getClass().isAssignableFrom(type)) {
            ArchetypeHandler<?> handler;
            boolean concrete = !type.isInterface();
            if (concrete) {
                handler = handlers.getHandler((Class) type);
            } else {
                handler = handlers.getHandler(archetype);
                if (handler == null) {
                    handler = handlers.getHandler((Class) type);
                }
            }
            if (handler != null) {
                result = doCreate(object, handler);
            } else if (fallbackType != null) {
                // no explicit handler for the object. Create the fallback type directly
                result = doCreate(object, fallbackType);
            } else if (concrete) {
                result = doCreate(object, type);
            }
        }
        return type.cast(result);
    }

    /**
     * Creates a domain object for an object.
     *
     * @param object  the object. An {@link IMObject} or {@link IMObjectBean}
     * @param handler the handler
     * @return the domain object
     */
    private Object doCreate(Object object, ArchetypeHandler<?> handler) {
        return doCreate(object, handler.getType());
    }

    /**
     * Creates an object of the specified type, passing the supplied object in the constructor.
     *
     * @param object the object. An {@link IMObject} or {@link IMObjectBean}
     * @return a new instance of the specified type
     */
    private <T> T doCreate(Object object, Class<T> type) {
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory(beanFactory) {
            @Override
            public boolean isAutowireCandidate(String beanName, DependencyDescriptor descriptor)
                    throws NoSuchBeanDefinitionException {
                // ensure only the archetypeService registered below is used when autowiring ArchetypeService
                // to avoid NoUniqueBeanDefinitionException
                if (descriptor.getDependencyType().equals(ArchetypeService.class)
                    && (beanName.equals("archetypeRuleService") || beanName.equals("pluginArchetypeService"))) {
                    return false;
                }
                return super.isAutowireCandidate(beanName, descriptor);
            }
        };
        factory.registerSingleton("object", object);
        factory.registerSingleton("archetypeService", service);
        return type.cast(factory.createBean(type, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false));
    }
}
