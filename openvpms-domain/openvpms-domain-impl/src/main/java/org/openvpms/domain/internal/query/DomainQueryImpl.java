/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.query;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Path;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.query.DomainQuery;
import org.openvpms.domain.query.Filter;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Default implementation of {@link DomainQuery}.
 *
 * @author Tim Anderson
 */
public abstract class DomainQueryImpl<D, T extends IMObject, Q extends DomainQuery<D, Q>> implements DomainQuery<D, Q> {

    /**
     * The query state.
     */
    private final DomainQueryState<D, T> state;

    /**
     * Constructs a {@link DomainQueryImpl}.
     *
     * @param domainTpe     the domain type
     * @param type          the {@link IMObject} type
     * @param service       the service
     * @param domainService the domain service
     */
    protected DomainQueryImpl(Class<D> domainTpe, Class<T> type, ArchetypeService service,
                              DomainService domainService) {
        this(null, domainTpe, type, service, domainService);
    }

    /**
     * Constructs a {@link DomainQueryImpl}.
     *
     * @param archetype     the archetype. If {@code null}, {@link #archetypes(String...)} must be invoked
     * @param domainTpe     the domain type
     * @param type          the {@link IMObject} type
     * @param service       the service
     * @param domainService the domain service
     */
    protected DomainQueryImpl(String archetype, Class<D> domainTpe, Class<T> type, ArchetypeService service,
                              DomainService domainService) {
        state = new DomainQueryState<>(domainTpe, type, service, domainService);
        if (archetype != null) {
            archetypes(archetype);
        }
    }

    /**
     * Constructs a {@link DomainQueryImpl}.
     *
     * @param state the query state
     */
    protected DomainQueryImpl(DomainQueryState<D, T> state) {
        this.state = state;
    }

    /**
     * Filter by identifier.
     *
     * @param id the identifier
     * @return this
     */
    @Override
    public Q id(long id) {
        state.setId(Filter.equal(id));
        return getThis();
    }

    /**
     * Filter by name.
     *
     * @param name the name to filter on
     * @return this
     */
    @Override
    public Q name(String name) {
        return name(Filter.equal(name));
    }

    /**
     * Filter by name.
     *
     * @param name the name criteria
     * @return this
     */
    @Override
    public Q name(Filter<String> name) {
        state.setName(name);
        return getThis();
    }

    /**
     * Only query active objects.
     *
     * @return this
     */
    @Override
    public Q active() {
        state.setActive(Filter.equal(true));
        return getThis();
    }

    /**
     * Only query inactive objects.
     *
     * @return this
     */
    @Override
    public Q inactive() {
        state.setActive(Filter.equal(false));
        return getThis();
    }

    /**
     * Order objects by id.
     * <p/>
     * This orders on ascending id.
     * <p/>
     * This is the default ordering.
     *
     * @return this
     */
    @Override
    public Q orderById() {
        return orderByName(true);
    }

    /**
     * Order objects by id.
     *
     * @param ascending if {@code true}, order on ascending id, else order on descending id
     * @return this
     */
    @Override
    public Q orderById(boolean ascending) {
        state.setOrderById(ascending);
        return getThis();
    }

    /**
     * Order objects by name.
     *
     * @return this
     */
    @Override
    public Q orderByName() {
        return orderByName(true);
    }

    /**
     * Order objects by name.
     *
     * @param ascending if {@code true}, order on ascending name, else order on descending name
     * @return this
     */
    @Override
    public Q orderByName(boolean ascending) {
        state.setOrderByName(ascending);
        return getThis();
    }

    /**
     * Sets the first result.
     *
     * @param firstResult the first result
     * @return this
     */
    @Override
    public Q firstResult(int firstResult) {
        state.setFirstResult(firstResult);
        return getThis();
    }

    /**
     * Sets the page size.
     *
     * @param pageSize the page size
     * @return this
     */
    public Q pageSize(int pageSize) {
        state.setPageSize(pageSize);
        return getThis();
    }

    /**
     * The maximum no. of results to return in any given query.
     *
     * @param maxResults the maximum no. of results
     * @return this
     */
    @Override
    public Q maxResults(int maxResults) {
        state.setMaxResults(maxResults);
        return getThis();
    }

    /**
     * Returns the first result matching the query.
     *
     * @return the first result, or {@code null} if none is found
     */
    @Override
    public D findFirst() {
        TypedQuery<T> query = createQuery();
        T object = query.getFirstResult();
        return object != null ? getDomainObject(object) : null;
    }

    /**
     * Executes the query.
     * <p/>
     * NOTE: while this returns an {@code Iterable}, underlying state is shared. The behaviour of using multiple
     * iterators created from it concurrently is undefined.
     *
     * @return the query results
     */
    @Override
    public Iterable<D> query() {
        TypedQuery<T> query = createQuery();
        Integer size = state.getPageSize();
        Integer maxResults = state.getMaxResults();
        ;
        if (size == null) {
            size = state.getMaxResults();
        }
        if (size == null || size > 500) {
            size = 500;
        }
        int limit = (maxResults != null) ? maxResults : -1;
        return createIterable(query, state.getFirstResult(), size, limit);
    }

    /**
     * Sets the archetypes to query.
     *
     * @param archetypes the archetypes
     * @return this
     */
    protected Q archetypes(String... archetypes) {
        state.setArchetypes(archetypes);
        return getThis();
    }

    /**
     * Returns the query state.
     *
     * @return the query state
     */
    protected DomainQueryState<D, T> getState() {
        return state;
    }


    /**
     * Creates the query.
     *
     * @return the query
     */
    protected TypedQuery<T> createQuery() {
        List<String> archetypes = state.getArchetypes();
        ArchetypeService service = state.getService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(state.getType());
        if (archetypes == null || archetypes.size() == 0) {
            throw new IllegalStateException("No archetypes specified");
        }
        Class<T> type = state.getType();
        Root<T> from = query.from(type, archetypes.toArray(new String[0]));
        List<Predicate> predicates = new ArrayList<>();
        addPredicates(predicates, from, builder);
        addOrderBy(query, from, builder);
        query.where(predicates);
        return service.createQuery(query);
    }

    /**
     * Adds predicates.
     *
     * @param predicates collects the predicates
     * @param from       the from clause
     * @param builder    the criteria builder
     */
    protected void addPredicates(List<Predicate> predicates, Root<T> from, CriteriaBuilder builder) {
        Filter<Long> id = state.getId();
        if (id != null) {
            addPredicate(from.get("id"), id, predicates, builder);
        }
        Filter<Boolean> active = state.getActive();
        if (active != null) {
            addPredicate(from.get("active"), active, predicates, builder);
        }
        Filter<String> name = state.getName();
        if (name != null) {
            addPredicate(from.get("name"), name, predicates, builder);
        }
    }

    /**
     * Adds order-by clauses.
     *
     * @param query   the query
     * @param from    the from clause
     * @param builder the criteria builder
     */
    protected void addOrderBy(CriteriaQuery<T> query, Root<T> from, CriteriaBuilder builder) {
        boolean id = false;
        List<Order> clauses = new ArrayList<>();

        for (DomainQueryState.Order order : state.getOrder()) {
            if (order.getNode() == DomainQueryState.Order.Node.NAME) {
                clauses.add(getOrderBy("name", from, order.isAscending(), builder));
            } else {
                clauses.add(getOrderBy("id", from, order.isAscending(), builder));
                id = true;
            }
        }
        if (!id) {
            // add an id clause to make the query deterministic when paging (providing no objects are added/deleted)
            clauses.add(getOrderBy("id", from, true, builder));
        }
        query.orderBy(clauses.toArray(new Order[0]));
    }

    /**
     * Adds a predicate.
     *
     * @param path       the path
     * @param filter     the criteria
     * @param predicates collects the predicates
     * @param builder    the criteria builder
     */
    protected <X> void addPredicate(Path<X> path, Filter<X> filter, List<Predicate> predicates,
                                    CriteriaBuilder builder) {
        predicates.add(createPredicate(path, filter, builder));
    }

    /**
     * Creates a predicate for a filter.
     *
     * @param path    the path to filter on
     * @param filter  the filter
     * @param builder the criteria builder
     * @return the predicate
     */
    @SuppressWarnings("unchecked")
    protected <X> Predicate createPredicate(Path<X> path, Filter<X> filter, CriteriaBuilder builder) {
        Predicate result;
        X value = filter.getValue();
        if (filter.getOperator() == Filter.Operator.EQUAL) {
            result = (value != null) ? builder.equal(path, value) : builder.isNull(path);
        } else if (filter.getOperator() == Filter.Operator.NOT_EQUAL) {
            result = (value != null) ? builder.notEqual(path, value) : builder.isNotNull(path);
        } else if (filter.getOperator() == Filter.Operator.LIKE) {
            result = builder.like((Expression<String>) path, (String) value);
        } else {
            throw new IllegalStateException("Unsupported operator=" + filter.getOperator());
        }
        return result;
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected Q getThis() {
        return (Q) this;
    }

    /**
     * Returns a domain object for an object.
     *
     * @param object the object
     * @return the corresponding domain object
     */
    protected D getDomainObject(T object) {
        D result;
        Class<D> domainType = state.getDomainType();
        if (domainType == state.getType()) {
            result = domainType.cast(object);
        } else {
            result = createDomainObject(object);
        }
        return result;
    }

    /**
     * Creates a domain object for the given model object.
     *
     * @param object the model object
     * @return the domain object
     */
    protected D createDomainObject(T object) {
        return state.getDomainService().create(object, state.getDomainType());
    }

    /**
     * Returns an order clause.
     *
     * @param name the node name
     * @param from the query root
     * @param ascending if {@code true}, sort ascending, else sort descending
     * @param builder the query builder
     * @return a new order clause
     */
    private Order getOrderBy(String name, Root<T> from, boolean ascending, CriteriaBuilder builder) {
        Path<Object> expression = from.get(name);
        return ascending ? builder.asc(expression) : builder.desc(expression);
    }

    /**
     * Creates an iterable over the domain objects.
     *
     * @param query       the query
     * @param firstResult the first result
     * @param pageSize    the page size
     * @return the iterable
     */
    private Iterable<D> createIterable(TypedQuery<T> query, int firstResult, int pageSize, int maxResults) {
        return () -> createIterator(query, firstResult, pageSize, maxResults);
    }

    /**
     * Creates an iterator over the domain objects.
     *
     * @param query       the query
     * @param firstResult the first result
     * @param pageSize    the page size
     * @param maxResults  the maximum results, or {@code -1} if results aren't restricted
     * @return the iterator
     */
    private Iterator<D> createIterator(TypedQuery<T> query, int firstResult, int pageSize, int maxResults) {
        Iterator<T> iterator = new TypedQueryIterator<>(query, firstResult, pageSize);
        if (maxResults >= 0) {
            iterator = new LimitedIterator<>(iterator, maxResults);
        }
        return new DomainIterator(iterator);
    }

    class DomainIterator implements Iterator<D> {

        private final Iterator<T> iterator;

        public DomainIterator(Iterator<T> iterator) {
            this.iterator = iterator;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public D next() {
            T object = iterator.next();
            return getDomainObject(object);
        }
    }

    private static class LimitedIterator<X> implements Iterator<X> {

        private final Iterator<X> iterator;

        private final int maxResults;

        private int count;

        public LimitedIterator(Iterator<X> iterator, int maxResults) {
            this.iterator = iterator;
            this.maxResults = maxResults;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return count < maxResults && iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         */
        @Override
        public X next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            X next = iterator.next();
            ++count;
            return next;
        }
    }

}
