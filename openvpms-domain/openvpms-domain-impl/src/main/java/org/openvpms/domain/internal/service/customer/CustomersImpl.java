/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.service.customer.Customers;

/**
 * Default implementation of {@link Customers}.
 *
 * @author Tim Anderson
 */
public class CustomersImpl implements Customers {

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link CustomersImpl}.
     *
     * @param domainService the domain object service
     */
    public CustomersImpl(DomainService domainService) {
        this.domainService = domainService;
    }

    /**
     * Returns a customer given its identifier.
     *
     * @param id the customer identifier
     * @return the corresponding customer, or {@code null} if none is found
     */
    @Override
    public Customer getCustomer(long id) {
        return domainService.get(CustomerArchetypes.PERSON, id, Customer.class);
    }
}
