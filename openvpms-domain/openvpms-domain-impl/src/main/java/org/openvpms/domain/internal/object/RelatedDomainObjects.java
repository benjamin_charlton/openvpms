/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.object;

import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;

import java.util.List;

/**
 * Queries related domain objects.
 *
 * @author Tim Anderson
 */
public class RelatedDomainObjects<T, R extends Relationship>
        extends AbstractRelatedDomainObjects<T, R, RelatedDomainObjects<T, R>> {

    /**
     * Constructs a {@link RelatedDomainObjects}.
     *
     * @param relationships the relationships to adapt
     * @param type          the domain object type
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param domainService the domain object service
     * @param service       the archetype service
     */
    public RelatedDomainObjects(List<R> relationships, Class<T> type, boolean source, DomainService domainService,
                                ArchetypeService service) {
        super(relationships, type, null, source, domainService, service);
    }

    /**
     * Constructs a {@link RelatedDomainObjects}.
     *
     * @param relationships the relationships to adapt
     * @param type          the domain object type
     * @param fallbackType  the fallback domain object type, if there is no specific domain object type for an IMObject
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param domainService the domain object service
     * @param service       the archetype service
     */
    public RelatedDomainObjects(List<R> relationships, Class<T> type, Class<? extends T> fallbackType, boolean source,
                                DomainService domainService, ArchetypeService service) {
        super(relationships, type, fallbackType, source, domainService, service);
    }

    /**
     * Constructs a {@link RelatedDomainObjects}.
     *
     * @param state  the state
     * @param policy the policy. May be {@code null}
     */
    protected RelatedDomainObjects(State<T, R> state, Policy<R> policy) {
        super(state, policy);
    }

    /**
     * Creates a new instance with the specified state and policy.
     *
     * @param state  the state
     * @param policy the policy. May be {@code null}
     * @return a new instance
     */
    @Override
    protected RelatedDomainObjects<T, R> newInstance(State<T, R> state, Policy<R> policy) {
        return new RelatedDomainObjects<>(state, policy);
    }

}
