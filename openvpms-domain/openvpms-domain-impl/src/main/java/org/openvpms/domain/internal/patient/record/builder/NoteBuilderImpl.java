/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.Reference;
import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.record.Note;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.builder.NoteBuilder;
import org.openvpms.domain.patient.record.builder.RecordBuilder;

/**
 * Default implementation of {@link NoteBuilder}.
 *
 * @author Tim Anderson
 */
public class NoteBuilderImpl<B extends NoteBuilder<B, P, PB>, P extends Record, PB extends RecordBuilder<P, PB>>
        extends ChildRecordBuilderImpl<Note, B, P, PB>
        implements NoteBuilder<B, P, PB> {

    /**
     * The note.
     */
    private String note;

    /**
     * The note node.
     */
    private static final String NOTE = "note";

    /**
     * The document node.
     */
    private static final String DOCUMENT = "document";

    /**
     * Constructs a {@link RecordBuilderImpl}.
     *
     * @param parent   the parent builder
     * @param services the builder services
     */
    public NoteBuilderImpl(PB parent, PatientServices services) {
        super(Note.ARCHETYPE, parent, services);
    }

    /**
     * Sets the note.
     *
     * @param note the note
     * @return this
     */
    @Override
    public B note(String note) {
        this.note = note;
        return getThis();
    }

    /**
     * Builds the record.
     *
     * @param object  the record
     * @param bean    a bean wrapping the record
     * @param context the build context
     */
    @Override
    protected void build(Act object, IMObjectBean bean, BuildContext context) {
        super.build(object, bean, context);
        note = StringUtils.trimToNull(note);
        if (note == null) {
            throw new IllegalStateException("Note not set");
        }
        if (note.length() <= bean.getMaxLength(NOTE)) {
            // note can fit in the details node
            bean.setValue(NOTE, note);

            // if there is an existing document, remove it
            Reference reference = bean.getReference(DOCUMENT);
            if (reference != null) {
                bean.setValue(DOCUMENT, null);
                context.remove(reference);
            }
        } else {
            // note too long so store it in a document
            bean.setValue(NOTE, null);
            Document document = bean.getObject(DOCUMENT, Document.class);
            if (document == null) {
                document = create(DocumentArchetypes.TEXT_DOCUMENT, Document.class);
            }
            TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
            handler.update((org.openvpms.component.business.domain.im.document.Document) document, note);
            bean.setValue(DOCUMENT, document.getObjectReference());
            context.addChange(document);
        }
        context.addChange(object);
    }
}
