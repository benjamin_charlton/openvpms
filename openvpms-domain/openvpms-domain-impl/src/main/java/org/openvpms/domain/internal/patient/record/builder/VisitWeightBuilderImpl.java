/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.patient.record.builder.VisitBuilder;
import org.openvpms.domain.patient.record.builder.VisitWeightBuilder;

/**
 * Default implementation of {@link VisitWeightBuilder}.
 *
 * @author Tim Anderson
 */
public class VisitWeightBuilderImpl extends WeightBuilderImpl<VisitWeightBuilder, Visit, VisitBuilder>
        implements VisitWeightBuilder {

    /**
     * Constructs a {@link VisitWeightBuilderImpl}.
     *
     * @param parent   the parent builder
     * @param services the builder services
     */
    public VisitWeightBuilderImpl(VisitBuilder parent, PatientServices services) {
        super(parent, services);
    }
}
