/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.query.DomainQueryImpl;
import org.openvpms.domain.patient.Microchip;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.patient.PatientQuery;

import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 * Default implementation of {@link PatientQueryImpl}.
 *
 * @author Tim Anderson
 */
public class PatientQueryImpl extends DomainQueryImpl<Patient, Party, PatientQuery> implements PatientQuery {

    /**
     * The microchip to filter on.
     */
    private Filter<String> microchip;


    /**
     * Constructs  a {@link PatientQueryImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public PatientQueryImpl(ArchetypeService service, DomainService domainService) {
        super(Patient.ARCHETYPE, Patient.class, Party.class, service, domainService);
    }

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip
     * @return this
     */
    @Override
    public PatientQuery microchip(String microchip) {
        return microchip(Filter.equal(microchip));
    }

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip
     * @return this
     */
    @Override
    public PatientQuery microchip(Filter<String> microchip) {
        this.microchip = microchip;
        return this;
    }

    /**
     * Adds predicates.
     *
     * @param predicates collects the predicates
     * @param from       the from clause
     * @param builder    the criteria builder
     */
    @Override
    protected void addPredicates(List<Predicate> predicates, Root<Party> from, CriteriaBuilder builder) {
        super.addPredicates(predicates, from, builder);
        if (microchip != null) {
            Join<?, ?> identity = from.join("identities", Microchip.ARCHETYPE);
            identity.on(createPredicate(identity.get("identity"), microchip, builder));
        }
    }

}
