/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.query.DomainQueryImpl;
import org.openvpms.domain.internal.user.EmployeeImpl;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.user.AbstractUserQuery;
import org.openvpms.domain.service.user.EmployeeQuery;
import org.openvpms.domain.user.Employee;

import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 * Default implementation of {@link AbstractUserQuery}.
 *
 * @author Tim Anderson
 */
class AbstractUserQueryImpl<D extends User, Q extends AbstractUserQuery<D, Q>> extends DomainQueryImpl<D, User, Q>
        implements AbstractUserQuery<D, Q> {

    /**
     * Constructs an {@link AbstractUserQueryImpl}.
     *
     * @param domainType    the domain type
     * @param service       the service
     * @param domainService the domain service
     */
    public AbstractUserQueryImpl(Class<D> domainType, ArchetypeService service, DomainService domainService) {
        super(new UserQueryState<>(domainType, service, domainService));
    }

    /**
     * Constructs a {@link AbstractUserQueryImpl}.
     *
     * @param state the query state
     */
    protected AbstractUserQueryImpl(UserQueryState<D> state) {
        super(state);
    }

    /**
     * Filter users by username.
     *
     * @param username the username
     * @return this
     */
    @Override
    public Q username(String username) {
        getState().setUsername(Filter.equal(username));
        return getThis();
    }

    /**
     * Returns users that are employees.
     *
     * @return an employee query
     */
    @Override
    public EmployeeQuery employees() {
        UserQueryState<Employee> state = (UserQueryState<Employee>) getState().newState(Employee.class);
        return new EmployeeQueryImpl(state);
    }

    /**
     * Return users that are clinicians.
     *
     * @return this
     */
    @Override
    public Q clinicians() {
        getState().setClinicians(true);
        return getThis();
    }

    /**
     * Returns the query state.
     *
     * @return the query state
     */
    @Override
    protected UserQueryState<D> getState() {
        return (UserQueryState<D>) super.getState();
    }

    /**
     * Adds predicates.
     *
     * @param predicates collects the predicates
     * @param from       the from clause
     * @param builder    the criteria builder
     */
    @Override
    protected void addPredicates(List<Predicate> predicates, Root<User> from, CriteriaBuilder builder) {
        super.addPredicates(predicates, from, builder);
        UserQueryState<D> state = getState();
        if (state.getUsername() != null) {
            addPredicate(from.get("username"), state.getUsername(), predicates, builder);
        }
        if (state.getEmployees()) {
            predicates.add(builder.isNotNull(from.get(EmployeeImpl.FIRST_NAME)));
            predicates.add(builder.isNotNull(from.get(EmployeeImpl.LAST_NAME)));
        }
        if (state.getClinicians()) {
            Join<?, Lookup> userType = from.join("classifications", UserArchetypes.USER_TYPE);
            userType.on(builder.equal(userType.get("code"), UserArchetypes.CLINICIAN_USER_TYPE));
        }
    }

    /**
     * Returns a domain object for an object.
     *
     * @param object the object
     * @return the corresponding domain object
     */
    @Override
    @SuppressWarnings("unchecked")
    protected D getDomainObject(User object) {
        return (D) UserFactory.create(object, getState().getDomainService());
    }
}
