/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.domain.product.Template;
import org.openvpms.domain.service.product.TemplateQuery;

/**
 * Default implementation of {@link TemplateQuery}.
 *
 * @author Tim Anderson
 */
public class TemplateQueryImpl extends AbstractProductQueryImpl<Template, TemplateQuery> implements TemplateQuery {

    /**
     * Constructs a {@link TemplateQueryImpl}.
     *
     * @param state the query state
     */
    protected TemplateQueryImpl(ProductQueryState<Template> state) {
        super(state);
        state.setArchetypes(ProductArchetypes.TEMPLATE);
    }

}
