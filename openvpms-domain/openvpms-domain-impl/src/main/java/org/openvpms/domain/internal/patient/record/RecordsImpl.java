/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.object.AbstractRelatedDomainObjects;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.Records;

import java.util.List;

/**
 * Default implementation of {@link Records}.
 *
 * @author Tim Anderson
 */
public class RecordsImpl extends AbstractRelatedDomainObjects<Record, ActRelationship, Records> implements Records {

    /**
     * Constructs a {@link RecordsImpl}.
     *
     * @param relationships the relationships to adapt
     * @param domainService the domain object service
     * @param service       the archetype service
     */
    public RecordsImpl(List<ActRelationship> relationships, DomainService domainService, ArchetypeService service) {
        super(relationships, Record.class, DefaultRecordImpl.class, false, domainService, service);
    }

    /**
     * Constructs a {@link RecordsImpl}.
     *
     * @param state  the state
     * @param policy the policy. May be {@code null}
     */
    protected RecordsImpl(State<Record, ActRelationship> state, Policy<ActRelationship> policy) {
        super(state, policy);
    }

    /**
     * Creates a new instance with the specified state and policy.
     *
     * @param state  the state
     * @param policy the policy. May be {@code null}
     * @return a new instance
     */
    @Override
    protected Records newInstance(State<Record, ActRelationship> state, Policy<ActRelationship> policy) {
        return new RecordsImpl(state, policy);
    }
}
