/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.query;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Queries objects by external identifier.
 *
 * @author Tim Anderson
 */
public class IdQuery {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link IdQuery}.
     *
     * @param service the archetype service
     */
    public IdQuery(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the first object with matching identity.
     *
     * @param archetype   the archetype
     * @param type        the object type
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return the corresponding object, or {@code null} if none is found
     */
    public <T extends IMObject> T getObject(String archetype, Class<T> type, String idArchetype, String identity) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(type);
        Root<T> from = buildIdQuery(archetype, type, idArchetype, identity, builder, query);
        query.select(from);
        query.orderBy(builder.asc(from.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Determines if an object exists with the specified identity.
     *
     * @param archetype   the archetype
     * @param type        the object type
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return the object id, or {@code null} if none is found
     */
    public <T extends IMObject> Long getId(String archetype, Class<T> type, String idArchetype, String identity) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> from = buildIdQuery(archetype, type, idArchetype, identity, builder, query);
        query.select(from.get("id"));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Determines if an object exists with the specified identity.
     *
     * @param archetype   the archetype
     * @param type        the object type
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return {@code true} if the record exists, otherwise {@code false}
     */
    public <T extends IMObject> boolean exists(String archetype, Class<T> type, String idArchetype, String identity) {
        return getId(archetype, type, idArchetype, identity) != null;
    }

    /**
     * Builds a query joining on identity.
     *
     * @param archetype   the archetype
     * @param type        the object type
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @param builder     the criteria builder
     * @param query       the query
     * @return the query root
     */
    private <T extends IMObject> Root<T> buildIdQuery(String archetype, Class<T> type, String idArchetype,
                                                      String identity, CriteriaBuilder builder,
                                                      CriteriaQuery<?> query) {
        Root<T> from = query.from(type, archetype);
        Join<T, Identity> identities = from.join("identities", idArchetype);
        query.where(builder.equal(identities.get("identity"), identity));
        return from;
    }
}