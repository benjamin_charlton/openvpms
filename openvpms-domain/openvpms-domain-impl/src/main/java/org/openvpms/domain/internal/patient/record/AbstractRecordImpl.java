/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.business.domain.im.act.BeanActDecorator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;

/**
 * Abstract implementation of {@link Record}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractRecordImpl extends BeanActDecorator implements Record {

    /**
     * The domain object service.
     */
    private final DomainService service;

    /**
     * Constructs an {@link AbstractRecordImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    protected AbstractRecordImpl(Act peer, DomainService service) {
        super(peer, service);
        this.service = service;
    }

    /**
     * Returns the patient.
     *
     * @return the patient
     */
    @Override
    public Patient getPatient() {
        IMObject patient = getBean().getTarget("patient");
        if (patient == null) {
            throw new IllegalStateException("Record has no patient");
        }
        return service.create(patient, Patient.class);
    }

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public User getClinician() {
        IMObject clinician = getBean().getTarget("clinician");
        return (clinician != null) ? service.create(clinician, User.class) : null;
    }

    /**
     * Determines if this record is locked.
     * <p/>
     * Once locked, a record should not be updated by users.
     *
     * @return {@code true} if this is locked
     */
    @Override
    public boolean isLocked() {
        return ActStatus.POSTED.equals(getStatus());
    }

    /**
     * Returns the domain service.
     *
     * @return the domain service
     */
    protected DomainService getService() {
        return service;
    }
}
