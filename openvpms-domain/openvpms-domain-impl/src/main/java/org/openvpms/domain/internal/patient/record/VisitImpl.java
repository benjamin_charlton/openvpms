/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.record.Records;
import org.openvpms.domain.patient.record.Visit;

import java.util.List;

/**
 * Default implementation of {@link Visit}.
 *
 * @author Tim Anderson
 */
public class VisitImpl extends AbstractRecordImpl implements Visit {

    /**
     * The medical record rules.
     */
    private final AppointmentRules rules;

    /**
     * Constructs a {@link VisitImpl}.
     *
     * @param peer          the peer to delegate to
     * @param domainService the domain object service
     */
    public VisitImpl(Act peer, DomainService domainService, AppointmentRules rules) {
        super(peer, domainService);
        this.rules = rules;
    }

    /**
     * Determines if this is a boarding visit.
     *
     * @return {@code true} if this is a boarding visit
     */
    @Override
    public boolean isBoardingVisit() {
        Act appointment = getBean().getSource("appointment", Act.class);
        return appointment != null && rules.isBoardingAppointment(appointment);
    }

    /**
     * Returns the visit records.
     *
     * @return the records
     */
    @Override
    public Records getRecords() {
        List<ActRelationship> records = getBean().getValues("items", ActRelationship.class);
        return getService().createRelated(records, RecordsImpl.class);
    }
}
