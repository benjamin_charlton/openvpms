/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.WeightRecord;
import org.openvpms.domain.patient.record.builder.RecordBuilder;
import org.openvpms.domain.patient.record.builder.WeightBuilder;

import java.math.BigDecimal;

/**
 * Default implementation of {@link WeightBuilder}.
 *
 * @author Tim Anderson
 */
public class WeightBuilderImpl<B extends WeightBuilder<B, P, PB>, P extends Record, PB extends RecordBuilder<P, PB>>
        extends ChildRecordBuilderImpl<WeightRecord, B, P, PB>
        implements WeightBuilder<B, P, PB> {

    /**
     * The weight.
     */
    private BigDecimal weight;

    /**
     * The weight units.
     */
    private WeightUnits units;

    /**
     * Constructs a {@link WeightBuilderImpl}.
     *
     * @param parent   the parent builder
     * @param services the builder services
     */
    public WeightBuilderImpl(PB parent, PatientServices services) {
        super(WeightRecord.ARCHETYPE, parent, services);
    }

    /**
     * Sets the weight.
     *
     * @param weight the weight
     * @param units  the weight units
     * @return this
     */
    @Override
    public B weight(BigDecimal weight, WeightUnits units) {
        this.weight = weight;
        this.units = units;
        return getThis();
    }

    /**
     * Builds the record.
     *
     * @param object  the record
     * @param bean    a bean wrapping the record
     * @param context the build context
     */
    @Override
    protected void build(Act object, IMObjectBean bean, BuildContext context) {
        super.build(object, bean, context);
        if (weight == null) {
            throw new IllegalStateException("Weight not set");
        }
        if (weight.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalStateException("Invalid weight: " + weight);
        }
        if (units == null) {
            throw new IllegalStateException("Units not set");
        }
        bean.setValue("weight", weight);
        bean.setValue("units", units);
    }
}
