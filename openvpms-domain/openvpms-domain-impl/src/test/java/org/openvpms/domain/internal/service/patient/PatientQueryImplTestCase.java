/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.patient.PatientQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientQueryImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientQueryImplTestCase extends ArchetypeServiceTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Tests the {@link PatientQueryImpl#id(long)} method.
     */
    @Test
    public void testQueryById() {
        Patient patient = domainService.create(patientFactory.createPatient(), Patient.class);
        PatientQuery query = new PatientQueryImpl(getArchetypeService(), domainService)
                .id(patient.getId());

        checkQuery(query, patient);
        assertEquals(patient, query.findFirst());
    }

    /**
     * Tests the {@link PatientQueryImpl#name(String)} method.
     */
    @Test
    public void testQueryByName() {
        String name = TestHelper.randomName("");
        Patient patientA = createPatient(name);
        Patient patientB = createPatient(name);

        PatientQuery query = new PatientQueryImpl(getArchetypeService(), domainService)
                .name(name);
        checkQuery(query, patientA, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#name(Filter)} method.
     */
    @Test
    public void testQueryByNameFilter() {
        String name = TestHelper.randomName("");
        Patient patientA = createPatient(name + "A");
        Patient patientB = createPatient(name + "B");

        PatientQuery query1 = new PatientQueryImpl(getArchetypeService(), domainService)
                .name(Filter.like(name + "%"));
        checkQuery(query1, patientA, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#active()} and {@link PatientQueryImpl#inactive()}.
     */
    @Test
    public void testQueryByActiveInactive() {
        // create 3 patients, each with the same microchip to limit the amount of data returned
        String microchip = TestHelper.randomName("");
        Patient patientA = createPatient("A", microchip, true);
        Patient patientB = createPatient("B", microchip, false);
        Patient patientC = createPatient("C", microchip, true);

        // verify all patients are returned by default
        PatientQuery query1 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip);
        checkQuery(query1, patientA, patientB, patientC);

        // check active patients
        PatientQuery query2 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip).active();
        checkQuery(query2, patientA, patientC);

        // check inactive patients
        PatientQuery query3 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip).inactive();
        checkQuery(query3, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#microchip(String)} method.
     */
    @Test
    public void testQueryByMicrochip() {
        String microchip1 = TestHelper.randomName("");
        String microchip2 = TestHelper.randomName("");
        Patient patientA = createPatient("A", microchip1);
        Patient patientB = createPatient("B", microchip1);
        Patient patientC = createPatient("C", microchip2);

        PatientQuery query1 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip1);
        checkQuery(query1, patientA, patientB);

        PatientQuery query2 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip2);
        assertEquals(patientC, query2.findFirst());
        checkQuery(query2, patientC);
    }

    /**
     * Tests the {@link PatientQueryImpl#microchip(Filter)} method.
     */
    @Test
    public void testQueryByMicrochipFilter() {
        String microchip = TestHelper.randomName("");
        Patient patientA = createPatient("Spot", microchip + "A");
        Patient patientB = createPatient("Fido", microchip + "B");

        PatientQuery query1 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(Filter.like(microchip + "%"));
        checkQuery(query1, patientA, patientB);
    }


    /**
     * Tests the {@link PatientQueryImpl#orderById()} and {@link PatientQueryImpl#orderById(boolean)} methods.
     */
    @Test
    public void testOrderById() {
        // create 3 patients, each with the same microchip to limit the amount of data returned
        String microchip = TestHelper.randomName("");
        Patient patientA = createPatient("A", microchip);
        Patient patientB = createPatient("B", microchip);
        Patient patientC = createPatient("C", microchip);

        // default should be to order by ascending id, if no order specified.
        PatientQuery query1 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip);
        checkQuery(query1, patientA, patientB, patientC);

        PatientQuery query2 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderById();
        checkQuery(query2, patientA, patientB, patientC);

        PatientQuery query3 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderById(true);
        checkQuery(query3, patientA, patientB, patientC);

        PatientQuery query4 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderById(false);
        checkQuery(query4, patientC, patientB, patientA);
    }

    /**
     * Tests the {@link PatientQueryImpl#orderByName()} and {@link PatientQueryImpl#orderByName(boolean)} methods.
     */
    @Test
    public void testOrderByName() {
        // create 3 patients, each with the same microchip to limit the amount of data returned
        String microchip = TestHelper.randomName("");
        Patient patientA = createPatient("Spot", microchip);
        Patient patientB = createPatient("Fido", microchip);
        Patient patientC = createPatient("Fido", microchip);
        // by default, patient2 should always appear before patient3 as it has a lower id
        assertTrue(patientB.getId() < patientC.getId());

        PatientQuery query1 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderByName();

        checkQuery(query1, patientB, patientC, patientA);

        PatientQuery query2 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderByName(true);
        checkQuery(query2, patientB, patientC, patientA);

        PatientQuery query3 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderByName(false);
        checkQuery(query3, patientA, patientB, patientC);

        // verify can order by name and descending id
        PatientQuery query4 = new PatientQueryImpl(getArchetypeService(), domainService)
                .microchip(microchip)
                .orderByName(true)
                .orderById(false);
        checkQuery(query4, patientC, patientB, patientA);
    }

    /**
     * Verifies a query returns the expected result, in the order given.
     *
     * @param query    the query
     * @param expected the expected results
     */
    private void checkQuery(PatientQuery query, Patient... expected) {
        List<Patient> matches = IterableUtils.toList(query.query());
        assertEquals(Arrays.asList(expected), matches);
    }

    /**
     * Creates a patient with the specified name.
     *
     * @param name the patient name
     * @return a new patient
     */
    private Patient createPatient(String name) {
        Party patient = patientFactory.newPatient()
                .name(name)
                .build();
        return domainService.create(patient, Patient.class);
    }

    /**
     * Creates a patient with the specified name and microchip.
     *
     * @param name      the patient name
     * @param microchip the microchip
     * @return a new patient
     */
    private Patient createPatient(String name, String microchip) {
        return createPatient(name, microchip, true);
    }

    /**
     * Creates a patient with the specified name, microchip and active flag
     *
     * @param name      the patient name
     * @param microchip the microchip
     * @param active    if {@code true} the patient is active, else it is inactive
     * @return a new patient
     */
    private Patient createPatient(String name, String microchip, boolean active) {
        Party patient = patientFactory.newPatient()
                .name(name)
                .addMicrochip(microchip)
                .active(active)
                .build();
        return domainService.create(patient, Patient.class);
    }
}