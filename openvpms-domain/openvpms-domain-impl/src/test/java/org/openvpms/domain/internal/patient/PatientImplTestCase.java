/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.patient.Microchip;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.alert.Allergy;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link PatientImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientImplTestCase extends AbstractDomainObjectTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;


    /**
     * Verifies that the {@link DomainService} can create patients.
     */
    @Test
    public void testFactory() {
        Party party = patientFactory.newPatient().build(false);
        checkFactory(party, Patient.class, PatientImpl.class);
    }

    /**
     * Verifies a domain implementation of a product has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Party party1 = patientFactory.newPatient().build(false);
        Party party2 = patientFactory.newPatient().build(false);
        checkEquality(party1, party2, Patient.class);
    }

    /**
     * Verifies that the {@link PatientImpl} methods return the expected results.
     */
    @Test
    public void testAccessors() {
        Party customer = customerFactory.createCustomer();
        Date dateOfBirth = getDate("2013-01-01");
        Date dateOfDeath = getDate("2019-01-01");
        Party party = patientFactory.newPatient()
                .dateOfBirth(dateOfBirth)
                .deceased(true)
                .dateOfDeath(dateOfDeath)
                .species("CANINE")
                .breed("DINGO")
                .female()
                .desexed(true)
                .colour("Tan")
                .addMicrochip("123456789")
                .owner(customer)
                .build();
        patientFactory.createWeight(party, BigDecimal.TEN);
        Patient patient = getDomainService().create(party, Patient.class);
        assertEquals(dateOfBirth, DateRules.toDate(patient.getDateOfBirth()));
        assertEquals(dateOfDeath, DateRules.toDate(patient.getDateOfDeath()));
        assertTrue(patient.isDeceased());
        Microchip microchip = patient.getMicrochip();
        assertNotNull(microchip);
        assertEquals("123456789", microchip.getIdentity());
        assertEquals("CANINE", patient.getSpeciesCode());
        assertEquals("DINGO", patient.getBreedCode());
        assertEquals(Patient.Sex.FEMALE, patient.getSex());
        assertTrue(patient.isDesexed());
        assertEquals("Tan", patient.getColourName());
        assertEquals(customer.getId(), patient.getOwner().getId());
        Weight weight = patient.getWeight();
        assertNotNull(weight);
        assertEquals(BigDecimal.TEN, weight.getWeight());
        assertEquals(WeightUnits.KILOGRAMS, weight.getUnits());
    }

    /**
     * Tests the {@link PatientImpl#getAge()} method and {@link PatientImpl#getAge(LocalDate)} methods.
     */
    @Test
    public void testGetAge() {
        // patient with no date of birth recorded
        Party party1 = patientFactory.newPatient().build(false);
        Patient patient1 = getDomainService().create(party1, Patient.class);
        assertNull(patient1.getAge());
        assertNull(patient1.getAge(LocalDate.now()));

        // patient with date of birth
        Party party2 = patientFactory.newPatient().dateOfBirth(getDate("2013-01-01")).build(false);
        Patient patient2 = getDomainService().create(party2, Patient.class);
        long years = patient2.getDateOfBirth().until(LocalDate.now(), ChronoUnit.YEARS);
        assertTrue(patient2.getAge().startsWith(years + " Years"));
        assertEquals("5 Years", patient2.getAge(LocalDate.of(2018, 1, 1)));

        // patient with date of birth, date of death
        Party party3 = patientFactory.newPatient().dateOfBirth(getDate("2013-01-01"))
                .deceased(true).dateOfDeath(getDate("2017-01-01")).build(false);
        Patient patient3 = getDomainService().create(party3, Patient.class);
        assertEquals("4 Years", patient3.getAge());
        assertEquals("3 Years", patient2.getAge(LocalDate.of(2016, 1, 1)));
        assertEquals("4 Years", patient3.getAge(LocalDate.now()));

        // patient with date of death
        Party party4 = patientFactory.newPatient()
                .deceased(true).dateOfDeath(getDate("2017-01-01")).build(false);
        Patient patient4 = getDomainService().create(party4, Patient.class);
        assertNull(patient4.getAge());
        assertNull(patient4.getAge(LocalDate.now()));
    }

    /**
     * Tests the {@link Patient#isAggressive()} method.
     */
    @Test
    public void testIsAggressive() {
        Party party = patientFactory.createPatient();
        Patient patient = getDomainService().create(party, Patient.class);
        assertFalse(patient.isAggressive());

        Entity type = patientFactory.newAlertType()
                .alertType(PatientRules.AGGRESSION_ALERT_TYPE)
                .build();
        patientFactory.newAlert()
                .patient(patient)
                .alertType(type)
                .build();

        assertTrue(patient.isAggressive());
    }

    /**
     * Tests the {@link Patient#getAllergies()} method.
     */
    @Test
    public void testAllergies() {
        Party party = patientFactory.createPatient();
        Patient patient = getDomainService().create(party, Patient.class);
        User clinician1 = userFactory.createClinician();
        User clinician2 = userFactory.createClinician();
        assertFalse(patient.getAllergies().getCurrent().iterator().hasNext());

        Entity type = patientFactory.newAlertType()
                .alertType(PatientRules.ALLERGY_ALERT_TYPE)
                .build();
        patientFactory.newAlert()
                .startTime("2020-01-01")
                .patient(patient)
                .alertType(type)
                .reason("allergy 1")
                .notes("notes 1")
                .clinician(clinician1)
                .build();
        patientFactory.newAlert()
                .startTime("2021-06-1")
                .patient(patient)
                .alertType(type)
                .reason("allergy 2")
                .notes("notes 2")
                .clinician(clinician2)
                .build();

        List<Allergy> allergies = new ArrayList<>();
        CollectionUtils.addAll(allergies, patient.getAllergies().getCurrent().iterator());
        assertEquals(2, allergies.size());
        checkAllergy(allergies.get(0), "2020-01-01", "allergy 1", "notes 1", clinician1);
        checkAllergy(allergies.get(1), "2021-06-01", "allergy 2", "notes 2", clinician2);
    }

    /**
     * Verifies an allergy matches that expected.
     *
     * @param actual    the actual allergy
     * @param date      the expected date
     * @param allergy   the expected allergy
     * @param notes     the expected notes
     * @param clinician the expected clinician
     */
    private void checkAllergy(Allergy actual, String date, String allergy, String notes, User clinician) {
        assertEquals(DateRules.toOffsetDateTime(TestHelper.getDate(date)), actual.getDate());
        assertEquals(allergy, actual.getAllergy());
        assertEquals(notes, actual.getNotes());
        assertEquals(clinician, actual.getClinician());
    }

}
