/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.practice;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.practice.Practice;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

/**
 * Tests the {@link PracticeServiceImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PracticeServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * The underlying service.
     */
    @Autowired
    private org.openvpms.archetype.rules.practice.PracticeService service;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests the {@link PracticeServiceImpl#getPractice()} method.
     */
    @Test
    public void testGetPractice() {
        Party party = practiceFactory.newPractice().name("Practice A").build();
        PracticeServiceImpl practiceService = new PracticeServiceImpl(service, domainService);
        Practice practice1 = practiceService.getPractice();
        assertNotNull(practice1);
        assertEquals(party, practice1);

        // verify the underlying Party isn't shared. This is to prevent plugins changing the practice globally
        practice1.setName("Practice B");
        assertNotEquals(party.getName(), practice1.getName());

        // verify the Practice isn't shared as well
        Practice practice2 = practiceService.getPractice();
        assertEquals(party, practice2);
        assertNotSame(practice1, practice2);
        assertNotEquals(practice1.getName(), practice2.getName());
    }

    /**
     * Tests the {@link PracticeServiceImpl#getLocations()} method.
     */
    @Test
    public void testGetLocations() {
        Party party = practiceFactory.newLocation()
                .name("Location A")
                .build();
        practiceFactory.newPractice()
                .locations(party)
                .build();

        PracticeServiceImpl practiceService = new PracticeServiceImpl(service, domainService);
        List<Location> locations1 = practiceService.getLocations();
        assertEquals(1, locations1.size());

        // verify the underlying Party isn't shared. This is to prevent plugins changing the location globally
        Location location1 = locations1.get(0);
        location1.setName("Location B");
        assertNotEquals(party.getName(), location1.getName());

        // verify the Location isn't shared as well
        List<Location> locations2 = practiceService.getLocations();
        assertEquals(1, locations2.size());

        Location location2 = locations2.get(0);
        assertNotEquals(location1.getName(), location2.getName());
    }
}
