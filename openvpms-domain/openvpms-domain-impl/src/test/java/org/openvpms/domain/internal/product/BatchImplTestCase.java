/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.product.Batch;
import org.openvpms.domain.service.object.DomainObjectService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests the {@link BatchImpl} class.
 *
 * @author Tim Anderson
 */
public class BatchImplTestCase extends AbstractDomainObjectTest {

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Verifies a domain implementation of a product has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Product product = productFactory.createMedication();
        Entity batch1 = productFactory.newBatch().product(product).batchNo("1234").build();
        Entity batch2 = productFactory.newBatch().product(product).batchNo("5678").build();
        checkEquality(batch1, batch2, Batch.class);
    }

    /**
     * Verifies a domain object can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Override
    public void testFactory() {
        Product product = productFactory.createMerchandise();
        Entity batch = productFactory.newBatch().product(product).batchNo("1234").build();
        checkFactory(batch, Batch.class, BatchImpl.class);
    }
}
