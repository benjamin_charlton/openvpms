/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.product;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.service.product.MedicationQuery;
import org.openvpms.domain.service.product.MerchandiseQuery;
import org.openvpms.domain.service.product.ProductQuery;
import org.openvpms.domain.service.product.Products;
import org.openvpms.domain.service.product.ServiceQuery;
import org.openvpms.domain.service.product.TemplateQuery;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.assertExcluded;
import static org.openvpms.archetype.test.TestHelper.assertIncluded;

/**
 * Tests the {@link ProductsImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ProductsImplTestCase extends ArchetypeServiceTest {

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The products service.
     */
    private Products products;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        products = new ProductsImpl(getArchetypeService(), domainService);
    }

    /**
     * Tests the {@link ProductsImpl#getProduct(long)} method.
     */
    @Test
    public void testGetProduct() {
        Product medication = productFactory.createMedication();
        Product merchandise = productFactory.createMerchandise();
        Product service = productFactory.createService();
        Product template = productFactory.createTemplate();

        assertEquals(medication, products.getProduct(medication.getId()));
        assertEquals(merchandise, products.getProduct(merchandise.getId()));
        assertEquals(service, products.getProduct(service.getId()));
        assertEquals(template, products.getProduct(template.getId()));
    }

    /**
     * Tests the {@link ProductsImpl#getQuery()} method.
     */
    @Test
    public void testQuery() {
        Entity productType1 = productFactory.createProductType();
        Entity productType2 = productFactory.createProductType();
        Product medication = productFactory.newMedication().name(ValueStrategy.random()).type(productType1).build();
        Product merchandise = productFactory.newMerchandise().name(ValueStrategy.random()).type(productType2).build();
        Product service = productFactory.newService().name(ValueStrategy.random()).type(productType2).build();
        Product template = productFactory.newTemplate().name(ValueStrategy.random()).build();

        // query by product type
        ProductQuery query1 = products.getQuery().productType(productType1.getName());
        assertIncluded(query1.query(), medication);
        assertExcluded(query1.query(), merchandise, service, template);

        ProductQuery query2 = products.getQuery().productType(productType2.getName());
        assertIncluded(query2.query(), merchandise, service);
        assertExcluded(query2.query(), medication, template);

        // query by id
        ProductQuery query3 = products.getQuery().id(medication.getId());
        assertIncluded(query3.query(), medication);
        assertExcluded(query3.query(), merchandise, service, template);

        // query by name
        ProductQuery query4 = products.getQuery().name(medication.getName());
        assertIncluded(query4.query(), medication);
        assertExcluded(query4.query(), merchandise, service, template);

        // query by medication
        MedicationQuery query5 = products.getQuery().medications();
        assertIncluded(query5.query(), medication);
        assertExcluded(query5.query(), merchandise, service, template);

        // query by merchandise
        MerchandiseQuery query6 = products.getQuery().merchandise();
        assertIncluded(query6.query(), merchandise);
        assertExcluded(query6.query(), medication, service, template);

        // query by service
        ServiceQuery query7 = products.getQuery().services();
        assertIncluded(query7.query(), service);
        assertExcluded(query7.query(), medication, merchandise, template);

        // query by service
        TemplateQuery query8 = products.getQuery().templates();
        assertIncluded(query8.query(), template);
        assertExcluded(query8.query(), medication, merchandise, service);
    }

    /**
     * Tests the {@link ProductsImpl#getBatch(Product, String)} method.
     */
    @Test
    public void testGetBatch() {
        Product medication = productFactory.createMedication();
        assertNull(products.getBatch(medication, "123456"));

        Entity batch = productFactory.newBatch().product(medication).batchNo("123456").build();
        assertEquals(batch, products.getBatch(medication, "123456"));
    }
}
