/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.user;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.user.Users;
import org.openvpms.domain.user.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.assertExcluded;
import static org.openvpms.archetype.test.TestHelper.assertIncluded;

/**
 * Tests the {@link UsersImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class UsersImplTestCase extends ArchetypeServiceTest {

    /**
     * The user factory.
     */
    @Autowired
    TestUserFactory userFactory;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainObjectService;

    /**
     * The users service.
     */
    private Users users;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        users = new UsersImpl(getArchetypeService(), domainObjectService);
    }

    /**
     * Tests the {@link UsersImpl#getUser(long)} method.
     */
    @Test
    public void testGetUser() {
        assertNull(users.getUser(0));

        User user1 = userFactory.createUser();
        User result1 = users.getUser(user1.getId());
        assertEquals(user1, result1);
        assertFalse(result1 instanceof Employee);

        User user2 = userFactory.createUser("J", "Bloggs");
        User result2 = users.getUser(user2.getId());
        assertEquals(user2, result2);
        assertTrue(result2 instanceof Employee);

        // verify inactive users can be retrieved
        user1.setActive(false);
        save(user1);
        assertEquals(user1, users.getUser(user1.getId()));
    }

    /**
     * Tests the {@link UsersImpl#getUser(String)} method.
     */
    @Test
    public void testGetUserByUsername() {
        assertNull(users.getUser(0));

        User user1 = userFactory.createUser();
        User result1 = users.getUser(user1.getUsername());
        assertEquals(user1, result1);
        assertFalse(result1 instanceof Employee);

        User user2 = userFactory.createUser("J", "Bloggs");
        User result2 = users.getUser(user2.getUsername());
        assertEquals(user2, result2);
        assertTrue(result2 instanceof Employee);

        // verify inactive users can be retrieved
        user1.setActive(false);
        save(user1);
        assertEquals(user1, users.getUser(user1.getUsername()));
    }

    /**
     * Tests querying by name.
     */
    @Test
    public void testQueryByName() {
        User user1 = userFactory.createUser("A", TestHelper.randomName("XUserA"));
        User user2 = userFactory.createUser("B", TestHelper.randomName("XUserB"));
        User user3 = userFactory.createUser("C", TestHelper.randomName("YUserC"));

        // equals
        assertEquals(user1, users.getQuery().name(user1.getName()).findFirst());
        assertEquals(user1, users.getQuery().name(Filter.equal(user1.getName())).findFirst());

        // not equals
        assertNotEquals(user1, users.getQuery().name(Filter.notEqual(user1.getName())).findFirst());

        // like
        List<User> matches = IterableUtils.toList(users.getQuery().name(Filter.like("%XUser%")).query());
        assertTrue(matches.contains(user1));
        assertTrue(matches.contains(user2));
        assertExcluded(matches, user3);
    }

    /**
     * Tests querying active and inactive users.
     */
    @Test
    public void testQueryActive() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser();
        User user3 = userFactory.newUser().active(false).build();

        List<User> matches1 = IterableUtils.toList(users.getQuery().query());
        assertIncluded(matches1, user1, user2, user3);

        List<User> matches2 = IterableUtils.toList(users.getQuery().active().query());
        assertIncluded(matches2, user1, user2);
        assertExcluded(matches2, user3);

        List<User> matches3 = IterableUtils.toList(users.getQuery().inactive().query());
        assertIncluded(matches3, user3);
        assertExcluded(matches3, user1, user2);
    }

    /**
     * Tests querying by username.
     */
    @Test
    public void testQueryByUsername() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser();
        User user3 = userFactory.createUser();

        List<User> matches1 = IterableUtils.toList(users.getQuery().username(user1.getUsername()).query());
        assertIncluded(matches1, user1);
        assertExcluded(matches1, user2, user3);

        // check filtering by active/inactive
        user1.setActive(false);
        save(user1);

        List<User> matches2 = IterableUtils.toList(users.getQuery().username(user1.getUsername()).query());
        assertIncluded(matches2, user1);
        assertExcluded(matches2, user2, user3);

        List<User> matches3 = IterableUtils.toList(users.getQuery().username(user1.getUsername()).active().query());
        assertExcluded(matches3, user1, user2, user3);

        List<User> matches4 = IterableUtils.toList(users.getQuery().username(user1.getUsername()).inactive().query());
        assertIncluded(matches1, user1);
        assertExcluded(matches4, user2, user3);
    }

    /**
     * Tests querying by employee.
     */
    @Test
    @SuppressWarnings("unhecked")
    public void testQueryByEmployee() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser("J", "Bloggs");
        User user3 = userFactory.createUser("A", "Smith");
        List<User> matches = new ArrayList<>(IterableUtils.toList(users.getQuery().employees().query()));
        assertIncluded(matches, user2, user3);
        assertExcluded(matches, user1);
    }

    /**
     * Tests querying by clinician.
     */
    @Test
    public void testQueryByClinician() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createClinician();
        User user3 = userFactory.createClinician();
        List<User> matches1 = IterableUtils.toList(users.getQuery().clinicians().query());
        assertIncluded(matches1, user2, user3);
        assertExcluded(matches1, user1);

        // check filtering by active/inactive
        user2.setActive(false);
        save(user2);

        List<User> matches2 = IterableUtils.toList(users.getQuery().clinicians().query());
        assertIncluded(matches2, user2, user3);
        assertExcluded(matches2, user1);

        List<User> matches3 = IterableUtils.toList(users.getQuery().clinicians().active().query());
        assertIncluded(matches3, user3);
        assertExcluded(matches3, user1, user2);

        List<User> matches4 = IterableUtils.toList(users.getQuery().clinicians().inactive().query());
        assertIncluded(matches4, user2);
        assertExcluded(matches4, user1, user3);
    }
}
