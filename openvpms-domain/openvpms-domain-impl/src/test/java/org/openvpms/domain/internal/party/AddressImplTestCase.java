/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.party;

import org.junit.Test;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.party.TestLocationContactBuilder;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Contact;
import org.openvpms.domain.internal.factory.DomainService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link AddressImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AddressImplTestCase extends ArchetypeServiceTest {

    /**
     * The party rules.
     */
    @Autowired
    private PartyRules partyRules;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService service;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Tests the address.
     */
    @Test
    public void testAddress() {
        TestLocationContactBuilder<?, ?> builder = new TestLocationContactBuilder<>(getArchetypeService());
        Contact contact = builder.address("1234 Main Rd")
                .suburbCode("KONGWAK")
                .suburbName("Kongwak")
                .stateCode("VIC")
                .stateName("Victoria")
                .postCode("9876")
                .build(false);

        AddressImpl address = new AddressImpl(contact, partyRules, service);
        assertEquals("1234 Main Rd", address.getAddress());

        assertEquals("KONGWAK", address.getSuburbCode());
        assertEquals("Kongwak", address.getSuburbName());
        assertNotNull(address.getSuburbLookup());
        assertEquals("KONGWAK", address.getSuburbLookup().getCode());

        assertEquals("VIC", address.getStateCode());
        assertEquals("Victoria", address.getStateName());
        assertNotNull(address.getStateLookup());
        assertEquals("VIC", address.getStateLookup().getCode());

        assertEquals("9876", address.getPostcode());

        // by default, addresses don't have a country set, but instead use the system default
        Lookup nz = lookupFactory.newCountry().code("NZ").name("New Zealand").isDefault(true).build();

        assertEquals("NZ", address.getCountryCode());
        assertEquals("New Zealand", address.getCountryName());
        assertEquals(nz, address.getCountryLookup());

        Lookup au = lookupFactory.newCountry().code("AU").name("Australia").isDefault(true).build();

        assertEquals("AU", address.getCountryCode());
        assertEquals("Australia", address.getCountryName());
        assertEquals(au, address.getCountryLookup());
    }
}
