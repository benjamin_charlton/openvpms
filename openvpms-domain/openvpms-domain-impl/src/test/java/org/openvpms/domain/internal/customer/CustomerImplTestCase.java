/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer;

import org.junit.Test;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.party.TestLocationContactBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.CustomerPatients;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.patient.Patient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link CustomerImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CustomerImplTestCase extends AbstractDomainObjectTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Verifies a domain implementation of a product has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Party party1 = customerFactory.newCustomer().build(false);
        Party party2 = customerFactory.newCustomer().build(false);
        checkEquality(party1, party2, Customer.class);
    }

    /**
     * Verifies that the {@link DomainService} can create customers.
     */
    @Test
    public void testFactory() {
        Party party = customerFactory.newCustomer().build(false);
        checkFactory(party, Customer.class, CustomerImpl.class);
    }

    /**
     * Verifies that {@link Customer} methods return the expected results.
     */
    @Test
    public void testAccessors() {
        Party party = customerFactory.newCustomer().title("MS").firstName("J").lastName("Smith")
                .companyName("Smith Co")
                .addAddress("31 Foo St", "SALE", "VIC", "3000", ContactArchetypes.BILLING_PURPOSE)
                .addPhone("91234567", ContactArchetypes.HOME_PURPOSE)
                .addPhone("93334444", ContactArchetypes.WORK_PURPOSE)
                .addMobilePhone("41234567")
                .addEmail("jsmith@foo.com").build();
        Party patient1 = patientFactory.newPatient().owner(party).name("Fido").active(false).build();
        Party patient2 = patientFactory.newPatient().owner(party).name("Spot").build();
        Customer customer = getDomainService().create(party, Customer.class);
        assertTrue(customer instanceof CustomerImpl);
        assertEquals("J", customer.getFirstName());
        assertEquals("Smith", customer.getLastName());
        assertEquals("Ms J Smith", customer.getFullName());
        assertEquals("Smith Co", customer.getCompanyName());
        Address address = customer.getAddress();
        Address mailing = customer.getMailingAddress();
        assertNotNull(address);
        assertEquals(address, mailing);
        assertEquals("31 Foo St", address.getAddress());
        assertEquals("SALE", address.getSuburbCode());
        assertEquals("VIC", address.getStateCode());
        assertEquals("3000", address.getPostcode());
        checkPhone(customer.getHomePhone(), "91234567", false);
        checkPhone(customer.getWorkPhone(), "93334444", false);
        checkPhone(customer.getMobilePhone(), "41234567", true);
        assertEquals("jsmith@foo.com", customer.getEmail().getEmailAddress());

        CustomerPatients patients = customer.getPatients();
        checkPatients(patients.getObjects(), patient1, patient2);
        checkPatients(patients.active().getObjects(), patient2);

        // now add a mailing address
        TestLocationContactBuilder<?, ?> builder = new TestLocationContactBuilder<>(getArchetypeService());
        party.addContact(builder.address("PO Box 1").suburbCode("SALE").stateCode("VIC").postCode("3085").
                purposes(ContactArchetypes.CORRESPONDENCE_PURPOSE).build());

        Address mailing2 = customer.getMailingAddress();
        assertNotNull(mailing);
        assertNotEquals(mailing2, customer.getAddress());
        assertEquals("PO Box 1", mailing2.getAddress());
    }

    /**
     * Verifies patients are present.
     *
     * @param actual   the actual patients
     * @param expected the expected patients
     */
    private void checkPatients(Iterable<Patient> actual, Party... expected) {
        List<Patient> patients = StreamSupport.stream(actual.spliterator(), false)
                .collect(Collectors.toList());
        assertEquals(expected.length, patients.size());
        for (Party expectedPatient : expected) {
            boolean found = false;
            for (Party actualPatient : patients) {
                if (expectedPatient.getId() == actualPatient.getId()) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                fail("Failed to find patient=" + expectedPatient.getName() + ", id=" + expectedPatient.getId());
            }
        }
    }

    /**
     * Verifies a phone number matches that expected.
     *
     * @param phone  the phone
     * @param number the expected number
     * @param mobile if {@code true}, the phone is a mobile
     */
    private void checkPhone(Phone phone, String number, boolean mobile) {
        assertNotNull(phone);
        assertEquals(number, phone.getPhoneNumber());
        assertEquals(mobile, phone.isMobile());
        assertFalse(phone.isFax());
    }

}
