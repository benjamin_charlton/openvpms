/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.patient.Patient;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link PatientsImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientsImplTestCase extends ArchetypeServiceTest {

    /**
     * The patient services.
     */
    @Autowired
    private PatientServices patientServices;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Tests the {@link PatientsImpl#getPatient(long)} method.
     */
    @Test
    public void testGetPatient() {
        PatientsImpl patients = new PatientsImpl(patientServices);

        Party party = patientFactory.createPatient();

        Patient patient1 = patients.getPatient(party.getId());
        assertNotNull(patient1);
        assertEquals(party, patient1);

        // now check a patient that doesn't exist
        Patient patient2 = patients.getPatient(0);
        assertNull(patient2);
    }

}
