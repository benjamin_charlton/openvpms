/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.service.object.DomainObjectService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests the {@link InvoiceItemImpl} class.
 *
 * @author Tim Anderson
 */
public class InvoiceItemImplTestCase extends AbstractDomainObjectTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Tests the {@link InvoiceItemImpl#equals(Object)} and {@link InvoiceItemImpl#hashCode()} methods.
     */
    @Test
    public void testEquality() {
        FinancialAct act1 = new TestInvoiceItemBuilder(null, getArchetypeService())
                .patient(patientFactory.createPatient())
                .product(productFactory.createMerchandise())
                .build(false);
        FinancialAct act2 = new TestInvoiceItemBuilder(null, getArchetypeService())
                .patient(patientFactory.createPatient())
                .product(productFactory.createMerchandise())
                .build(false);
        checkEquality(act1, act2, InvoiceItem.class);
    }

    /**
     * Verifies an {@link InvoiceImpl} can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Test
    public void testFactory() {
        FinancialAct act = new TestInvoiceItemBuilder(null, getArchetypeService())
                .patient(patientFactory.createPatient())
                .product(productFactory.createMerchandise())
                .build(false);

        checkFactory(act, InvoiceItem.class, InvoiceItemImpl.class);
    }
}
