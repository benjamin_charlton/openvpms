/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.patient;

import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.DomainQuery;
import org.openvpms.domain.query.Filter;

/**
 * Patient query.
 *
 * @author Tim Anderson
 */
public interface PatientQuery extends DomainQuery<Patient, PatientQuery> {

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip
     * @return this
     */
    PatientQuery microchip(String microchip);

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip
     * @return this
     */
    PatientQuery microchip(Filter<String> microchip);

}
