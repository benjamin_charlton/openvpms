/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.product;

import org.openvpms.component.model.product.Product;
import org.openvpms.domain.product.Batch;

/**
 * Manages products.
 *
 * @author Tim Anderson
 */
public interface Products {

    /**
     * Returns a product given its identifier.
     *
     * @param id the product identifier
     * @return the corresponding product, or {@code null} if none is found
     */
    Product getProduct(long id);

    /**
     * Returns a product query.
     *
     * @return a new query
     */
    ProductQuery getQuery();

    /**
     * Returns a batch for a product.
     *
     * @param product     the product
     * @param batchNumber the batch number
     * @return the corresponding batch, or {@code null} if none is found
     */
    Batch getBatch(Product product, String batchNumber);
}
