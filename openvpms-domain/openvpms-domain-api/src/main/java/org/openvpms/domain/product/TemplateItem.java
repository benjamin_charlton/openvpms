/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.product;

import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;

/**
 * An item in a {@link Template}.
 *
 * @author Tim Anderson
 */
public interface TemplateItem {

    /**
     * Returns the product.
     *
     * @return the product
     */
    Product getProduct();

    /**
     * The minimum quantity.
     *
     * @return the minimum quantity
     */
    BigDecimal getLowQuantity();

    /**
     * The minimum quantity.
     *
     * @return the minimum quantity
     */
    BigDecimal getHighQuantity();

    /**
     * The minimum patient weight that this item applies to.
     *
     * @return the minimum weight, inclusive
     */
    BigDecimal getMinWeight();

    /**
     * The maximum patient weight that this item applies to.
     *
     * @return the maximum weight, exclusive
     */
    BigDecimal getMaxWeight();

    /**
     * The weight units.
     *
     * @return the weight units
     */
    WeightUnits getWeightUnits();

    /**
     * Determines if inclusion of the item requires a patient weight.
     *
     * @return {@code true} if a patient weight is required, otherwise {@code false}
     */
    boolean requiresWeight();

    /**
     * Determines if a patient weight is in the weight range for this item.
     *
     * @param weight the patient weight
     * @return {@code true} if the patient weight is within the weight range
     */
    boolean inWeightRange(Weight weight);

    /**
     * Determines if this item should be skipped if the product is not available.
     *
     * @return {@code true} if the item should be skipped, otherwise {@code false}
     */
    boolean getSkipIfMissing();

    /**
     * Determines if this item should have a zero price when charged.
     *
     * @return {@code true} if the item should have a zero price, otherwise {@code false}
     */
    boolean getZeroPrice();

    /**
     * Determines if this item should be printed.
     *
     * @return {@code true} if the item should be printed, {@code false} if printing should be suppressed
     */
    boolean getPrint();

}
