/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.product;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;

import java.time.OffsetDateTime;

/**
 * Product batch.
 *
 * @author Tim Anderson
 */
public interface Batch extends Entity {

    /**
     * Returns the batch number.
     *
     * @return the batch number
     */
    String getBatchNumber();

    /**
     * Returns the expiry date.
     *
     * @return the expiry date. May be {@code null}
     */
    OffsetDateTime getExpiry();

    /**
     * Returns the product.
     *
     * @return the product
     */
    Product getProduct();
}
