/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.practice;

import org.openvpms.domain.practice.Location;
import org.openvpms.domain.practice.Practice;

import java.util.List;

/**
 * Service to access the practice.
 *
 * @author Tim Anderson
 */
public interface PracticeService {

    /**
     * Returns the practice.
     *
     * @return the practice, or {@code null} if none is available
     */
    Practice getPractice();

    /**
     * Returns the active practice locations.
     *
     * @return the practice locations
     */
    List<Location> getLocations();

    /**
     * Returns a practice location given its identifier.
     *
     * @param id the practice location identifier
     * @return the corresponding location, or {@code null} if none is found. The location may be inactive
     */
    Location getLocation(long id);
}
