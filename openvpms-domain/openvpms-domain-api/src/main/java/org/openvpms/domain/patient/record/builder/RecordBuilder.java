/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.record.builder;

import org.openvpms.component.model.user.User;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;

import java.time.OffsetDateTime;

/**
 * A builder for {@link Record} instances.
 *
 * @author Tim Anderson
 */
public interface RecordBuilder<R extends Record, RB extends RecordBuilder<R, RB>> {

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    Patient getPatient();

    /**
     * Returns the record date.
     *
     * @return the record date. May be {@code null}
     */
    OffsetDateTime getDate();

    /**
     * Sets the record date.
     *
     * @param date the date
     * @return this
     */
    RB date(OffsetDateTime date);

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    User getClinician();

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    RB clinician(User clinician);

    /**
     * Adds an identity.
     * <p/>
     * The identity must be unique for the record type.
     *
     * @param archetype the identity archetype
     * @param identity  the identity
     * @return this
     */
    RB addIdentity(String archetype, String identity);

}
