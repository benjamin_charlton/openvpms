/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.customer.transaction;

import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.patient.Patient;

import java.math.BigDecimal;

/**
 * Customer charge item.
 *
 * @author Tim Anderson
 */
public interface ChargeItem extends TransactionItem {

    /**
     * Returns the patient the item was for.
     *
     * @return the patient the item was for
     */
    Patient getPatient();

    /**
     * Returns the product.
     *
     * @return the product
     */
    Product getProduct();

    /**
     * Returns the quantity.
     *
     * @return the quantity
     */
    BigDecimal getQuantity();

    /**
     * Returns the clinician responsible for this charge item.
     *
     * @return the clinician. May be {@code null}
     */
    User getClinician();

    /**
     * Returns the discount amount, including tax.
     *
     * @return the discount amount
     */
    BigDecimal getDiscount();

    /**
     * Returns the discount tax amount.
     *
     * @return the discount tax amount
     */
    BigDecimal getDiscountTax();

    /**
     * Returns the total tax amount.
     *
     * @return the tax amount
     */
    BigDecimal getTotalTax();

}
