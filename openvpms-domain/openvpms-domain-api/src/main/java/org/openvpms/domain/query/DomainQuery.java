/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.query;

/**
 * Domain object query.
 * <p/>
 * The order methods can be chained to specify composite orders e.g.:
 * <br/>
 * <br/>
 * {@code query.orderByName(true).orderById(false)}
 * <br/>
 * <br/>
 * will sort on ascending name and descending id.
 *
 *
 * @author Tim Anderson
 */
public interface DomainQuery<D, Q extends DomainQuery<D, Q>> {

    /**
     * Filter by identifier.
     *
     * @param id the identifier
     * @return this
     */
    Q id(long id);

    /**
     * Filter by name.
     *
     * @param name the name to filter on
     * @return this
     */
    Q name(String name);

    /**
     * Filter by name.
     *
     * @param name the name criteria
     * @return this
     */
    Q name(Filter<String> name);

    /**
     * Only query active objects.
     *
     * @return this
     */
    Q active();

    /**
     * Only query inactive objects.
     *
     * @return this
     */
    Q inactive();

    /**
     * Order objects on ascending id.
     * <p/>
     * This is the default ordering.
     *
     * @return this
     */
    Q orderById();

    /**
     * Order objects by id.
     *
     * @param ascending if {@code true}, order on ascending id, else order on descending id
     * @return this
     */
    Q orderById(boolean ascending);

    /**
     * Order objects on ascending name.
     *
     * @return this
     */
    Q orderByName();

    /**
     * Order objects by name.
     *
     * @param ascending if {@code true}, order on ascending name, else order on descending name
     * @return this
     */
    Q orderByName(boolean ascending);

    /**
     * Sets the first result.
     *
     * @param firstResult the first result
     * @return this
     */
    Q firstResult(int firstResult);

    /**
     * The maximum no. of results to return in any given query.
     *
     * @param maxResults the maximum no. of results
     * @return this
     */
    Q maxResults(int maxResults);

    /**
     * Returns the first result matching the query.
     *
     * @return the first result, or {@code null} if none is found
     */
    D findFirst();

    /**
     * Executes the query.
     *
     * @return the query results
     */
    Iterable<D> query();
}
