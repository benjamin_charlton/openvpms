/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.record.builder;

import org.openvpms.domain.patient.record.Note;
import org.openvpms.domain.patient.record.Record;

/**
 * A builder for {@link Note} instances.
 *
 * @author Tim Anderson
 */
public interface NoteBuilder<B extends NoteBuilder<B, P, PB>, P extends Record, PB extends RecordBuilder<P, PB>>
        extends ChildRecordBuilder<Note, B, P, PB> {

    /**
     * Sets the note.
     *
     * @param note the note
     * @return this
     */
    B note(String note);

}
