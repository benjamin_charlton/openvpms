/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.query;

/**
 * Domain query filter criteria.
 *
 * @author Tim Anderson
 */
public class Filter<T> {

    public enum Operator {
        EQUAL,
        NOT_EQUAL,
        LIKE
    }

    private final T value;

    private final Operator operator;

    /**
     * Constructs a {@link Filter}.
     *
     * @param value    the value to filter by
     * @param operator the filter operator
     */
    private Filter(T value, Operator operator) {
        this.value = value;
        this.operator = operator;
    }

    /**
     * Returns the value to filter by.
     *
     * @return the value. May be {@code null}
     */
    public T getValue() {
        return value;
    }

    /**
     * Returns the operator.
     *
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    public static <T> Filter<T> equal(T value) {
        return new Filter<>(value, Operator.EQUAL);
    }

    public static <T> Filter<T> notEqual(T value) {
        return new Filter<>(value, Operator.NOT_EQUAL);
    }

    public static Filter<String> like(String value) {
        return new Filter<>(value, Operator.LIKE);
    }
}
