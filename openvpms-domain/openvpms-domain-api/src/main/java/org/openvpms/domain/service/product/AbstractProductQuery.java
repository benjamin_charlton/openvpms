/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.product;

import org.openvpms.component.model.product.Product;
import org.openvpms.domain.query.DomainQuery;

/**
 * Product query.
 *
 * @author Tim Anderson
 */
public interface AbstractProductQuery<D extends Product,
        Q extends AbstractProductQuery<D, Q>> extends DomainQuery<D, Q> {

    /**
     * Filter products by product type name.
     *
     * @param name the product type name
     * @return this
     */
    Q productType(String name);

    /**
     * Return medication products.
     *
     * @return a medication query
     */
    MedicationQuery medications();

    /**
     * Return merchandise products.
     *
     * @return a merchandise query
     */
    MerchandiseQuery merchandise();

    /**
     * Return service products.
     *
     * @return a service query
     */
    ServiceQuery services();

    /**
     * Return product templates.
     *
     * @return a template query
     */
    TemplateQuery templates();

}
