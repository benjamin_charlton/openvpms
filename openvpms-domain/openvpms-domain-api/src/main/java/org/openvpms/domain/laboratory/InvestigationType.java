/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;

import java.util.List;

/**
 * Denotes the type of an investigation, such as a blood test or x-ray.
 *
 * @author Tim Anderson
 */
public interface InvestigationType extends Entity {

    /**
     * The investigation type archetype.
     */
    String ARCHETYPE = "entity.investigationType";

    /**
     * Returns the laboratory assigned investigation type id.
     * <p/>
     * This is short for: {@code getTypeIdentity().getIdentity()}
     *
     * @return the type id, or {@code null} if the investigation type is not managed by a laboratory
     */
    String getTypeId();

    /**
     * Returns the laboratory assigned identifier for this type.
     *
     * @return the type identity, or {@code null} if the investigation type is not managed by a laboratory
     */
    Identity getTypeIdentity();

    /**
     * Returns the laboratory to submit orders to.
     *
     * @return the laboratory, or {@code null} if ordering is not supported
     */
    Laboratory getLaboratory();

    /**
     * Returns the devices that can perform tests for this investigation type.
     *
     * @return the devices
     */
    List<Device> getDevices();
}
