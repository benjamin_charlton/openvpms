/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.alert;

import org.openvpms.component.model.user.User;

import java.time.OffsetDateTime;

/**
 * Patient allergy alert.
 *
 * @author Tim Anderson
 */
public interface Allergy {

    /**
     * Returns the date when the allergy was recorded.
     *
     * @return the date
     */
    OffsetDateTime getDate();

    /**
     * Returns a summary of the allergy.
     *
     * @return a summary of the allergy
     */
    String getAllergy();

    /**
     * Return notes about the allergy.
     *
     * @return the notes. May be {@code null}
     */
    String getNotes();

    /**
     * Returns the clinician the recorded the allergy.
     *
     * @return the clinician. May be {@code null}
     */
    User getClinician();

}