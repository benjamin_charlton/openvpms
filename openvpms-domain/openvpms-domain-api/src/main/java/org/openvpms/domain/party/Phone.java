/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.party;

import org.openvpms.component.model.party.Contact;

/**
 * Phone contact.
 *
 * @author Tim Anderson
 */
public interface Phone extends Contact {

    /**
     * Phone contact archetype.
     */
    String ARCHETYPE = "contact.phoneNumber";

    /**
     * Returns the phone number.
     *
     * @return the phone number. May be {@code null}
     */
    String getPhoneNumber();

    /**
     * Determines if this is the preferred phone contact.
     *
     * @return {@code true} if this is the preferred phone contact
     */
    boolean isPreferred();

    /**
     * Determines if this a mobile phone.
     *
     * @return {@code true} if this is a mobile phone
     */
    boolean isMobile();

    /**
     * Determines if this a fax.
     *
     * @return {@code true} if this is a fax
     */
    boolean isFax();

}
