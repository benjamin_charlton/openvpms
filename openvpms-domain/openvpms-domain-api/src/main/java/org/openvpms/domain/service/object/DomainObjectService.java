/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.object;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

/**
 * Service for adapting model objects to domain objects.
 *
 * @author Tim Anderson
 */
public interface DomainObjectService {

    /**
     * Creates a domain object from a model object.
     *
     * @param object the model object
     * @param type   the domain object type
     * @return a new domain object
     * @throws OpenVPMSException for any error
     */
    <R, T extends IMObject> R create(T object, Class<R> type);

    /**
     * Retrieves a domain object given its reference.
     *
     * @param reference the object reference
     * @param type      the domain object type
     * @return the corresponding domain object, or {@code null} if the object is not found
     */
    <R> R get(Reference reference, Class<R> type);

    /**
     * Retrieves a domain object given its reference.
     *
     * @param reference the object reference
     * @param type      the domain object type
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    <R> R get(Reference reference, Class<R> type, boolean active);

    /**
     * Retrieves a domain object given its archetype and identifier.
     *
     * @param archetype the object archetype
     * @param id        the object identifier
     * @param type      the domain object type
     * @return the corresponding domain object, or {@code null} if the object is not found
     */
    <R> R get(String archetype, long id, Class<R> type);

    /**
     * Retrieves a domain object given its archetype and identifier.
     *
     * @param archetype the object archetype
     * @param id        the object identifier
     * @param type      the domain object type
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding domain object, or {@code null} if the object is not found
     */
    <R> R get(String archetype, long id, Class<R> type, boolean active);

}
