/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;
import org.openvpms.domain.practice.Location;

import java.util.List;

/**
 * Laboratory device.
 *
 * @author Tim Anderson
 */
public interface Device extends Entity {

    /**
     * The default laboratory device archetype.
     */
    String ARCHETYPE = "entity.laboratoryDevice";

    /**
     * The laboratory device archetypes.
     */
    String ARCHETYPES = "entity.laboratoryDevice*";

    /**
     * Returns the laboratory assigned device identifier.
     * <p/>
     * This is short for: {@code getDeviceIdentity().getIdentity()}
     *
     * @return the device identifier
     */
    String getDeviceId();

    /**
     * Returns the laboratory assigned identifier for this device.
     *
     * @return the device identifier
     */
    Identity getDeviceIdentity();

    /**
     * Returns the laboratory that manages tests for this device.
     *
     * @return the laboratory
     */
    Laboratory getLaboratory();

    /**
     * Returns the practice locations that this device may be used at.
     *
     * @return the locations, or an empty list if the device is available at all locations
     */
    List<Location> getLocations();

}
