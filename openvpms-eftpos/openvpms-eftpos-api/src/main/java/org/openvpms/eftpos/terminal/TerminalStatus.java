/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.terminal;

import org.openvpms.eftpos.service.EFTPOSService;

/**
 * Terminal status.
 *
 * @author Tim Anderson
 */
public class TerminalStatus {

    public static class Builder {

        private boolean registered;

        private boolean canRegister;

        private boolean registrationRequired;

        private String error;

        /**
         * Indicates that the terminal isn't registered and may not be used in EFTPOS transactions.
         *
         * @return this
         */
        public Builder unregistered() {
            registered = false;
            return this;
        }

        /**
         * Indicates that the terminal is registered and may be used in EFTPOS transactions.
         *
         * @return this
         */
        public Builder registered() {
            registered = true;
            return this;
        }

        /**
         * Indicates that the terminal can be registered via {@link EFTPOSService#register(Terminal)}.
         *
         * @return this
         */
        public Builder canRegister() {
            canRegister = true;
            return this;
        }

        /**
         * Indicates that the terminal must be registered prior to use.
         *
         * @return this
         */
        public Builder registrationRequired() {
            registrationRequired = true;
            return this;
        }

        /**
         * Indicates an error in the terminal.
         *
         * @param message the error message
         * @return this
         */
        public Builder error(String message) {
            if (message == null) {
                throw new IllegalArgumentException("Argument 'message' is required");
            }
            this.error = message;
            return this;
        }

        /**
         * Builds the {@link TerminalStatus}.
         *
         * @return the status
         */
        public TerminalStatus build() {
            return new TerminalStatus(registered, registrationRequired, canRegister, error != null, error);
        }
    }

    /**
     * Determines if the terminal is registered.
     */
    private final boolean registered;

    /**
     * Determines if registration is required.
     */
    private final boolean registrationRequired;

    /**
     * Determines if the terminal can be registered via the service.
     */
    private final boolean canRegister;

    /**
     * Indicates that there is an error with the terminal.
     */
    private final boolean error;

    /**
     * The message.
     */
    private final String message;

    /**
     * Construct a {@link TerminalStatus}.
     *
     * @param registered           determines if the terminal is registered
     * @param registrationRequired determines if the terminal needs to be registered before it can be used
     * @param canRegister          determines if the terminal can be registered via
     *                             {@link EFTPOSService#register(Terminal)}
     * @param error                determines if there is an error
     * @param message              the message
     */
    private TerminalStatus(boolean registered, boolean registrationRequired, boolean canRegister, boolean error,
                           String message) {
        this.registered = registered;
        this.registrationRequired = registrationRequired;
        this.canRegister = canRegister;
        this.error = error;
        this.message = message;
    }

    /**
     * Determines if the terminal is registered.
     *
     * @return if the terminal is registered
     */
    public boolean isRegistered() {
        return registered;
    }

    /**
     * Determines if the terminal needs registration.
     *
     * @return {@code true} if the terminal needs to be registered before it is used
     */
    public boolean isRegistrationRequired() {
        return registrationRequired;
    }

    /**
     * Determines if the terminal can be registered via {@link EFTPOSService#register(Terminal)}.
     *
     * @return {@code true} if the terminal can be registered via the service
     */
    public boolean canRegister() {
        return canRegister;
    }

    /**
     * Determines if there is an error with the terminal.
     * <p/>
     * If so, the {@link #getMessage() message} will be non-null.
     *
     * @return {@code true} if there is an error with the terminal.
     */
    public boolean isError() {
        return error;
    }

    /**
     * Returns the terminal status message.
     * <p/>
     * Only guaranteed to be set when {@link #isError()} is {@code true}.
     *
     * @return the message. May be {@code null}
     */
    public String getMessage() {
        return message;
    }

    /**
     * Creates a status indicating that the terminal is registered.
     *
     * @return a new status
     */
    public static TerminalStatus registered() {
        return newStatus().registered().build();
    }

    /**
     * Creates a status indicating that the terminal is unregistered and requires registration before use.
     * <p/>
     * The terminal can be registered via {@link EFTPOSService#register(Terminal)}.
     *
     * @return a new status
     */
    public static TerminalStatus registrationRequired() {
        return newStatus()
                .unregistered()
                .registrationRequired()
                .canRegister()
                .build();
    }

    /**
     * Creates a status indicating that there is an error with the terminal.
     *
     * @param message the error message
     * @return a new status
     */
    public static TerminalStatus error(String message) {
        return newStatus().error(message).build();
    }

    /**
     * Returns a builder to create a new {@link TerminalStatus}.
     *
     * @return a builder
     */
    public static Builder newStatus() {
        return new Builder();
    }
}
