/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

import org.openvpms.eftpos.exception.EFTPOSException;

import java.util.List;

/**
 * EFTPOS transaction display, managed by OpenVPMS.
 * <p/>
 * This is periodically polled for updates.
 *
 * @author Tim Anderson
 */
public interface ManagedTransactionDisplay extends TransactionDisplay {

    /**
     * Returns the prompt.
     * <p/>
     * This displays a message to the user, with options they may select.
     * <p/>
     * This can be used to verify a signature, or cancel the transaction.
     *
     * @return the prompt, or {@code null} if no prompt is required
     */
    Prompt getPrompt();

    /**
     * Returns the EFTPOS terminal messages.
     * <p/>
     * This should return:
     * <ul>
     *  <li>all messages issued since a transaction was initiated or resumed, excluding that for the current prompt;
     *  with</li>
     *  <li>the oldest message at the beginning of the list, the newest, the end; and </li>
     *  <li>multi-line messages should be separated by a new line.</li>
     * </ul>
     *
     * @return the messages
     */
    List<String> getMessages();

    /**
     * Performs an update.
     * <p/>
     * This requests the latest status of the transaction, and updates it transaction, if required.
     *
     * @return {@code true} if the display needs to refresh, otherwise {@code false}
     * @throws EFTPOSException for any error
     */
    boolean update();

}
