/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.terminal;

import org.openvpms.component.model.object.Reference;

import java.util.List;

/**
 * EFTPOS terminal query service.
 *
 * @author Tim Anderson
 */
public interface Terminals {

    /**
     * Returns a terminal given its reference.
     *
     * @param reference the terminal reference
     * @return the corresponding terminal or {@code null} if none is found. The returned terminal may be inactive
     */
    Terminal getTerminal(Reference reference);

    /**
     * Returns all terminals with the specified  archetype.
     *
     * @param archetype  the terminal archetype. Must be an <em>entity.EFTPOSTerminal*</em>
     * @param activeOnly if {@code true}, only return active terminals
     * @return the terminals
     */
    List<Terminal> getTerminals(String archetype, boolean activeOnly);

    /**
     * Notifies that the status of an EFTPOS terminal has been updated.
     * <p/>
     * This should be invoked whenever a terminal is registered, or when registration fails.
     *
     * @param terminal the terminal
     * @param status   the terminal status
     */
    void terminalUpdated(Terminal terminal, TerminalStatus status);

}
