/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

/**
 * Transaction status.
 *
 * @author Tim Anderson
 */
public class TransactionStatus {

    public enum Status {
        COMPLETED,   // the transaction was successfully completed
        ERROR        // an error occurred during registration
    }

    /**
     * The status.
     */
    private final Status status;

    /**
     * The message
     */
    private final String message;

    /**
     * Construct a {@link TransactionStatus}.
     *
     * @param status the status
     */
    private TransactionStatus(Status status) {
        this(status, null);
    }

    /**
     * Construct a {@link TransactionStatus}.
     *
     * @param status  the status
     * @param message the message
     */
    private TransactionStatus(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * Returns the status.
     *
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null} if the status is {@code VALID}
     */
    public String getMessage() {
        return message;
    }

    /**
     * Creates a status indicating that the terminal was registered.
     *
     * @return a new status
     */
    public static TransactionStatus completed() {
        return new TransactionStatus(Status.COMPLETED);
    }

    /**
     * Creates a status indicating that registration failed.
     *
     * @param message the error message
     * @return a new status
     */
    public static TransactionStatus error(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Argument 'message' must be provided");
        }
        return new TransactionStatus(Status.ERROR, message);
    }
}
