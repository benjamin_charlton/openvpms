/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

import org.openvpms.eftpos.exception.EFTPOSException;

import java.util.List;

/**
 * EFTPOS terminal prompt.
 *
 * @author Tim Anderson
 */
public interface Prompt {

    /**
     * Prompt option.
     */
    interface Option {

        /**
         * Returns the option text for display.
         *
         * @return the option text
         */
        String getText();

    }

    /**
     * Returns the prompt message.
     *
     * @return the prompt message
     */
    String getMessage();

    /**
     * Returns the prompt options.
     *
     * @return the prompt options
     */
    List<Option> getOptions();

    /**
     * Sends the selected option.
     *
     * @param option the option
     * @throws EFTPOSException for any EFTPOS error
     */
    void send(Option option);
}
