/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.transaction;

import org.openvpms.component.model.party.Party;
import org.openvpms.domain.practice.Location;
import org.openvpms.eftpos.terminal.Terminal;

import java.math.BigDecimal;
import java.util.List;

/**
 * EFT transaction.
 *
 * @author Tim Anderson
 */
public interface Transaction {

    enum Status {
        PENDING,      // transaction hasn't been submitted
        IN_PROGRESS,  // transaction has been submitted
        APPROVED,     // transaction has been approved
        DECLINED,     // transaction has been declined
        ERROR,        // transaction failed with an error
        NO_TERMINAL   // transaction entered manually via terminal as the provider was offline
    }

    /**
     * Returns the OpenVPMS identifier for the transaction.
     *
     * @return the identifier
     */
    long getId();

    /**
     * Returns the OpenVPMS identifier for the parent payment or refund.
     *
     * @return the parent payment or refund id
     */
    long getParentId();

    /**
     * Returns the transaction identifier, issued by the provider.
     *
     * @return the transaction identifier, or {@code null} if none has been issued
     */
    String getTransactionId();

    /**
     * Returns the transaction status.
     *
     * @return the transaction status
     */
    Status getStatus();

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    String getMessage();

    /**
     * Determines if the transaction is complete.
     * <p/>
     * A transaction is complete if it has {@link Status#APPROVED}, {@link Status#DECLINED}, {@link Status#ERROR}
     * or {@link Status#NO_TERMINAL} status.
     *
     * @return {@code true} if the transaction is complete, otherwise {@code false}
     */
    boolean isComplete();

    /**
     * Returns the customer the transaction is for.
     *
     * @return the customer
     */
    Party getCustomer();

    /**
     * Returns the terminal to use.
     *
     * @return the terminal, or {@code null} if the status is {@link Status#NO_TERMINAL}
     */
    Terminal getTerminal();

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    Location getLocation();

    /**
     * Returns the transaction amount.
     *
     * @return the transaction amount
     */
    BigDecimal getAmount();

    /**
     * Returns the transaction currency code.
     * <p/>
     * This is a 3 character code as specified by ISO 4217.
     *
     * @return the transaction currency code
     */
    String getCurrencyCode();

    /**
     * Returns the card type.
     *
     * @return the card type. May be {@code null}
     */
    String getCardType();

    /**
     * Returns the authorisation code.
     *
     * @return the authorisation code. May be {@code null}
     */
    String getAuthorisationCode();

    /**
     * Returns the response code.
     *
     * @return the response code. May be {@code null}
     */
    String getResponseCode();

    /**
     * Returns the response text.
     *
     * @return the response text. May be {@code null}
     */
    String getResponse();

    /**
     * Returns the masked card number.
     *
     * @return the masked card number. May be {@code null}
     */
    String getMaskedCardNumber();

    /**
     * Returns the retrieval reference number (RRN).
     *
     * @return the retrieval reference number
     */
    String getRetrievalReferenceNumber();

    /**
     * Returns the system trace audit number (STAN).
     *
     * @return the system trace audit number. May be {@code null}
     */
    String getSystemTraceAuditNumber();

    /**
     * Returns the merchant transaction receipts.
     *
     * @return the merchant transaction receipts
     */
    List<Receipt> getMerchantReceipts();

    /**
     * Returns the customer transaction receipts.
     *
     * @return the customer transaction receipts
     */
    List<Receipt> getCustomerReceipts();

    /**
     * Returns an updater to change the state of the transaction.
     *
     * @return the updater
     */
    TransactionUpdater state();

}
