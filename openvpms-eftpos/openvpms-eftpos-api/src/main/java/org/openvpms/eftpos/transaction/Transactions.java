/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.transaction;

/**
 * Transaction service.
 *
 * @author Tim Anderson
 */
public interface Transactions {

    /**
     * Returns a payment given its OpenVPMS identifier.
     *
     * @param id the payment identifier
     * @return the corresponding payment or {@code null} if none is found
     */
    Payment getPayment(long id);

    /**
     * Returns a refund given its OpenVPMS identifier.
     *
     * @param id the refund identifier
     * @return the corresponding refund or {@code null} if none is found
     */
    Refund getRefund(long id);

    /**
     * Returns a payment, given its provider id.
     * <p>
     * A payment can have a single identifier issued by a provider. To avoid duplicates, each EFTPOS provider must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.EFTPOSTransaction</em> prefix.
     * @param id        the payment identifier
     * @return the corresponding transaction or {@code null} if none is found
     */
    Payment getPayment(String archetype, String id);

    /**
     * Returns a refund, given its provider id.
     * <p>
     * A refund can have a single identifier issued by a provider. To avoid duplicates, each EFTPOS provider must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.EFTPOSTransaction</em> prefix.
     * @param id        the refund identifier
     * @return the corresponding transaction or {@code null} if none is found
     */
    Refund getRefund(String archetype, String id);

    /**
     * Indicates that a merchant receipt needs to printed for signature verification purposes.
     * <p/>
     * This should be invoked when the terminal does not support printing.
     *
     * @param transaction the transaction the receipt is for
     * @param receipt     the receipt
     */
    void printMerchantReceipt(Transaction transaction, Receipt receipt);

    /**
     * Used to indicate that processing of a transaction has been completed.
     *
     * @param transaction the transaction
     */
    void completed(Transaction transaction);
}
