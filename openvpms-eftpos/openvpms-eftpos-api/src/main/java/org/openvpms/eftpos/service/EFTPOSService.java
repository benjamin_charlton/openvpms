/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;


import org.openvpms.eftpos.exception.EFTPOSException;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Refund;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.Transaction.Status;

/**
 * EFTPOS service.
 *
 * @author Tim Anderson
 */
public interface EFTPOSService {

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     * @throws EFTPOSException for any error
     */
    String getName();

    /**
     * Returns the terminal archetype that this service supports.
     *
     * @return an <em>entity.EFTPOSTerminal*</em> archetype
     */
    String getTerminalArchetype();

    /**
     * Returns the status of a terminal.
     *
     * @param terminal the terminal
     * @return the terminal status
     * @throws EFTPOSException for any EFTPOS error
     */
    TerminalStatus getTerminalStatus(Terminal terminal);

    /**
     * Registers the terminal prior to use.
     * <p>
     * This will be invoked if {@link #getTerminalStatus(Terminal)} returns a status where
     * {@link TerminalStatus#canRegister() canRegister()} returns {@code true}.
     *
     * @param terminal the terminal
     * @return a registrar for the terminal or {@code null} if no further registration is required
     * @throws EFTPOSException for any EFTPOS error
     */
    TerminalRegistrar register(Terminal terminal);

    /**
     * Determines if a terminal is available.
     *
     * @param terminal the terminal
     * @return {@code true} if the terminal is available
     * @throws EFTPOSException for any EFTPOS error
     */
    boolean isAvailable(Terminal terminal);

    /**
     * Make a payment.
     *
     * @param payment the payment
     * @return the transaction display
     * @throws EFTPOSException for any error
     */
    TransactionDisplay pay(Payment payment);

    /**
     * Attempt to resume a payment after an error.
     * <p/>
     * If a transaction cannot be resumed, or has subsequently completed, the status must be updated to one
     * of {@link Status#APPROVED APPROVED}, {@link Status#DECLINED DECLINED} or {@link Status#ERROR ERROR}.
     *
     * @param payment the payment to resume
     * @return the transaction display, or {@code null} if the transaction cannot be resumed
     * @throws EFTPOSException for any error
     */
    TransactionDisplay resume(Payment payment);

    /**
     * Make a refund.
     *
     * @param refund the refund
     * @return the transaction display
     * @throws EFTPOSException for any error
     */
    TransactionDisplay refund(Refund refund);

    /**
     * Attempt to resume a refund after an error.
     * <p/>
     * If a transaction cannot be resumed, or has subsequently completed, the status must be updated to one
     * of {@link Status#APPROVED APPROVED}, {@link Status#DECLINED DECLINED} or {@link Status#ERROR ERROR}.
     *
     * @param refund the refund to resume
     * @return the transaction display, or {@code null} if the transaction cannot be resumed
     * @throws EFTPOSException for any error
     */
    TransactionDisplay resume(Refund refund);

    /**
     * Returns the last receipts issued by a terminal for a transaction.
     *
     * @param transaction the transaction
     * @return the most recent receipts, or {@code null} if there is none or the operation is not supported
     * @throws EFTPOSException for any error
     */
    Receipts getLastReceipts(Transaction transaction);

}
