/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.sample.service;

import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.eftpos.exception.EFTPOSException;
import org.openvpms.eftpos.sample.internal.service.TransactionDisplayImpl;
import org.openvpms.eftpos.service.EFTPOSService;
import org.openvpms.eftpos.service.Receipts;
import org.openvpms.eftpos.service.TerminalRegistrar;
import org.openvpms.eftpos.service.TransactionDisplay;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Refund;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.Transaction.Status;
import org.openvpms.plugin.service.archetype.ArchetypeInstaller;

import java.util.UUID;

/**
 * Sample EFTPOS service.
 *
 * @author Tim Anderson
 */
public class SampleEFTPOSService implements EFTPOSService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Transaction id archetype.
     */
    private static final String TRANSACTION_ID = "actIdentity.EFTPOSTransactionSample";

    /**
     * Constructs an {@link SampleEFTPOSService}.
     *
     * @param service   the archetype service
     * @param installer the archetype installer
     */
    public SampleEFTPOSService(ArchetypeService service, ArchetypeInstaller installer) {
        this.service = service;
        installer.install(getClass(), "/org/openvpms/eftpos/sample/internal/entity.EFTPOSTerminalSample.adl",
                          "/org/openvpms/eftpos/sample/internal/" + TRANSACTION_ID + ".adl");
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Sample EFTPOS Service";
    }

    /**
     * Returns the terminal configuration archetype that this service supports.
     * <p>
     * This must be an entity archetype.
     *
     * @return the terminal configuration archetype
     */
    @Override
    public String getTerminalArchetype() {
        return "entity.EFTPOSTerminalSample";
    }

    /**
     * Returns the status of a terminal.
     *
     * @param terminal the terminal
     * @return the terminal status
     */
    @Override
    public TerminalStatus getTerminalStatus(Terminal terminal) {
        return TerminalStatus.registered();
    }

    /**
     * Registers the terminal prior to use.
     *
     * @param terminal the terminal
     * @return a registrar for the terminal
     */
    @Override
    public TerminalRegistrar register(Terminal terminal) {
        throw new UnsupportedOperationException();
    }

    /**
     * Determines if a terminal is available.
     *
     * @param terminal the terminal
     * @return {@code true} if the terminal is available
     */
    @Override
    public boolean isAvailable(Terminal terminal) {
        return true;
    }

    /**
     * Make a payment.
     *
     * @param payment the payment
     * @return the transaction display
     * @throws EFTPOSException for any error
     */
    @Override
    public TransactionDisplay pay(Payment payment) {
        payment.state()
                .transactionId(TRANSACTION_ID, UUID.randomUUID().toString())
                .status(Status.IN_PROGRESS)
                .update();
        return new TransactionDisplayImpl(payment, service);
    }

    /**
     * Attempt to resume a payment after an error.
     * <p/>
     * If a transaction cannot be resumed, or has subsequently completed, the status must be updated to one
     * of {@link Status#APPROVED APPROVED}, {@link Status#DECLINED DECLINED} or {@link Status#ERROR ERROR}.
     *
     * @param payment the payment to resume
     * @return the transaction display, or {@code null} if the transaction cannot be resumed
     * @throws EFTPOSException for any error
     */
    @Override
    public TransactionDisplay resume(Payment payment) {
        return new TransactionDisplayImpl(payment, service);
    }

    /**
     * Make a refund.
     *
     * @param refund the refund
     * @return the transaction display
     * @throws EFTPOSException for any error
     */
    @Override
    public TransactionDisplay refund(Refund refund) {
        refund.state()
                .transactionId(TRANSACTION_ID, UUID.randomUUID().toString())
                .status(Status.IN_PROGRESS)
                .update();
        return new TransactionDisplayImpl(refund, service);
    }

    /**
     * Attempt to resume a refund after an error.
     * <p/>
     * If a transaction cannot be resumed, or has subsequently completed, the status must be updated to one
     * of {@link Status#APPROVED APPROVED}, {@link Status#DECLINED DECLINED} or {@link Status#ERROR ERROR}.
     *
     * @param refund the refund to resume
     * @return the transaction display, or {@code null} if the transaction cannot be resumed
     * @throws EFTPOSException for any error
     */
    @Override
    public TransactionDisplay resume(Refund refund) {
        return new TransactionDisplayImpl(refund, service);
    }

    /**
     * Returns the last receipts issued by a terminal for a transaction.
     *
     * @param transaction the transaction
     * @return the most recent receipts, or {@code null} if there is none or the operation is not supported
     * @throws EFTPOSException for any error
     */
    @Override
    public Receipts getLastReceipts(Transaction transaction) {
        return null;
    }
}
