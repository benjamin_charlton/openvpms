/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainServiceImpl;
import org.openvpms.eftpos.transaction.Receipt;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.Transaction.Status;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Tests the {@link TransactionImpl} class.
 *
 * @author Tim Anderson
 */
public class TransactionImplTestCase extends ArchetypeServiceTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * Tests the transaction.
     */
    @Test
    public void testTransaction() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Party location = practiceFactory.createLocation();

        Act payment = accountFactory.newEFTPOSPayment()
                .customer(customer)
                .terminal(terminal)
                .location(location)
                .build();
        checkTransaction(payment);

        Act refund = accountFactory.newEFTPOSRefund()
                .customer(customer)
                .terminal(terminal)
                .location(location)
                .build();
        checkTransaction(refund);
    }

    /**
     * Verifies that receipts greater than 5000 characters stored correctly.
     */
    @Test
    public void testLongReceipts() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Party location = practiceFactory.createLocation();
        Act act = accountFactory.newEFTPOSPayment()
                .customer(customer)
                .terminal(terminal)
                .location(location)
                .build();
        Transaction txn = getTransaction(act);

        String receipt1 = StringUtils.repeat("1234567890", 6500);
        assertEquals(65000, receipt1.length());
        String receipt2 = StringUtils.reverse(receipt1);

        txn.state().addMerchantReceipt(receipt1, true)
                .addCustomerReceipt(receipt2)
                .update();

        // reload
        txn = getTransaction(get(act));

        List<Receipt> merchantReceipts = txn.getMerchantReceipts();
        assertEquals(1, merchantReceipts.size());
        List<Receipt> customerReceipts = txn.getCustomerReceipts();
        assertEquals(1, customerReceipts.size());

        checkReceipt(merchantReceipts.get(0), receipt1, true);
        checkReceipt(customerReceipts.get(0), receipt2, false);
    }

    /**
     * Verifies receipts are returned in the order they were added.
     */
    @Test
    public void testReceiptOrder() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Party location = practiceFactory.createLocation();
        Act act = accountFactory.newEFTPOSPayment()
                .customer(customer)
                .terminal(terminal)
                .location(location)
                .build();
        Transaction txn = getTransaction(act);
        txn.state().addMerchantReceipt("MERCHANT 1", false)
                .update();
        txn.state().addMerchantReceipt("MERCHANT 2", true)
                .update();
        txn.state().addCustomerReceipt("CUSTOMER 1")
                .addCustomerReceipt("CUSTOMER 2")
                .update();

        // reload
        txn = getTransaction(get(act));

        List<Receipt> merchantReceipts = txn.getMerchantReceipts();
        assertEquals(2, merchantReceipts.size());
        List<Receipt> customerReceipts = txn.getCustomerReceipts();
        assertEquals(2, customerReceipts.size());

        checkReceipt(merchantReceipts.get(0), "MERCHANT 1", false);
        checkReceipt(merchantReceipts.get(1), "MERCHANT 2", true);
        checkReceipt(customerReceipts.get(0), "CUSTOMER 1", false);
        checkReceipt(customerReceipts.get(1), "CUSTOMER 2", false);
    }

    /**
     * Tests transaction population.
     *
     * @param act the transaction
     */
    private void checkTransaction(Act act) {
        Transaction txn = getTransaction(act);
        assertEquals(Status.PENDING, txn.getStatus());
        txn.state().addMerchantReceipt("MERCHANT COPY", true).update();
        txn.state().status(Status.APPROVED)
                .cardType("VISA")
                .authorisationCode("498023")
                .response("00", "Approved")
                .maskedCardNumber("XXXXXXX6662")
                .retrievalReferenceNumber("123456")
                .systemTraceAuditNumber("98765")
                .addCustomerReceipt("CUSTOMER COPY")
                .update();

        // reload
        txn = getTransaction(get(act));

        assertEquals(Status.APPROVED, txn.getStatus());
        assertEquals("AUD", txn.getCurrencyCode());
        assertEquals("VISA", txn.getCardType());
        assertEquals("498023", txn.getAuthorisationCode());
        assertEquals("00", txn.getResponseCode());
        assertEquals("Approved", txn.getResponse());
        assertEquals("XXXXXXX6662", txn.getMaskedCardNumber());
        assertEquals("123456", txn.getRetrievalReferenceNumber());
        assertEquals("98765", txn.getSystemTraceAuditNumber());
        List<Receipt> merchantReceipts = txn.getMerchantReceipts();
        assertEquals(1, merchantReceipts.size());
        List<Receipt> customerReceipts = txn.getCustomerReceipts();
        assertEquals(1, customerReceipts.size());

        checkReceipt(merchantReceipts.get(0), "MERCHANT COPY", true);
        checkReceipt(customerReceipts.get(0), "CUSTOMER COPY", false);
    }

    /**
     * Creates an {@link Transaction} from an act.
     *
     * @param act the ect
     * @return the corresponding transaction
     */
    private Transaction getTransaction(Act act) {
        DomainServiceImpl domainService = new DomainServiceImpl(getArchetypeService());
        return new TransactionImpl(act, getArchetypeService(), domainService, practiceService);
    }

    /**
     * Verifies a receipt matches that expected
     *
     * @param receipt           the receipt
     * @param text              the expected receipt text
     * @param signatureRequired the expected signature required flag
     */
    private void checkReceipt(Receipt receipt, String text, boolean signatureRequired) {
        assertEquals(text, receipt.getReceipt());
        assertEquals(signatureRequired, receipt.isSignatureRequired());
    }

}
