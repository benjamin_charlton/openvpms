/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.i18n;


import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.eftpos.transaction.Transaction;

/**
 * Messages reported by the POS API.
 *
 * @author Tim Anderson
 */
public class EFTPOSMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("EFTPOS", EFTPOSMessages.class.getName());

    /**
     * Default constructor.
     */
    private EFTPOSMessages() {
        // no-op
    }

    /**
     * Message to indicate that the EFTPOS service for a terminal is unavailable.
     *
     * @param terminalName the terminal name
     * @return a new message
     */
    public static Message serviceUnavailable(String terminalName) {
        return messages.create(1, terminalName);
    }

    /**
     * Message to indicate that a transaction status cannot be updated.
     *
     * @param current the current status
     * @param other   the requested status
     * @return a new message
     */
    public static Message cannotChangeStatus(Transaction.Status current, Transaction.Status other) {
        return messages.create(100, current, other);
    }

    /**
     * Message to indicate that the transaction identifier archetype cannot be changed.
     *
     * @param current the current archetype
     * @param other   the requested archetype
     * @return a new message
     */
    public static Message differentTxnIdentifierArchetype(String current, String other) {
        return messages.create(101, current, other);
    }

    /**
     * Returns a message to indicate that {@link Transaction.Status#NO_TERMINAL} transactions cannot be updated.
     *
     * @return a new message
     */
    public static Message cannotChangeNoTerminalTransaction() {
        return messages.create(102);
    }
}
