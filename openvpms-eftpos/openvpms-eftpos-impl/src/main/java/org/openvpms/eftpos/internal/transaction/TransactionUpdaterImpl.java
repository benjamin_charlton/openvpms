/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.eftpos.exception.EFTPOSException;
import org.openvpms.eftpos.internal.i18n.EFTPOSMessages;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.TransactionUpdater;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.openvpms.eftpos.internal.transaction.TransactionImpl.AUTHORISATION_CODE;
import static org.openvpms.eftpos.internal.transaction.TransactionImpl.RETRIEVAL_REFERENCE_NUMBER;
import static org.openvpms.eftpos.internal.transaction.TransactionImpl.SYSTEM_TRACE_AUDIT_NUMBER;
import static org.openvpms.eftpos.internal.transaction.TransactionImpl.TRANSACTION_ID;

/**
 * Default implementation of {@link TransactionUpdater}.
 *
 * @author Tim Anderson
 */
public class TransactionUpdaterImpl implements TransactionUpdater {

    /***
     * The transaction bean.
     */
    private final IMObjectBean bean;

    /**
     * Determines if the terminal has printed receipts.
     */
    private final boolean printed;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction status.
     */
    private Transaction.Status status;

    /**
     * The status message.
     */
    private MutableObject<String> message;

    /**
     * The transaction id archetype.
     */
    private String transactionIdArchetype;

    /**
     * The transaction id.
     */
    private String transactionId;

    /**
     * The card type.
     */
    private MutableObject<String> cardType;

    /**
     * The authorisation code.
     */
    private MutableObject<String> authorisationCode;

    /**
     * The response code.
     */
    private MutableObject<String> responseCode;

    /**
     * The response text.
     */
    private MutableObject<String> response;

    /**
     * The masked card number.
     */
    private MutableObject<String> maskedCardNumber;

    /**
     * The retrieval reference number.
     */
    private MutableObject<String> retrievalReferenceNumber;

    /**
     * The system trace audit number.
     */
    private MutableObject<String> systemTraceAuditNumber;

    /**
     * The receipts.
     */
    private List<ReceiptState> receipts = new ArrayList<>();

    /**
     * Constructs a {@link TransactionUpdaterImpl}.
     *
     * @param transaction the transaction
     * @param bean        the transaction bean
     * @param service     the archetype service
     */
    public TransactionUpdaterImpl(Transaction transaction, IMObjectBean bean, ArchetypeService service) {
        this.bean = bean;
        this.service = service;
        printed = transaction.getTerminal().isReceiptPrinter();
    }

    /**
     * Sets the  status.
     *
     * @param status the status
     * @return this
     */
    @Override
    public TransactionUpdater status(Transaction.Status status) {
        return status(status, null);
    }

    /**
     * Sets the status and message.
     *
     * @param status  the status
     * @param message the status message. May be {@code null}
     * @return this
     */
    @Override
    public TransactionUpdater status(Transaction.Status status, String message) {
        this.status = status;
        this.message = new MutableObject<>(message);
        return this;
    }

    /**
     * Sets the transaction identifier, issued by the provider.
     * <p>
     * A transaction can have a single identifier issued by a provider. To avoid duplicates, each EFTPOS service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.EFTPOSTransaction</em> prefix.
     * @param id        the transaction identifier
     * @return this
     */
    @Override
    public TransactionUpdater transactionId(String archetype, String id) {
        transactionIdArchetype = archetype;
        transactionId = id;
        return this;
    }

    /**
     * Sets the card type.
     *
     * @param cardType the card type
     * @return this
     */
    @Override
    public TransactionUpdater cardType(String cardType) {
        this.cardType = new MutableObject<>(cardType);
        return this;
    }

    /**
     * Sets the authorisation code.
     *
     * @param authorisationCode the authorisation code
     * @return this
     */
    @Override
    public TransactionUpdater authorisationCode(String authorisationCode) {
        this.authorisationCode = new MutableObject<>(authorisationCode);
        return this;
    }

    /**
     * Sets the response code and message.
     * <p/>
     * The code is issued by the provider and indicates the reason for the success or failure of a transaction.
     * <p/>
     * The text is a human readable interpretation of the code.
     *
     * @param code the response code. May be {@code null}
     * @param text the response text. May be {@code null}
     * @return this
     */
    @Override
    public TransactionUpdater response(String code, String text) {
        this.responseCode = new MutableObject<>(code);
        this.response = new MutableObject<>(text);
        return this;
    }

    /**
     * Sets the masked card number.
     * <p/>
     * This is the card number with most digits elided for security.
     * <p/>
     * Also known as the elided PAN (primary account number).
     *
     * @param maskedCardNumber the masked card number
     * @return this
     */
    @Override
    public TransactionUpdater maskedCardNumber(String maskedCardNumber) {
        this.maskedCardNumber = new MutableObject<>(maskedCardNumber);
        return this;
    }

    /**
     * Sets the retrieval reference number (RRN).
     *
     * @param retrievalReferenceNumber the retrieval reference number. May be {@code null}
     * @return this
     */
    @Override
    public TransactionUpdater retrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = new MutableObject<>(retrievalReferenceNumber);
        return this;
    }

    /**
     * Sets the system trace audit number (STAN).
     *
     * @param systemTraceAuditNumber the system trace audit number. May be {@code null}
     * @return this
     */
    @Override
    public TransactionUpdater systemTraceAuditNumber(String systemTraceAuditNumber) {
        this.systemTraceAuditNumber = new MutableObject<>(systemTraceAuditNumber);
        return this;
    }

    /**
     * Adds a merchant receipt.
     *
     * @param receipt           the merchant receipt. May be {@code null}
     * @param signatureRequired if {@code true} a signature is required
     * @return this
     */
    @Override
    public TransactionUpdater addMerchantReceipt(String receipt, boolean signatureRequired) {
        if (!StringUtils.isEmpty(receipt)) {
            receipts.add(new ReceiptState(receipt, false, signatureRequired));
        }
        return this;
    }

    /**
     * Adds a customer receipt.
     *
     * @param receipt the customer receipt
     * @return this
     */
    @Override
    public TransactionUpdater addCustomerReceipt(String receipt) {
        if (receipt != null) {
            receipts.add(new ReceiptState(receipt, true, false));
        }
        return this;
    }

    /**
     * Commits any changes.
     * <p/>
     * This updates the transaction and saves it, in a single transaction.
     */
    @Override
    public void update() {
        Transaction.Status current = getStatus();
        if (current == Transaction.Status.NO_TERMINAL) {
            throw new EFTPOSException(EFTPOSMessages.cannotChangeNoTerminalTransaction());
        }
        List<IMObject> toSave = new ArrayList<>();
        boolean updated = false;
        if (status != null) {
            if (status != current && (current == Transaction.Status.APPROVED
                                      || current == Transaction.Status.DECLINED
                                      || current == Transaction.Status.ERROR)) {
                throw new EFTPOSException(EFTPOSMessages.cannotChangeStatus(current, status));
            }
            bean.setValue("status", status.toString());
            updated = true;
        }
        updated |= updateIfChanged("message", message);
        if (transactionId != null) {
            updated = updateTransactionId();
        }
        updated |= updateIfChanged("cardType", cardType);
        updated |= updateIdentity(authorisationCode, AUTHORISATION_CODE, EFTPOSArchetypes.AUTH_CODE_ID);
        updated |= updateIfChanged("responseCode", responseCode);
        updated |= updateIfChanged("response", response);
        updated |= updateIfChanged("maskedCardNumber", maskedCardNumber);
        updated |= updateIdentity(retrievalReferenceNumber, RETRIEVAL_REFERENCE_NUMBER, EFTPOSArchetypes.RRN_ID);
        updated |= updateIdentity(systemTraceAuditNumber, SYSTEM_TRACE_AUDIT_NUMBER, EFTPOSArchetypes.STAN_ID);

        if (!receipts.isEmpty()) {
            int sequence = getNextReceiptSequence();
            for (ReceiptState state : receipts) {
                addReceipt(state, sequence++, toSave);
                updated = true;
            }
        }
        reset();
        if (updated) {
            toSave.add(0, bean.getObject());
            service.save(toSave);
        }
    }

    /**
     * Updates the transaction identifier.
     *
     * @return {@code true} if the transaction identifier was changed
     */
    private boolean updateTransactionId() {
        boolean updated = false;
        ActIdentity identity = getIdentity(transactionIdArchetype, TRANSACTION_ID);
        if (!identity.isNew()) {
            if (!identity.isA(transactionIdArchetype)) {
                throw new EFTPOSException(EFTPOSMessages.differentTxnIdentifierArchetype(
                        identity.getArchetype(), transactionIdArchetype));
            }
        } else {
            updated = true;
        }
        updated |= updateIdentity(identity, transactionId);
        return updated;
    }

    /**
     * Updates an identity.
     *
     * @param id        the identity. If {@code null}, no update occurs. If the mutable object contains a null value,
     *                  the existing identity will be removed
     * @param node      the identity node
     * @param archetype the identity archetype
     * @return {@code true} if the identity was archetype
     */
    private boolean updateIdentity(MutableObject<String> id, String node, String archetype) {
        boolean updated = false;
        if (id != null) {
            String value = id.getValue();
            if (value == null) {
                ActIdentity identity = bean.getObject(node, ActIdentity.class);
                if (identity != null) {
                    bean.removeValue(node, identity);
                    updated = true;
                }
            } else {
                ActIdentity identity = getIdentity(archetype, node);
                if (identity.isNew()) {
                    updated = true;
                }
                updated |= updateIdentity(identity, value);
            }
        }
        return updated;
    }

    /**
     * Updates an identity.
     *
     * @param identity the identity to update
     * @param value the new value
     * @return {@code true} if the identity was updated
     */
    private boolean updateIdentity(ActIdentity identity, String value) {
        boolean updated = false;
        if (!Objects.equals(identity.getIdentity(), value)) {
            identity.setIdentity(value);
            updated = true;
        }
        return updated;
    }

    private ActIdentity getIdentity(String archetype, String node) {
        ActIdentity identity = bean.getObject(node, ActIdentity.class);
        if (identity == null) {
            identity = service.create(archetype, ActIdentity.class);
            bean.addValue(node, identity);
        }
        return identity;
    }

    /**
     * Returns the current transaction status.
     *
     * @return the current transaction status. May be {@code null}
     */
    private Transaction.Status getStatus() {
        Act act = (Act) bean.getObject();
        String result = act.getStatus();
        return result != null ? Transaction.Status.valueOf(result) : null;
    }

    /**
     * Clears the state.
     */
    private void reset() {
        status = null;
        transactionId = null;
        transactionIdArchetype = null;
        cardType = null;
        authorisationCode = null;
        responseCode = null;
        response = null;
        maskedCardNumber = null;
        retrievalReferenceNumber = null;
        systemTraceAuditNumber = null;
        receipts.clear();
    }

    /**
     * Updates a node if its value has changed.
     *
     * @param name  the node name
     * @param value the node value. May be {@code null}
     * @return {@code true} if the node was updated
     */
    private boolean updateIfChanged(String name, Object value) {
        boolean updated = false;
        if (!Objects.equals(value, bean.getValue(name))) {
            bean.setValue(name, value);
            updated = true;
        }
        return updated;
    }

    /**
     * Updates a node if its value has changed.
     *
     * @param name  the node name
     * @param value the node value. May be {@code null}
     * @return {@code true} if the node was updated
     */
    private boolean updateIfChanged(String name, MutableObject<String> value) {
        return (value != null) && updateIfChanged(name, value.getValue());
    }

    /**
     * Adds a receipt.
     *
     * @param receipt  the receipt
     * @param sequence the sequence number to ensure correct ordering on retrieval
     * @param toSave   collects the objects to save
     */
    private void addReceipt(ReceiptState receipt, int sequence, List<IMObject> toSave) {
        String archetype = receipt.customer ? EFTPOSArchetypes.CUSTOMER_RECEIPT : EFTPOSArchetypes.MERCHANT_RECEIPT;
        DocumentAct act = service.create(archetype, DocumentAct.class);
        act.setPrinted(printed);
        toSave.add(act);
        IMObjectBean receiptBean = service.getBean(act);

        if (receipt.text.length() > receiptBean.getMaxLength("receipt")) {
            TextDocumentHandler handler = new TextDocumentHandler(service);
            Document document = handler.create(receiptBean.getDisplayName(), receipt.text);
            act.setDocument(document.getObjectReference());
            toSave.add(document);
        } else {
            receiptBean.setValue("receipt", receipt.text);
        }
        if (!receipt.customer) {
            receiptBean.setValue("signatureRequired", receipt.signatureRequired);
        }
        ActRelationship relationship = (ActRelationship) bean.addTarget("receipts", act, "transaction");
        relationship.setSequence(sequence);
    }

    /**
     * Returns the next sequence number for a receipt.
     *
     * @return the next sequence number
     */
    private int getNextReceiptSequence() {
        int sequence = 0;
        for (ActRelationship relationship : bean.getValues("receipts", ActRelationship.class)) {
            if (relationship.getSequence() >= sequence) {
                sequence = relationship.getSequence() + 1;
            }
        }
        return sequence;
    }


    private static class ReceiptState {

        private final String text;

        private final boolean customer;

        private final boolean signatureRequired;

        public ReceiptState(String text, boolean customer, boolean signatureRequired) {
            this.text = text;
            this.customer = customer;
            this.signatureRequired = signatureRequired;
        }
    }
}
