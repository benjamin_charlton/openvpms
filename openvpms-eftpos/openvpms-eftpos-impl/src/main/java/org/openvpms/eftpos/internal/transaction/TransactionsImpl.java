/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.eftpos.internal.event.EventMonitor;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Receipt;
import org.openvpms.eftpos.transaction.Refund;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.Transactions;

/**
 * Default implementation of {@link Transactions}.
 *
 * @author Tim Anderson
 */
public class TransactionsImpl implements Transactions {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The event monitor.
     */
    private final EventMonitor monitor;

    /**
     * Constructs a {@link TransactionsImpl}.
     *
     * @param service         the archetype service
     * @param domainService   the domain service
     * @param practiceService the practice service
     * @param monitor         the event monitor
     */
    public TransactionsImpl(ArchetypeService service, DomainService domainService, PracticeService practiceService,
                            EventMonitor monitor) {
        this.service = service;
        this.domainService = domainService;
        this.practiceService = practiceService;
        this.monitor = monitor;
    }

    /**
     * Returns a payment given its OpenVPMS identifier.
     *
     * @param id the payment identifier
     * @return the corresponding transaction or {@code null} if none is found
     */
    @Override
    public Payment getPayment(long id) {
        Act act = service.get(EFTPOSArchetypes.PAYMENT, id, Act.class);
        return (act != null) ? new PaymentImpl(act, service, domainService, practiceService) : null;
    }

    /**
     * Returns a transaction, given its provider id.
     * <p>
     * A transaction can have a single identifier issued by a provider. To avoid duplicates, each EFTPOS provider must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.EFTPOSTransaction</em> prefix.
     * @param id        the transaction identifier
     * @return the corresponding transaction or {@code null} if none is found
     */
    @Override
    public Payment getPayment(String archetype, String id) {
        Act act = getTransaction(archetype, id, EFTPOSArchetypes.PAYMENT);
        return (act != null) ? new PaymentImpl(act, service, domainService, practiceService) : null;
    }

    /**
     * Returns a refund given its OpenVPMS identifier.
     *
     * @param id the refund identifier
     * @return the corresponding refund or {@code null} if none is found
     */
    @Override
    public Refund getRefund(long id) {
        Act act = service.get(EFTPOSArchetypes.REFUND, id, Act.class);
        return (act != null) ? new RefundImpl(act, service, domainService, practiceService) : null;
    }

    /**
     * Returns a refund, given its provider id.
     * <p>
     * A refund can have a single identifier issued by a provider. To avoid duplicates, each EFTPOS provider must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.EFTPOSTransaction</em> prefix.
     * @param id        the refund identifier
     * @return the corresponding transaction or {@code null} if none is found
     */
    @Override
    public Refund getRefund(String archetype, String id) {
        Act act = getTransaction(archetype, id, EFTPOSArchetypes.REFUND);
        return (act != null) ? new RefundImpl(act, service, domainService, practiceService) : null;
    }

    /**
     * Indicates that a merchant receipt needs to printed for signature verification purposes.
     * <p/>
     * This should be invoked when the terminal does not support printing.
     *
     * @param transaction the transaction the receipt is for
     * @param receipt     the receipt
     */
    @Override
    public void printMerchantReceipt(Transaction transaction, Receipt receipt) {
        monitor.printMerchantReceipt(transaction, receipt);
    }

    /**
     * Used to indicate that processing of a transaction has been completed.
     *
     * @param transaction the transaction
     */
    @Override
    public void completed(Transaction transaction) {
        monitor.completed(transaction);
    }

    private Act getTransaction(String archetype, String id, String type) {
        if (!TypeHelper.matches(archetype, EFTPOSArchetypes.TRANSACTION_IDS)) {
            throw new IllegalStateException("Invalid transaction id archetype: " + archetype);
        }
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, type);
        Join<Act, ActIdentity> identity = root.join("transactionId", archetype);
        identity.on(builder.equal(identity.get("identity"), id));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }
}
