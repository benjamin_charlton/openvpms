/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.event;

import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.eftpos.transaction.Receipt;
import org.openvpms.eftpos.transaction.Transaction;

/**
 * Monitors EFTPOS events.
 *
 * @author Tim Anderson
 */
public interface EventMonitor {

    /**
     * Notifies that the status of an EFTPOS terminal has been updated.
     *
     * @param terminal the terminal
     * @param status   the terminal status
     */
    void terminalUpdated(Terminal terminal, TerminalStatus status);

    /**
     * Indicates that a merchant receipt needs to printed for signature verification purposes.
     * <p/>
     * This should be invoked when the terminal does not support printing.
     *
     * @param transaction the transaction the receipt is for
     * @param receipt     the receipt
     */
    void printMerchantReceipt(Transaction transaction, Receipt receipt);

    /**
     * Used to indicate that processing of a transaction has been completed.
     *
     * @param transaction the transaction
     */
    void completed(Transaction transaction);

}
