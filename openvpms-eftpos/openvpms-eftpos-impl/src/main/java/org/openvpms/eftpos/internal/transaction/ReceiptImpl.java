/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.component.business.domain.im.act.DocumentActDecorator;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.eftpos.transaction.Receipt;

/**
 * Default implementation of {@link Receipt}.
 *
 * @author Tim Anderson
 */
public class ReceiptImpl extends DocumentActDecorator implements Receipt {

    /**
     * The receipt.
     */
    private final String receipt;

    /**
     * Determines if a signature is required.
     */
    private final boolean isSignaturedRequired;

    /**
     * Signature required node name. Only present on merchant receipts.
     */
    private static final String SIGNATURE_REQUIRED = "signatureRequired";

    /**
     * Constructs a {@link ReceiptImpl}.
     *
     * @param act     the receipt act
     * @param service the archetype service
     */
    public ReceiptImpl(DocumentAct act, ArchetypeService service) {
        super(act);
        String text = null;
        if (act.getDocument() != null) {
            Document document = service.get(act.getDocument(), Document.class);
            if (document != null) {
                // receipt too long too be stored in the receipt node
                TextDocumentHandler handler = new TextDocumentHandler(service);
                text = handler.toString(document);
            }
        }
        IMObjectBean bean = service.getBean(act);
        if (text == null) {
            text = bean.getString("receipt");
        }
        receipt = text;
        isSignaturedRequired = bean.hasNode(SIGNATURE_REQUIRED) && bean.getBoolean(SIGNATURE_REQUIRED);
    }

    /**
     * Returns the formatted receipt.
     *
     * @return the formatted receipt
     */
    @Override
    public String getReceipt() {
        return receipt;
    }

    /**
     * Determines if a signature is required.
     *
     * @return {@code true} if a signature is required
     */
    @Override
    public boolean isSignatureRequired() {
        return isSignaturedRequired;
    }
}
