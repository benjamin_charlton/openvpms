/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.eftpos.transaction.Refund;

/**
 * Default implementation of {@link Refund}.
 *
 * @author Tim Anderson
 */
public class RefundImpl extends TransactionImpl implements Refund {

    /**
     * Constructs a {@link RefundImpl}.
     *
     * @param act             the act
     * @param service         the archetype service
     * @param domainService   the domain service
     * @param practiceService the practice service
     */
    public RefundImpl(Act act, ArchetypeService service, DomainService domainService, PracticeService practiceService) {
        super(act, service, domainService, practiceService);
    }

}
