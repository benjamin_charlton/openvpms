/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.etl.load;

import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.LookupRelationship;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.lookup.ILookupService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.lookup.Lookup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.openvpms.etl.load.LoaderException.ErrorCode.ArchetypeNotFound;
import static org.openvpms.etl.load.LoaderException.ErrorCode.LookupNotFound;
import static org.openvpms.etl.load.LoaderException.ErrorCode.LookupRelationshipNotFound;
import static org.openvpms.etl.load.LoaderException.ErrorCode.LookupRelationshipTargetNotFound;


/**
 * Automatically generates lookups for lookup nodes.
 *
 * @author Tim Anderson
 */
class LookupHandler {

    /**
     * A mapping of lookup nodes to their corresponding lookup descriptors.
     */
    private final Map<NodeDescriptor, LookupDescriptor> lookups = new HashMap<>();

    /**
     * A mapping of target lookup nodes to their corresponding lookup
     * relationship descriptors.
     */
    private final Map<NodeDescriptor, LookupRelationshipDescriptor> relationships = new HashMap<>();

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Exception messages.
     */
    private final ExceptionHelper messages;

    /**
     * The lookup cache.
     */
    private final LookupCache cache;

    /**
     * The error listener, to notify of processing errors. May be {@code null}
     */
    private ErrorListener listener;

    /**
     * Lookup assertion descriptor name.
     */
    private static final String LOOKUP = "lookup";


    /**
     * Constructs a {@link LookupHandler}.
     *
     * @param mappings the mappings
     * @param service  the archetype service
     * @param lookups  the lookup service
     */
    public LookupHandler(Mappings mappings, IArchetypeService service, ILookupService lookups) {
        this.service = service;
        cache = new LookupCache(service, lookups);
        messages = new ExceptionHelper(service);

        // cache of lookups of type 'targetLookup'
        Map<NodeDescriptor, ArchetypeDescriptor> targets = new HashMap<>();

        // for each node in the mapping, determine which of those nodes are lookups.
        // Lookups of type 'lookup' are processed first.
        for (Mapping mapping : mappings.getMapping()) {
            String target = mapping.getTarget();
            Node node = NodeParser.parse(target);
            while (node != null) {
                processNode(node, targets);
                node = node.getChild();
            }
        }

        // process target lookups. This needs to be done last as the
        // source lookups need to have been registered first
        for (Map.Entry<NodeDescriptor, ArchetypeDescriptor> entry : targets.entrySet()) {
            processTargetLookupDescriptor(entry.getValue(), entry.getKey());
        }
    }

    /**
     * Determines if a node descriptor refers to a lookup that is automatically
     * generated.
     *
     * @param descriptor the node descriptor
     * @return {@code true} if the descriptor refers to an automatically generated lookup
     */
    public boolean isGeneratedLookup(NodeDescriptor descriptor) {
        return lookups.containsKey(descriptor);
    }

    /**
     * Adds a set of lookups for a row.
     *
     * @param descriptors the lookup descriptors
     */
    public void add(Map<NodeDescriptor, LookupData> descriptors) {
        for (Map.Entry<NodeDescriptor, LookupData> entry : descriptors.entrySet()) {
            NodeDescriptor descriptor = entry.getKey();
            LookupData data = entry.getValue();
            LookupDescriptor lookup = lookups.get(descriptor);
            lookup.add(data);
            LookupRelationshipDescriptor relationship = relationships.get(descriptor);
            if (relationship != null) {
                LookupData source = descriptors.get(relationship.getSource().getDescriptor());
                if (source != null) {
                    relationship.add(source.getCode(), data.getCode());
                }
            }
        }
    }

    /**
     * Generates a code for a lookup name.
     * This capitalises the lookup name, and replaces all non-alphanumeric
     * characters with underscores.
     *
     * @param name the lookup name
     * @return the lookup code
     */
    public String getCode(String name) {
        String result = null;
        if (name != null) {
            result = name.toUpperCase();
            result = result.replaceAll("[^A-Z0-9]+", "_");
        }
        return result;
    }

    /**
     * Sets a listener to be notified of errors.
     *
     * @param listener the listener. May be {@code null}
     */
    public void setErrorListener(ErrorListener listener) {
        this.listener = listener;
    }

    /**
     * Commits all generated lookups.
     *
     * @throws ArchetypeServiceException for any archetype service exception
     * @throws LoaderException           for any loader error
     */
    public void commit() {
        if (!lookups.isEmpty()) {
            saveLookups();
        }
        if (!relationships.isEmpty()) {
            saveRelationships();
        }
    }

    /**
     * Closes the handler, releasing resources.
     */
    public void close() {
        lookups.clear();
        relationships.clear();
    }

    /**
     * Saves a collection of objects.
     *
     * @param objects the objects to save
     */
    protected void save(Collection<IMObject> objects) {
        try {
            service.save(objects);
        } catch (OpenVPMSException exception) {
            // can't process as a batch. Process individual objects.
            for (IMObject object : objects) {
                save(object);
            }
        }
    }

    /**
     * Saves an object.
     *
     * @param object the object to save
     */
    protected void save(IMObject object) {
        try {
            service.save(object);
        } catch (OpenVPMSException exception) {
            notifyListener(exception);
        }
    }

    /**
     * Determines if a lookup exists.
     *
     * @param archetype the lookup archetype short name
     * @param code      the lookup code
     * @return {@code true} if it exists, otherwise {@code false}
     */
    protected boolean exists(String archetype, String code) {
        return cache.exists(archetype, code);
    }

    /**
     * Determines if a lookup relationship exists.
     *
     * @param archetype the relationship archetype short name
     * @param source    the source lookup
     * @param target    the target lookup
     * @return {@code true} if it exists, otherwise {@code false}
     */
    protected boolean exists(String archetype, Lookup source, Lookup target) {
        return cache.exists(archetype, source.getObjectReference(), target.getObjectReference());
    }

    /**
     * Saves the lookups.
     */
    private void saveLookups() {
        List<IMObject> objects = new ArrayList<>();
        for (LookupDescriptor descriptor : lookups.values()) {
            for (LookupData data : descriptor.getLookups()) {
                if (!exists(descriptor.getArchetype(), data.getCode())) {
                    createLookup(data, descriptor, objects);
                }
            }
            descriptor.clear();
        }
        if (!objects.isEmpty()) {
            save(objects);
        }
    }

    /**
     * Saves relationships between lookups.
     */
    private void saveRelationships() {
        for (LookupRelationshipDescriptor descriptor : relationships.values()) {
            List<IMObject> objects = createRelationships(descriptor);
            if (!objects.isEmpty()) {
                save(objects);
            }
            descriptor.clear();
        }
    }

    /**
     * Creates a lookup.
     *
     * @param data       describes the lookup to create
     * @param descriptor the lookup descriptor
     * @param objects    collects the created lookup
     */
    private void createLookup(LookupData data, LookupDescriptor descriptor, List<IMObject> objects) {
        String archetype = data.getArchetype();
        if (archetype == null) {
            archetype = descriptor.getArchetype();
        }
        Lookup lookup = service.create(archetype, Lookup.class);
        lookup.setCode(data.getCode());
        lookup.setName(data.getName());
        objects.add((IMObject) lookup);
        cache.add(lookup);
    }

    /**
     * Processes a {@link Node}.
     * <p/>
     * If it refers to a lookup, this is processed by {@link #processLookupDescriptor(NodeDescriptor)}.<br/>
     * If it refers to a targetLookup, this is collected in {@code targets} for later processing.
     *
     * @param node    the node
     * @param targets collects lookups of type 'targetLookup'
     */
    private void processNode(Node node, Map<NodeDescriptor, ArchetypeDescriptor> targets) {
        ArchetypeDescriptor archetype = service.getArchetypeDescriptor(node.getArchetype());
        if (archetype != null) {
            NodeDescriptor descriptor = archetype.getNodeDescriptor(node.getName());
            if (descriptor != null && descriptor.isLookup()) {
                if (isLookupType(descriptor)) {
                    processLookupDescriptor(descriptor);
                }
                if (isTargetLookupType(descriptor)) {
                    targets.put(descriptor, archetype);
                }
            }
        }
    }

    /**
     * Processes a lookup node descriptor with type 'lookup'.
     * This creates and registers a {@link LookupDescriptor}.
     *
     * @param descriptor the lookup node descriptor
     */
    private void processLookupDescriptor(NodeDescriptor descriptor) {
        AssertionDescriptor assertion = descriptor.getAssertionDescriptor(LOOKUP);
        String archetype = getValue(assertion, "source");
        lookups.put(descriptor, new LookupDescriptor(descriptor, archetype));
    }

    /**
     * Processes a lookup node descriptor with type 'targetLookup'.
     * This creates and registers a {@link LookupDescriptor},
     * and adds a {@link LookupRelationshipDescriptor} if the source is also
     * mapped.
     *
     * @param archetype the archetype that the node descriptor belongs to
     * @param target    the target lookup node descriptor
     * @throws LoaderException if the lookup definition is invalid
     */
    private void processTargetLookupDescriptor(ArchetypeDescriptor archetype, NodeDescriptor target) {
        AssertionDescriptor assertion = target.getAssertionDescriptor(LOOKUP);
        String relationship = getValue(assertion, "relationship");
        if (relationship == null) {
            throw new LoaderException(LookupRelationshipNotFound, archetype.getArchetypeType(), target.getName());
        }
        String value = getValue(assertion, "value");
        NodeDescriptor source = getNodeByPath(archetype, value);
        if (source == null) {
            String shortName = archetype.getType().getShortName();
            throw new LoaderException(LoaderException.ErrorCode.LookupSourceNodeNotFound,
                                      shortName, target.getName(), value);
        }
        String targetShortName = getTargetShortName(relationship);
        LookupDescriptor targetLookup = new LookupDescriptor(target, targetShortName);
        lookups.put(target, targetLookup);

        LookupDescriptor sourceLookup = lookups.get(source);
        if (sourceLookup != null) {
            LookupRelationshipDescriptor descriptor = new LookupRelationshipDescriptor(relationship, sourceLookup,
                                                                                       targetLookup);
            relationships.put(target, descriptor);
        }
    }

    /**
     * Returns the target node archetype short name from a lookup relationship.
     *
     * @param relationship the lookup relationship short name
     * @return the target node archetype short name
     */
    private String getTargetShortName(String relationship) {
        ArchetypeDescriptor archetype = service.getArchetypeDescriptor(relationship);
        if (archetype == null) {
            throw new LoaderException(ArchetypeNotFound, relationship);
        }
        NodeDescriptor node = archetype.getNodeDescriptor("target");
        String result = null;
        if (node != null) {
            String[] shortNames = DescriptorHelper.getShortNames(node, service);
            if (shortNames.length > 0) {
                // NOTE: don't support multiple lookup archetypes, but its difficult to see how these could be used
                result = shortNames[0];
            }
        }
        if (result == null) {
            throw new LoaderException(LookupRelationshipTargetNotFound, relationship);
        }
        return result;
    }

    /**
     * Creates lookup relationships for a lookup relationship descriptor.
     *
     * @param descriptor the lookup relationship descriptor
     * @return the lookup relationships
     * @throws ArchetypeServiceException for any archetype service error
     * @throws LoaderException           for any loader error
     */
    private List<IMObject> createRelationships(LookupRelationshipDescriptor descriptor) {
        List<IMObject> result = new ArrayList<>();
        for (Pair pair : descriptor.getPairs()) {
            String sourceCode = pair.getValue1();
            String targetCode = pair.getValue2();
            Lookup source = getLookup(descriptor.getSource(), sourceCode);
            Lookup target = getLookup(descriptor.getTarget(), targetCode);

            if (source != null && target != null) {
                if (!exists(descriptor.getArchetype(), source, target)) {
                    LookupRelationship relationship = service.create(descriptor.getArchetype(),
                                                                     LookupRelationship.class);
                    relationship.setSource(source.getObjectReference());
                    relationship.setTarget(target.getObjectReference());
                    result.add(relationship);
                    cache.add(relationship);
                }
            } else if (source == null) {
                throw new LoaderException(LookupNotFound, descriptor.getSource().getArchetype(), sourceCode);
            } else {
                throw new LoaderException(LookupNotFound, descriptor.getTarget().getArchetype(), targetCode);
            }
        }
        return result;
    }

    /**
     * Helper to get a lookup with the specified code from a cache of lookups.
     *
     * @param descriptor the lookup descriptor
     * @param code       the lookup code
     * @return the corresponding lookup or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    private Lookup getLookup(LookupDescriptor descriptor, String code) {
        return cache.get(descriptor.getArchetype(), code);
    }

    /**
     * Returns the node with matching jxpath.
     *
     * @param archetype the archetype descriptor
     * @param path      the node jxpath
     * @return the node with corresponding path, or {@code null} if none is found
     */
    private NodeDescriptor getNodeByPath(ArchetypeDescriptor archetype, String path) {
        for (NodeDescriptor node : archetype.getAllNodeDescriptors()) {
            if (node.getPath().equals(path)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Determines if a lookup node has type 'lookup'.
     *
     * @param descriptor the node descriptor
     * @return {@code true} if the node has type 'lookup'
     */
    private boolean isLookupType(NodeDescriptor descriptor) {
        AssertionDescriptor assertion = descriptor.getAssertionDescriptor(LOOKUP);
        if (assertion != null) {
            String type = getValue(assertion, "type");
            return LOOKUP.equals(type);
        }
        return false;
    }

    /**
     * Determines if a lookup node has type 'targetLookup'.
     *
     * @param descriptor the node descriptor
     * @return {@code true} if the node has type 'targetLookup'
     */
    private boolean isTargetLookupType(NodeDescriptor descriptor) {
        AssertionDescriptor assertion = descriptor.getAssertionDescriptor(LOOKUP);
        if (assertion != null) {
            String type = getValue(assertion, "type");
            return "targetLookup".equals(type);
        }
        return false;
    }

    /**
     * Returns the value of the named property from an assertion descriptor.
     *
     * @param assertion the assertion descriptor
     * @param name      the property name
     * @return the property value, or {@code null} if it doesn't exist
     */
    private String getValue(AssertionDescriptor assertion, String name) {
        NamedProperty property = assertion.getProperty(name);
        return (property != null) ? (String) property.getValue() : null;
    }

    /**
     * Notifies any registered listener of an error.
     *
     * @param exception the exception
     */
    private void notifyListener(Throwable exception) {
        if (listener != null) {
            listener.error(messages.getMessage(exception), exception);
        }
    }
}
