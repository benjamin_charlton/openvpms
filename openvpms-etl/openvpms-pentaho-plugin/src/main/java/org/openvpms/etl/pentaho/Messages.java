/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.etl.pentaho;

import org.pentaho.di.i18n.BaseMessages;


/**
 * Internationalisation support.
 *
 * @author Tim Anderson
 */
public class Messages {

    /**
     * Pentaho PDI looks for a messages package a messages resource bundle relative to the package of this class.
     */
    private static final Class<?> PACKAGE = Messages.class;

    /**
     * Default constructor.
     */
    private Messages() {
        // no-op
    }

    /**
     * Returns a formatted, localised message.
     *
     * @param key  the message key.
     * @param args message arguments
     * @return a formatted message
     */
    public static String get(String key, Object... args) {
        return BaseMessages.getString(PACKAGE, key, args);
    }

}