/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.openvpms.booking.domain.ScheduleRange;
import org.openvpms.component.model.entity.Entity;

import java.util.Date;

/**
 * Associates a schedule entity with a range.
 *
 * @author Tim Anderson
 */
class ScheduleEntityRange extends ScheduleRange {

    /**
     * The schedule entity.
     */
    private final Entity entity;

    /**
     * Constructs a {@link ScheduleEntityRange}.
     *
     * @param schedule the schedule
     * @param start    the start of the range
     * @param end      the end of the range
     */
    public ScheduleEntityRange(Entity schedule, Date start, Date end) {
        super(schedule.getId(), start, end);
        this.entity = schedule;
    }

    /**
     * Returns the schedule.
     *
     * @return the schedule entity
     */
    public Entity getEntity() {
        return entity;
    }

}
