/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestScheduleBuilder;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.booking.domain.Location;
import org.openvpms.booking.domain.Range;
import org.openvpms.booking.domain.Schedule;
import org.openvpms.booking.domain.ScheduleRange;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base class for booking service tests dealing with date ranges.
 *
 * @author Tim Anderson
 */
public abstract class AbstractBookingServiceTest extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    protected TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    protected TestUserFactory userFactory;

    /**
     * Returns a customer's last name.
     *
     * @param customer the customer
     * @return the last name
     */
    protected String getLastName(Party customer) {
        IMObjectBean bean = getBean(customer);
        return bean.getString("lastName");
    }

    /**
     * Enables or disables online booking for an entity.
     *
     * @param entity the the entity
     * @param enable if {@code true} enable online booking, else disable it
     */
    protected void enableOnlineBooking(Entity entity, boolean enable) {
        IMObjectBean bean = getBean(entity);
        bean.setValue("onlineBooking", enable);
        bean.save();
    }

    /**
     * Verifies a location matches that expected.
     *
     * @param expected the expected location
     * @param actual   the actual location
     */
    protected void checkLocation(Party expected, Location actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
    }

    /**
     * Verifies a schedule matches that expected.
     *
     * @param expected the expected schedule
     * @param slotSize the expected slot size
     * @param actual   the actual schedule
     */
    protected void checkSchedule(Entity expected, int slotSize, Schedule actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(slotSize, actual.getSlotSize());
        assertEquals(expected.getName(), actual.getName());
    }

    /**
     * Verifies ranges match those expected.
     *
     * @param ranges   the ranges
     * @param expected the expected ranges
     */
    protected void checkRanges(List<? extends Range> ranges, Range... expected) {
        assertEquals(expected.length, ranges.size());
        for (int i = 0; i < expected.length; ++i) {
            checkRange(expected[i], ranges.get(i));
        }
    }

    /**
     * Verifies ranges are present for a given schedule identifier.
     *
     * @param ranges     the available ranges. May contain those for multiple schedules
     * @param scheduleId the schedule identifier
     * @param expected   the expected ranges, in date order
     */
    protected void checkRanges(List<ScheduleRange> ranges, long scheduleId, Range... expected) {
        List<Range> expectedList = new ArrayList<>(Arrays.asList(expected));
        for (ScheduleRange range : ranges) {
            if (range.getSchedule() == scheduleId) {
                assertTrue(expectedList.size() > 0);
                checkRange(expectedList.remove(0), range);
            }
        }
        assertTrue(expectedList.isEmpty());
    }

    /**
     * Verifies a range matches that expected.
     *
     * @param range the range
     * @param start the expected start time
     * @param end   the expected end time
     */
    protected void checkRange(Range range, String start, String end) {
        assertEquals(range.getStart(), TestHelper.getDatetime(start));
        assertEquals(range.getEnd(), TestHelper.getDatetime(end));
    }

    /**
     * Creates a new range.
     *
     * @param start the start of the range
     * @param end   the end of the range
     * @return a new range
     */
    protected Range createRange(String start, String end) {
        return new Range(TestHelper.getDatetime(start), TestHelper.getDatetime(end));
    }

    /**
     * Returns an ISO date/time, with the current timezone offset.
     *
     * @param date a date string, yyyy-mm-dd format
     * @return the ISO date/time
     */
    protected String getISODate(String date) {
        return getISODate(date, "00:00");
    }

    /**
     * Returns an ISO date/time, with the current timezone offset.
     *
     * @param date a date string, yyyy-mm-dd format
     * @param time a time string, hh:mm format
     * @return the ISO date/time
     */
    protected String getISODate(String date, String time) {
        Date value = TestHelper.getDate(date);
        TimeZone tz = TimeZone.getDefault();
        int offset = tz.getOffset(value.getTime()); // to allow for daylight savings
        long hours = TimeUnit.MILLISECONDS.toHours(offset);
        long minutes = Math.abs(TimeUnit.MILLISECONDS.toMinutes(offset) - TimeUnit.HOURS.toMinutes(hours));
        StringBuilder result = new StringBuilder();
        result.append(date).append('T').append(time).append(":00");
        if (hours > 0) {
            result.append('+');
        }
        result.append(String.format("%d:%02d", hours, minutes));
        return result.toString();
    }

    /**
     * Creates a new practice location.
     *
     * @param onlineBooking if {@code true}, the location supports online booking
     * @return the location
     */
    protected Party createLocation(boolean onlineBooking) {
        return practiceFactory.newLocation()
                .onlineBooking(onlineBooking)
                .build();
    }

    /**
     * Creates a new appointment type with online booking enabled.
     *
     * @return the appointment type
     */
    protected Entity createAppointmentType() {
        return schedulingFactory.newAppointmentType()
                .onlineBooking()
                .build();
    }

    /**
     * Creates an appointment.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param schedule  the schedule
     * @return a new appointment
     */
    protected Act createAppointment(String startTime, String endTime, Entity schedule) {
        return createAppointment(startTime, endTime, schedule, null);
    }

    /**
     * Creates an appointment.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param schedule  the schedule
     * @param clinician the clinician. May be {@code null}
     * @return a new appointment
     */
    protected Act createAppointment(String startTime, String endTime, Entity schedule, User clinician) {
        Act appointment = ScheduleTestHelper.createAppointment(TestHelper.getDatetime(startTime),
                                                               TestHelper.getDatetime(endTime),
                                                               schedule);
        if (clinician != null) {
            getBean(appointment).setTarget("clinician", clinician);
        }
        save(appointment);
        return appointment;
    }

    /**
     * Returns a schedule builder for a new schedule at the specified location, with online booking enabled.
     *
     * @param location the practice location
     * @return a schedule builder
     */
    protected TestScheduleBuilder newSchedule(Party location) {
        return schedulingFactory.newSchedule()
                .location(location)
                .onlineBooking();
    }

    /**
     * Creates a new  24 hour schedule with 15 minute slots.
     *
     * @param location the practice location
     * @return a new schedule
     */
    protected Entity createSchedule(Party location) {
        return newSchedule(location)
                .slotSize(15, DateUnits.MINUTES)
                .build();
    }

    /**
     * Creates a new 9-5 schedule with 15 minute slots.
     *
     * @param location the practice location
     * @return a new schedule
     */
    protected Entity createNineToFiveSchedule(Party location) {
        return newSchedule(location)
                .times(9, 17)
                .slotSize(15, DateUnits.MINUTES)
                .build();
    }

    /**
     * Creates a new schedule with 15 minute slots.
     *
     * @param location  the practice location
     * @param startTime the schedule start time. May be {@code null}
     * @param endTime   the schedule end time. May be {@code null}
     * @return a new schedule
     */
    protected Entity createSchedule(Party location, String startTime, String endTime) {
        return newSchedule(location)
                .times(startTime, endTime)
                .slotSize(15, DateUnits.MINUTES)
                .build();
    }

    protected Entity createRosterArea(Party location, Entity... schedules) {
        return schedulingFactory.createRosterArea(location, schedules);
    }

    /**
     * Creates a clinician with online booking enabled.
     *
     * @return the clinician
     */
    protected User createClinician() {
        return userFactory.newUser()
                .clinician()
                .onlineBooking()
                .build();
    }

    /**
     * Creates a clinician with online booking enabled, linked to a practice location.
     *
     * @param location the practice location
     * @return the clinician
     */
    protected User createClinician(Party location) {
        return userFactory.newUser()
                .clinician()
                .onlineBooking()
                .addLocations(location)
                .build();
    }

    /**
     * Verifies a range matches that expected.
     *
     * @param expected the expected range
     * @param range    the range
     */
    private void checkRange(Range expected, Range range) {
        assertEquals(expected.getStart(), range.getStart());
        assertEquals(expected.getEnd(), range.getEnd());
    }
}
