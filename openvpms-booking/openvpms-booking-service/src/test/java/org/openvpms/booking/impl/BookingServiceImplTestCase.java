/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.glassfish.jersey.uri.internal.JerseyUriBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.booking.api.BookingService;
import org.openvpms.booking.domain.Booking;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link BookingServiceImpl}.
 *
 * @author Tim Anderson
 */
public class BookingServiceImplTestCase extends AbstractBookingServiceTest {

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules customerRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The user rules.
     */
    private UserRules userRules;

    /**
     * The test location.
     */
    private Party location;

    /**
     * The appointment type.
     */
    private Entity appointmentType;

    /**
     * The schedule.
     */
    private Entity schedule;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The appointment rules.
     */
    private AppointmentRules appointmentRules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        appointmentRules = new AppointmentRules(getArchetypeService());
        location = practiceFactory.createLocation();
        enableOnlineBooking(location, true);
        appointmentType = schedulingFactory.newAppointmentType().sendReminders(true).build();
        clinician = userFactory.createClinician();
        userRules = new UserRules(getArchetypeService());
        schedule = schedulingFactory.newSchedule()
                .location(location)
                .slotSize(15, DateUnits.MINUTES)
                .addAppointmentType(appointmentType, 1, true)
                .onlineBooking()
                .sendReminders(true)
                .build();
        configureSMSJob();
    }

    /**
     * Tests making a booking.
     */
    @Test
    public void testBooking() {
        Party customer = customerFactory.newCustomer()
                .title("MS")
                .addEmail("foo@bar.com")
                .build();
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build();
        Booking booking = new Booking();
        booking.setLocation(location.getId());
        booking.setSchedule(schedule.getId());
        booking.setAppointmentType(appointmentType.getId());
        Date startTime = DateRules.getTomorrow();
        Date endTime = DateRules.getDate(startTime, 15, DateUnits.MINUTES);
        booking.setStart(startTime);
        booking.setEnd(endTime);
        booking.setUser(clinician.getId());
        booking.setTitle("Ms");
        booking.setFirstName("J");
        booking.setLastName(getLastName(customer));
        booking.setEmail("foo@bar.com");
        booking.setPatientName(patient.getName());
        booking.setNotes("Some notes");

        Act appointment = createBooking(booking);
        checkAppointment(appointment, startTime, endTime, customer, patient, clinician, false,
                         "Notes: " + booking.getNotes());
    }

    /**
     * Tests the {@link BookingService#getBooking(String)} method.
     */
    @Test
    public void testGetBooking() {
        Party customer = customerFactory.newCustomer()
                .title("MS")
                .addEmail("foo@bar.com")
                .build();
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build();
        Booking booking1 = new Booking();
        booking1.setLocation(location.getId());
        booking1.setSchedule(schedule.getId());
        booking1.setAppointmentType(appointmentType.getId());
        Date startTime = DateRules.getTomorrow();
        Date endTime = DateRules.getDate(startTime, 15, DateUnits.MINUTES);
        booking1.setStart(startTime);
        booking1.setEnd(endTime);
        booking1.setUser(clinician.getId());
        booking1.setTitle("Ms");
        booking1.setFirstName("J");
        booking1.setLastName(getLastName(customer));
        booking1.setEmail("foo@bar.com");
        booking1.setPatientName(patient.getName());
        booking1.setNotes("Some notes");

        BookingService service = createBookingService();
        Response response = service.create(booking1, createUriInfo());
        assertEquals("text/plain", response.getHeaderString(HttpHeaders.CONTENT_TYPE));
        assertEquals(201, response.getStatus());
        String reference = (String) response.getEntity();
        assertNotNull(reference);

        Booking booking2 = service.getBooking(reference);

        assertEquals(booking1.getLocation(), booking2.getLocation());
        assertEquals(booking1.getSchedule(), booking2.getSchedule());
        assertEquals(booking1.getAppointmentType(), booking2.getAppointmentType());
        assertEquals(booking1.getStart(), booking2.getStart());
        assertEquals(booking1.getEnd(), booking2.getEnd());
        assertEquals(booking1.getUser(), booking2.getUser());
        assertEquals("MS", booking2.getTitle());
        assertEquals(booking1.getFirstName(), booking2.getFirstName());
        assertEquals(booking1.getLastName(), booking2.getLastName());
        assertEquals(booking1.getEmail(), booking2.getEmail());
        assertEquals("", booking2.getMobile());
        assertEquals("", booking2.getPhone());
        assertEquals(booking1.getPatientName(), booking2.getPatientName());
        assertNull(booking2.getNotes());
    }

    /**
     * Test booking for a new customer. The appointment will be created without a customer participation.
     */
    @Test
    public void testBookingForNewCustomer() {
        Booking booking = new Booking();
        booking.setLocation(location.getId());
        booking.setSchedule(schedule.getId());
        booking.setAppointmentType(appointmentType.getId());
        Date startTime = DateRules.getTomorrow();
        Date endTime = DateRules.getDate(startTime, 15, DateUnits.MINUTES);
        booking.setStart(startTime);
        booking.setEnd(endTime);
        booking.setTitle("Mr");
        booking.setFirstName("Foo");
        booking.setLastName("Bar" + System.currentTimeMillis());
        booking.setEmail("foo@bar.com");
        booking.setPatientName("Fido");

        Act appointment = createBooking(booking);
        String bookingNotes = "Title: Mr\n" +
                              "First Name: Foo\n" +
                              "Last Name: " + booking.getLastName() + "\n" +
                              "Email: foo@bar.com\n" +
                              "Patient: Fido";
        checkAppointment(appointment, startTime, endTime, null, null, null, false, bookingNotes);
    }

    /**
     * Verifies that bookings cannot be made if the associated location is inactive.
     */
    @Test
    public void testInactiveLocation() {
        location.setActive(false);
        save(location);
        String message = location.getName() + " is not available for booking";
        checkBadRequest(message);
    }

    /**
     * Verifies that bookings cannot be made if the associated location has online booking turned off.
     */
    @Test
    public void testOnlineBookingDisabledForLocation() {
        enableOnlineBooking(location, false);
        String message = location.getName() + " is not available for booking";
        checkBadRequest(message);
    }

    /**
     * Verifies that bookings cannot be made if the associated schedule is inactive.
     */
    @Test
    public void testInactiveSchedule() {
        schedule.setActive(false);
        save(schedule);
        String message = schedule.getName() + " is not available for booking";
        checkBadRequest(message);
    }

    /**
     * Verifies that bookings cannot be made if the associated schedule has online booking turned off.
     */
    @Test
    public void testOnlineBookingDisabledForSchedule() {
        enableOnlineBooking(schedule, false);
        String message = schedule.getName() + " is not available for booking";
        checkBadRequest(message);
    }

    /**
     * Verifies that appointments can still be made for inactive clinicians.
     */
    @Test
    public void testInactiveClinician() {
        clinician.setActive(false);
        save(clinician);

        Booking booking = createBooking();
        BookingService service = createBookingService();
        Response response = service.create(booking, createUriInfo());
        assertEquals(201, response.getStatus());
    }

    /**
     * Verifies that bookings can be made if the associated clinician has online booking turned off.
     */
    @Test
    public void testOnlineBookingDisabledForClinician() {
        enableOnlineBooking(clinician, false);

        Booking booking = createBooking();
        BookingService service = createBookingService();
        Response response = service.create(booking, createUriInfo());
        assertEquals(201, response.getStatus());
    }

    /**
     * Verifies that appointments can still be made for inactive appointment types.
     */
    @Test
    public void testInactiveAppointmentType() {
        appointmentType.setActive(false);
        save(appointmentType);

        Booking booking = createBooking();
        BookingService service = createBookingService();
        Response response = service.create(booking, createUriInfo());
        assertEquals(201, response.getStatus());
    }

    /**
     * Verifies that appointments can still be made for appointment types with online booking disabled.
     */
    @Test
    public void testOnlineBookingDisabledForAppointmentType() {
        enableOnlineBooking(appointmentType, false);
        Booking booking = createBooking();
        BookingService service = createBookingService();
        Response response = service.create(booking, createUriInfo());
        assertEquals(201, response.getStatus());
    }

    /**
     * Verifies that the send reminder flag is set if the appointment if the customer can receive SMS.
     */
    @Test
    public void testSendReminder() {
        String phoneNumber = "04123456789";
        Party customer = customerFactory.newCustomer()
                .title("MS")
                .firstName("J")
                .addMobilePhone(phoneNumber)
                .build();
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build();
        Booking booking = new Booking();
        booking.setLocation(location.getId());
        booking.setSchedule(schedule.getId());
        booking.setAppointmentType(appointmentType.getId());
        Date startTime = DateRules.getNextDate(DateRules.getTomorrow());
        Date endTime = DateRules.getDate(startTime, 30, DateUnits.MINUTES);
        booking.setStart(startTime);
        booking.setEnd(endTime);
        booking.setTitle("Ms");
        booking.setFirstName("J");
        booking.setLastName(getLastName(customer));
        booking.setMobile(phoneNumber);
        booking.setPatientName(patient.getName());

        Act appointment = createBooking(booking);
        checkAppointment(appointment, startTime, endTime, customer, patient, null, true, null);
    }

    /**
     * Tests booking cancellation.
     */
    @Test
    public void testCancel() {
        Booking booking = createBooking();

        ArrayList<Act> acts = new ArrayList<>();
        BookingService service = createBookingService(acts);
        Response response1 = service.create(booking, createUriInfo());
        assertEquals(201, response1.getStatus());
        assertEquals("text/plain", response1.getHeaderString(HttpHeaders.CONTENT_TYPE));
        assertEquals(1, acts.size());
        Act appointment = acts.get(0);
        assertEquals(AppointmentStatus.PENDING, appointment.getStatus());

        String reference = (String) response1.getEntity();
        assertEquals(appointment.getId() + ":" + appointment.getLinkId(), reference);
        Response response2 = service.cancel(reference);
        assertEquals(204, response2.getStatus());

        appointment = get(appointment);
        assertEquals(ActStatus.CANCELLED, appointment.getStatus());

        try {
            service.cancel(reference);
            fail("Expected cancellation to fail");
        } catch (NotFoundException exception) {
            assertEquals("Booking not found", exception.getMessage());
        }
    }

    /**
     * Verifies that trying to create an appointment in the past throws {@link BadRequestException}.
     */
    @Test
    public void testBackDatedAppointment() {
        Date startTime = DateRules.getYesterday();
        Date endTime = DateRules.getDate(startTime, 45, DateUnits.MINUTES);
        Booking booking = createBooking(startTime, endTime);
        BookingService service = createBookingService();

        try {
            service.create(booking, createUriInfo());
            fail("Expected back-dated create to fail");
        } catch (BadRequestException exception) {
            assertEquals("Cannot make a booking in the past", exception.getMessage());
        }
    }

    /**
     * Verifies that trying to create an appointment with a start less than end throws {@link BadRequestException}.
     */
    @Test
    public void testStartLessThanEnd() {
        Date startTime = DateRules.getTomorrow();
        Date endTime = DateRules.getDate(startTime, 45, DateUnits.MINUTES);
        Booking booking = createBooking(endTime, startTime);
        BookingService service = createBookingService();

        try {
            service.create(booking, createUriInfo());
            fail("Expected create to fail");
        } catch (BadRequestException exception) {
            assertEquals("Booking start must be less than end", exception.getMessage());
        }
    }

    /**
     * Verifies that trying to create an appointment that doesn't start or end on a slot boundary throws
     * {@link BadRequestException}.
     */
    @Test
    public void testInvalidSlotBoundary() {
        BookingService service = createBookingService();
        Date start1 = DateRules.getDate(DateRules.getTomorrow(), 5, DateUnits.MINUTES);
        Date end1 = DateRules.getDate(start1, 45, DateUnits.MINUTES);
        Booking booking1 = createBooking(start1, end1);

        try {
            service.create(booking1, createUriInfo());
            fail("Expected create to fail");
        } catch (BadRequestException exception) {
            assertEquals("Booking start is not on a slot boundary", exception.getMessage());
        }

        Date start2 = DateRules.getTomorrow();
        Date end2 = DateRules.getDate(start1, 20, DateUnits.MINUTES);
        Booking booking2 = createBooking(start2, end2);

        try {
            service.create(booking2, createUriInfo());
            fail("Expected create to fail");
        } catch (BadRequestException exception) {
            assertEquals("Booking end is not on a slot boundary", exception.getMessage());
        }
    }

    /**
     * Verifies that attempting to make an appointment that overlaps another results in {@link BadRequestException}.
     */
    @Test
    public void testOverlappingBooking() {
        Date nineAM = DateRules.getDate(DateRules.getTomorrow(), 9, DateUnits.HOURS);
        Date nine30 = DateRules.getDate(nineAM, 30, DateUnits.MINUTES);
        Date nine15 = DateRules.getDate(nineAM, 15, DateUnits.MINUTES);
        Booking booking1 = createBooking(nineAM, nine30);
        Booking booking2 = createBooking(nine15, nine30);

        BookingService service = createBookingService();
        Response response1 = service.create(booking1, createUriInfo());
        assertEquals(201, response1.getStatus());

        try {
            service.create(booking2, createUriInfo());
            fail("Expected booking request to fail");
        } catch (BadRequestException expected) {
            String message = "An appointment is already scheduled for " + new Timestamp(nineAM.getTime())
                             + "-" + new Timestamp(nine30.getTime());
            assertEquals(message, expected.getMessage());
        }
    }

    /**
     * Verifies that making a booking fails with a {@code BadRequestException} with the expected message.
     *
     * @param message the expected message
     */
    private void checkBadRequest(String message) {
        Booking booking = createBooking();
        BookingService service = createBookingService();
        try {
            service.create(booking, createUriInfo());
            fail("Call should have failed");
        } catch (BadRequestException expected) {
            assertEquals(message, expected.getMessage());
        }
    }

    /**
     * Helper to create a booking.
     *
     * @return a new booking
     */
    private Booking createBooking() {
        Date startTime = DateRules.getTomorrow();
        Date endTime = DateRules.getDate(startTime, 45, DateUnits.MINUTES);
        return createBooking(startTime, endTime);
    }

    /**
     * Helper to create a booking.
     *
     * @param startTime the booking start time
     * @param endTime   the booking end time
     * @return a new booking
     */
    private Booking createBooking(Date startTime, Date endTime) {
        Booking booking = new Booking();
        booking.setLocation(location.getId());
        booking.setSchedule(schedule.getId());
        booking.setAppointmentType(appointmentType.getId());
        booking.setUser(clinician.getId());
        booking.setStart(startTime);
        booking.setEnd(endTime);
        booking.setTitle("Mr");
        booking.setFirstName("Foo");
        booking.setLastName(ValueStrategy.random("Bar").toString());
        booking.setEmail("foo@bar.com");
        booking.setPatientName("Fido");
        return booking;
    }

    /**
     * Verifies an appointment matches that expected.
     *
     * @param appointment  the appointment to check
     * @param startTime    the expected start time
     * @param endTime      the expected end time
     * @param customer     the expected customer
     * @param patient      the expected patient
     * @param clinician    the expected clinician
     * @param sendReminder the expected 'send reminder' flag
     * @param bookingNotes the expected booking notes
     */
    private void checkAppointment(Act appointment, Date startTime, Date endTime, Party customer, Party patient,
                                  User clinician, boolean sendReminder, String bookingNotes) {
        IMObjectBean bean = getBean(appointment);
        assertEquals(startTime, bean.getDate("startTime"));
        assertEquals(endTime, bean.getDate("endTime"));
        assertEquals(customer, bean.getTarget("customer"));
        assertEquals(patient, bean.getTarget("patient"));
        assertEquals(schedule, bean.getTarget("schedule"));
        assertEquals(appointmentType, bean.getTarget("appointmentType"));
        assertEquals(clinician, bean.getTarget("clinician"));
        assertEquals(sendReminder, bean.getBoolean("sendReminder"));
        assertEquals(bookingNotes, bean.getString("bookingNotes"));
        assertTrue(bean.getBoolean("onlineBooking"));
    }

    /**
     * Configure the SMS job; this is required for the sendReminder flag to be set.
     */
    private void configureSMSJob() {
        IMObjectBean bean;
        ArchetypeQuery query = new ArchetypeQuery("entity.jobAppointmentReminder", true);
        IMObjectQueryIterator<Entity> iterator = new IMObjectQueryIterator<>(query);
        if (!iterator.hasNext()) {
            bean = getBean(create("entity.jobAppointmentReminder"));
            bean.setTarget("runAs", TestHelper.createUser());
        } else {
            bean = getBean(iterator.next());
        }
        bean.setValue("smsFrom", 3);
        bean.setValue("smsFromUnits", DateUnits.DAYS);
        bean.setValue("smsTo", 1);
        bean.setValue("smsToUnits", DateUnits.DAYS);
        bean.setValue("noReminder", 1);
        bean.setValue("noReminderUnits", DateUnits.DAYS);
        bean.save();
    }

    /**
     * Creates a booking.
     *
     * @param booking the booking
     * @return the corresponding appointment
     */
    private Act createBooking(Booking booking) {
        final List<Act> acts = new ArrayList<>();
        BookingService service = createBookingService(acts);
        Response response = service.create(booking, createUriInfo());
        assertEquals("text/plain", response.getHeaderString(HttpHeaders.CONTENT_TYPE));
        assertEquals(201, response.getStatus());
        assertEquals(1, acts.size());
        return acts.get(0);
    }

    /**
     * Creates a booking service.
     *
     * @return the booking service
     */
    private BookingServiceImpl createBookingService() {
        return new BookingServiceImpl(getArchetypeService(), customerRules, appointmentRules,
                                      userRules, transactionManager);
    }

    /**
     * Creates a booking service that collects appointments.
     *
     * @param acts the list to collect appointment acts
     * @return the booking service
     */
    private BookingService createBookingService(final List<Act> acts) {
        return new BookingServiceImpl(getArchetypeService(), customerRules, appointmentRules,
                                      userRules, transactionManager) {
            @Override
            protected void save(org.openvpms.component.model.act.Act act,
                                org.openvpms.component.model.entity.Entity schedule) {
                super.save(act, schedule);
                acts.add((Act) act);
            }
        };
    }

    /**
     * Helper to create a {@code UriInfo}.
     *
     * @return a new {@code UriInfo}
     */
    private UriInfo createUriInfo() {
        UriInfo uriInfo = Mockito.mock(UriInfo.class);
        try {
            URI uri = new URI("http://localhost:8080/openvpms/ws/booking/v1/bookings");
            Mockito.when(uriInfo.getAbsolutePath()).thenReturn(uri);
            JerseyUriBuilder builder = new JerseyUriBuilder();
            builder.uri(uri);
            Mockito.when(uriInfo.getAbsolutePathBuilder()).thenReturn(builder);
        } catch (Exception exception) {
            fail(exception.getMessage());
        }
        return uriInfo;
    }

}
