/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.tool.toolbox.Commands;
import picocli.CommandLine.Command;

/**
 * User command group.
 *
 * @author Tim Anderson
 */
@Command(name = "user", description = "Manage users",
        subcommands = {ListUsersCommand.class, UserPasswordCommand.class, EnableUserCommand.class,
                       DisableUserCommand.class, AddRoleCommand.class, RemoveRoleCommand.class})
public class UserCommands extends Commands {

}
