/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.tool.db.DBTool;
import org.openvpms.tool.toolbox.i18n.ToolboxMessages;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.io.Console;

import static org.openvpms.tool.toolbox.archetype.ArchetypeLoadCommand.DEFAULT_PATH;

/**
 * Command to update the database to the latest version.
 *
 * @author Tim Anderson
 */
@Command(name = "--update", header = "Updates the database to the latest version")
public class UpdateDBCommand extends AbstractDBCommand {

    /**
     * The archetype directory.
     */
    @CommandLine.Option(names = {"-d", "--dir"}, required = true, description = "The archetype directory.",
            defaultValue = DEFAULT_PATH)
    String dir = DEFAULT_PATH;

    /**
     * The plugins directory.
     */
    @CommandLine.Option(names = {"--plugins"}, required = true, description = "The plugins directory.",
            defaultValue = DEFAULT_PLUGINS_DIR)
    String pluginsDir = DEFAULT_PLUGINS_DIR;

    /**
     * Flag to override interactive prompt.
     */
    @CommandLine.Option(names = "--database-is-backed-up", hidden = true)
    boolean backedup;

    /**
     * The default plugins directory.
     */
    private static final String DEFAULT_PLUGINS_DIR = "${openvpms.home}/plugins";

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result = 1;
        DBTool tool = getTool();
        if (tool.needsUpdate()) {
            dir = getDir(dir, "--dir");
            pluginsDir = getDir(pluginsDir, "--plugins");
            if (backedup(tool.getChangesToApply())) {
                updateDatabase(dir, pluginsDir);
                result = 0;
            }
        } else {
            System.out.println(ToolboxMessages.get("db.update.uptodate"));
            result = 0;
        }
        return result;
    }

    /**
     * Determines if the database is backed up.
     *
     * @param changes the number of changes that need to be applied
     * @return {@code true} if the database is backed up, otherwise {@code false}
     */
    private boolean backedup(int changes) {
        boolean result = false;
        if (backedup) {
            result = true;
        } else {
            Console console = System.console();
            if (console == null) {
                System.err.println(ToolboxMessages.get("db.update.console"));
            } else {
                console.printf(ToolboxMessages.get("db.update.changes", changes) + "\n\n");
                console.printf(ToolboxMessages.get("db.update.warning") + "\n\n");
                while (true) {
                    console.printf(ToolboxMessages.get("db.update.proceed") + " ");
                    String input = console.readLine();
                    if ("Y".equals(input)) {
                        result = true;
                        break;
                    } else if ("n".equals(input)) {
                        break;
                    }
                }
            }
        }
        return result;
    }
}
