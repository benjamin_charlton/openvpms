/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.component.model.user.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * Command to set a user's password.
 *
 * @author Tim Anderson
 */
@Command(name = "--setpassword", description = "Sets a user's password")
public class UserPasswordCommand extends AbstractUserCommand {

    /**
     * The user name.
     */
    @Parameters(index = "0", arity = "1")
    String user;

    /**
     * The user password. Needs to be an option rather than a parameter to support interactive entry.
     */
    @Option(names = "-p", description = "password", required = true, interactive = true, arity = "0..1")
    String password;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        int result;
        User match = getUser(user);
        if (match != null) {
            PasswordEncoder encoder = getBean(PasswordEncoder.class);
            String encrypted = encoder.encode(password);
            ((org.openvpms.component.business.domain.im.security.User) match).setPassword(encrypted);
            save(match);
            result = 0;
            System.err.println("Password updated");
        } else {
            result = 1;
            System.err.println("User not found");
        }
        return result;
    }

}
