/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.openvpms.tool.toolbox.AbstractCommand;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * Command to set up openvpms.properties.
 *
 * @author Tim Anderson
 */
@Command(name = "configure", header = "Configure openvpms.properties")
public class ConfigCommand extends AbstractCommand {

    /**
     * Determines if a default configuration file should be created without prompting.
     */
    @CommandLine.Option(names = "--default", description = "Don't prompt. Use a default value where none is set.")
    private boolean useDefaults;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        ConfigCreator configCreator = new ConfigCreator();
        if (useDefaults) {
            configCreator.createDefault(getPropertiesPath());
        } else {
            configCreator.prompt(getPropertiesPath());
        }
        return 0;
    }
}
