/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.tool.db.DBTool;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import static org.openvpms.tool.toolbox.archetype.ArchetypeLoadCommand.DEFAULT_PATH;

/**
 * Command to create the database.
 * <p/>
 * This supports creating empty databases, which should  be used when restoring backups of a prior version of OpenVPMS
 * to a new server.
 *
 * @author Tim Anderson
 */
@Command(name = "--create", header = "Create the database")
public class CreateDBCommand extends AbstractDBCommand {

    /**
     * The admin user name.
     */
    @Option(names = "-u", required = true, description = "Database administrator user name")
    private String user;

    /**
     * The admin password.
     */
    @Option(names = "-p", required = true, description = "Database administrator password", interactive = true,
            arity = "0..1")
    private String password;

    /**
     * The host the user is connecting from.
     */
    @Option(names = "--host", description = "Host user is connecting from", required = true,
            defaultValue = "localhost", showDefaultValue = CommandLine.Help.Visibility.ALWAYS)
    private String host;

    /**
     * Determines if an empty database should be created.
     */
    @Option(names = "--empty", description = "Create an empty database.\n" +
                                             "Use this when restoring a backup to a new server.")
    private boolean empty;

    /**
     * The archetype directory.
     */
    @Option(names = {"-d", "--dir"}, description = "The archetype directory.", defaultValue = DEFAULT_PATH)
    private String dir = DEFAULT_PATH;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result;
        DBTool tool = getTool();
        tool.create(user, password, host, !empty);
        if (!empty) {
            updateDatabase(dir, null); // loads archetypes
            result = 0;
        } else {
            result = 0;
        }
        return result;
    }

}
