/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

/**
 * The config properties.
 *
 * @author Tim Anderson
 */
public class ConfigProperties extends AbstractConfigProperties {

    /**
     * The property file path.
     */
    private final Path path;

    /**
     * The properties.
     */
    private static final String[] PROPERTIES = {DB_DRIVER, DB_URL, DB_USERNAME, DB_PASSWORD,
                                                REPORTING_DB_URL, REPORTING_DB_USERNAME,
                                                REPORTING_DB_PASSWORD, OPENVPMS_KEY};

    /**
     * Creates a {@link ConfigProperties}.
     *
     * @param path the configuration file path
     * @throws IOException for any I/O error
     */
    public ConfigProperties(Path path) throws IOException {
        this(path, true);
    }

    /**
     * Constructs a {@link ConfigProperties}.
     *
     * @param path the properties path
     * @param read if {@code true}, read any existing properties
     * @throws IOException for any I/O error
     */
    public ConfigProperties(Path path, boolean read) throws IOException {
        super(new OrderedProperties());
        this.path = path;
        if (read && exists()) {
            try (FileInputStream stream = new FileInputStream(path.toFile())) {
                getProperties().load(stream);
            }
        }
    }

    /**
     * Determines if the configuration is complete.
     *
     * @return {@code true} if the configuration is complete
     */
    public boolean isComplete() {
        for (String name : PROPERTIES) {
            if (get(name) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validates that all required properties are present.
     *
     * @throws IllegalStateException if a property is missing
     */
    public void validate() {
        for (String name : PROPERTIES) {
            checkRequired(name);
        }
    }

    /**
     * Returns the properties path.
     *
     * @return the properties path
     */
    public Path getPath() {
        return path;
    }

    /**
     * Determines if the properties file exists.
     *
     * @return {@code true} if the properties file exists
     */
    public boolean exists() {
        return Files.exists(path);
    }

    /**
     * Copy the configuration from the specified properties.
     *
     * @param properties the properties
     */
    public void copy(ConfigProperties properties) {
        Properties source = properties.getProperties();
        for (String name : PROPERTIES) {
            set(name, source.getProperty(name));
        }
    }

    /**
     * Verifies that a required property is present.
     *
     * @param name the property name
     * @throws IllegalStateException if the property is missing
     */
    protected void checkRequired(String name) {
        if (get(name) == null) {
            throw new IllegalStateException("Property '" + name + "' missing from " + getPath());
        }
    }

    /**
     * Sets a property.
     *
     * @param name  the property name
     * @param value the property value
     * @return {@code true} if the property was changed
     */
    protected boolean set(String name, String value) {
        return !Objects.equals(getProperties().setProperty(name, value), value);
    }

    /**
     * Ensures that property ordering is preserved when writing properties for improved readability.
     * <p/>
     * This is a little brittle as different versions of Java use different techniques for iterating through
     * the properties when writing to file.
     */
    private static class OrderedProperties extends Properties {

        /**
         * Returns the keys in {@link #PROPERTIES} order.
         * <p/>
         * This is required in Java 8.
         *
         * @return the entries
         */
        @Override
        public Enumeration<Object> keys() {
            return Collections.enumeration(keySet());
        }

        /**
         * Returns the keys in {@link #PROPERTIES} order.
         * <p/>
         * This is required in Java 8.
         *
         * @return the entries
         */
        @Override
        public Set<Object> keySet() {
            Set<Object> keys = new LinkedHashSet<>(super.keySet());
            Set<Object> result = new LinkedHashSet<>();
            for (Object key : PROPERTIES) {
                for (Object entry : keys.toArray()) {
                    if (entry.equals(key)) {
                        result.add(entry);
                        keys.remove(entry);
                        break;
                    }
                }
            }
            result.addAll(keys);
            return result;
        }

        /**
         * Returns the entries in {@link #PROPERTIES} order.
         * <p/>
         * This is required in Java 11.
         *
         * @return the entries
         */
        @Override
        @SuppressWarnings("unchecked")
        public Set<Map.Entry<Object, Object>> entrySet() {
            Set<Map.Entry<Object, Object>> entries = new LinkedHashSet<>(super.entrySet());
            Set<Map.Entry<Object, Object>> result = new LinkedHashSet<>();
            for (Object key : PROPERTIES) {
                for (Map.Entry<Object, Object> entry : entries.toArray(new Map.Entry[0])) {
                    if (entry.getKey().equals(key)) {
                        result.add(entry);
                        entries.remove(entry);
                        break;
                    }
                }
            }
            result.addAll(entries);
            return result;
        }
    }
}
