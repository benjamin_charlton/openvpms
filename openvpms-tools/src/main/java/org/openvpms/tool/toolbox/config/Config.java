/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

/**
 * OpenVPMS configuration properties.
 *
 * @author Tim Anderson
 */
public interface Config {

    /**
     * JDBC driver class name property.
     */
    String DB_DRIVER = "db.driver";

    /**
     * JDBC url property.
     */
    String DB_URL = "db.url";

    /**
     * JDBC username property.
     */
    String DB_USERNAME = "db.username";

    /**
     * JDBC password property.
     */
    String DB_PASSWORD = "db.password";

    /**
     * Reporting JDBC URL property.
     */
    String REPORTING_DB_URL = "reporting.db.url";

    /**
     * Reporting JDBC username property.
     */
    String REPORTING_DB_USERNAME = "reporting.db.username";

    /**
     * Reporting JDBC password property.
     */
    String REPORTING_DB_PASSWORD = "reporting.db.password";

    /**
     * Encryption key property.
     */
    String OPENVPMS_KEY = "openvpms.key";

    /**
     * Returns the JDBC driver class name.
     *
     * @return the JDBC driver class name. May be {@code null}
     */
    String getDriver();

    /**
     * Returns the database URL.
     *
     * @return the database URL. May be {@code null}
     */
    String getUrl();

    /**
     * Returns the database username.
     *
     * @return the database username. May be {@code null}
     */
    String getUsername();

    /**
     * Returns the database password.
     *
     * @return the database password. May be {@code null}
     */
    String getPassword();

    /**
     * Returns the reporting database URL.
     *
     * @return the reporting database URL. May be {@code null}
     */
    String getReportingUrl();

    /**
     * Returns the reporting username.
     *
     * @return the reporting username. May be {@code null}
     */
    String getReportingUsername();

    /**
     * Returns the reporting password.
     *
     * @return the reporting password. May be {@code null}
     */
    String getReportingPassword();

    /**
     * Returns the encryption key.
     *
     * @return the encryption key. May be {@code null}
     */
    String getKey();
}
