/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.tool.toolbox.AbstractCommand;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.util.PathHelper;

import java.nio.file.Path;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

/**
 * Command to patch the openvpms.war with properties from openvpms.properties.
 *
 * @author Tim Anderson
 */
@Command(name = "war", header = "Create the web archive")
public class WarCommand extends AbstractCommand {

    /**
     * The war file name.
     */
    @Option(names = {"-n", "--name"}, description = "The war file name", defaultValue = "openvpms.war")
    String name;

    /**
     * The source zip.
     */
    @Option(names = {"-a", "--archive"}, description = "The archive path", defaultValue = WarBuilder.MASTER_ZIP_PATH)
    String archive;

    /**
     * The output directory.
     */
    @Option(names = {"-d", "--dir"}, required = true, description = "Output directory",
            defaultValue = WarBuilder.OUTPUT_DIR_PATH)
    String dir;


    /**
     * Initialises the command.
     *
     * @throws Exception for any error
     */
    @Override
    protected void init() throws Exception {
        ConfigProperties properties = loadProperties();
        checkProperties(properties);
    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result = 1;
        String war = getWarName();
        if (war != null) {
            WarBuilder builder = new WarBuilder();
            Path target = builder.build(war, dir, archive, getPropertiesPath());
            Path relative = PathHelper.getPathRelativeToCWD(target);
            System.out.printf("Created %s\n\n", relative);
            try {
                PathHelper.restrictPermissions(target);
            } catch (Throwable exception) {
                System.err.printf("\nThe file %s should have restrictive permissions to protect passwords\n\n",
                                  relative);
            }
            result = 0;
        }
        return result;
    }

    /**
     * Returns the war name.
     *
     * @return the war name, minus any extension
     */
    private String getWarName() {
        String result = null;
        if (name != null) {
            String war = StringUtils.removeEndIgnoreCase(name, ".war");
            if (!war.matches("[a-zA-Z]+[a-zA-Z0-9_\\-]*")) {
                System.err.printf("%s is not a valid name\n", name);
            } else {
                result = war;
            }
        } else {
            result = "openvpms";
        }
        return result;
    }

}
