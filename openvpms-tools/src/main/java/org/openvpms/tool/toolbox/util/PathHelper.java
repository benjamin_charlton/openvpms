/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

/**
 * Path util.
 *
 * @author Tim Anderson
 */
public class PathHelper {

    /**
     * Returns a path relative to the current working directory.
     *
     * @param path the path
     * @return the relative path
     */
    public static Path getPathRelativeToCWD(Path path) {
        Path cwd = Paths.get(".").toAbsolutePath().normalize();
        return cwd.relativize(path);
    }

    /**
     * Returns the path to the OpenVPMS installation directory.
     * <p/>
     * This requires the system property <em>openvpms.home</em> to be set.
     *
     * @return the installation directory
     * @throws IllegalStateException if openvpms.home is not defined or is invalid
     */
    public static Path getHome() {
        String home = System.getProperty("openvpms.home");
        if (home == null) {
            throw new IllegalStateException("openvpms.home is not defined");
        }
        Path path = Paths.get(home);
        if (!Files.exists(path) || !Files.isDirectory(path)) {
            throw new IllegalStateException("Invalid openvpms.home: " + path);
        }
        return path;
    }

    /**
     * Replaces the ${openvpms.home} placeholder with the actual path.
     *
     * @param path the path to update
     * @return the updated path
     * @throws IllegalStateException if openvpms.home is not defined or is invalid
     */
    public static String replaceHome(String path) {
        return path.replace("${openvpms.home}", getHome().toString());
    }

    /**
     * Restricts permissions on the specified path.
     *
     * @param path the path
     * @throws IOException if the permissions cannot be set
     */
    public static void restrictPermissions(Path path) throws IOException {
        Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rw-------");
        Files.setPosixFilePermissions(path, permissions);
    }
}
