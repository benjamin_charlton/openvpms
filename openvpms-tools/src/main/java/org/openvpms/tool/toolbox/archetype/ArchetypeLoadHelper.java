/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.archetype;

import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.tool.toolbox.util.PathHelper;
import org.openvpms.tools.archetype.loader.ArchetypeLoader;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Helper to load archetypes.
 *
 * @author Tim Anderson
 */
public class ArchetypeLoadHelper {

    /**
     * The archetype loader.
     */
    private final ArchetypeLoader loader;

    /**
     * Determines if logging should be verbose or not.
     */
    private boolean verbose = false;

    /**
     * Constructs a {@link ArchetypeLoadHelper}.
     *
     * @param service the archetype service
     */
    public ArchetypeLoadHelper(IArchetypeRuleService service) {
        this.loader = ArchetypeLoader.newBootstrapLoader(service);
    }

    /**
     * Determines if logging will be verbose.
     *
     * @param verbose if {@code true} perform verbose logging.
     */
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * Loads archetypes from the specified file.
     *
     * @param file the file
     */
    public void load(String file) {
        loader.setVerbose(verbose);
        loader.loadArchetypes(file);
    }

    /**
     * Loads all archetypes from the specified directory.
     *
     * @param dir the directory
     */
    public void loadAll(String dir) {
        loader.setVerbose(verbose);
        dir = PathHelper.replaceHome(dir);
        Path mappingFile = Paths.get(dir, "org/openvpms/archetype/assertionTypes.xml");
        loader.loadAssertions(mappingFile.toString());
        loader.loadArchetypes(dir, true);
    }
}
