/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.util;

import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Spring application context helper.
 *
 * @author Tim Anderson
 */
public class ApplicationContextHelper {

    /**
     * Loads the Spring application context.
     *
     * @param properties the configuration properties
     * @return a new context
     */
    public static ApplicationContext getContext(ConfigProperties properties) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
        PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
        configurer.setProperties(properties.getProperties());
        context.addBeanFactoryPostProcessor(configurer);
        context.setConfigLocation("toolbox-context.xml");
        context.refresh();
        return context;
    }
}
