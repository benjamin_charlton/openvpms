/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.system.common.crypto.KeyGenerator;
import org.openvpms.tool.toolbox.util.PathHelper;

import java.io.Console;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Creates or updates an openvpms.properties file.
 *
 * @author Tim Anderson
 */
public class ConfigCreator {

    /**
     * Creates or updates an openvpms.properties file from the openvpms-defaults.properties.
     * <p/>
     * This does not prompt for values.<br/>
     * If the openvpms.properties file exists, only those properties that aren't present will be updated.
     *
     * @param path the openvpms.properties file path. If {@code null}, uses the default.
     * @throws IOException for any I/O error
     */
    public void createDefault(Path path) throws IOException {
        DefaultConfig defaults = new DefaultConfig();
        ConfigPropertiesUpdater config = new ConfigPropertiesUpdater(path);
        generate(config, new FallbackConfig(config.getProperties(), defaults.getProperties()));
    }

    /**
     * Creates or updates openvpms.properties, prompting for each property.
     *
     * @param path the openvpms.properties file path. If {@code null}, uses the default.
     * @throws IOException for any I/O error
     */
    public void prompt(Path path) throws IOException {
        Console console = System.console();
        if (console == null) {
            throw new IOException("This command must be executed in a console");
        }
        DefaultConfig defaults = new DefaultConfig();
        ConfigPropertiesUpdater config = new ConfigPropertiesUpdater(path);
        ConsoleConfig consoleConfig = new ConsoleConfig(console, config.getProperties(), defaults.getProperties());
        generate(config, consoleConfig);
    }

    /**
     * Generates the configuration.
     *
     * @param config the configuration
     * @param source the source configuration
     * @throws IOException for any I/O error
     */
    private void generate(ConfigPropertiesUpdater config, Config source) throws IOException {
        config.setDriver(source.getDriver());
        config.setUrl(source.getUrl());
        config.setUsername(source.getUsername());
        config.setPassword(source.getPassword());
        config.setReportingUrl(source.getReportingUrl());
        config.setReportingUsername(source.getReportingUsername());
        config.setReportingPassword(source.getReportingPassword());

        if (StringUtils.isEmpty(config.getKey())) {
            config.setKey(KeyGenerator.generate(14, 32));
        }

        if (config.isModified()) {
            config.write();
        }
        Path relative = PathHelper.getPathRelativeToCWD(config.getPath());
        try {
            config.setPermissions();
        } catch (Exception exception) {
            System.err.printf("\nThe file %s should have restrictive permissions to protect passwords\n", relative);
        }

        System.out.printf("\nTake a secure backup of the file %s\nThis needs to be kept for subsequent updates.\n\n",
                          relative);
    }
}
