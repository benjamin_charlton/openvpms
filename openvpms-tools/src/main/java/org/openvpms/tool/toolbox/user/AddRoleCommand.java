/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Adds roles to a user.
 *
 * @author Tim Anderson
 */
@Command(name = "--add-role", description = "Adds roles to a user")
public class AddRoleCommand extends AbstractRoleCommand {

    /**
     * The roles.
     */
    @CommandLine.Parameters(description = "The roles to add", index = "1..*", arity = "1..*")
    String[] roles;

    /**
     * Updates roles on a user.
     *
     * @param user the user to update
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int updateUser(User user) {
        int result;
        int added = 0;
        List<SecurityRole> newRoles = getRoles(roles);
        if (!newRoles.isEmpty()) {
            for (SecurityRole newRole : newRoles) {
                if (!user.getRoles().contains(newRole)) {
                    user.addRole(newRole);
                    added++;
                } else {
                    System.out.println("User already has role: " + newRole.getName());
                }
            }
            if (added != 0) {
                save(user);
                System.out.println("User updated");
            }
            result = 0;
        } else {
            result = 1;
        }
        return result;
    }

    /**
     * Returns the roles corresponding to the supplied names.
     *
     * @param names the role names
     * @return the roles, or an empty list if one could not be found
     */
    protected List<SecurityRole> getRoles(String[] names) {
        List<SecurityRole> result = new ArrayList<>();
        for (String name : names) {
            SecurityRole role = getRole(name);
            if (role == null) {
                System.err.println("Role not found: " + name);
                return Collections.emptyList();
            } else {
                result.add(role);
            }
        }
        return result;
    }

    /**
     * Returns the role matching the specified name.
     *
     * @param name the role name
     * @return the corresponding role, or {@code null} if none is found
     */
    private SecurityRole getRole(String name) {
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<SecurityRole> query = builder.createQuery(SecurityRole.class);
        Root<SecurityRole> from = query.from(SecurityRole.class, UserArchetypes.ROLE);
        query.where(builder.equal(from.get("name"), name));
        query.orderBy(builder.asc(from.get("id")));
        return service.createQuery(query).getFirstResult();
    }

}
