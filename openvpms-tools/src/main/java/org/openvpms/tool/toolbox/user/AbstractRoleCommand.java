/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.component.business.domain.im.security.User;
import picocli.CommandLine;

/**
 * Base class for commands updating user roles.
 *
 * @author Tim Anderson
 */
public abstract class AbstractRoleCommand extends AbstractUserCommand {

    /**
     * The user name.
     */
    @CommandLine.Parameters(index = "0", arity = "1")
    String user;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result;
        User match = (User) getUser(user);
        if (match != null) {
            result = updateUser(match);
        } else {
            result = 1;
            System.err.println("User not found");
        }
        return result;
    }

    /**
     * Updates roles on a user.
     *
     * @param user the user to update
     * @return {@code 0} indicates success, non-zero failure
     */
    protected abstract int updateUser(User user);

}
