/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.jar.JarOutputStream;

/**
 * Replaces the openvpms.properties file in the war.
 *
 * @author Tim Anderson
 */
class CopyPropertiesPatch implements Patch {

    /**
     * The path to openvpms.properties.
     */
    private final Path properties;

    /**
     * Constructs a {@link CopyPropertiesPatch}.
     *
     * @param properties the path to the openvpms.properties
     */
    public CopyPropertiesPatch(Path properties) {
        this.properties = properties;
    }

    /**
     * Applies a patch.
     *
     * @param input  input stream to the entry to patch
     * @param output the entry to write to
     * @throws IOException for any I/O error
     */
    @Override
    public void apply(InputStream input, JarOutputStream output) throws IOException {
        try (InputStream propertyStream = Files.newInputStream(properties)) {
            IOUtils.copy(propertyStream, output);
        }
    }
}
