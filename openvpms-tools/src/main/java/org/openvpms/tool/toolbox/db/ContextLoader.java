/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.util.ApplicationContextHelper;
import org.springframework.context.ApplicationContext;

/**
 * Lazily loads the Spring application context.
 *
 * @author Tim Anderson
 */
public class ContextLoader {

    /**
     * The configuration properties.
     */
    private final ConfigProperties properties;

    /**
     * The context.
     */
    private ApplicationContext context;

    /**
     * Constructs a {@link ContextLoader}.
     *
     * @param properties the configuration properties
     */
    public ContextLoader(ConfigProperties properties) {
        this.properties = properties;
    }

    /**
     * Returns the Spring application context.
     *
     * @return the context
     */
    public ApplicationContext getContext() {
        if (context == null) {
            context = ApplicationContextHelper.getContext(properties);
        }
        return context;
    }
}