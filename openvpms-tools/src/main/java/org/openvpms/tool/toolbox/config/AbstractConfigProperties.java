/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

/**
 * Properties backed implementation of {@link Config}.
 *
 * @author Tim Anderson
 */
public class AbstractConfigProperties implements Config {

    /**
     * The properties.
     */
    private final Properties properties;

    /**
     * Constructs an {@link AbstractConfigProperties}.
     *
     * @param properties the underlying properties
     */
    public AbstractConfigProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * Returns the JDBC driver class name.
     *
     * @return the JDBC driver class name. May be {@code null}
     */
    @Override
    public String getDriver() {
        return get(DB_DRIVER);
    }

    /**
     * Returns the database URL.
     *
     * @return the database URL. May be {@code null}
     */
    @Override
    public String getUrl() {
        return get(DB_URL);
    }

    /**
     * Returns the database username.
     *
     * @return the database username. May be {@code null}
     */
    @Override
    public String getUsername() {
        return get(DB_USERNAME);
    }

    /**
     * Returns the database password.
     *
     * @return the database password. May be {@code null}
     */
    @Override
    public String getPassword() {
        return get(DB_PASSWORD);
    }

    /**
     * Returns the reporting database URL.
     *
     * @return the reporting database URL. May be {@code null}
     */
    @Override
    public String getReportingUrl() {
        return get(REPORTING_DB_URL);
    }

    /**
     * Returns the reporting username.
     *
     * @return the reporting username. May be {@code null}
     */
    @Override
    public String getReportingUsername() {
        return get(REPORTING_DB_USERNAME);
    }

    /**
     * Returns the reporting password.
     *
     * @return the reporting password. May be {@code null}
     */
    @Override
    public String getReportingPassword() {
        return get(REPORTING_DB_PASSWORD);
    }

    /**
     * Returns the encryption key.
     *
     * @return the encryption key. May be {@code null}
     */
    @Override
    public String getKey() {
        return get(OPENVPMS_KEY);
    }

    /**
     * Returns the properties.
     *
     * @return the properties
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Returns a property.
     *
     * @param name the property name
     * @return the property value. May be {@code null}
     */
    protected String get(String name) {
        return StringUtils.trimToNull(properties.getProperty(name));
    }
}