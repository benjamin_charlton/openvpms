/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.archetype;

import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import org.openvpms.tools.archetype.diff.ArchDiff;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * Command to list archetypes.
 *
 * @author Tim Anderson
 */
@Command(name = "--list", description = "List archetypes")
public class ArchetypeListCommand extends AbstractApplicationContextCommand {

    @Parameters(index = "0", description = "The archetypes to list.", arity = "0", defaultValue = "db")
    String source;

    @Option(names = {"--no-recurse", "-n"}, description = "Disable search of subdirectories.")
    boolean norecurse;

    @Option(names = {"--verbose", "-v"}, description = "Displays verbose info to the console.")
    boolean verbose;

    /**
     * Constructs an {@link ArchetypeListCommand}.
     */
    public ArchetypeListCommand() {
        super(true); // can still list if the database is not up-to-date, unless there are breaking schema changes
    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        ArchDiff diff = new ArchDiff(getBean(IArchetypeRuleService.class));
        diff.list(diff.getDescriptorLoader(source, !norecurse), verbose);
        return 0;
    }
}
