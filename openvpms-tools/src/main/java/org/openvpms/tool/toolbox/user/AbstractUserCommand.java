/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.component.business.dao.hibernate.im.IMObjectDAOHibernate;
import org.openvpms.component.business.dao.hibernate.im.security.UserDAOHibernate;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;

import java.util.List;

/**
 * Base class for commands operating on users.
 *
 * @author Tim Anderson
 */
public abstract class AbstractUserCommand extends AbstractApplicationContextCommand {

    /**
     * Returns a user given its name.
     *
     * @param username the user name
     * @return the corresponding user, or {@code null} if none is found
     */
    protected User getUser(String username) {
        UserDAOHibernate dao = getBean(UserDAOHibernate.class);
        List<org.openvpms.component.business.domain.im.security.User> users = dao.getByUserName(username);
        return (users.size() == 1) ? users.get(0) : null;
    }

    /**
     * Saves the specified user.
     *
     * @param user the user
     */
    protected void save(User user) {
        IMObjectDAOHibernate dao = getBean(IMObjectDAOHibernate.class);
        dao.save((IMObject) user);
    }

    /**
     * Sets a user active or inactive.
     *
     * @param username the user name
     * @param active   if {@code true}, make the user active, else make it inactive
     * @return {@code true} if the user was found
     */
    protected boolean setActive(String username, boolean active) {
        boolean result = false;
        User match = getUser(username);
        if (match != null) {
            if (active != match.isActive()) {
                match.setActive(active);
                save(match);
                System.out.println("User " + ((active) ? "enabled" : "disabled"));
            } else {
                System.out.println("User already " + ((active) ? "enabled" : "disabled"));
            }
            result = true;
        } else {
            System.err.println("User not found");
        }
        return result;
    }

}


