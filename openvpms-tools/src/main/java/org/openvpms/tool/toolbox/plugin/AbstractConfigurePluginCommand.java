/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.plugin;

import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;

/**
 * Enables/disables plugins.
 *
 * @author Tim Anderson
 */
abstract class AbstractConfigurePluginCommand extends AbstractApplicationContextCommand {

    /**
     * Enable plugins node name.
     */
    private static final String ENABLE_PLUGINS = "enablePlugins";

    /**
     * Enables/disables plugins.
     *
     * @param enable if {@code true} enable plugins, else disable them
     * @return {@code 0} if the operation was successful, {@code 1} if not
     */
    protected int enable(boolean enable) {
        int result = 1;
        IMObjectBean practice = getPractice();
        if (practice != null) {
            if (practice.getBoolean(ENABLE_PLUGINS) != enable) {
                practice.setValue(ENABLE_PLUGINS, enable);
                practice.save();
                if (enable) {
                    System.out.println("Plugins enabled. If OpenVPMS is already running it needs to be restarted.");
                } else {
                    System.out.println("Plugins disabled. If OpenVPMS is already running it needs to be restarted.");
                }
            } else if (enable) {
                System.out.println("Plugins already enabled.");
            } else {
                System.out.println("Plugins already disabled.");
            }
            result = 0;
        } else {
            System.out.println("No practice found.");
        }
        return result;
    }

    /**
     * Returns the practice
     *
     * @return the practice, or {@code null} if it does not exist
     */
    protected IMObjectBean getPractice() {
        IArchetypeRuleService service = getBean(IArchetypeRuleService.class);
        Party practice = PracticeRules.getPractice(service);
        return (practice != null) ? service.getBean(practice) : null;
    }
}