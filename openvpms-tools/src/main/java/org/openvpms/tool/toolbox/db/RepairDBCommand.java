/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.tool.db.DBTool;
import picocli.CommandLine.Command;

/**
 * Command to repair the migration information in a database.
 *
 * @author Tim Anderson
 */
@Command(name = "--repair", header = "Repairs the database", hidden = true)
public class RepairDBCommand extends AbstractDBCommand {

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        DBTool tool = getTool();
        tool.repair();
        return 0;
    }
}
