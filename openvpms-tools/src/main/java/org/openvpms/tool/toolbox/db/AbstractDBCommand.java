/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.PluginMigrator;
import org.openvpms.tool.db.DBTool;
import org.openvpms.tool.toolbox.AbstractCommand;
import org.openvpms.tool.toolbox.archetype.ArchetypeLoadHelper;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.util.PathHelper;

import java.io.File;
import java.sql.SQLException;

/**
 * Base class for commands that perform low-level database changes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDBCommand extends AbstractCommand {

    /**
     * The database tool.
     */
    private DBTool tool;

    /**
     * The configuration properties.
     */
    private ConfigProperties properties;

    /**
     * Plugin migrator that does nothing. This will ensure that the plugin checksum is updated.
     */
    private static final PluginMigrator NO_OP_MIGRATOR = () -> {
    };

    /**
     * Initialises the command.
     *
     * @throws Exception for any error
     */
    @Override
    protected void init() throws Exception {
        properties = loadProperties();
        checkProperties(properties);
        checkDriver(properties);
        tool = createDBTool(properties);
    }

    /**
     * Creates a new database tool.
     *
     * @param properties the configuration properties
     * @return a new database tool
     */
    protected DBTool createDBTool(ConfigProperties properties) {
        return new DBTool(properties.getDriver(), properties.getUrl(), properties.getUsername(),
                          properties.getPassword());
    }

    /**
     * Returns the configuration properties.
     *
     * @return the properties
     */
    protected ConfigProperties getProperties() {
        return properties;
    }

    /**
     * Returns the database tool.
     *
     * @return the database tool
     */
    protected DBTool getTool() {
        return tool;
    }

    /**
     * Gets a directory.
     *
     * @param dir      the directory. May contain ${openvpms.home}
     * @param argument the commandline argument, for error reporting
     * @return the directory, with any ${openvpms.home} replaced with the actual path
     * @throws IllegalStateException if the directory is invalid
     */
    protected String getDir(String dir, String argument) {
        dir = PathHelper.replaceHome(dir);
        if (!new File(dir).isDirectory()) {
            throw new IllegalStateException("Argument " + argument + " is not a valid directory");
        }
        return dir;
    }

    /**
     * Updates the database.
     * <p/>
     * This runs any necessary migrations, including loading archetypes.
     *
     * @param archetypesDir the archetypes directory
     * @param pluginsDir    the plugins directory, or {@code null} if plugins aren't being loaded
     */
    protected void updateDatabase(String archetypesDir, String pluginsDir) throws SQLException {
        ContextLoader loader = new ContextLoader(getProperties());
        updateDatabase(archetypesDir, pluginsDir, loader);
    }

    /**
     * Updates the database.
     * <p/>
     * This runs any necessary migrations, including loading archetypes.
     *
     * @param archetypesDir the archetypes directory
     * @param pluginsDir    the plugins directory, or {@code null} if plugins aren't being loaded
     * @param loader        the Spring application context loader
     */
    protected void updateDatabase(String archetypesDir, String pluginsDir, ContextLoader loader) throws SQLException {
        String key = getProperties().getKey();
        System.setProperty(ConfigProperties.OPENVPMS_KEY, key);
        try {
            ArchetypeMigrator migrator = () -> {
                IArchetypeRuleService service = loader.getContext().getBean(IArchetypeRuleService.class);
                ArchetypeLoadHelper helper = new ArchetypeLoadHelper(service);
                helper.loadAll(archetypesDir);
            };
            PluginMigrator pluginMigrator = (pluginsDir != null) ? new PluginMigratorImpl(loader, pluginsDir)
                                                                 : NO_OP_MIGRATOR;
            tool.update(migrator, pluginMigrator);
        } finally {
            System.getProperties().remove(ConfigProperties.OPENVPMS_KEY);
        }
    }
}
