
/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.db;


import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.callback.BaseFlywayCallback;
import org.flywaydb.core.api.callback.FlywayCallback;
import org.flywaydb.core.internal.info.MigrationInfoDumper;
import org.flywaydb.core.internal.util.TimeFormat;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.PluginMigrator;
import org.openvpms.db.service.impl.DatabaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Tool to create and migrate OpenVPMS databases.
 *
 * @author Tim Anderson
 */
public class DBTool {

    /**
     * The OpenVPMS user.
     */
    private final String user;

    /**
     * The OpenVPMS user password.
     */
    private final String password;

    /**
     * The database service.
     */
    private final DatabaseServiceImpl service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DBTool.class);

    /**
     * Constructs a {@link DBTool}.
     *
     * @param driver   the driver class name
     * @param url      the driver url
     * @param user     the database user name
     * @param password the database password
     */
    public DBTool(String driver, String url, String user, String password) {
        this(driver, url, user, password, false);
    }

    /**
     * Constructs a {@link DBTool}.
     *
     * @param driver   the driver class name
     * @param url      the driver url
     * @param user     the database user name
     * @param password the database password
     * @param headless if {@code true}, don't log to the console
     */
    public DBTool(String driver, String url, String user, String password, boolean headless) {
        this(driver, url, user, password, new FlywayLogger(headless));
    }

    /**
     * Constructs a {@link DBTool}.
     *
     * @param driver   the driver class name
     * @param url      the driver url
     * @param user     the database user name
     * @param password the database password
     * @param listener the listener to notify of Flyway events. May be {@code null}
     */
    public DBTool(String driver, String url, String user, String password, FlywayCallback listener) {
        this.user = user;
        this.password = password;
        service = new DatabaseServiceImpl(driver, url, user, password, listener);
    }

    /**
     * Determines if the database exists.
     *
     * @param adminUser     the admin user
     * @param adminPassword the admin password
     * @return {@code true} if the database exists, otherwise {@code false}
     * @throws SQLException for any SQL error
     */
    public boolean exists(String adminUser, String adminPassword) throws SQLException {
        return service.exists(adminUser, adminPassword);
    }

    /**
     * Creates the database and tables, if it doesn't already exist.
     *
     * @param adminUser     the admin user
     * @param adminPassword the admin password
     * @param host          the host the user is connecting from
     * @param createTables  if {@code true}, create the tables, and base-line
     * @throws SQLException for any SQL error
     */
    public void create(String adminUser, String adminPassword, String host, boolean createTables) throws SQLException {
        service.create(adminUser, adminPassword, user, password, host, createTables);
        log.info("Created {} ", service.getSchemaName());
    }

    /**
     * Repair database version meta-data.
     */
    public void repair() {
        service.repair();
    }

    /**
     * Displays the database version.
     */
    public void version() {
        String version = service.getVersion();
        if (version == null) {
            log.info("Database '{}' has no version information", service.getSchemaName());
        } else {
            log.info("Database '{}' is at version {}", service.getSchemaName(), version);
        }
    }

    /**
     * Displays migration info.
     */
    public void info() {
        log.info(MigrationInfoDumper.dumpToAsciiTable(service.getInfo().all()));
    }

    /**
     * Determines if the database needs updating.
     *
     * @return {@code true} if the database needs updating
     */
    public boolean needsUpdate() {
        return service.needsUpdate();
    }

    /**
     * Returns the number of migration steps that need to be applied to bring the database up-to-date.
     *
     * @return the number of migration steps, or {@code 0} if no update is required
     */
    public int getChangesToApply() {
        return service.getChangesToApply();
    }

    /**
     * Updates the database to the latest version.
     *
     * @param archetypeMigrator the archetype migrator
     * @param pluginMigrator    the plugin migrator, or {@code null} if plugins aren't being loaded
     * @throws SQLException for any SQL error
     */
    public void update(ArchetypeMigrator archetypeMigrator, PluginMigrator pluginMigrator) throws SQLException {
        String version = service.getVersion();
        String schemaName = service.getSchemaName();
        if (service.needsUpdate()) {
            StopWatch stopWatch = StopWatch.createStarted();
            service.update(archetypeMigrator, pluginMigrator);
            if (version == null) {
                // no version information
                log.info("Database '{}' updated to version {} in {}", schemaName, service.getVersion(), stopWatch);
            } else {
                log.info("Database '{}' updated from version {} to {} in {}", schemaName, version, service.getVersion(),
                         stopWatch);
            }
        } else {
            log.info("Database '{}' is up to date", schemaName);
        }
    }

    /**
     * Displays the database size.
     *
     * @throws SQLException for any SQL error
     */
    public void size() throws SQLException {
        long size = service.getSize();
        String value;
        if (size / (10 * FileUtils.ONE_GB) > 0) {
            value = getSize(size, FileUtils.ONE_GB, "GB");
        } else if (size / FileUtils.ONE_MB > 0) {
            value = getSize(size, FileUtils.ONE_MB, "MB");
        } else if (size / FileUtils.ONE_KB > 0) {
            value = getSize(size, FileUtils.ONE_KB, "KB");
        } else {
            value = size + " bytes";
        }
        log.info(service.getSchemaName() + " " + value);
    }

    /**
     * Helper to return a formatted size, rounded.
     *
     * @param size    the size
     * @param divisor the divisor
     * @param suffix  the size suffix
     * @return the formatted size
     */
    private String getSize(long size, long divisor, String suffix) {
        BigDecimal result = new BigDecimal(size).divide(BigDecimal.valueOf(divisor), 2, RoundingMode.CEILING);
        DecimalFormat format = new DecimalFormat("#,##.##");
        return format.format(result) + " " + suffix;
    }

    private static class FlywayLogger extends BaseFlywayCallback {

        private final Map<MigrationInfo, StopWatch> state = new HashMap<>();

        private final boolean headless;

        public FlywayLogger(boolean headless) {
            this.headless = headless;
        }

        @Override
        public void beforeEachMigrate(Connection connection, MigrationInfo info) {
            StopWatch watch = new StopWatch();
            state.put(info, watch);
            if (info.getVersion() != null) {
                if (headless) {
                    log.info("Updating to {} - {}", info.getVersion(), info.getDescription());
                } else {
                    System.out.print("Updating to " + info.getVersion() + " - " + info.getDescription() + " ... ");
                }
            } else {
                if (headless) {
                    log.info("Running {} ", info.getDescription());
                } else {
                    System.out.print("Running " + info.getDescription() + " ... ");
                }
            }
            watch.start();
        }

        @Override
        public void afterEachMigrate(Connection connection, MigrationInfo info) {
            StopWatch watch = state.get(info);
            if (watch != null) {
                watch.stop();
                String time = TimeFormat.format(watch.getTime());
                if (headless) {
                    log.info("Completed {} in {}", info.getDescription(), time);
                } else {
                    System.out.println(time);
                }
            }
        }
    }
}
