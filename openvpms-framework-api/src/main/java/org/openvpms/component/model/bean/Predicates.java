/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;


/**
 * Predicates for {@link Relationship} instances.
 *
 * @author Tim Anderson
 */
public final class Predicates {

    /**
     * Predicate that determines if relationships are active as at the time
     * of evaluation.
     */
    private static final Predicate<?> ACTIVE_NOW = new IsActiveAt<>();

    /**
     * Default constructor.
     */
    private Predicates() {
        // no-op
    }

    /**
     * Returns a predicate that determines if relationships are active as at the time of evaluation.
     *
     * @return a predicate that returns {@code true} if a relationship is active at time of evaluation
     */
    @SuppressWarnings("unchecked")
    public static <T extends Relationship> Predicate<T> activeNow() {
        return (Predicate<T>) ACTIVE_NOW;
    }

    /**
     * Creates a new predicate that evaluates {@code true} if an {@link Relationship} or
     * {@link PeriodRelationship} is active.
     * The {@link PeriodRelationship} must be active at the specified time.
     *
     * @param time the time
     * @return a new predicate
     */
    public static <T extends Relationship> Predicate<T> activeAt(Date time) {
        return new IsActiveAt<>(time);
    }

    /**
     * Creates a new predicate that evaluates {@code true} if an {@link Relationship} or
     * {@link PeriodRelationship} is active.
     * The {@link PeriodRelationship} must be active within the specified time range.
     *
     * @param from the from date. May be {@code null}
     * @param to   the to date. May be {@code null}
     * @return a new predicate
     */
    public static <T extends Relationship> Predicate<T> active(Date from, Date to) {
        return new IsActiveRange<>(from, to);
    }

    /**
     * Returns a predicate that determines if the source of an {@link Relationship} is that of the supplied
     * object.
     *
     * @param object the object. May be {@code null}
     * @return a predicate
     */
    public static <T extends Relationship> Predicate<T> sourceEquals(IMObject object) {
        return new SourceEquals<>(object);
    }

    /**
     * Returns a predicate that determines if the source of a {@link Relationship} is that of the supplied
     * reference.
     *
     * @param object the object. May be {@code null}
     * @return a predicate
     */
    public static <T extends Relationship> Predicate<T> sourceEquals(Reference object) {
        return new SourceEquals<>(object);
    }

    /**
     * Returns a predicate that determines if the target of a {@link Relationship} is that of the supplied
     * object.
     *
     * @param object the object. May be {@code null}
     * @return a predicate
     */
    public static <T extends Relationship> Predicate<T> targetEquals(IMObject object) {
        return new TargetEquals<>(object);
    }

    /**
     * Returns a predicate that determines if the target of an {@link Relationship} is that of the supplied
     * reference.
     *
     * @param object the object. May be {@code null}
     * @return a predicate
     */
    public static <T extends Relationship> Predicate<T> targetEquals(Reference object) {
        return new TargetEquals<>(object);
    }

    /**
     * Returns a predicate that determines if the source of a {@link Relationship} is one of the specified set of
     * archetypes.
     *
     * @param archetypes the archetypes
     * @return a predicate
     */
    public static <T extends Relationship> Predicate<T> sourceIsA(String... archetypes) {
        return new SourceIsA<>(archetypes);
    }

    /**
     * Returns a predicate that determines if the target of a {@link Relationship} is one of the specified set of
     * archetypes.
     *
     * @param archetypes the archetypes
     * @return a predicate
     */
    public static <T extends Relationship> Predicate<T> targetIsA(String... archetypes) {
        return new TargetIsA<>(archetypes);
    }

    /**
     * Returns a predicate that determines if an object is one of the specified set of archetypes.
     *
     * @param archetypes the archetypes
     * @return a new predicate
     */
    public static <T extends IMObject> Predicate<T> isA(String... archetypes) {
        return new ObjectIsA<>(archetypes);
    }

    private abstract static class IsA<T> implements Predicate<T> {

        private final String[] archetypes;

        public IsA(String[] archetypes) {
            this.archetypes = archetypes;
        }

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param object the input argument
         * @return {@code true} if the input argument matches the predicate,
         * otherwise {@code false}
         */
        @Override
        public boolean test(T object) {
            return test(object, archetypes);
        }

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param object     the input argument
         * @param archetypes the archetypes
         * @return {@code true} if the input argument matches the predicate, otherwise {@code false}
         */
        abstract boolean test(T object, String[] archetypes);

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof IsA && Arrays.equals(archetypes, ((IsA<?>) obj).archetypes);
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Arrays.hashCode(archetypes);
        }
    }

    private static class ObjectIsA<T extends IMObject> extends IsA<T> {

        public ObjectIsA(String[] archetypes) {
            super(archetypes);
        }

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param object     the input argument
         * @param archetypes the archetypes
         * @return {@code true} if the input argument matches the predicate, otherwise {@code false}
         */
        @Override
        boolean test(T object, String[] archetypes) {
            return object.isA(archetypes);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof ObjectIsA && super.equals(obj);
        }
    }

    private static class SourceIsA<T extends Relationship> extends IsA<T> {

        public SourceIsA(String[] archetypes) {
            super(archetypes);
        }

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param object     the input argument
         * @param archetypes the archetypes
         * @return {@code true} if the input argument matches the predicate, otherwise {@code false}
         */
        @Override
        boolean test(T object, String[] archetypes) {
            Reference reference = object.getSource();
            return reference != null && reference.isA(archetypes);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof SourceIsA && super.equals(obj);
        }
    }

    private static class TargetIsA<T extends Relationship> extends IsA<T> {

        public TargetIsA(String[] archetypes) {
            super(archetypes);
        }

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param object     the input argument
         * @param archetypes the archetypes
         * @return {@code true} if the input argument matches the predicate, otherwise {@code false}
         */
        @Override
        boolean test(T object, String[] archetypes) {
            Reference reference = object.getTarget();
            return reference != null && reference.isA(archetypes);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof TargetIsA && super.equals(obj);
        }
    }

    private static class IsActive<T extends Relationship> implements Predicate<T> {
        /**
         * Determines if a relationship is active.
         *
         * @param relationship the relationship
         * @return {@code true} if the relationship matches the predicate, otherwise {@code false}
         */
        @Override
        public boolean test(T relationship) {
            return relationship.isActive();
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof IsActive;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    }

    private static class IsActiveAt<T extends Relationship> extends IsActive<T> {

        /**
         * The time to compare with. If {@code -1}, indicates to use the
         * system time at the time of comparison.
         */
        private final long time;

        /**
         * Constructs an {@link IsActiveAt} that evaluates {@code true} for all relationships that are active as of the
         * time of evaluation.
         */
        IsActiveAt() {
            this(-1);
        }

        /**
         * Constructs an {@link IsActiveAt} that evaluates {@code true} for all relationships that are active at the
         * specified time.
         *
         * @param time the time to compare against
         */
        IsActiveAt(Date time) {
            this(time.getTime());
        }

        /**
         * Creates a new {@code Predicates} that evaluates {@code true}
         * for all relationships that are active at the specified time.
         *
         * @param time the time to compare against. If {@code -1}, indicates to use the system time at evaluation
         */
        IsActiveAt(long time) {
            this.time = time;
        }

        /**
         * Determines if a relationship is active.
         *
         * @param relationship the object to evaluate. Must be an {@code Relationship}
         *                     or {@code PeriodRelationship}
         * @return {@code true} if the relationship is active, otherwise {@code false}
         */
        @Override
        public boolean test(T relationship) {
            boolean result;
            if (relationship instanceof PeriodRelationship) {
                PeriodRelationship period = (PeriodRelationship) relationship;
                result = (time == -1) ? period.isActive() : period.isActive(time);
            } else {
                result = super.test(relationship);
            }
            return result;
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (obj instanceof IsActiveAt) {
                IsActiveAt<?> other = (IsActiveAt<?>) obj;
                return time == other.time;
            }
            return false;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Long.hashCode(time);
        }
    }

    private static class IsActiveRange<T extends Relationship> extends IsActive<T> {

        /**
         * The from date. May be {@code null}
         */
        private final Date from;

        /**
         * The to date. May be {@code null}
         */
        private final Date to;

        /**
         * Constructs an {@link Predicates.IsActiveRange}.
         *
         * @param from the from date. May be {@code null}
         * @param to   the to date. May be {@code null}
         */
        IsActiveRange(Date from, Date to) {
            this.from = from;
            this.to = to;
        }

        /**
         * Determines if a relationship is active.
         *
         * @param relationship the relationship
         * @return {@code true} if the relationship matches the predicate, otherwise {@code false}
         */
        @Override
        public boolean test(T relationship) {
            boolean result;
            if (relationship instanceof PeriodRelationship) {
                PeriodRelationship period = (PeriodRelationship) relationship;
                result = period.isActive(from, to);
            } else {
                result = super.test(relationship);
            }
            return result;
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (obj instanceof IsActiveRange) {
                IsActiveRange<?> other = (IsActiveRange<?>) obj;
                return Objects.equals(from, other.from) && Objects.equals(to, other.to);
            }
            return false;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Objects.hash(from, to);
        }
    }

    private abstract static class RefEquals<T extends Relationship> implements Predicate<T> {

        /**
         * The reference to compare.
         */
        private final Reference ref;

        RefEquals(IMObject object) {
            this(object != null ? object.getObjectReference() : null);
        }

        RefEquals(Reference ref) {
            this.ref = ref;
        }

        /**
         * Evaluates this predicate on the given argument.
         *
         * @param relationship the input argument
         * @return {@code true} if the input argument matches the predicate, otherwise {@code false}
         */
        @Override
        public boolean test(T relationship) {
            return test(relationship, ref);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (obj instanceof RefEquals) {
                RefEquals<?> other = (RefEquals<?>) obj;
                return Objects.equals(ref, other.ref);
            }
            return false;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Objects.hash(ref);
        }

        /**
         * Evaluates this predicate on the given relationship.
         *
         * @param relationship the relationship
         * @param reference    the reference to compare
         * @return {@code true} if the relationship matches the predicate, otherwise {@code false}
         */
        protected abstract boolean test(T relationship, Reference reference);
    }

    private static class SourceEquals<T extends Relationship> extends RefEquals<T> {

        SourceEquals(IMObject object) {
            super(object);
        }

        SourceEquals(Reference ref) {
            super(ref);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof SourceEquals && super.equals(obj);
        }

        /**
         * Evaluates this predicate on the given relationship.
         *
         * @param relationship the relationship
         * @param reference    the reference to compare
         * @return {@code true} if the relationship matches the predicate, otherwise {@code false}
         */
        @Override
        protected boolean test(T relationship, Reference reference) {
            return Objects.equals(reference, relationship.getSource());
        }
    }

    private static class TargetEquals<T extends Relationship> extends RefEquals<T> {

        TargetEquals(IMObject object) {
            super(object);
        }

        TargetEquals(Reference ref) {
            super(ref);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            return obj instanceof TargetEquals && super.equals(obj);
        }

        /**
         * Evaluates this predicate on the given relationship.
         *
         * @param relationship the relationship
         * @param reference    the reference to compare
         * @return {@code true} if the relationship matches the predicate, otherwise {@code false}
         */
        @Override
        protected boolean test(T relationship, Reference reference) {
            return Objects.equals(reference, relationship.getTarget());
        }
    }
}
