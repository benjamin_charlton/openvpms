/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.openvpms.component.model.object.Relationship;

import java.util.Comparator;
import java.util.Date;
import java.util.function.Predicate;

/**
 * Builder for {@link Policy} instances.
 *
 * @author Tim Anderson
 */
public interface PolicyBuilder<R extends Relationship> {

    /**
     * Selects active relationships, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @return this
     */
    PolicyBuilder<R> active();

    /**
     * Selects relationships active at the specified time, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @param time the time
     * @return this
     */
    PolicyBuilder<R> active(Date time);

    /**
     * Selects active objects.
     *
     * @return this
     */
    PolicyBuilder<R> activeObjects();

    /**
     * Selects inactive objects.
     *
     * @return this
     */
    PolicyBuilder<R> inactiveObjects();

    /**
     * Selects both active and inactive objects.
     *
     * @return this
     */
    PolicyBuilder<R> anyObject();

    /**
     * Adds a predicate for filtering relationships.
     *
     * @param predicate the predicate
     * @return this
     */
    PolicyBuilder<R> predicate(Predicate<R> predicate);

    /**
     * Adds a predicate that is a logical AND of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    PolicyBuilder<R> and(Predicate<R> predicate);

    /**
     * Adds a predicate that is a logical OR of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    PolicyBuilder<R> or(Predicate<R> predicate);

    /**
     * Orders relationships on ascending sequence.
     * <p/>
     * Ignored if the relationships aren't sequenced.
     *
     * @return this
     */
    PolicyBuilder<R> orderBySequence();

    /**
     * Orders relationships by sequence.
     * <p/>
     * Ignored if the relationships aren't sequenced.
     *
     * @param ascending if {@code true}, order on ascending sequence, else order on descending sequence
     * @return this
     */
    PolicyBuilder<R> orderBySequence(boolean ascending);

    /**
     * Sets the comparator for ordering relationships.
     *
     * @param comparator the comparator
     * @return this
     */
    PolicyBuilder<R> comparator(Comparator<R> comparator);

    /**
     * Builds the policy
     *
     * @return the new policy
     */
    Policy<R> build();

}
