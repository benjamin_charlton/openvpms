/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.document;

import java.io.InputStream;

/**
 * Builds a {@link Document}.
 *
 * @author Tim Anderson
 */
public interface DocumentBuilder {

    /**
     * Sets the document file name.
     *
     * @param name the file name
     * @return this
     */
    DocumentBuilder fileName(String name);

    /**
     * Sets the document mime type.
     *
     * @param mimeType the mime type
     * @return this
     */
    DocumentBuilder mimeType(String mimeType);

    /**
     * Sets the document content.
     *
     * @param stream the content stream
     * @return this
     */
    DocumentBuilder content(InputStream stream);

    /**
     * Determines if any existing document should versioned.
     *
     * @param version if {@code true}, version any existing document, else replace it
     * @return this
     */
    DocumentBuilder version(boolean version);

    /**
     * Build the document.
     *
     * @return the document
     */
    Document build();

}
